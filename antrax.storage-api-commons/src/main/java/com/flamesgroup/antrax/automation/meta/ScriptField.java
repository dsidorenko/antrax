/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

public enum ScriptField {

  VS_BUSINESS("Business"),
  VS_INCOMING_CALL_MANAGER("Imcoming Call Management"),
  VS_ACTION_PROVIDER("Action Provider"),

  /**
   * Voice server factor script field.
   * <p>
   * Correspond to {@link ScriptType#FACTOR}
   */
  VS_FACTOR("VoiceServer factor"),

  /**
   * SIM server factor script field.
   * <p>
   * Correspond to {@link ScriptType#FACTOR}
   */
  SS_FACTOR("SimServer factor"),

  SS_GW_SELECTOR("Gateway Selector"),

  /**
   * Activity period script field.
   * <p>
   * Correspond to {@link ScriptType#ACTIVITY_PERIOD}
   */
  SS_ACTIVITY("Activity"),

  /**
   * Session script field.
   * <p>
   * Correspond to {@link ScriptType#ACTIVITY_PERIOD}
   */
  SS_SESSION("Session"),

  /**
   * Correspond to {@link ScriptType#IMEI_GENERATOR}
   */
  SS_IMEI_GENERATOR("IMEI Generator"),


  /**
   * Correspond to {@link ScriptType#CALL_FILTER}
   */
  VS_CALL_FILTER("Call Filter"),
  VS_SMS_FILTER("SMS Filter");

  private final String humanString;

  ScriptField(final String hString) {
    this.humanString = hString;
  }

  public static ScriptField strToScriptField(final String scriptField) {
    return (scriptField == null) ? null : valueOf(scriptField);
  }

  public String toHumanString() {
    return humanString;
  }

}
