/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.util.Date;
import java.util.UUID;

public class AlarisHttpSms extends AlarisSms {

  private static final long serialVersionUID = -7030143750632118708L;

  private final LongMessageMode longMessageMode;
  private final String dlrUrl;
  private final String clientIp;

  public AlarisHttpSms(final UUID id, final String ani, final String dnis, final String message, final String serviceType, final LongMessageMode longMessageMode, final Integer dataCoding,
      final Date createTime, final Sar sar, final String dlrUrl, final String username, final String clientIp) {

    super(id, ani, dnis, message, serviceType, dataCoding, createTime, username, sar);
    this.longMessageMode = longMessageMode;
    this.dlrUrl = dlrUrl;
    this.clientIp = clientIp;
    if (sar != null) {
      this.multiPartSmsKey = AlarisHttpSms.class.getSimpleName() + ani + dnis + sar.getId() + sar.getParts() + clientIp;
    }
  }

  public LongMessageMode getLongMessageMode() {
    return longMessageMode;
  }

  public String getDlrUrl() {
    return dlrUrl;
  }

  public String getClientIp() {
    return clientIp;
  }

  @Override
  public String toString() {
    return AlarisHttpSms.class.getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[id:" + getId() +
        " ani:" + getSourceNumber() +
        " dnis:" + getDestNumber() +
        " message:" + getMessage() +
        " status:" + getStatus() +
        " sar:" + getSar() +
        " smsId:" + getSmsId() +
        " username:" + getUsername() +
        " clientIp:" + getClientIp() +
        ']';
  }

  public enum LongMessageMode {
    CUT, SPLIT, SPLIT_SAR, PLAYLOAD
  }

}
