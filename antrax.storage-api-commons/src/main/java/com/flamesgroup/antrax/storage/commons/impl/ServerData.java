/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.VoiceServerConfig;

import java.net.InetAddress;

public class ServerData extends Domain implements IServerData, Cloneable {

  private static final long serialVersionUID = -9061815351158704962L;

  private String name;
  private boolean simServerEnabled;
  private boolean voiceServerEnabled;
  private VoiceServerConfig voiceServerConfig;
  private InetAddress publicAddress;

  public ServerData() {
  }

  public ServerData(final long id) {
    this.id = id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public IServerData setName(final String name) {
    this.name = name;
    return this;
  }

  @Override
  public InetAddress getPublicAddress() {
    return publicAddress;
  }

  @Override
  public IServerData setPublicAddress(final InetAddress publicAddress) {
    this.publicAddress = publicAddress;
    return this;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof ServerData) {
      ServerData that = (ServerData) obj;
      return (name != null && name.equals(that.name)) || super.equals(obj);
    }
    return false;
  }

  @Override
  public boolean isSimServerEnabled() {
    return simServerEnabled;
  }

  @Override
  public void setSimServerEnabled(final boolean simServerEnabled) {
    this.simServerEnabled = simServerEnabled;
  }

  @Override
  public boolean isVoiceServerEnabled() {
    return voiceServerEnabled;
  }

  @Override
  public void setVoiceServerEnabled(final boolean voiceServerEnabled) {
    this.voiceServerEnabled = voiceServerEnabled;
  }

  @Override
  public VoiceServerConfig getVoiceServerConfig() {
    if (isVoiceServerEnabled()) {
      return voiceServerConfig;
    } else {
      return null;
    }
  }

  @Override
  public IServerData setVoiceServerConfig(final VoiceServerConfig config) {
    this.voiceServerConfig = config;
    return this;
  }

}
