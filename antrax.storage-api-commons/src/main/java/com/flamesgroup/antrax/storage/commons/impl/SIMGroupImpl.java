/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

public class SIMGroupImpl extends Domain implements SIMGroup, Cloneable {

  private static final long serialVersionUID = 4843172960993527087L;

  private long revision;

  private String name;

  private String operatorSelection = "auto";

  private Timeout idleAfterRegistration;
  private Timeout idleBeforeUnregistration;
  private Timeout idleAfterSuccessfullCall;
  private Timeout idleAfterZeroCall;

  private int selfDialFactor;

  private int selfDialChance;

  private Timeout idleBeforeSelfCall;

  private Timeout idleAfterSelfCallTimeout;

  private String description;

  private ScriptInstance activityPeriodScript;
  private ScriptInstance[] businessActivityScripts;
  private ScriptInstance factorScript;
  private ScriptInstance gwSelectorScript;
  private ScriptInstance sessionScript;

  private boolean canBeAppointed;

  private ScriptInstance[] actionProviderScripts;

  private ScriptInstance incomingCallManagementScript;

  private ScriptInstance vsFactorScript;

  private ScriptInstance[] callFilterScripts;

  private ScriptInstance[] smsFilterScripts;

  private ScriptInstance imeiGeneratorScript;

  private boolean syntheticRingingEnabled;

  private boolean fasDetection = true;
  private boolean smsDeliveryReport = false;
  private boolean suppressIncomingCallSignal = true;

  private ScriptCommons[] scrptCommons;

  private int registrationPeriod;
  private int registrationCount;

  public SIMGroupImpl(final long id) {
    this.id = id;
  }

  public SIMGroupImpl() {

  }

  public SIMGroupImpl(final String name) {
    this();
    setName(name);
  }

  @Override
  public long getRevision() {
    return revision;
  }

  @Override
  public void setRevision(final long rev) {
    revision = rev;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getOperatorSelection() {
    return operatorSelection;
  }

  @Override
  public SIMGroupImpl setName(final String name) {
    this.name = name;
    return this;
  }

  @Override
  public SIMGroupImpl setOperatorSelection(final String operatorSelection) {
    this.operatorSelection = operatorSelection;
    return this;
  }

  @Override
  public Timeout getIdleAfterRegistration() {
    return idleAfterRegistration;
  }

  @Override
  public Timeout getIdleBeforeUnregistration() {
    return idleBeforeUnregistration;
  }

  @Override
  public Timeout getIdleAfterSuccessfullCall() {
    return idleAfterSuccessfullCall;
  }

  @Override
  public Timeout getIdleAfterZeroCall() {
    return idleAfterZeroCall;
  }

  @Override
  public int getSelfDialFactor() {
    return selfDialFactor;
  }

  @Override
  public int getSelfDialChance() {
    return selfDialChance;
  }

  @Override
  public Timeout getIdleBeforeSelfCall() {
    return idleBeforeSelfCall;
  }

  @Override
  public Timeout getIdleAfterSelfCallTimeout() {
    return idleAfterSelfCallTimeout;
  }

  @Override
  public ScriptInstance getActivityPeriodScript() {
    return activityPeriodScript;
  }

  @Override
  public ScriptInstance[] getCallFilterScripts() {
    return callFilterScripts;
  }

  @Override
  public ScriptInstance[] getSmsFilterScripts() {
    return smsFilterScripts;
  }

  @Override
  public ScriptInstance[] getBusinessActivityScripts() {
    return businessActivityScripts;
  }

  @Override
  public boolean canBeAppointed() {
    return canBeAppointed;
  }

  @Override
  public SIMGroupImpl setIdleAfterRegistration(final Timeout idleAfterRegistration) {
    this.idleAfterRegistration = idleAfterRegistration;
    return this;
  }

  @Override
  public SIMGroupImpl setIdleBeforeUnregistration(final Timeout idleBeforeUnregistration) {
    this.idleBeforeUnregistration = idleBeforeUnregistration;
    return this;
  }

  @Override
  public SIMGroupImpl setIdleAfterSuccessfullCall(final Timeout idleBetweenCalls) {
    this.idleAfterSuccessfullCall = idleBetweenCalls;
    return this;
  }

  @Override
  public SIMGroupImpl setIdleAfterZeroCall(final Timeout idle) {
    this.idleAfterZeroCall = idle;
    return this;
  }

  @Override
  public SIMGroupImpl setSelfDialFactor(final int selfDialFactor) {
    this.selfDialFactor = selfDialFactor;
    return this;
  }

  @Override
  public SIMGroupImpl setSelfDialChance(final int selfDialChance) {
    this.selfDialChance = selfDialChance;
    return this;
  }

  @Override
  public SIMGroupImpl setIdleBeforeSelfCall(final Timeout idleBeforeSelfCall) {
    this.idleBeforeSelfCall = idleBeforeSelfCall;
    return this;
  }

  @Override
  public SIMGroupImpl setIdleAfterSelfCallTimeout(final Timeout idleAfterSelfCallTimeout) {
    this.idleAfterSelfCallTimeout = idleAfterSelfCallTimeout;
    return this;
  }

  @Override
  public SIMGroupImpl setActivityPeriodScript(final ScriptInstance activityPeriodScript) {
    assertScriptType(activityPeriodScript, ScriptType.ACTIVITY_PERIOD);
    this.activityPeriodScript = activityPeriodScript;
    return this;
  }

  @Override
  public SIMGroupImpl setBusinessActivityScripts(final ScriptInstance[] businessActivityScripts) {
    if (businessActivityScripts != null) {
      assertScriptsType(businessActivityScripts, ScriptType.BUSINESS_ACTIVITY);
    }
    this.businessActivityScripts = businessActivityScripts;
    return this;
  }

  @Override
  public SIMGroupImpl setFactorScript(final ScriptInstance factorScript) {
    assertScriptType(factorScript, ScriptType.FACTOR);
    this.factorScript = factorScript;
    return this;
  }

  @Override
  public SIMGroupImpl setGWSelectorScript(final ScriptInstance gwSelectorScript) {
    assertScriptType(gwSelectorScript, ScriptType.GATEWAY_SELECTOR);
    this.gwSelectorScript = gwSelectorScript;
    return this;
  }

  @Override
  public SIMGroupImpl setSessionScript(final ScriptInstance sessionScript) {
    assertScriptType(sessionScript, ScriptType.ACTIVITY_PERIOD);
    this.sessionScript = sessionScript;
    return this;
  }

  @Override
  public ScriptInstance getSessionScript() {
    return sessionScript;
  }

  @Override
  public ScriptInstance getGWSelectorScript() {
    return gwSelectorScript;
  }

  @Override
  public ScriptInstance getFactorScript() {
    return factorScript;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  @Override
  public SIMGroupImpl setDescription(final String description) {
    this.description = description;
    return this;
  }

  @Override
  public SIMGroup setCanBeAppointed(final boolean canBeAppointed) {
    this.canBeAppointed = canBeAppointed;
    return this;
  }

  @Override
  public SIMGroupImpl clone() {
    try {
      return (SIMGroupImpl) super.clone();
    } catch (CloneNotSupportedException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public ScriptInstance[] getActionProviderScripts() {
    return actionProviderScripts;
  }

  @Override
  public SIMGroupImpl setActionProviderScripts(final ScriptInstance[] actionProviderScripts) {
    if (actionProviderScripts != null) {
      assertScriptsType(actionProviderScripts, ScriptType.ACTION_PROVIDER);
    }
    this.actionProviderScripts = actionProviderScripts;
    return this;
  }

  @Override
  public ScriptInstance getIncomingCallManagementScript() {
    return incomingCallManagementScript;
  }

  @Override
  public SIMGroupImpl setIncomingCallManagementScript(final ScriptInstance incomingCallManagementScript) {
    assertScriptType(incomingCallManagementScript, ScriptType.INCOMING_CALL_MANAGEMENT);
    this.incomingCallManagementScript = incomingCallManagementScript;
    return this;
  }

  @Override
  public ScriptInstance getVSFactorScript() {
    return vsFactorScript;
  }

  @Override
  public SIMGroupImpl setVSFactorScript(final ScriptInstance vsFactorScript) {
    assertScriptType(vsFactorScript, ScriptType.FACTOR);
    this.vsFactorScript = vsFactorScript;
    return this;
  }

  @Override
  public SIMGroupImpl setCallFilterScripts(final ScriptInstance[] callFilterScripts) {
    if (callFilterScripts != null) {
      assertScriptsType(callFilterScripts, ScriptType.CALL_FILTER);
    }
    this.callFilterScripts = callFilterScripts;
    return this;
  }

  @Override
  public SIMGroupImpl setSmsFilterScripts(final ScriptInstance[] smsFilterScripts) {
    if (smsFilterScripts != null) {
      assertScriptsType(smsFilterScripts, ScriptType.SMS_FILTER);
    }
    this.smsFilterScripts = smsFilterScripts;
    return this;
  }

  @Override
  public ScriptInstance getIMEIGeneratorScript() {
    return imeiGeneratorScript;
  }

  @Override
  public SIMGroupImpl setIMEIGeneratorScript(final ScriptInstance imeiGeneratorScript) {
    assertScriptType(imeiGeneratorScript, ScriptType.IMEI_GENERATOR);
    this.imeiGeneratorScript = imeiGeneratorScript;
    return this;
  }

  private void assertScriptType(final ScriptInstance scriptInstance, final ScriptType type) {
    if (scriptInstance == null) {
      return;
    }
    if (scriptInstance.getDefinition().getType() != type) {
      throw new IllegalArgumentException("Waiting " + type + ". But got " + scriptInstance.getDefinition().getType());
    }
  }

  private void assertScriptsType(final ScriptInstance[] scripts, final ScriptType type) {
    for (ScriptInstance s : scripts) {
      assertScriptType(s, type);
    }
  }

  @Override
  public boolean isSyntheticRinging() {
    return syntheticRingingEnabled;
  }

  @Override
  public SIMGroup setSyntheticRinging(final boolean enabled) {
    syntheticRingingEnabled = enabled;
    return this;
  }

  public void setActionProviderScript(final ScriptInstance scriptInstance, final int scriptIndex) {
    setScriptByIndex(actionProviderScripts, scriptInstance, scriptIndex);
  }

  public void setBusinessActivityScript(final ScriptInstance scriptInstance, final int scriptIndex) {
    setScriptByIndex(businessActivityScripts, scriptInstance, scriptIndex);
  }

  private void setScriptByIndex(final ScriptInstance[] scripts, final ScriptInstance script, final int scriptIndex) {
    if (scripts == null || scripts.length == 0) {
      throw new IndexOutOfBoundsException("Can't set scriptInstance:[" + script + "] to scriptInstanceArray by index [" + scriptIndex + "] (array is empty)");
    }
    if (scriptIndex > scripts.length - 1) {
      throw new IndexOutOfBoundsException(
          "Can't set scriptInstance:[" + script + "] to scriptInstanceArray by index [" + scriptIndex + "] (array length is less then index, array length = " + scripts.length + ")");
    }
    scripts[scriptIndex] = script;
  }

  @Override
  public ScriptCommons[] listScriptCommons() {
    return scrptCommons;
  }

  @Override
  public SIMGroup setScriptCommons(final ScriptCommons[] scriptCommons) {
    this.scrptCommons = scriptCommons;
    return this;
  }

  @Override
  public boolean isFASDetection() {
    return fasDetection;
  }

  @Override
  public SIMGroup setSmsDeliveryReport(final boolean smsDeliveryReport) {
    this.smsDeliveryReport = smsDeliveryReport;
    return this;
  }

  @Override
  public boolean isSmsDeliveryReport() {
    return smsDeliveryReport;
  }

  @Override
  public SIMGroup setFASDetection(final boolean fasDetection) {
    this.fasDetection = fasDetection;
    return this;
  }

  @Override
  public boolean isSuppressIncomingCallSignal() {
    return suppressIncomingCallSignal;
  }

  @Override
  public SIMGroup setSuppressIncomingCallSignal(final boolean extendedAT) {
    this.suppressIncomingCallSignal = extendedAT;
    return this;
  }

  @Override
  public int getRegistrationPeriod() {
    return registrationPeriod;
  }

  @Override
  public SIMGroup setRegistrationPeriod(final int registrationPeriod) {
    this.registrationPeriod = registrationPeriod;
    return this;
  }

  @Override
  public int getRegistrationCount() {
    return registrationCount;
  }

  @Override
  public SIMGroup setRegistrationCount(final int registrationCount) {
    this.registrationCount = registrationCount;
    return this;
  }

}
