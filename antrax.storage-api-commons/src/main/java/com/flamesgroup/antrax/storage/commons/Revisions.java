/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Revisions {

  private static final Revisions INSTANCE = new Revisions();

  private final AtomicLong gsmToSimGroupLinksRevision = new AtomicLong(1);
  private final AtomicLong serversRevision = new AtomicLong(1);

  private final AtomicLong gsmGroupsRevision = new AtomicLong(1);

  private final AtomicLong scriptFilesRevision = new AtomicLong(1);

  private final ConcurrentHashMap<Long, Long> simGroupsRevision = new ConcurrentHashMap<>();

  private Revisions() {
  }

  public static Revisions getInstance() {
    return INSTANCE;
  }

  public long getGsmToSimGroupLinksRevision() {
    return gsmToSimGroupLinksRevision.get();
  }

  public long incrementAndGetGsmToSimGroupLinksRevision() {
    return gsmToSimGroupLinksRevision.incrementAndGet();
  }

  public long getServersRevision() {
    return serversRevision.get();
  }

  public long incrementAndGetServersRevision() {
    return serversRevision.incrementAndGet();
  }

  public long getScriptFilesRevision() {
    return scriptFilesRevision.get();
  }

  public long incrementAndGetScriptFilesRevision() {
    return scriptFilesRevision.incrementAndGet();
  }

  public long getGsmGroupsRevision() {
    return gsmGroupsRevision.get();
  }

  public long incrementAndGetGsmGroupsRevision() {
    return gsmGroupsRevision.incrementAndGet();
  }

  public long getSimGroupRevision(final SIMGroup simGroup) {
    return simGroupsRevision.getOrDefault(simGroup.getID(), 0L);
  }

  public long getLastSimGroupRevision() {
    return simGroupsRevision.entrySet().stream().mapToLong(Map.Entry::getValue).sum();
  }

  public void putSimGroupRevision(final SIMGroup simGroup) {
    simGroupsRevision.put(simGroup.getID(), simGroup.getRevision());
  }

  public long incrementAndGetSimGroupRevision(final SIMGroup simGroup) {
    return simGroupsRevision.computeIfPresent(simGroup.getID(), (k, v) -> v + 1);
  }

  public void removeSimGroupRevision(final SIMGroup simGroup) {
    simGroupsRevision.remove(simGroup.getID());
  }

}
