/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.util.Date;
import java.util.UUID;

public class AlarisSmppSms extends AlarisSms {

  private static final long serialVersionUID = 3178236494220353550L;

  private final Integer sourceTon;
  private final Integer sourceNpi;
  private final Integer destTon;
  private final Integer destNpi;

  private final Integer registeredDelivery;

  public AlarisSmppSms(final UUID id, final Integer sourceTon, final Integer sourceNpi, final String sourceNumber, final Integer destTon, final Integer destNpi, final String destNumber,
      final String message, final String serviceType, final Integer dataCoding, final Date createTime, final Sar sar, final String username, final Integer registeredDelivery) {
    super(id, sourceNumber, destNumber, message, serviceType, dataCoding, createTime, username, sar);
    this.sourceTon = sourceTon;
    this.sourceNpi = sourceNpi;
    this.destTon = destTon;
    this.destNpi = destNpi;
    this.registeredDelivery = registeredDelivery;
    if (sar != null) {
      this.multiPartSmsKey = AlarisSmppSms.class.getSimpleName() + sourceNumber + destNumber + sar.getId() + sar.getParts() + username;
    }
  }

  public Integer getSourceTon() {
    return sourceTon;
  }

  public Integer getSourceNpi() {
    return sourceNpi;
  }

  public Integer getDestTon() {
    return destTon;
  }

  public Integer getDestNpi() {
    return destNpi;
  }

  public Integer getRegisteredDelivery() {
    return registeredDelivery;
  }

  @Override
  public String toString() {
    return AlarisSmppSms.class.getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[id:" + getId() +
        " source:" + getSourceNumber() +
        " dest:" + getDestNumber() +
        " message:" + getMessage() +
        " status:" + getStatus() +
        " sar:" + getSar() +
        " smsId:" + getSmsId() +
        " username:" + getUsername() +
        ']';
  }

}
