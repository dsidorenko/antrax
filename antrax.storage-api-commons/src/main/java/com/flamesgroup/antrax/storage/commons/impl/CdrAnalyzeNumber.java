/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;
import java.util.Objects;

public class CdrAnalyzeNumber implements Serializable {

  private static final long serialVersionUID = -1874572695020354712L;

  private PhoneNumber number;
  private int total;
  private int success;
  private long duration;

  public PhoneNumber getNumber() {
    return number;
  }

  public CdrAnalyzeNumber setNumber(final PhoneNumber number) {
    this.number = number;
    return this;
  }

  public int getTotal() {
    return total;
  }

  public CdrAnalyzeNumber setTotal(final int total) {
    this.total = total;
    return this;
  }

  public int getSuccess() {
    return success;
  }

  public CdrAnalyzeNumber setSuccess(final int success) {
    this.success = success;
    return this;
  }

  public long getDuration() {
    return duration;
  }

  public CdrAnalyzeNumber setDuration(final long duration) {
    this.duration = duration;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CdrAnalyzeNumber)) {
      return false;
    }
    final CdrAnalyzeNumber that = (CdrAnalyzeNumber) object;

    return total == that.total
        && success == that.success
        && duration == that.duration
        && Objects.equals(number, that.number);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(number);
    result = prime * result + total;
    result = prime * result + success;
    result = prime * result + Long.hashCode(duration);
    return result;
  }

}
