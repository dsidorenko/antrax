/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import com.flamesgroup.commons.ChannelUID;

import java.io.Serializable;

public interface GSMChannel extends Serializable {

  ChannelUID getChannelUID();

  GSMGroup getGSMGroup();

  GSMChannel setChannelUID(ChannelUID uid);

  GSMChannel setGsmGroup(GSMGroup gsmGroup);

  boolean isLock();

  GSMChannel setLock(boolean lock);

  long getLockTime();

  GSMChannel setLockTime(long lockTime);

  String getLockReason();

  GSMChannel setLockReason(String lockReason);

  Integer getLockArfcn();

  GSMChannel setLockArfcn(Integer lockArfcn);

}
