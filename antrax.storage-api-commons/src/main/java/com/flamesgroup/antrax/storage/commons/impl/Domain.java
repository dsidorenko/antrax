/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.Serializable;

public class Domain implements Serializable {

  private static final long serialVersionUID = 2254940252614714073L;

  protected long id;

  public long getID() {
    return id;
  }

  @Override
  public int hashCode() {
    return (int) id;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj instanceof Domain) {
      Domain that = (Domain) obj;
      return that.id == id;
    }
    return false;
  }

  protected boolean equals(final Object a, final Object b) {
    if (a == null)
      return b == null;
    if (b == null)
      return false;
    return a.equals(b);
  }

  protected int hashCode(final Object o) {
    if (o != null) {
      return o.hashCode();
    }
    return 0;
  }

  public void setID(final long id) {
    this.id = id;
  }

}
