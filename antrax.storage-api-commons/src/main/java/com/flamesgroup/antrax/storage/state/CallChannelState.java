/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.state;

import com.flamesgroup.antrax.commons.TimePeriodWriter;

import java.io.Serializable;

public class CallChannelState implements Serializable {

  private static final long serialVersionUID = -6475367505439319488L;

  public enum State {
    READY_TO_CALL(false),
    OUTGOING_CALL(false),
    INCOMING_CALL(false),
    INCOMING_SCRIPT_CALL(false),
    IDLE_AFTER_REGISTRATION(true),
    IDLE_BEFORE_UNREG(true),
    IDLE_BETWEEN_CALLS(true),
    IDLE_BEFORE_SELF_CALL(true),
    IDLE_AFTER_SELF_CALL(true),
    RELEASING(false),
    WAITING_SELF_CALL(false),
    REGISTERING_IN_GSM(false),
    STARTED_BUSINESS_ACTIVITY(false),
    NETWORK_SURVEY(false),
    SETTING_IMEI(false),
    TURNING_ON_MODULE(false),
    CELL_EQUALIZER(false);

    final boolean predictable;

    State(final boolean predictable) {
      this.predictable = predictable;
    }
  }

  private final long param;
  private final State state;

  public CallChannelState(final State state, final long param) {
    if (state.predictable) {
      this.param = param + System.currentTimeMillis();
    } else {
      this.param = System.currentTimeMillis();
    }
    this.state = state;
  }

  public State getState() {
    return state;
  }

  public Object writeTime(final TimePeriodWriter writer, final long currentTime) {
    if (state.predictable) {
      return writer.writeLeftTime(param - currentTime);
    } else {
      return writer.writePassedTime(currentTime - param);
    }
  }

}
