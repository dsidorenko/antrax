/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

public class SimSearchingParams implements Serializable {

  private static final long serialVersionUID = 1904857267940877860L;

  private PhoneNumber phoneNumber;
  private Boolean enabled;
  private Boolean locked;
  private String simGroupName;

  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

  public SimSearchingParams setPhoneNumber(final PhoneNumber phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public SimSearchingParams setEnabled(final Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public Boolean getLocked() {
    return locked;
  }

  public SimSearchingParams setLocked(final Boolean active) {
    this.locked = active;
    return this;
  }

  public String getSimGroupName() {
    return simGroupName;
  }

  public SimSearchingParams setSimGroupName(final String simGroupName) {
    this.simGroupName = simGroupName;
    return this;
  }

  @Override
  public String toString() {
    return "[ phone:" + phoneNumber + "; enabled:" + enabled + "locked:" + locked + "; simGroupName:" + simGroupName + " ]";
  }

}
