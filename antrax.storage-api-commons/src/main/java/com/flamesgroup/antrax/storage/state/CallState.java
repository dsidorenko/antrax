/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.state;

import com.flamesgroup.antrax.commons.TimePeriodWriter;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

import java.io.Serializable;

public class CallState implements Serializable {

  private static final long serialVersionUID = 6104266363804556310L;

  public enum State {
    IDLE,
    DIALING,
    ALERTING,
    ACTIVE
  }

  private volatile State state;
  private volatile long startTime;
  private final TimeProvider time = new ServerSyncTimeProvider();
  private volatile PhoneNumber phoneNumber;

  public CallState(final State state) {
    this.state = state;
    this.startTime = time.currentTimeMillis();
  }

  public State getState() {
    return state;
  }

  public Object writeTime(final TimePeriodWriter writer) {
    return writer.writePassedTime(time.currentTimeMillis() - startTime);
  }

  /**
   * @return phoneNumber or null if state is IDLE
   */
  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

  public void changeState(final State state) {
    this.state = state;
    this.startTime = time.currentTimeMillis();
  }

  public void setPhoneNumber(final PhoneNumber phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

}
