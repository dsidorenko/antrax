/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public abstract class AlarisSms implements Serializable {

  private static final long serialVersionUID = 4805962775936273210L;

  private final UUID id;
  private final String sourceNumber;
  private final String destNumber;
  private final String message;
  private final String serviceType;
  private final Integer dataCoding;
  private final Date createTime;
  private final String username;
  private final Sar sar;

  private Date lastUpdateTime;
  private Status status;
  private UUID smsId;
  private Date dlrTime;

  protected String multiPartSmsKey;

  public AlarisSms(final UUID id, final String sourceNumber, final String destNumber, final String message, final String serviceType, final Integer dataCoding, final Date createTime,
      final String username, final Sar sar) {
    this.id = id;
    this.sourceNumber = sourceNumber;
    this.destNumber = destNumber;
    this.message = message;
    this.serviceType = serviceType;
    this.dataCoding = dataCoding;
    this.createTime = createTime;
    this.username = username;
    this.sar = sar;
  }

  public AlarisSms setLastUpdateTime(final Date lastUpdateTime) {
    this.lastUpdateTime = lastUpdateTime;
    return this;
  }

  public AlarisSms setStatus(final Status status) {
    this.status = status;
    return this;
  }

  public UUID getId() {
    return id;
  }

  public String getSourceNumber() {
    return sourceNumber;
  }

  public String getDestNumber() {
    return destNumber;
  }

  public String getMessage() {
    return message;
  }

  public String getServiceType() {
    return serviceType;
  }

  public Integer getDataCoding() {
    return dataCoding;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public Date getLastUpdateTime() {
    return lastUpdateTime;
  }

  public Status getStatus() {
    return status;
  }

  public String getUsername() {
    return username;
  }

  public Sar getSar() {
    return sar;
  }

  public UUID getSmsId() {
    return smsId;
  }

  public AlarisSms setSmsId(final UUID smsId) {
    this.smsId = smsId;
    return this;
  }

  public String getMultiPartSmsKey() {
    return multiPartSmsKey;
  }

  public Date getDlrTime() {
    return dlrTime;
  }

  public AlarisSms setDlrTime(final Date dlrTime) {
    this.dlrTime = dlrTime;
    return this;
  }

  public enum Status {
    ENROUTE, SENT, DELIVRD, EXPIRED, UNDELIV, ACCEPTD, UNKNOWN, REJECTD, DELETED
  }

  public static class Sar implements Serializable {

    private static final long serialVersionUID = -1845643899765464598L;

    private final String id;
    private final int parts;
    private final int partNumber;

    public Sar(final String id, final int parts, final int partNumber) {
      this.id = id;
      this.parts = parts;
      this.partNumber = partNumber;
    }

    public String getId() {
      return id;
    }

    public int getParts() {
      return parts;
    }

    public int getPartNumber() {
      return partNumber;
    }

    @Override
    public String toString() {
      return Sar.class.getSimpleName() + '@' + Integer.toHexString(hashCode()) +
          "[id:" + id +
          " parts:" + parts +
          " partNumber:" + partNumber +
          ']';
    }

  }

}
