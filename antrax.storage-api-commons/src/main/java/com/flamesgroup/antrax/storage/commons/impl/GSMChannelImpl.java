/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.commons.ChannelUID;

public class GSMChannelImpl extends Domain implements GSMChannel {

  private static final long serialVersionUID = 6033034533384008902L;

  private ChannelUID uid;
  private GSMGroup gsmGroup;
  private boolean lock;
  private long lockTime;
  private String lockReason;
  private Integer lockArfcn;

  @Override
  public ChannelUID getChannelUID() {
    return uid;
  }

  @Override
  public GSMGroup getGSMGroup() {
    return gsmGroup;
  }

  @Override
  public long getID() {
    return id;
  }

  @Override
  public GSMChannel setChannelUID(final ChannelUID uid) {
    this.uid = uid;
    return this;
  }

  @Override
  public GSMChannel setGsmGroup(final GSMGroup gsmGroup) {
    this.gsmGroup = gsmGroup;
    return this;
  }

  @Override
  public boolean isLock() {
    return lock;
  }

  @Override
  public GSMChannel setLock(final boolean lock) {
    this.lock = lock;
    return this;
  }

  @Override
  public long getLockTime() {
    return lockTime;
  }

  @Override
  public GSMChannel setLockTime(final long lockTime) {
    this.lockTime = lockTime;
    return this;
  }

  @Override
  public String getLockReason() {
    return lockReason;
  }

  @Override
  public GSMChannel setLockReason(final String lockReason) {
    this.lockReason = lockReason;
    return this;
  }

  @Override
  public Integer getLockArfcn() {
    return lockArfcn;
  }

  @Override
  public GSMChannel setLockArfcn(final Integer lockArfcn) {
    this.lockArfcn = lockArfcn;
    return this;
  }

  @Override
  public String toString() {
    String groupName = gsmGroup == null ? null : gsmGroup.getName();
    return uid + " in Group: " + groupName;
  }

}
