/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.enums;

public enum CellStatus {

  CELL_SUITABLE(0),
  CELL_LOW_PRIORITY(1),
  CELL_FORBIDDEN(2),
  CELL_BARRED(3),
  CELL_LOW_LEVEL(4),
  CELL_OTHER(5);

  private final int value;

  CellStatus(final int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public static CellStatus getStatusByValue(final int value) {
    for (CellStatus status : CellStatus.values()) {
      if (status.getValue() == value) {
        return status;
      }
    }
    throw new IllegalArgumentException("Unsupported value: " + value);
  }

}
