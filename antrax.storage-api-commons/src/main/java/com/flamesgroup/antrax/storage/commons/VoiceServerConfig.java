/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import com.flamesgroup.commons.CallRouteConfig;
import com.flamesgroup.commons.SmsRouteConfig;

import java.io.Serializable;

public final class VoiceServerConfig implements Serializable {

  private static final long serialVersionUID = -6319585444100564680L;

  private final CallRouteConfig callRouteConfig;
  private final SmsRouteConfig smsRouteConfig;
  private final boolean audioCaptureEnabled;

  public VoiceServerConfig(final CallRouteConfig callRouteConfig, final SmsRouteConfig smsRouteConfig, final boolean audioCaptureEnabled) {
    this.callRouteConfig = callRouteConfig;
    this.smsRouteConfig = smsRouteConfig;
    this.audioCaptureEnabled = audioCaptureEnabled;
  }

  public CallRouteConfig getCallRouteConfig() {
    return callRouteConfig;
  }

  public SmsRouteConfig getSmsRouteConfig() {
    return smsRouteConfig;
  }

  public boolean isAudioCaptureEnabled() {
    return audioCaptureEnabled;
  }

}
