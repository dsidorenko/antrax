/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.util.Date;

public class WhiteListNumber extends ListNumber {

  private static final long serialVersionUID = -7838157467252038989L;

  private Date addTime;

  public WhiteListNumber() {
  }

  public WhiteListNumber(final VoipAntiSpamNumberType numberType, final String number, final VoipAntiSpamListNumbersStatus status, final String statusDescription, final int routingRequestCount,
      final Date addTime) {
    super(numberType, number, status, statusDescription, routingRequestCount);
    this.addTime = addTime;
  }

  public Date getAddTime() {
    return addTime;
  }

  public void setAddTime(final Date addTime) {
    this.addTime = addTime;
  }

  @Override
  protected void appendToStringProperty(final StringBuilder sb) {
    super.appendToStringProperty(sb);
    sb.append(" at:").append(addTime);
  }

}
