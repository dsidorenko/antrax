/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.enums.CellStatus;
import com.flamesgroup.commons.ChannelUID;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public final class NetworkSurvey extends Domain {

  private static final long serialVersionUID = 621981766133660957L;

  private Integer arfcn;
  private Integer bsic;
  private Integer rxLev;
  private Float ber;
  private Short mcc;
  private Short mnc;
  private Integer lac;
  private Integer cellId;
  private CellStatus cellStatus;
  private List<Integer> arfcns;
  private List<Integer> neighboursArfcn;
  private Date addTime;
  private ChannelUID gsmChannel;
  private Long serverId;
  private Boolean trusted;

  public NetworkSurvey() {
  }

  public NetworkSurvey(final Integer id) {
    this.id = id;
  }

  public Integer getArfcn() {
    return arfcn;
  }

  public NetworkSurvey setArfcn(final Integer arfcn) {
    this.arfcn = arfcn;
    return this;
  }

  public Integer getBsic() {
    return bsic;
  }

  public NetworkSurvey setBsic(final Integer bsic) {
    this.bsic = bsic;
    return this;
  }

  public Integer getRxLev() {
    return rxLev;
  }

  public NetworkSurvey setRxLev(final Integer rxLev) {
    this.rxLev = rxLev;
    return this;
  }

  public Float getBer() {
    return ber;
  }

  public NetworkSurvey setBer(final Float ber) {
    this.ber = ber;
    return this;
  }

  public Short getMcc() {
    return mcc;
  }

  public NetworkSurvey setMcc(final Short mcc) {
    this.mcc = mcc;
    return this;
  }

  public Short getMnc() {
    return mnc;
  }

  public NetworkSurvey setMnc(final Short mnc) {
    this.mnc = mnc;
    return this;
  }

  public Integer getLac() {
    return lac;
  }

  public NetworkSurvey setLac(final Integer lac) {
    this.lac = lac;
    return this;
  }

  public Integer getCellId() {
    return cellId;
  }

  public NetworkSurvey setCellId(final Integer cellId) {
    this.cellId = cellId;
    return this;
  }

  public CellStatus getCellStatus() {
    return cellStatus;
  }

  public NetworkSurvey setCellStatus(final CellStatus cellStatus) {
    this.cellStatus = cellStatus;
    return this;
  }

  public List<Integer> getArfcns() {
    return arfcns;
  }

  public NetworkSurvey setArfcns(final List<Integer> arfcns) {
    this.arfcns = arfcns;
    return this;
  }

  public List<Integer> getNeighboursArfcn() {
    return neighboursArfcn;
  }

  public NetworkSurvey setNeighboursArfcn(final List<Integer> neighboursArfcn) {
    this.neighboursArfcn = neighboursArfcn;
    return this;
  }

  public Date getAddTime() {
    return addTime;
  }

  public NetworkSurvey setAddTime(final Date addTime) {
    this.addTime = addTime;
    return this;
  }

  public ChannelUID getGsmChannel() {
    return gsmChannel;
  }

  public NetworkSurvey setGsmChannel(final ChannelUID gsmChannel) {
    this.gsmChannel = gsmChannel;
    return this;
  }

  public Long getServerId() {
    return serverId;
  }

  public NetworkSurvey setServerId(final Long serverId) {
    this.serverId = serverId;
    return this;
  }

  public Boolean isTrusted() {
    return trusted;
  }

  public NetworkSurvey setTrusted(final Boolean trusted) {
    this.trusted = trusted;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof NetworkSurvey)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final NetworkSurvey that = (NetworkSurvey) object;

    return Objects.equals(arfcn, that.arfcn)
        && Objects.equals(bsic, that.bsic)
        && Objects.equals(rxLev, that.rxLev)
        && Objects.equals(ber, that.ber)
        && Objects.equals(mcc, that.mcc)
        && Objects.equals(mnc, that.mnc)
        && Objects.equals(lac, that.lac)
        && Objects.equals(cellId, that.cellId)
        && cellStatus == that.cellStatus
        && Objects.equals(arfcns, that.arfcns)
        && Objects.equals(neighboursArfcn, that.neighboursArfcn)
        && Objects.equals(addTime, that.addTime)
        && Objects.equals(gsmChannel, that.gsmChannel)
        && Objects.equals(serverId, that.serverId)
        && Objects.equals(trusted, that.trusted);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(arfcn);
    result = prime * result + Objects.hashCode(bsic);
    result = prime * result + Objects.hashCode(rxLev);
    result = prime * result + Objects.hashCode(ber);
    result = prime * result + Objects.hashCode(mcc);
    result = prime * result + Objects.hashCode(mnc);
    result = prime * result + Objects.hashCode(lac);
    result = prime * result + Objects.hashCode(cellId);
    result = prime * result + Objects.hashCode(cellStatus);
    result = prime * result + Objects.hashCode(arfcns);
    result = prime * result + Objects.hashCode(neighboursArfcn);
    result = prime * result + Objects.hashCode(addTime);
    result = prime * result + Objects.hashCode(gsmChannel);
    result = prime * result + Objects.hashCode(serverId);
    result = prime * result + Objects.hashCode(trusted);
    return result;
  }

}
