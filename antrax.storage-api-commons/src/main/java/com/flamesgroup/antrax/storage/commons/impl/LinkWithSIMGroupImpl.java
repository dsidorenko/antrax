/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

public class LinkWithSIMGroupImpl extends Domain implements LinkWithSIMGroup {

  private static final long serialVersionUID = 4937333855070958079L;

  private SIMGroup simGroup;
  private GSMGroup gsmGroup;

  public LinkWithSIMGroupImpl(final long id, final SIMGroup simGroup, final GSMGroup gsmGroup) {
    this.id = id;
    this.simGroup = simGroup;
    this.gsmGroup = gsmGroup;
  }

  public LinkWithSIMGroupImpl() {
  }

  @Override
  public GSMGroup getGSMGroup() {
    return gsmGroup;
  }

  @Override
  public SIMGroup getSIMGroup() {
    return simGroup;
  }

  @Override
  public LinkWithSIMGroup setGSMGroup(final GSMGroup group) {
    this.gsmGroup = group;
    return this;

  }

  @Override
  public LinkWithSIMGroup setSIMGroup(final SIMGroup group) {
    this.simGroup = group;
    return this;
  }

  @Override
  public String toString() {
    return simGroup + "linked with " + gsmGroup;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }

}
