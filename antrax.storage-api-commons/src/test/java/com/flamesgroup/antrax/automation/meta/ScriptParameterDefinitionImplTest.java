/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import org.junit.Test;

public class ScriptParameterDefinitionImplTest {

  @Test
  public void testCheckAssign() throws Exception {
    ScriptParameterDefinition param0 = new ScriptParameterDefinitionImpl("prop0", "setProp0", "", "int", false, null);
    ScriptDefinitionImpl def = new ScriptDefinitionImpl(new JavaFileName("test"), "test", "", ScriptType.FACTOR, param0);
    ScriptInstance inst = def.createInstance();

    ClassLoader classLoader = ScriptParameterDefinitionImplTest.class.getClassLoader();
    inst.setParameter("prop0", 5, classLoader);

    inst.setParameter("prop0", new SharedReference("ref", "int"), classLoader);
    inst.setParameter("prop0", new SharedReference("ref", "java.lang.Integer"), classLoader);
  }

  @Test
  public void testCheckAssign2() throws Exception {
    ScriptParameterDefinition param0 = new ScriptParameterDefinitionImpl("prop0", "setProp0", "", "java.lang.Integer", false, null);
    ScriptDefinitionImpl def = new ScriptDefinitionImpl(new JavaFileName("test"), "test", "", ScriptType.FACTOR, param0);
    ScriptInstance inst = def.createInstance();

    ClassLoader classLoader = ScriptParameterDefinitionImplTest.class.getClassLoader();
    inst.setParameter("prop0", 5, classLoader);

    inst.setParameter("prop0", new SharedReference("ref", "int"), classLoader);
    inst.setParameter("prop0", new SharedReference("ref", "java.lang.Integer"), classLoader);
  }

}
