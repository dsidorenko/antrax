/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ScriptInstanceImplTest {

  @Test
  public void setParameter() {
    ScriptParameterDefinitionImpl prop1 = new ScriptParameterDefinitionImpl("prop1", "setProp1", "", "java.lang.String", false, null);
    ScriptParameterDefinitionImpl prop2 = new ScriptParameterDefinitionImpl("prop2", "setProp2", "", "java.lang.String", true, null);
    ScriptParameterDefinitionImpl prop3 = new ScriptParameterDefinitionImpl("prop3", "setProp3", "", "java.lang.String", false, null);

    ScriptInstance inst = new ScriptDefinitionImpl(new JavaFileName("hello"), "name", "desc", ScriptType.GATEWAY_SELECTOR, prop1, prop2, prop3).createInstance();
    ClassLoader classLoader = ScriptInstanceImplTest.class.getClassLoader();

    inst.setParameter("prop1", "prop1Value", classLoader);
    inst.addParameter("prop2", "prop2[0]Value", classLoader);
    inst.addParameter("prop2", "prop2[1]Value", classLoader);
    inst.setParameter("prop3", "prop3Value", classLoader);

    assertEquals("prop1Value", inst.getParameters()[0].getValue(classLoader));
    assertEquals("prop2[0]Value", inst.getParameters()[1].getValue(classLoader));
    assertEquals("prop3Value", inst.getParameters()[2].getValue(classLoader));
    assertEquals("prop2[1]Value", inst.getParameters()[3].getValue(classLoader));
  }

  @Test
  public void setParameterWithBoxing() {
    ScriptParameterDefinitionImpl prop1 = new ScriptParameterDefinitionImpl("prop1", "setProp1", "", "int", false, null);

    ScriptInstance inst = new ScriptDefinitionImpl(new JavaFileName("hello"), "name", "", ScriptType.GATEWAY_SELECTOR, prop1).createInstance();
    ClassLoader classLoader = ScriptInstanceImplTest.class.getClassLoader();
    inst.setParameter("prop1", 2, classLoader);

    assertEquals(2, inst.getParameters()[0].getValue(classLoader));
  }

  @Test
  public void copyParametersFromOtherInstance() {
    ScriptParameterDefinitionImpl primitiveProp = new ScriptParameterDefinitionImpl("intProp", "setIntProp", "", "int", false, null);
    ScriptParameterDefinitionImpl arrayProp = new ScriptParameterDefinitionImpl("arrayProp", "setArrayProp", "", "int", true, null);

    ScriptInstance srcInst = new ScriptDefinitionImpl(new JavaFileName("hello"), "SrcInst", "", ScriptType.GATEWAY_SELECTOR, primitiveProp, arrayProp).createInstance();
    ClassLoader classLoader = ScriptInstanceImplTest.class.getClassLoader();

    srcInst.setParameter("intProp", 2, classLoader);
    srcInst.addParameter("arrayProp", 1, classLoader);
    srcInst.addParameter("arrayProp", 2, classLoader);

    ScriptInstance dstInst = new ScriptDefinitionImpl(new JavaFileName("hello"), "DstInst", "", ScriptType.GATEWAY_SELECTOR, primitiveProp, arrayProp).createInstance();
    dstInst.setParameters(srcInst);

    assertEquals(srcInst.getParameter("intProp").getValue(classLoader), dstInst.getParameter("intProp").getValue(classLoader));
    assertEquals(srcInst.getParameter("arrayProp").getValue(classLoader), dstInst.getParameter("arrayProp").getValue(classLoader));
  }

}
