ALTER TABLE alaris_sms RENAME TO alaris_http_sms;

CREATE TABLE alaris_smpp_sms
(
  id uuid NOT NULL,
  source_ton integer NOT NULL,
  source_npi integer NOT NULL,
  source_addr text NOT NULL,
  dest_ton integer NOT NULL,
  dest_npi integer NOT NULL,
  dest_addr text NOT NULL,
  message text NOT NULL,
  service_type text,
  data_coding integer,
  status text NOT NULL,
  create_time timestamp with time zone NOT NULL DEFAULT now(),
  last_update_time timestamp with time zone NOT NULL DEFAULT now(),
  sar_id text,
  sar_parts integer,
  sar_part_number integer,
  sms_id uuid,
  username text,
  registered_delivery int,
  dlr_time timestamp with time zone,
  CONSTRAINT alaris_smpp_sms_pkey PRIMARY KEY (id)
);

