--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

-- Started on 2017-02-08 16:46:41 EET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13223)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 245 (class 1255 OID 860546)
-- Name: delete_by_id(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION delete_by_id(in_table_name text, in_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE
    sql_str text;
BEGIN
    sql_str = 'delete from '||in_table_name|| ' where id = ' ||in_id||';';      
    execute sql_str;
END;
$$;


ALTER FUNCTION public.delete_by_id(in_table_name text, in_id bigint) OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 860548)
-- Name: get_max_table_rev(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION get_max_table_rev(in_table_name text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
	tmp_rev bigint;  
	sql_str text;
BEGIN
       sql_str ='select max(id) from '||in_table_name|| ';';
      execute sql_str into tmp_rev;
      return tmp_rev;
END;
$$;


ALTER FUNCTION public.get_max_table_rev(in_table_name text) OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 861673)
-- Name: get_next_rev(text); Type: FUNCTION; Schema: public; Owner: antrax
--

CREATE FUNCTION get_next_rev(tb_name text) RETURNS bigint
    LANGUAGE plpgsql
    AS $$
DECLARE
    result bigint;
    sql_str text;
BEGIN
    sql_str = 'select max(rev) from '||tb_name||';';
    execute sql_str INTO result;
    IF result is null then
        result = 0;
    end IF;  
    return result + 1;
END;
$$;


ALTER FUNCTION public.get_next_rev(tb_name text) OWNER TO antrax;

--
-- TOC entry 247 (class 1255 OID 861177)
-- Name: tr_aft_ins_sim(); Type: FUNCTION; Schema: public; Owner: antrax
--

CREATE FUNCTION tr_aft_ins_sim() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  tmp_id bigint;
BEGIN
  IF NEW.uid IS NOT NULL THEN
    tmp_id = 0;
    SELECT id INTO tmp_id FROM sim_serializable_data WHERE uid = NEW.uid;
    IF tmp_id IS NULL OR tmp_id < 1 THEN
      INSERT INTO sim_serializable_data (uid) VALUES (NEW.uid);
    END IF;
  END IF;
  return NEW;
END;
$$;


ALTER FUNCTION public.tr_aft_ins_sim() OWNER TO antrax;

--
-- TOC entry 262 (class 1255 OID 860551)
-- Name: tr_registry_upd(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION tr_registry_upd() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
    NEW.updating_time = now();
    return NEW;
END;
$$;


ALTER FUNCTION public.tr_registry_upd() OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 860552)
-- Name: tr_script_instance(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION tr_script_instance() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    duplicate_instance_id bigint;
BEGIN
    IF (TG_OP = 'UPDATE') THEN   
        NEW.rev = get_next_rev(TG_TABLE_NAME);  
        RETURN NEW;
     end IF;
     IF (TG_OP = 'INSERT') THEN 
        NEW.rev = get_next_rev(TG_TABLE_NAME); 
        IF (New.field = 'SS_ACTIVITY' or New.field = 'SS_FACTOR' or New.field = 'SS_GW_SELECTOR' or New.field = 'SS_SESSION' or New.field = 'VS_INCOMING_CALL_MANAGER' or New.field = 'VS_FACTOR') THEN 
            select id into duplicate_instance_id from script_instance where field = New.field and sim_group_id = New.sim_group_id;
            IF (duplicate_instance_id is not null and duplicate_instance_id >1) then
                return Null;
            end IF;
         end IF; 
         RETURN NEW;
     END IF;  
     IF (TG_OP = 'DELETE') THEN
        RETURN OLD;  
     END IF;
END;
$$;


ALTER FUNCTION public.tr_script_instance() OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 860553)
-- Name: tr_set_curr_rev(); Type: FUNCTION; Schema: public; Owner: antrax
--

CREATE FUNCTION tr_set_curr_rev() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    sql_str text;
    next_rev bigint;
BEGIN
    next_rev = get_next_rev(TG_TABLE_NAME);
    IF(TG_TABLE_NAME = 'link_with_sim_group' or TG_TABLE_NAME = 'server') then   
     sql_str = 'update revisions set '||TG_TABLE_NAME|| ' = ' ||next_rev||';';      
     execute sql_str;
    end if;
    IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
        NEW.rev = next_rev;
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
END;
$$;


ALTER FUNCTION public.tr_set_curr_rev() OWNER TO antrax;

--
-- TOC entry 263 (class 1255 OID 860554)
-- Name: tr_upd_server_config_rev(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION tr_upd_server_config_rev() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
BEGIN
	NEW.rev = OLD.rev + 1;
	return NEW;
END;
$$;


ALTER FUNCTION public.tr_upd_server_config_rev() OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 860555)
-- Name: validate(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION validate(in_table_name text, in_id bigint) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    tmp_id bigint;  
    sql_str text;
BEGIN
    sql_str = 'select id from '||in_table_name|| ' where id = ' ||in_id||';';      
    execute sql_str into tmp_id;
    if tmp_id > 0 THEN
    	 return true;
    end if;
    return false;
END;
$$;


ALTER FUNCTION public.validate(in_table_name text, in_id bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- TOC entry 233 (class 1259 OID 861451)
-- Name: alaris_sms; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE alaris_sms (
    id uuid NOT NULL,
    ani text NOT NULL,
    dnis text NOT NULL,
    message text NOT NULL,
    service_type text,
    long_message_mode text NOT NULL,
    data_coding integer,
    status text NOT NULL,
    create_time timestamp with time zone DEFAULT now() NOT NULL,
    last_update_time timestamp with time zone DEFAULT now() NOT NULL,
    sar_id text,
    sar_parts integer,
    sar_part_number integer,
    sms_id uuid
);


ALTER TABLE alaris_sms OWNER TO antrax;

--
-- TOC entry 181 (class 1259 OID 860556)
-- Name: call_path_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE call_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE call_path_id_seq OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 861556)
-- Name: call_path; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE call_path (
    id bigint DEFAULT nextval('call_path_id_seq'::regclass) NOT NULL,
    cdr_id bigint NOT NULL,
    sim_device_uid integer,
    sim_channel_num smallint,
    gsm_device_uid integer,
    gsm_channel_num smallint,
    iccid text,
    gsm_group_name text,
    sim_group_name text,
    cdr_drop_reason text,
    start_time timestamp with time zone,
    cdr_drop_code smallint DEFAULT 0 NOT NULL
);


ALTER TABLE call_path OWNER TO antrax;

--
-- TOC entry 182 (class 1259 OID 860566)
-- Name: cdr_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cdr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cdr_id_seq OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 860568)
-- Name: cdr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE cdr (
    id bigint DEFAULT nextval('cdr_id_seq'::regclass) NOT NULL,
    setup_time timestamp with time zone NOT NULL,
    start_time timestamp with time zone,
    stop_time timestamp with time zone,
    called_phone_number text,
    voice_server_name text,
    call_type text,
    dial_time timestamp with time zone,
    alert_time timestamp with time zone,
    caller_phone_number text,
    CONSTRAINT cdr_call_type_check CHECK ((call_type = ANY (ARRAY[NULL::text, 'VS_TO_GSM'::text, 'VS_TO_VS'::text, 'GSM_TO_VS'::text, 'VOIP_TO_GSM'::text])))
);
ALTER TABLE ONLY cdr ALTER COLUMN called_phone_number SET STATISTICS 0;


ALTER TABLE cdr OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 861497)
-- Name: cell_equalizer_configuration; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE cell_equalizer_configuration (
    id bigint NOT NULL,
    server_id bigint NOT NULL,
    class_name text NOT NULL,
    configuration json
);


ALTER TABLE cell_equalizer_configuration OWNER TO antrax;

--
-- TOC entry 239 (class 1259 OID 861493)
-- Name: cell_equalizer_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE cell_equalizer_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cell_equalizer_configuration_id_seq OWNER TO antrax;

--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 239
-- Name: cell_equalizer_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE cell_equalizer_configuration_id_seq OWNED BY cell_equalizer_configuration.id;


--
-- TOC entry 240 (class 1259 OID 861495)
-- Name: cell_equalizer_configuration_server_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE cell_equalizer_configuration_server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cell_equalizer_configuration_server_id_seq OWNER TO antrax;

--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 240
-- Name: cell_equalizer_configuration_server_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE cell_equalizer_configuration_server_id_seq OWNED BY cell_equalizer_configuration.server_id;


--
-- TOC entry 229 (class 1259 OID 861384)
-- Name: cell_monitor; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE cell_monitor (
    id integer NOT NULL,
    gsm_channel text NOT NULL,
    cell smallint NOT NULL,
    bsic integer NOT NULL,
    lac integer NOT NULL,
    cell_id integer NOT NULL,
    arfcn integer NOT NULL,
    rxlev integer NOT NULL,
    c1 integer NOT NULL,
    c2 integer NOT NULL,
    ta integer,
    rxqual smallint,
    plmn text,
    add_time timestamp with time zone NOT NULL
);


ALTER TABLE cell_monitor OWNER TO antrax;

--
-- TOC entry 228 (class 1259 OID 861382)
-- Name: cell_monitor_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE cell_monitor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cell_monitor_id_seq OWNER TO antrax;

--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 228
-- Name: cell_monitor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE cell_monitor_id_seq OWNED BY cell_monitor.id;


--
-- TOC entry 184 (class 1259 OID 860588)
-- Name: edit_config_rev_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE edit_config_rev_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE edit_config_rev_seq OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 860599)
-- Name: gsm_channel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gsm_channel (
    id bigint DEFAULT nextval(('"public"."gsm_channel_id_seq"'::text)::regclass) NOT NULL,
    device_uid integer NOT NULL,
    channel_num smallint NOT NULL,
    gsm_group_id bigint,
    lock boolean DEFAULT false,
    lock_time timestamp with time zone,
    lock_reason text,
    lock_arfcn integer
);


ALTER TABLE gsm_channel OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 860606)
-- Name: gsm_channel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gsm_channel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gsm_channel_id_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 860608)
-- Name: gsm_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gsm_group (
    id bigint DEFAULT nextval(('"public"."gsm_group_id_seq"'::text)::regclass) NOT NULL,
    name text NOT NULL,
    rev bigint DEFAULT 0 NOT NULL,
    lock_on_imsi_catcher_detect boolean DEFAULT false,
    lock_all_gsm_channels_on_server boolean DEFAULT false
);


ALTER TABLE gsm_group OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 860617)
-- Name: gsm_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gsm_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gsm_group_id_seq OWNER TO postgres;

SET default_with_oids = true;

--
-- TOC entry 232 (class 1259 OID 861421)
-- Name: gsm_view; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE gsm_view (
    trusted boolean DEFAULT false NOT NULL,
    serving_cell boolean DEFAULT false NOT NULL,
    arfcn integer DEFAULT 0 NOT NULL,
    bsic integer DEFAULT 0 NOT NULL,
    lac integer DEFAULT 0 NOT NULL,
    cell_id integer DEFAULT 0 NOT NULL,
    mcc smallint DEFAULT 0 NOT NULL,
    mnc smallint DEFAULT 0 NOT NULL,
    network_survey_rx_lev integer DEFAULT 0 NOT NULL,
    first_appearance timestamp with time zone DEFAULT now() NOT NULL,
    last_appearance timestamp with time zone DEFAULT now() NOT NULL,
    server_id bigint NOT NULL,
    network_survey_number_occurrences integer DEFAULT 1 NOT NULL,
    successful_calls_count integer DEFAULT 0 NOT NULL,
    total_calls_count integer DEFAULT 0 NOT NULL,
    calls_duration bigint DEFAULT 0 NOT NULL,
    outgoing_sms_count integer DEFAULT 0 NOT NULL,
    incoming_sms_count integer DEFAULT 0 NOT NULL,
    ussd_count integer DEFAULT 0 NOT NULL,
    amount_pdd bigint DEFAULT 0 NOT NULL,
    notes text,
    serving_number_occurrences integer DEFAULT 0 NOT NULL,
    neighbours_number_occurrences integer DEFAULT 0 NOT NULL,
    cell_info_rx_lev integer DEFAULT 0 NOT NULL
);


ALTER TABLE gsm_view OWNER TO antrax;

SET default_with_oids = false;

--
-- TOC entry 189 (class 1259 OID 860670)
-- Name: link_with_sim_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE link_with_sim_group (
    id bigint DEFAULT nextval(('"public"."link_with_sim_group_id_seq"'::text)::regclass) NOT NULL,
    gsm_group_id bigint NOT NULL,
    sim_group_id bigint NOT NULL,
    rev bigint DEFAULT 0 NOT NULL
);


ALTER TABLE link_with_sim_group OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 860676)
-- Name: link_with_sim_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE link_with_sim_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE link_with_sim_group_id_seq OWNER TO postgres;

SET default_with_oids = true;

--
-- TOC entry 231 (class 1259 OID 861396)
-- Name: network_survey; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE network_survey (
    id integer NOT NULL,
    arfcn integer DEFAULT 0 NOT NULL,
    bsic integer,
    rxlev integer DEFAULT 0 NOT NULL,
    ber real,
    mcc smallint,
    mnc smallint,
    lac integer,
    cell_id integer,
    cell_status text,
    arfcns integer[],
    neighbours_arfcn integer[],
    add_time timestamp with time zone NOT NULL,
    gsm_channel text NOT NULL,
    server_id bigint NOT NULL
);


ALTER TABLE network_survey OWNER TO antrax;

--
-- TOC entry 230 (class 1259 OID 861394)
-- Name: network_survey_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE network_survey_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE network_survey_id_seq OWNER TO antrax;

--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 230
-- Name: network_survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE network_survey_id_seq OWNED BY network_survey.id;


--
-- TOC entry 236 (class 1259 OID 861469)
-- Name: phone_number_prefix; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE phone_number_prefix (
    id bigint NOT NULL,
    name text NOT NULL,
    prefix text NOT NULL,
    phone_number_count integer DEFAULT 0 NOT NULL
);


ALTER TABLE phone_number_prefix OWNER TO antrax;

--
-- TOC entry 234 (class 1259 OID 861461)
-- Name: phone_number_prefix_config; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE phone_number_prefix_config (
    enable boolean NOT NULL,
    default_prefix text NOT NULL
);


ALTER TABLE phone_number_prefix_config OWNER TO antrax;

--
-- TOC entry 235 (class 1259 OID 861467)
-- Name: phone_number_prefix_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE phone_number_prefix_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE phone_number_prefix_id_seq OWNER TO antrax;

--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 235
-- Name: phone_number_prefix_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE phone_number_prefix_id_seq OWNED BY phone_number_prefix.id;


--
-- TOC entry 238 (class 1259 OID 861481)
-- Name: phone_number_prefix_list; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE phone_number_prefix_list (
    id bigint NOT NULL,
    phone_number_prefix_id bigint NOT NULL,
    phone_number text NOT NULL
);


ALTER TABLE phone_number_prefix_list OWNER TO antrax;

--
-- TOC entry 237 (class 1259 OID 861479)
-- Name: phone_number_prefix_list_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE phone_number_prefix_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE phone_number_prefix_list_id_seq OWNER TO antrax;

--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 237
-- Name: phone_number_prefix_list_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE phone_number_prefix_list_id_seq OWNED BY phone_number_prefix_list.id;


--
-- TOC entry 219 (class 1259 OID 861280)
-- Name: received_sms; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE received_sms (
    id integer NOT NULL,
    iccid text NOT NULL,
    sms_id uuid,
    sms_center_number text NOT NULL,
    sender_phone_number text NOT NULL,
    message text NOT NULL,
    message_class text NOT NULL,
    encoding text NOT NULL,
    sender_time timestamp with time zone NOT NULL,
    received_time timestamp with time zone DEFAULT now() NOT NULL,
    reference_number integer,
    number_of_sms_part integer,
    count_of_sms_parts integer
);


ALTER TABLE received_sms OWNER TO antrax;

--
-- TOC entry 218 (class 1259 OID 861278)
-- Name: received_sms_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE received_sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE received_sms_id_seq OWNER TO antrax;

--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 218
-- Name: received_sms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE received_sms_id_seq OWNED BY received_sms.id;


--
-- TOC entry 191 (class 1259 OID 860704)
-- Name: registry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE registry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE registry_id_seq OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 860706)
-- Name: registry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE registry (
    id bigint DEFAULT nextval('registry_id_seq'::regclass) NOT NULL,
    path text NOT NULL,
    value text,
    updating_time timestamp with time zone DEFAULT now() NOT NULL,
    rev bigint DEFAULT 0
);


ALTER TABLE registry OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 860715)
-- Name: revisions; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE revisions (
    device bigint DEFAULT 0 NOT NULL,
    gsm_channel bigint DEFAULT 0 NOT NULL,
    gsm_group bigint DEFAULT 0 NOT NULL,
    ip_range bigint DEFAULT 0 NOT NULL,
    link_with_sim_group bigint DEFAULT 0 NOT NULL,
    net_interface bigint DEFAULT 0 NOT NULL,
    script_definition bigint DEFAULT 0 NOT NULL,
    script_instance bigint DEFAULT 0 NOT NULL,
    script_param bigint DEFAULT 0 NOT NULL,
    script_param_def bigint DEFAULT 0 NOT NULL,
    server bigint DEFAULT 0 NOT NULL,
    sim bigint DEFAULT 0 NOT NULL,
    sim_group bigint DEFAULT 0 NOT NULL
);


ALTER TABLE revisions OWNER TO antrax;

--
-- TOC entry 244 (class 1259 OID 861664)
-- Name: route_config; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE route_config (
    id bigint NOT NULL,
    call_allocation_algorithm text NOT NULL,
    sms_allocation_algorithm text NOT NULL
);


ALTER TABLE route_config OWNER TO antrax;

--
-- TOC entry 243 (class 1259 OID 861662)
-- Name: route_config_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE route_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE route_config_id_seq OWNER TO antrax;

--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 243
-- Name: route_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE route_config_id_seq OWNED BY route_config.id;


--
-- TOC entry 194 (class 1259 OID 860731)
-- Name: script_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE script_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE script_definition_id_seq OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 860733)
-- Name: script_definition; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE script_definition (
    id bigint DEFAULT nextval('script_definition_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    code text NOT NULL,
    description text,
    rev bigint DEFAULT 0
);


ALTER TABLE script_definition OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 216 (class 1259 OID 861220)
-- Name: script_file; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE script_file (
    id integer NOT NULL,
    name text,
    version text,
    data bytea,
    rev bigint,
    check_sum bigint,
    compilation_time timestamp with time zone,
    upload_time timestamp with time zone,
    file_count integer,
    class_count integer
);


ALTER TABLE script_file OWNER TO antrax;

--
-- TOC entry 215 (class 1259 OID 861218)
-- Name: script_file_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE script_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE script_file_id_seq OWNER TO antrax;

--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 215
-- Name: script_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE script_file_id_seq OWNED BY script_file.id;


SET default_with_oids = true;

--
-- TOC entry 196 (class 1259 OID 860741)
-- Name: script_instance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE script_instance (
    id bigint DEFAULT nextval(('"public"."script_instance_id_seq"'::text)::regclass) NOT NULL,
    script_def_id bigint NOT NULL,
    sim_group_id bigint,
    field text NOT NULL,
    field_index bigint NOT NULL,
    rev bigint DEFAULT 0
);


ALTER TABLE script_instance OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 860749)
-- Name: script_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE script_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE script_instance_id_seq OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 860751)
-- Name: script_param_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE script_param_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE script_param_id_seq OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 860753)
-- Name: script_param; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE script_param (
    id bigint DEFAULT nextval('script_param_id_seq'::regclass) NOT NULL,
    script_instance_id bigint NOT NULL,
    script_param_def_id bigint NOT NULL,
    value bytea,
    rev bigint DEFAULT 0
);


ALTER TABLE script_param OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 860761)
-- Name: script_parameter_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE script_parameter_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE script_parameter_definition_id_seq OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 860763)
-- Name: script_param_def; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE script_param_def (
    id bigint DEFAULT nextval('script_parameter_definition_id_seq'::regclass) NOT NULL,
    script_def_id bigint NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    description text,
    "array" boolean DEFAULT false,
    method text,
    default_value bytea NOT NULL,
    rev bigint DEFAULT 0
);


ALTER TABLE script_param_def OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 860773)
-- Name: scripts_commons; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE scripts_commons (
    id bigint NOT NULL,
    name text NOT NULL,
    value bytea,
    sim_group_id bigint NOT NULL
);


ALTER TABLE scripts_commons OWNER TO antrax;

--
-- TOC entry 203 (class 1259 OID 860779)
-- Name: scripts_commons_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE scripts_commons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE scripts_commons_id_seq OWNER TO antrax;

--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 203
-- Name: scripts_commons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE scripts_commons_id_seq OWNED BY scripts_commons.id;


--
-- TOC entry 221 (class 1259 OID 861292)
-- Name: sent_sms; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE sent_sms (
    id integer NOT NULL,
    iccid text NOT NULL,
    sms_id uuid,
    sms_center_number text,
    recipient_phone_number text NOT NULL,
    message text NOT NULL,
    message_class text NOT NULL,
    encoding text NOT NULL,
    sender_time timestamp with time zone DEFAULT now() NOT NULL,
    reference_number integer,
    number_of_sms_part integer,
    count_of_sms_parts integer,
    receipt_requested boolean DEFAULT false NOT NULL,
    delivery_status text NOT NULL
);


ALTER TABLE sent_sms OWNER TO antrax;

--
-- TOC entry 220 (class 1259 OID 861290)
-- Name: sent_sms_id_seq; Type: SEQUENCE; Schema: public; Owner: antrax
--

CREATE SEQUENCE sent_sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sent_sms_id_seq OWNER TO antrax;

--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 220
-- Name: sent_sms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: antrax
--

ALTER SEQUENCE sent_sms_id_seq OWNED BY sent_sms.id;


--
-- TOC entry 204 (class 1259 OID 860781)
-- Name: server_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE server_id_seq OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 860783)
-- Name: server; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE server (
    id bigint DEFAULT nextval('server_id_seq'::regclass) NOT NULL,
    name text NOT NULL,
    vs_enable boolean DEFAULT false,
    ss_enable boolean DEFAULT false,
    rev bigint DEFAULT 0 NOT NULL,
    public_address inet,
    call_route_config json,
    sms_route_config json
);


ALTER TABLE server OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 860802)
-- Name: session_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE session_id_seq OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 860804)
-- Name: session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE session (
    id bigint DEFAULT nextval('session_id_seq'::regclass) NOT NULL,
    client_uid text NOT NULL,
    user_name text,
    "group" text,
    ip inet,
    start_time timestamp with time zone,
    end_time timestamp with time zone,
    last_access_time timestamp with time zone
);


ALTER TABLE session OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 860811)
-- Name: sim_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sim_id_seq OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 860862)
-- Name: sim; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sim (
    id bigint DEFAULT nextval('sim_id_seq'::regclass) NOT NULL,
    iccid text NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    locked boolean DEFAULT false NOT NULL,
    lock_reason text,
    lock_time timestamp with time zone,
    all_call_count integer DEFAULT 0 NOT NULL,
    successful_call_count integer DEFAULT 0 NOT NULL,
    successful_call_duration bigint DEFAULT 0 NOT NULL,
    sim_group_id bigint,
    incoming_call_count integer DEFAULT 0 NOT NULL,
    incoming_successful_call_count integer DEFAULT 0 NOT NULL,
    incoming_successful_call_duration bigint DEFAULT 0 NOT NULL,
    reset_ss_scripts_flag boolean DEFAULT false,
    reset_vs_scripts_flag boolean DEFAULT false,
    imei text,
    phone_number text,
    success_sms_count integer DEFAULT 0,
    incoming_sms_count integer DEFAULT 0,
    total_sms_count integer DEFAULT 0 NOT NULL,
    total_sms_statuses integer DEFAULT 0 NOT NULL,
    note text
);


ALTER TABLE sim OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 860823)
-- Name: sim_event_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sim_event_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sim_event_log_id_seq OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 861267)
-- Name: sim_event_log; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE sim_event_log (
    id bigint DEFAULT nextval('sim_event_log_id_seq'::regclass) NOT NULL,
    iccid text NOT NULL,
    event text NOT NULL,
    start_time timestamp with time zone DEFAULT now() NOT NULL,
    args text[] NOT NULL
);


ALTER TABLE sim_event_log OWNER TO antrax;

SET default_with_oids = false;

--
-- TOC entry 210 (class 1259 OID 860834)
-- Name: sim_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sim_group (
    id bigint DEFAULT nextval(('"public"."sim_group_id_seq"'::text)::regclass) NOT NULL,
    name text NOT NULL,
    idle_after_registration_mediana bigint DEFAULT 0 NOT NULL,
    idle_after_registration_delta bigint DEFAULT 0 NOT NULL,
    idle_after_successful_call_mediana bigint DEFAULT 0 NOT NULL,
    idle_after_successful_call_delta bigint DEFAULT 0 NOT NULL,
    idle_before_unregistration_mediana bigint DEFAULT 0 NOT NULL,
    idle_before_unregistration_delta bigint DEFAULT 0 NOT NULL,
    self_dial_factor integer DEFAULT 0 NOT NULL,
    self_dial_chance integer DEFAULT 0 NOT NULL,
    idle_before_self_call_mediana bigint DEFAULT 0 NOT NULL,
    idle_before_self_call_delta bigint DEFAULT 0 NOT NULL,
    idle_after_self_call_timeout_mediana bigint DEFAULT 0 NOT NULL,
    idle_after_self_call_timeout_delta bigint DEFAULT 0 NOT NULL,
    rev bigint DEFAULT 0 NOT NULL,
    description text,
    can_be_appointed boolean DEFAULT true,
    synthetic_ringing boolean DEFAULT false,
    fas_detection boolean DEFAULT true,
    suppress_incoming_call_signal boolean DEFAULT true,
    idle_after_zero_call_mediana bigint DEFAULT 0 NOT NULL,
    idle_after_zero_call_delta bigint DEFAULT 0 NOT NULL,
    registration_period integer,
    registration_count integer,
    operator_selection text DEFAULT 'auto'::text
);


ALTER TABLE sim_group OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 860858)
-- Name: sim_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sim_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sim_group_id_seq OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 860881)
-- Name: sim_serializable_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sim_serializable_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sim_serializable_data_id_seq OWNER TO postgres;

SET default_with_oids = true;

--
-- TOC entry 214 (class 1259 OID 860883)
-- Name: sim_serializable_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE sim_serializable_data (
    id bigint DEFAULT nextval('sim_serializable_data_id_seq'::regclass) NOT NULL,
    iccid text NOT NULL,
    activity_period_script bytea,
    session_script bytea,
    gw_selector_script bytea,
    business_activity_script bytea,
    factor_script bytea,
    incoming_call_management_script bytea,
    vs_factor_script bytea,
    action_provider_script bytea,
    call_filter_script bytea,
    sms_filter_script bytea,
    imei_generator_script bytea
);


ALTER TABLE sim_serializable_data OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 861353)
-- Name: voip_anti_spam_black_list_numbers; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_black_list_numbers (
    number text NOT NULL,
    status text DEFAULT 'AUTO'::text NOT NULL,
    status_description text DEFAULT '[]'::text NOT NULL,
    add_time timestamp with time zone DEFAULT now() NOT NULL,
    routing_request_count integer DEFAULT 0
);


ALTER TABLE voip_anti_spam_black_list_numbers OWNER TO antrax;

--
-- TOC entry 222 (class 1259 OID 861313)
-- Name: voip_anti_spam_configuration; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_configuration (
    enable boolean DEFAULT false NOT NULL,
    gray_list_period integer DEFAULT 0 NOT NULL,
    gray_list_max_routing_request_per_period integer DEFAULT 0 NOT NULL,
    gray_list_block_period integer DEFAULT 0 NOT NULL,
    acd_analyze_enable boolean DEFAULT false NOT NULL,
    acd_period integer DEFAULT 0 NOT NULL,
    acd_max_min_acd_call_per_period integer DEFAULT 0 NOT NULL,
    acd_min_acd integer DEFAULT 0 NOT NULL,
    black_list_period integer DEFAULT 0 NOT NULL,
    black_list_max_routing_request_per_period integer DEFAULT 0 NOT NULL,
    black_list_max_block_count_before_move_to_black_list integer DEFAULT 0 NOT NULL,
    gsm_alerting_analyze_enable boolean DEFAULT false NOT NULL,
    gsm_alerting_time integer DEFAULT 0 NOT NULL,
    gsm_alerting_routing_period integer DEFAULT 0 NOT NULL,
    gsm_alerting_max_routing_request_per_period integer DEFAULT 0 NOT NULL,
    fas_analyze_enable boolean DEFAULT false NOT NULL,
    fas_routing_period integer DEFAULT 0 NOT NULL,
    fas_max_routing_request_per_period integer DEFAULT 0 NOT NULL,
    voip_alerting_analyze_enable boolean DEFAULT false NOT NULL,
    voip_alerting_time integer DEFAULT 0 NOT NULL,
    voip_alerting_routing_period integer DEFAULT 0 NOT NULL,
    voip_alerting_max_routing_request_per_period integer DEFAULT 0 NOT NULL
);


ALTER TABLE voip_anti_spam_configuration OWNER TO antrax;

--
-- TOC entry 224 (class 1259 OID 861339)
-- Name: voip_anti_spam_gray_list_numbers; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_gray_list_numbers (
    number text NOT NULL,
    block_time timestamp with time zone DEFAULT now() NOT NULL,
    block_count integer,
    block_time_left timestamp with time zone DEFAULT now() NOT NULL,
    routing_request_count integer DEFAULT 0,
    status text DEFAULT 'AUTO'::text NOT NULL,
    status_description text DEFAULT '[]'::text NOT NULL
);


ALTER TABLE voip_anti_spam_gray_list_numbers OWNER TO antrax;

--
-- TOC entry 227 (class 1259 OID 861374)
-- Name: voip_anti_spam_history_block_numbers; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_history_block_numbers (
    number text,
    "time" timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE voip_anti_spam_history_block_numbers OWNER TO antrax;

--
-- TOC entry 226 (class 1259 OID 861366)
-- Name: voip_anti_spam_history_routing_request; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_history_routing_request (
    number text,
    "time" timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE voip_anti_spam_history_routing_request OWNER TO antrax;

--
-- TOC entry 223 (class 1259 OID 861327)
-- Name: voip_anti_spam_white_list_numbers; Type: TABLE; Schema: public; Owner: antrax
--

CREATE TABLE voip_anti_spam_white_list_numbers (
    number text NOT NULL,
    status text DEFAULT 'AUTO'::text NOT NULL,
    add_time timestamp with time zone DEFAULT now() NOT NULL,
    routing_request_count integer DEFAULT 0
);


ALTER TABLE voip_anti_spam_white_list_numbers OWNER TO antrax;

--
-- TOC entry 3236 (class 2604 OID 861500)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_equalizer_configuration ALTER COLUMN id SET DEFAULT nextval('cell_equalizer_configuration_id_seq'::regclass);


--
-- TOC entry 3237 (class 2604 OID 861501)
-- Name: server_id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_equalizer_configuration ALTER COLUMN server_id SET DEFAULT nextval('cell_equalizer_configuration_server_id_seq'::regclass);


--
-- TOC entry 3205 (class 2604 OID 861387)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_monitor ALTER COLUMN id SET DEFAULT nextval('cell_monitor_id_seq'::regclass);


--
-- TOC entry 3206 (class 2604 OID 861399)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY network_survey ALTER COLUMN id SET DEFAULT nextval('network_survey_id_seq'::regclass);


--
-- TOC entry 3233 (class 2604 OID 861472)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY phone_number_prefix ALTER COLUMN id SET DEFAULT nextval('phone_number_prefix_id_seq'::regclass);


--
-- TOC entry 3235 (class 2604 OID 861484)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY phone_number_prefix_list ALTER COLUMN id SET DEFAULT nextval('phone_number_prefix_list_id_seq'::regclass);


--
-- TOC entry 3164 (class 2604 OID 861283)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY received_sms ALTER COLUMN id SET DEFAULT nextval('received_sms_id_seq'::regclass);


--
-- TOC entry 3240 (class 2604 OID 861667)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY route_config ALTER COLUMN id SET DEFAULT nextval('route_config_id_seq'::regclass);


--
-- TOC entry 3161 (class 2604 OID 861223)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY script_file ALTER COLUMN id SET DEFAULT nextval('script_file_id_seq'::regclass);


--
-- TOC entry 3118 (class 2604 OID 860895)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY scripts_commons ALTER COLUMN id SET DEFAULT nextval('scripts_commons_id_seq'::regclass);


--
-- TOC entry 3166 (class 2604 OID 861295)
-- Name: id; Type: DEFAULT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY sent_sms ALTER COLUMN id SET DEFAULT nextval('sent_sms_id_seq'::regclass);


--
-- TOC entry 3323 (class 2606 OID 861460)
-- Name: alaris_sms_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY alaris_sms
    ADD CONSTRAINT alaris_sms_pkey PRIMARY KEY (id);


--
-- TOC entry 3338 (class 2606 OID 861565)
-- Name: call_path_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY call_path
    ADD CONSTRAINT call_path_pkey PRIMARY KEY (id);


--
-- TOC entry 3242 (class 2606 OID 860899)
-- Name: cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cdr
    ADD CONSTRAINT cdr_pkey PRIMARY KEY (id);


--
-- TOC entry 3332 (class 2606 OID 861506)
-- Name: cell_equalizer_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_equalizer_configuration
    ADD CONSTRAINT cell_equalizer_configuration_pkey PRIMARY KEY (id);


--
-- TOC entry 3334 (class 2606 OID 861676)
-- Name: cell_equalizer_configuration_server_id_key; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_equalizer_configuration
    ADD CONSTRAINT cell_equalizer_configuration_server_id_key UNIQUE (server_id);


--
-- TOC entry 3314 (class 2606 OID 861392)
-- Name: cell_monitor_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY cell_monitor
    ADD CONSTRAINT cell_monitor_pkey PRIMARY KEY (id);


--
-- TOC entry 3248 (class 2606 OID 860907)
-- Name: gsm_channel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gsm_channel
    ADD CONSTRAINT gsm_channel_pkey PRIMARY KEY (id);


--
-- TOC entry 3251 (class 2606 OID 860909)
-- Name: gsm_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gsm_group
    ADD CONSTRAINT gsm_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3321 (class 2606 OID 861436)
-- Name: gsm_view_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY gsm_view
    ADD CONSTRAINT gsm_view_pkey PRIMARY KEY (arfcn, bsic, lac, cell_id, server_id);


--
-- TOC entry 3254 (class 2606 OID 860915)
-- Name: link_with_sim_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY link_with_sim_group
    ADD CONSTRAINT link_with_sim_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3319 (class 2606 OID 861406)
-- Name: network_survey_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY network_survey
    ADD CONSTRAINT network_survey_pkey PRIMARY KEY (id);


--
-- TOC entry 3328 (class 2606 OID 861491)
-- Name: phone_number_prefix_list_key; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY phone_number_prefix_list
    ADD CONSTRAINT phone_number_prefix_list_key UNIQUE (phone_number);


--
-- TOC entry 3330 (class 2606 OID 861489)
-- Name: phone_number_prefix_list_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY phone_number_prefix_list
    ADD CONSTRAINT phone_number_prefix_list_pkey PRIMARY KEY (id);


--
-- TOC entry 3325 (class 2606 OID 861478)
-- Name: phone_number_prefix_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY phone_number_prefix
    ADD CONSTRAINT phone_number_prefix_pkey PRIMARY KEY (id);


--
-- TOC entry 3298 (class 2606 OID 861289)
-- Name: received_sms_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY received_sms
    ADD CONSTRAINT received_sms_pkey PRIMARY KEY (id);


--
-- TOC entry 3258 (class 2606 OID 860923)
-- Name: registry_path_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY registry
    ADD CONSTRAINT registry_path_key UNIQUE (path, value);


--
-- TOC entry 3260 (class 2606 OID 860925)
-- Name: registry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY registry
    ADD CONSTRAINT registry_pkey PRIMARY KEY (id);


--
-- TOC entry 3341 (class 2606 OID 861672)
-- Name: route_config_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY route_config
    ADD CONSTRAINT route_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3262 (class 2606 OID 860927)
-- Name: script_definition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_definition
    ADD CONSTRAINT script_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 3293 (class 2606 OID 861228)
-- Name: script_file_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY script_file
    ADD CONSTRAINT script_file_pkey PRIMARY KEY (id);


--
-- TOC entry 3264 (class 2606 OID 860929)
-- Name: script_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_instance
    ADD CONSTRAINT script_instance_pkey PRIMARY KEY (id);


--
-- TOC entry 3266 (class 2606 OID 860931)
-- Name: script_param_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_param
    ADD CONSTRAINT script_param_pkey PRIMARY KEY (id);


--
-- TOC entry 3268 (class 2606 OID 860933)
-- Name: script_parameter_definition_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_param_def
    ADD CONSTRAINT script_parameter_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 3270 (class 2606 OID 860935)
-- Name: scripts_commons_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY scripts_commons
    ADD CONSTRAINT scripts_commons_pkey PRIMARY KEY (id);


--
-- TOC entry 3300 (class 2606 OID 861303)
-- Name: sent_sms_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY sent_sms
    ADD CONSTRAINT sent_sms_pkey PRIMARY KEY (id);


--
-- TOC entry 3273 (class 2606 OID 860939)
-- Name: server_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY server
    ADD CONSTRAINT server_pkey PRIMARY KEY (id);


--
-- TOC entry 3275 (class 2606 OID 860941)
-- Name: session_client_uid_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_client_uid_key UNIQUE (client_uid);


--
-- TOC entry 3278 (class 2606 OID 860943)
-- Name: session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (id);


--
-- TOC entry 3296 (class 2606 OID 861274)
-- Name: sim_event_log_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY sim_event_log
    ADD CONSTRAINT sim_event_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3281 (class 2606 OID 860947)
-- Name: sim_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim_group
    ADD CONSTRAINT sim_group_pkey PRIMARY KEY (id);


--
-- TOC entry 3284 (class 2606 OID 860951)
-- Name: sim_key_key_ind; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim
    ADD CONSTRAINT sim_key_key_ind UNIQUE (iccid);


--
-- TOC entry 3286 (class 2606 OID 860953)
-- Name: sim_pkey_ind; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim
    ADD CONSTRAINT sim_pkey_ind PRIMARY KEY (id);


--
-- TOC entry 3289 (class 2606 OID 860955)
-- Name: sim_serializable_data_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim_serializable_data
    ADD CONSTRAINT sim_serializable_data_key UNIQUE (iccid);


--
-- TOC entry 3291 (class 2606 OID 860957)
-- Name: sim_serializable_data_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim_serializable_data
    ADD CONSTRAINT sim_serializable_data_pkey PRIMARY KEY (id);


--
-- TOC entry 3306 (class 2606 OID 861351)
-- Name: voip_anti_spam_gray_list_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY voip_anti_spam_gray_list_numbers
    ADD CONSTRAINT voip_anti_spam_gray_list_numbers_pkey PRIMARY KEY (number);


--
-- TOC entry 3309 (class 2606 OID 861364)
-- Name: voip_anti_spam_spam_gray_list_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY voip_anti_spam_black_list_numbers
    ADD CONSTRAINT voip_anti_spam_spam_gray_list_numbers_pkey PRIMARY KEY (number);


--
-- TOC entry 3303 (class 2606 OID 861337)
-- Name: voip_anti_spam_white_list_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY voip_anti_spam_white_list_numbers
    ADD CONSTRAINT voip_anti_spam_white_list_numbers_pkey PRIMARY KEY (number);


--
-- TOC entry 3335 (class 1259 OID 861571)
-- Name: call_path_cdr_id_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX call_path_cdr_id_idx ON call_path USING btree (cdr_id);


--
-- TOC entry 3336 (class 1259 OID 861572)
-- Name: call_path_gsm_group_name_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX call_path_gsm_group_name_idx ON call_path USING btree (gsm_group_name);


--
-- TOC entry 3339 (class 1259 OID 861573)
-- Name: call_path_sim_group_name_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX call_path_sim_group_name_idx ON call_path USING btree (sim_group_name);


--
-- TOC entry 3243 (class 1259 OID 860963)
-- Name: cdr_setup_time_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX cdr_setup_time_idx ON cdr USING btree (setup_time);


--
-- TOC entry 3312 (class 1259 OID 861393)
-- Name: cell_monitor_gsm_channel_add_time_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX cell_monitor_gsm_channel_add_time_idx ON cell_monitor USING btree (gsm_channel, add_time);


--
-- TOC entry 3244 (class 1259 OID 860988)
-- Name: gsm_channel_channel_num_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gsm_channel_channel_num_idx ON gsm_channel USING btree (channel_num);


--
-- TOC entry 3245 (class 1259 OID 861134)
-- Name: gsm_channel_device_uid_channel_num_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX gsm_channel_device_uid_channel_num_idx ON gsm_channel USING btree (device_uid, channel_num);


--
-- TOC entry 3246 (class 1259 OID 861132)
-- Name: gsm_channel_device_uid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gsm_channel_device_uid_idx ON gsm_channel USING btree (device_uid);


--
-- TOC entry 3249 (class 1259 OID 861133)
-- Name: gsm_device_device_uid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gsm_device_device_uid_idx ON gsm_channel USING btree (device_uid);


--
-- TOC entry 3252 (class 1259 OID 860968)
-- Name: gsm_group_rev_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX gsm_group_rev_idx ON gsm_group USING btree (rev);


--
-- TOC entry 3255 (class 1259 OID 860971)
-- Name: link_with_sim_group_rev_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX link_with_sim_group_rev_idx ON link_with_sim_group USING btree (rev);


--
-- TOC entry 3315 (class 1259 OID 861408)
-- Name: network_survey_add_time_gsm_channel_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX network_survey_add_time_gsm_channel_idx ON network_survey USING btree (add_time, gsm_channel);


--
-- TOC entry 3316 (class 1259 OID 861407)
-- Name: network_survey_arfcn_add_time_gsm_channel_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX network_survey_arfcn_add_time_gsm_channel_idx ON network_survey USING btree (arfcn, add_time, gsm_channel);


--
-- TOC entry 3317 (class 1259 OID 861409)
-- Name: network_survey_mcc_mnc_add_time_gsm_channel_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX network_survey_mcc_mnc_add_time_gsm_channel_idx ON network_survey USING btree (mcc, mnc, add_time, gsm_channel);


--
-- TOC entry 3326 (class 1259 OID 861492)
-- Name: phone_number_prefix_list_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX phone_number_prefix_list_idx ON phone_number_prefix_list USING btree (phone_number);


--
-- TOC entry 3256 (class 1259 OID 860972)
-- Name: registry_path_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX registry_path_idx ON registry USING btree (path);


--
-- TOC entry 3271 (class 1259 OID 860973)
-- Name: scripts_commons_sim_group_id_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX scripts_commons_sim_group_id_idx ON scripts_commons USING btree (sim_group_id);


--
-- TOC entry 3276 (class 1259 OID 860975)
-- Name: session_end_time_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX session_end_time_idx ON session USING btree (end_time);


--
-- TOC entry 3279 (class 1259 OID 860974)
-- Name: session_start_time_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX session_start_time_idx ON session USING btree (start_time);


--
-- TOC entry 3294 (class 1259 OID 861277)
-- Name: sim_event_log_iccid_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX sim_event_log_iccid_idx ON sim_event_log USING btree (iccid);


--
-- TOC entry 3282 (class 1259 OID 860980)
-- Name: sim_group_rev_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sim_group_rev_idx ON sim_group USING btree (rev);


--
-- TOC entry 3287 (class 1259 OID 860986)
-- Name: sim_sim_group_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX sim_sim_group_idx ON sim USING btree (sim_group_id);


--
-- TOC entry 3307 (class 1259 OID 861365)
-- Name: voip_anti_spam_black_list_numbers_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX voip_anti_spam_black_list_numbers_idx ON voip_anti_spam_black_list_numbers USING btree (number, status, add_time, routing_request_count);


--
-- TOC entry 3304 (class 1259 OID 861352)
-- Name: voip_anti_spam_gray_list_numbers_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX voip_anti_spam_gray_list_numbers_idx ON voip_anti_spam_gray_list_numbers USING btree (number, block_time, block_count, block_time_left, routing_request_count, status);


--
-- TOC entry 3311 (class 1259 OID 861381)
-- Name: voip_anti_spam_history_block_numbers_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX voip_anti_spam_history_block_numbers_idx ON voip_anti_spam_history_block_numbers USING btree (number);


--
-- TOC entry 3310 (class 1259 OID 861373)
-- Name: voip_anti_spam_history_routing_request_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX voip_anti_spam_history_routing_request_idx ON voip_anti_spam_history_routing_request USING btree (number, "time");


--
-- TOC entry 3301 (class 1259 OID 861338)
-- Name: voip_anti_spam_white_list_numbers_idx; Type: INDEX; Schema: public; Owner: antrax
--

CREATE INDEX voip_anti_spam_white_list_numbers_idx ON voip_anti_spam_white_list_numbers USING btree (number, status, add_time, routing_request_count);


--
-- TOC entry 3362 (class 2620 OID 861178)
-- Name: tr_aft_ins_sim; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_aft_ins_sim AFTER INSERT ON sim FOR EACH ROW EXECUTE PROCEDURE tr_aft_ins_sim();

ALTER TABLE sim DISABLE TRIGGER tr_aft_ins_sim;


--
-- TOC entry 3353 (class 2620 OID 860994)
-- Name: tr_gsm_group; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_gsm_group BEFORE INSERT OR DELETE OR UPDATE ON gsm_group FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3354 (class 2620 OID 860997)
-- Name: tr_link_with_sim_group; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_link_with_sim_group BEFORE INSERT OR DELETE OR UPDATE ON link_with_sim_group FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3355 (class 2620 OID 860999)
-- Name: tr_registry_upd; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_registry_upd BEFORE UPDATE ON registry FOR EACH ROW EXECUTE PROCEDURE tr_registry_upd();


--
-- TOC entry 3356 (class 2620 OID 861000)
-- Name: tr_script_definition; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_script_definition BEFORE INSERT OR DELETE OR UPDATE ON script_definition FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3363 (class 2620 OID 861229)
-- Name: tr_script_file; Type: TRIGGER; Schema: public; Owner: antrax
--

CREATE TRIGGER tr_script_file BEFORE INSERT OR DELETE OR UPDATE ON script_file FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3357 (class 2620 OID 861001)
-- Name: tr_script_instance; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_script_instance BEFORE INSERT OR DELETE OR UPDATE ON script_instance FOR EACH ROW EXECUTE PROCEDURE tr_script_instance();


--
-- TOC entry 3358 (class 2620 OID 861002)
-- Name: tr_script_param; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_script_param BEFORE INSERT OR DELETE OR UPDATE ON script_param FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3359 (class 2620 OID 861003)
-- Name: tr_script_param_def; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_script_param_def BEFORE INSERT OR DELETE OR UPDATE ON script_param_def FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3360 (class 2620 OID 861004)
-- Name: tr_server; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_server BEFORE INSERT OR DELETE OR UPDATE ON server FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3361 (class 2620 OID 861006)
-- Name: tr_sim_group; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_sim_group BEFORE INSERT OR DELETE OR UPDATE ON sim_group FOR EACH ROW EXECUTE PROCEDURE tr_set_curr_rev();


--
-- TOC entry 3352 (class 2606 OID 861566)
-- Name: call_path_cdr_fkey; Type: FK CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY call_path
    ADD CONSTRAINT call_path_cdr_fkey FOREIGN KEY (cdr_id) REFERENCES cdr(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3342 (class 2606 OID 861023)
-- Name: gsm_channel_gsm_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gsm_channel
    ADD CONSTRAINT gsm_channel_gsm_group_fkey FOREIGN KEY (gsm_group_id) REFERENCES gsm_group(id) ON UPDATE SET NULL ON DELETE SET NULL;


--
-- TOC entry 3344 (class 2606 OID 861038)
-- Name: link_with_gsm_group_gsm_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY link_with_sim_group
    ADD CONSTRAINT link_with_gsm_group_gsm_group_id_fkey FOREIGN KEY (gsm_group_id) REFERENCES gsm_group(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3343 (class 2606 OID 861033)
-- Name: link_with_gsm_group_sim_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY link_with_sim_group
    ADD CONSTRAINT link_with_gsm_group_sim_group_id_fkey FOREIGN KEY (sim_group_id) REFERENCES sim_group(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3346 (class 2606 OID 861048)
-- Name: script_instance_script_def_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_instance
    ADD CONSTRAINT script_instance_script_def_id_fkey FOREIGN KEY (script_def_id) REFERENCES script_definition(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3345 (class 2606 OID 861053)
-- Name: script_instance_sim_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_instance
    ADD CONSTRAINT script_instance_sim_group_id_fkey FOREIGN KEY (sim_group_id) REFERENCES sim_group(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3349 (class 2606 OID 861058)
-- Name: script_param_def_script_def_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_param_def
    ADD CONSTRAINT script_param_def_script_def_id_fkey FOREIGN KEY (script_def_id) REFERENCES script_definition(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3347 (class 2606 OID 861063)
-- Name: script_param_script_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_param
    ADD CONSTRAINT script_param_script_instance_id_fkey FOREIGN KEY (script_instance_id) REFERENCES script_instance(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3348 (class 2606 OID 861068)
-- Name: script_param_script_param_def_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY script_param
    ADD CONSTRAINT script_param_script_param_def_id_fkey FOREIGN KEY (script_param_def_id) REFERENCES script_param_def(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3350 (class 2606 OID 861073)
-- Name: scripts_commons_sim_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: antrax
--

ALTER TABLE ONLY scripts_commons
    ADD CONSTRAINT scripts_commons_sim_group_id_fkey FOREIGN KEY (sim_group_id) REFERENCES sim_group(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3351 (class 2606 OID 861172)
-- Name: sim_sim_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sim
    ADD CONSTRAINT sim_sim_group_id_fkey FOREIGN KEY (sim_group_id) REFERENCES sim_group(id) ON UPDATE SET NULL ON DELETE SET NULL;


--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-02-08 16:46:42 EET

--
-- PostgreSQL database dump complete
--

