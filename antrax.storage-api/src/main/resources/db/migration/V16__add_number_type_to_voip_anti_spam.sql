ALTER TABLE voip_anti_spam_white_list_numbers ADD COLUMN number_type text NOT NULL DEFAULT 'CALLED';
ALTER TABLE voip_anti_spam_gray_list_numbers ADD COLUMN number_type text NOT NULL DEFAULT 'CALLED';
ALTER TABLE voip_anti_spam_black_list_numbers ADD COLUMN number_type text NOT NULL DEFAULT 'CALLED';

ALTER TABLE voip_anti_spam_history_routing_request RENAME COLUMN number TO called_number;
ALTER TABLE voip_anti_spam_history_routing_request ADD COLUMN caller_number text;

ALTER TABLE voip_anti_spam_history_block_numbers ADD COLUMN number_type text NOT NULL DEFAULT 'CALLED';

UPDATE voip_anti_spam_configuration
SET gray_list_config = array_to_json((ARRAY(SELECT json_array_elements(gray_list_config)::jsonb || '{"numberType":"CALLED"}'::jsonb FROM voip_anti_spam_configuration)))
WHERE json_array_length(gray_list_config) > 0;

UPDATE voip_anti_spam_configuration
SET black_list_config = array_to_json((ARRAY(SELECT json_array_elements(black_list_config)::jsonb || '{"numberType":"CALLED"}'::jsonb FROM voip_anti_spam_configuration)))
WHERE json_array_length(black_list_config) > 0;

UPDATE voip_anti_spam_configuration
SET gsm_alerting_config = array_to_json((ARRAY(SELECT json_array_elements(gsm_alerting_config)::jsonb || '{"numberType":"CALLED"}'::jsonb FROM voip_anti_spam_configuration)))
WHERE json_array_length(gsm_alerting_config) > 0;

UPDATE voip_anti_spam_configuration
SET voip_alerting_config = array_to_json((ARRAY(SELECT json_array_elements(voip_alerting_config)::jsonb || '{"numberType":"CALLED"}'::jsonb FROM voip_anti_spam_configuration)))
WHERE json_array_length(voip_alerting_config) > 0;

UPDATE voip_anti_spam_configuration
SET fas_config = array_to_json((ARRAY(SELECT json_array_elements(fas_config)::jsonb || '{"numberType":"CALLED"}'::jsonb FROM voip_anti_spam_configuration)))
WHERE json_array_length(fas_config) > 0;

DROP INDEX public.voip_anti_spam_black_list_numbers_idx;
DROP INDEX public.voip_anti_spam_gray_list_numbers_idx;
DROP INDEX public.voip_anti_spam_history_block_numbers_idx;
DROP INDEX public.voip_anti_spam_history_routing_request_idx;
DROP INDEX public.voip_anti_spam_white_list_numbers_idx;

CREATE INDEX voip_anti_spam_black_list_numbers_idx ON voip_anti_spam_black_list_numbers (number_type, number);
CREATE INDEX voip_anti_spam_gray_list_numbers_idx ON voip_anti_spam_gray_list_numbers (number_type, number);
CREATE INDEX voip_anti_spam_white_list_numbers_idx ON voip_anti_spam_white_list_numbers (number_type, number);

CREATE INDEX voip_anti_spam_history_routing_request_caller_idx ON voip_anti_spam_history_routing_request (caller_number);
CREATE INDEX voip_anti_spam_history_routing_request_called_idx ON voip_anti_spam_history_routing_request (called_number);
CREATE INDEX voip_anti_spam_history_routing_request_time_idx ON voip_anti_spam_history_routing_request (time);

CREATE INDEX voip_anti_spam_history_block_numbers_idx ON voip_anti_spam_history_block_numbers (number_type);

ALTER TABLE voip_anti_spam_black_list_numbers DROP CONSTRAINT voip_anti_spam_spam_gray_list_numbers_pkey;
ALTER TABLE voip_anti_spam_gray_list_numbers DROP CONSTRAINT voip_anti_spam_gray_list_numbers_pkey;
ALTER TABLE voip_anti_spam_white_list_numbers DROP CONSTRAINT voip_anti_spam_white_list_numbers_pkey;

ALTER TABLE voip_anti_spam_black_list_numbers ADD PRIMARY KEY (number_type, number);
ALTER TABLE voip_anti_spam_gray_list_numbers ADD PRIMARY KEY (number_type, number);
ALTER TABLE voip_anti_spam_white_list_numbers ADD PRIMARY KEY (number_type, number);
