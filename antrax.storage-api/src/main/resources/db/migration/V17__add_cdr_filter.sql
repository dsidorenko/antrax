ALTER TABLE voip_anti_spam_configuration ADD COLUMN cdr_analyze_enable boolean NOT NULL DEFAULT false;
ALTER TABLE voip_anti_spam_configuration ADD COLUMN cdr_config json DEFAULT '[]';

ALTER TABLE voip_anti_spam_white_list_numbers ADD COLUMN status_description text NOT NULL DEFAULT '';
UPDATE voip_anti_spam_white_list_numbers SET status_description = (CASE status WHEN 'AUTO' THEN 'Added by cdr analyze' ELSE '' END);
