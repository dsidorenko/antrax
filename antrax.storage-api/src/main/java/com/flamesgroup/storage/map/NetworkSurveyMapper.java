/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.NetworkSurvey;
import com.flamesgroup.storage.jooq.tables.records.NetworkSurveyRecord;

import java.util.Arrays;
import java.util.Objects;

public final class NetworkSurveyMapper {

  private NetworkSurveyMapper() {
  }

  public static NetworkSurvey mapNetworkSurveyRecordToNetworkSurvey(final NetworkSurveyRecord networkSurveyRecord) {
    Objects.requireNonNull(networkSurveyRecord, "networkSurveyRecord mustn't be null");
    NetworkSurvey networkSurvey = new NetworkSurvey(networkSurveyRecord.getId());
    networkSurvey.setArfcn(networkSurveyRecord.getArfcn())
        .setBsic(networkSurveyRecord.getBsic())
        .setRxLev(networkSurveyRecord.getRxlev())
        .setBer(networkSurveyRecord.getBer())
        .setMcc(networkSurveyRecord.getMcc())
        .setMnc(networkSurveyRecord.getMnc())
        .setLac(networkSurveyRecord.getLac())
        .setCellId(networkSurveyRecord.getCellId())
        .setCellStatus(networkSurveyRecord.getCellStatus())
        .setAddTime(networkSurveyRecord.getAddTime())
        .setGsmChannel(networkSurveyRecord.getGsmChannel())
        .setTrusted(false)
        .setServerId(networkSurveyRecord.getServerId());
    Integer[] arfcns = networkSurveyRecord.getArfcns();
    if (arfcns != null) {
      networkSurvey.setArfcns(Arrays.asList(arfcns));
    }
    Integer[] neighboursArfcn = networkSurveyRecord.getNeighboursArfcn();
    if (neighboursArfcn != null) {
      networkSurvey.setNeighboursArfcn(Arrays.asList(neighboursArfcn));
    }
    return networkSurvey;
  }

  public static void mapNetworkSurveyToNetworkSurveyRecord(final NetworkSurvey networkSurvey, final NetworkSurveyRecord networkSurveyRecord) {
    networkSurveyRecord.setArfcn(networkSurvey.getArfcn())
        .setBsic(networkSurvey.getBsic())
        .setRxlev(networkSurvey.getRxLev())
        .setBer(networkSurvey.getBer())
        .setMcc(networkSurvey.getMcc())
        .setMnc(networkSurvey.getMnc())
        .setLac(networkSurvey.getLac())
        .setCellId(networkSurvey.getCellId())
        .setCellStatus(networkSurvey.getCellStatus())
        .setAddTime(networkSurvey.getAddTime())
        .setGsmChannel(networkSurvey.getGsmChannel())
        .setServerId(networkSurvey.getServerId());
    if (networkSurvey.getArfcns() != null) {
      networkSurveyRecord.setArfcns(networkSurvey.getArfcns().toArray(new Integer[networkSurvey.getArfcns().size()]));
    } else {
      networkSurveyRecord.setArfcns(null);
    }
    if (networkSurvey.getNeighboursArfcn() != null) {
      networkSurveyRecord.setNeighboursArfcn(networkSurvey.getNeighboursArfcn().toArray(new Integer[networkSurvey.getNeighboursArfcn().size()]));
    } else {
      networkSurveyRecord.setNeighboursArfcn(null);
    }
  }

}
