/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.VoiceServerConfig;
import com.flamesgroup.antrax.storage.commons.impl.ServerData;
import com.flamesgroup.storage.jooq.tables.records.ServerRecord;

import java.util.Objects;

public final class ServerDataMapper {

  private ServerDataMapper() {
  }

  public static IServerData mapServerRecordToServerData(final ServerRecord serverRecord) {
    Objects.requireNonNull(serverRecord, "serverRecord mustn't be null");
    IServerData serverData = new ServerData(serverRecord.getId());
    serverData.setName(serverRecord.getName());
    serverData.setPublicAddress(serverRecord.getPublicAddress());
    serverData.setSimServerEnabled(serverRecord.getSsEnable());
    serverData.setVoiceServerEnabled(serverRecord.getVsEnable());
    if (serverRecord.getVsEnable()) {
      serverData.setVoiceServerConfig(new VoiceServerConfig(serverRecord.getCallRouteConfig(), serverRecord.getSmsRouteConfig(), serverRecord.getAudioCapture()));
    }
    return serverData;
  }

  public static void mapServerDataToServerRecord(final ServerData server, final ServerRecord serverRecord) {
    Objects.requireNonNull(server, "server mustn't be null");
    Objects.requireNonNull(serverRecord, "serverRecord mustn't be null");

    serverRecord.setName(server.getName())
        .setId(server.getID())
        .setPublicAddress(server.getPublicAddress())
        .setSsEnable(server.isSimServerEnabled())
        .setVsEnable(server.isVoiceServerEnabled())
        .setSmsRouteConfig(server.isVoiceServerEnabled() ? server.getVoiceServerConfig().getSmsRouteConfig() : null)
        .setCallRouteConfig(server.isVoiceServerEnabled() ? server.getVoiceServerConfig().getCallRouteConfig() : null)
        .setAudioCapture(server.isVoiceServerEnabled() ? server.getVoiceServerConfig().isAudioCaptureEnabled() : null);
  }

}
