/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamBlackListNumbersRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamGrayListNumbersRecord;
import com.flamesgroup.storage.jooq.tables.records.VoipAntiSpamWhiteListNumbersRecord;

import java.util.Objects;

public final class VoipAntiSpamStatisticMapper {

  private VoipAntiSpamStatisticMapper() {
  }

  public static WhiteListNumber mapVoipAntiSpamWhiteListNumbersRecordToWhiteListNumbers(final VoipAntiSpamWhiteListNumbersRecord voipAntiSpamWhiteListNumbersRecord) {
    Objects.requireNonNull(voipAntiSpamWhiteListNumbersRecord, "voipAntiSpamWhiteListNumbersRecord mustn't be null");
    return new WhiteListNumber(voipAntiSpamWhiteListNumbersRecord.getNumberType(), voipAntiSpamWhiteListNumbersRecord.getNumber(), voipAntiSpamWhiteListNumbersRecord.getStatus(),
        voipAntiSpamWhiteListNumbersRecord.getStatusDescription(), voipAntiSpamWhiteListNumbersRecord.getRoutingRequestCount(), voipAntiSpamWhiteListNumbersRecord.getAddTime());
  }

  public static void mapWhiteListNumberToVoipAntiSpamWhiteListNumbersRecord(final WhiteListNumber whiteListNumber, final VoipAntiSpamWhiteListNumbersRecord voipAntiSpamWhiteListNumbersRecord) {
    Objects.requireNonNull(whiteListNumber, "whiteListNumber mustn't be null");
    Objects.requireNonNull(voipAntiSpamWhiteListNumbersRecord, "voipAntiSpamWhiteListNumbersRecord mustn't be null");
    voipAntiSpamWhiteListNumbersRecord
        .setNumberType(whiteListNumber.getNumberType())
        .setNumber(whiteListNumber.getNumber())
        .setStatus(whiteListNumber.getStatus())
        .setStatusDescription(whiteListNumber.getStatusDescription())
        .setRoutingRequestCount(whiteListNumber.getRoutingRequestCount())
        .setAddTime(whiteListNumber.getAddTime());
  }

  public static GrayListNumber mapVoipAntiSpamGrayListNumbersRecordToGrayListNumbers(final VoipAntiSpamGrayListNumbersRecord voipAntiSpamGrayListNumbersRecord) {
    Objects.requireNonNull(voipAntiSpamGrayListNumbersRecord, "voipAntiSpamGrayListNumbersRecord mustn't be null");
    return new GrayListNumber(voipAntiSpamGrayListNumbersRecord.getNumberType(), voipAntiSpamGrayListNumbersRecord.getNumber(), voipAntiSpamGrayListNumbersRecord.getStatus(),
        voipAntiSpamGrayListNumbersRecord.getStatusDescription(), voipAntiSpamGrayListNumbersRecord.getRoutingRequestCount(), voipAntiSpamGrayListNumbersRecord.getBlockTime(),
        voipAntiSpamGrayListNumbersRecord.getBlockCount(), voipAntiSpamGrayListNumbersRecord.getBlockTimeLeft());
  }

  public static void mapGrayListNumberToVoipAntiSpamGrayListNumbersRecord(final GrayListNumber grayListNumber, final VoipAntiSpamGrayListNumbersRecord voipAntiSpamGrayListNumbersRecord) {
    Objects.requireNonNull(grayListNumber, "grayListNumber mustn't be null");
    Objects.requireNonNull(voipAntiSpamGrayListNumbersRecord, "voipAntiSpamGrayListNumbersRecord mustn't be null");
    voipAntiSpamGrayListNumbersRecord
        .setNumberType(grayListNumber.getNumberType())
        .setNumber(grayListNumber.getNumber())
        .setStatus(grayListNumber.getStatus())
        .setStatusDescription(grayListNumber.getStatusDescription())
        .setRoutingRequestCount(grayListNumber.getRoutingRequestCount())
        .setBlockTime(grayListNumber.getBlockTime())
        .setBlockCount(grayListNumber.getBlockCount())
        .setBlockTimeLeft(grayListNumber.getBlockTimeLeft());
  }

  public static BlackListNumber mapVoipAntiSpamBlackListNumbersRecordToBlackListNumbers(final VoipAntiSpamBlackListNumbersRecord voipAntiSpamBlackListNumbersRecord) {
    Objects.requireNonNull(voipAntiSpamBlackListNumbersRecord, "voipAntiSpamBlackListNumbersRecord mustn't be null");
    return new BlackListNumber(voipAntiSpamBlackListNumbersRecord.getNumberType(), voipAntiSpamBlackListNumbersRecord.getNumber(), voipAntiSpamBlackListNumbersRecord.getStatus(),
        voipAntiSpamBlackListNumbersRecord.getStatusDescription(), voipAntiSpamBlackListNumbersRecord.getRoutingRequestCount(), voipAntiSpamBlackListNumbersRecord.getAddTime());
  }

  public static void mapBlackListNumberToVoipAntiSpamBlackListNumbersRecord(final BlackListNumber blackListNumber, final VoipAntiSpamBlackListNumbersRecord voipAntiSpamBlackListNumbersRecord) {
    Objects.requireNonNull(blackListNumber, "blackListNumber mustn't be null");
    Objects.requireNonNull(voipAntiSpamBlackListNumbersRecord, "voipAntiSpamBlackListNumbersRecord mustn't be null");
    voipAntiSpamBlackListNumbersRecord
        .setNumberType(blackListNumber.getNumberType())
        .setNumber(blackListNumber.getNumber())
        .setStatus(blackListNumber.getStatus())
        .setRoutingRequestCount(blackListNumber.getRoutingRequestCount())
        .setStatusDescription(blackListNumber.getStatusDescription())
        .setAddTime(blackListNumber.getAddTime());
  }

}
