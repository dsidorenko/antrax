/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerBasicAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.storage.jooq.tables.records.CellEqualizerConfigurationRecord;
import com.google.gson.Gson;

import java.util.Objects;

public final class CellEqualizerMapper {

  private CellEqualizerMapper() {
  }

  public static void cellEqualizerBasicAlgorithmToCellEqualizerConfigurationRecord(final CellEqualizerBasicAlgorithm cellEqualizerBasicAlgorithm,
      final CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord) {
    Objects.requireNonNull(cellEqualizerBasicAlgorithm, "cellEqualizerBasicAlgorithm mustn't be null");
    Objects.requireNonNull(cellEqualizerConfigurationRecord, "cellEqualizerConfigurationRecord mustn't be null");
    cellEqualizerConfigurationRecord.setServerId(cellEqualizerBasicAlgorithm.getServerId());
    cellEqualizerConfigurationRecord.setClassName(cellEqualizerBasicAlgorithm.getClass().getName());
  }

  public static CellEqualizerAlgorithm cellEqualizerConfigurationRecordToCellEqualizerAlgorithm(final CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord) {
    Objects.requireNonNull(cellEqualizerConfigurationRecord, "cellEqualizerConfigurationRecord mustn't be null");
    if (cellEqualizerConfigurationRecord.getClassName().equals(CellEqualizerBasicAlgorithm.class.getName())) {
      CellEqualizerBasicAlgorithm equalizerBasicAlgorithm = new CellEqualizerBasicAlgorithm(cellEqualizerConfigurationRecord.getId());
      equalizerBasicAlgorithm.setServerId(cellEqualizerConfigurationRecord.getServerId());
      return equalizerBasicAlgorithm;
    } else if (cellEqualizerConfigurationRecord.getClassName().equals(CellEqualizerRandomAlgorithm.class.getName())) {
      CellEqualizerRandomAlgorithm cellEqualizerRandomAlgorithm = new CellEqualizerRandomAlgorithm(cellEqualizerConfigurationRecord.getId());
      cellEqualizerRandomAlgorithm.setServerId(cellEqualizerConfigurationRecord.getServerId());
      cellEqualizerRandomAlgorithm.setConfiguration(new Gson().fromJson(cellEqualizerConfigurationRecord.getConfiguration(),
          CellEqualizerRandomAlgorithm.CellEqualizerRandomConfiguration.class));
      return cellEqualizerRandomAlgorithm;
    } else if (cellEqualizerConfigurationRecord.getClassName().equals(CellEqualizerStatisticAlgorithm.class.getName())) {
      CellEqualizerStatisticAlgorithm cellEqualizerStatisticAlgorithm = new CellEqualizerStatisticAlgorithm(cellEqualizerConfigurationRecord.getId());
      cellEqualizerStatisticAlgorithm.setServerId(cellEqualizerConfigurationRecord.getServerId());
      cellEqualizerStatisticAlgorithm.setConfiguration(new Gson().fromJson(cellEqualizerConfigurationRecord.getConfiguration(),
          CellEqualizerStatisticAlgorithm.CellEqualizerStatisticConfiguration.class));
      return cellEqualizerStatisticAlgorithm;
    } else {
      throw new IllegalArgumentException("Can't find algorithm: " + cellEqualizerConfigurationRecord.getClassName());
    }
  }

  public static void cellEqualizerRandomAlgorithmToCellEqualizerConfigurationRecord(final CellEqualizerRandomAlgorithm cellEqualizerRandomAlgorithm,
      final CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord) {
    Objects.requireNonNull(cellEqualizerRandomAlgorithm, "cellEqualizerRandomAlgorithm mustn't be null");
    Objects.requireNonNull(cellEqualizerConfigurationRecord, "cellEqualizerConfigurationRecord mustn't be null");
    cellEqualizerConfigurationRecord.setServerId(cellEqualizerRandomAlgorithm.getServerId());
    cellEqualizerConfigurationRecord.setClassName(cellEqualizerRandomAlgorithm.getClass().getName());
    cellEqualizerConfigurationRecord.setConfiguration(new Gson().toJsonTree(cellEqualizerRandomAlgorithm.getConfiguration()));
  }

  public static void cellEqualizerStatisticAlgorithmToCellEqualizerConfigurationRecord(final CellEqualizerStatisticAlgorithm cellEqualizerStatisticAlgorithm,
      final CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord) {
    Objects.requireNonNull(cellEqualizerStatisticAlgorithm, "cellEqualizerStatisticAlgorithm mustn't be null");
    Objects.requireNonNull(cellEqualizerConfigurationRecord, "cellEqualizerConfigurationRecord mustn't be null");
    cellEqualizerConfigurationRecord.setServerId(cellEqualizerStatisticAlgorithm.getServerId());
    cellEqualizerConfigurationRecord.setClassName(cellEqualizerStatisticAlgorithm.getClass().getName());
    cellEqualizerConfigurationRecord.setConfiguration(new Gson().toJsonTree(cellEqualizerStatisticAlgorithm.getConfiguration()));
  }

}
