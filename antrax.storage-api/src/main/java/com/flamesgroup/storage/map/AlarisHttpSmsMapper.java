/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.storage.jooq.tables.records.AlarisHttpSmsRecord;

import java.util.Objects;

public final class AlarisHttpSmsMapper {

  private AlarisHttpSmsMapper() {
  }

  public static void mapAlarisHttpSmsToAlarisHttpSmsRecord(final AlarisHttpSms alarisSms, final AlarisHttpSmsRecord alarisSmsRecord) {
    Objects.requireNonNull(alarisSms, "alarisSms mustn't be null");
    Objects.requireNonNull(alarisSmsRecord, "alarisSmsRecord mustn't be null");
    alarisSmsRecord.setId(alarisSms.getId())
        .setAni(alarisSms.getSourceNumber())
        .setDnis(alarisSms.getDestNumber())
        .setMessage(alarisSms.getMessage())
        .setLongMessageMode(alarisSms.getLongMessageMode())
        .setServiceType(alarisSms.getServiceType())
        .setStatus(alarisSms.getStatus())
        .setDataCoding(alarisSms.getDataCoding())
        .setCreateTime(alarisSms.getCreateTime())
        .setSmsId(alarisSms.getSmsId())
        .setLastUpdateTime(alarisSms.getLastUpdateTime())
        .setDlrUrl(alarisSms.getDlrUrl())
        .setDlrTime(alarisSms.getDlrTime())
        .setUsername(alarisSms.getUsername())
        .setClientIp(alarisSms.getClientIp());
    if (alarisSms.getSar() != null) {
      alarisSmsRecord.setSarId(alarisSms.getSar().getId());
      alarisSmsRecord.setSarParts(alarisSms.getSar().getParts());
      alarisSmsRecord.setSarPartNumber(alarisSms.getSar().getPartNumber());
    }
  }

  public static AlarisHttpSms mapAlarisSmsHttpRecordToAlarisHttpSms(final AlarisHttpSmsRecord alarisSmsRecord) {
    Objects.requireNonNull(alarisSmsRecord, "alarisSmsRecord mustn't be null");
    AlarisSms.Sar sar = null;
    if (alarisSmsRecord.getSarId() != null) {
      sar = new AlarisSms.Sar(alarisSmsRecord.getSarId(), alarisSmsRecord.getSarParts(), alarisSmsRecord.getSarPartNumber());
    }
    AlarisHttpSms alarisSms = new AlarisHttpSms(alarisSmsRecord.getId(),
        alarisSmsRecord.getAni(),
        alarisSmsRecord.getDnis(),
        alarisSmsRecord.getMessage(),
        alarisSmsRecord.getServiceType(),
        alarisSmsRecord.getLongMessageMode(),
        alarisSmsRecord.getDataCoding(),
        alarisSmsRecord.getCreateTime(),
        sar,
        alarisSmsRecord.getDlrUrl(),
        alarisSmsRecord.getUsername(),
        alarisSmsRecord.getClientIp());

    alarisSms.setSmsId(alarisSmsRecord.getSmsId());
    alarisSms.setLastUpdateTime(alarisSmsRecord.getLastUpdateTime());
    alarisSms.setStatus(alarisSmsRecord.getStatus());
    alarisSms.setDlrTime(alarisSmsRecord.getDlrTime());
    return alarisSms;
  }

}
