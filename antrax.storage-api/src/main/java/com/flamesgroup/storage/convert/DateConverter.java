/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import org.jooq.Converter;

import java.sql.Timestamp;
import java.util.Date;

public final class DateConverter implements Converter<Timestamp, Date> {

  private static final long serialVersionUID = -181788453497502464L;

  @Override
  public Date from(final Timestamp databaseObject) {
    return databaseObject == null ? null : new Date(databaseObject.getTime());
  }

  @Override
  public Timestamp to(final Date userObject) {
    return userObject == null ? null : new Timestamp(userObject.getTime());
  }

  @Override
  public Class<Timestamp> fromType() {
    return Timestamp.class;
  }

  @Override
  public Class<Date> toType() {
    return Date.class;
  }

}
