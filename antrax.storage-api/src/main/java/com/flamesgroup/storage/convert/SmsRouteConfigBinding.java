/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import com.flamesgroup.commons.SmsRouteConfig;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.impl.DSL;

import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Types;
import java.util.Objects;

public final class SmsRouteConfigBinding implements Binding<Object, SmsRouteConfig> {

  private static final long serialVersionUID = 715626910707692722L;

  @Override
  public Converter<Object, SmsRouteConfig> converter() {
    return new RouteConfigConverter();
  }

  @Override
  public void sql(final BindingSQLContext<SmsRouteConfig> ctx) throws SQLException {
    ctx.render().visit(DSL.val(ctx.convert(converter()).value())).sql("::json");
  }

  @Override
  public void register(final BindingRegisterContext<SmsRouteConfig> ctx) throws SQLException {
    ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR);
  }

  @Override
  public void set(final BindingSetStatementContext<SmsRouteConfig> ctx) throws SQLException {
    ctx.statement().setString(ctx.index(), Objects.toString(ctx.convert(converter()).value(), null));
  }

  @Override
  public void set(final BindingSetSQLOutputContext<SmsRouteConfig> ctx) throws SQLException {
    throw new SQLFeatureNotSupportedException();
  }

  @Override
  public void get(final BindingGetResultSetContext<SmsRouteConfig> ctx) throws SQLException {
    ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()));
  }

  @Override
  public void get(final BindingGetStatementContext<SmsRouteConfig> ctx) throws SQLException {
    ctx.convert(converter()).value(ctx.statement().getString(ctx.index()));
  }

  @Override
  public void get(final BindingGetSQLInputContext<SmsRouteConfig> ctx) throws SQLException {
    throw new SQLFeatureNotSupportedException();
  }

  private static class RouteConfigConverter implements Converter<Object, SmsRouteConfig> {

    private static final long serialVersionUID = -6753952225890483194L;

    @Override
    public SmsRouteConfig from(final Object databaseObject) {
      try {
        return databaseObject == null ? null : new Gson().fromJson("" + databaseObject, SmsRouteConfig.class);
      } catch (JsonSyntaxException ignored) {
        return null;
      }
    }

    @Override
    public String to(final SmsRouteConfig userObject) {
      return userObject == null ? null : new Gson().toJson(userObject);
    }

    @Override
    public Class<Object> fromType() {
      return Object.class;
    }

    @Override
    public Class<SmsRouteConfig> toType() {
      return SmsRouteConfig.class;
    }

  }

}
