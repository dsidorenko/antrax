/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

public interface IAlarisHttpSmsDAO {

  void insertSms(AlarisHttpSms alarisSms) throws DataModificationException;

  void updateSms(AlarisHttpSms alarisSms) throws DataModificationException;

  List<AlarisHttpSms> listSmsAtRouting();

  List<AlarisHttpSms> listSmsDlr(EnumSet<AlarisHttpSms.Status> dlrStatus);

  int getCountOfSendSmsToday(String clientIp);

  AlarisHttpSms getBySmsIdAndPartNumber(UUID smsId, final int numberOfSmsPart);

  AlarisHttpSms getById(UUID id);

  List<AlarisHttpSms> getEdr(Date fromDate, Date toDate, String source, String destination, String clientIp, AlarisSms.Status status);

}
