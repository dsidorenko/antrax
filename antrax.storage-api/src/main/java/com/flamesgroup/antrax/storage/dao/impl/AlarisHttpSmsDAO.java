/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;


import static com.flamesgroup.storage.jooq.Tables.ALARIS_HTTP_SMS;
import static com.flamesgroup.storage.jooq.Tables.ALARIS_SMPP_SMS;

import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IAlarisHttpSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.tables.records.AlarisHttpSmsRecord;
import com.flamesgroup.storage.jooq.tables.records.AlarisSmppSmsRecord;
import com.flamesgroup.storage.map.AlarisHttpSmsMapper;
import com.flamesgroup.storage.map.AlarisSmppSmsMapper;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AlarisHttpSmsDAO implements IAlarisHttpSmsDAO {

  private static final Logger logger = LoggerFactory.getLogger(AlarisHttpSmsDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public AlarisHttpSmsDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void insertSms(final AlarisHttpSms alarisHttpSms) throws DataModificationException {
    logger.debug("[{}] - trying to insert alarisHttpSms: [{}]", this, alarisHttpSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisHttpSmsRecord alarisHttpSmsRecord = context.newRecord(ALARIS_HTTP_SMS);
        AlarisHttpSmsMapper.mapAlarisHttpSmsToAlarisHttpSmsRecord(alarisHttpSms, alarisHttpSmsRecord);

        int insert = alarisHttpSmsRecord.insert();
        if (insert == 1) {
          logger.debug("[{}] - inserted alarisHttpSms: [{}]", this, alarisHttpSms);
        } else {
          logger.debug("[{}] - can't insert alarisHttpSms: [{}]", this, alarisHttpSms);
          throw new DataModificationException(String.format("[%s] - can't insert alarisHttpSms: [%s]", this, alarisHttpSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while insert alarisHttpSms [{}]", this, alarisHttpSms, e);
      throw new DataModificationException(String.format("[%s] - execution error, while insert alarisHttpSms: [%s]", this, alarisHttpSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateSms(final AlarisHttpSms alarisHttpSms) throws DataModificationException {
    logger.debug("[{}] - trying to update alarisHttpSms: [{}]", this, alarisHttpSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisHttpSmsRecord alarisHttpSmsRecord = context.newRecord(ALARIS_HTTP_SMS);
        AlarisHttpSmsMapper.mapAlarisHttpSmsToAlarisHttpSmsRecord(alarisHttpSms, alarisHttpSmsRecord);

        int update = alarisHttpSmsRecord.update();
        if (update == 1) {
          logger.debug("[{}] - updated alarisHttpSms: [{}]", this, alarisHttpSms);
        } else {
          logger.debug("[{}] - can't update alarisHttpSms: [{}]", this, alarisHttpSms);
          throw new DataModificationException(String.format("[%s] - execution error, while update alarisHttpSms: [%s]", this, alarisHttpSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while update alarisHttpSms", this, e);
      throw new DataModificationException(String.format("[%s] - execution error, while update alarisHttpSms: [%s]", this, alarisHttpSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisHttpSms> listSmsAtRouting() {
    logger.debug("[{}] - trying to select alarisHttpSms at routing", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.STATUS.eq(AlarisHttpSms.Status.ENROUTE));

        return selectQuery.fetchInto(AlarisHttpSmsRecord.class).stream().map(AlarisHttpSmsMapper::mapAlarisSmsHttpRecordToAlarisHttpSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of alarisHttpSms at routing", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisHttpSms> listSmsDlr(final EnumSet<AlarisHttpSms.Status> dlrStatus) {
    logger.debug("[{}] - trying to select alarisHttpSms dlr to delivery", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.DLR_URL.isNotNull().and(ALARIS_HTTP_SMS.DLR_TIME.isNull().and(ALARIS_HTTP_SMS.STATUS.in(dlrStatus))));

        return selectQuery.fetchInto(AlarisHttpSmsRecord.class).stream().map(AlarisHttpSmsMapper::mapAlarisSmsHttpRecordToAlarisHttpSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of alarisHttpSms dlr to delivery", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfSendSmsToday(final String clientIp) {
    logger.debug("[{}] - trying to select count of alarisHttpSms for today", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.CREATE_TIME.greaterOrEqual(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())));
        if (clientIp != null) {
          selectQuery.addConditions(ALARIS_HTTP_SMS.CLIENT_IP.eq(clientIp));
        }
        return context.fetchCount(selectQuery);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting count of alarisHttpSms for today", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisHttpSms getBySmsIdAndPartNumber(final UUID smsId, final int numberOfSmsPart) {
    logger.debug("[{}] - trying to select alarisHttpSms by smsId: [{}]", this, smsId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.SMS_ID.eq(smsId));
        selectQuery.addConditions(ALARIS_HTTP_SMS.SAR_PART_NUMBER.isNull().or(ALARIS_HTTP_SMS.SAR_PART_NUMBER.eq(numberOfSmsPart)));

        AlarisHttpSmsRecord alarisHttpSmsRecord = selectQuery.fetchOne();
        return alarisHttpSmsRecord == null ? null : AlarisHttpSmsMapper.mapAlarisSmsHttpRecordToAlarisHttpSms(alarisHttpSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisHttpSms by smsId: [{}]", this, smsId, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisHttpSms getById(final UUID id) {
    logger.debug("[{}] - trying to select alarisHttpSms by id: [{}]", this, id);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.ID.eq(id));

        AlarisHttpSmsRecord alarisHttpSmsRecord = selectQuery.fetchOne();
        return alarisHttpSmsRecord == null ? null : AlarisHttpSmsMapper.mapAlarisSmsHttpRecordToAlarisHttpSms(alarisHttpSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisHttpSms by id: [{}]", this, id, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisHttpSms> getEdr(final Date fromDate, final Date toDate, final String ani, final String dnis, final String clientIp, final AlarisSms.Status status) {
    logger.debug("[{}] - trying to select edr for alarisHttpSms", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisHttpSmsRecord> selectQuery = context.selectQuery(ALARIS_HTTP_SMS);
        selectQuery.addConditions(ALARIS_HTTP_SMS.CREATE_TIME.between(fromDate, toDate));
        if (ani != null && !ani.isEmpty()) {
          selectQuery.addConditions(ALARIS_HTTP_SMS.ANI.eq(ani));
        }
        if (dnis != null && !dnis.isEmpty()) {
          selectQuery.addConditions(ALARIS_HTTP_SMS.DNIS.eq(dnis));
        }
        if (clientIp != null && !clientIp.isEmpty()) {
          selectQuery.addConditions(ALARIS_HTTP_SMS.CLIENT_IP.eq(clientIp));
        }
        if (status != null) {
          selectQuery.addConditions(ALARIS_HTTP_SMS.STATUS.eq(status));
        }

        return selectQuery.fetchInto(AlarisHttpSmsRecord.class).stream().map(AlarisHttpSmsMapper::mapAlarisSmsHttpRecordToAlarisHttpSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting edr for alarisHttpSms", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

}
