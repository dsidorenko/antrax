/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.enums.SIMEvent;
import com.flamesgroup.unit.ICCID;

public interface ISIMHistoryDAO {

  /**
   * If event was inserted into db, then returned id of the record. If error
   * was occurred then it throws IOException.
   *
   * @param simUID
   * @param event
   * @param args
   * @return simCoreEventID or 0
   */
  long simEventOccurred(ICCID simUID, SIMEvent event, long startTime, String[] args) throws SQLConnectionException, UpdateFailedException;

  /**
   * Returns a list of sim events, selected from sim event tree on level 0.
   */
  SIMEventRec[] listSIMEventsByUid(ICCID simUid, long startTime, long endTime, int limit);

}
