/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.unit.ICCID;

public interface ISimpleConfigViewDAO {

  byte[] getSessionScript(ICCID uid);

  byte[] getActivityPeriodScript(ICCID uid);

  byte[] getFactorScript(ICCID uid);

  byte[] getBusinessActivityScript(ICCID uid);

  byte[] getIncomingCallManagementScript(ICCID simUID);

  byte[] getGwSelectorScript(ICCID uid);

  byte[] getVSFactorScript(ICCID simUID);

  byte[] getActionProviderScript(ICCID simUID);

  byte[] getCallFilterScript(ICCID uid);

  byte[] getSmsFilterScript(ICCID uid);

  byte[] getIMEIGeneratorScript(ICCID uid);

  ICCID[] findSIMList(SimSearchingParams params);

  boolean shouldResetVsScripts(ICCID uid);

  boolean shouldResetSsScripts(ICCID uid);

}
