/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CdrAnalyzeNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.util.List;
import java.util.Set;

import javax.swing.*;

public interface IVoipAntiSpamDAO {

  /*
    Configuration
   */

  void upsertVoipAntiSpamConfiguration(VoipAntiSpamConfiguration voipAntiSpamConfiguration) throws DataModificationException;

  VoipAntiSpamConfiguration getVoipAntiSpamConfiguration();

  /*
    White List
  */

  void insertVoipAntiSpamWhiteListNumbers(Set<WhiteListNumber> whiteListNumbers) throws DataModificationException;

  boolean incrementRoutingRequestWhiteNumber(VoipAntiSpamNumberType numberType, String number) throws DataModificationException;

  void deleteVoipAntiSpamWhiteListNumbers(VoipAntiSpamNumberType numberType, Set<String> whiteListNumbers) throws DataModificationException;

  void deleteAllVoipAntiSpamWhiteListNumbers(VoipAntiSpamNumberType numberType) throws DataModificationException;

  List<WhiteListNumber> listWhiteListNumbers(VoipAntiSpamNumberType numberType, String search, Pair<String, SortOrder> sort, int offset, int limit);

  int getCountOfWhiteListNumbers(VoipAntiSpamNumberType numberType, String search);

  /*
    Gray List
  */

  GrayListNumber incrementRoutingRequestGrayNumber(VoipAntiSpamNumberType numberType, String number) throws DataModificationException;

  void insertVoipAntiSpamGrayListNumbers(Set<GrayListNumber> grayListNumbers) throws DataModificationException;

  void deleteAllVoipAntiSpamGrayListNumbers(VoipAntiSpamNumberType numberType) throws DataModificationException;

  void deleteVoipAntiSpamGrayListNumbers(VoipAntiSpamNumberType numberType, Set<String> grayListNumbers) throws DataModificationException;

  GrayListNumber getGrayListNumber(VoipAntiSpamNumberType numberType, String number);

  List<GrayListNumber> listGrayListNumbers(VoipAntiSpamNumberType numberType, String search, Pair<String, SortOrder> sort, int offset, int limit);

  int getCountOfGrayListNumbers(VoipAntiSpamNumberType numberType, String search);

  /*
    Black List
  */

  void insertVoipAntiSpamBlackListNumbers(Set<BlackListNumber> blackListNumbers) throws DataModificationException;

  boolean incrementRoutingRequestBlackNumber(VoipAntiSpamNumberType numberType, String number) throws DataModificationException;

  void deleteAllVoipAntiSpamBlackListNumbers(VoipAntiSpamNumberType numberType) throws DataModificationException;

  void deleteVoipAntiSpamBlackListNumbers(VoipAntiSpamNumberType numberType, Set<String> blackListNumbers) throws DataModificationException;

  BlackListNumber getBlackListNumber(VoipAntiSpamNumberType numberType, String number);

  List<BlackListNumber> listBlackListNumbers(VoipAntiSpamNumberType numberType, String search, Pair<String, SortOrder> sort, int offset, int limit);

  int getCountOfBlackListNumbers(VoipAntiSpamNumberType numberType, String search);

  /*
    History routing request
  */

  void insertHistoryRoutingRequest(String callerNumber, String calledNumber, long time) throws DataModificationException;

  void clearHistoryRoutingRequest() throws DataModificationException;

  int getRoutingCount(VoipAntiSpamNumberType numberType, String number, long period);

  int getRoutingCount(String callerNumber, String calledNumber, long period);

  List<VoipAntiSpamPlotData> getPlotDataForRoutingRequest(VoipAntiSpamNumberType numberType);

  /*
    History block numbers
  */

  void insertHistoryBlockNumbers(VoipAntiSpamNumberType numberType, String number, long time) throws DataModificationException;

  void clearHistoryBlockNumbers() throws DataModificationException;

  List<VoipAntiSpamPlotData> getPlotDataForBlockNumbers(VoipAntiSpamNumberType numberType);

  /*
    CDR analyze numbers
  */

  List<CdrAnalyzeNumber> getCdrAnalyzeNumbers(VoipAntiSpamNumberType numberType, int count, long time);


}
