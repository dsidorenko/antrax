/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.GSM_CHANNEL;

import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.storage.jooq.Tables;
import com.flamesgroup.storage.jooq.tables.records.GsmChannelRecord;
import com.flamesgroup.storage.jooq.tables.records.SimRecord;
import com.flamesgroup.storage.jooq.tables.records.SimSerializableDataRecord;
import com.flamesgroup.storage.map.GsmChannelMapper;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.UpdateQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

public class SimpleConfigEditDAO implements ISimpleConfigEditDAO {

  private final Logger logger = LoggerFactory.getLogger(SimpleConfigEditDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public SimpleConfigEditDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void enableSIM(final ICCID iccid, final boolean enabled) {
    logger.debug("[{}] - trying to update simEnabledState(ICCID: [{}], enabled: [{}])", this, iccid, enabled);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update simEnabledState to [{}] (key is not valid: [{}])", this, enabled, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.ENABLED, enabled);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          String state = "DISABLED";
          if (enabled) {
            state = "ENABLED";
          }
          logger.info("[{}] - SIM: [{}] was {}", this, iccid, state);
        } else {
          logger.warn("[{}] - can't update simEnabledState (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating simEnabledState for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void lockSIM(final ICCID iccid, final boolean locked, final String reason) {
    logger.debug("[{}] - trying to update simLockState (ICCID: [{}], locked: [{}], reason: [{}])",
        this, iccid, locked, reason);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update simLockState to [{}] (key is not valid: [{}])", this, locked, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.LOCKED, locked);
        updateQuery.addValue(Tables.SIM.LOCK_REASON, reason);
        updateQuery.addValue(Tables.SIM.LOCK_TIME, locked ? new Date() : null);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          String state = "UNLOCKED";
          if (locked) {
            state = "LOCKED";
          }
          logger.info("[{}] - SIM: [{}] was {}", this, iccid, state);
        } else {
          logger.warn("[{}] - can't update simLockState (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating simLockState for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void increaseSIMCalledTime(final ICCID iccid, final long duration) {
    logger.trace("[{}] - trying to increase callDuration (ICCID: [{}], duration: [{}])", this, iccid, duration);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty() || duration <= 0) {
      logger.warn("[{}] - can't increase simCallDuration on: [{}], for sim: [{}]", this, duration, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.SUCCESSFUL_CALL_DURATION, Tables.SIM.SUCCESSFUL_CALL_DURATION.add(duration));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - callDuration was increased on: [{}] for sim: [{}]", this, duration, iccid);
        } else {
          logger.warn("[{}] - can't increase callDuration (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while increasing callDuration for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMAllCallCount(final ICCID iccid) {
    logger.trace("[{}] - trying to increment allCallCount (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment allCallCount (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.ALL_CALL_COUNT, Tables.SIM.ALL_CALL_COUNT.add(1));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - allCallCount was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment allCallCount (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing allCallCount for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMSuccessfulCallCount(final ICCID iccid) {
    logger.trace("[{}] - trying to increment successfulCallCount (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment successfulCallCount (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.SUCCESSFUL_CALL_COUNT, Tables.SIM.SUCCESSFUL_CALL_COUNT.add(1));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - successfulCallCount was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment successfulCallCount (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing successfulCallCount for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMSMSCount(final ICCID iccid, final int totalSmsParts, final int successSendSmsParts) {
    logger.trace("[{}] - trying to increment sms count (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment sms count (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.TOTAL_SMS_COUNT, Tables.SIM.TOTAL_SMS_COUNT.add(totalSmsParts));
        updateQuery.addValue(Tables.SIM.SUCCESS_SMS_COUNT, Tables.SIM.SUCCESS_SMS_COUNT.add(successSendSmsParts));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - sms count was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment sms count (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing sms count for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMSMSStatuses(final ICCID iccid, final int totalSmsStatuses) {
    logger.trace("[{}] - trying to increment sms statuses, ICCID: [{}]", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment sms count (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.TOTAL_SMS_STATUSES, Tables.SIM.TOTAL_SMS_STATUSES.add(totalSmsStatuses));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - sms statuses was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment sms statuses (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing sms statuses for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMIncomingSMSCount(final ICCID iccid, final int parts) {
    logger.trace("[{}] - trying to increment incoming sms count (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment incoming sms count (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.INCOMING_SMS_COUNT, Tables.SIM.INCOMING_SMS_COUNT.add(parts));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - incoming sms count was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment incoming sms count (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing incoming sms count for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void saveScript(final ICCID iccid, final ScriptType scriptType, final byte[] script) {
    logger.debug("[{}] - trying to save {} (ICCID: [{}])", this, scriptType, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty() || script == null) {
      logger.warn("[{}] - can't save {} (key is not valid: [{}] or script is null)", this, scriptType, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimSerializableDataRecord> updateQuery = context.updateQuery(Tables.SIM_SERIALIZABLE_DATA);
        updateQuery.addConditions(Tables.SIM_SERIALIZABLE_DATA.ICCID.eq(iccid));
        switch (scriptType) {
          case ACTIVITY_PERIOD_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.ACTIVITY_PERIOD_SCRIPT, script);
            break;
          case FACTOR_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.FACTOR_SCRIPT, script);
            break;
          case VS_FACTOR_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.VS_FACTOR_SCRIPT, script);
            break;
          case BUSINESS_ACTIVITY_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.BUSINESS_ACTIVITY_SCRIPT, script);
            break;
          case GW_SELECTOR_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.GW_SELECTOR_SCRIPT, script);
            break;
          case SESSION_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.SESSION_SCRIPT, script);
            break;
          case ACTION_PROVIDER_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.ACTION_PROVIDER_SCRIPT, script);
            break;
          case INCOMING_CALL_MANAGEMENT_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.INCOMING_CALL_MANAGEMENT_SCRIPT, script);
            break;
          case CALL_FILTER_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.CALL_FILTER_SCRIPT, script);
            break;
          case SMS_FILTER_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.SMS_FILTER_SCRIPT, script);
            break;
          case IMEI_GENERATOR_SCRIPT:
            updateQuery.addValue(Tables.SIM_SERIALIZABLE_DATA.IMEI_GENERATOR_SCRIPT, script);
            break;
        }
        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - [{}] was updated for sim: [{}]", this, scriptType, iccid);
        } else {
          logger.warn("[{}] - can't update [{}] (result after execution is < 1)", this, scriptType);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating [{}] for sim: [{}]", this, scriptType, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean ensureSIMUIDIsPresent(final ICCID iccid) {
    logger.debug("[{}] - trying to ensure that ICCID: [{}] is present in db", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't ensure that ICCID is present in db (key is not valid: [{}])", this, iccid);
      return false;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addSelect(Tables.SIM.ID);
        selectQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        logger.trace("[{}] - trying to select sim_id by ICCID : [{}]", this, key);
        Long id = selectQuery.fetchOne(Tables.SIM.ID);
        if (id == null) {
          logger.warn("[{}] - can't find simID by ICCID: [{}]", this, key);
        } else {
          logger.trace("[{}] - simID: [{}] found by ICCID: [{}]", this, id, key);
          logger.debug("[{}] - sim: [{}] is present in db", this, iccid);
          return false;
        }

        SimRecord simRecord = context.newRecord(Tables.SIM);
        simRecord.setIccid(iccid);

        int res1 = simRecord.store();

        SimSerializableDataRecord simSerializableDataRecord = context.newRecord(Tables.SIM_SERIALIZABLE_DATA);
        simSerializableDataRecord.setIccid(iccid);

        int res2 = simSerializableDataRecord.store();
        if (res1 > 0 && res2 > 0) {
          logger.debug("[{}] - sim: [{}] inserted to db", this, iccid);
        } else {
          logger.warn("[{}] - can't insert sim [{}] to db (result after execution < 1)", this, iccid);
        }
        return true;
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while ensuring that ICCID : [{}] is present in db", this, iccid, e);
      return false;
    } finally {
      connection.close();
    }
  }

  @Override
  public boolean isIMEIPresent(final ICCID iccid) {
    logger.debug("[{}] - trying to ensure that imei is present in sim: [{}]", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't ensure that imei is present in sim (key is not valid: [{}])", this, iccid);
      return false;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<SimRecord> selectQuery = context.selectQuery(Tables.SIM);
        selectQuery.addSelect(Tables.SIM.IMEI);

        selectQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        IMEI imei = selectQuery.fetchOne(Tables.SIM.IMEI);
        if (imei == null) {
          return false;
        } else {
          logger.debug("[{}] - imei: [{}] is present in sim: [{}]", this, imei, iccid);
          return true;
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while ensuring that ICCID : [{}] is present in db", this, iccid, e);
      return false;
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateIMEI(final ICCID iccid, final IMEI imei) {
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));
        updateQuery.addValue(Tables.SIM.IMEI, imei);

        int res = updateQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - imei: [{}] updated on sim: [{}]", this, imei, iccid);
        } else {
          logger.warn("[{}] - can't update imei: [{}] on sim: [{}] (result after execution < 1)", this, imei, iccid);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while ensuring that ICCID : [{}] is present in db", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updatePhoneNumber(final ICCID iccid, final PhoneNumber number) {
    logger.debug("[{}] - trying to update phoneNumber (ICCID: [{}], number: [{}])", this, iccid, number);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update phoneNumber: [{}] ICCID is not valid: [{}]))", this, number, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));
        updateQuery.addValue(Tables.SIM.PHONE_NUMBER, number);

        int res = updateQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - phoneNumber was updated to: [{}] for sim: [{}]", this, number, iccid);
        } else {
          logger.warn("[{}] - can't update phoneNumber (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating phoneNumber for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void increaseSIMIncomingCalledTime(final ICCID iccid, final long duration) {
    logger.trace("[{}] - trying to increase incomingCallDuration (ICCID: [{}], duration: [{}])", this, iccid, duration);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty() || duration <= 0) {
      logger.warn("[{}] - can't increase incomingCallDuration on: [{}], for sim: [{}]", this, duration, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.INCOMING_SUCCESSFUL_CALL_DURATION, Tables.SIM.INCOMING_SUCCESSFUL_CALL_DURATION.add(duration));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - incomingCallDuration was increased on: [{}] for sim: [{}]", this, duration, iccid);
        } else {
          logger.warn("[{}] - can't increase incomingCallDuration (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while increasing incomingCallDuration for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMIncomingAllCallCount(final ICCID iccid) {
    logger.trace("[{}] - trying to increment incomingCallCount (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment incomingCallCount (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.INCOMING_CALL_COUNT, Tables.SIM.INCOMING_CALL_COUNT.add(1));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - incomingCallCount was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment incomingCallCount (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing incomingCallCount for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void incrementSIMIncomingSuccessfulCallCount(final ICCID iccid) {
    logger.trace("[{}] - trying to increment incomingSuccessfulCallCount (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't increment incomingSuccessfulCallCount (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.INCOMING_SUCCESSFUL_CALL_COUNT, Tables.SIM.INCOMING_SUCCESSFUL_CALL_COUNT.add(1));
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - incomingSuccessfulCallCount was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't increment incomingSuccessfulCallCount (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while incrementing incomingSuccessfulCallCount for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateResetSsScriptsFlag(final ICCID iccid, final boolean flag) {
    logger.debug("[{}] - trying to update resetSsScriptsFlag to: [{}] for sim (ICCID: [{}])", this, flag, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update userMessage (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.RESET_SS_SCRIPTS_FLAG, flag);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - resetSsScriptsFlag was updated to: [{}] for sim: [{}]", this, flag, iccid);
        } else {
          logger.warn("[{}] - can't update resetSsScriptsFlag for sim: [{}] (result after execution is < 1)", this, iccid);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating resetSsScriptsFlag for sim: [{}] to: [{}]", this, iccid, flag, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateResetVsScriptsFlag(final ICCID iccid, final boolean flag) {
    logger.debug("[{}] - trying to update resetVsScriptsFlag to: [{}] for sim (ICCID: [{}])", this, flag, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update userMessage (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.RESET_VS_SCRIPTS_FLAG, flag);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - resetVsScriptsFlag was updated to: [{}] for sim: [{}]", this, flag, iccid);
        } else {
          logger.warn("[{}] - can't update resetVsScriptsFlag for sim: [{}] (result after execution is < 1)", this, iccid);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating resetVsScriptsFlag for sim: [{}] to: [{}]", this, iccid, flag, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void changeSimGroup(final ICCID iccid, final long simGroupID) throws DataModificationException {
    logger.debug("trying to move sim:[{}] to sim_group:[{}]", iccid, simGroupID);
    if (iccid == null) {
      String msg = "Can't move sim to sim_group:[" + simGroupID + "] (sim_uid is null)";
      throw new DataModificationException(msg);
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);

        updateQuery.addValue(Tables.SIM.SIM_GROUP_ID, simGroupID > 0 ? simGroupID : null);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.info("[{}] - sim: [{}] moved to simGroup: [{}]", this, iccid, simGroupID);
        } else {
          throw new DataModificationException(String.format("[%s] - can't move sim: [%s] to simGroup: [%s] (result after execution < 1)", this, iccid, simGroupID));
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while moving sim: [%s] to simGroup: [%s]", this, iccid, simGroupID), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void importSimUIDs(final Map<ICCID, PhoneNumber> iccids) {
    logger.debug("[{}] - trying to import [{}] sim to db", this, iccids.size());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();

      context.transaction(configuration -> {
        for (Map.Entry<ICCID, PhoneNumber> entry : iccids.entrySet()) {
          ICCID iccid = entry.getKey();
          PhoneNumber phoneNumber = entry.getValue();

          try {
            UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
            updateQuery.addValue(Tables.SIM.PHONE_NUMBER, phoneNumber);
            updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

            int res = updateQuery.execute();

            if (res > 0) {
              logger.debug("[{}] - phoneNumber was updated to: [{}] for sim: [{}]", this, phoneNumber, iccid);
              continue;
            }

            SimRecord simRecord = context.newRecord(Tables.SIM);
            simRecord.setIccid(iccid);
            simRecord.setPhoneNumber(phoneNumber);

            res = simRecord.store();

            SimSerializableDataRecord simSerializableDataRecord = context.newRecord(Tables.SIM_SERIALIZABLE_DATA);
            simSerializableDataRecord.setIccid(iccid);

            int res2 = simSerializableDataRecord.store();

            if (res > 0 && res2 > 0) {
              logger.debug("[{}] - sim: [{}] inserted to db", this, iccid);
            } else {
              logger.warn("[{}] - can't insert sim: [{}] to db", this, iccid);
            }
          } catch (DataAccessException e) {
            logger.warn("[{}] - while inserting ICCID: [{}] to db", this, iccid, e);
            return;
          }
        }
      });
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateGSMChannel(final GSMChannel gsmChannel) throws DataModificationException {
    logger.debug("[{}] - trying to update gsmChannel: [{}]", this, gsmChannel);
    if (!(gsmChannel instanceof GSMChannelImpl)) {
      throw new DataModificationException(String.format("[%s] - can't update gsmChannel: [%s] (gsmChannel is null or not instance of [%s])", this, gsmChannel, GSMChannelImpl.class.getSimpleName()));
    }

    if (gsmChannel.getChannelUID() == null) {
      throw new DataModificationException(String.format("[%s] - can't update gsmChannel with invalid channelUID: [%s])", this, gsmChannel.getChannelUID()));
    }

    GSMChannelImpl gsmChannelObj = (GSMChannelImpl) gsmChannel;
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      long id = gsmChannelObj.getID();
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        GsmChannelRecord gsmChannelRecord = context.newRecord(GSM_CHANNEL);
        GsmChannelMapper.mapGsmChannelToGsmChannelRecord(gsmChannelObj, gsmChannelRecord);
        int res;
        if (id >= 1) {
          gsmChannelRecord.setId(id);
          res = gsmChannelRecord.update();
        } else {
          res = gsmChannelRecord.insert();
        }
        if (res > 0) {
          logger.info("[{}] - new gsmChannel: [{}] was updated", this, gsmChannel);
          gsmChannelObj.setID(gsmChannelRecord.getId());
        } else {
          throw new DataModificationException(String.format("[%s] - can't update gsmChannel: [%s] (result after execution < 1)", this, gsmChannel));
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while updating gsmChannel: [%s]", this, gsmChannel), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void lockGsmChannel(final ChannelUID channel, final boolean lock, final String lockReason) {
    logger.debug("[{}] - trying to set lock to [{}:{}] for gsmChannel: [{}]", this, lock, lockReason, channel);

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<GsmChannelRecord> updateQuery = context.updateQuery(Tables.GSM_CHANNEL);
        updateQuery.addValue(Tables.GSM_CHANNEL.LOCK, lock);
        updateQuery.addValue(Tables.GSM_CHANNEL.LOCK_REASON, lockReason);
        updateQuery.addValue(Tables.GSM_CHANNEL.LOCK_TIME, lock ? new Date() : null);
        updateQuery.addConditions(GSM_CHANNEL.DEVICE_UID.eq(channel.getDeviceUID().getUID()));
        updateQuery.addConditions(GSM_CHANNEL.CHANNEL_NUM.eq((short) channel.getChannelNumber()));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - set lock to [{}:{}] for gsmChannel: [{}]", this, lock, lockReason, channel);
        } else {
          logger.warn("[{}] - can't set lock to [{}:{}] for gsmChannel: [{}]", this, lock, lockReason, channel);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while setting lock to [{}:{}] for gsmChannel: [{}]", this, lock, lockReason, channel, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void lockGsmChannelToArfcn(final ChannelUID channel, final Integer arfcn) {
    logger.debug("[{}] - trying to set arfcn to [{}] for gsmChannel: [{}]", this, arfcn, channel);

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<GsmChannelRecord> updateQuery = context.updateQuery(Tables.GSM_CHANNEL);
        updateQuery.addValue(Tables.GSM_CHANNEL.LOCK_ARFCN, arfcn);
        updateQuery.addConditions(GSM_CHANNEL.DEVICE_UID.eq(channel.getDeviceUID().getUID()));
        updateQuery.addConditions(GSM_CHANNEL.CHANNEL_NUM.eq((short) channel.getChannelNumber()));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - set arfcn to [{}] for gsmChannel: [{}]", this, arfcn, channel);
        } else {
          logger.warn("[{}] - can't set arfcn to [{}] for gsmChannel: [{}]", this, arfcn, channel);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while setting arfcn to [{}:{}] for gsmChannel: [{}]", this, arfcn, channel, e);
    } finally {
      connection.close();
    }
  }


  @Override
  public void noteSIM(final ICCID iccid, final String note) {
    logger.debug("[{}] - trying to update note: [{}] for ICCID: [{}]", this, note, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't update noteSIM to [{}] (key is not valid: [{}])", this, note, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.NOTE, note);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - updated note successful", this);
        } else {
          logger.warn("[{}] - can't update note (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while updating note for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void setAllowedInternet(final ICCID iccid, final boolean allowed) {
    logger.trace("[{}] - trying to set allowed internet (ICCID: [{}])", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't set allowed internet (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.ALLOWED_INTERNET, allowed);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.trace("[{}] - set allowed internet was incremented for sim: [{}]", this, iccid);
        } else {
          logger.warn("[{}] - can't set allowed internet (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while set allowed internet for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void setTariffPlanEndDate(final ICCID iccid, final long endDate) {
    logger.debug("[{}] - trying to set tariff plan end date: [{}] for ICCID: [{}]", this, endDate, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't set tariff plan end date to [{}] (key is not valid: [{}])", this, endDate, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.TARIFF_PLAN_END_DATE, endDate);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - set tariff plan end date successful", this);
        } else {
          logger.warn("[{}] - can't set tariff plan end date (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while setting tariff plan end date for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void setBalance(final ICCID iccid, final double balance) {
    logger.debug("[{}] - trying to set balance [{}] for ICCID: [{}]", this, balance, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't set balance to [{}] (key is not valid: [{}])", this, balance, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.BALANCE, balance);
        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - set balance successful", this);
        } else {
          logger.warn("[{}] - can't set balance (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while setting balance for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void resetStatistic(final ICCID iccid) {
    logger.debug("[{}] - trying to reset statistic for ICCID: [{}]", this, iccid);
    String key = iccid == null ? null : iccid.getValue();
    if (key == null || key.isEmpty()) {
      logger.warn("[{}] - can't reset statistic (key is not valid: [{}])", this, key);
      return;
    }

    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        UpdateQuery<SimRecord> updateQuery = context.updateQuery(Tables.SIM);
        updateQuery.addValue(Tables.SIM.ALL_CALL_COUNT, 0);
        updateQuery.addValue(Tables.SIM.INCOMING_CALL_COUNT, 0);
        updateQuery.addValue(Tables.SIM.INCOMING_SMS_COUNT, 0);
        updateQuery.addValue(Tables.SIM.INCOMING_SUCCESSFUL_CALL_COUNT, 0);
        updateQuery.addValue(Tables.SIM.INCOMING_SUCCESSFUL_CALL_DURATION, 0L);
        updateQuery.addValue(Tables.SIM.SUCCESS_SMS_COUNT, 0);
        updateQuery.addValue(Tables.SIM.SUCCESSFUL_CALL_COUNT, 0);
        updateQuery.addValue(Tables.SIM.SUCCESSFUL_CALL_DURATION, 0L);
        updateQuery.addValue(Tables.SIM.TOTAL_SMS_COUNT, 0);
        updateQuery.addValue(Tables.SIM.TOTAL_SMS_STATUSES, 0);

        updateQuery.addConditions(Tables.SIM.ICCID.eq(iccid));

        int res = updateQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - set reset statistic successful", this);
        } else {
          logger.warn("[{}] - can't reset statistic (result after execution is < 1)", this);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while resetting statistic for sim: [{}]", this, iccid, e);
    } finally {
      connection.close();
    }
  }
}
