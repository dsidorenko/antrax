/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;


import static com.flamesgroup.storage.jooq.Tables.ALARIS_SMPP_SMS;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IAlarisSmppSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.tables.records.AlarisSmppSmsRecord;
import com.flamesgroup.storage.map.AlarisSmppSmsMapper;
import org.jooq.DSLContext;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AlarisSmppSmsDAO implements IAlarisSmppSmsDAO {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmppSmsDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public AlarisSmppSmsDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void insertSms(final AlarisSmppSms alarisHttpSms) throws DataModificationException {
    logger.debug("[{}] - trying to insert alarisSmppSms: [{}]", this, alarisHttpSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisSmppSmsRecord alarisHttpSmsRecord = context.newRecord(ALARIS_SMPP_SMS);
        AlarisSmppSmsMapper.mapAlarisSmppSmsToAlarisSmppSmsRecord(alarisHttpSms, alarisHttpSmsRecord);

        int insert = alarisHttpSmsRecord.insert();
        if (insert == 1) {
          logger.debug("[{}] - inserted alarisSmppSms: [{}]", this, alarisHttpSms);
        } else {
          logger.debug("[{}] - can't insert alarisSmppSms: [{}]", this, alarisHttpSms);
          throw new DataModificationException(String.format("[%s] - can't insert alarisSmppSms: [%s]", this, alarisHttpSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while insert alarisSmppSms [{}]", this, alarisHttpSms, e);
      throw new DataModificationException(String.format("[%s] - execution error, while insert alarisSmppSms: [%s]", this, alarisHttpSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updateSms(final AlarisSmppSms alarisHttpSms) throws DataModificationException {
    logger.debug("[{}] - trying to update alarisSmppSms: [{}]", this, alarisHttpSms);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        AlarisSmppSmsRecord alarisHttpSmsRecord = context.newRecord(ALARIS_SMPP_SMS);
        AlarisSmppSmsMapper.mapAlarisSmppSmsToAlarisSmppSmsRecord(alarisHttpSms, alarisHttpSmsRecord);

        int update = alarisHttpSmsRecord.update();
        if (update == 1) {
          logger.debug("[{}] - updated alarisSmppSms: [{}]", this, alarisHttpSms);
        } else {
          logger.debug("[{}] - can't update alarisSmppSms: [{}]", this, alarisHttpSms);
          throw new DataModificationException(String.format("[%s] - execution error, while update alarisSmppSms: [%s]", this, alarisHttpSms));
        }
      });
    } catch (DataAccessException e) {
      logger.error("[{}] - execution error, while update alarisSmppSms", this, e);
      throw new DataModificationException(String.format("[%s] - execution error, while update alarisSmppSms: [%s]", this, alarisHttpSms), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisSmppSms> listSmsAtRouting() {
    logger.debug("[{}] - trying to select alarisSmppSms at routing", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.STATUS.eq(AlarisSmppSms.Status.ENROUTE));

        return selectQuery.fetchInto(AlarisSmppSmsRecord.class).stream().map(AlarisSmppSmsMapper::mapAlarisSmsSmppRecordToAlarisSmppSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of alarisSmppSms at routing", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisSmppSms> listSmsDlr(final EnumSet<AlarisSmppSms.Status> dlrStatus) {
    logger.debug("[{}] - trying to select alarisSmppSms dlr to delivery", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.REGISTERED_DELIVERY.isNotNull().and(ALARIS_SMPP_SMS.DLR_TIME.isNull().and(ALARIS_SMPP_SMS.STATUS.in(dlrStatus))));

        return selectQuery.fetchInto(AlarisSmppSmsRecord.class).stream().map(AlarisSmppSmsMapper::mapAlarisSmsSmppRecordToAlarisSmppSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of alarisSmppSms dlr to delivery", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public int getCountOfSendSmsToday(final String username) {
    logger.debug("[{}] - trying to select count of alarisSmppSms for today", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.CREATE_TIME.greaterOrEqual(Date.from(LocalDate.now().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())));
        if (username != null) {
          selectQuery.addConditions(ALARIS_SMPP_SMS.USERNAME.eq(username));
        }
        return context.fetchCount(selectQuery);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting count of alarisSmppSms for today", this, e);
      return 0;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisSmppSms getBySmsIdAndPartNumber(final UUID smsId, final int numberOfSmsPart) {
    logger.debug("[{}] - trying to select alarisSmppSms by smsId: [{}]", this, smsId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.SMS_ID.eq(smsId));
        selectQuery.addConditions(ALARIS_SMPP_SMS.SAR_PART_NUMBER.isNull().or(ALARIS_SMPP_SMS.SAR_PART_NUMBER.eq(numberOfSmsPart)));

        AlarisSmppSmsRecord alarisHttpSmsRecord = selectQuery.fetchOne();
        return alarisHttpSmsRecord == null ? null : AlarisSmppSmsMapper.mapAlarisSmsSmppRecordToAlarisSmppSms(alarisHttpSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisSmppSms by smsId: [{}]", this, smsId, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public AlarisSmppSms getById(final UUID id) {
    logger.debug("[{}] - trying to select alarisSmppSms by id: [{}]", this, id);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.ID.eq(id));

        AlarisSmppSmsRecord alarisHttpSmsRecord = selectQuery.fetchOne();
        return alarisHttpSmsRecord == null ? null : AlarisSmppSmsMapper.mapAlarisSmsSmppRecordToAlarisSmppSms(alarisHttpSmsRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting alarisSmppSms by id: [{}]", this, id, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public List<AlarisSmppSms> getEdr(final Date fromDate, final Date toDate, final String source, final String destination, final String username, final AlarisSms.Status status) {
    logger.debug("[{}] - trying to select edr for alarisSmppSms", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<AlarisSmppSmsRecord> selectQuery = context.selectQuery(ALARIS_SMPP_SMS);
        selectQuery.addConditions(ALARIS_SMPP_SMS.CREATE_TIME.between(fromDate, toDate));
        if (source != null && !source.isEmpty()) {
          selectQuery.addConditions(ALARIS_SMPP_SMS.SOURCE_ADDR.eq(source));
        }
        if (destination != null && !destination.isEmpty()) {
          selectQuery.addConditions(ALARIS_SMPP_SMS.DEST_ADDR.eq(destination));
        }
        if (username != null && !username.isEmpty()) {
          selectQuery.addConditions(ALARIS_SMPP_SMS.USERNAME.eq(username));
        }
        if (status != null) {
          selectQuery.addConditions(ALARIS_SMPP_SMS.STATUS.eq(status));
        }

        return selectQuery.fetchInto(AlarisSmppSmsRecord.class).stream().map(AlarisSmppSmsMapper::mapAlarisSmsSmppRecordToAlarisSmppSms).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting edr for alarisSmppSms", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

}
