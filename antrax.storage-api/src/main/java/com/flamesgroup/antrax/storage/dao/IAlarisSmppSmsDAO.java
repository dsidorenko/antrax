/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

public interface IAlarisSmppSmsDAO {

  void insertSms(AlarisSmppSms alarisSms) throws DataModificationException;

  void updateSms(AlarisSmppSms alarisSms) throws DataModificationException;

  List<AlarisSmppSms> listSmsAtRouting();

  int getCountOfSendSmsToday(String username);

  List<AlarisSmppSms> listSmsDlr(EnumSet<AlarisSmppSms.Status> dlrStatus);

  AlarisSmppSms getBySmsIdAndPartNumber(UUID smsId, int numberOfSmsPart);

  AlarisSmppSms getById(UUID id);

  List<AlarisSmppSms> getEdr(Date fromDate, Date toDate, String source, String destination, String username, AlarisSms.Status status);

}
