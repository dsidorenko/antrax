/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.control.communication.ISimChannelManager;
import com.flamesgroup.antrax.control.distributor.ISimChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.simserver.PINRemoveFailedException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUException;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.ISCReaderChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.ISIMEntry;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ISIMCardUnit;
import com.flamesgroup.unit.sc.SIMCardUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class SimChannelManager implements ISimChannelManager {

  private static final Logger logger = LoggerFactory.getLogger(SimChannelManager.class);

  private static final int THREADS_COUNT = 10;

  private final Map<ChannelUID, ISIMEntry> simEntries = new ConcurrentHashMap<>();

  private final Lock removeCHVLock = new ReentrantLock();

  private final BlockingQueue<ChannelEntry> queue = new LinkedBlockingQueue<>();
  private final Map<ChannelUID, ISCReaderSubChannelsHandler> scReaderSubChannelsHandlers = new ConcurrentHashMap<>();

  private ExecutorService executorService;

  public void start(final ISimChannelManagerHandler channelManagerHandler) {
    if (executorService != null) {
      throw new IllegalStateException("SCReaderSubChannelsThread already started");
    }
    executorService = Executors.newFixedThreadPool(THREADS_COUNT);

    SCReaderSubChannelsTask scReaderSubChannelsTask = new SCReaderSubChannelsTask();
    for (int i = 0; i < THREADS_COUNT; i++) {
      executorService.execute(scReaderSubChannelsTask);
    }

    startManager(channelManagerHandler);
  }

  public void stop() {
    stopManager();

    if (executorService == null) {
      return;
    }

    executorService.shutdown();
    for (int i = 0; i < THREADS_COUNT; i++) {
      queue.offer(new ChannelEntry(null));
    }

    try {
      while (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of executorService", this);
      }
    } catch (InterruptedException ignored) {
      logger.warn("[{}] - while awaiting completion of executorService was interrupted", this);
    }
    executorService = null;
  }

  @Override
  public ATR start(final ChannelUID channel, final ISCReaderSubChannelsHandler scReaderSubChannelsHandler) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    ISCReaderChannel scReaderChannel = getSCReaderChannel(channel);
    // FIXME: channel must be already free!
    if (scReaderChannel.isEnslaved()) {
      logger.warn("[{}] - channel {} is enslaved, so stop and free channels", this, channel);
      try {
        freeSCReaderChannels(getSCReaderChannel(channel));
      } catch (ChannelException ignored) {
        logger.warn("[{}] - can't stop and free channels for {}", this, channel);
      }
    }

    scReaderChannel.enslave();

    ISCReaderSubChannel scReaderSubChannel = scReaderChannel.getSCReaderSubChannel();
    logger.debug("[{}] - starting [{}] with [{}]", this, channel, scReaderSubChannel);

    ATR atr;
    try {
      atr = scReaderSubChannel.start(new SCReaderSubChannelHandler(channel));
    } catch (ChannelException | InterruptedException e) {
      scReaderChannel.free();
      throw e;
    }

    scReaderSubChannelsHandlers.put(channel, scReaderSubChannelsHandler);
    return atr;
  }

  @Override
  public void stop(final ChannelUID channel) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    scReaderSubChannelsHandlers.remove(channel);
    freeSCReaderChannels(getSCReaderChannel(channel));
  }

  @Override
  public void writeAPDUCommand(final ChannelUID channel, final APDUCommand command) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    ISCReaderChannel scReaderChannel = getSCReaderChannel(channel);
    scReaderChannel.getSCReaderSubChannel().writeAPDUCommand(command);
  }

  @Override
  public void removeCHV(final ChannelUID channel, final CHV chv1) throws RemoteException, PINRemoveFailedException {
    if (!simEntries.containsKey(channel)) {
      return;
    }

    removeCHVLock.lock();
    try {
      ISIMCardUnit simCardUnit = new SIMCardUnit(simEntries.get(channel).getSCReaderChannel());
      try {
        simCardUnit.turnOn();
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't turn on sim card unit [{}]", this, channel, e);
        throw new PINRemoveFailedException(String.format("Can't turn on sim card unit [%s]", channel));
      }

      try {
        simCardUnit.verifyCHV1(chv1);
        simCardUnit.disableCHV1(chv1);
      } catch (APDUException | ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't disable CHV for [{}]", this, channel, e);
        throw new PINRemoveFailedException(e.getMessage());
      } finally {
        try {
          simCardUnit.turnOff();
        } catch (ChannelException | InterruptedException e) {
          logger.warn("[{}] - can't turn off sim card unit [{}]", this, channel, e);
        }
      }
    } finally {
      removeCHVLock.unlock();
    }
  }

  protected void addSimEntry(final ChannelUID channel, final ISIMEntry simEntry) {
    simEntries.put(channel, simEntry);
  }

  protected void removeSimEntry(final ChannelUID channel) {
    ISCReaderChannel scReaderChannel = simEntries.remove(channel).getSCReaderChannel();
    if (scReaderChannel.isEnslaved()) {
      try {
        freeSCReaderChannels(scReaderChannel);
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't force free sc reader channel for {}", this, channel, e);
      }
    }
  }

  private ISCReaderChannel getSCReaderChannel(final ChannelUID channel) throws IllegalChannelException {
    ISIMEntry simEntry = simEntries.get(channel);
    if (simEntry == null) {
      throw new IllegalChannelException(channel);
    }

    return simEntry.getSCReaderChannel();
  }

  private void freeSCReaderChannels(final ISCReaderChannel scReaderChannel) throws ChannelException, InterruptedException {
    try {
      scReaderChannel.getSCReaderSubChannel().stop();
    } finally {
      scReaderChannel.free();
    }
  }

  private class SCReaderSubChannelHandler implements ISCReaderSubChannelHandler {

    private final ChannelUID channel;

    private SCReaderSubChannelHandler(final ChannelUID channel) {
      this.channel = channel;
    }

    @Override
    public void handleAPDUResponse(final APDUResponse response) {
      if (!queue.offer(new ChannelAPDUResponse(channel, response))) {
        logger.warn("[{}] - can't handle [{}] - queue overflow for [{}]", this, response, channel);
      }
    }

    @Override
    public void handleErrorState(final SCReaderError error, final SCReaderState state) {
      if (!queue.offer(new ChannelErrorState(channel, error, state))) {
        logger.warn("[{}] - can't handle error state [{}:{}] - queue overflow for [{}]", this, error, state, channel);
      }
    }

  }

  public abstract void startManager(ISimChannelManagerHandler channelManagerHandler);

  public abstract void stopManager();

  private class ChannelEntry {

    private final ChannelUID channel;

    public ChannelEntry(final ChannelUID channel) {
      this.channel = channel;
    }

    public ChannelUID getChannel() {
      return channel;
    }

  }

  private class ChannelAPDUResponse extends ChannelEntry {

    private final APDUResponse apduResponse;

    public ChannelAPDUResponse(final ChannelUID channel, final APDUResponse apduResponse) {
      super(channel);
      this.apduResponse = apduResponse;
    }

    public APDUResponse getAPDUResponse() {
      return apduResponse;
    }

  }

  private class ChannelErrorState extends ChannelEntry {

    private final SCReaderError error;
    private final SCReaderState state;

    public ChannelErrorState(final ChannelUID channel, final SCReaderError error, final SCReaderState state) {
      super(channel);
      this.error = error;
      this.state = state;
    }

    public SCReaderError getError() {
      return error;
    }

    public SCReaderState getState() {
      return state;
    }

  }

  private class SCReaderSubChannelsTask implements Runnable {

    @Override
    public void run() {
      while (!executorService.isShutdown()) {
        try {
          ChannelEntry channelEntry = queue.take();
          if (channelEntry instanceof ChannelAPDUResponse) {
            ChannelAPDUResponse channelAPDUResponse = (ChannelAPDUResponse) channelEntry;
            ISCReaderSubChannelsHandler channelsHandler = scReaderSubChannelsHandlers.get(channelAPDUResponse.getChannel());
            if (channelsHandler == null) {
              continue; // already stopped SC Reader Channel
            }

            try {
              channelsHandler.handleAPDUResponse(channelAPDUResponse.getChannel(), channelAPDUResponse.getAPDUResponse());
            } catch (RemoteException | IllegalChannelException e) {
              logger.warn("[{}] - while handle APDU response for [{}]", this, channelAPDUResponse.getChannel(), e);
            }
          } else if (channelEntry instanceof ChannelErrorState) {
            ChannelErrorState channelErrorState = (ChannelErrorState) channelEntry;
            ISCReaderSubChannelsHandler channelsHandler = scReaderSubChannelsHandlers.get(channelErrorState.getChannel());
            if (channelsHandler == null) {
              continue; // already stopped SC Reader Channel
            }

            try {
              channelsHandler.handleErrorState(channelErrorState.getChannel(), channelErrorState.getError(), channelErrorState.getState());
            } catch (RemoteException | IllegalChannelException e) {
              logger.warn("[{}] - while handle error state for [{}]", this, channelErrorState.getChannel(), e);
            }
          }
        } catch (InterruptedException e) {
          logger.error("[{}] - was interrupted", this, e);
        }
      }
    }

  }

}
