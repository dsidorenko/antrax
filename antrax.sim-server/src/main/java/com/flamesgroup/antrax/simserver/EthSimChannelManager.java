/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.commons.DeviceJournalHelper;
import com.flamesgroup.antrax.control.distributor.ISimChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.PINRemoveFailedException;
import com.flamesgroup.antrax.control.simserver.SimChannelConfig;
import com.flamesgroup.antrax.control.utils.AliasDeviceUID;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.ChannelConfig.ChannelState;
import com.flamesgroup.antrax.simserver.properties.SimServerPropUtils;
import com.flamesgroup.antrax.storage.state.CHVState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.device.DeviceUtil;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.device.protocol.cudp.CudpConnection;
import com.flamesgroup.device.protocol.cudp.CudpException;
import com.flamesgroup.device.protocol.cudp.CudpStream;
import com.flamesgroup.device.protocol.cudp.ExpireTime;
import com.flamesgroup.device.protocol.cudp.ICudpConnection;
import com.flamesgroup.device.protocol.cudp.IPConfig;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigLicenseExpireException;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigLicenseInvalidException;
import com.flamesgroup.device.protocol.cudp.command.SetIPConfigParametersErrException;
import com.flamesgroup.device.protocol.mudp.MudpException;
import com.flamesgroup.device.sc.APDUException;
import com.flamesgroup.device.simb.ISIMDevice;
import com.flamesgroup.device.simb.ISIMEntry;
import com.flamesgroup.device.simb.SIMBDevice;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.ISIMCardUnit;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sc.SIMCardUnit;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EthSimChannelManager extends SimChannelManager {

  private static final Logger logger = LoggerFactory.getLogger(EthSimChannelManager.class);

  private static final int CUDP_DEVCIE_PORT = 9010;

  private final SocketAddress remoteBroadcastAddress;
  private final Map<DeviceUID, IPConfig> deviceIPConfigs;
  private final UUID ipConfigUUID;

  private final Map<DeviceUID, Integer> lostAttempts = new HashMap<>();
  private final Map<DeviceUID, Long> setIPConfigTimeouts = new HashMap<>();

  private final Lock cudpConnectionLock = new ReentrantLock();
  private final ICudpConnection cudpConnection = new CudpConnection(new CudpStream(), 1000); //TODO: move to constructor

  private final Map<DeviceUID, ISIMDevice> activeDevices = new HashMap<>();
  private final Map<DeviceUID, List<ChannelUID>> activeChannels = new ConcurrentHashMap<>();
  private final Map<DeviceUID, List<ChannelUID>> removedCHVChannels = new ConcurrentHashMap<>();

  private ISimChannelManagerHandler channelManagerHandler;
  private SimChannelManagerThread channelManagerThread;

  private ThreadPoolExecutor channelExecutor;

  private Timer setIPConfigTimer;

  public EthSimChannelManager(final InetAddress broadcastAddress, final Map<DeviceUID, IPConfig> deviceIPConfigs, final UUID ipConfigUUID) {
    this.remoteBroadcastAddress = new InetSocketAddress(broadcastAddress, CUDP_DEVCIE_PORT);
    this.deviceIPConfigs = deviceIPConfigs;
    this.ipConfigUUID = ipConfigUUID;
  }

  @Override
  public void removeCHV(final ChannelUID channel, final CHV chv1) throws RemoteException, PINRemoveFailedException {
    super.removeCHV(channel, chv1);
    removedCHVChannels.computeIfAbsent(channel.getDeviceUID(), k -> new CopyOnWriteArrayList<>());
    removedCHVChannels.get(channel.getDeviceUID()).add(channel);
  }

  @Override
  public void setIndicationMode(final ChannelUID channel, final IndicationMode indicationMode) throws RemoteException {
    // not supported for old devices
  }

  @Override
  public synchronized void startManager(final ISimChannelManagerHandler channelManagerHandler) {
    if (channelManagerThread != null) {
      throw new IllegalStateException(String.format("[%s] - CudpSimChannelManager has already connected", this));
    }

    cudpConnectionLock.lock();
    try {
      try {
        cudpConnection.connect(remoteBroadcastAddress);
      } catch (CudpException e) {
        throw new IllegalStateException(String.format("[%s] - can't connect to remote broadcast address %s", this, remoteBroadcastAddress), e);
      }
    } finally {
      cudpConnectionLock.unlock();
    }

    this.channelManagerHandler = channelManagerHandler;
    channelExecutor = new WaitThreadPoolExecutor(20, 40, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<>(20), r -> {
      Thread thread = new Thread(r);
      thread.setName("SimChannelExecutor@" + thread.hashCode());
      return thread;
    });

    channelManagerThread = new SimChannelManagerThread();
    channelManagerThread.start();

    setIPConfigTimer = new Timer();
    long setIPConfigInterval = SimServerPropUtils.getInstance().getSimServerProperties().getSetIpConfigInterval();
    setIPConfigTimer.scheduleAtFixedRate(new SetIPConfigTask(), setIPConfigInterval, setIPConfigInterval);
  }

  @Override
  public synchronized void stopManager() {
    if (channelManagerThread == null) {
      return;
    }

    setIPConfigTimer.cancel();
    setIPConfigTimer = null;

    Thread localDeviceThread = channelManagerThread;
    channelManagerThread = null;
    try {
      localDeviceThread.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - while join thread was interrupted", this, e);
    }

    for (Map.Entry<DeviceUID, ISIMDevice> entry : activeDevices.entrySet()) {
      channelExecutor.execute(new ChannelRemoveTask(entry.getValue(), activeChannels.get(entry.getKey())));
    }
    channelExecutor.shutdown();
    try {
      while (!channelExecutor.awaitTermination(5, TimeUnit.SECONDS)) {
        logger.debug("[{}] - awaiting completion of DeviceRemoveExecutor", this);
      }
    } catch (InterruptedException e) {
      logger.debug("[{}] - while awaiting completion of executor", this, e);
    }
    channelManagerHandler = null;

    cudpConnectionLock.lock();
    try {
      try {
        cudpConnection.disconnect();
      } catch (CudpException | InterruptedException e) {
        logger.warn("[{}] - can't disconnect from remote broadcast address", this, e);
      }
    } finally {
      cudpConnectionLock.unlock();
    }
  }

  private ChannelUID createChannelUID(final DeviceUID deviceUID, final byte channelNumber) {
    return new ChannelUID(deviceUID, AliasDeviceUID.getAlias(deviceUID), channelNumber);
  }

  private void handleEmptyChannel(final ChannelUID channel, final ChannelConfig channelConfig) {
    try {
      channelManagerHandler.handleEmptyChannel(AntraxProperties.SERVER_NAME, channel, channelConfig);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't handle empty channel {}", this, channel, e);
    }
  }

  private class ChannelAddTask implements Runnable {

    private final ChannelUID channel;
    private final ISIMEntry simEntry;
    private final ChannelConfig channelConfig;


    public ChannelAddTask(final ChannelUID channel, final ISIMEntry simEntry, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.simEntry = simEntry;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      ISIMCardUnit simCardUnit = new SIMCardUnit(simEntry.getSCReaderChannel());
      try {
        simCardUnit.turnOn();
      } catch (ChannelException | InterruptedException e) {
        logger.warn("[{}] - can't turn on sim card unit", this, e);
        handleEmptyChannel(channel, channelConfig);
        return;
      }

      SimChannelConfig simChannelConfig;
      try {
        ICCID iccid = simCardUnit.readICCID();
        if (simCardUnit.isCHV1Enabled()) {
          simChannelConfig = new SimChannelConfig(iccid, CHVState.REQUIRES_CHV, channelConfig);
        } else {
          IMSI imsi = simCardUnit.readIMSI();
          PhoneNumber phoneNumber;
          try {
            phoneNumber = simCardUnit.readPhoneNumber();
          } catch (APDUException e) {
            phoneNumber = null;
            logger.warn("[{}] - can't read phone number", this, e);
          }

          simChannelConfig = new SimChannelConfig(iccid, CHVState.NO_CHV, channelConfig, imsi, phoneNumber);
        }
      } catch (ChannelException | APDUException | InterruptedException e) {
        logger.warn("[{}] - can't read sim card data", this, e);
        handleEmptyChannel(channel, channelConfig);
        return;
      } finally {
        try {
          simCardUnit.turnOff();
        } catch (ChannelException | InterruptedException e) {
          logger.warn("[{}] - can't turn off sim card unit", this, e);
        }
      }

      List<ChannelUID> channels = activeChannels.get(channel.getDeviceUID());
      if (channels == null) {
        return;
      }

      try {
        channelManagerHandler.handleAvailableChannel(AntraxProperties.SERVER_NAME, channel, simChannelConfig);
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle available channel {}", this, channel, e);
        return;
      }

      addSimEntry(channel, simEntry);
      channels.add(channel);
    }

    @Override
    public String toString() {
      return "ChannelAddTask[" + channel + "]";
    }

  }

  private class ChannelRemoveTask implements Runnable {

    private final ISIMDevice simDevice;
    private final List<ChannelUID> channels;

    public ChannelRemoveTask(final ISIMDevice simDevice, final List<ChannelUID> channels) {
      this.simDevice = simDevice;
      this.channels = channels;
    }

    @Override
    public void run() {
      channels.forEach(EthSimChannelManager.this::removeSimEntry);
      try {
        channelManagerHandler.handleLostChannels(AntraxProperties.SERVER_NAME, channels);
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle lost channels {}", this, channels, e);
      }
      try {
        simDevice.detach();
      } catch (MudpException | InterruptedException e) {
        logger.warn("[{}] - can't detach from device {}", this, simDevice, e);
      }
    }

    @Override
    public String toString() {
      return "ChannelRemoveTask[" + simDevice + "]";
    }

  }

  private class ChannelInvalidConfigTask implements Runnable {

    private final ChannelUID channel;
    private final ChannelConfig channelConfig;

    private ChannelInvalidConfigTask(final ChannelUID channel, final ChannelConfig channelConfig) {
      this.channel = channel;
      this.channelConfig = channelConfig;
    }

    @Override
    public void run() {
      try {
        channelManagerHandler.handleInvalidChannel(AntraxProperties.SERVER_NAME, channel, channelConfig);
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle available channel {}", this, channel, e);
      }
    }

  }

  private class SimChannelManagerThread extends Thread {

    private static final int MUDP_DEVICE_PORT = 9020;

    private static final long READ_DEVICE_UID_TIMEOUT = 3000;
    private static final long WAIT_RESPONSE_INTERVAL = 1000;
    private static final long RESET_DELAY = 100;

    private static final int LOST_DEVICE_ATTEMPT_COUNT = 2;
    private static final long SET_IP_CONFIG_TIMEOUT = 60_000;

    public SimChannelManagerThread() {
      setName("SimChannelManagerThread[" + ipConfigUUID + "]");
    }

    @Override
    public void run() {
      List<ChannelUID> declaredChannels = new ArrayList<>();
      for (DeviceUID deviceUID : deviceIPConfigs.keySet()) {
        for (int i = 0; i < SIMBDevice.SIM_ENTRY_COUNT; i++) {
          declaredChannels.add(createChannelUID(deviceUID, (byte) i));
        }
      }

      try {
        channelManagerHandler.handleDeclaredChannels(AntraxProperties.SERVER_NAME, declaredChannels);
      } catch (RemoteException e) {
        logger.error("[{}] - can't handle declared channels", this, e);
        return;
      }

      Thread currentThread = Thread.currentThread();
      while (currentThread == channelManagerThread) {
        try {
          Thread.sleep(READ_DEVICE_UID_TIMEOUT);

          Collection<Future<?>> futures = new LinkedList<>();
          Map<DeviceUID, Pair<IPConfig, ExpireTime>> availableDevices = new HashMap<>();
          cudpConnectionLock.lock();
          try {
            Set<DeviceUID> deviceUIDs;
            try {
              deviceUIDs = cudpConnection.getDeviceUIDs(WAIT_RESPONSE_INTERVAL);
            } catch (CudpException e) {
              logger.warn("[{}] - can't find devices", this, e);
              continue;
            }

            for (DeviceUID deviceUID : deviceUIDs) {
              if (DeviceType.SIMB != deviceUID.getDeviceType() || !deviceIPConfigs.containsKey(deviceUID)) {
                continue;
              }

              try {
                IPConfig ipConfig = cudpConnection.getIPConfig(deviceUID);
                ExpireTime expireTime = cudpConnection.getExpireTime(deviceUID);
                availableDevices.put(deviceUID, new Pair<>(ipConfig, expireTime));
              } catch (CudpException e) {
                logger.warn("[{}] - {} is not responding", this, deviceUID.getCanonicalName(), e);
              }
            }

            for (Map.Entry<DeviceUID, Pair<IPConfig, ExpireTime>> entry : availableDevices.entrySet()) {
              DeviceUID deviceUID = entry.getKey();
              IPConfig ipConfig = entry.getValue().first();

              if (removedCHVChannels.containsKey(deviceUID)) {
                for (ChannelUID channel : removedCHVChannels.remove(deviceUID)) {
                  ChannelConfig channelConfig = new ChannelConfig(entry.getValue().second(), ChannelState.ACTIVE);
                  ISIMEntry simEntry = activeDevices.get(deviceUID).getSIMEntries().get(channel.getChannelNumber());
                  futures.add(channelExecutor.submit(new ChannelAddTask(channel, simEntry, channelConfig)));
                }
              }

              if (ipConfig.getUUID().equals(ipConfigUUID) && activeDevices.containsKey(deviceUID)) {
                continue;
              }

              if (ipConfig.getUUID().equals(IPConfig.UNINITIALIZED_UUID)) {
                if (activeDevices.containsKey(deviceUID)) {
                  logger.debug("[{}] - lost device {}", this, deviceUID.getCanonicalName());
                  futures.add(channelExecutor.submit(new ChannelRemoveTask(activeDevices.remove(deviceUID), activeChannels.remove(deviceUID))));
                  continue;
                }

                if (setIPConfigTimeouts.containsKey(deviceUID)) {
                  if (System.currentTimeMillis() - setIPConfigTimeouts.get(deviceUID) >= SET_IP_CONFIG_TIMEOUT) {
                    setIPConfigTimeouts.remove(deviceUID);
                  } else {
                    continue;
                  }
                }

                ChannelState errorStatus = ChannelState.ACTIVE;
                IPConfig deviceIPConfig = deviceIPConfigs.get(deviceUID);
                try {
                  cudpConnection.setIPConfig(deviceUID, deviceIPConfig);
                } catch (SetIPConfigParametersErrException ignored) {
                  logger.warn("[{}] - device {} get invalid one of ip config parameters {}", this, deviceUID.getCanonicalName(), deviceIPConfig);
                  errorStatus = ChannelState.IP_CONFIG_PARAMETERS_INVALID;
                } catch (SetIPConfigLicenseInvalidException ignored) {
                  logger.warn("[{}] - device {} has invalid license", this, deviceUID);
                  errorStatus = ChannelState.LICENSE_INVALID;
                } catch (SetIPConfigLicenseExpireException ignored) {
                  logger.warn("[{}] - device {} has expired license", this, deviceUID);
                  errorStatus = ChannelState.LICENSE_EXPIRE;
                } catch (CudpException e) {
                  logger.warn("[{}] - can't set IP config for device {}", this, deviceUID, e);
                  errorStatus = ChannelState.SET_IP_CONFIG_ERROR;
                }

                if (errorStatus != ChannelState.ACTIVE) {
                  ChannelConfig channelConfig = new ChannelConfig(entry.getValue().second(), errorStatus);
                  for (int i = 0; i < SIMBDevice.SIM_ENTRY_COUNT; i++) {
                    futures.add(channelExecutor.submit(new ChannelInvalidConfigTask(createChannelUID(deviceUID, (byte) i), channelConfig)));
                  }

                  setIPConfigTimeouts.put(deviceUID, System.currentTimeMillis());
                  continue;
                }

                ISIMDevice simDevice;
                try {
                  simDevice = DeviceUtil.createEthSIMDevice(new InetSocketAddress(deviceIPConfig.getIP(), MUDP_DEVICE_PORT));
                } catch (ChannelException | MudpException e) {
                  logger.warn("[{}] - can't create sim device for [{}]", this, deviceUID.getCanonicalName(), e);
                  continue;
                }

                logger.debug("[{}] - attach to sim device [{}]", this, deviceUID.getCanonicalName());
                try {
                  simDevice.attach();
                } catch (MudpException | InterruptedException e) {
                  logger.warn("[{}] - can't attach to [{}]", this, deviceUID.getCanonicalName(), e);
                  continue;
                }

                activeDevices.put(deviceUID, simDevice);
                activeChannels.put(deviceUID, new CopyOnWriteArrayList<>());

                int channelNumber = 0;
                ChannelConfig channelConfig = new ChannelConfig(entry.getValue().second(), errorStatus);
                for (ISIMEntry simEntry : simDevice.getSIMEntries()) {
                  futures.add(channelExecutor.submit(new ChannelAddTask(createChannelUID(deviceUID, (byte) channelNumber++), simEntry, channelConfig)));
                }
                try {
                  DeviceJournalHelper.readJournal(simDevice, deviceUID);
                } catch (ChannelException | InterruptedException e) {
                  logger.warn("[{}] - can't read device journal", this, e);
                }
              } else {
                try {
                  cudpConnection.reset(deviceUID, RESET_DELAY);
                } catch (CudpException e) {
                  logger.warn("[{}] - can't reset device {}", this, deviceUID.getCanonicalName(), e);
                }
              }
            }
          } finally {
            cudpConnectionLock.unlock();
          }

          Set<DeviceUID> lostDevices = new HashSet<>(activeDevices.keySet());
          lostDevices.removeAll(availableDevices.keySet());
          lostAttempts.keySet().retainAll(lostDevices);
          for (DeviceUID deviceUID : lostDevices) {
            int attempts = lostAttempts.getOrDefault(deviceUID, 0);
            if (attempts <= LOST_DEVICE_ATTEMPT_COUNT) {
              lostAttempts.put(deviceUID, attempts + 1);
              continue;
            }

            logger.debug("[{}] - lost device {}", this, deviceUID.getCanonicalName());
            lostAttempts.remove(deviceUID);
            futures.add(channelExecutor.submit(new ChannelRemoveTask(activeDevices.remove(deviceUID), activeChannels.remove(deviceUID))));
          }

          for (Future<?> future : futures) {
            try {
              future.get();
            } catch (ExecutionException e) {
              logger.warn("[{}] - execution exception", this, e);
            }
          }

        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected interruption", this, e);
          break;
        }
      }
    }
  }

  private final class SetIPConfigTask extends TimerTask {

    @Override
    public void run() {
      cudpConnectionLock.lock();
      try {
        for (DeviceUID deviceUID : activeDevices.keySet()) {
          try {
            cudpConnection.setIPConfig(deviceUID, deviceIPConfigs.get(deviceUID));
          } catch (CudpException | InterruptedException ignored) {
            logger.warn("[{}] - can't set ip config for [{}] by timer", this, deviceUID);
          }
        }
      } finally {
        cudpConnectionLock.unlock();
      }
    }

  }

}
