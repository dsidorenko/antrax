/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.simserver;

import com.flamesgroup.antrax.configuration.IPingServerManager;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.control.communication.SimServerShortInfo;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.simserver.properties.SimServerPropUtils;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class ControlServerPingThread extends Thread {

  private static final Logger logger = LoggerFactory.getLogger(ControlServerPingThread.class);

  private final Server server = new Server(AntraxProperties.SERVER_NAME, ServerType.SIM_SERVER);

  private final IPingServerManager pingSimServerManager;
  private final ISimServer simServer;
  private final long timeout;

  private volatile Thread currentThread;

  public ControlServerPingThread(final IPingServerManager pingSimServerManager, final ISimServer simServer, final long timeout) {
    super("ControlServerPingThread (" + pingSimServerManager + ")");
    this.pingSimServerManager = pingSimServerManager;
    this.simServer = simServer;
    this.timeout = timeout;
  }

  @Override
  public void run() {
    try {
      pingSimServerManager.setExpireTimeout(server, timeout);
    } catch (RemoteException e) {
      logger.warn("Control server not responding, terminate thread");
      throw new ControlServerPingException(e);
    }

    Thread thisThread = Thread.currentThread();
    currentThread = thisThread;
    while (currentThread == thisThread) {
      int rmiRegistryPort = SimServerPropUtils.getInstance().getSimServerProperties().getRmiRegistryPort();
      try {
        pingSimServerManager.ping(server, simServer.getServerStatus(), rmiRegistryPort);
      } catch (RemoteException e) {
        logger.warn("Control server not responding, terminate thread", e);
        throw new ControlServerPingException(e);
      }

      try {
        Thread.sleep(timeout);
      } catch (InterruptedException e) {
        logger.error("Unexpected interruption of controlServerPingThread ", e);
        break;
      }
    }
  }

  public void terminate() {
    currentThread = null;
  }

}
