package com.flamesgroup.antrax.control.swingwidgets.table;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class JUpdateTableTableSetDataTests {

  private String selected;

  @Test
  public void testAlternatelySetDataAndSelect() throws InterruptedException {
    UpdatableTableModelTest.StringWithColonTableBuilder rowBuilder = new UpdatableTableModelTest.StringWithColonTableBuilder("Column 0", "Column 1");
    final JUpdatableTable<String, String> table = new JUpdatableTable<>(rowBuilder);
    //table.getRowSorter().toggleSortOrder(0);
    table.setName("TestTable");

    //final Semaphore sm = new Semaphore(1);
    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

      @Override
      public void valueChanged(final ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
          return;
        }
        System.out.println("try to get selected");
        selected = table.getSelectedElem();
        System.out.println("selected '" + String.valueOf(selected) + "'");
        //sm.release();
      }

    });

    String row1 = "a:b";
    String row2 = "c:d";

    table.setData(row1);
    table.selectRow(0);
    //sm.acquire();
    assertEquals(row1, selected);
    assertEquals(table.getRowCount(), table.getElems().size());
    System.out.println();

    table.setData(row2);
    table.selectRow(0);
    //sm.acquire();
    assertEquals(row2, selected);
    assertEquals(table.getRowCount(), table.getElems().size());
    System.out.println();

    table.setData(row1);
    table.selectRow(0);
    //sm.acquire();
    assertEquals(row1, selected);
    assertEquals(table.getRowCount(), table.getElems().size());
  }

}
