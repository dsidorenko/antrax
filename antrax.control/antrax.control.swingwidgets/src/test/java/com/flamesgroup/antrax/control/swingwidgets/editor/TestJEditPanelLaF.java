package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.*;

import javax.swing.*;

public class TestJEditPanelLaF {

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
      JFrame frame = new JFrame("JEditPanel Look & Feel");

      JEditPanel editPanel = new JEditPanel(new FlowLayout());
      JEditLabel editLabelEnabled = (new JEditLabel.JEditLabelBuilder()).setText("<Enabled edit label>").build();
      JEditLabel editLabelDisabled = (new JEditLabel.JEditLabelBuilder()).setText("<Disabled edit label>").build();
      editLabelDisabled.setEnabled(false);
      editPanel.add(editLabelEnabled);
      editPanel.add(editLabelDisabled);

      frame.add(editPanel);
      frame.pack();
      Dimension dim = new Dimension(300, 200);
      frame.setPreferredSize(dim);
      frame.setSize(dim);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setLocationRelativeTo(null);
      frame.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
