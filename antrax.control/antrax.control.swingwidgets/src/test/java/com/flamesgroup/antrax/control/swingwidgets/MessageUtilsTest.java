package com.flamesgroup.antrax.control.swingwidgets;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MessageUtilsTest {
  @Test
  public void wrapLines() {
    String msg = MessageUtils.wrapLines("abc", 1, 100);
    assertEquals("a\n  b\n  c\n", msg);
    msg = MessageUtils.wrapLines("hello\nworld", 3, 100);
    assertEquals("hel\n  lo\nwor\n  ld\n", msg);
    msg = MessageUtils.wrapLines("abc", 1, 2);
    assertEquals("a\n  b\n", msg);
    msg = MessageUtils.wrapLines("abc\ndef", 1, 4);
    assertEquals("a\n  b\n  c\nd\n", msg);
  }
}
