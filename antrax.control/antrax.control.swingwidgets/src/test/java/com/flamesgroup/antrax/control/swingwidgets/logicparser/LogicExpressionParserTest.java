package com.flamesgroup.antrax.control.swingwidgets.logicparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class LogicExpressionParserTest {

  @Test
  public void testValidParsing() throws LogicExpressionSyntaxException {
    LogicExpressionParser parser = new LogicExpressionParser();
    Node root = parser.parseExpression("a");
    assertEquals("a", root.toString());

    root = parser.parseExpression("!a");
    assertEquals("a !", root.toString());

    root = parser.parseExpression("a & b");
    assertEquals("a b &", root.toString());

    root = parser.parseExpression("a b", LogicToken.AND);
    assertEquals("a b &", root.toString());

    root = parser.parseExpression("a    b", LogicToken.AND);
    assertEquals("a b &", root.toString());

    root = parser.parseExpression("a & b & !c");
    assertEquals("a b & c ! &", root.toString());

    root = parser.parseExpression("a b !c", LogicToken.AND);
    assertEquals("a b & c ! &", root.toString());

    root = parser.parseExpression("a & (b | (c & !d))");
    assertEquals("a b c d ! & | &", root.toString());

    root = parser.parseExpression("a & (b | (c !d))", LogicToken.AND);
    assertEquals("a b c d ! & | &", root.toString());
  }

  @Test
  public void testQuotedString() throws LogicExpressionSyntaxException {
    LogicExpressionParser parser = new LogicExpressionParser();
    Node root = parser.parseExpression("\"a(b c d) -- ! hello\"");
    assertEquals("a(b c d) -- ! hello", root.toString());
  }

  @Test(expected = LogicExpressionSyntaxException.class)
  public void testManyNotOperators() throws LogicExpressionSyntaxException {
    LogicExpressionParser parser = new LogicExpressionParser();
    parser.parseExpression("!!a");
  }

  @Test(expected = LogicExpressionSyntaxException.class)
  public void testManyAndOperators() throws LogicExpressionSyntaxException {
    LogicExpressionParser parser = new LogicExpressionParser();
    parser.parseExpression("&&a");
  }

  private void testSyntaxError(final String expression, final int errorIndex) {
    LogicExpressionParser parser = new LogicExpressionParser();
    try {
      parser.parseExpression(expression);
      assertFalse("Syntax error not detected in '" + expression + "'", true);
    } catch (LogicExpressionSyntaxException e) {
      String message = String.format("'%s'", expression);
      assertEquals(message, errorIndex, e.getErrorPosition());
    }
  }

  @Test
  public void testSyntaxErrorPosition() {
    testSyntaxError("!!s", 1);
    testSyntaxError("a && b", 3);
    testSyntaxError("a & b)", 5);
  }

}
