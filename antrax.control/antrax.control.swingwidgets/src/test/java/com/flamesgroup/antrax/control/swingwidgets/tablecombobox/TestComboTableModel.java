package com.flamesgroup.antrax.control.swingwidgets.tablecombobox;

import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class TestComboTableModel implements TableModel {

  private static final Class<?>[] columnClasses = new Class<?>[] {
      String.class,
      String.class,
      String.class,
      String.class
  };

  private static final String[] columnName = new String[] {
      "CEM",
      "Phone number",
      "IMSI",
      "Bekki"
  };

  private final String[][] data = {
      {"CEM-5:1", "0995625623", "293234681734", "BEKKI-12:0"},
      {"CEM-5:5", "0660988293", "923742343424", "BEKKI-14:0"},
      {"CEM-7:8", "0660854512", "563418633496", "BEKKI-14:0"},
      {"CEM-8:2", "0698632527", "214521212544", "BEKKI-24:0"},
  };

  private final List<TableModelListener> listeners = new LinkedList<>();

  public TestComboTableModel() {

  }

  @Override
  public void addTableModelListener(final TableModelListener l) {
    listeners.add(l);
  }

  @Override
  public void removeTableModelListener(final TableModelListener l) {
    listeners.remove(l);
  }

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return columnClasses[columnIndex];
  }

  @Override
  public int getColumnCount() {
    return columnClasses.length;
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return columnName[columnIndex];
  }

  @Override
  public int getRowCount() {
    return data.length;
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    if (rowIndex < 0)
      return null;
    return data[rowIndex][columnIndex];
  }

  @Override
  public boolean isCellEditable(final int rowIndex, final int columnIndex) {
    return false;
  }

  @Override
  public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
  }

}
