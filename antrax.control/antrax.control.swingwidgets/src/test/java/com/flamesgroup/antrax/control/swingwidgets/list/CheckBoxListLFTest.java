package com.flamesgroup.antrax.control.swingwidgets.list;

import java.awt.*;

import javax.swing.*;

public class CheckBoxListLFTest {

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      JCheckBoxList list = new JCheckBoxList(new Object[] {
          "Item 1",
          "Item 2",
          "Item 3",
          "Item 4",
          "Item 5",
          "Item 6",
          "Item 7",
          "Item 8",
          "Item 9",
          "Item 10",
          "Item 11",
          "Item 12",
      });

      JFrame frame = new JFrame();
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      JPanel panel = new JPanel(new FlowLayout());
      panel.add(new JScrollPane(list.getComponent()));
      frame.add(panel);
      frame.setSize(500, 250);
      frame.setVisible(true);
      frame.setLocationRelativeTo(null);
    } catch (ClassNotFoundException | IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException ignored) {
    }

  }

}
