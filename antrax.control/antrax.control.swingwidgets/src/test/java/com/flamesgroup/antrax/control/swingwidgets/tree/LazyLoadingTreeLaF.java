package com.flamesgroup.antrax.control.swingwidgets.tree;

import java.util.ArrayList;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;

public class LazyLoadingTreeLaF {

  private static class LazyNode extends AbstractLazyLoadingTreeNode {

    private static final long serialVersionUID = 1L;

    public LazyNode(final String userObject, final JTree tree) {
      super(userObject, tree);
    }

    @Override
    public MutableTreeNode[] loadChildren(final TreeModel model) {
      ArrayList<MutableTreeNode> list = new ArrayList<>(5);
      for (int i = 0; i < 5; i++) {
        list.add(new LazyNode("Node " + i, getTree()));
        try {
          Thread.sleep(250);
        } catch (InterruptedException ignored) {
          break;
        }
      }
      list.add(new DefaultMutableTreeNode("Leaf"));
      return list.toArray(new MutableTreeNode[list.size()]);
    }

  }

  public static void main(final String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      JFrame frame = new JFrame("");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setSize(400, 350);
      frame.setLocationRelativeTo(null);

      JTree tree = new JTree();
      TreeFactory.applyTreeLazyLoading(tree);
      DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Root", true);
      DefaultTreeModel model = new DefaultTreeModel(rootNode);
      for (int i = 0; i < 5; i++) {
        rootNode.add(new LazyNode("Node " + i, tree));
      }
      tree.setModel(model);


      frame.add(new JScrollPane(tree));
      frame.setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
