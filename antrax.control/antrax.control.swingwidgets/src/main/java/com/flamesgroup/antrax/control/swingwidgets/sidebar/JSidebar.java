package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import java.awt.*;

import javax.swing.*;
import javax.swing.tree.TreeModel;

public class JSidebar extends JTree {

  private static final long serialVersionUID = -7382272602433924347L;

  public final static String CELL_MARGIN_LEFT = "Sidebar.cellMarginLeft";
  public final static String CELL_MARGIN_RIGHT = "Sidebar.cellMarginRight";
  public final static String NODE_MARGIN_Y = "Sidebar.badgeMraginY";
  public final static String BADGE_SPACE = "Sidebar.badgeSpace";
  public final static String TEXT_INDENT = "Sidebar.textIndent";
  public final static String ICON_INDENT = "Sidebar.iconIndent";
  public final static String TEXT_COLOR = "Sidebar.textColor";
  public final static String TEXT_SELECTED_COLOR = "Sidebar.textSelectedColor";
  public final static String TEXT_SHADOW_COLOR = "Sidebar.textShadowColor";

  public final static String ROW_SELECTED_COLOR1 = "Sidebar.rowSelectedColor1";
  public final static String ROW_SELECTED_COLOR2 = "Sidebar.rowSelectedColor2";
  public final static String ROW_STROKE_COLOR = "Sidebar.rowStrokeColor";

  public final static String BACKGROUND_COLOR1 = "Sidebar.backgroundColor1";
  public final static String BACKGROUND_COLOR2 = "Sidebar.backgroundColor2";

  public enum SkinType {
    DEFAULT,
    MACOS,
    VISTA,
    OFFICE_2008
  }

  public JSidebar() {
    super();
    defaultUI();
  }

  public JSidebar(final TreeModel newModel) {
    super(newModel);
    defaultUI();
  }

  private void defaultUI() {
    setLargeModel(true);
    setRootVisible(false);
    setShowsRootHandles(true);
    setFont(new Font("Helvetica", Font.BOLD, 11));
    setSkin(SkinType.MACOS);

    setRowHeight(20);
    setUI(new SidebarUI());
    setCellRenderer(new SidebarCellRenderer());
  }

  private void setSkin(final SkinType skin) {
    putClientProperty(TEXT_INDENT, 2);
    putClientProperty(ICON_INDENT, 3);
    putClientProperty(CELL_MARGIN_LEFT, 2);
    putClientProperty(CELL_MARGIN_RIGHT, 4);
    putClientProperty(NODE_MARGIN_Y, 2);
    putClientProperty(BADGE_SPACE, 2);

    putClientProperty(TEXT_COLOR, new Color(0x536279));
    putClientProperty(TEXT_SELECTED_COLOR, Color.WHITE);
    putClientProperty(TEXT_SHADOW_COLOR, new Color(0x333333));

    //    // darkness
    //    putClientProperty(BACKGROUND_COLOR1, new Color(0xc1c4c5));
    //    putClientProperty(BACKGROUND_COLOR2, new Color(0xd7d9d9));

    putClientProperty(BACKGROUND_COLOR1, new Color(0xd1d7e2));
    putClientProperty(BACKGROUND_COLOR2, new Color(0xd1d7e2));

    //    putClientProperty(BACKGROUND_COLOR1, new Color(0xd7d9d9));
    //    putClientProperty(BACKGROUND_COLOR2, new Color(0xdde3ea));

    switch (skin) {
      case DEFAULT:
      default:
        putClientProperty(ROW_SELECTED_COLOR1, new Color(0xa2b2ca));
        putClientProperty(ROW_SELECTED_COLOR2, new Color(0x7182a7));
        putClientProperty(ROW_STROKE_COLOR, new Color(0x959ec5));
        break;

      case MACOS:
        putClientProperty(ROW_SELECTED_COLOR1, new Color(0x5D94E6));
        putClientProperty(ROW_SELECTED_COLOR2, new Color(0x1452A9));
        putClientProperty(ROW_STROKE_COLOR, new Color(0x4580C8));
        break;

      case VISTA:
        putClientProperty(ROW_SELECTED_COLOR1, new Color(0xf2f9fd));
        putClientProperty(ROW_SELECTED_COLOR2, new Color(0xd7f0fc));
        putClientProperty(ROW_STROKE_COLOR, new Color(0xb6e6fb));
        break;

      case OFFICE_2008:
        putClientProperty(ROW_SELECTED_COLOR1, new Color(0xfff3d4));
        putClientProperty(ROW_SELECTED_COLOR2, new Color(0xffe6aa));
        putClientProperty(ROW_STROKE_COLOR, new Color(0xffde90));
        break;
    }

  }

  @Override
  protected void paintComponent(final Graphics g) {
    Graphics2D gc2D = (Graphics2D) g.create();
    try {
      // Paint component background yourself
      GradientPaint gradient = new GradientPaint(
          0, 0, (Color) getClientProperty(BACKGROUND_COLOR1),
          0, (int) (getHeight() * 0.9), (Color) getClientProperty(BACKGROUND_COLOR2));
      gc2D.setPaint(gradient);
      gc2D.fillRect(0, 0, getWidth(), getHeight());

      // Paint the background for the selected entries
      int[] rows = getSelectionModel().getSelectionRows();
      if (rows != null && rows.length > 0) {
        Color color1 = (Color) getClientProperty(ROW_SELECTED_COLOR1);
        Color color2 = (Color) getClientProperty(ROW_SELECTED_COLOR2);
        Color colorStroke = (Color) getClientProperty(ROW_STROKE_COLOR);
        for (int i = 0; i < rows.length; i++) {
          int selectedRow = rows[i];
          if (selectedRow >= 0 && isVisible(getPathForRow(selectedRow))) {
            gc2D = (Graphics2D) g.create();
            // Get the bounds of the selected row
            Rectangle bounds = getRowBounds(selectedRow);
            paintSelectedBackground(gc2D, 0, bounds.y, getWidth(), bounds.height,
                color1, color2, colorStroke);
          }
        }
      }

      //      DropLocation loc = getDropLocation();
      //      if (loc != null) {
      //      }

    } finally {
      if (gc2D != null) {
        gc2D.dispose();
      }
    }

    setOpaque(false);
    super.paintComponent(g);
    setOpaque(true);
  }

  private void paintSelectedBackground(final Graphics2D gc, final int x, final int y, final int w, final int h,
      final Color color1, final Color color2, final Color colorStroke) {
    // create a GradientPaint here and set the graphics context
    GradientPaint gradient = new GradientPaint(
        x, y, color1,
        x, y + (h * 9 / 10), color2);
    gc.setPaint(gradient);

    int arc = 3;
    int dw = 1;
    gc.fillRoundRect(x, y, w - dw, h - 1, arc, arc);
    gc.setColor(colorStroke);
    gc.drawRoundRect(x, y, w - dw - 1, h - 1, arc, arc);
  }

}
