package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import com.flamesgroup.antrax.control.swingwidgets.badge.JBadge;

import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class SidebarNode extends DefaultMutableTreeNode {

  private static final long serialVersionUID = -5856231236887831631L;

  private Icon icon;
  private String name;
  private final List<JBadge> badges = new LinkedList<>();

  public static class SidebarNodeBuilder {
    private String name;
    private boolean allowsChildren = true;
    private Icon icon;
    private Object userObject;

    public SidebarNodeBuilder setName(final String name) {
      this.name = name;
      return this;
    }

    public SidebarNodeBuilder setAllowsChildren(final boolean allowsChildren) {
      this.allowsChildren = allowsChildren;
      return this;
    }

    public SidebarNodeBuilder setIcon(final Icon icon) {
      this.icon = icon;
      return this;
    }

    public SidebarNodeBuilder setUserObject(final Object userObject) {
      this.userObject = userObject;
      return this;
    }

    public SidebarNode build() {
      return new SidebarNode(this);
    }

  }

  protected SidebarNode(final SidebarNodeBuilder builder) {
    super(builder.userObject, builder.allowsChildren);
    if (builder.name == null) {
      this.name = (builder.userObject == null) ? this.toString()
          : builder.userObject.toString();
    } else {
      this.name = builder.name;
    }
    setIcon(builder.icon);
  }

  public Icon getIcon() {
    return icon;
  }

  public void setIcon(final Icon icon) {
    this.icon = icon;
  }

  public void addBadge(final JBadge badge) {
    badges.add(badge);
  }

  public void removeBadge(final JBadge badge) {
    badges.remove(badge);
  }

  public List<JBadge> getBadges() {
    return badges;
  }

  public int getBadgesCount() {
    return badges.size();
  }

  public boolean hasVisibleBadges() {
    if (badges.isEmpty())
      return false;

    boolean hasVisible = false;
    for (JBadge bg : badges) {
      if (bg.isVisible())
        hasVisible = true;
    }

    return hasVisible;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    if (name == null) {
      return super.toString();
    } else {
      return name;
    }
  }

}
