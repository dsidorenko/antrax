package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.*;

import javax.swing.*;

public class JCodeEditor extends JTextArea {

  private static final long serialVersionUID = -1491808513729618724L;

  public JCodeEditor() {
    initUI();
  }

  private void initUI() {
    setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
  }

}
