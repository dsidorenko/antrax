package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class LogicLexeme {

  public static final char EMPTY_SYMBOL = (char) 0;

  public static final char AND = '&';
  public static final char OR = '|';
  public static final char NOT = '!';
  public static final char EQ = '=';
  public static final char RIGHT_BRACE = ')';
  public static final char LEFT_BRACE = '(';
  public static final char GT = '>';
  public static final char LT = '<';

  public static boolean isLexeme(final char symbol) {
    switch (symbol) {
      case AND:
      case OR:
      case NOT:
      case EQ:
      case RIGHT_BRACE:
      case LEFT_BRACE:
      case GT:
      case LT:
        return true;

      default:
        return false;
    }
  }

}
