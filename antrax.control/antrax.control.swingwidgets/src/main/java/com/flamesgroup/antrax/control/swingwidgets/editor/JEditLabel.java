package com.flamesgroup.antrax.control.swingwidgets.editor;

import javax.swing.*;

public class JEditLabel extends JLabel {

  private static final long serialVersionUID = -4872698588890575636L;

  public static class JEditLabelBuilder {
    private String text;
    private Icon icon;
    private int horizontalAlignment;

    public JEditLabelBuilder() {
      setText("");
      setHorizontalAlignment(SwingConstants.LEADING);
    }

    public JEditLabelBuilder setText(final String text) {
      this.text = text;
      return this;
    }

    public JEditLabelBuilder setIcon(final Icon icon) {
      this.icon = icon;
      return this;
    }

    public JEditLabelBuilder setHorizontalAlignment(final int horizontalAlignment) {
      this.horizontalAlignment = horizontalAlignment;
      return this;
    }

    public JEditLabel build() {
      return new JEditLabel(this);
    }

  }

  public JEditLabel(final String text) {
    this((new JEditLabelBuilder()).setText(text));
  }

  protected JEditLabel(final JEditLabelBuilder builder) {
    super();
    setText(builder.text);
    setIcon(builder.icon);
    setHorizontalAlignment(builder.horizontalAlignment);
  }

}
