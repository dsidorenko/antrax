package com.flamesgroup.antrax.control.swingwidgets.table;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class MetalTableUI extends BasicTableUI {
  public static final Color evenRowColor = new Color(0xf1f5fa);
  public static final Color oddRowColor = Color.WHITE;
  public static final Color selectedRowColor = Color.WHITE;
  public static final Color gridColor = new Color(0xd9d9d9);

  public static final Color selectedFocusedColor = new Color(0x3D80DF);
  public static final Color selectedNotFocusedColor = new Color(0xC0C0C0);

  @Override
  protected void installDefaults() {
    super.installDefaults();
    table.setShowGrid(false);
    table.setRowHeight(table.getFont().getSize() + 6);
    table.setGridColor(gridColor);
    table.getSelectionModel().addListSelectionListener(
        new ListSelectionListener() {
          @Override
          public void valueChanged(final ListSelectionEvent e) {
            table.repaint();
          }
        });

    table.addFocusListener(new FocusListener() {
      @Override
      public void focusGained(final FocusEvent e) {
        table.repaint();
      }

      @Override
      public void focusLost(final FocusEvent e) {
        table.repaint();
      }
    });

  }

  @Override
  public void paint(final Graphics g, final JComponent c) {
    JTable table = (JTable) c;
    int rowHeight = table.getRowHeight();
    if (rowHeight == 0) {
      return;
    }

    Rectangle clip = g.getClipBounds();
    int x = clip.x;
    int y = clip.y;
    int w = clip.width;
    int h = clip.height;
    int row = 0;

    int y2 = y + h;

    if (y != 0) {
      int diff = y % rowHeight;
      row = y / rowHeight;
      y -= diff;
    }

    for (; y < y2; y += rowHeight, row++) {
      boolean isRowSelected = table.isRowSelected(row);
      Color color = (row % 2 == 0) ? evenRowColor : oddRowColor;
      if (isRowSelected) {
        color = c.hasFocus() ? selectedFocusedColor : selectedNotFocusedColor;
        //        Color color2 = c.hasFocus() ? new Color(0x1452A9) : new Color(0xD9D9D9);
        //        paintSelectedBackground((Graphics2D)g, x, y, w, rowHeight, color, color2);
      }
      g.setColor(color);

      rowHeight = table.getRowHeight(row);
      if (rowHeight == 0) {
        rowHeight = table.getRowHeight();
      }
      g.fillRect(x, y, w, rowHeight);
    }

    super.paint(g, c);

    x = 0;
    g.setColor(gridColor);
    TableColumnModel vModel = table.getColumnModel();
    for (int i = 0; i < vModel.getColumnCount(); i++) {
      TableColumn vColumn = vModel.getColumn(i);
      x += vColumn.getWidth();
      if ((x >= clip.x) && (x <= clip.x + clip.width)) {
        g.drawLine(x - 1, clip.y, x - 1, clip.y + clip.height);
      }
    }
  }

  //  private void paintSelectedBackground(Graphics2D gc, int x, int y, int w, int h,
  //      Color color1, Color color2) {
  //    GradientPaint gradient = new GradientPaint(
  //        x, y, color1,
  //        x, y + (h * 9/10), color2);
  //    gc.setPaint(gradient);
  //    gc.fillRect(x, y, w, h);
  //  }
}
