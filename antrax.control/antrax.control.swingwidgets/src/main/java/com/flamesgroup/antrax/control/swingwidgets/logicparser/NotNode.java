package com.flamesgroup.antrax.control.swingwidgets.logicparser;

import java.util.List;

public class NotNode extends OperatorNode {

  @Override
  public int getPrecedence() {
    return Precedence.NOT;
  }

  @Override
  public boolean evaluate() {
    return !getLeft().evaluate();
  }

  /**
   * Overridden to pop only one value.
   */
  @Override
  public void popValues(final List<Node> values) throws NoOperandsException {
    if (values.size() < 1) {
      throw new NoOperandsException("No operands for unary operator.");
    }
    setLeft(values.remove(0));
  }

  @Override
  public String toString() {
    return getLeft() + " " + LogicToken.NOT.getLexeme();
  }

}
