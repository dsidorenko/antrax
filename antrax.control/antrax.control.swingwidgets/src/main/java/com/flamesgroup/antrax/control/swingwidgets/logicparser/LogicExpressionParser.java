package com.flamesgroup.antrax.control.swingwidgets.logicparser;

import java.util.LinkedList;
import java.util.List;

public class LogicExpressionParser {

  /**
   * Contains the current set of completed nodes. This is a workspace for the
   * parser.
   */
  private final List<Node> nodeStack = new LinkedList<>();

  /**
   * Contains operator nodes that don't yet have values. This is a workspace
   * for the parser.
   */
  private final List<OperatorNode> operatorsStack = new LinkedList<>();

  private LogicExpressionTokenizer tokenizer;

  private final StringNodeFactory defaultStrNodeFactory = new DefaultStringNodeFactory();

  /**
   * Pushes a new operator onto the operator stack, resolving existing
   * operations as needed.
   *
   * @throws LogicExpressionSyntaxException
   * @throws NoOperandsException
   */
  private void pushAndResolveOperator(final OperatorNode node) throws LogicExpressionSyntaxException {
    // If node is null then it's just a group marker
    if (node == null) {
      pushOperator(node);
      return;
    }

    while (operatorsStack.size() > 0) {
      OperatorNode top = operatorsStack.get(0);

      // If the top is a spacer then don't pop anything
      if (top == null) {
        break;
      }

      // If the top node has a lower precedence then let it stay
      if (top.getPrecedence() < node.getPrecedence()) {
        break;
      }

      // Remove the top node
      popOperator();

      // Let it fill its branches
      try {
        top.popValues(nodeStack);
      } catch (NoOperandsException e) {
        throw new LogicExpressionSyntaxException(e, getErrorIndex());
      }

      // Stick it on the resolved node stack
      nodeStack.add(0, top);
    }

    // Add the new node to the opp stack
    pushOperator(node);
  }

  private void pushOperator(final OperatorNode node) {
    operatorsStack.add(0, node);
  }

  private OperatorNode popOperator() throws LogicExpressionSyntaxException {
    if (operatorsStack.isEmpty()) {
      throw new LogicExpressionSyntaxException("Logic expression syntax error.", getErrorIndex());
    }

    return operatorsStack.remove(0);
  }

  /**
   * Resolves all pending opp nodes on the stack until the next group marker
   * is reached.
   */
  private void resolveGroup() throws LogicExpressionSyntaxException {
    try {
      OperatorNode top;
      while ((top = popOperator()) != null) {
        // Let it fill its branches
        top.popValues(nodeStack);

        // Stick it on the resolved node stack
        nodeStack.add(0, top);
      }
    } catch (NoOperandsException e) {
      throw new LogicExpressionSyntaxException(e, getErrorIndex());
    }
  }

  private void pushOperatorNode(final LogicToken operatorToken) throws LogicExpressionSyntaxException {
    switch (operatorToken) {
      case AND:
        pushAndResolveOperator(new AndNode());
        break;

      case OR:
        pushAndResolveOperator(new OrNode());
        break;

      case NOT:
        pushAndResolveOperator(new NotNode());
        break;

      case EQUAL:
        pushAndResolveOperator(new EqualNode());
        break;

      case NOT_EQ:
        pushAndResolveOperator(new NotNode());
        // Sneak the regular node in. The NOT will
        // be resolved when the next opp comes along.
        pushOperator(new EqualNode());
        break;

      case GE:
        pushAndResolveOperator(new NotNode());
        // Similar strategy to NOT_EQ above, except this
        // is NOT less than
        pushOperator(new LessThanNode());
        break;

      case LE:
        pushAndResolveOperator(new NotNode());
        // Similar strategy to NOT_EQ above, except this
        // is NOT greater than
        pushOperator(new GreaterThanNode());
        break;

      case GT:
        pushAndResolveOperator(new GreaterThanNode());
        break;

      case LT:
        pushAndResolveOperator(new LessThanNode());
        break;

      case END:
        break;
    }
  }

  private int getErrorIndex() {
    if (tokenizer == null) {
      return -1;
    } else {
      int i = tokenizer.getIndex();
      if (i > 0) {
        i--;
      }
      return i;
    }
  }

  private void init() {
    nodeStack.clear();
    operatorsStack.clear();
  }

  public Node parseExpression(final String expression) throws LogicExpressionSyntaxException {
    return parseExpression(expression, null);
  }

  public Node parseExpression(final String expression, final LogicToken defaultOperator)
      throws LogicExpressionSyntaxException {

    return parseExpression(expression, defaultOperator, null);
  }

  public Node parseExpression(final String expression, LogicToken defaultOperator, StringNodeFactory factory)
      throws LogicExpressionSyntaxException {

    init();

    if (defaultOperator != null) {
      if (!defaultOperator.isOperator()
          || defaultOperator == LogicToken.LEFT_BRACE
          || defaultOperator == LogicToken.RIGHT_BRACE) {
        defaultOperator = LogicToken.AND;
      }
    }

    if (factory == null) {
      factory = defaultStrNodeFactory;
    }

    StringNode currStringNode = null;
    StringNode prevStringNode = null;

    // Cheat a little and start an artificial
    // group right away. It makes finishing easier.
    pushAndResolveOperator(null);

    tokenizer = new LogicExpressionTokenizer(expression);
    while (tokenizer.hasMoreTokens()) {
      LogicToken token = tokenizer.nextToken();
      if (token != LogicToken.STRING) {
        prevStringNode = currStringNode;
        currStringNode = null;
      }

      switch (token) {
        case STRING:
          if (currStringNode == null) {
            prevStringNode = currStringNode;
            currStringNode = factory.createStringNode(tokenizer.getTokenValue());
            nodeStack.add(0, currStringNode);
          } else {
            if (defaultOperator == null) {
              // add to existing
              currStringNode.appendValue(" ");
              currStringNode.appendValue(tokenizer.getTokenValue());
            } else {
              pushOperatorNode(defaultOperator);
              StringNode nextStringNode = factory.createStringNode(tokenizer.getTokenValue());
              nodeStack.add(0, nextStringNode);

              prevStringNode = currStringNode;
              currStringNode = nextStringNode;
            }
          }
          break;

        case RIGHT_BRACE:
          // Closeout the current group
          resolveGroup();
          break;

        case LEFT_BRACE:
          // Push a group marker
          pushAndResolveOperator(null);
          break;

        default:
          if (defaultOperator != null && token == LogicToken.NOT && prevStringNode != null) {
            pushOperatorNode(defaultOperator);
          }

          pushOperatorNode(token);
          break;
      }
    }

    // Finish off the rest of the opps
    resolveGroup();

    if (nodeStack.size() == 0) {
      throw new LogicExpressionSyntaxException("No nodes created.", getErrorIndex());
    }

    if (nodeStack.size() > 1) {
      throw new LogicExpressionSyntaxException("Extra nodes created.", getErrorIndex());
    }

    if (operatorsStack.size() != 0) {
      throw new LogicExpressionSyntaxException("Unused operator nodes exist.", getErrorIndex());
    }

    return nodeStack.get(0);
  }

}
