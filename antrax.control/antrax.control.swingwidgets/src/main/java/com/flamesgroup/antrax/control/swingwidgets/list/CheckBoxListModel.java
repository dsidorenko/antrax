package com.flamesgroup.antrax.control.swingwidgets.list;

import javax.swing.*;

public interface CheckBoxListModel extends ListModel {

  boolean isChecked(Object item);

  void setChecked(Object item, boolean checked);

  void invertChecking(Object item);

  void addItem(Object item);

  void insertItemAt(Object item, int index);

  void removeItemAt(int index);

  boolean removeItem(Object item);

  void removeAllItems();

  Object[] getCheckedItems();

}
