package com.flamesgroup.antrax.control.swingwidgets.field;

import java.awt.event.ActionEvent;

public interface DelegateAction {

  boolean delegateActionPerformed(ActionEvent event);

}
