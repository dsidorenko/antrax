package com.flamesgroup.antrax.control.swingwidgets.badge;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class DefaultBadgeModel implements BadgeModel {

  /**
   * Only one {@code ChangeEvent} is needed per button model instance since
   * the event's only state is the source property. The source of events
   * generated is always "this"
   */
  protected transient ChangeEvent changeEvent = null;

  /**
   * Stores the listeners on this model
   */
  protected EventListenerList listenerList = new EventListenerList();

  private Color color;
  private String text;
  private boolean visible = false;
  private Icon icon;

  @Override
  public Color getColor() {
    return color;
  }

  @Override
  public String getText() {
    return text;
  }

  @Override
  public boolean isVisible() {
    return visible;
  }

  @Override
  public void setColor(final Color color) {
    if (getColor() != null && getColor().equals(color)) {
      return;
    }

    this.color = color;
    fireStateChanged();
  }

  @Override
  public void setText(final String text) {
    String oldText = getText();
    if ((oldText == null && text == null) || (oldText != null && oldText.equals(text))) {
      return;
    }

    this.text = text;
    fireStateChanged();
  }

  @Override
  public void setVisible(final boolean visible) {
    if (isVisible() == visible) {
      return;
    }

    this.visible = visible;
    fireStateChanged();
  }

  @Override
  public Icon getIcon() {
    return icon;
  }

  @Override
  public void setIcon(final Icon icon) {
    this.icon = icon;
    fireStateChanged();
  }

  @Override
  public void addChangeListener(final ChangeListener l) {
    listenerList.add(ChangeListener.class, l);
  }

  @Override
  public void removeChangeListener(final ChangeListener l) {
    listenerList.remove(ChangeListener.class, l);
  }

  protected void fireStateChanged() {
    // Guaranteed to return a non-null array
    Object[] listeners = listenerList.getListenerList();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ChangeListener.class) {
        // Lazily create the event:
        if (changeEvent == null) {
          changeEvent = new ChangeEvent(this);
        }
        ((ChangeListener) listeners[i + 1]).stateChanged(changeEvent);
      }
    }
  }

}
