package com.flamesgroup.antrax.control.swingwidgets.tablecombobox;

import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class TableComboBoxModel implements TableModel, ComboBoxModel {

  private final TableModel tableModel;
  private final int comboBoxColumnIndex;
  private final ListSelectionModel listSelectionModel;
  private final List<ListDataListener> listDataListeners;

  public TableComboBoxModel(final TableModel tablemodel, final int i, final ListSelectionModel listSelectionModel) {
    listDataListeners = new ArrayList<>();
    tableModel = tablemodel;
    comboBoxColumnIndex = i;
    this.listSelectionModel = listSelectionModel;
    listSelectionModel.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        //selectedValueChanged(e.getSource());
      }
    });
  }

  private void selectedValueChanged(final Object obj) {
    fireItemStateChanged(new ListDataEvent(obj, 0, -1, -1));
  }

  private void fireItemStateChanged(final ListDataEvent event) {
    for (ListDataListener l : listDataListeners) {
      l.contentsChanged(event);
    }
  }

  //
  // 'ComboBoxModel' interface
  //

  @Override
  public Object getSelectedItem() {
    return getValueAt(listSelectionModel.getLeadSelectionIndex(), comboBoxColumnIndex);
  }

  @Override
  public void setSelectedItem(final Object anItem) {
    int i = 0;
    for (int j = getRowCount(); i < j; i++) {
      if (getValueAt(i, comboBoxColumnIndex) == anItem) {
        listSelectionModel.setSelectionInterval(i, i);
        selectedValueChanged(this);
      }
    }
  }

  @Override
  public Object getElementAt(final int index) {
    return getValueAt(index, comboBoxColumnIndex);
  }

  @Override
  public int getSize() {
    return getRowCount();
  }

  @Override
  public void addListDataListener(final ListDataListener l) {
    listDataListeners.add(l);
  }

  @Override
  public void removeListDataListener(final ListDataListener l) {
    listDataListeners.remove(l);
  }

  //
  // 'TableModel' interface
  //

  @Override
  public Class<?> getColumnClass(final int columnIndex) {
    return tableModel.getColumnClass(columnIndex);
  }

  @Override
  public int getColumnCount() {
    return tableModel.getColumnCount();
  }

  @Override
  public String getColumnName(final int columnIndex) {
    return tableModel.getColumnName(columnIndex);
  }

  @Override
  public int getRowCount() {
    return tableModel.getRowCount();
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return tableModel.getValueAt(rowIndex, columnIndex);
  }

  @Override
  public boolean isCellEditable(final int rowIndex, final int columnIndex) {
    return tableModel.isCellEditable(rowIndex, columnIndex);
  }

  @Override
  public void addTableModelListener(final TableModelListener l) {
    tableModel.addTableModelListener(l);
  }

  @Override
  public void removeTableModelListener(final TableModelListener l) {
    tableModel.removeTableModelListener(l);
  }

  @Override
  public void setValueAt(final Object value, final int rowIndex, final int columnIndex) {
    tableModel.setValueAt(value, rowIndex, columnIndex);
  }

}
