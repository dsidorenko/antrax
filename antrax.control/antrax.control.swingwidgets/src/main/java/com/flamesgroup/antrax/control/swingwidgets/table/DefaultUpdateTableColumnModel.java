package com.flamesgroup.antrax.control.swingwidgets.table;

import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class DefaultUpdateTableColumnModel implements UpdateTableColumnModel {

  private final List<UpdateTableColumn> columns = new LinkedList<>();

  private final List<ColumnSortOrder> defaultSortSequence = new LinkedList<>();

  @Override
  public UpdateTableColumn addColumn(final String name, final Class<?> type) {
    UpdateTableColumn column = new UpdateTableColumnImpl(name, type);
    columns.add(column);
    return column;
  }

  @Override
  public UpdateTableColumn getColumnAt(final int columnIndex) {
    return columns.get(columnIndex);
  }

  @Override
  public ColumnSortOrder[] getDefaultSortSequence() {
    return defaultSortSequence.toArray(new ColumnSortOrder[defaultSortSequence.size()]);
  }

  @Override
  public int size() {
    return columns.size();
  }

  @Override
  public int columnIndexOf(final UpdateTableColumn column) {
    return columns.indexOf(column);
  }

  @Override
  public void addColumnSortSequence(final UpdateTableColumn column, final SortOrder sortOrder) {
    defaultSortSequence.add(new ColumnSortOrder(column, sortOrder));

  }

}
