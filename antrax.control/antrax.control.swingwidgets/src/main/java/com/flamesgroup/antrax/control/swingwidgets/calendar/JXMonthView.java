package com.flamesgroup.antrax.control.swingwidgets.calendar;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.swing.*;
import javax.swing.border.Border;

/**
 * Component that displays a month calendar which can be used to select a day or
 * range of days. By default the <code>JXMonthView</code> will display a single
 * calendar using the current month and year, using <code>Calendar.SUNDAY</code>
 * as the first day of the week.
 * <p>
 * The <code>JXMonthView</code> can be configured to display more than one
 * calendar at a time by calling <code>setPreferredCalCols</code>/
 * <code>setPreferredCalRows</code>. These methods will set the preferred number
 * of calendars to use in each column/row. As these values change, the
 * <code>Dimension</code> returned from <code>getMinimumSize</code> and
 * <code>getPreferredSize</code> will be updated. The following example shows
 * how to create a 2x2 view which is contained within a <code>JFrame</code>:
 * <p>
 * <pre>
 * JXMonthView monthView = new JXMonthView();
 * monthView.setPreferredCols(2);
 * monthView.setPreferredRows(2);
 *
 * JFrame frame = new JFrame();
 * frame.getContentPane().add(monthView);
 * frame.pack();
 * frame.setVisible(true);
 * </pre>
 * <p>
 * <code>JXMonthView</code> can be further configured to allow any day of the
 * week to be considered the first day of the week. Character representation of
 * those days may also be set by providing an array of strings.
 * <p>
 * <pre>
 * monthView.setFirstDayOfWeek(Calendar.MONDAY);
 * monthView.setDaysOfTheWeek(new String[] { &quot;S&quot;, &quot;M&quot;, &quot;T&quot;, &quot;W&quot;, &quot;R&quot;, &quot;F&quot;, &quot;S&quot; });
 * </pre>
 * <p>
 * This component supports flagging days. These flagged days, which must be
 * provided in sorted order, are displayed in a bold font. This can be used to
 * inform the user of such things as scheduled appointment.
 * <p>
 * <pre>
 * // Create some dates that we want to flag as being important.
 * Calendar cal1 = Calendar.getInstance();
 * cal1.set(2004, 1, 1);
 * Calendar cal2 = Calendar.getInstance();
 * cal2.set(2004, 1, 5);
 *
 * long[] flaggedDates = new long[] { cal1.getTimeInMillis(),
 *     cal2.getTimeInMillis(), System.currentTimeMillis() };
 *
 * // Sort them in ascending order.
 * java.util.Arrays.sort(flaggedDates);
 * monthView.setFlaggedDates(flaggedDates);
 * </pre>
 * <p>
 * Applications may have the need to allow users to select different ranges of
 * dates. There are four modes of selection that are supported, single,
 * multiple, week and no selection. Once a selection is made an action is fired,
 * with exception of the no selection mode, to inform listeners that selection
 * has changed.
 * <p>
 * <pre>
 * // Change the selection mode to select full weeks.
 * monthView.setSelectionMode(JXMonthView.WEEK_SELECTION);
 *
 * // Add an action listener that will be notified when the user
 * // changes selection via the mouse.
 * monthView.addActionListener(new ActionListener() {
 *   public void actionPerformed(ActionEvent e) {
 *     System.out.println(((JXMonthView) e.getSource()).getSelectedDateSpan());
 *  }
 * });
 * </pre>
 *
 * @version $Revision: 1.2 $
 */
public class JXMonthView extends JComponent {
  private static final long serialVersionUID = 6413496319982933738L;
  /**
   * Mode that disallows selection of days from the calendar.
   */
  public static final int NO_SELECTION = 0;
  /**
   * Mode that allows for selection of a single day.
   */
  public static final int SINGLE_SELECTION = 1;
  /**
   * Mode that allows for selecting of multiple consecutive days.
   */
  public static final int MULTIPLE_SELECTION = 2;
  /**
   * Mode where selections consisting of more than 7 days will snap to a full
   * week.
   */
  public static final int WEEK_SELECTION = 3;

  /**
   * Insets used in determining the rectangle for the month string background.
   */
  protected Insets monthStringInsets = new Insets(0, 8, 0, 8);

  private static final int MONTH_DROP_SHADOW = 1;
  private static final int MONTH_LINE_DROP_SHADOW = 2;
  private static final int WEEK_DROP_SHADOW = 4;

  private int boxPaddingX = 3;
  private int boxPaddingY = 3;
  private static final int CALENDAR_SPACING = 10;
  private static final int DAYS_IN_WEEK = 7;
  private static final int MONTHS_IN_YEAR = 12;

  /**
   * Keeps track of the first date we are displaying. We use this as a restore
   * point for the calendar.
   */
  private long firstDisplayedDate;
  private int firstDisplayedMonth;
  private int firstDisplayedYear;

  private long lastDisplayedDate;
  private Font derivedFont;

  /**
   * Beginning date of selection. -1 if no date is selected.
   */
  private long startSelectedDate = -1;

  /**
   * End date of selection. -1 if no date is selected.
   */
  private long endSelectedDate = -1;

  /**
   * For multiple selection we need to record the date we pivot around.
   */
  private long pivotDate = -1;

  /** Bounds of the selected date including its visual border. */
  // private final Rectangle selectedDateRect = new Rectangle();
  /**
   * The number of calendars able to be displayed horizontally.
   */
  private int numCalCols = 1;

  /**
   * The number of calendars able to be displayed vertically.
   */
  private int numCalRows = 1;

  private int minCalCols = 1;
  private int minCalRows = 1;
  private long today;
  private long[] flaggedDates;
  private int selectionMode = SINGLE_SELECTION;
  private int boxHeight;
  private int boxWidth;
  private int calendarWidth;
  private int calendarHeight;
  private int firstDayOfWeek = Calendar.MONDAY;
  private int startX;
  private int startY;
  private static final int dropShadowMask = MONTH_DROP_SHADOW;
  private boolean dirty = false;
  private boolean antiAlias = false;
  private boolean ltr;
  private boolean asKirkWouldSay_FIRE = false;
  private final Calendar cal;
  private String[] daysOfTheWeek;
  private final String[] monthsOfTheYear;
  private final Dimension dim = new Dimension();
  private final Rectangle bounds = new Rectangle();
  private final Rectangle dirtyRect = new Rectangle();
  private Color todayBackgroundColor;
  private Color monthStringBackground = Color.LIGHT_GRAY;
  private Color selectedBackground = Color.LIGHT_GRAY;
  private final SimpleDateFormat dayOfMonthFormatter = new SimpleDateFormat("d");
  private String actionCommand = "selectionChanged";
  private Timer todayTimer = null;

  /**
   * Create a new instance of the <code>JXMonthView</code> class using the
   * month and year of the current day as the first date to display.
   */
  public JXMonthView() {
    this(new Date().getTime());
  }

  /**
   * Create a new instance of the <code>JXMonthView</code> class using the
   * month and year from <code>initialTime</code> as the first date to
   * display.
   *
   * @param initialTime The first month to display.
   */
  public JXMonthView(final long initialTime) {
    super();

    ltr = getComponentOrientation().isLeftToRight();

    // Set up calendar instance.
    cal = Calendar.getInstance(getLocale());
    cal.setFirstDayOfWeek(firstDayOfWeek);
    // Keep track of today.
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    today = cal.getTimeInMillis();

    cal.setTimeInMillis(initialTime);
    setFirstDisplayedDate(cal.getTimeInMillis());

    // Get string representation of the months of the year.
    cal.set(Calendar.MONTH, cal.getMinimum(Calendar.MONTH));
    cal.set(Calendar.DAY_OF_MONTH, cal
        .getActualMinimum(Calendar.DAY_OF_MONTH));
    monthsOfTheYear = new String[MONTHS_IN_YEAR];
    SimpleDateFormat fullMonthNameFormatter = new SimpleDateFormat("MMMM");
    for (int i = 0; i < MONTHS_IN_YEAR; i++) {
      monthsOfTheYear[i] = fullMonthNameFormatter.format(cal.getTime());
      cal.add(Calendar.MONTH, 1);
    }

    setOpaque(true);
    setBackground(Color.WHITE);
    setFont(new Font("Dialog", Font.PLAIN, 12));
    todayBackgroundColor = getForeground();

    // Restore original time value.
    cal.setTimeInMillis(firstDisplayedDate);

    enableEvents(AWTEvent.MOUSE_EVENT_MASK);
    enableEvents(AWTEvent.MOUSE_MOTION_EVENT_MASK);

    updateUI();
  }

  /**
   * Resets the UI property to a value from the current look and feel.
   */
  @Override
  public void updateUI() {
    super.updateUI();

    String[] daysOfTheWeek = (String[]) UIManager
        .get("JXMonthView.daysOfTheWeek");
    // Use some meaningful default if the UIManager doesn't have anything
    // for us.
    if (daysOfTheWeek == null) {
      daysOfTheWeek = new String[] {"Sn", "Mn", "Tu", "Wd", "Th", "Fr",
          "St"};
    }
    setDaysOfTheWeek(daysOfTheWeek);

    Color color = UIManager.getColor("JXMonthView.monthStringBackground");
    // Use some meaningful default if the UIManager doesn't have anything
    // for us.
    if (color == null) {
      color = Color.LIGHT_GRAY;
    }
    setMonthStringBackground(color);

    color = UIManager.getColor("JXMonthView.selectedBackground");
    // Use some meaningful default if the UIManager doesn't have anything
    // for us.
    if (color == null) {
      color = Color.LIGHT_GRAY;
    }
    setSelectedBackground(color);
  }

  /**
   * Returns the first displayed date.
   *
   * @return long The first displayed date.
   */
  public long getFirstDisplayedDate() {
    return firstDisplayedDate;
  }

  /**
   * Set the first displayed date. We only use the month and year of this
   * date. The <code>Calendar.DAY_OF_MONTH</code> field is reset to 1 and all
   * other fields, with exception of the year and month , are reset to 0.
   *
   * @param date The first displayed date.
   */
  public void setFirstDisplayedDate(final long date) {
    long old = firstDisplayedDate;

    cal.setTimeInMillis(date);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);

    firstDisplayedDate = cal.getTimeInMillis();
    firstDisplayedMonth = cal.get(Calendar.MONTH);
    firstDisplayedYear = cal.get(Calendar.YEAR);

    calculateLastDisplayedDate();
    firePropertyChange("firstDisplayedDate", old, firstDisplayedDate);

    repaint();
  }

  /**
   * Returns the last date able to be displayed. For example, if the last
   * visible month was April the time returned would be April 30, 23:59:59.
   *
   * @return long The last displayed date.
   */
  public long getLastDisplayedDate() {
    return lastDisplayedDate;
  }

  private void calculateLastDisplayedDate() {
    long old = lastDisplayedDate;

    cal.setTimeInMillis(firstDisplayedDate);

    // Figure out the last displayed date.
    cal.add(Calendar.MONTH, ((numCalCols * numCalRows) - 1));
    cal.set(Calendar.DAY_OF_MONTH, cal
        .getActualMaximum(Calendar.DAY_OF_MONTH));
    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);

    lastDisplayedDate = cal.getTimeInMillis();

    firePropertyChange("lastDisplayedDate", old, lastDisplayedDate);
  }

  /**
   * Moves the <code>date</code> into the visible region of the calendar. If
   * the date is greater than the last visible date it will become the last
   * visible date. While if it is less than the first visible date it will
   * become the first visible date.
   *
   * @param date Date to make visible.
   */
  public void ensureDateVisible(final long date) {
    if (date < firstDisplayedDate) {
      setFirstDisplayedDate(date);
    } else if (date > lastDisplayedDate) {
      cal.setTimeInMillis(date);
      int month = cal.get(Calendar.MONTH);
      int year = cal.get(Calendar.YEAR);

      cal.setTimeInMillis(lastDisplayedDate);
      int lastMonth = cal.get(Calendar.MONTH);
      int lastYear = cal.get(Calendar.YEAR);

      int diffMonths = month - lastMonth + ((year - lastYear) * 12);

      cal.setTimeInMillis(firstDisplayedDate);
      cal.add(Calendar.MONTH, diffMonths);
      setFirstDisplayedDate(cal.getTimeInMillis());
    }

    if (startSelectedDate != -1 || endSelectedDate != -1) {
      calculateDirtyRectForSelection();
    }
  }

  /**
   * Returns a date span of the selected dates. The result will be null if no
   * dates are selected.
   */
  public DateSpan getSelectedDateSpan() {
    DateSpan result = null;
    if (startSelectedDate != -1) {
      result = new DateSpan(new Date(startSelectedDate), new Date(
          endSelectedDate));
    }
    return result;
  }

  /**
   * Selects the dates in the DateSpan. This method will not change the
   * initial date displayed so the caller must update this if necessary. If we
   * are in SINGLE_SELECTION mode only the start time from the DateSpan will
   * be used. If we are in WEEK_SELECTION mode the span will be modified to be
   * valid if necessary.
   *
   * @param dateSpan DateSpan defining the selected dates. Passing
   *                 <code>null</code> will clear the selection.
   */
  public void setSelectedDateSpan(final DateSpan dateSpan) {
    DateSpan oldSpan = null;
    if (startSelectedDate != -1 && endSelectedDate != -1) {
      oldSpan = new DateSpan(startSelectedDate, endSelectedDate);
    }

    if (dateSpan == null) {
      startSelectedDate = -1;
      endSelectedDate = -1;
    } else {
      cal.setTimeInMillis(dateSpan.getStart());
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      startSelectedDate = cal.getTimeInMillis();

      if (selectionMode == SINGLE_SELECTION) {
        endSelectedDate = startSelectedDate;
      } else {
        cal.setTimeInMillis(dateSpan.getEnd());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        endSelectedDate = cal.getTimeInMillis();

        if (selectionMode == WEEK_SELECTION) {
          // Make sure if we are over 7 days we span full weeks.
          cal.setTimeInMillis(startSelectedDate);
          int count = 1;
          while (cal.getTimeInMillis() < endSelectedDate) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            count++;
          }
          if (count > 7) {
            // Make sure start date is on the beginning of the
            // week.
            cal.setTimeInMillis(startSelectedDate);
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
            if (dayOfWeek != firstDayOfWeek) {
              // Move the start date back to the first day of the
              // week.
              int daysFromStart = dayOfWeek - firstDayOfWeek;
              if (daysFromStart < 0) {
                daysFromStart += DAYS_IN_WEEK;
              }
              cal.add(Calendar.DAY_OF_MONTH, -daysFromStart);
              count += daysFromStart;
              startSelectedDate = cal.getTimeInMillis();
            }

            // Make sure we have full weeks. Otherwise modify the
            // end date.
            int remainder = count % 7;
            if (remainder != 0) {
              cal.setTimeInMillis(endSelectedDate);
              cal.add(Calendar.DAY_OF_MONTH, (7 - remainder));
              endSelectedDate = cal.getTimeInMillis();
            }
          }
        }
      }
      // Restore original time value.
      cal.setTimeInMillis(firstDisplayedDate);
    }

    repaint(dirtyRect);
    calculateDirtyRectForSelection();
    repaint(dirtyRect);

    // Fire property change.
    firePropertyChange("selectedDates", oldSpan, dateSpan);
  }

  /**
   * Returns the current selection mode for this JXMonthView.
   *
   * @return int Selection mode.
   */
  public int getSelectionMode() {
    return selectionMode;
  }

  /**
   * Set the selection mode for this JXMonthView.
   *
   * @throws IllegalArgumentException
   */
  public void setSelectionMode(final int mode) throws IllegalArgumentException {
    if (mode != SINGLE_SELECTION &&
        mode != MULTIPLE_SELECTION &&
        mode != WEEK_SELECTION &&
        mode != NO_SELECTION) {
      throw new IllegalArgumentException(mode + " is not a valid selection mode");
    }
    selectionMode = mode;
  }

  /**
   * An array of longs defining days that should be flagged. This array is
   * assumed to be in sorted order from least to greatest.
   */
  public void setFlaggedDates(final long[] dates) {
    this.flaggedDates = dates;

    if (this.flaggedDates == null) {
      repaint();
      return;
    }

    // Loop through the flaggedDates and set the hour, minute, seconds and
    // milliseconds to 0 so we can compare times later.
    for (int i = 0; i < this.flaggedDates.length; i++) {
      cal.setTimeInMillis(this.flaggedDates[i]);

      // We only want to compare the day, month and year
      // so reset all other values to 0.
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);

      this.flaggedDates[i] = cal.getTimeInMillis();
    }

    // Restore the time.
    cal.setTimeInMillis(firstDisplayedDate);

    repaint();
  }

  /**
   * Returns the padding used between days in the calendar.
   */
  public int getBoxPaddingX() {
    return boxPaddingX;
  }

  /**
   * Sets the number of pixels used to pad the left and right side of a day.
   * The padding is applied to both sides of the days. Therefore, if you used
   * the padding value of 3, the number of pixels between any two days would
   * be 6.
   */
  public void setBoxPaddingX(final int boxPaddingX) {
    this.boxPaddingX = boxPaddingX;
    dirty = true;
  }

  /**
   * Returns the padding used above and below days in the calendar.
   */
  public int getBoxPaddingY() {
    return boxPaddingY;
  }

  /**
   * Sets the number of pixels used to pad the top and bottom of a day. The
   * padding is applied to both the top and bottom of a day. Therefore, if you
   * used the padding value of 3, the number of pixels between any two days
   * would be 6.
   */
  public void setBoxPaddingY(final int boxPaddingY) {
    this.boxPaddingY = boxPaddingY;
    dirty = true;
  }

  /**
   * Sets the single character representation for each day of the week. For
   * this method the first days of the week days[0] is assumed to be
   * <code>Calendar.SUNDAY</code>.
   *
   * @throws IllegalArgumentException if <code>days.length</code> != 7
   * @throws NullPointerException     if <code>days</code> == null
   */
  public void setDaysOfTheWeek(final String[] days)
      throws IllegalArgumentException, NullPointerException {
    if (days == null) {
      throw new NullPointerException("Array of days is null.");
    } else if (days.length != 7) {
      throw new IllegalArgumentException("Array of days is not of length 7 as expected.");
    }
    daysOfTheWeek = days;
  }

  /**
   * Returns the single character representation for each day of the week.
   *
   * @return Single character representation for the days of the week
   */
  public String[] getDaysOfTheWeek() {
    String[] days = new String[7];
    System.arraycopy(daysOfTheWeek, 0, days, 0, 7);
    return days;
  }

  /**
   * Gets what the first day of the week is; e.g.,
   * <code>Calendar.SUNDAY</code> in the U.S., <code>Calendar.MONDAY</code> in
   * France.
   *
   * @return int The first day of the week.
   */
  public int getFirstDayOfWeek() {
    return firstDayOfWeek;
  }

  /**
   * Sets what the first day of the week is; e.g.,
   * <code>Calendar.SUNDAY</code> in US, <code>Calendar.MONDAY</code> in
   * France.
   *
   * @param firstDayOfWeek The first day of the week.
   * @see java.util.Calendar
   */
  public void setFirstDayOfWeek(final int firstDayOfWeek) {
    if (firstDayOfWeek == this.firstDayOfWeek) {
      return;
    }

    this.firstDayOfWeek = firstDayOfWeek;
    cal.setFirstDayOfWeek(this.firstDayOfWeek);

    repaint();
  }

  /**
   * Gets the time zone.
   *
   * @return The <code>TimeZone</code> used by the <code>JXMonthView</code>.
   */
  public TimeZone getTimeZone() {
    return cal.getTimeZone();
  }

  /**
   * Sets the time zone with the given time zone value.
   *
   * @param tz The <code>TimeZone</code>.
   */
  public void setTimeZone(final TimeZone tz) {
    cal.setTimeZone(tz);
  }

  /**
   * Returns true if anti-aliased text is enabled for this component, false
   * otherwise.
   *
   * @return boolean <code>true</code> if anti-aliased text is enabled,
   * <code>false</code> otherwise.
   */
  public boolean getAntialiased() {
    return antiAlias;
  }

  /**
   * Turns on/off anti-aliased text for this component.
   *
   * @param antiAlias <code>true</code> for anti-aliased text, <code>false</code> to
   *                  turn it off.
   */
  public void setAntialiased(final boolean antiAlias) {
    if (this.antiAlias == antiAlias) {
      return;
    }
    this.antiAlias = antiAlias;
    repaint();
  }

  /**
   * public void setDropShadowMask(int mask) { dropShadowMask = mask;
   * repaint(); }
   */

  /**
   * Returns the selected background color.
   *
   * @return the selected background color.
   */
  public Color getSelectedBackground() {
    return selectedBackground;
  }

  /**
   * Sets the selected background color to <code>c</code>. The default color
   * is <code>Color.LIGHT_GRAY</code>.
   *
   * @param c Selected background.
   */
  public void setSelectedBackground(final Color c) {
    selectedBackground = c;
  }

  /**
   * Returns the color used when painting the today background.
   *
   * @return Color Color
   */
  public Color getTodayBackground() {
    return todayBackgroundColor;
  }

  /**
   * Sets the color used to draw the bounding box around today. The default is
   * the background of the <code>JXMonthView</code> component.
   */
  public void setTodayBackground(final Color c) {
    todayBackgroundColor = c;
    repaint();
  }

  /**
   * Returns the color used to paint the month string background.
   *
   * @return Color Color.
   */
  public Color getMonthStringBackground() {
    return monthStringBackground;
  }

  /**
   * Sets the color used to draw the background of the month string. The
   * default is <code>Color.LIGHT_GRAY</code>.
   */
  public void setMonthStringBackground(final Color c) {
    monthStringBackground = c;
    repaint();
  }

  /**
   * Returns a copy of the insets used to paint the month string background.
   *
   * @return Insets Month string insets.
   */
  public Insets getMonthStringInsets() {
    return (Insets) monthStringInsets.clone();
  }

  /**
   * Insets used to modify the width/height when painting the background of
   * the month string area.
   *
   * @param insets Insets
   */
  public void setMonthStringInsets(final Insets insets) {
    if (insets == null) {
      monthStringInsets.top = 0;
      monthStringInsets.left = 0;
      monthStringInsets.bottom = 0;
      monthStringInsets.right = 0;
    } else {
      monthStringInsets.top = insets.top;
      monthStringInsets.left = insets.left;
      monthStringInsets.bottom = insets.bottom;
      monthStringInsets.right = insets.right;
    }
    repaint();
  }

  /**
   * Returns the preferred number of columns to paint calendars in.
   *
   * @return int Columns of calendars.
   */
  public int getPreferredCols() {
    return minCalCols;
  }

  /**
   * The preferred number of columns to paint calendars.
   *
   * @param cols The number of columns of calendars.
   */
  public void setPreferredCols(final int cols) {
    if (cols <= 0) {
      return;
    }
    minCalCols = cols;
    dirty = true;
    revalidate();
    repaint();
  }

  /**
   * Returns the preferred number of rows to paint calendars in.
   *
   * @return int Rows of calendars.
   */
  public int getPreferredRows() {
    return minCalRows;
  }

  /**
   * Sets the preferred number of rows to paint calendars.
   *
   * @param rows The number of rows of calendars.
   */
  public void setPreferredRows(final int rows) {
    if (rows <= 0) {
      return;
    }
    minCalRows = rows;
    dirty = true;
    revalidate();
    repaint();
  }

  private void updateIfNecessary() {
    if (dirty) {
      update();
      dirty = false;
    }
  }

  /**
   * Calculates size information necessary for laying out the month view.
   */
  private void update() {
    // Loop through year and get largest representation of the month.
    // Keep track of the longest month so we can loop through it to
    // determine the width of a date box.
    int currDays;
    int longestMonth = 0;
    int daysInLongestMonth = 0;

    int currWidth;
    int longestMonthWidth = 0;

    // We use a bold font for figuring out size constraints since
    // it's larger and flaggedDates will be noted in this style.
    derivedFont = getFont().deriveFont(Font.BOLD);
    FontMetrics fm = getFontMetrics(derivedFont);

    cal.set(Calendar.MONTH, cal.getMinimum(Calendar.MONTH));
    cal.set(Calendar.DAY_OF_MONTH, cal
        .getActualMinimum(Calendar.DAY_OF_MONTH));
    for (int i = 0; i < cal.getMaximum(Calendar.MONTH); i++) {
      currWidth = fm.stringWidth(monthsOfTheYear[i]);
      if (currWidth > longestMonthWidth) {
        longestMonthWidth = currWidth;
      }
      currDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
      if (currDays > daysInLongestMonth) {
        longestMonth = cal.get(Calendar.MONTH);
        daysInLongestMonth = currDays;
      }
      cal.add(Calendar.MONTH, 1);
    }

    // Loop through longest month and get largest representation of the day
    // of the month.
    cal.set(Calendar.MONTH, longestMonth);
    cal.set(Calendar.DAY_OF_MONTH, cal
        .getActualMinimum(Calendar.DAY_OF_MONTH));
    boxHeight = fm.getHeight();
    for (int i = 0; i < daysInLongestMonth; i++) {
      currWidth = fm.stringWidth(dayOfMonthFormatter
          .format(cal.getTime()));
      if (currWidth > boxWidth) {
        boxWidth = currWidth;
      }
      cal.add(Calendar.DAY_OF_MONTH, 1);
    }

    // Modify _boxWidth if month string is longer
    dim.width = (boxWidth + (2 * boxPaddingX)) * DAYS_IN_WEEK;
    if (dim.width < longestMonthWidth) {
      double diff = longestMonthWidth - dim.width;
      boxWidth += Math.ceil(diff / DAYS_IN_WEEK);
      dim.width = (boxWidth + (2 * boxPaddingX)) * DAYS_IN_WEEK;
    }

    // Keep track of calendar width and height for use later.
    calendarWidth = (boxWidth + (2 * boxPaddingX)) * DAYS_IN_WEEK;
    calendarHeight = (boxPaddingY + boxHeight + boxPaddingY) * 8;

    // Calculate minimum width/height for the component.
    dim.height = (calendarHeight * minCalRows)
        + (CALENDAR_SPACING * (minCalRows - 1));

    dim.width = (calendarWidth * minCalCols)
        + (CALENDAR_SPACING * (minCalCols - 1));

    // Add insets to the dimensions.
    Insets insets = getInsets();
    dim.width += insets.left + insets.right;
    dim.height += insets.top + insets.bottom;

    // Restore calendar.
    cal.setTimeInMillis(firstDisplayedDate);
  }

  private void updateToday() {
    // Update _today.
    cal.setTimeInMillis(today);
    cal.add(Calendar.DAY_OF_MONTH, 1);
    today = cal.getTimeInMillis();

    // Restore calendar.
    cal.setTimeInMillis(firstDisplayedDate);
    repaint();
  }

  /**
   * Returns the minimum size needed to display this component.
   *
   * @return Dimension Minimum size.
   */
  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

  /**
   * Returns the preferred size of this component.
   *
   * @return Dimension Preferred size.
   */
  @Override
  public Dimension getPreferredSize() {
    updateIfNecessary();
    return new Dimension(dim);
  }

  /**
   * Returns the maximum size of this component.
   *
   * @return Dimension Maximum size.
   */
  @Override
  public Dimension getMaximumSize() {
    return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
  }

  /**
   * Sets the border of this component. The Border object is responsible for
   * defining the insets for the component (overriding any insets set directly
   * on the component) and for optionally rendering any border decorations
   * within the bounds of those insets. Borders should be used (rather than
   * insets) for creating both decorative and non-decorative (such as margins
   * and padding) regions for a swing component. Compound borders can be used
   * to nest multiple borders within a single component.
   * <p>
   * As the border may modify the bounds of the component, setting the border
   * may result in a reduced number of displayed calendars.
   *
   * @param border Border.
   */
  @Override
  public void setBorder(final Border border) {
    super.setBorder(border);
    calculateNumDisplayedCals();
    calculateStartPosition();
    dirty = true;
  }

  /**
   * Moves and resizes this component. The new location of the top-left corner
   * is specified by x and y, and the new size is specified by width and
   * height.
   *
   * @param x      The new x-coordinate of this component
   * @param y      The new y-coordinate of this component
   * @param width  The new width of this component
   * @param height The new height of this component
   */
  @Override
  public void setBounds(final int x, final int y, final int width, final int height) {
    super.setBounds(x, y, width, height);

    calculateNumDisplayedCals();
    calculateStartPosition();

    if (startSelectedDate != -1 || endSelectedDate != -1) {
      if (startSelectedDate > lastDisplayedDate
          || startSelectedDate < firstDisplayedDate) {
        // Already does the recalculation for the dirty rect.
        ensureDateVisible(startSelectedDate);
      } else {
        calculateDirtyRectForSelection();
      }
    }
  }

  /**
   * Moves and resizes this component to conform to the new bounding rectangle
   * r. This component's new position is specified by r.x and r.y, and its new
   * size is specified by r.width and r.height
   *
   * @param r The new bounding rectangle for this component
   */
  @Override
  public void setBounds(final Rectangle r) {
    setBounds(r.x, r.y, r.width, r.height);
  }

  /**
   * Sets the language-sensitive orientation that is to be used to order the
   * elements or text within this component. Language-sensitive LayoutManager
   * and Component subclasses will use this property to determine how to lay
   * out and draw components.
   * <p>
   * At construction time, a component's orientation is set to
   * ComponentOrientation.UNKNOWN, indicating that it has not been specified
   * explicitly. The UNKNOWN orientation behaves the same as
   * ComponentOrientation.LEFT_TO_RIGHT.
   *
   * @param o The component orientation.
   */
  @Override
  public void setComponentOrientation(final ComponentOrientation o) {
    super.setComponentOrientation(o);
    ltr = o.isLeftToRight();
    calculateStartPosition();
  }

  /**
   * Sets the font of this component.
   *
   * @param font The font to become this component's font; if this parameter is
   *             null then this component will inherit the font of its parent.
   */
  @Override
  public void setFont(final Font font) {
    super.setFont(font);
    dirty = true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void removeNotify() {
    todayTimer.stop();
    super.removeNotify();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void addNotify() {
    super.addNotify();

    // Setup timer to update the value of _today.
    int secondsTillTomorrow = 86400;

    if (todayTimer == null) {
      todayTimer = new Timer(secondsTillTomorrow * 1000,
          new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
              updateToday();
            }
          });
    }

    // Modify the initial delay by the current time.
    cal.setTimeInMillis(System.currentTimeMillis());
    secondsTillTomorrow = secondsTillTomorrow
        - (cal.get(Calendar.HOUR_OF_DAY) * 3600)
        - (cal.get(Calendar.MINUTE) * 60) - cal.get(Calendar.SECOND);
    todayTimer.setInitialDelay(secondsTillTomorrow * 1000);
    todayTimer.start();

    // Restore calendar
    cal.setTimeInMillis(firstDisplayedDate);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void paintComponent(final Graphics g) {
    // Object oldTextAAValue = null;
    Object oldAAValue = null;
    Graphics2D g2 = (g instanceof Graphics2D) ? (Graphics2D) g : null;
    if (g2 != null && antiAlias) {
      // oldTextAAValue =
      // g2.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING);
      // g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
      // RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
      oldAAValue = g2.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
          RenderingHints.VALUE_ANTIALIAS_ON);
    }

    Rectangle clip = g.getClipBounds();

    updateIfNecessary();

    if (isOpaque()) {
      g.setColor(getBackground());
      g.fillRect(clip.x, clip.y, clip.width, clip.height);
    }
    g.setColor(getForeground());
    Color shadowColor = Color.WHITE;// g.getColor();
    shadowColor = new Color(shadowColor.getRed(), shadowColor.getGreen(),
        shadowColor.getBlue(), (int) (.55 * 255));

    FontMetrics fm = g.getFontMetrics();

    // Reset the calendar.
    cal.setTimeInMillis(firstDisplayedDate);

    // Center the calendars vertically in the available space.
    int y = startY;
    for (int row = 0; row < numCalRows; row++) {
      // Center the calendars horizontally in the available space.
      int x = startX;
      int tmpX, tmpY;

      // Check if this row falls in the clip region.
      bounds.x = 0;
      bounds.y = startY + row * (calendarHeight + CALENDAR_SPACING);
      bounds.width = getWidth();
      bounds.height = calendarHeight;

      if (!bounds.intersects(clip)) {
        cal.add(Calendar.MONTH, numCalCols);
        y += calendarHeight + CALENDAR_SPACING;
        continue;
      }

      for (int column = 0; column < numCalCols; column++) {
        String monthName = monthsOfTheYear[cal.get(Calendar.MONTH)];
        monthName = monthName + " " + cal.get(Calendar.YEAR);

        bounds.x = ltr ? x : x - calendarWidth;
        bounds.y = y + boxPaddingY;
        bounds.width = calendarWidth;
        bounds.height = boxHeight;

        if (bounds.intersects(clip)) {
          // Paint month name background.
          paintMonthStringBackground(g, bounds.x, bounds.y,
              bounds.width, bounds.height);

          // Paint month name.
          g.setColor(getForeground());
          tmpX = ltr ? x + (calendarWidth / 2)
              - (fm.stringWidth(monthName) / 2) : x
              - (calendarWidth / 2)
              - (fm.stringWidth(monthName) / 2) - 1;
          tmpY = y + boxPaddingY + boxHeight - fm.getDescent();

          if ((dropShadowMask & MONTH_DROP_SHADOW) != 0) {
            g.setColor(shadowColor);
            g.drawString(monthName, tmpX + 1, tmpY + 1);
            g.setColor(getForeground());
          }
          g.drawString(monthName, tmpX, tmpY);
        }

        bounds.x = ltr ? x : x - calendarWidth;
        bounds.y = y + boxPaddingY + boxHeight + boxPaddingY
            + boxPaddingY;
        bounds.width = calendarWidth;
        bounds.height = boxHeight;

        if (bounds.intersects(clip)) {
          cal.set(Calendar.DAY_OF_MONTH, cal
              .getActualMinimum(Calendar.DAY_OF_MONTH));

          // Paint short representation of day of the week.
          int dayIndex = firstDayOfWeek - 1;
          for (int i = 0; i < DAYS_IN_WEEK; i++) {
            tmpX = ltr ? x
                + (i * (boxPaddingX + boxWidth + boxPaddingX))
                + boxPaddingX + (boxWidth / 2)
                - (fm.stringWidth(daysOfTheWeek[dayIndex]) / 2)
                : x
                - (i * (boxPaddingX + boxWidth + boxPaddingX))
                - boxPaddingX
                - (boxWidth / 2)
                - (fm
                .stringWidth(daysOfTheWeek[dayIndex]) / 2);
            tmpY = y + boxPaddingY + boxHeight + boxPaddingY
                + boxPaddingY + fm.getAscent();
            g.drawString(daysOfTheWeek[dayIndex], tmpX, tmpY);
            if ((dropShadowMask & WEEK_DROP_SHADOW) != 0) {
              g.setColor(shadowColor);
              g.drawString(daysOfTheWeek[dayIndex], tmpX + 1,
                  tmpY + 1);
              g.setColor(getForeground());
            }
            dayIndex++;
            if (dayIndex == 7) {
              dayIndex = 0;
            }
          }

          // Paint a line across bottom of days of the week.
          g.drawLine(ltr ? x + 2 : x - 3, y + (boxPaddingY * 3)
              + (boxHeight * 2), ltr ? x + calendarWidth - 3 : x
              - calendarWidth + 2, y + (boxPaddingY * 3)
              + (boxHeight * 2));
          if ((dropShadowMask & MONTH_LINE_DROP_SHADOW) != 0) {
            g.setColor(shadowColor);
            g.drawLine(ltr ? x + 3 : x - 2, y + (boxPaddingY * 3)
                + (boxHeight * 2) + 1, ltr ? x + calendarWidth
                - 2 : x - calendarWidth + 3, y
                + (boxPaddingY * 3) + (boxHeight * 2) + 1);
            g.setColor(getForeground());
          }
        }

        // Check if the month to paint falls in the clip.
        bounds.x = startX
            + (ltr ? column * (calendarWidth + CALENDAR_SPACING)
            : -(column * (calendarWidth + CALENDAR_SPACING) + calendarWidth));
        bounds.y = startY + row * (calendarHeight + CALENDAR_SPACING);
        bounds.width = calendarWidth;
        bounds.height = calendarHeight;

        // Paint the month if it intersects the clip. If we don't move
        // the calendar forward a month as it would have if paintMonth
        // was called.
        if (bounds.intersects(clip)) {
          paintMonth(g, column, row);
        } else {
          cal.add(Calendar.MONTH, 1);
        }

        x += ltr ? calendarWidth + CALENDAR_SPACING
            : -(calendarWidth + CALENDAR_SPACING);
      }
      y += calendarHeight + CALENDAR_SPACING;
    }

    // Restore the calendar.
    cal.setTimeInMillis(firstDisplayedDate);
    if (g2 != null && antiAlias) {
      // g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
      // oldTextAAValue);
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, oldAAValue);
    }
  }

  /**
   * Paints a month. It is assumed the calendar, _cal, is already set to the
   * first day of the month to be painted.
   *
   * @param col X (column) the calendar is displayed in.
   * @param row Y (row) the calendar is displayed in.
   * @param g   Graphics object.
   */
  private void paintMonth(final Graphics g, final int col, final int row) {
    String numericDay;
    int days = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    FontMetrics fm = g.getFontMetrics();
    Rectangle clip = g.getClipBounds();

    long nextFlaggedDate = -1;
    int flaggedDateIndex = 0;
    if (flaggedDates != null && flaggedDates.length > 0) {
      nextFlaggedDate = flaggedDates[flaggedDateIndex];
    }

    for (int i = 0; i < days; i++) {
      calculateBoundsForDay(bounds);

      if (bounds.intersects(clip)) {
        numericDay = dayOfMonthFormatter.format(cal.getTime());

        // Paint bounding box around any date that falls within the
        // selection.
        if (isSelectedDate(cal.getTimeInMillis())) {
          // Keep track of the rectangle for the currently
          // selected date so we don't have to recalculate it
          // later when it becomes unselected. This is only
          // useful for SINGLE_SELECTION mode.
          if (selectionMode == SINGLE_SELECTION) {
            dirtyRect.x = bounds.x;
            dirtyRect.y = bounds.y;
            dirtyRect.width = bounds.width;
            dirtyRect.height = bounds.height;
          }

          paintSelectedDayBackground(g, bounds.x, bounds.y,
              bounds.width, bounds.height);

          g.setColor(getForeground());
        }

        // Paint bounding box around today.
        if (cal.getTimeInMillis() == today) {
          paintTodayBackground(g, bounds.x, bounds.y, bounds.width,
              bounds.height);

          g.setColor(getForeground());
        }

        // If the appointment date is less than the current
        // calendar date increment to the next appointment.
        while (nextFlaggedDate != -1
            && nextFlaggedDate < cal.getTimeInMillis()) {
          flaggedDateIndex++;
          if (flaggedDateIndex < flaggedDates.length) {
            nextFlaggedDate = flaggedDates[flaggedDateIndex];
          } else {
            nextFlaggedDate = -1;
          }
        }

        // Paint numeric day of the month.
        if (nextFlaggedDate != -1
            && cal.getTimeInMillis() == nextFlaggedDate) {
          Font oldFont = getFont();
          g.setFont(derivedFont);
          g.drawString(numericDay, ltr ? bounds.x + boxPaddingX
              + boxWidth - fm.stringWidth(numericDay) : bounds.x
              + boxPaddingX + boxWidth
              - fm.stringWidth(numericDay) - 1, bounds.y
              + boxPaddingY + fm.getAscent());
          g.setFont(oldFont);
        } else {
          g.drawString(numericDay, ltr ? bounds.x + boxPaddingX
              + boxWidth - fm.stringWidth(numericDay) : bounds.x
              + boxPaddingX + boxWidth
              - fm.stringWidth(numericDay) - 1, bounds.y
              + boxPaddingY + fm.getAscent());
        }
      }
      cal.add(Calendar.DAY_OF_MONTH, 1);
    }
  }

  /**
   * Paints the background of the month string. The bounding box for this
   * background can be modified by setting its insets via
   * setMonthStringInsets. The color of the background can be set via
   * setMonthStringBackground.
   *
   * @param g      Graphics object to paint to.
   * @param x      x-coordinate of upper left corner.
   * @param y      y-coordinate of upper left corner.
   * @param width  width of the bounding box.
   * @param height height of the bounding box.
   * @see #setMonthStringBackground
   * @see #setMonthStringInsets
   */
  protected void paintMonthStringBackground(final Graphics g, int x, int y,
      int width, int height) {
    // Modify bounds by the month string insets.
    x = ltr ? x + monthStringInsets.left : x + monthStringInsets.right;
    y = y + monthStringInsets.top;
    width = width - monthStringInsets.left - monthStringInsets.right;
    height = height - monthStringInsets.top - monthStringInsets.bottom;

    Color colorMbs2 = new Color(monthStringBackground.getRed(),
        monthStringBackground.getGreen(), monthStringBackground
        .getBlue(), (int) (.50 * 255));

    GradientPaint gradient = new GradientPaint(x, y, monthStringBackground,
        x, y + height - 1, colorMbs2);
    Graphics2D g2 = (Graphics2D) g;
    g2.setPaint(gradient);
    g2.fillRoundRect(x, y, width, height, 5, 5);
  }

  /**
   * Paints the background for today. The default is a rectangle drawn in
   * using the color set by <code>setTodayBackground</code>
   *
   * @param g      Graphics object to paint to.
   * @param x      x-coordinate of upper left corner.
   * @param y      y-coordinate of upper left corner.
   * @param width  width of bounding box for the day.
   * @param height height of bounding box for the day.
   * @see #setTodayBackground
   */
  protected void paintTodayBackground(final Graphics g, final int x, final int y, final int width,
      final int height) {
    g.setColor(todayBackgroundColor);
    int arc = height * 2 / 5;
    g.drawRoundRect(x, y, width - 1, height - 1, arc, arc);
  }

  /**
   * Paint the background for a selected day. The default is a filled
   * rectangle in the in the component's background color.
   *
   * @param g      Graphics object to paint to.
   * @param x      x-coordinate of upper left corner.
   * @param y      y-coordinate of upper left corner.
   * @param width  width of bounding box for the day.
   * @param height height of bounding box for the day.
   */
  protected void paintSelectedDayBackground(final Graphics g, final int x, final int y,
      final int width, final int height) {

    Color bkColor1 = getSelectedBackground();
    Color bkColor2 = new Color(bkColor1.getRed(), bkColor1.getGreen(),
        bkColor1.getBlue(), (int) (.50 * 255));
    GradientPaint gradient = new GradientPaint(x, y, bkColor1, x, y
        + height - 1, bkColor2);

    int arc = height * 2 / 5;

    Graphics2D g2 = (Graphics2D) g;
    g2.setPaint(gradient);
    g2.fillRoundRect(x, y, width - 1, height - 1, arc, arc);

    Color strokeColor = bkColor1.darker();
    g2.setColor(strokeColor);
    g2.drawRoundRect(x, y, width - 1, height - 1, arc, arc);
  }

  /**
   * Returns true if the specified time falls within the _startSelectedDate
   * and _endSelectedDate range.
   */
  private boolean isSelectedDate(final long time) {
    if (time >= startSelectedDate && time <= endSelectedDate) {
      return true;
    }
    return false;
  }

  /**
   * Calculates the _numCalCols/_numCalRows that determine the number of
   * calendars that can be displayed.
   */
  private void calculateNumDisplayedCals() {
    int oldNumCalCols = numCalCols;
    int oldNumCalRows = numCalRows;

    // Determine how many columns of calendars we want to paint.
    numCalCols = 1;
    numCalCols += (getWidth() - calendarWidth)
        / (calendarWidth + CALENDAR_SPACING);

    // Determine how many rows of calendars we want to paint.
    numCalRows = 1;
    numCalRows += (getHeight() - calendarHeight)
        / (calendarHeight + CALENDAR_SPACING);

    if (oldNumCalCols != numCalCols || oldNumCalRows != numCalRows) {
      calculateLastDisplayedDate();
    }
  }

  /**
   * Calculates the _startX/_startY position for centering the calendars
   * within the available space.
   */
  private void calculateStartPosition() {
    // Calculate offset in x-axis for centering calendars.
    startX = (getWidth() - ((calendarWidth * numCalCols) + (CALENDAR_SPACING * (numCalCols - 1)))) / 2;
    if (!ltr) {
      startX = getWidth() - startX;
    }

    // Calculate offset in y-axis for centering calendars.
    startY = (getHeight() - ((calendarHeight * numCalRows) + (CALENDAR_SPACING * (numCalRows - 1)))) / 2;
  }

  /**
   * Returns the bounding box for drawing a date. It is assumed that the
   * calendar, _cal, is already set to the date you want to find the offset
   * for.
   *
   * @param bounds Bounds of the date to draw in.
   * @return Point X/Y coordinate to the upper left corner of the bounding box
   * for date.
   */
  private void calculateBoundsForDay(final Rectangle bounds) {
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
    int weekOfMonth = cal.get(Calendar.WEEK_OF_MONTH);

    // Determine what row/column we are in.
    int diffMonths = month - firstDisplayedMonth
        + ((year - firstDisplayedYear) * 12);
    int calRowIndex = diffMonths / numCalCols;
    int calColIndex = diffMonths - (calRowIndex * numCalCols);

    // Modify the index relative to the first day of the week.
    bounds.x = dayOfWeek - firstDayOfWeek;
    if (bounds.x < 0) {
      bounds.x += DAYS_IN_WEEK;
    }

    // Offset for location of the day in the week.
    bounds.x = ltr ? bounds.x * (boxPaddingX + boxWidth + boxPaddingX)
        : (bounds.x + 1) * (boxPaddingX + boxWidth + boxPaddingX);

    // Offset for the column the calendar is displayed in.
    bounds.x += calColIndex * (calendarWidth + CALENDAR_SPACING);

    // Adjust by centering value.
    bounds.x = ltr ? startX + bounds.x : startX - bounds.x;

    // Initial offset for Month and Days of the Week display.
    bounds.y = 2 * (boxPaddingY + boxHeight + boxPaddingY);

    // Offset for centering and row the calendar is displayed in.
    bounds.y += startY + calRowIndex * (calendarHeight + CALENDAR_SPACING);

    // Offset for Week of the Month.
    bounds.y += (weekOfMonth - 1) * (boxPaddingY + boxHeight + boxPaddingY);

    bounds.width = boxPaddingX + boxWidth + boxPaddingX;
    bounds.height = boxPaddingY + boxHeight + boxPaddingY;
  }

  /**
   * Return a long representing the date at the specified x/y position. The
   * date returned will have a valid day, month and year. Other fields such as
   * hour, minute, second and milli-second will be set to 0.
   *
   * @param x X position
   * @param y Y position
   * @return long The date, -1 if position does not contain a date.
   */
  public long getDayAt(final int x, final int y) {
    if (ltr ? (startX > x) : (startX < x) || startY > y) {
      return -1;
    }

    // Determine which column of calendars we're in.
    int calCol = (ltr ? (x - startX) : (startX - x))
        / (calendarWidth + CALENDAR_SPACING);

    // Determine which row of calendars we're in.
    int calRow = (y - startY) / (calendarHeight + CALENDAR_SPACING);

    if (calRow > numCalRows - 1 || calCol > numCalCols - 1) {
      return -1;
    }

    // Determine what row (week) in the selected month we're in.
    int row;
    row = ((y - startY) - (calRow * (calendarHeight + CALENDAR_SPACING)))
        / (boxPaddingY + boxHeight + boxPaddingY);
    // The first two lines in the calendar are the month and the days
    // of the week. Ignore them.
    row -= 2;

    if (row < 0 || row > 5) {
      return -1;
    }

    // Determine which column in the selected month we're in.
    int col = ((ltr ? (x - startX) : (startX - x)) - (calCol * (calendarWidth + CALENDAR_SPACING)))
        / (boxPaddingX + boxWidth + boxPaddingX);

    if (col > DAYS_IN_WEEK - 1) {
      return -1;
    }

    // Use the first day of the month as a key point for determining the
    // date of our click.
    // The week index of the first day will always be 0.
    cal.setTimeInMillis(firstDisplayedDate);
    // _cal.set(Calendar.DAY_OF_MONTH, 1);
    cal.add(Calendar.MONTH, calCol + (calRow * numCalCols));

    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
    int firstDayIndex = dayOfWeek - firstDayOfWeek;
    if (firstDayIndex < 0) {
      firstDayIndex += DAYS_IN_WEEK;
    }

    int daysToAdd = (row * DAYS_IN_WEEK) + (col - firstDayIndex);
    if (daysToAdd < 0
        || daysToAdd > (cal.getActualMaximum(Calendar.DAY_OF_MONTH) - 1)) {
      return -1;
    }

    cal.add(Calendar.DAY_OF_MONTH, daysToAdd);

    long selected = cal.getTimeInMillis();

    // Restore the time.
    cal.setTimeInMillis(firstDisplayedDate);

    return selected;
  }

  private void calculateDirtyRectForSelection() {
    if (startSelectedDate == -1 || endSelectedDate == -1) {
      dirtyRect.x = 0;
      dirtyRect.y = 0;
      dirtyRect.width = 0;
      dirtyRect.height = 0;
    } else {
      cal.setTimeInMillis(startSelectedDate);
      calculateBoundsForDay(dirtyRect);
      cal.add(Calendar.DAY_OF_MONTH, 1);

      Rectangle tmpRect;
      while (cal.getTimeInMillis() <= endSelectedDate) {
        calculateBoundsForDay(bounds);
        tmpRect = dirtyRect.union(bounds);
        dirtyRect.x = tmpRect.x;
        dirtyRect.y = tmpRect.y;
        dirtyRect.width = tmpRect.width;
        dirtyRect.height = tmpRect.height;
        cal.add(Calendar.DAY_OF_MONTH, 1);
      }

      // Restore the time.
      cal.setTimeInMillis(firstDisplayedDate);
    }
  }

  /**
   * Returns the string currently used to identiy fired ActionEvents.
   *
   * @return String The string used for identifying ActionEvents.
   */
  public String getActionCommand() {
    return actionCommand;
  }

  /**
   * Sets the string used to identify fired ActionEvents.
   *
   * @param actionCommand The string used for identifying ActionEvents.
   */
  public void setActionCommand(final String actionCommand) {
    this.actionCommand = actionCommand;
  }

  /**
   * Adds an ActionListener.
   * <p>
   * The ActionListener will receive an ActionEvent when a selection has been
   * made.
   *
   * @param l The ActionListener that is to be notified
   */
  public void addActionListener(final ActionListener l) {
    listenerList.add(ActionListener.class, l);
  }

  /**
   * Removes an ActionListener.
   *
   * @param l The action listener to remove.
   */
  public void removeActionListener(final ActionListener l) {
    listenerList.remove(ActionListener.class, l);
  }

  /**
   * Fires an ActionEvent to all listeners.
   */
  protected void fireActionPerformed() {
    Object[] listeners = listenerList.getListenerList();
    ActionEvent e = null;
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ActionListener.class) {
        if (e == null) {
          e = new ActionEvent(JXMonthView.this, ActionEvent.ACTION_PERFORMED, actionCommand);
        }
        ((ActionListener) listeners[i + 1]).actionPerformed(e);
      }
    }
  }

  protected void fireActionSelected() {
    Object[] listeners = listenerList.getListenerList();
    ActionEvent e = null;
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ActionListener.class) {
        if (e == null) {
          e = new ActionEvent(JXMonthView.this, ActionEvent.ACTION_PERFORMED, "MONTH_VIEW_CHANGED");
        }
        ((ActionListener) listeners[i + 1]).actionPerformed(e);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void processMouseEvent(final MouseEvent e) {
    if (!isEnabled() || selectionMode == NO_SELECTION) {
      return;
    }

    int id = e.getID();

    if (id == MouseEvent.MOUSE_PRESSED) {
      int x = e.getX();
      int y = e.getY();

      long selected = getDayAt(x, y);
      if (selected == -1) {
        return;
      }

      // Update the selected dates.
      startSelectedDate = selected;
      endSelectedDate = selected;

      if (selectionMode == MULTIPLE_SELECTION ||
          selectionMode == WEEK_SELECTION) {
        pivotDate = selected;
      }

      // Determine the dirty rectangle of the new selected date so we
      // draw the bounding box around it. This dirty rect includes the
      // visual border of the selected date.
      cal.setTimeInMillis(selected);

      calculateBoundsForDay(bounds);
      cal.setTimeInMillis(firstDisplayedDate);

      // Repaint the old dirty area.
      repaint(dirtyRect);

      // Repaint the new dirty area.
      repaint(bounds);

      // Update the dirty area.
      dirtyRect.x = bounds.x;
      dirtyRect.y = bounds.y;
      dirtyRect.width = bounds.width;
      dirtyRect.height = bounds.height;

      if (e.getClickCount() >= 2) {
        // Arm so we fire action performed on mouse release.
        asKirkWouldSay_FIRE = true;
      }

      fireActionSelected();
    } else if (id == MouseEvent.MOUSE_RELEASED && e.getClickCount() >= 2) {
      if (asKirkWouldSay_FIRE) {
        fireActionPerformed();
      }
      asKirkWouldSay_FIRE = false;
    }
    super.processMouseEvent(e);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  protected void processMouseMotionEvent(final MouseEvent e) {
    if (!isEnabled() || selectionMode == NO_SELECTION) {
      return;
    }

    int id = e.getID();

    if (id == MouseEvent.MOUSE_DRAGGED) {
      int x = e.getX();
      int y = e.getY();
      long selected = getDayAt(x, y);

      if (selected == -1) {
        return;
      }

      long oldStart = startSelectedDate;
      long oldEnd = endSelectedDate;

      if (selectionMode == SINGLE_SELECTION) {
        if (selected == oldStart) {
          return;
        }
        startSelectedDate = selected;
        endSelectedDate = selected;
      } else {
        if (selected <= pivotDate) {
          startSelectedDate = selected;
          endSelectedDate = pivotDate;
        } else if (selected > pivotDate) {
          startSelectedDate = pivotDate;
          endSelectedDate = selected;
        }
      }

      if (selectionMode == WEEK_SELECTION) {
        // Do we span a week.
        long start = (selected > pivotDate) ? pivotDate : selected;
        long end = (selected > pivotDate) ? selected : pivotDate;

        cal.setTimeInMillis(start);
        int count = 1;
        while (cal.getTimeInMillis() < end) {
          cal.add(Calendar.DAY_OF_MONTH, 1);
          count++;
        }

        if (count > 7) {
          // Move the start date to the first day of the week.
          cal.setTimeInMillis(start);
          int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
          int daysFromStart = dayOfWeek - firstDayOfWeek;
          if (daysFromStart < 0) {
            daysFromStart += DAYS_IN_WEEK;
          }
          cal.add(Calendar.DAY_OF_MONTH, -daysFromStart);

          startSelectedDate = cal.getTimeInMillis();

          // Move the end date to the last day of the week.
          cal.setTimeInMillis(end);
          dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
          int lastDayOfWeek = firstDayOfWeek - 1;
          if (lastDayOfWeek == 0) {
            lastDayOfWeek = Calendar.SATURDAY;
          }
          int daysTillEnd = lastDayOfWeek - dayOfWeek;
          if (daysTillEnd < 0) {
            daysTillEnd += DAYS_IN_WEEK;
          }
          cal.add(Calendar.DAY_OF_MONTH, daysTillEnd);
          endSelectedDate = cal.getTimeInMillis();
        }
      }

      if (oldStart == startSelectedDate && oldEnd == endSelectedDate) {
        return;
      }

      // Repaint the old dirty area.
      repaint(dirtyRect);

      // Repaint the new dirty area.
      calculateDirtyRectForSelection();
      repaint(dirtyRect);

      //      if (e.getClickCount() >= 2) {
      //        // Set trigger.
      //        asKirkWouldSay_FIRE = true;
      //      }

      fireActionSelected();
    }
    super.processMouseMotionEvent(e);
  }
}
