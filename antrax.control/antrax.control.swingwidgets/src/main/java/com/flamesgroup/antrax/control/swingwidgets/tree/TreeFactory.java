package com.flamesgroup.antrax.control.swingwidgets.tree;

import javax.swing.*;

public final class TreeFactory {

  public static JTree applyTreeLazyLoading(final JTree tree) {
    LazyLoadingTreeController controller = new LazyLoadingTreeController(tree);
    tree.addTreeWillExpandListener(controller);
    return tree;
  }

}
