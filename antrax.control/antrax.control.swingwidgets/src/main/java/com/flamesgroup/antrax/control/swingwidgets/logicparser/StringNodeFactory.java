package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public interface StringNodeFactory {

  StringNode createStringNode(String str);

}
