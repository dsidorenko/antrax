package com.flamesgroup.antrax.control.swingwidgets.field;

import java.util.regex.Pattern;

/**
 * Field to edit text values
 */
public class StringField extends AbstractField {

  private static final long serialVersionUID = -2624184166480503059L;
  private int minLength, maxLength;
  private Pattern pattern;

  /**
   * Creates a string field
   */
  public StringField() {
    this(8);
  }

  /**
   * Creates a string field
   *
   * @param columns the number of characters to calculate component's width
   */
  public StringField(final int columns) {
    setColumns(columns);
  }

  /**
   * Creates a string field
   *
   * @param columns   the number of characters
   * @param required  true for non empty field requirement
   * @param minLength minimum length of the text, or null
   * @param maxLength maximum length of the text, or null
   */
  public StringField(final int columns, final boolean required, final Integer minLength,
      final Integer maxLength) {
    setColumns(columns);
    setRequired(required);
    this.minLength = minLength;
    this.maxLength = maxLength;
  }

  /**
   * Sets a pattern for validation
   *
   * @param pattern a regular expression, or null
   */
  public void setPattern(final String pattern) {
    this.pattern = (pattern != null) ? Pattern.compile(pattern) : null;
  }

  /**
   * Checks the validity
   */
  @Override
  protected boolean isValid(final String text) {
    int n = text.length();
    if ((minLength > 0) && (n < minLength))
      return false;
    if ((maxLength > 0) && (n > maxLength))
      return false;
    if ((pattern != null) && !pattern.matcher(text).matches())
      return false;
    return true;
  }
}
