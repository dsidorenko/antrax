package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import com.flamesgroup.antrax.control.swingwidgets.logicparser.DelegatedStringNode;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.LogicExpressionParser;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.LogicExpressionSyntaxException;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.LogicToken;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.Node;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.StringNode;
import com.flamesgroup.antrax.control.swingwidgets.logicparser.StringNodeFactory;

public class LogicSearchFilter implements SearchFilter {

  private static final LogicToken DEFAULT_OPERATION = LogicToken.AND;

  private final DelegatedStringNodeFactory factory;
  private final LogicExpressionParser parser;
  private final LogicToken defaultOperation;
  private Node expressionNode;
  private String filteredString;

  public LogicSearchFilter() {
    this(DEFAULT_OPERATION);
  }

  public LogicSearchFilter(final LogicToken defaultOperation) {
    this.defaultOperation = defaultOperation;
    this.factory = new DelegatedStringNodeFactory();
    this.parser = new LogicExpressionParser();
  }

  private String getFilteredString() {
    return filteredString;
  }

  private void setFilteredString(final String filteredString) {
    this.filteredString = filteredString;
  }

  public void setFilterExpression(final String expression) throws LogicExpressionSyntaxException {
    try {
      this.expressionNode = parser.parseExpression(expression, defaultOperation, factory);
    } catch (LogicExpressionSyntaxException e) {
      this.expressionNode = null;
      throw e;
    }
  }

  @Override
  public boolean isApplied(final String filteredString) {
    setFilteredString(filteredString);
    return (expressionNode == null) ? true : expressionNode.evaluate();
  }

  /**
   * Logic expression string node factory
   */
  private class DelegatedStringNodeFactory implements StringNodeFactory {
    private DelegatedStringNode.EvaluationDelegate delegate;

    private DelegatedStringNode.EvaluationDelegate getDelegate() {
      if (delegate == null) {
        delegate = new DelegatedStringNode.EvaluationDelegate() {
          @Override
          public boolean evaluate(final String value) {
            String str = getFilteredString();
            return (str == null) ? false : str.toLowerCase().contains(value.toLowerCase());
          }
        };
      }
      return delegate;
    }

    @Override
    public StringNode createStringNode(final String str) {
      return new DelegatedStringNode(str, getDelegate());
    }
  }

}
