package com.flamesgroup.antrax.control.swingwidgets.scroller;

import java.awt.*;

import javax.swing.*;

public final class ScrollBarWidgetFactory {

  private ScrollBarWidgetFactory() {
    // utility class - no constructor needed
  }

  /**
   * Creates an iApp style {@link JScrollPane}, with vertical and horizontal
   * scrollbars shown as needed.
   *
   * @param view the view to wrap inside the {@code JScrollPane}.
   * @return an iApp style {@code JScrollPane}.
   */
  public static JScrollPane createScrollPaneWithButtonsTogether(final Component view) {
    return createScrollPaneWithButtonsTogether(view, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
  }

  /**
   * Creates an iApp style {@link JScrollPane} using the given scroll bar
   * policies.
   *
   * @param view                      the view to wrap inside the {@code JScrollPane}.
   * @param verticalScrollBarPolicy   the vertical scroll bar policy.
   * @param horizontalScrollBarPolicy the horizontal scroll bar policy.
   * @return an iApp style {@code JScrollPane} using the given scroll bar
   * policies.
   */
  public static JScrollPane createScrollPaneWithButtonsTogether(final Component view, final int verticalScrollBarPolicy, final int horizontalScrollBarPolicy) {
    return new JScrollPane(view, verticalScrollBarPolicy, horizontalScrollBarPolicy);
  }

}
