package com.flamesgroup.antrax.control.swingwidgets.field;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;

/**
 * Field to edit integer numbers
 */
public class IntegerField extends AbstractField {

  private static final long serialVersionUID = 3177870451362795718L;
  private static final NumberFormat format = NumberFormat.getIntegerInstance();
  private final Integer min;
  private final Integer max;

  /**
   * Creates a number field
   *
   * @param min minimum accepted value, or null
   * @param max maximum accepted value, or null
   */
  public IntegerField(final Integer min, final Integer max) {
    this(false, min, max);
  }

  /**
   * Creates a number field
   *
   * @param required true for non empty field requirement
   * @param min      minimum accepted value, or null
   * @param max      maximum accepted value, or null
   */
  public IntegerField(final boolean required, final Integer min, final Integer max) {
    setColumns(6);
    setRequired(required);
    this.min = min;
    this.max = max;

    format.setGroupingUsed(false);
  }

  /**
   * Updates the number value
   *
   * @param value the new value
   */
  public void setContent(final Integer value) {
    setText((value != null) ? format.format(value) : "");
  }

  /**
   * Returns the valid number that the component edits
   *
   * @return the current value
   * @throws RuntimeException for invalid content
   */
  @Override
  public Integer getContent() {
    try {
      String text = getText();
      return (text.length() > 0) ? format.parse(text).intValue() : null;
    } catch (ParseException exc) {
      throw new RuntimeException(exc);
    }
  }

  /**
   * Checks the validity
   */
  @Override
  protected boolean isValid(final String text) {
    ParsePosition position = new ParsePosition(0);
    Number integer = format.parse(text, position);
    if (integer == null)
      return false;
    if (position.getIndex() != text.length())
      return false;
    int value = integer.intValue();
    if ((min != null) && (value < min.intValue()))
      return false;
    if ((max != null) && (value > max.intValue()))
      return false;
    return true;
  }

  /**
   * Reformats the number
   */
  @Override
  protected String getFormattedText(final String text) {
    try {
      return format.format(format.parse(text).longValue());
    } catch (ParseException ignored) {
      return text;
    }
  }
}
