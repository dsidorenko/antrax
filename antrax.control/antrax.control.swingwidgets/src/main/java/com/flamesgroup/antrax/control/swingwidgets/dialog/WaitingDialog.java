package com.flamesgroup.antrax.control.swingwidgets.dialog;

import java.awt.*;
import java.awt.Dialog.ModalityType;

import javax.swing.*;

public class WaitingDialog {

  private Thread taskThread;
  private final Icon icon;
  private final String title;
  private final String message;

  public WaitingDialog(final Icon icon, final String title, final String message) {
    this.icon = icon;
    this.title = title;
    this.message = message;
  }

  private JPanel createWaitPanel() {
    JPanel waitPanel = new JPanel(new GridLayout(2, 1));
    JPanel barPanel = new JPanel();
    JProgressBar bar = new JProgressBar(0, 100);
    Dimension dimension = new Dimension(220, 15);
    bar.setPreferredSize(dimension);
    bar.setMinimumSize(dimension);
    bar.setMaximumSize(dimension);
    bar.setIndeterminate(true);
    barPanel.add(bar);
    JLabel label = new JLabel();
    label.setText(message);
    label.setIcon(icon);
    waitPanel.add(label);
    waitPanel.add(barPanel);
    return waitPanel;
  }

  public void show(final Component invoker, final Runnable task) {
    Window windowAncestor = SwingUtilities.getWindowAncestor(invoker);
    final JDialog dialog = new JDialog(windowAncestor);
    dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    dialog.setTitle(title);
    dialog.add(createWaitPanel(), BorderLayout.CENTER);
    dialog.pack();
    if (taskThread == null) {

      taskThread = new Thread(new Runnable() {
        @Override
        public void run() {
          task.run();

          SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
              dialog.setVisible(false);
            }
          });
          taskThread = null;
        }
      }, "WaitDialogThread");

      taskThread.start();
      dialog.setLocationRelativeTo(windowAncestor);
      dialog.setVisible(true);
      dialog.dispose();
    }
  }
}
