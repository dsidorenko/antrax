package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public final class Precedence {

  private Precedence() {
    throw new AssertionError();
  }

  public static final int LOGICAL = 1;
  public static final int COMPARE = 4;
  public static final int NOT = 5;

}
