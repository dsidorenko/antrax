package com.flamesgroup.antrax.control.swingwidgets.table;

public class ExportException extends Exception {

  private static final long serialVersionUID = -770463754242952347L;

  public ExportException() {
    super();
  }

  public ExportException(final String message) {
    super(message);
  }

  public ExportException(final Throwable cause) {
    super(cause);
  }

  public ExportException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
