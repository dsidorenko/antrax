package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

public interface ContextSearcherListener {

  void itemRegistrationChanged(SearchItem item, boolean isRegistered);

}
