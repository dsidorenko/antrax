package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class GreaterThanNode extends CompareNode {

  @Override
  public int getPrecedence() {
    return Precedence.COMPARE;
  }

  @Override
  public boolean evaluate() {
    return (compareBranches() > 0);
  }

  @Override
  public String toString() {
    return getLeft() + " " + getRight() + " " + LogicToken.GT.getLexeme();
  }

}
