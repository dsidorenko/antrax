package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.*;

import javax.swing.*;

public class JEditPanel extends JPanel implements Scrollable {

  private static final long serialVersionUID = -7142329901236750075L;

  public JEditPanel(final LayoutManager layout, final boolean isDoubleBuffered) {
    super(layout, isDoubleBuffered);
    initUI();
  }

  public JEditPanel(final LayoutManager layout) {
    super(layout, true);
    initUI();
  }

  public JEditPanel() {
    super();
    initUI();
  }

  public JEditPanel(final boolean isDoubleBuffered) {
    super(isDoubleBuffered);
    initUI();
  }

  private void initUI() {
  }

  @Override
  public Dimension getPreferredScrollableViewportSize() {
    return getPreferredSize();
  }

  @Override
  public int getScrollableBlockIncrement(final Rectangle visibleRect, final int orientation, final int direction) {
    return 100;
  }

  @Override
  public boolean getScrollableTracksViewportHeight() {
    return false;
  }

  @Override
  public boolean getScrollableTracksViewportWidth() {
    return true;
  }

  @Override
  public int getScrollableUnitIncrement(final Rectangle visibleRect, final int orientation, final int direction) {
    return 25;
  }

}
