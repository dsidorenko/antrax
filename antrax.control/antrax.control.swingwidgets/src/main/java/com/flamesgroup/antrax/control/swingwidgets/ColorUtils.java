package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;

/**
 * Provides miscellaneous color operations.
 * <p>
 * HLS = Hue, Luminance, Saturation HSB = Hue, Saturation, Brightness HSI = Hue,
 * Saturation, Intensity
 */
public class ColorUtils {

  // Suppress default constructor, ensuring non-instantiability
  private ColorUtils() {
    throw new AssertionError();
  }

  public static Color darker(final Color color) {
    int r = color.getRed();
    int g = color.getGreen();
    int b = color.getBlue();

    return new Color((int) (r * 0.9), (int) (g * 0.9), (int) (b * 0.9));
  }

  /**
   * RGB -> HLS -> change L -> RGB
   */
  public static Color lighter(final Color clr) {
    float[] hls = ColorUtils.RGBtoHLS(clr);
    float moreL = hls[1] * 1.38f;
    if (moreL >= 0.95f) {
      moreL = 0.85f;
    }
    if (moreL < hls[1]) {
      moreL = hls[1] + (1.0f - hls[1]) * 1.8f;

      if (hls[1] >= 1.0f || moreL >= 1.0f) {
        moreL = 1.2f;
      }
    }
    hls[1] = moreL;
    return ColorUtils.HLStoRGB(hls);
  }

  /**
   * Converts from RGB color space to HLS color space.
   */
  public static float[] RGBtoHLS(final Color clr) {
    float r = clr.getRed() / 255.0f;
    float g = clr.getGreen() / 255.0f;
    float b = clr.getBlue() / 255.0f;

    // calculate lightness
    float max = Math.max(Math.max(r, g), b);
    float min = Math.min(Math.min(r, g), b);
    float l = (max + min) / 2.0f;
    float s = 0;
    float h = 0;

    if (max != min) {
      float delta = max - min;
      s = (l <= .5f) ? (delta / (max + min)) : (delta / (2.0f - max - min));
      if (r == max) {
        h = (g - b) / delta;
      } else if (g == max) {
        h = 2.0f + (b - r) / delta;
      } else {
        h = 4.0f + (r - g) / delta;
      }
      h *= 60.0f;
      if (h < 0) {
        h += 360.0f;
      }
    }

    return new float[] {h, l, s};
  }

  public static Color HLStoRGB(final float[] hls) {
    return HLStoRGB(hls[0], hls[1], hls[2]);
  }

  public static Color HLStoRGB(final float h, final float l, final float s) {
    float m2 = (l <= 0.5f) ? (l * (1 + s)) : (l + s - l * s);
    float m1 = 2.0f * l - m2;
    float r, g, b;

    if (s == 0.0) {
      if (h == 0.0) {
        r = g = b = l;
      } else {
        r = g = b = 0;
      }
    } else {
      r = hlsValue(m1, m2, h + 120);
      g = hlsValue(m1, m2, h);
      b = hlsValue(m1, m2, h - 120);
    }
    int rgb = (((int) (r * 255.0)) << 16) | (((int) (g * 255.0)) << 8) | ((int) (b * 255.0));

    return new Color(rgb);
  }

  private static float hlsValue(final float n1, final float n2, float h) {
    if (h > 360) {
      h -= 360;
    } else if (h < 0) {
      h += 360;
    }
    if (h < 60) {
      return n1 + (n2 - n1) * h / 60.0f;
    } else if (h < 180) {
      return n2;
    } else if (h < 240) {
      return n1 + (n2 - n1) * (240.0f - h) / 60.0f;
    }
    return n1;
  }

}
