package com.flamesgroup.antrax.control.swingwidgets;

import java.awt.*;
import java.io.File;
import java.net.URL;

import javax.swing.*;

public class DescrImageIcon extends ImageIcon {

  private static final long serialVersionUID = -7946677508205177628L;

  public DescrImageIcon(final String filename, final String description) {
    super(filename, description);
  }

  private static String getDefaultDescr(final String filename) {
    int startInd = filename.lastIndexOf(File.separator);
    if (startInd < 0) {
      startInd = 0;
    } else if (startInd + 1 < filename.length() - 1) {
      startInd++;
    }

    int stopInd = filename.lastIndexOf('.');
    if (stopInd < 0) {
      stopInd = filename.length();
    }

    return filename.substring(startInd, stopInd);
  }

  public DescrImageIcon(final String filename) {
    this(filename, getDefaultDescr(filename));
  }

  public DescrImageIcon(final URL location, final String description) {
    super(location, description);
  }

  public DescrImageIcon(final URL location) {
    this(location, getDefaultDescr(location.toExternalForm()));
  }

  public DescrImageIcon(final Image image, final String description) {
    super(image, description);
  }

  public DescrImageIcon(final Image image) {
    super(image);
  }

  public DescrImageIcon(final byte[] imageData, final String description) {
    super(imageData, description);
  }

  public DescrImageIcon(final byte[] imageData) {
    super(imageData);
  }

  public DescrImageIcon() {

  }

}
