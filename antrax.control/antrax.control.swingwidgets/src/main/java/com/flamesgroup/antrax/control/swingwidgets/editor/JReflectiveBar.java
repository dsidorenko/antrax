package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.*;

import javax.swing.*;

public class JReflectiveBar extends JPanel {

  private static final long serialVersionUID = -7142329901236750075L;

  private JPanel leftPanel;
  private JPanel rightPanel;

  public JReflectiveBar() {
    this(3, 3);
  }

  public JReflectiveBar(final int hgap, final int vgap) {
    super(new BorderLayout());

    initPanel(getLeftPanel(), hgap, vgap);
    initPanel(getRightPanel(), hgap, vgap);

    add(getLeftPanel(), BorderLayout.CENTER);
    add(getRightPanel(), BorderLayout.EAST);
  }

  @Override
  public void setBackground(final Color bg) {
    getLeftPanel().setBackground(bg);
    getRightPanel().setBackground(bg);
    super.setBackground(bg);
  }

  private void initPanel(final JPanel panel, final int hgap, final int vgap) {
    ((FlowLayout) panel.getLayout()).setHgap(hgap);
    ((FlowLayout) panel.getLayout()).setVgap(vgap);
  }

  private JPanel getLeftPanel() {
    if (leftPanel == null) {
      leftPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    }
    return leftPanel;
  }

  private JPanel getRightPanel() {
    if (rightPanel == null) {
      rightPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    }
    return rightPanel;
  }

  @Override
  public void setOpaque(final boolean isOpaque) {
    super.setOpaque(isOpaque);
    getLeftPanel().setOpaque(isOpaque);
    getRightPanel().setOpaque(isOpaque);
  }

  public void addToLeft(final Component component) {
    getLeftPanel().add(component);
  }

  public void addToRight(final Component component) {
    getRightPanel().add(component);
  }

  private JSeparator createSeparator(final int width) {
    JSeparator separator = new JSeparator(SwingConstants.HORIZONTAL);
    separator.setSize(new Dimension(width, 0));
    return separator;
  }

  public void addSeparatorToLeft(final int width) {
    addToLeft(createSeparator(width));
  }

  public void addSeparatorToRight(final int width) {
    addToRight(createSeparator(width));
  }

  private void setEnabled(final JComponent jComponent, final boolean enabled) {
    for (Component cmp : jComponent.getComponents()) {
      cmp.setEnabled(enabled);
    }
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    setEnabled(getLeftPanel(), enabled);
    setEnabled(getRightPanel(), enabled);
  }

}
