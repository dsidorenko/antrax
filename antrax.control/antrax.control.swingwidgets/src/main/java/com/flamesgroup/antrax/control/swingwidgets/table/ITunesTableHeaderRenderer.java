package com.flamesgroup.antrax.control.swingwidgets.table;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

public class ITunesTableHeaderRenderer extends JPanel implements
    TableCellRenderer {

  private static final long serialVersionUID = 6136461774397584014L;

  protected static final Color BORDER_COLOR = new Color(0x555555);

  private static final int PADDING_TOP = 2;
  private static final int SORT_ICON_TEXT_SPACE = 2;
  private static final int PADDING_BOTTOM = 2;

  protected JTable table;
  protected Object value;
  protected boolean isSelected;
  protected boolean hasFocus;
  protected int row;
  protected int column;

  private Dimension lastPaintedSize;
  protected GradientPaint backgroundGradientSorted;
  protected GradientPaint backgroundGradientUnsorted;
  protected GradientPaint borderGradientUnsorted;
  protected GradientPaint borderGradientSorted;
  protected GradientPaint shadowGradientUnsorted;
  protected GradientPaint shadowGradientSorted;
  protected GradientPaint selectedBackgroundGradientUnsorted;
  protected GradientPaint selectedBackgroundGradientSorted;

  protected Image ascImage = loadImage("ascending_arrow.png");
  protected Image descImage = loadImage("descending_arrow.png");
  protected Image headerImg = loadImage("itunes_table_header.png");

  protected Font headerFont;

  public ITunesTableHeaderRenderer() {
    headerFont = new Font("Helvetica", Font.BOLD, 12);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value,
      final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    this.table = table;
    this.value = value;
    this.isSelected = isSelected;
    this.hasFocus = hasFocus;
    this.row = row;
    this.column = column;
    return this;
  }

  private static Image loadImage(final String name) {
    return new ImageIcon(ITunesTableHeaderRenderer.class.getResource(name)).getImage();
  }

  @Override
  public void validate() {
  }

  @Override
  public void revalidate() {
  }

  @Override
  protected void firePropertyChange(final String propertyName, final Object oldValue,
      final Object newValue) {
  }

  @Override
  public void firePropertyChange(final String propertyName, final boolean oldValue,
      final boolean newValue) {
  }

  @Override
  public Dimension getPreferredSize() {
    Dimension vDimension = super.getPreferredSize();
    vDimension.height = PADDING_TOP + ascImage.getHeight(null)
        + SORT_ICON_TEXT_SPACE + getFont().getSize() + PADDING_BOTTOM;
    return vDimension;
  }

  private static GradientPaint createHeaderGradient(final int height, final Color color1, final Color color2) {
    return new GradientPaint(0, 0, color1, 0, height, color2);
  }

  private void initGraphics() {
    Dimension vSize = getSize();
    if (lastPaintedSize == null) {
      lastPaintedSize = vSize;
    }

    boolean heightChanged = lastPaintedSize.height != vSize.height;
    int h = vSize.height;
    if (backgroundGradientUnsorted == null || heightChanged) {
      backgroundGradientUnsorted = createHeaderGradient(h, new Color(0xdddddd), new Color(0xbbbbbb));
    }

    if (backgroundGradientSorted == null || heightChanged) {
      backgroundGradientSorted = createHeaderGradient(h, new Color(0xc2cfdd), new Color(0x7d93b2));
    }

    if (borderGradientUnsorted == null || heightChanged) {
      borderGradientUnsorted = createHeaderGradient(h, new Color(0xafafaf), new Color(0x969696));
    }

    if (borderGradientSorted == null || heightChanged) {
      borderGradientSorted = createHeaderGradient(h, new Color(0x9ba6b1), new Color(0x64768e));
    }

    if (shadowGradientUnsorted == null || heightChanged) {
      shadowGradientUnsorted = createHeaderGradient(h, new Color(0xe6e6e6), new Color(0xd0d0d0));
    }

    if (shadowGradientSorted == null || heightChanged) {
      shadowGradientSorted = createHeaderGradient(h, new Color(0xcbd6e2), new Color(0x90a3bd));
    }

    if (selectedBackgroundGradientUnsorted == null || heightChanged) {
      selectedBackgroundGradientUnsorted = createHeaderGradient(h, new Color(0xc4c4c4), new Color(0x959595));
    }

    if (selectedBackgroundGradientSorted == null || heightChanged) {
      selectedBackgroundGradientSorted = createHeaderGradient(h, new Color(0xa6b7cb), new Color(0x536b90));
    }

    lastPaintedSize = new Dimension(vSize);
  }

  private SortOrder getSortOrder() {
    SortOrder sortOrder;
    RowSorter<?> rs = (table == null) ? null : table.getRowSorter();
    java.util.List<? extends RowSorter.SortKey> sortKeys = (rs == null) ? null : rs.getSortKeys();
    if (sortKeys != null && sortKeys.size() > 0 && sortKeys.get(0).getColumn() ==
        table.convertColumnIndexToModel(column)) {
      sortOrder = sortKeys.get(0).getSortOrder();
    } else {
      sortOrder = SortOrder.UNSORTED;
    }

    return sortOrder;
  }

  private static boolean isSorted(final SortOrder sortOrder) {
    return (sortOrder != SortOrder.UNSORTED);
  }

  @Override
  public void paint(final Graphics g) {
    Graphics2D g2 = (Graphics2D) g.create();
    try {
      initGraphics();

      SortOrder sortOrder = getSortOrder();
      GradientPaint bgGradient = isSorted(sortOrder)
          ? (isSelected ? selectedBackgroundGradientSorted : backgroundGradientSorted)
          : (isSelected ? selectedBackgroundGradientUnsorted : backgroundGradientUnsorted);
      GradientPaint borderGradient = isSorted(sortOrder)
          ? borderGradientSorted
          : borderGradientUnsorted;
      GradientPaint shadowGradient = isSorted(sortOrder)
          ? shadowGradientSorted
          : shadowGradientUnsorted;

      Dimension vSize = getSize();
      int x = 0;
      int y = 0;
      int h = vSize.height;
      int w = vSize.width;

      //g2.setClip(g.getClipBounds().x, y, w+80, h);

      // Draw background
      if (!isSorted(sortOrder) && !isSelected) {
        g2.drawImage(headerImg, x + 1, y + 1, x + 1 + w - 2, y + 1 + h - 2, 0, 0, headerImg.getWidth(null), headerImg.getHeight(null), null);
      } else {
        g2.setPaint(bgGradient);
        g2.fillRect(x, y, w, h);
      }

      // Draw border
      g2.setPaint(shadowGradient);
      if (column == 0) {
        g2.drawLine(x + 1, y, x + 1, y + h - 1);
      } else {
        g2.drawLine(x, y, x, y + h - 1);
      }

      g2.setPaint(borderGradient);
      if (column == 0) {
        g2.drawLine(x, y, x, y + h - 1);
      }
      g2.drawLine(x + w - 1, y, x + w - 1, y + h - 1);

      g2.setColor(BORDER_COLOR);
      g2.drawLine(x, y, x + w - 1, y);
      g2.drawLine(x, y + h - 1, x + w, y + h - 1);

      // Draw sort icon
      if (isSorted(sortOrder)) {
        Image sortOrderImg = (sortOrder == SortOrder.ASCENDING) ? ascImage : descImage;
        int dy1 = y + PADDING_TOP;
        int dy2 = dy1 + sortOrderImg.getHeight(null);
        int dx1 = x + (w - sortOrderImg.getWidth(null)) / 2;
        int dx2 = dx1 + sortOrderImg.getWidth(null);

        g2.drawImage(sortOrderImg, dx1, dy1, dx2, dy2, 0, 0, sortOrderImg
            .getWidth(null), sortOrderImg.getHeight(null), null);
      }

      // Draw text
      String text = (value == null) ? null : value.toString();
      if (text != null) {
        g2.setFont(headerFont);

        int topMargin = PADDING_TOP + ascImage.getHeight(null) + SORT_ICON_TEXT_SPACE;
        int leftMargin = 2;
        int xt = x + leftMargin;
        int yt = y + topMargin;
        int ht = h - (topMargin + PADDING_BOTTOM);
        int wt = w - 2 * leftMargin;

        int centerX = xt + wt / 2;
        int centerY = yt + ht / 2;

        // get the bounds of the string to draw.
        FontMetrics fontMetrics = g2.getFontMetrics();
        Rectangle stringBounds = fontMetrics.getStringBounds(text, g2).getBounds();

        // get the visual bounds of the text using a GlyphVector.
        Font font = g2.getFont();
        FontRenderContext renderContext = g2.getFontRenderContext();
        GlyphVector glyphVector = font.createGlyphVector(renderContext, text);
        Rectangle visualBounds = glyphVector.getVisualBounds().getBounds();

        // calculate the lower left point at which to draw the string. note that this we
        // give the graphics context the y coordinate at which we want the baseline to
        // be placed. use the visual bounds height to center on in conjunction with the
        // position returned in the visual bounds. the vertical position given back in the
        // visualBounds is a negative offset from the baseline of the text.
        int textX;
        if (visualBounds.width > wt) {
          textX = xt;
        } else {
          textX = centerX - stringBounds.width / 2;
        }
        int textY = centerY - visualBounds.height / 2 - visualBounds.y;

        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);

        g2.setColor(new Color(0xcccccc));
        g2.drawString(text, textX, textY + 1);
        g2.setColor(Color.BLACK);
        g2.drawString(text, textX, textY);
      }
    } finally {
      g2.dispose();
    }
  }

}

