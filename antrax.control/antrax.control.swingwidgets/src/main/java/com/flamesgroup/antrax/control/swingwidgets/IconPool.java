package com.flamesgroup.antrax.control.swingwidgets;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class IconPool {

  private static final IconPool iconPool = new IconPool();

  private final Map<String, Icon> pool;

  public IconPool() {
    pool = new HashMap<>();
  }

  public static IconPool shared() {
    return iconPool;
  }

  public static Icon getShared(final String url) {
    return shared().get(url, new Exception().getStackTrace());
  }

  public static Icon getShared(final URL url) {
    return shared().get(url);
  }

  /**
   * Gets the icon by URL.
   * If URL is relative, it is relative to the caller.
   *
   * @param url
   * @return an icon
   */
  private Icon get(final String url, final StackTraceElement[] stacks) {
    try {
      Class<?> callerClazz = Class.forName(stacks[1].getClassName());
      URL resURL = callerClazz.getResource(url);
      if (resURL == null) {
        System.err.println("Icon couldn't be found '" + url + "'");
        return null;
      } else {
        return get(resURL);
      }
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Gets the icon by URL.
   * If URL is relative, it is relative to the caller.
   *
   * @param url
   * @return an icon
   */
  public Icon get(final String url) {
    return get(url, new Exception().getStackTrace());
  }

  public synchronized Icon get(final URL url) {

    if (url == null) {
      System.err.println("Icon couldn't be found");
      return null;
    }

    Icon icon = (Icon) pool.get(url.toString());
    if (icon == null) {
      icon = new DescrImageIcon(url);
      pool.put(url.toString(), icon);
    }

    return icon;
  }

  public synchronized void clear() {
    pool.clear();
  }

}
