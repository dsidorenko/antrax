package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.transfer.AntraxTransferable;

import java.awt.datatransfer.Transferable;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class JUpdatableTableTransferHandler extends TransferHandler {

  private static final long serialVersionUID = 4403553445578843963L;

  @Override
  public int getSourceActions(final JComponent c) {
    return COPY;
  }

  @Override
  protected Transferable createTransferable(final JComponent c) {
    JUpdatableTable<?, ?> table = (JUpdatableTable<?, ?>) c;
    List<Object> transferable = new LinkedList<>();
    for (int row : table.getSelectedRows()) {
      transferable.addAll(table.getTransferableObjects(row));
    }
    return new AntraxTransferable(transferable.toArray(new Object[transferable.size()]));
  }
}
