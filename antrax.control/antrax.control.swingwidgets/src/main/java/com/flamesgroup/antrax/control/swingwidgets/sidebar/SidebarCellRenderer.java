package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import com.flamesgroup.antrax.control.swingwidgets.SwingHelper;
import com.flamesgroup.antrax.control.swingwidgets.badge.JBadge;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.ListIterator;

import javax.swing.*;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public class SidebarCellRenderer extends BaseSidebarCellRenderer {

  private static final long serialVersionUID = 7286368395951951413L;

  /**
   * Used to paint the TreeCellRenderer.
   */
  protected BadgeRendererPane rendererPane;

  public SidebarCellRenderer() {
    super();
  }

  private boolean isTopmost(final TreeNode node) {
    boolean topmost = false;
    if (node != null) {
      if (node.getParent() == null) {
        topmost = true;
      } else {
        topmost = (node.getParent().getParent() == null);
      }
    }

    return topmost;
  }

  private Rectangle lastBadgeBounds;

  @Override
  public String getToolTipText(final MouseEvent e) {
    if (lastBadgeBounds == null)
      return null;

    Point cmpPoint = new Point(e.getLocationOnScreen());
    SwingUtilities.convertPointFromScreen(cmpPoint, tree);

    Object node = null;
    TreePath path = tree.getPathForLocation(cmpPoint.x, cmpPoint.y);
    if (path != null) {
      node = path.getLastPathComponent();
    }

    if (node == null || !(node instanceof SidebarNode)) {
      return null;
    }

    SidebarNode sidebarNode = (SidebarNode) node;
    if (sidebarNode.hasVisibleBadges()) {
      Rectangle bounds = new Rectangle(lastBadgeBounds);

      //      int offset = 0;
      //      int depthOffset = 15;
      //      TreeNode curNode = sidebarNode;
      //      do {
      //        TreeNode parent = curNode.getParent();
      //        if (parent != null) {
      ////          if (!(parent.getParent() == null && !tree.isRootVisible()))
      ////            offset += depthOffset;
      //          if (curNode.getChildCount() > 0 || isTopmost(curNode))
      //            offset += 13;
      //        }
      //
      //        curNode = parent;
      //      } while(curNode != null);
      //      bounds.x += offset;

      //bounds.x += (isTopmost(sidebarNode)) ? 13 : 0;

      if (!bounds.contains(e.getX(), e.getY()))
        return null;

      int badgeSpace = getIntProperty(JSidebar.BADGE_SPACE, 3);
      List<JBadge> badges = sidebarNode.getBadges();
      ListIterator<JBadge> litr = badges.listIterator(badges.size());
      int lx = bounds.x;
      int rx = lx + bounds.width - 1;
      while (litr.hasPrevious()) {
        JBadge badge = litr.previous();
        if (badge.isVisible()) {
          int bl = rx - badge.getWidth() - (badge.getIcon() == null ? 0 : 13);
          if (e.getX() > bl && e.getX() <= rx) {
            return badge.getToolTipText();
          }
          rx -= badge.getWidth() + badgeSpace;
        }

      }
    }
    return null;
  }

  /**
   * Returns the renderer pane that renderer components are placed in.
   */
  protected BadgeRendererPane createBadgeRendererPane() {
    return new BadgeRendererPane();
  }

  private int getIntProperty(final Object key, final int defaultValue) {
    Object value = tree.getClientProperty(key);
    if (value instanceof Integer) {
      return (Integer) value;
    } else {
      return defaultValue;
    }
  }

  private Color getColorProperty(final Object key, final Color defaultValue) {
    Object value = tree.getClientProperty(key);
    if (value instanceof Color) {
      return (Color) value;
    } else {
      return defaultValue;
    }
  }

  @Override
  protected void paintComponent(final Graphics g) {
    // Create a buffered image to draw the component into.
    // This lets us draw "out" an area, making it transparent
    //BufferedImage image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
    // Create the graphics and set its initial state
    //Graphics2D g2d = image.createGraphics();

    Graphics2D g2d = (Graphics2D) g;

    if (rendererPane == null) {
      if ((rendererPane = createBadgeRendererPane()) != null) {
        tree.add(rendererPane);
      }
    }

    boolean topmost = isTopmost(getNode());

    try {
      // Get cell bounds
      //Rectangle cb = g.getClipBounds();
      Rectangle cellBounds = new Rectangle(0, 0, getWidth(), getHeight());
      lastBadgeBounds = cellBounds;

      // Calculate initial bounds
      //int marginY = getIntProperty(JSidebar.NODE_MARGIN_Y, 0);
      int marginLeft = getIntProperty(JSidebar.CELL_MARGIN_LEFT, 0);
      Rectangle bounds = new Rectangle(cellBounds.x + marginLeft, cellBounds.y, cellBounds.width - marginLeft, cellBounds.height);

      // Apply font
      g2d.setFont(getFont());

      // Draw cell image
      int offset = paintIcon(g2d, bounds, topmost);
      bounds.x += offset;
      bounds.width -= offset;

      // Apply antialiasing
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // Draw cell text
      offset = paintText(g2d, bounds, topmost);
      int marginRight = getIntProperty(JSidebar.CELL_MARGIN_RIGHT, 0);
      bounds.x += offset;
      bounds.width -= offset + marginRight;

      lastBadgeBounds = bounds;

      // Draw cell badges
      paintBadges(g2d, bounds, topmost);

      // Draw the back-buffer image into this component
      //g.drawImage(image, 0, 0, null);
    } finally {
      // dispose of the buffered image graphics.
      if (g2d != null) {
        g2d.dispose();
      }

      // Empty out the renderer pane, allowing renderers to be gc'ed.
      rendererPane.removeAll();
    }
  }

  private int paintIcon(final Graphics2D gc, final Rectangle bounds, final boolean isTopmost) {
    if (getNode() == null || getNode().getIcon() == null) {
      return 0;
    }

    Icon icon = getNode().getIcon();
    int x = bounds.x;
    int y = bounds.y + (bounds.height - icon.getIconHeight()) / 2;
    icon.paintIcon(this, gc, x, y);

    int textIndent = getIntProperty(JSidebar.ICON_INDENT, 2);
    return textIndent + icon.getIconWidth();
  }

  private int paintText(final Graphics2D gc, final Rectangle bounds, final boolean isTopmost) {
    int textIndent = getIntProperty(JSidebar.TEXT_INDENT, 0);
    Color textColor = getColorProperty(JSidebar.TEXT_COLOR, Color.BLACK);
    Color textSelectedColor = getColorProperty(JSidebar.TEXT_SELECTED_COLOR, Color.WHITE);
    Color textShadowColor = getColorProperty(JSidebar.TEXT_SHADOW_COLOR, Color.GRAY);

    // Get text
    SidebarNode node = getNode();
    String text = (node != null) ? node.getName() : getText();
    String nodeText = isTopmost ? text.toUpperCase() : text;

    Rectangle2D strBounds = gc.getFontMetrics().getStringBounds(nodeText, gc);
    int tx = bounds.x;
    int ty = bounds.y + (int) (bounds.getHeight() - strBounds.getY()) / 2;

    // Get text & shadow colors
    Color clr = isSelected() ? textSelectedColor : (isTopmost ? textColor : textShadowColor);
    Color backClr = isSelected() ? (isTopmost ? textColor : textShadowColor) : textSelectedColor;

    gc.setColor(backClr);
    SwingHelper.drawString(gc, nodeText, tx, ty + 1);

    gc.setColor(clr);
    SwingHelper.drawString(gc, nodeText, tx, ty);

    return textIndent + (int) strBounds.getWidth();
  }

  private void paintBadges(final Graphics2D gc, final Rectangle bounds, final boolean isTopmost) {
    // Check badges count
    if (getNode() == null)
      return;
    List<JBadge> badges = getNode().getBadges();
    if (badges == null || badges.isEmpty())
      return;

    // Set clip rectangle for badges
    gc.setClip(bounds.x, bounds.y, bounds.width, bounds.height);

    Rectangle badgeLeftBounds = new Rectangle(bounds);
    int badgeSpace = getIntProperty(JSidebar.BADGE_SPACE, 3);

    // Get badges from last to first
    // and draw from right to left
    ListIterator<JBadge> litr = badges.listIterator(badges.size());
    while (litr.hasPrevious()) {
      JBadge badge = litr.previous();
      if (badge.isVisible()) {
        int badgeWidth = drawBadge(badge, gc, badgeLeftBounds);
        badgeLeftBounds.width -= badgeWidth + badgeSpace;
      }
    }
  }

  private int drawBadge(final JBadge badge, final Graphics2D gc, final Rectangle bounds) {
    int marginY = getIntProperty(JSidebar.NODE_MARGIN_Y, 3);
    return rendererPane.paintComponent(badge, gc, bounds, isSelected(), marginY);

    //    int x = bounds.x;
    //    int y = bounds.y + marginY/2;
    //    int w = bounds.width;
    //    int h = bounds.height - marginY;

    //    if (badge == null) {
    //        return 0;
    //    }
    //
    //    if (badge.getParent() != this) {
    //        this.add(badge);
    //        badge.setFont(getFont());
    //    }
    //
    //    badge.setSelected(isSelected());
    //    badge.setBounds(x, y, w, h);
    //
    //    Graphics cg = gc.create(x, y, w, h);
    //    try {
    //      badge.paint(cg);
    //    }
    //    finally {
    //        cg.dispose();
    //    }
    //
    //    int wd = badge.getWidth();
    //
    //    badge.setBounds(-w, -h, 0, 0);
    //
    //    return wd;
  }

}
