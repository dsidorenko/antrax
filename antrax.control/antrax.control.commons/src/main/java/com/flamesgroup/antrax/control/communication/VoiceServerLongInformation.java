/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.TimeUtils;
import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class VoiceServerLongInformation implements IVoiceServerLongInformation {

  private static final long serialVersionUID = 90356666023871102L;

  private String version;

  private final CallsStatistic callsStatistic = new CallsStatistic();
  private final ChannelsStatistic channelsStatistic = new ChannelsStatistic();
  private final HardwareStatistic hardwareStatistic = new HardwareStatistic();

  private final Map<ChannelUID, CallChannelState.State> prevStates = new HashMap<>();
  private final Map<ChannelUID, Pair<CallState.State, Long>> lastStates = new HashMap<>();

  private long startTime = 0;

  private final TimeProvider time = new ServerSyncTimeProvider();

  public void setVersion(final String version) {
    this.version = version;
  }

  @Override
  public String getVersion() {
    return version;
  }

  @Override
  public long getUptimeMs() {
    if (startTime == 0) {
      return 0;
    }
    return time.currentTimeMillis() - startTime;
  }

  @Override
  public long getACD() {
    int count = callsStatistic.getSuccessfulPerHistory().getCount();
    long total = callsStatistic.getSuccessfulPerHistory().getDuration();
    if (count == 0) {
      return 0;
    }
    return total / count;
  }

  @Override
  public long getASR() {
    int total = callsStatistic.getTotalPerHistory().getCount();
    int count = callsStatistic.getSuccessfulPerHistory().getCount();
    if (total == 0) {
      return 0;
    }
    return (long) (((double) count / total) * 100);
  }

  @Override
  public CallsStatistic getCallsStatistic() {
    return callsStatistic;
  }

  @Override
  public void handleCallEnded(final ChannelUID simUnit) {
    Pair<CallState.State, Long> pair = lastStates.get(simUnit);
    if (pair == null) {
      callsStatistic.registerBadCall();
      return;
    }

    CallState.State last = pair.first();
    long timeout = System.currentTimeMillis() - pair.second();
    switch (last) {
      case ACTIVE:
        callsStatistic.registerSuccessCall(timeout);
        break;
      default:
        callsStatistic.registerBadCall();
    }
  }

  @Override
  public void handleCallChangeState(final ChannelUID simUnit, final CallState.State callState) {
    lastStates.put(simUnit, new Pair<>(callState, System.currentTimeMillis()));
  }

  @Override
  public void handleCallChannelChangedState(final ChannelUID simUnit, final CallChannelState.State state) {
    CallChannelState.State prevState = prevStates.get(simUnit);
    if (state == CallChannelState.State.RELEASING) {
      prevStates.remove(simUnit);
    } else {
      prevStates.put(simUnit, state);
    }
    if (prevState != null) {
      decrease(prevState);
    }
    increase(state);
  }

  @Override
  public ChannelsStatistic getChannelsStatistic() {
    return channelsStatistic;
  }

  @Override
  public HardwareStatistic getHardwareStatistic() {
    return hardwareStatistic;
  }

  public void changeServerStatus(final ServerStatus status) {
    if (status == ServerStatus.STARTED) {
      startTime = time.currentTimeMillis();
    } else if (status == ServerStatus.STOPPING) {
      startTime = 0;
    }
  }

  @Override
  public void report(final PrintStream out) {
    out.append(report());
  }

  @Override
  public String report() {
    StringBuilder out = new StringBuilder();
    out.append(String.format("           Uptime: %-8s    Version: %s%n", TimeUtils.writeTime(getUptimeMs()), getVersion()));
    out.append(String.format("              ACD: %-8d        ASR: %d%n", getACD(), getASR()));
    out.append(String.format("            GSM Count: %-8d   Channels: %d%n", hardwareStatistic.getGsmCount(), hardwareStatistic.getGsmChannels()));
    out.append(String.format("       ------      %-8s  Sim Channels: %d%n", "", hardwareStatistic.getSimChannels()));
    out.append(String.format("            Total: %-8dUnconnected: %d%n", channelsStatistic.getTotalChannelsCount(), channelsStatistic.getNotConnectedChannels()));
    out.append(String.format("             Free: %-8d   Sleeping: %d%n", channelsStatistic.getFreeChannels(), channelsStatistic.getSleepingChannels()));
    out.append(String.format("Business Activity: %-8d   SelfCall: %d%n", channelsStatistic.getInBusinessActivity(), channelsStatistic.getWaitingSelfCallsChannels()));
    out.append(String.format("       ------      %-8s  Bandwidth: %d%n", "", channelsStatistic.getBandwidth()));
    out.append(String.format("    Total Per Day: %-8d   Duration: %s%n", callsStatistic.getTotalPerDay().getCount(), TimeUtils.writeTime(callsStatistic.getTotalPerDay().getDuration())));
    out.append(String.format("   Total Per Hour: %-8d   Duration: %s%n", callsStatistic.getTotalPerHour().getCount(), TimeUtils.writeTime(callsStatistic.getTotalPerHour().getDuration())));
    out.append(String.format("Total Per History: %-8d   Duration: %s%n", callsStatistic.getTotalPerHistory().getCount(), TimeUtils.writeTime(callsStatistic.getTotalPerHistory().getDuration())));
    out.append(String.format("  Success Per Day: %-8dBad Per Day: %s%n", callsStatistic.getSuccessfulPerDay().getCount(), callsStatistic.getZeroPerDay().getCount()));
    out.append(String.format(" Success Per Hour: %-7dBad Per Hour: %s%n", callsStatistic.getSuccessfulPerHour().getCount(), callsStatistic.getZeroPerHour().getCount()));
    out.append(String.format(" Success Per Hist: %-7dBad Per Hist: %s%n", callsStatistic.getSuccessfulPerHistory().getCount(), callsStatistic.getZeroPerHistory().getCount()));
    return out.toString();
  }


  private void increase(final CallChannelState.State state) {
    switch (state) {
      case IDLE_AFTER_SELF_CALL:
      case IDLE_BEFORE_SELF_CALL:
      case IDLE_BEFORE_UNREG:
      case IDLE_BETWEEN_CALLS:
      case IDLE_AFTER_REGISTRATION:
        channelsStatistic.incSleeping();
        break;
      case STARTED_BUSINESS_ACTIVITY:
        channelsStatistic.incInBusinessActivity();
        break;
      case READY_TO_CALL:
        channelsStatistic.incFree();
        break;
      case OUTGOING_CALL:
        channelsStatistic.incTalking();
        break;
      case RELEASING:
        channelsStatistic.incNotConnectedChannelsCount();
        break;
      case WAITING_SELF_CALL:
        channelsStatistic.incWaitingSelfCall();
        break;
    }
  }

  private void decrease(final CallChannelState.State state) {
    switch (state) {
      case IDLE_AFTER_SELF_CALL:
      case IDLE_BEFORE_SELF_CALL:
      case IDLE_BEFORE_UNREG:
      case IDLE_BETWEEN_CALLS:
      case IDLE_AFTER_REGISTRATION:
        channelsStatistic.decSleeping();
        break;
      case STARTED_BUSINESS_ACTIVITY:
        channelsStatistic.decInBusinessActivity();
        break;
      case READY_TO_CALL:
        channelsStatistic.decFree();
        break;
      case OUTGOING_CALL:
        channelsStatistic.decTalking();
        break;
      case REGISTERING_IN_GSM:
        channelsStatistic.decNotConnectedChannelsCount();
        break;
      case RELEASING:
        break;
      case WAITING_SELF_CALL:
        channelsStatistic.decWaitingSelfCall();
        break;
    }
  }

}
