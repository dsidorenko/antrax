/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.authorization;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class AuthorizationData implements Serializable {

  private static final long serialVersionUID = -2257033076019978607L;

  private final long timestamp1;
  private final double random1;
  private final long timestamp2;
  private final double random2;
  private final String name;
  private BigInteger secretKey;

  public AuthorizationData(final String name, final String passwd) {
    this(name);
    setPasswd(passwd);

  }

  public AuthorizationData(final String name) {
    if (name == null)
      throw new IllegalArgumentException("name is null");
    timestamp1 = System.currentTimeMillis();
    random1 = Math.random(); // I think time of counting this two randoms
    random2 = Math.random(); // is hardly predictable
    timestamp2 = System.currentTimeMillis();
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public boolean checkPassword(final String passwd) {
    return secretKey.equals(countSecret(name, timestamp1, random1, timestamp2, random2, passwd));
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof AuthorizationData) {
      return equals((AuthorizationData) obj);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return (int) (name.hashCode() + 25 * timestamp1 + 145 * timestamp2 + 6345 * random1 + 56732 * random2);
  }

  private boolean equals(final AuthorizationData data) {
    return name.equals(data.name) && timestamp1 == data.timestamp1 && timestamp2 == data.timestamp2 && random1 == data.random1
        && random2 == data.random2;
  }

  public void setPasswd(final String passwd) {
    if (passwd == null)
      throw new IllegalArgumentException("passwd is null");
    secretKey = countSecret(name, timestamp1, random1, timestamp2, random2, passwd);
  }

  static BigInteger countSecret(final String name, final long timestamp1, final double random1, final long timestamp2, final double random2, final String passwd) {
    try {
      byte[] digest = makeDigest(name, passwd, timestamp1, random1);
      return new BigInteger(makeDigest(digest, timestamp2, random2));
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  private static byte[] makeDigest(final String user, final String password, final long timestamp, final double random) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("SHA");
    md.update(user.getBytes());
    md.update(password.getBytes());
    md.update(makeBytes(timestamp, random));
    return md.digest();
  }

  private static byte[] makeDigest(final byte[] mush, final long t2, final double q2) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("SHA");
    md.update(mush);
    md.update(makeBytes(t2, q2));
    return md.digest();
  }

  private static byte[] makeBytes(final long t, final double q) {
    try {
      ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
      DataOutputStream dataOut = new DataOutputStream(byteOut);
      dataOut.writeLong(t);
      dataOut.writeDouble(q);
      return byteOut.toByteArray();
    } catch (IOException ignore) {
      return new byte[0];
    }
  }

}
