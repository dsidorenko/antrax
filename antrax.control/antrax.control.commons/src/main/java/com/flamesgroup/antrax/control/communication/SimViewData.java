/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

import java.io.Serializable;

public class SimViewData implements Serializable {

  private static final long serialVersionUID = -7439393960267097233L;

  // TODO: remove and set correct timeout on server
  private final TimeProvider timeProvider = new ServerSyncTimeProvider();

  // copied from SimViewData
  private ChannelUID simHolder;
  private ChannelUID gsmHolder;

  private String currentGateway;
  private String previousGateway;

  private String status;
  private long statusTime;

  private long reconfigureTime;
  private long lastCallDuration;

  private String userMessage1;
  private long userMessageTime1;
  private String userMessage2;
  private long userMessageTime2;
  private String userMessage3;
  private long userMessageTime3;

  private boolean empty;
  private boolean live;
  private ChannelConfig channelConfig;

  // copied from SimData
  private ICCID uid;

  private PhoneNumber phoneNumber;
  private IMEI imei;

  private String simGroupName;

  private boolean enabled;
  private boolean locked;
  private String lockReason;
  private long lockTime;

  private int totalCallsCount;
  private int successfulCallsCount;
  private long callDuration;
  private int incomingTotalCallsCount;
  private int incomingSuccessfulCallsCount;
  private long incomingCallDuration;

  private int successOutgoingSmsCount;
  private int totalOutgoingSmsCount;
  private int incomingSMSCount;
  private int countSmsStatuses;
  private String note;
  private boolean allowInternet;
  private long tariffPlanEndDate;

  private double balance;

  public ChannelUID getSimHolder() {
    return simHolder;
  }

  public SimViewData setSimHolder(final ChannelUID simHolder) {
    this.simHolder = simHolder;
    return this;
  }

  public ChannelUID getGsmHolder() {
    return gsmHolder;
  }

  public SimViewData setGsmHolder(final ChannelUID gsmHolder) {
    this.gsmHolder = gsmHolder;
    return this;
  }

  public String getCurrentGateway() {
    return currentGateway;
  }

  public SimViewData setCurrentGateway(final String currentGateway) {
    this.currentGateway = currentGateway;
    return this;
  }

  public String getPreviousGateway() {
    return previousGateway;
  }

  public SimViewData setPreviousGateway(final String previousGateway) {
    this.previousGateway = previousGateway;
    return this;
  }

  public String getStatus() {
    return status;
  }

  public SimViewData setStatus(final String status) {
    this.status = status;
    return this;
  }

  public long getStatusTime() {
    return statusTime;
  }

  public SimViewData setStatusTime(final long statusTime) {
    this.statusTime = statusTime;
    return this;
  }

  public long getReconfigureTime() {
    return reconfigureTime;
  }

  public SimViewData setReconfigureTime(final long reconfigureTime) {
    this.reconfigureTime = reconfigureTime;
    return this;
  }

  public long getLastCallDuration() {
    return lastCallDuration;
  }

  public SimViewData setLastCallDuration(final long lastCallDuration) {
    this.lastCallDuration = lastCallDuration;
    return this;
  }

  public String getUserMessage1() {
    return userMessage1;
  }

  public SimViewData setUserMessage1(final String userMessage1) {
    this.userMessage1 = userMessage1;
    return this;
  }

  public long getUserMessageTime1() {
    return userMessageTime1;
  }

  public SimViewData setUserMessageTime1(final long userMessageTime1) {
    this.userMessageTime1 = userMessageTime1;
    return this;
  }

  public String getUserMessage2() {
    return userMessage2;
  }

  public SimViewData setUserMessage2(final String userMessage2) {
    this.userMessage2 = userMessage2;
    return this;
  }

  public long getUserMessageTime2() {
    return userMessageTime2;
  }

  public SimViewData setUserMessageTime2(final long userMessageTime2) {
    this.userMessageTime2 = userMessageTime2;
    return this;
  }

  public String getUserMessage3() {
    return userMessage3;
  }

  public SimViewData setUserMessage3(final String userMessage3) {
    this.userMessage3 = userMessage3;
    return this;
  }

  public long getUserMessageTime3() {
    return userMessageTime3;
  }

  public SimViewData setUserMessageTime3(final long userMessageTime3) {
    this.userMessageTime3 = userMessageTime3;
    return this;
  }

  public boolean isEmpty() {
    return empty;
  }

  public SimViewData setEmpty(final boolean empty) {
    this.empty = empty;
    return this;
  }

  public boolean isLive() {
    return live;
  }

  public SimViewData setLive(final boolean live) {
    this.live = live;
    return this;
  }

  public ChannelConfig getChannelConfig() {
    return channelConfig;
  }

  public SimViewData setChannelConfig(final ChannelConfig channelConfig) {
    this.channelConfig = channelConfig;
    return this;
  }

  public ICCID getUid() {
    return uid;
  }

  public SimViewData setUid(final ICCID uid) {
    this.uid = uid;
    return this;
  }

  public PhoneNumber getPhoneNumber() {
    return phoneNumber;
  }

  public SimViewData setPhoneNumber(final PhoneNumber phoneNumber) {
    this.phoneNumber = phoneNumber;
    return this;
  }

  public IMEI getImei() {
    return imei;
  }

  public SimViewData setImei(final IMEI imei) {
    this.imei = imei;
    return this;
  }

  public String getSimGroupName() {
    return simGroupName;
  }

  public SimViewData setSimGroupName(final String simGroupName) {
    this.simGroupName = simGroupName;
    return this;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public SimViewData setEnabled(final boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public boolean isLocked() {
    return locked;
  }

  public SimViewData setLocked(final boolean locked) {
    this.locked = locked;
    return this;
  }

  public String getLockReason() {
    return lockReason;
  }

  public SimViewData setLockReason(final String lockReason) {
    this.lockReason = lockReason;
    return this;
  }

  public long getLockTime() {
    return lockTime;
  }

  public SimViewData setLockTime(final long lockTime) {
    this.lockTime = lockTime;
    return this;
  }

  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  public SimViewData setTotalCallsCount(final int totalCallsCount) {
    this.totalCallsCount = totalCallsCount;
    return this;
  }

  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  public SimViewData setSuccessfulCallsCount(final int successfulCallsCount) {
    this.successfulCallsCount = successfulCallsCount;
    return this;
  }

  public long getCallDuration() {
    return callDuration;
  }

  public SimViewData setCallDuration(final long callDuration) {
    this.callDuration = callDuration;
    return this;
  }

  public int getIncomingTotalCallsCount() {
    return incomingTotalCallsCount;
  }

  public SimViewData setIncomingTotalCallsCount(final int incomingTotalCallsCount) {
    this.incomingTotalCallsCount = incomingTotalCallsCount;
    return this;
  }

  public int getIncomingSuccessfulCallsCount() {
    return incomingSuccessfulCallsCount;
  }

  public SimViewData setIncomingSuccessfulCallsCount(final int incomingSuccessfulCallsCount) {
    this.incomingSuccessfulCallsCount = incomingSuccessfulCallsCount;
    return this;
  }

  public long getIncomingCallDuration() {
    return incomingCallDuration;
  }

  public SimViewData setIncomingCallDuration(final long incomingCallDuration) {
    this.incomingCallDuration = incomingCallDuration;
    return this;
  }

  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  public SimViewData setSuccessOutgoingSmsCount(final int successOutgoingSmsCount) {
    this.successOutgoingSmsCount = successOutgoingSmsCount;
    return this;
  }

  public int getTotalOutgoingSmsCount() {
    return totalOutgoingSmsCount;
  }

  public SimViewData setTotalOutgoingSmsCount(final int totalOutgoingSmsCount) {
    this.totalOutgoingSmsCount = totalOutgoingSmsCount;
    return this;
  }

  public int getIncomingSMSCount() {
    return incomingSMSCount;
  }

  public SimViewData setIncomingSMSCount(final int incomingSMSCount) {
    this.incomingSMSCount = incomingSMSCount;
    return this;
  }

  public int getCountSmsStatuses() {
    return countSmsStatuses;
  }

  public SimViewData setCountSmsStatuses(final int countSmsStatuses) {
    this.countSmsStatuses = countSmsStatuses;
    return this;
  }

  public long getServerTimeMillis() {
    return timeProvider.currentTimeMillis();
  }

  public String getNote() {
    return note;
  }

  public SimViewData setNote(final String note) {
    this.note = note;
    return this;
  }

  public boolean isAllowInternet() {
    return allowInternet;
  }

  public SimViewData setAllowInternet(final boolean allowInternet) {
    this.allowInternet = allowInternet;
    return this;
  }

  public long getTariffPlanEndDate() {
    return tariffPlanEndDate;
  }

  public SimViewData setTariffPlanEndDate(final long tariffPlanEndDate) {
    this.tariffPlanEndDate = tariffPlanEndDate;
    return this;
  }

  public double getBalance() {
    return balance;
  }

  public SimViewData setBalance(final double balance) {
    this.balance = balance;
    return this;
  }

}
