/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import java.io.Serializable;
import java.util.Objects;

public class SmsStatistic implements Serializable {

  private static final long serialVersionUID = 3024873658612582519L;

  private final String ipAddress;
  private final int amountSendSmsADay;
  private final int limitADay;

  public SmsStatistic(final String ipAddress, final int amountSendSmsADay, final int limitADay) {
    this.ipAddress = ipAddress;
    this.amountSendSmsADay = amountSendSmsADay;
    this.limitADay = limitADay;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public int getAmountSendSmsADay() {
    return amountSendSmsADay;
  }

  public int getLimitADay() {
    return limitADay;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SmsStatistic)) {
      return false;
    }
    final SmsStatistic that = (SmsStatistic) object;

    return amountSendSmsADay == that.amountSendSmsADay
        && limitADay == that.limitADay
        && Objects.equals(ipAddress, that.ipAddress);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(ipAddress);
    result = prime * result + amountSendSmsADay;
    result = prime * result + limitADay;
    return result;
  }

}
