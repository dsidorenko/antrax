/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.unit.CHV;
import com.flamesgroup.unit.ICCID;

import java.util.List;

public interface ActivationBean extends PermissionChecker {

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "remove PIN")
  void removeCHV(ClientUID client, ICCID[] simUIDs, CHV chv1) throws NotPermittedException, NoSuchSimException, PinOperationException, ConnectionException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "enable SIM")
  void enableSimUnit(ClientUID client, ICCID[] simUIDs, boolean enableStatus) throws NotPermittedException, NoSuchSimException, ConnectionException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "lock SIM")
  void lockSimUnit(ClientUID client, ICCID[] simUIDs, boolean lockStatus, String reason) throws NotPermittedException, NoSuchSimException, ConnectionException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "reset scripts states")
  void resetScriptsStates(ClientUID clientuid, ICCID[] simUIDs);

  @PermitTo(groups = {UserGroup.ADMIN}, description = "reset IMEI")
  void resetIMEI(ClientUID clientuid, List<ICCID> simUIDs);

  @PermitTo(groups = {UserGroup.ADMIN}, description = "update GSM channels list")
  void updateGSMChannels(ClientUID client, GSMChannel[] channels) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "note SIM")
  void noteSimUnit(ClientUID clientUID, ICCID[] uids, String note) throws ConnectionException, NoSuchSimException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "invert allow internet")
  void invertAllowInternet(ClientUID clientUID, ICCID[] uids) throws NotPermittedException, ConnectionException, NoSuchSimException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "set tariff plan end date for sim")
  void setTariffPlanEndDate(ClientUID clientUID, ICCID[] uids, long endDate) throws ConnectionException, NoSuchSimException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "reset statistic of SIM card")
  void resetSimCardStatistic(ClientUID clientUID, ICCID[] iccids) throws ConnectionException;
}
