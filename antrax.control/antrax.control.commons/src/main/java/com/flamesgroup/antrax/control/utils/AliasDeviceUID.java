/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.utils;

import com.flamesgroup.device.DeviceUID;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class AliasDeviceUID {

  private static final String ALIAS_DEVICEUID_PROPERTIES = "devices.alias.properties";

  private static final Map<DeviceUID, String> aliases = new HashMap<>();

  private AliasDeviceUID() {
  }

  static {
    Properties properties = new Properties();
    InputStream inputStream = AliasDeviceUID.class.getClassLoader().getResourceAsStream(ALIAS_DEVICEUID_PROPERTIES);
    if (inputStream == null) {
      throw new IllegalStateException("Can't find properties " + ALIAS_DEVICEUID_PROPERTIES);
    }

    try {
      properties.load(inputStream);
    } catch (IOException e) {
      throw new IllegalStateException("Can't load properties " + ALIAS_DEVICEUID_PROPERTIES, e);
    }

    for (Map.Entry<Object, Object> entry : properties.entrySet()) {
      DeviceUID deviceUID = DeviceUID.valueFromCanonicalName(entry.getKey().toString());
      String alias = entry.getValue().toString();
      if (aliases.containsValue(alias)) {
        throw new IllegalStateException("found duplicate of alias " + alias);
      } else {
        aliases.put(deviceUID, alias);
      }
    }
  }

  public static String getAlias(final DeviceUID deviceUID) {
    return aliases.get(deviceUID);
  }

}
