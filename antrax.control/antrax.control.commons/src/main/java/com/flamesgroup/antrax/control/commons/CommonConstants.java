/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.commons;

public interface CommonConstants {

  long REFRESH_TIMEOUT = 5000;

  String RMI_AUTHORIZATION_BEAN = "AuthorizationBean";
  String RMI_CONTROL_BEAN = "ControlBean";
  String RMI_CONFIGURATION_BEAN = "ConfigurationBean";
  String RMI_ACTIVATION_BEAN = "ActivationBean";
  String RMI_REGISTRY_ACCESS_BEAN = "RegistryAccessBean";
  String RMI_VOIP_ANTI_SPAM_STATISTIC_BEAN = "IVoipAntiSpamStatisticBean";
  String RMI_GSM_VIEW_BEAN = "IGsmViewBean";
  String RMI_PREFIX_LIST_BEAN = "IPrefixListBean";
  String RMI_CONTROL_AUDIO_MANAGER = "ControlAudioManager";

}
