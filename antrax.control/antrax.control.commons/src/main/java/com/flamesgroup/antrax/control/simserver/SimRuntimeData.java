/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.ChannelUID;

import java.io.Serializable;

public class SimRuntimeData implements Serializable {

  private static final long serialVersionUID = 4470852958146388036L;

  private ChannelUID simHolder;
  private ChannelUID gsmHolder;

  private String currentGateway;
  private String previousGateway;

  private String status;
  private long statusTime;

  private long reconfigureTime;
  private long lastCallDuration;

  private String userMessage1;
  private long userMessageTime1;
  private String userMessage2;
  private long userMessageTime2;
  private String userMessage3;
  private long userMessageTime3;

  private boolean empty;
  private boolean live;
  private ChannelConfig channelConfig;

  public ChannelUID getSimHolder() {
    return simHolder;
  }

  public SimRuntimeData setSimHolder(final ChannelUID simHolder) {
    this.simHolder = simHolder;
    return this;
  }

  public ChannelUID getGsmHolder() {
    return gsmHolder;
  }

  public SimRuntimeData setGsmHolder(final ChannelUID gsmHolder) {
    this.gsmHolder = gsmHolder;
    return this;
  }

  public String getCurrentGateway() {
    return currentGateway;
  }

  public SimRuntimeData setCurrentGateway(final String currentGateway) {
    this.currentGateway = currentGateway;
    return this;
  }

  public String getPreviousGateway() {
    return previousGateway;
  }

  public SimRuntimeData setPreviousGateway(final String previousGateway) {
    this.previousGateway = previousGateway;
    return this;
  }

  public String getStatus() {
    return status;
  }

  public SimRuntimeData setStatus(final String status) {
    this.status = status;
    return this;
  }

  public long getStatusTime() {
    return statusTime;
  }

  public SimRuntimeData setStatusTime(final long statusTime) {
    this.statusTime = statusTime;
    return this;
  }

  public long getReconfigureTime() {
    return reconfigureTime;
  }

  public SimRuntimeData setReconfigureTime(final long reconfigureTime) {
    this.reconfigureTime = reconfigureTime;
    return this;
  }

  public long getLastCallDuration() {
    return lastCallDuration;
  }

  public SimRuntimeData setLastCallDuration(final long lastCallDuration) {
    this.lastCallDuration = lastCallDuration;
    return this;
  }

  public String getUserMessage1() {
    return userMessage1;
  }

  public SimRuntimeData setUserMessage1(final String userMessage1) {
    this.userMessage1 = userMessage1;
    return this;
  }

  public long getUserMessageTime1() {
    return userMessageTime1;
  }

  public SimRuntimeData setUserMessageTime1(final long userMessageTime1) {
    this.userMessageTime1 = userMessageTime1;
    return this;
  }

  public String getUserMessage2() {
    return userMessage2;
  }

  public SimRuntimeData setUserMessage2(final String userMessage2) {
    this.userMessage2 = userMessage2;
    return this;
  }

  public long getUserMessageTime2() {
    return userMessageTime2;
  }

  public SimRuntimeData setUserMessageTime2(final long userMessageTime2) {
    this.userMessageTime2 = userMessageTime2;
    return this;
  }

  public String getUserMessage3() {
    return userMessage3;
  }

  public SimRuntimeData setUserMessage3(final String userMessage3) {
    this.userMessage3 = userMessage3;
    return this;
  }

  public long getUserMessageTime3() {
    return userMessageTime3;
  }

  public SimRuntimeData setUserMessageTime3(final long userMessageTime3) {
    this.userMessageTime3 = userMessageTime3;
    return this;
  }

  public boolean isEmpty() {
    return empty;
  }

  public SimRuntimeData setEmpty(final boolean empty) {
    this.empty = empty;
    return this;
  }

  public boolean isLive() {
    return live;
  }

  public SimRuntimeData setLive(final boolean live) {
    this.live = live;
    return this;
  }

  public ChannelConfig getChannelConfig() {
    return channelConfig;
  }

  public SimRuntimeData setChannelConfig(final ChannelConfig channelConfig) {
    this.channelConfig = channelConfig;
    return this;
  }

}
