/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.control.communication.PermissionChecker;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;

import java.util.List;
import java.util.Map;

public interface IControlAudioManager extends PermissionChecker {

  @PermitTo(groups = {UserGroup.ADMIN}, description = "list voice server audio files")
  List<AudioFile> getAudioFiles(ClientUID clientUID, IServerData selectedServer) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "download audio files")
  byte[] downloadAudioFileFromServer(ClientUID clientUID, IServerData selectedServer, String fileName) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get IVR templates SIM groups")
  List<String> getIvrTemplatesSimGroups(ClientUID clientUID, IServerData selectedServer) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get IVR templates")
  List<IvrTemplateWrapper> getIvrTemplates(ClientUID clientUID, IServerData selectedServer, String simGroup) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "create IVR templates SIM groups")
  void createIvrTemplateSimGroup(ClientUID clientUID, IServerData selectedServer, String simGroup) throws NotPermittedException, IvrTemplateException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "remove IVR templates SIM groups")
  void removeIvrTemplateSimGroups(ClientUID clientUID, IServerData selectedServer, List<String> simGroups) throws NotPermittedException, IvrTemplateException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get call drop Reasons")
  Map<String, Integer> getCallDropReasons(ClientUID clientUID, IServerData selectedServer) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "upload ivr template file")
  void uploadIvrTemplate(ClientUID clientUID, IServerData selectedServer, String selectedSimGroup, byte[] bytes, String fileName) throws NotPermittedException, IvrTemplateException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "remove ivr template file")
  void removeIvrTemplate(ClientUID clientUID, IServerData selectedServer, String selectedSimGroup, List<IvrTemplateWrapper> selectedIvrTemplates) throws NotPermittedException, IvrTemplateException;

}
