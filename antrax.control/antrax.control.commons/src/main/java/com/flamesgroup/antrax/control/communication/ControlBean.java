/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SessionParams;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ControlBean extends PermissionChecker {

  /**
   * Checks session status
   * <p>
   * When session is active nothing happens. In case of already closed session
   * in throws {@link NotPermittedException}
   *
   * @param clientUID
   * @throws NotPermittedException in case of closed session
   */
  // This method should be permitted to anyone
  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "test whether session is closed")
  void checkSession(ClientUID clientUID) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "get SIM channels information by server")
  List<SimViewData> getSimRuntimeData(ClientUID client, String serverName) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "see voice servers list")
  List<IServerData> listVoiceServers(ClientUID client) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "get sim channels config")
  Pair<Long, List<ChannelConfig>> getSimChannelConfig(ClientUID client, String serverName) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "see sim servers list")
  List<IServerData> listSimServers(ClientUID client) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "get SIM channels information diff by server")
  List<GraphComparator.Delta> getSimRuntimeDataDiff(ClientUID client, String serverName) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "see voice server channels")
  List<IVoiceServerChannelsInfo> listVoiceServerChannels(ClientUID client, IServerData server) throws NotPermittedException, NoSuchFieldException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "see voice server lists info")
  Map<Server, IVoiceServerStatus> listVoiceServerStatus(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "see sim server lists info")
  Map<Server, ISimServerStatus> listSimServerStatus(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER, UserGroup.GUEST}, description = "see voice server channels statistic")
  List<MobileGatewayChannelInformation> listVoiceServerChannelsStatistic(ClientUID clientUID, IServerData server);

  @PermitTo(groups = {UserGroup.SYSTEM}, description = "Shutdown ui server")
  void shutdown(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.SYSTEM}, description = "Get ui server uptime")
  long getUptime(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "List CDRs")
  CDR[] listCDRs(ClientUID client, Date fromDate, Date toDate, String gsmGroup, String simGroup, String callerNumber, String calledNumber, ICCID simUid,
      int pageSize, int offset) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "List SIM events by SIM UID")
  SIMEventRec[] listSIMEvents(ClientUID client, ICCID simUID, Date fromDate, Date toDate, int limit) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "Find SIM UID")
  ICCID[] findSIMList(ClientUID client, SimSearchingParams params) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "Find CDR by ID")
  CDR getCDR(ClientUID client, Long id) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "fire event")
  void fireEvent(ClientUID client, IServerData server, ICCID simUID, String event) throws NotPermittedException, NoSuchFieldException, EventException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "lock/unlock gsm channels")
  void lockGsmChannel(ClientUID client, IServerData server, ChannelUID gsmChannel, boolean lock, final String lockReason) throws NotPermittedException,
      NoSuchFieldException, LockException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "sending ussd")
  void sendUssd(ClientUID client, IServerData server, ICCID simUID, String ussd) throws NotPermittedException, NoSuchFieldException, USSDException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "startUSSDSession")
  String startUSSDSession(ClientUID client, IServerData server, ICCID simUID, String ussd) throws NotPermittedException, NoSuchFieldException, USSDException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "sendUSSDSessionCommand")
  String sendUSSDSessionCommand(ClientUID client, IServerData server, ICCID simUID, String command) throws NotPermittedException, NoSuchFieldException, USSDException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "endUSSDSession")
  void endUSSDSession(ClientUID client, IServerData server, ICCID simUID) throws NotPermittedException, NoSuchFieldException, USSDException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "execute NetworkSurvey")
  void executeNetworkSurvey(ClientUID client, IServerData server, ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, NetworkSurveyException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "sending sms")
  void sendSMS(ClientUID client, IServerData server, ICCID simUID, String number, String smsText) throws NotPermittedException, NoSuchFieldException, SMSException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "sending dtmf")
  void sendDTMF(ClientUID client, IServerData server, ICCID simUID, String dtmf) throws NotPermittedException, NoSuchFieldException, DTMFException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "reset statistic")
  void resetStatistic(ClientUID client, IServerData server) throws NotPermittedException, NoSuchFieldException;

  //
  // Report methods
  //
  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "see sessions count")
  int getSessionsCount(ClientUID clientUid) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "see sessions info")
  SessionParams[] getSessionInfos(ClientUID clientUID) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "lock GsmChannel to Arfcn")
  void lockGsmChannelToArfcn(ClientUID clientUID, IServerData server, ChannelUID gsmChannel, int arfn) throws NotPermittedException, NoSuchFieldException, LockArfcnException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "unLock GsmChannel to Arfcn")
  void unLockGsmChannelToArfcn(ClientUID clientUID, IServerData server, ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, LockArfcnException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "list Sms History")
  List<SmsHistory> listSmsHistory(ClientUID clientUID, Date fromDate, Date toDate, List<String> iccids, String clientIp) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "list Sms Statistic")
  List<SmsStatistic> listSmsStatistic(ClientUID clientUID);

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "list voice server call statistic")
  List<VoiceServerCallStatistic> listVoiceServerCallStatistic(ClientUID clientUID, Date fromDate, Date toDate, String prefix, String server) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "list alaris smpp smses")
  List<AlarisSmppSms> listAlarisSmppSmses(ClientUID clientUID, Date fromDate, Date toDate, String source, String destination, String username, AlarisSms.Status status) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN, UserGroup.USER}, description = "list alaris http smses")
  List<AlarisHttpSms> listAlarisHttpSmses(ClientUID clientUID, Date fromDate, Date toDate, String ani, String dnis, String clientIp, AlarisSms.Status status) throws NotPermittedException;


}
