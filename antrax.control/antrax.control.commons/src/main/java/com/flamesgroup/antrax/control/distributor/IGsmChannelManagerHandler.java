/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.distributor;

import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.NetworkSurveyInfo;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

public interface IGsmChannelManagerHandler extends IChannelManagerHandler {

  void handleAvailableChannel(String serverName, ChannelUID channel) throws RemoteException;

  void handleChannelCellInfos(String serverName, ChannelUID channel, List<CellInfo> cellInfos) throws RemoteException;

  void handleChannelNetworkSurveys(String serverName, ChannelUID channel, List<NetworkSurveyInfo> networkSurveys, Date handleTime) throws RemoteException;

}
