/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.control.authorization.UserGroup;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefix;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.commons.Pair;

import java.util.List;
import java.util.Set;

public interface IPrefixListBean extends PermissionChecker {

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get prefix list")
  List<PhoneNumberPrefix> listPhoneNumberPrefixes(ClientUID clientUid) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "add new element")
  void addPhoneNumberPrefix(ClientUID clientUID, PhoneNumberPrefix newElem) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "change new element")
  void editPhoneNumberPrefix(ClientUID clientUID, PhoneNumberPrefix editElem) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "delete new element")
  void deletePhoneNumberPrefix(ClientUID clientUID, PhoneNumberPrefix deleteElem) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "import elements")
  void importPhoneNumberPrefix(ClientUID clientUID, Set<String> numbers, long id, boolean ignoreOrUpdateOnDuplicate) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "clear prefix")
  int clearPhoneNumberPrefixes(ClientUID clientUID, PhoneNumberPrefix selectedElem) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "update prefix count")
  void updatePhoneNumberPrefixesCount(ClientUID clientUID, PhoneNumberPrefix selectedElem, Boolean ignoreOrUpdateOnDuplicate) throws NotPermittedException, StorageException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "check update prefix count executing")
  Pair<Boolean, Exception> checkPhoneNumberPrefixesCountExecuting(ClientUID clientUID);

  @PermitTo(groups = {UserGroup.ADMIN}, description = "export numbers")
  String exportNumbers(ClientUID clientUID, PhoneNumberPrefix selectedElem) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get Prefix Config")
  PhoneNumberPrefixConfig getPrefixConfig(ClientUID clientUID) throws NotPermittedException;

  @PermitTo(groups = {UserGroup.ADMIN}, description = "get Prefix Config")
  void savePrefixConfig(ClientUID clientUID, PhoneNumberPrefixConfig phoneNumberPrefixConfig) throws NotPermittedException, StorageException;

}
