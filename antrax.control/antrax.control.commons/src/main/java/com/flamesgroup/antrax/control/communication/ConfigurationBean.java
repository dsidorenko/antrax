/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import static com.flamesgroup.antrax.control.authorization.UserGroup.ADMIN;
import static com.flamesgroup.antrax.control.authorization.UserGroup.GUEST;
import static com.flamesgroup.antrax.control.authorization.UserGroup.USER;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.authorization.PermitTo;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.util.List;
import java.util.Map;

public interface ConfigurationBean extends PermissionChecker {

  //
  // Edit transaction
  //

  /**
   * Start configuration edit transaction.
   *
   * @return configuration transaction id or null if transaction already
   * started
   * @throws NotPermittedException
   */
  @PermitTo(groups = {ADMIN}, description = "start edit transaction")
  int startEditTransaction(ClientUID client) throws NotPermittedException, TransactionException;

  /**
   * Commit all changes have been done between calls
   * {@code startEditTransaction()} and {@code commitEditTransaction()}
   *
   * @param transactionId transaction id to commit
   * @throws NotPermittedException
   */
  @PermitTo(groups = {ADMIN}, description = "commit edit transaction")
  void commitEditTransaction(ClientUID client, int transactionId) throws NotPermittedException, TransactionException;

  /**
   * Rollback edit transaction and discard any changes that have been done
   * after {@code startEditTransaction()} call
   *
   * @param transactionId transaction id to commit
   * @throws NotPermittedException
   */
  @PermitTo(groups = {ADMIN}, description = "rollback edit transaction")
  void rollbackEditTransaction(ClientUID client, int transactionId) throws NotPermittedException, TransactionException;

  /**
   * Retain edit transaction (keep alive transaction).
   *
   * @param transactionId transaction id to retain
   * @throws NotPermittedException
   */
  @PermitTo(groups = {ADMIN}, description = "retain edit transaction")
  void retainEditTransaction(ClientUID client, int transactionId) throws NotPermittedException, TransactionException;

  //
  // GSM group CRUD
  //

  @PermitTo(groups = {ADMIN, USER}, description = "read GSM groups")
  GSMGroup[] listGSMGroups(ClientUID client) throws NotPermittedException, StorageException;

  @PermitTo(groups = {ADMIN, USER}, description = "list GSM group names")
  String[] listGsmGroupNames(ClientUID client) throws NotPermittedException, TransactionException, StorageException;

  //
  // GsmGroupEditInfo
  //

  @PermitTo(groups = {ADMIN}, description = "create GsmGroupEditInfo")
  GsmGroupEditInfo createGsmGroupEditInfo(ClientUID client, int transactionId) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN, USER}, description = "read GsmGroupEditInfo")
  GsmGroupEditInfo[] listGsmGroupEditInfo(ClientUID client) throws NotPermittedException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "update GsmGroupEditInfo")
  GsmGroupEditInfo updateGsmGroupEditInfo(ClientUID client, int transactionId, GsmGroupEditInfo gsmGroupInfo) throws NotPermittedException,
      TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "delete GsmGroupEditInfo")
  void deleteGsmGroupEditInfo(ClientUID client, int transactionId, GsmGroupEditInfo gsmGroupInfo) throws NotPermittedException, TransactionException,
      StorageException;

  //
  // GSM - SIM groups link
  //

  @PermitTo(groups = {ADMIN}, description = "list links by GSM group")
  LinkWithSIMGroup[] listLinks(ClientUID client, GSMGroup gsmGroup) throws NotPermittedException, StorageException;

  //
  // SIM group CRUD
  //

  @PermitTo(groups = {ADMIN, USER}, description = "list SIM group names")
  String[] listSimGroupNames(ClientUID client) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "SIM group create")
  SIMGroup createSIMGroup(ClientUID client, int transactionId) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "list SIM groups")
  SIMGroup[] listSIMGroups(ClientUID client) throws NotPermittedException, StorageException, DataSelectionException;

  @PermitTo(groups = {ADMIN, USER}, description = "list simple SIM groups")
  SimpleSimGroup[] listSimpleSIMGroups(ClientUID client) throws NotPermittedException, StorageException, DataSelectionException;

  @PermitTo(groups = {ADMIN, USER}, description = "read SIM groups in transaction")
  SIMGroup[] listSIMGroups(ClientUID client, int transactionId) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "apply SIM group changes")
  SIMGroup updateSIMGroup(ClientUID client, int transactionId, SIMGroup simGroup) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN, USER}, description = "set own IMEI to the sim card")
  void updateIMEI(ClientUID client, ICCID sim, IMEI imei) throws NotPermittedException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "delete SIM group")
  void deleteSIMGroup(ClientUID client, int transactionId, SIMGroup simGroup) throws NotPermittedException, TransactionException, StorageException;

  //
  // SIM card CRUD
  //

  /**
   * Temporary method for update SIM group, before firmware does not support
   * reading ICCID without PIN code
   *
   * @param transactionId
   * @param simGroup
   * @throws NotPermittedException
   * @throws TransactionException
   * @throws NoSuchSimException
   */
  @PermitTo(groups = {ADMIN, USER}, description = "change SIM group")
  void updateSimCardGroup(ClientUID client, int transactionId, ICCID[] simUID, SimpleSimGroup simGroup) throws NotPermittedException, TransactionException,
      NoSuchSimException, StorageException;

  @PermitTo(groups = {ADMIN, USER}, description = "change phone number")
  void setPhoneNumber(ClientUID client, int transactionId, ICCID[] simUIDs, PhoneNumber phoneNumber) throws NotPermittedException, TransactionException,
      StorageException, NoSuchSimException;

  //
  // Servers CRUD
  //

  @PermitTo(groups = {ADMIN}, description = "list servers")
  List<IServerData> listServers(ClientUID client) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "get servers revision")
  long getServersRevision(ClientUID client) throws StorageException;

  @PermitTo(groups = {ADMIN}, description = "create server")
  IServerData createServer(ClientUID clientUID, int transactionId) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "update server")
  IServerData updateServer(ClientUID clientUID, int transactionId, IServerData server) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "delete server")
  void deleteServer(ClientUID clientUID, int transactionId, IServerData server) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "get route config")
  RouteConfig getRouteConfig(ClientUID clientUID) throws NotPermittedException, TransactionException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "update route config")
  void updateRouteConfig(ClientUID clientUID, int transactionId, RouteConfig routeConfig) throws NotPermittedException, TransactionException, StorageException;

  //
  // Scripts
  //

  @PermitTo(groups = {ADMIN}, description = "get script definition by name")
  ScriptDefinition getScriptDefinition(ClientUID client, String name) throws NotPermittedException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "list script definitions")
  ScriptDefinition[] listScriptDefinitions(ClientUID client, ScriptType... scriptType) throws NotPermittedException, StorageException;

  @PermitTo(groups = {ADMIN}, description = "get version of antrax")
  String getVersionOfAntrax(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {ADMIN}, description = "get scripts of antrax")
  ScriptFile getScriptFile(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {ADMIN}, description = "get new scripts conflicts")
  ScriptConflict[] getScriptConflicts(ClientUID client, int transactionId, byte[] inputByteArray) throws Exception;

  @PermitTo(groups = {ADMIN}, description = "commit code base")
  void commitCodeBase(ClientUID client, int transactionId, ScriptFile scriptFile, SimGroupScriptModificationConflict[] resolvedSimGroupConflicts) throws Exception;

  @PermitTo(groups = {ADMIN}, description = "request for plugins store")
  AntraxPluginsStore getPluginsStore(ClientUID client, int transactionId) throws Exception;

  @PermitTo(groups = {ADMIN}, description = "analyze scripts")
  AntraxPluginsStore analyzeJavaFileClasses(ClientUID client, int transactionId, byte[] inputByteArray) throws Exception;

  @PermitTo(groups = {ADMIN}, description = "list scripts revision")
  long getPluginsStoreRevision(ClientUID clientuid, int transactionId) throws NotPermittedException;

  @PermitTo(groups = {ADMIN}, description = "list sim groups revision")
  long getSIMGroupsRevision(ClientUID clientuid, int transactionId) throws NotPermittedException;

  @PermitTo(groups = {ADMIN, USER, GUEST}, description = "see scripts status")
  boolean isScriptsOk(ClientUID client) throws NotPermittedException;

  @PermitTo(groups = {ADMIN}, description = "list gsm groups revision")
  long getGSMGroupsRevision(ClientUID client, int transactionId);

  @PermitTo(groups = {ADMIN}, description = "set update gsm groups revision")
  void incGsmToSimGroupLinksRevision(ClientUID client);

  @PermitTo(groups = {ADMIN}, description = "set update server revision")
  void incServersRevision(ClientUID client);

  @PermitTo(groups = {ADMIN, USER}, description = "import own numbers for sim cards by ICCID(uid)")
  void importSimUIDs(ClientUID client, Map<ICCID, PhoneNumber> simUIDs) throws NotPermittedException, StorageException;

  //
  // VoIP anti spam configuration
  //
  @PermitTo(groups = {ADMIN}, description = "get VoipAntiSpamConfiguration")
  VoipAntiSpamConfiguration getVoipAntiSpamConfiguration(ClientUID clientuid) throws NotPermittedException;

  @PermitTo(groups = {ADMIN}, description = "save VoipAntiSpamConfiguration")
  void saveVoipAntiSpamConfiguration(ClientUID clientuid, int transactionId, VoipAntiSpamConfiguration voipAntiSpamConfiguration) throws NotPermittedException, TransactionException, StorageException;

}
