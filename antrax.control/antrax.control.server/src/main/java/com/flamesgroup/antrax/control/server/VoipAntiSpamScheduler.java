/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.control.server.config.ControlServerConfig;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.voipantispam.CdrConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class VoipAntiSpamScheduler implements IConfigurationHandler {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpamScheduler.class);

  private static final long ONCE_PER_DAY = 1000 * 60 * 60 * 24;
  private static final int HOUR_TIME = 24;
  private static final int MINUTES_TIME = 0;

  private final IVoipAntiSpamDAO voipAntiSpamDAO;
  private final VoipAntiSpam voipAntiSpam;

  private Timer timer;

  public VoipAntiSpamScheduler(final DAOProvider daoProvider, final ControlServerConfig controlServerConfig) {
    Objects.requireNonNull(daoProvider, "daoProvider mustn't be null");
    Objects.requireNonNull(controlServerConfig, "controlServerConfig mustn't be null");
    this.voipAntiSpamDAO = daoProvider.getVoipAntiSpamDAO();
    voipAntiSpam = new VoipAntiSpam(daoProvider);
  }

  public void start() {
    timer = new Timer();
    timer.scheduleAtFixedRate(new RemindHistoryTask(), getHistoryStartTime(), ONCE_PER_DAY);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isCdrAnalyze()) {
      return;
    }

    for (CdrConfig cdrConfig : voipAntiSpamConfiguration.getCdrConfigs()) {
      long periodInMillis = TimeUnit.MINUTES.toMillis(cdrConfig.getPeriod());
      timer.scheduleAtFixedRate(new AnalyzeCdrTask(cdrConfig), getAnalyzeCDRStartTime(periodInMillis), periodInMillis);
    }
  }

  public void stop() {
    timer.cancel();
    timer.purge();
  }

  @Override
  public void reconfigure(final IConfigViewDAO configViewDAO) {
    logger.info("[{}] - reconfigure", this);
    stop();
    start();
  }

  private class RemindHistoryTask extends TimerTask {

    @Override
    public void run() {
      VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
      if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
        return;
      }
      logger.info("[{}] - try to execute RemindHistoryTask", this);
      try {
        voipAntiSpamDAO.clearHistoryBlockNumbers();
        voipAntiSpamDAO.clearHistoryRoutingRequest();
      } catch (DataModificationException e) {
        logger.warn("[{}] - can't execute RemindHistoryTask", this, e);
      }
      logger.info("[{}] - executed RemindHistoryTask", this);
    }

  }

  class AnalyzeCdrTask extends TimerTask {

    private final CdrConfig cdrConfig;

    public AnalyzeCdrTask(final CdrConfig cdrConfig) {
      this.cdrConfig = cdrConfig;
    }

    @Override
    public void run() {
      logger.info("[{}] - try to execute AnalyzeCdrTask", this);
      voipAntiSpam.analyzeCdr(cdrConfig);
      logger.info("[{}] - executed AnalyzeCdrTask", this);
      return;
    }

  }

  private Date getHistoryStartTime() {
    Calendar tomorrow = new GregorianCalendar();
    tomorrow.add(Calendar.DATE, 0);
    Calendar result = new GregorianCalendar(
        tomorrow.get(Calendar.YEAR),
        tomorrow.get(Calendar.MONTH),
        tomorrow.get(Calendar.DATE),
        HOUR_TIME,
        MINUTES_TIME
    );
    return result.getTime();
  }

  private Date getAnalyzeCDRStartTime(final long analyzePeriod) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(calendar.getTimeInMillis() + analyzePeriod);
    return calendar.getTime();
  }

}
