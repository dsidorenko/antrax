/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.smpp;

import static com.flamesgroup.antrax.control.server.alaris.sms.smpp.AlarisSmppSmsStatusMapper.mapToMessageState;

import com.cloudhopper.commons.util.ByteUtil;
import com.cloudhopper.commons.util.HexUtil;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppServerConfiguration;
import com.cloudhopper.smpp.SmppServerHandler;
import com.cloudhopper.smpp.SmppServerSession;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.impl.DefaultSmppServer;
import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.BaseBind;
import com.cloudhopper.smpp.pdu.BaseBindResp;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.pdu.QuerySm;
import com.cloudhopper.smpp.pdu.QuerySmResp;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.pdu.Unbind;
import com.cloudhopper.smpp.ssl.SslConfiguration;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.tlv.TlvConvertException;
import com.cloudhopper.smpp.type.SmppProcessingException;
import com.cloudhopper.smpp.util.SmppUtil;
import com.flamesgroup.antrax.control.properties.AlarisMultiUser;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlarisSmsSmppServer {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsSmppServer.class);

  private static final String NULL_CHARACTER_TEMPLATE = "[\\0000]+";
  private static final Pattern nullCharacterPattern = Pattern.compile(NULL_CHARACTER_TEMPLATE);

  private DefaultSmppServer server;

  private final Map<String, SmppSession> smppSessions = new ConcurrentHashMap<>();

  private AlarisSmsSmppService alarisSmsSmppService;

  public void start() throws Exception {
    if (!ControlPropUtils.getInstance().getAlarisSmsProperties().isAlarisSmsSmppServerEnable()) {
      logger.info("[{}] - AlarisSmsSmppServer is disabled", this);
      return;
    }

    logger.info("[{}] - try to start AlarisSmsSmppServer", this);
    SmppServerConfiguration configuration = new SmppServerConfiguration();
    configuration.setNonBlockingSocketsEnabled(true);
    configuration.setHost(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerHostname());
    configuration.setPort(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerPort());
    configuration.setMaxConnectionSize(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerMaxConnection());
    configuration.setDefaultWindowSize(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerSendWindowSize());
    configuration.setDefaultRequestExpiryTimeout(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerRequestExpiryTimeout());
    configuration.setDefaultWindowMonitorInterval(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerWindowMonitorInterval());
    configuration.setDefaultWindowWaitTimeout(configuration.getDefaultRequestExpiryTimeout());

    if (ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerUseSsl()) {
      SslConfiguration sslConfiguration = new SslConfiguration();
      URL smppKeystoreUrl = AlarisSmsSmppService.class.getResource("/smppkeystore");
      String smppKeystorePath = Paths.get(smppKeystoreUrl.toURI()).toString();
      sslConfiguration.setKeyStorePath(smppKeystorePath);
      sslConfiguration.setKeyStorePassword("antraxsmppserver");
      sslConfiguration.setKeyManagerPassword("antraxsmppserver");
      configuration.setUseSsl(true);
      configuration.setSslConfiguration(sslConfiguration);
    }

    ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

    ScheduledThreadPoolExecutor monitorExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1, new ThreadFactory() {
      private final AtomicInteger sequence = new AtomicInteger(0);

      @Override
      public Thread newThread(final Runnable r) {
        Thread t = new Thread(r);
        t.setName("SmppServerSessionWindowMonitorPool-" + sequence.getAndIncrement());
        return t;
      }
    });

    server = new DefaultSmppServer(configuration, new AlarisSmsSmppServerHandler(), executor, monitorExecutor);
    server.start();

    logger.info("[{}] - started AlarisSmsSmppServer", this);
  }

  public void stop() throws Exception {
    if (server == null || server.isStopped()) {
      if (ControlPropUtils.getInstance().getAlarisSmsProperties().isAlarisSmsSmppServerEnable()) {
        throw new IllegalStateException("AlarisSmsSmppServer isn't started");
      } else {
        return;
      }
    }

    logger.info("[{}] - try to stop AlarisSmsSmppServer", this);
    server.stop();
    server = null;
    logger.info("[{}] - stopped AlarisSmsSmppServer", this);
  }

  public SmppSession getSession(final String systemId) {
    return smppSessions.get(systemId);
  }

  public void setAlarisSmsSmppService(final AlarisSmsSmppService alarisSmsSmppService) {
    this.alarisSmsSmppService = alarisSmsSmppService;
  }

  private class AlarisSmsSmppServerHandler implements SmppServerHandler {

    @Override
    public void sessionBindRequested(final Long sessionId, final SmppSessionConfiguration sessionConfiguration, final BaseBind bindRequest) throws SmppProcessingException {
      logger.debug("[{}] - session [{}] bind request for username [{}]", this, sessionId, bindRequest.getSystemId());
      AlarisMultiUser alarisMultiUser = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerMultiUser().get(bindRequest.getSystemId());
      if (alarisMultiUser == null) {
        throw new SmppProcessingException(SmppConstants.STATUS_INVSYSID, "Unknown username");
      }
      if (!bindRequest.getPassword().equals(alarisMultiUser.getPassword())) {
        throw new SmppProcessingException(SmppConstants.STATUS_INVPASWD, "Incorrect password");
      }

      sessionConfiguration.setName("session.smpp." + sessionConfiguration.getSystemId());
    }

    @Override
    public void sessionCreated(final Long sessionId, final SmppServerSession session, final BaseBindResp preparedBindResponse) throws SmppProcessingException {
      logger.debug("[{}] - session [{}] created", this, sessionId);
      session.serverReady(new AlarisSmsSmppSessionHandler(session));
      smppSessions.put(session.getConfiguration().getSystemId(), session);
    }

    @Override
    public void sessionDestroyed(final Long sessionId, final SmppServerSession session) {
      logger.debug("[{}] - session [{}] destroyed", this, sessionId);
      session.destroy(); // make sure it's really shutdown
      smppSessions.remove(session.getConfiguration().getSystemId());
    }

  }

  private class AlarisSmsSmppSessionHandler extends DefaultSmppSessionHandler {

    private final WeakReference<SmppSession> session;

    public AlarisSmsSmppSessionHandler(final SmppSession session) {
      this.session = new WeakReference<>(session);
    }

    @Override
    public void fireUnknownThrowable(final Throwable t) {
      super.fireUnknownThrowable(t);
      SmppSession sessionLocal = session.get();
      if (sessionLocal == null) {
        logger.error("[{}] - fireUnknownThrowable got empty session", this);
        return;
      }
      
      sessionLocal.unbind(1000);
      sessionLocal.destroy();
    }

    @Override
    public PduResponse firePduRequestReceived(final PduRequest pduRequest) {
      SmppSession sessionLocal = session.get();
      if (sessionLocal == null) {
        return pduRequest.createResponse();
      }

      if (pduRequest instanceof SubmitSm) {
        SubmitSm submitSm = (SubmitSm) pduRequest;
        UUID smsId = UUID.randomUUID();
        String username = sessionLocal.getConfiguration().getSystemId();
        int sourceTon = submitSm.getSourceAddress().getTon();
        int sourceNpi = submitSm.getSourceAddress().getNpi();
        String sourceAddress = submitSm.getSourceAddress().getAddress();
        int destTon = submitSm.getDestAddress().getTon();
        int destNpi = submitSm.getDestAddress().getNpi();
        String destAddress = submitSm.getDestAddress().getAddress();
        byte[] shortMessage = submitSm.getShortMessage();
        String serviceType = submitSm.getServiceType();
        int dataCoding = submitSm.getDataCoding();
        int registeredDelivery = submitSm.getRegisteredDelivery();

        AlarisHttpSms.Sar sar = null;
        if (shortMessage.length > 0 && SmppUtil.isUserDataHeaderIndicatorEnabled(submitSm.getEsmClass())) {
          int userDataHeaderLength = ByteUtil.decodeUnsigned(shortMessage[0]) + 1;
          if (userDataHeaderLength > shortMessage.length) {
            logger.warn("[{}] - user data header length [{}] exceeds short message length [{}]", this, userDataHeaderLength, shortMessage.length);
            SubmitSmResp submitSmResp = submitSm.createResponse();
            submitSmResp.setCommandStatus(SmppConstants.STATUS_INVOPTPARSTREAM);
            return submitSmResp;
          }

          if (userDataHeaderLength != shortMessage.length) { // is the user data header the only part of the payload (optimization)
            if (shortMessage[1] == 0x00) {
              byte sarRefNumber = shortMessage[3];
              int sarTotalSegments = shortMessage[4] & 0xFF;
              int sarSegmentSeqnum = shortMessage[5] & 0xFF;
              sar = new AlarisHttpSms.Sar(HexUtil.toHexString(sarRefNumber), sarTotalSegments, sarSegmentSeqnum);
            } else if (shortMessage[1] == 0x08) {
              byte[] sarRefNumber = new byte[] {shortMessage[3], shortMessage[4]};
              int sarTotalSegments = shortMessage[5] & 0xFF;
              int sarSegmentSeqnum = shortMessage[6] & 0xFF;
              sar = new AlarisHttpSms.Sar(HexUtil.toHexString(sarRefNumber), sarTotalSegments, sarSegmentSeqnum);
            }

            int shortMessageLocalLength = shortMessage.length - userDataHeaderLength;
            byte[] shortMessageLocal = new byte[shortMessageLocalLength];
            System.arraycopy(shortMessage, userDataHeaderLength, shortMessageLocal, 0, shortMessageLocalLength);
            shortMessage = shortMessageLocal;
          }
        }

        Tlv sarRefNumber = submitSm.getOptionalParameter(SmppConstants.TAG_SAR_MSG_REF_NUM);
        if (sar == null && sarRefNumber != null) {
          Tlv sarTotalSegments = submitSm.getOptionalParameter(SmppConstants.TAG_SAR_TOTAL_SEGMENTS);
          Tlv sarSegmentSeqnum = submitSm.getOptionalParameter(SmppConstants.TAG_SAR_SEGMENT_SEQNUM);
          if (sarTotalSegments != null && sarSegmentSeqnum != null) {
            try {
              sar = new AlarisHttpSms.Sar(sarRefNumber.getValueAsString(), sarTotalSegments.getValueAsInt(), sarSegmentSeqnum.getValueAsInt());
            } catch (TlvConvertException e) {
              logger.warn("[{}] - incorrect sar parameters", this, e);
              SubmitSmResp submitSmResp = submitSm.createResponse();
              submitSmResp.setCommandStatus(SmppConstants.STATUS_INVOPTPARAMVAL);
              return submitSmResp;
            }
          } else {
            logger.warn("[{}] - missing sar parameters", this);
            SubmitSmResp submitSmResp = submitSm.createResponse();
            submitSmResp.setCommandStatus(SmppConstants.STATUS_MISSINGOPTPARAM);
            return submitSmResp;
          }
        }

        String message = PduUtil.decodeShortMessage(dataCoding, shortMessage);
        Matcher nullCharacterMatcher = nullCharacterPattern.matcher(message);
        if (nullCharacterMatcher.find()) {
          logger.warn("[{}] - find NULL(0x0) character at message [{}], delete it from message", this, message);
          message = nullCharacterMatcher.replaceAll("");
        }

        AlarisSmppSms alarisSms = new AlarisSmppSms(smsId, sourceTon, sourceNpi, sourceAddress, destTon, destNpi, destAddress,
            message, serviceType, dataCoding, new Date(), sar, username, registeredDelivery);

        try {
          alarisSmsSmppService.sendAlarisSms(alarisSms);
        } catch (AlarisSmsSmppException e) {
          SubmitSmResp submitSmResp = submitSm.createResponse();
          submitSmResp.setCommandStatus(SmppConstants.STATUS_SUBMITFAIL);
          return submitSmResp;
        }

        SubmitSmResp submitSmResp = submitSm.createResponse();
        submitSmResp.setMessageId(smsId.toString());

        return submitSmResp;
      } else if (pduRequest instanceof QuerySm) {
        QuerySm querySm = (QuerySm) pduRequest;
        String messageId = querySm.getMessageId();
        UUID smsId;
        try {
          smsId = UUID.fromString(messageId);
        } catch (IllegalArgumentException ignore) {
          QuerySmResp querySmResp = querySm.createResponse();
          querySmResp.setCommandStatus(SmppConstants.STATUS_INVMSGID);
          return querySmResp;
        }

        AlarisSms.Status status = alarisSmsSmppService.getAlarisSmsStatus(smsId);
        QuerySmResp querySmResponse = querySm.createResponse();
        querySmResponse.setMessageId(messageId);
        querySmResponse.setMessageState(mapToMessageState(status));

        return querySmResponse;
      } else if (pduRequest instanceof Unbind) {
        sessionLocal.unbind(1000);
        return pduRequest.createResponse();
      } else {
        return pduRequest.createResponse();
      }
    }

  }

}
