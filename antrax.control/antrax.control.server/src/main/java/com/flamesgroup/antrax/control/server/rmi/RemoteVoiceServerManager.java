/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rmi;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.voiceserver.IVoiceServerAudioManager;
import com.flamesgroup.antrax.voiceserver.IVoiceServerManager;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RemoteVoiceServerManager implements IVoiceServerManager, IVoiceServerAudioManager {

  private final IVoiceServerManager voiceServerManager;
  private final IVoiceServerAudioManager voiceServerAudioManager;

  public RemoteVoiceServerManager(final IVoiceServerManager voiceServerManager, final IVoiceServerAudioManager voiceServerAudioManager) {
    this.voiceServerManager = voiceServerManager;
    this.voiceServerAudioManager = voiceServerAudioManager;
  }

  @Override
  public void lockSIM(final ICCID simUID) {
    voiceServerManager.lockSIM(simUID);
  }

  @Override
  public void fireEvent(final ICCID simUID, final String event) throws EventException {
    voiceServerManager.fireEvent(simUID, event);
  }

  @Override
  public void sendUSSD(final ICCID simUID, final String ussd) throws USSDException {
    voiceServerManager.sendUSSD(simUID, ussd);
  }

  @Override
  public String startUSSDSession(final ICCID simUID, final String ussd) throws USSDException {
    return voiceServerManager.startUSSDSession(simUID, ussd);
  }

  @Override
  public String sendUSSDSessionCommand(final ICCID simUID, final String command) throws USSDException {
    return voiceServerManager.sendUSSDSessionCommand(simUID, command);
  }

  @Override
  public void endUSSDSession(final ICCID simUID) throws USSDException {
    voiceServerManager.endUSSDSession(simUID);
  }

  @Override
  public void sendSMS(final ICCID simUID, final String number, final String smsText) throws SMSException {
    voiceServerManager.sendSMS(simUID, number, smsText);
  }

  @Override
  public VsSmsStatus sendSMS(final List<AlarisSms> alarisSmses, final UUID objectUuid) {
    return voiceServerManager.sendSMS(alarisSmses, objectUuid);
  }

  @Override
  public VsSmsStatus getSMSStatus(final UUID objectUuid) {
    return voiceServerManager.getSMSStatus(objectUuid);
  }

  @Override
  public void removeSMSStatus(final UUID objectUuid) {
    voiceServerManager.removeSMSStatus(objectUuid);
  }

  @Override
  public void sendDTMF(final ICCID simUID, final String dtmf) throws DTMFException {
    voiceServerManager.sendDTMF(simUID, dtmf);
  }

  @Override
  public void unlockSIM(final ICCID simUID) {
    voiceServerManager.unlockSIM(simUID);
  }

  @Override
  public void disableCallChannel(final ICCID simUID) {
    voiceServerManager.disableCallChannel(simUID);
  }

  @Override
  public void enableCallChannel(final ICCID simUID) {
    voiceServerManager.enableCallChannel(simUID);
  }

  @Override
  public void updateAudioCaptureEnable(final boolean enable) {
    voiceServerManager.updateAudioCaptureEnable(enable);
  }

  @Override
  public void shutdown() {
    voiceServerManager.shutdown();
  }

  @Override
  public void lockGsmChannel(final ChannelUID channel, final boolean lock, final String lockReason) throws LockException {
    voiceServerManager.lockGsmChannel(channel, lock, lockReason);
  }

  @Override
  public void lockGsmChannelToArfcn(final ChannelUID channel, final int arfcn) throws LockArfcnException {
    voiceServerManager.lockGsmChannelToArfcn(channel, arfcn);
  }

  @Override
  public void unLockGsmChannelToArfcn(final ChannelUID channel) throws LockArfcnException {
    voiceServerManager.unLockGsmChannelToArfcn(channel);
  }

  @Override
  public void executeNetworkSurvey(final ChannelUID channel) throws NetworkSurveyException {
    voiceServerManager.executeNetworkSurvey(channel);
  }

  @Override
  public void resetStatistic() {
    voiceServerManager.resetStatistic();
  }

  @Override
  public List<AudioFile> getAudioFiles() {
    return voiceServerAudioManager.getAudioFiles();
  }

  @Override
  public byte[] downloadAudioFileFromServer(final String fileName) {
    return voiceServerAudioManager.downloadAudioFileFromServer(fileName);
  }

  @Override
  public List<String> getIvrTemplatesSimGroups() {
    return voiceServerAudioManager.getIvrTemplatesSimGroups();
  }

  @Override
  public List<IvrTemplateWrapper> getIvrTemplates(final String simGroup) {
    return voiceServerAudioManager.getIvrTemplates(simGroup);
  }

  @Override
  public void createIvrTemplateSimGroup(final String simGroup) throws IvrTemplateException {
    voiceServerAudioManager.createIvrTemplateSimGroup(simGroup);
  }

  @Override
  public void removeIvrTemplateSimGroups(final List<String> simGroups) throws IvrTemplateException {
    voiceServerAudioManager.removeIvrTemplateSimGroups(simGroups);
  }

  @Override
  public Map<String, Integer> getCallDropReasons() {
    return voiceServerAudioManager.getCallDropReasons();
  }

  @Override
  public void uploadIvrTemplate(final String simGroup, final byte[] bytes, final String fileName) throws IvrTemplateException {
    voiceServerAudioManager.uploadIvrTemplate(simGroup, bytes, fileName);
  }

  @Override
  public void removeIvrTemplate(final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException {
    voiceServerAudioManager.removeIvrTemplate(simGroup, ivrTemplates);
  }
}
