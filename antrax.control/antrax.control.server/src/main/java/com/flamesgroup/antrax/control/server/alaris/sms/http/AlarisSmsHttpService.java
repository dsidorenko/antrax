/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;

import com.flamesgroup.antrax.control.communication.SmsStatistic;
import com.flamesgroup.antrax.control.properties.AlarisMultiUser;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.alaris.sms.AlarisSendSmsService;
import com.flamesgroup.antrax.control.server.alaris.sms.FakeSmsStatusStatistic;
import com.flamesgroup.antrax.control.server.alaris.sms.IAlarisSendSmsHandler;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.dao.IAlarisHttpSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.properties.IReloadListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AlarisSmsHttpService implements IReloadListener {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsHttpService.class);

  private static final EnumSet<AlarisSms.Status> DLR_STATUS = EnumSet.of(
      AlarisSms.Status.DELIVRD, AlarisSms.Status.EXPIRED, AlarisSms.Status.UNDELIV, AlarisSms.Status.DELETED, AlarisSms.Status.UNKNOWN, AlarisSms.Status.REJECTD
  );

  private final IAlarisHttpSmsDAO alarisSmsDAO;
  private final AlarisSendSmsService alarisSendSmsService;
  private final AlarisDlrHttpService alarisDlrHttpService;

  private final AlarisSendSmsHandler alarisSendSmsHandler = new AlarisSendSmsHandler();

  private final AtomicInteger countOfSendingSms = new AtomicInteger();
  private final AtomicInteger countOfSendSmsToday = new AtomicInteger();
  private final ConcurrentMap<String, Integer> countOfSendingSmsTodayByIp = new ConcurrentHashMap<>();
  private final ConcurrentMap<String, Integer> countOfSendSmsTodayByIp = new ConcurrentHashMap<>();

  private final ThreadPoolExecutor threadPoolExecutor = new WaitThreadPoolExecutor(5, 20, 30, TimeUnit.SECONDS, new SynchronousQueue<>(), r -> new Thread(r, "HttpDlrExecutor"));
  private final Map<String, FakeSmsStatusStatistic> fakeSmsStatusStatistics = new ConcurrentHashMap<>();

  private Timer timer;

  public AlarisSmsHttpService(final IAlarisHttpSmsDAO alarisSmsDAO, final AlarisSendSmsService alarisSendSmsService, final AlarisDlrHttpService alarisDlrHttpService) {
    this.alarisSmsDAO = alarisSmsDAO;
    this.alarisSendSmsService = alarisSendSmsService;
    this.alarisDlrHttpService = alarisDlrHttpService;
  }

  public void start() {
    alarisSendSmsService.start();

    int resentAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpResentAmount();
    int resentErrorAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpResentErrorAmount();
    alarisSmsDAO.listSmsAtRouting().forEach(sms -> alarisSendSmsService.sendAlarisSms(sms, alarisSendSmsHandler, resentAmount, resentErrorAmount));
    alarisSmsDAO.listSmsDlr(DLR_STATUS).forEach(sms -> threadPoolExecutor.execute(() -> sendDlr(sms)));

    countOfSendSmsToday.set(alarisSmsDAO.getCountOfSendSmsToday(null));
    for (String clientIp : ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDayByIp().keySet()) {
      int countOfSendSmsToday = alarisSmsDAO.getCountOfSendSmsToday(clientIp);
      countOfSendingSmsTodayByIp.put(clientIp, countOfSendSmsToday);
      countOfSendSmsTodayByIp.put(clientIp, countOfSendSmsToday);
    }

    timer = new Timer();
    timer.scheduleAtFixedRate(new ResendDlrTask(), TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(5));
    timer.scheduleAtFixedRate(new ResetCountOfSendSmsToday(), Date.from(LocalDateTime.now().withHour(23).withMinute(59).atZone(ZoneId.systemDefault()).toInstant()), TimeUnit.DAYS.toMillis(1));
  }

  public void stop() {
    threadPoolExecutor.shutdown();
    timer.cancel();
    timer.purge();

    alarisSendSmsService.stop();
  }

  public void sendAlarisSms(final AlarisHttpSms alarisSms) {
    if (isDeniedSendAlarisSms(alarisSms.getClientIp())) {
      throw new AlarisSmsHttpException("NO ROUTES");
    }

    alarisSms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.ENROUTE);
    try {
      alarisSmsDAO.insertSms(alarisSms);
    } catch (DataModificationException e) {
      throw new AlarisSmsHttpException("NO ROUTES", e);
    }

    int resentAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpResentAmount();
    int resentErrorAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpResentErrorAmount();
    alarisSendSmsService.sendAlarisSms(alarisSms, alarisSendSmsHandler, resentAmount, resentErrorAmount);
  }

  public AlarisHttpSms.Status getAlarisSmsStatus(final UUID smsId) {
    AlarisHttpSms alarisSms = alarisSmsDAO.getById(smsId);
    if (alarisSms == null) {
      return null;
    }

    if (alarisSms.getStatus() != AlarisHttpSms.Status.SENT) {
      if (AlarisHttpSms.Status.UNDELIV.equals(alarisSms.getStatus()) && ControlPropUtils.getInstance().getAlarisSmsProperties().isSmsHttpAlternativeResponse()) {
        throw new AlarisSmsHttpException("Bad Request");
      }
      return alarisSms.getStatus();
    }

    return alarisSms.getStatus();
  }

  public void sendAlarisSmsDlr(final AlarisHttpSms alarisSms) {
    if (alarisSms.getDlrTime() != null) {
      logger.debug("[{}] - receive real status, but already sent fake status for alarisSms [{}]", this, alarisSms);
      return;
    }

    try {
      alarisSmsDAO.updateSms(alarisSms);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't update status of alaris sms: [{}]", this, alarisSms, e);
    }

    if (alarisSms.getDlrUrl() != null && DLR_STATUS.contains(alarisSms.getStatus())) {
      threadPoolExecutor.execute(() -> sendDlr(alarisSms));
    }
  }

  public List<SmsStatistic> getSmsStatistic() {
    List<SmsStatistic> smsStatistics = new ArrayList<>();
    Map<String, Integer> alarisSmsLimitPearDayByIp = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDayByIp();
    for (Map.Entry<String, Integer> ipAddressLimit : alarisSmsLimitPearDayByIp.entrySet()) {
      smsStatistics.add(new SmsStatistic(ipAddressLimit.getKey(), countOfSendSmsTodayByIp.get(ipAddressLimit.getKey()), ipAddressLimit.getValue()));
    }
    return smsStatistics;
  }

  @Override
  public String getName() {
    return AlarisSmsHttpRestService.class.getSimpleName();
  }

  @Override
  public boolean reload() {
    for (String s : ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDayByIp().keySet()) {
      countOfSendingSmsTodayByIp.putIfAbsent(s, 0);
      countOfSendSmsTodayByIp.putIfAbsent(s, 0);
    }

    logger.debug("[{}] - reloaded configuration, clean fakeSmsStatusStatistic", this);
    return true;
  }

  private boolean isDeniedSendAlarisSms(final String clientIp) {
    if (countOfSendingSms.get() >= ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpSenderBuffer()) {
      logger.info("[{}] - NO ROUTES, overflow senderBuffer, current countOfSendingSms: [{}], smsSenderBuffer: [{}] ", this, countOfSendingSms.get(),
          ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpSenderBuffer());
      return true;
    }
    if (countOfSendSmsToday.get() >= ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDay()) {
      logger.info("[{}] - NO ROUTES, sender sms limits for today, current countOfSendSmsToday: [{}], limitPearDay: [{}] ", this, countOfSendSmsToday.get(),
          ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDay());
      return true;
    }

    Integer countOfSendSmsForClient = countOfSendingSmsTodayByIp.get(clientIp);
    Integer limitForClient = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDayByIp().get(clientIp);
    if (ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpAllowLimitPearDayByIp()) {
      if (countOfSendSmsForClient == null || limitForClient == null) {
        logger.info("[{}] - NO ROUTES, client IP: [{}], denied send sms from this client IP. For allow send sms from this client, configure it at properties", this, clientIp);
        return true;
      }
      if (countOfSendSmsForClient >= limitForClient) {
        logger.info("[{}] - NO ROUTES, sender sms limits for today, current countOfSendSmsToday: [{}], limitPearDay: [{}] for clientIP: [{}]", this, countOfSendSmsForClient, limitForClient, clientIp);
        return true;
      }
    }
    return false;
  }

  private void sendDlr(final AlarisHttpSms alarisSms) {
    if (alarisDlrHttpService.sendDlr(alarisSms)) {
      alarisSms.setDlrTime(new Date());
      try {
        alarisSmsDAO.updateSms(alarisSms);
      } catch (DataModificationException e) {
        logger.warn("[{}] - can't update dlr time of alaris sms: [{}]", this, alarisSms, e);
      }
    }
  }

  private class AlarisSendSmsHandler implements IAlarisSendSmsHandler {

    @Override
    public void handleReject(final List<AlarisSms> alarisSmses) {
      alarisSmses.forEach(sms -> sendAlarisSmsDlr((AlarisHttpSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.REJECTD)));
    }

    @Override
    public void handleSending(final List<AlarisSms> alarisSmses) {
      countOfSendingSms.addAndGet(alarisSmses.size());
      countOfSendSmsToday.addAndGet(alarisSmses.size());
      countOfSendingSmsTodayByIp.compute(((AlarisHttpSms) alarisSmses.get(0)).getClientIp(), (s, i) -> i == null ? alarisSmses.size() : i + alarisSmses.size());
    }

    @Override
    public void handleSent(final List<AlarisSms> alarisSmses, final UUID smsId) {
      countOfSendSmsTodayByIp.compute(((AlarisHttpSms) alarisSmses.get(0)).getClientIp(), (s, i) -> i == null ? 0 : i + alarisSmses.size());
      alarisSmses.forEach(sms -> {
        sendAlarisSmsDlr((AlarisHttpSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.SENT).setSmsId(smsId));

        FakeSmsStatusStatistic fakeSmsStatusStatistic = fakeSmsStatusStatistics.computeIfAbsent(sms.getUsername(), s -> new FakeSmsStatusStatistic());
        fakeSmsStatusStatistic.incTotalSmsStatus();

        AlarisMultiUser alarisMultiUser = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerMultiUser().get(sms.getUsername());
        int res = (fakeSmsStatusStatistic.getTotalSmsStatus() * alarisMultiUser.getPercentageRealStatus()) / 100;
        if (res < fakeSmsStatusStatistic.getTotalRealSmsStatus()) {
          logger.debug("[{}] - return fake status [DELIVRD] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, sms.getUsername(), sms.getId(), fakeSmsStatusStatistic);
          sendAlarisSmsDlr((AlarisHttpSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.DELIVRD));
        } else {
          fakeSmsStatusStatistic.incTotalRealSmsStatus();
          logger.debug("[{}] - waiting real status [{}] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, sms.getUsername(), sms.getId(), fakeSmsStatusStatistic);
        }
      });
    }

    @Override
    public void handleUndeliv(final List<AlarisSms> alarisSmses) {
      countOfSendSmsToday.getAndAccumulate(alarisSmses.size(), (left, right) -> left - right);
      countOfSendingSmsTodayByIp.compute(((AlarisHttpSms) alarisSmses.get(0)).getClientIp(), (s, i) -> i == null ? 0 : i - alarisSmses.size());
      alarisSmses.forEach(sms -> sendAlarisSmsDlr((AlarisHttpSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.UNDELIV)));
    }

    @Override
    public void handleFinalize(final List<AlarisSms> alarisSmses) {
      countOfSendingSms.updateAndGet(prev -> prev - alarisSmses.size());
    }

  }

  private class ResendDlrTask extends TimerTask {

    @Override
    public void run() {
      alarisSmsDAO.listSmsDlr(DLR_STATUS).forEach(sms -> threadPoolExecutor.execute(() -> sendDlr(sms)));
    }
  }

  private class ResetCountOfSendSmsToday extends TimerTask {

    @Override
    public void run() {
      countOfSendSmsToday.set(0);
      countOfSendingSmsTodayByIp.clear();
      countOfSendSmsTodayByIp.clear();
      for (String s : ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpLimitPearDayByIp().keySet()) {
        countOfSendingSmsTodayByIp.put(s, 0);
        countOfSendSmsTodayByIp.put(s, 0);
      }
    }

  }

}
