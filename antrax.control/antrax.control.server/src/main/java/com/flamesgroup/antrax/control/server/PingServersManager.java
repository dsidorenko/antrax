/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.configuration.IPingServerManager;
import com.flamesgroup.antrax.control.commons.ExpireMap;
import com.flamesgroup.antrax.control.communication.ActivityRemoteLogger;
import com.flamesgroup.antrax.control.communication.DefaultSimServerShortInfo;
import com.flamesgroup.antrax.control.communication.DefaultVoiceServerShortInfo;
import com.flamesgroup.antrax.control.communication.ISimServerStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SimServerShortInfo;
import com.flamesgroup.antrax.control.communication.SimServerStatus;
import com.flamesgroup.antrax.control.communication.VoiceServerStatus;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.routing.YateCallRouteEngine;
import com.flamesgroup.antrax.control.simserver.SimUnitManager;
import com.flamesgroup.antrax.control.voiceserver.ActivityLogger;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PingServersManager implements IPingServerManager, IConfigurationHandler {

  private static final Logger logger = LoggerFactory.getLogger(PingServersManager.class);

  private final SimUnitManager simUnitManager;

  private final ExpireMap<Server, ISimServerStatus> simServersStatus;
  private final ExpireMap<Server, IVoiceServerStatus> voiceServersStatus;

  private final long waitingServerPingTimeout;

  private final YateCallRouteEngine yateCallRouteEngine;
  private final IGsmViewManager gsmViewManager;

  private final ActivityRemoteLogger activityRemoteLogger;

  public PingServersManager(final SimUnitManager simUnitManager, final YateCallRouteEngine yateCallRouteEngine, final IGsmViewManager gsmViewManager,
      final ActivityRemoteLogger activityRemoteLogger) {
    this.simUnitManager = simUnitManager;
    this.yateCallRouteEngine = yateCallRouteEngine;
    this.gsmViewManager = gsmViewManager;
    this.activityRemoteLogger = activityRemoteLogger;

    simServersStatus = new ExpireMap<>(createServersStatusExpireHandler());
    voiceServersStatus = new ExpireMap<>(createVoiceServersStatusExpireHandler());

    waitingServerPingTimeout = ControlPropUtils.getInstance().getControlServerProperties().getWaitingServerPingTimeout();
  }

  @Override
  public void setExpireTimeout(final Server server, final long timeout) throws RemoteException {
    switch (server.getType()) {
      case SIM_SERVER:
        simServersStatus.putExpireTimeout(server, timeout + waitingServerPingTimeout);
        break;
      case VOICE_SERVER:
        voiceServersStatus.putExpireTimeout(server, timeout + waitingServerPingTimeout);
        break;
    }
  }

  @Override
  public void ping(final Server server, final ServerStatus serverStatus, final int rmiRegistryPort) throws RemoteException {
    if (server.getType() == ServerType.SIM_SERVER && simServersStatus.containsKey(server)) {
      SimServerShortInfo simServerShortInfo = new SimServerShortInfo();
      simServerShortInfo.setStatus(serverStatus);
      ISimServerStatus simServerStatus = new SimServerStatus(simServerShortInfo);

      simServersStatus.updateExpireKey(server);
      simServersStatus.put(server, simServerStatus);
    } else if (server.getType() == ServerType.VOICE_SERVER && voiceServersStatus.containsKey(server)) {
      IVoiceServerStatus voiceServerStatus = new VoiceServerStatus(activityRemoteLogger.getShortServerInfo(server), activityRemoteLogger.getChannelsInfo(server), rmiRegistryPort);

      voiceServersStatus.updateExpireKey(server);
      voiceServersStatus.put(server, voiceServerStatus);

      yateCallRouteEngine.ping(server, voiceServerStatus);
      gsmViewManager.ping(server, voiceServerStatus);
    } else {
      logger.debug("[{}] - receive ping from server [{}] without setting expire timeout", this, server);
    }
  }

  @Override
  public void reconfigure(final IConfigViewDAO configViewDAO) {
    List<Server> servers = new ArrayList<>();
    for (IServerData serverData : configViewDAO.listServers()) {
      if (serverData.isSimServerEnabled()) {
        Server server = new Server(serverData.getName(), ServerType.SIM_SERVER);
        if (!simServersStatus.containsKey(server)) {
          simServersStatus.put(server, createDefaultSimServerStatus());
        }
        servers.add(server);
      }
      if (serverData.isVoiceServerEnabled()) {
        Server server = new Server(serverData.getName(), ServerType.VOICE_SERVER);
        if (!voiceServersStatus.containsKey(server)) {
          voiceServersStatus.put(server, createDefaultVoiceServerStatus());
        }
        servers.add(server);
      }
    }

    simServersStatus.keySet().stream().filter(server -> !servers.contains(server)).forEach(server -> {
      simServersStatus.remove(server);
      simServersStatus.removeExpireTimeout(server);
    });

    voiceServersStatus.keySet().stream().filter(server -> !servers.contains(server)).forEach(server -> {
      voiceServersStatus.remove(server);
      voiceServersStatus.removeExpireTimeout(server);
    });
  }

  public Map<Server, IVoiceServerStatus> getVoiceServersStatus() {
    voiceServersStatus.clearExpireKeys();
    return new HashMap<>(voiceServersStatus);
  }

  public IVoiceServerStatus getVoiceServerStatus(final Server server) {
    voiceServersStatus.clearExpireKeys();
    return voiceServersStatus.get(server);
  }

  public Map<Server, ISimServerStatus> getSimServersStatus() {
    simServersStatus.clearExpireKeys();
    return new HashMap<>(simServersStatus);
  }

  private ExpireMap.IExpireHandler<Server> createServersStatusExpireHandler() {
    return server -> {
      logger.info("[{}] - detected expired status of server [{}]", this, server);
      simServersStatus.put(server, createDefaultSimServerStatus());
      simUnitManager.forceEndSimServerSession(server.getName());
    };
  }

  private ExpireMap.IExpireHandler<Server> createVoiceServersStatusExpireHandler() {
    return server -> {
      logger.info("[{}] - detected expired status of server [{}]", this, server);
      IVoiceServerStatus defaultVoiceServerStatus = createDefaultVoiceServerStatus();
      voiceServersStatus.put(server, defaultVoiceServerStatus);
      yateCallRouteEngine.ping(server, defaultVoiceServerStatus);
    };
  }

  private IVoiceServerStatus createDefaultVoiceServerStatus() {
    return new VoiceServerStatus(DefaultVoiceServerShortInfo.getInstance(), Collections.emptyList(), -1);
  }

  private ISimServerStatus createDefaultSimServerStatus() {
    return new SimServerStatus(DefaultSimServerShortInfo.getInstance());
  }

}
