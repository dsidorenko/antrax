/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.storage.commons.IServerData;

public class DefaultGatewaySelectorScript implements GatewaySelectorScript {

  @Override
  public boolean isGatewayApplies(final IServerData gateway) {
    return false;
  }

  @Override
  public String toString() {
    return "DefaultGatewaySelectorScript";
  }

  @Override
  public Prediction predictNextGateway() {
    return new Prediction() {

      private static final long serialVersionUID = -8322837215831992056L;

      @Override
      public Prediction trimToSmaller(final Prediction other) {
        return null;
      }

      @Override
      public Prediction trimToBigger(final Prediction other) {
        return null;
      }

      @Override
      public String toLocalizedString() {
        return "none";
      }

      @Override
      public Prediction or(final Prediction other) {
        return other;
      }

      @Override
      public Prediction and(final Prediction other) {
        return this;
      }
    };
  }

}
