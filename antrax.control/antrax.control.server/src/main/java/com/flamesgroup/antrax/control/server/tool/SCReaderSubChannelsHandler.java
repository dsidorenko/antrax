/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.tool;

import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannelsHandler;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderError;
import com.flamesgroup.device.simb.SCReaderState;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SCReaderSubChannelsHandler implements ISCReaderSubChannelsHandler {

  private final Map<ChannelUID, ISCReaderSubChannelHandler> scReaderSubChannelHandlers = new ConcurrentHashMap<>();

  @Override
  public void handleAPDUResponse(final ChannelUID channel, final APDUResponse response) throws RemoteException, IllegalChannelException {
    ISCReaderSubChannelHandler handler = scReaderSubChannelHandlers.get(channel);
    if (handler == null) {
      throw new IllegalChannelException(channel);
    } else {
      handler.handleAPDUResponse(response);
    }

  }

  @Override
  public void handleErrorState(final ChannelUID channel, final SCReaderError error, final SCReaderState state) throws RemoteException, IllegalChannelException {
    ISCReaderSubChannelHandler handler = scReaderSubChannelHandlers.get(channel);
    if (handler == null) {
      throw new IllegalChannelException(channel);
    } else {
      handler.handleErrorState(error, state);
    }
  }

  public void putSCReaderSubChannelHandler(final ChannelUID channelUID, final ISCReaderSubChannelHandler scReaderSubChannelHandler) {
    scReaderSubChannelHandlers.put(channelUID, scReaderSubChannelHandler);
  }

  public void removeSCReaderSubChannelHandler(final ChannelUID channelUID) {
    scReaderSubChannelHandlers.remove(channelUID);
  }

}
