/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.activity.ISimCallHistoryManager;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SimCallHistoryManager implements ISimCallHistoryManager, ISimCallHistory {

  private final ICallDAO callDAO;

  private final Lock simCallHistoryLock = new ReentrantLock();
  private Map<ICCID, Set<PhoneNumber>> simCallHistories = new HashMap<>();
  private Map<PhoneNumber, Map<ICCID, Integer>> calledChannelICCID = new HashMap<>();

  public SimCallHistoryManager(final ICallDAO callDAO) {
    this.callDAO = callDAO;
  }

  @Override
  public void updateHistory(final ICCID iccid, final PhoneNumber number) {
    simCallHistoryLock.lock();
    try {
      simCallHistories.computeIfAbsent(iccid, n -> new HashSet<>()).add(number);
      calledChannelICCID.computeIfAbsent(number, n -> new HashMap<>()).compute(iccid, (iccid1, count) -> count == null ? 1 : count + 1);
    } finally {
      simCallHistoryLock.unlock();
    }
  }

  @Override
  public String getName() {
    return getClass().getSimpleName();
  }

  @Override
  public boolean reload() {
    long simCallHistoryPeriod = ControlPropUtils.getInstance().getControlServerProperties().getSimCallHistoryPeriod();
    simCallHistoryLock.lock();
    try {
      simCallHistories = callDAO.getSimCalledNumbers(System.currentTimeMillis() - simCallHistoryPeriod);
      calledChannelICCID = callDAO.getCalledNumbersToSim(System.currentTimeMillis() - simCallHistoryPeriod);
    } finally {
      simCallHistoryLock.unlock();
    }
    return true;
  }

  @Override
  public boolean containNumber(final ICCID iccid, final PhoneNumber phoneNumber) throws RemoteException {
    simCallHistoryLock.lock();
    try {
      return simCallHistories.getOrDefault(iccid, new HashSet<>()).contains(phoneNumber);
    } finally {
      simCallHistoryLock.unlock();
    }
  }

  @Override
  public Map<ICCID, Integer> getICCIDForNumber(final PhoneNumber phoneNumber) {
    return calledChannelICCID.get(phoneNumber);
  }

}
