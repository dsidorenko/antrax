/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.tool;

import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannelsHandler;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.gsmb.IRawATSubChannelHandler;

import java.nio.ByteBuffer;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RawATSubChannelsHandler implements IRawATSubChannelsHandler {

  private final Map<ChannelUID, IRawATSubChannelHandler> rawATSubChannelHandlers = new ConcurrentHashMap<>();

  @Override
  public void handleRawATData(final ChannelUID channel, final byte[] atData) throws RemoteException, IllegalChannelException {
    IRawATSubChannelHandler handler = rawATSubChannelHandlers.get(channel);
    if (handler == null) {
      throw new IllegalChannelException(channel);
    } else {
      handler.handleRawATData(ByteBuffer.wrap(atData));
    }
  }

  public void putRawATSubChannelHandler(final ChannelUID channel, final IRawATSubChannelHandler handler) {
    rawATSubChannelHandlers.put(channel, handler);
  }

  public void removeRawATSubChannelHandler(final ChannelUID channel) {
    rawATSubChannelHandlers.remove(channel);
  }

}
