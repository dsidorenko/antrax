/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;

import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class AlarisDlrHttpService {

  private static final Logger logger = LoggerFactory.getLogger(AlarisDlrHttpService.class);

  private String createUrlParameters(final Map<String, String> parameters) throws UnsupportedEncodingException {
    StringBuilder result = new StringBuilder();
    for (Map.Entry<String, String> entry : parameters.entrySet()) {
      if (result.length() > 0) {
        result.append("&");
      }

      result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
      result.append("=");
      result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
    }

    return result.toString();
  }

  public boolean sendDlr(final AlarisHttpSms alarisSms) {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("messageId", alarisSms.getId().toString());
    parameters.put("status", alarisSms.getStatus().toString());

    String drlUrl = alarisSms.getDlrUrl();
    drlUrl += drlUrl.contains("?") ? "&" : "?";
    try {
      URL url = new URL(drlUrl + createUrlParameters(parameters));
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod("GET");

      int code = connection.getResponseCode();
      if (code == HttpURLConnection.HTTP_OK) {
        return true;
      } else {
        StringBuilder content = new StringBuilder();
        InputStream inputStream = connection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST ? connection.getInputStream() : connection.getErrorStream();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
          String line;
          while ((line = bufferedReader.readLine()) != null) {
            content.append(line).append(System.lineSeparator());
          }
        }
        logger.warn("[{}] - can't send callback to url [{}] response [{}]", this, alarisSms.getDlrUrl(), content);
        return false;
      }
    } catch (IOException e) {
      logger.error("[{}] - while send callback to url [{}]", this, alarisSms.getDlrUrl(), e);
      return false;
    }
  }

}
