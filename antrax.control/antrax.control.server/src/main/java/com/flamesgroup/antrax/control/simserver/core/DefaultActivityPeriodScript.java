/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript;

public class DefaultActivityPeriodScript implements ActivityPeriodScript {

  @Override
  public boolean isReadyToStartPeriod() {
    return false;
  }

  @Override
  public Prediction predictPeriodStart() {
    return new AlwaysFalsePrediction();
  }

  @Override
  public Prediction predictPeriodStop() {
    return new AlwaysTruePrediction();
  }

  @Override
  public boolean shouldStopPeriod() {
    return true;
  }

  @Override
  public String toString() {
    return "DefaultSIMCardSessionScript";
  }

}
