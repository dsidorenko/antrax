/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.cedarsoftware.util.GraphComparator;
import com.flamesgroup.antrax.configuration.diff.DiffUtil;
import com.flamesgroup.antrax.configuration.diff.ListContainer;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.autoactions.ActionExecutionManager;
import com.flamesgroup.antrax.control.commons.CommonConstants;
import com.flamesgroup.antrax.control.communication.ControlBean;
import com.flamesgroup.antrax.control.communication.ISimServerStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SmsHistory;
import com.flamesgroup.antrax.control.communication.SmsStatistic;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.server.alaris.sms.http.AlarisSmsHttpService;
import com.flamesgroup.antrax.control.server.alaris.sms.smpp.AlarisSmsSmppService;
import com.flamesgroup.antrax.control.server.routing.YateCallRouteEngine;
import com.flamesgroup.antrax.control.server.utils.ControlSessionListener;
import com.flamesgroup.antrax.control.server.utils.SessionClientData;
import com.flamesgroup.antrax.control.server.utils.SessionsPool;
import com.flamesgroup.antrax.control.simserver.SimUnitManager;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.Revisions;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.SessionParams;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;
import com.flamesgroup.antrax.storage.commons.impl.SimSearchingParams;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISessionDAO;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.antrax.voiceserv.utils.delivery.GuaranteedDeliveryProxyServer;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.DTMFException;
import com.flamesgroup.commons.EventException;
import com.flamesgroup.commons.LockArfcnException;
import com.flamesgroup.commons.LockException;
import com.flamesgroup.commons.NetworkSurveyException;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.commons.USSDException;
import com.flamesgroup.commons.VoiceServerCallStatistic;
import com.flamesgroup.rmi.RemoteServiceExporter;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.sms.SMSException;
import com.flamesgroup.yextmodule.YExtMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class ControlBeanImpl implements ControlBean {

  private final ISessionDAO sessionDAO;
  private final IConfigViewDAO configViewDAO;
  private final Thread sessionsCleaner;

  private final long startTime = System.currentTimeMillis();
  private final Map<ClientUID, Pair<SessionClientData, Long>> sessionsCount = new HashMap<>();

  private ControlBeanModel model;
  private PingServersManager pingServersManager;
  private GuaranteedDeliveryProxyServer simHistoryDAODeliveryServer;
  private ActionExecutionManager actionExecutionManager;
  private SimUnitManager simUnitManager;

  private final Map<ClientUID, Map<String, List<SimViewData>>> prevSimRuntimeData = new ConcurrentHashMap<>();

  private final Lock lockSimRuntimeData = new ReentrantLock();

  private YateCallRouteEngine yateCallRouteEngine;
  private AlarisSmsHttpService alarisSmsHttpService;
  private AlarisSmsSmppService alarisSmsSmppService;

  public ControlBeanImpl(final ISessionDAO sessionDAO, final IConfigViewDAO configViewDAO) {
    this.sessionDAO = sessionDAO;
    this.configViewDAO = configViewDAO;
    this.sessionsCleaner = createSessionsCleaner();

    SessionsPool.getInstance().addSessionListener(new ControlSessionListener() {
      private ClientUID systemUID;

      @Override
      public void handleSessionStarted(final SessionClientData data) {
        if (Objects.equals(data.getUserGroup(), "system")) {
          systemUID = data.getClientUID();
          return;
        }
        synchronized (sessionsCount) {
          sessionsCount.put(data.getClientUID(), mkPair(data));
        }
        ControlBeanImpl.this.sessionDAO.insertNewSession(data.getClientUID().toString(), data.getUserName(), data.getUserGroup(), data.getUserIP());
      }

      @Override
      public void handleSessionRenewed(final ClientUID client) {
        if (client.equals(systemUID)) {
          return;
        }
        synchronized (sessionsCount) {
          Pair<SessionClientData, Long> pair = sessionsCount.get(client);
          sessionsCount.put(pair.first().getClientUID(), mkPair(pair.first()));
        }
        ControlBeanImpl.this.sessionDAO.renewSession(client.toString());
      }

      @Override
      public void handleSessionEnded(final ClientUID client) {
        if (client.equals(systemUID)) {
          return;
        }
        synchronized (sessionsCount) {
          sessionsCount.remove(client);
        }
        ControlBeanImpl.this.sessionDAO.closeSession(client.toString());
      }

      private Pair<SessionClientData, Long> mkPair(final SessionClientData data) {
        return new Pair<>(data, System.currentTimeMillis());
      }

    });
  }

  private Thread createSessionsCleaner() {
    return new Thread("Sessions cleaner") {
      @Override
      public void run() {
        while (!isInterrupted()) {
          SessionsPool.getInstance().cleanup();
          try {
            sleep(SessionsPool.SESSION_LIFETIME);
          } catch (InterruptedException ignored) {
            break;
          }
        }
      }
    };
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) {
    return true;
  }

  @Override
  public void checkSession(final ClientUID clientUID) throws NotPermittedException {
  }

  @Override
  public List<SimViewData> getSimRuntimeData(final ClientUID client, final String serverName) throws NotPermittedException {
    List<SimViewData> newSimRuntimeData = simUnitManager.getSimRuntimeData(serverName);
    Map<String, List<SimViewData>> serversSimViewData = prevSimRuntimeData.getOrDefault(client, new HashMap<>());
    serversSimViewData.put(serverName, newSimRuntimeData);
    prevSimRuntimeData.put(client, serversSimViewData);
    return newSimRuntimeData;
  }

  @Override
  public Pair<Long, List<ChannelConfig>> getSimChannelConfig(final ClientUID client, final String serverName) throws NotPermittedException {
    List<SimViewData> simViewDatas = simUnitManager.getSimRuntimeData(serverName);
    if (simViewDatas == null) {
      return null;
    }
    List<ChannelConfig> channelConfigs = simViewDatas.stream().map(SimViewData::getChannelConfig).collect(Collectors.toList());
    long serverTime = 0;
    if (simViewDatas.size() > 0) {
      serverTime = simViewDatas.get(0).getServerTimeMillis();
    }
    return new Pair<>(serverTime, channelConfigs);
  }

  @Override
  public List<GraphComparator.Delta> getSimRuntimeDataDiff(final ClientUID client, final String serverName) {
    lockSimRuntimeData.lock();
    try {
      List<SimViewData> newSimViewData = simUnitManager.getSimRuntimeData(serverName);
      Map<String, List<SimViewData>> serversSimViewData = prevSimRuntimeData.getOrDefault(client, new HashMap<>());
      List<SimViewData> prevSimViewData = serversSimViewData.getOrDefault(serverName, new ArrayList<>());

      List<GraphComparator.Delta> diff = GraphComparator.compare(new ListContainer(0, prevSimViewData), new ListContainer(0, newSimViewData), DiffUtil.getIdFetcher());
      serversSimViewData.put(serverName, newSimViewData);
      prevSimRuntimeData.put(client, serversSimViewData);
      return diff;
    } finally {
      lockSimRuntimeData.unlock();
    }
  }

  @Override
  public SIMEventRec[] listSIMEvents(final ClientUID client, final ICCID simUID, final Date fromDate, final Date toDate, final int limit) throws NotPermittedException {
    return model.listSIMEvents(simUID, fromDate, toDate, limit);
  }

  @Override
  public List<IVoiceServerChannelsInfo> listVoiceServerChannels(final ClientUID client, final IServerData server) throws NoSuchFieldException {
    IVoiceServerStatus status = pingServersManager.getVoiceServerStatus(new Server(server.getName(), ServerType.VOICE_SERVER));
    if (status.getShortInfo().getStatus() == ServerStatus.NO_CONNECTION) {
      return Collections.emptyList();
    } else {
      return status.getChannelsInfo();
    }
  }

  @Override
  public List<IServerData> listVoiceServers(final ClientUID client) throws NotPermittedException, StorageException {
    return model.listVoiceServers();
  }

  @Override
  public List<IServerData> listSimServers(final ClientUID client) throws NotPermittedException, StorageException {
    return model.listSimServers(client);
  }

  @Override
  public List<MobileGatewayChannelInformation> listVoiceServerChannelsStatistic(final ClientUID clientUID, final IServerData server) {
    IVoiceServerStatus status = pingServersManager.getVoiceServerStatus(new Server(server.getName(), ServerType.VOICE_SERVER));

    if (status.getShortInfo().getStatus() == ServerStatus.NO_CONNECTION) {
      return Collections.emptyList();
    }
    return status.getChannelsInfo().stream()
        .filter(channel -> channel.getMobileGatewayChannelInfo() != null)
        .map(IVoiceServerChannelsInfo::getMobileGatewayChannelInfo).collect(Collectors.toList());
  }

  @Override
  public ICCID[] findSIMList(final ClientUID client, final SimSearchingParams params) throws NotPermittedException, StorageException {
    return model.findSIMList(params);
  }

  @Override
  public CDR getCDR(final ClientUID client, final Long id) throws NotPermittedException, StorageException {
    return model.getCDR(id);
  }

  @Override
  public Map<Server, IVoiceServerStatus> listVoiceServerStatus(final ClientUID client) throws NotPermittedException {
    return pingServersManager.getVoiceServersStatus();
  }

  @Override
  public Map<Server, ISimServerStatus> listSimServerStatus(final ClientUID client) throws NotPermittedException {
    return pingServersManager.getSimServersStatus();
  }

  @Override
  public void sendUssd(final ClientUID client, final IServerData server, final ICCID simUID, final String ussd) throws NoSuchFieldException, USSDException {
    model.sendUssd(server, simUID, ussd);
  }

  @Override
  public String startUSSDSession(final ClientUID client, final IServerData server, final ICCID simUID, final String ussd) throws NotPermittedException, NoSuchFieldException, USSDException {
    return model.startUSSDSession(server, simUID, ussd);
  }

  @Override
  public String sendUSSDSessionCommand(final ClientUID client, final IServerData server, final ICCID simUID, final String command) throws NotPermittedException, NoSuchFieldException, USSDException {
    return model.sendUSSDSessionCommand(server, simUID, command);
  }

  @Override
  public void endUSSDSession(final ClientUID client, final IServerData server, final ICCID simUID) throws NotPermittedException, NoSuchFieldException, USSDException {
    model.endUSSDSession(server, simUID);
  }

  @Override
  public void fireEvent(final ClientUID client, final IServerData server, final ICCID simUID, final String event) throws NoSuchFieldException, EventException {
    model.fireEvent(server, simUID, event);
  }

  @Override
  public void lockGsmChannel(final ClientUID client, final IServerData server, final ChannelUID gsmChannel, final boolean lock, final String lockReason) throws NotPermittedException,
      NoSuchFieldException, LockException {
    model.lockGsmChannel(server, gsmChannel, lock, lockReason);
  }

  @Override
  public void lockGsmChannelToArfcn(final ClientUID clientUID, final IServerData server, final ChannelUID gsmChannel, final int arfn)
      throws NotPermittedException, NoSuchFieldException, LockArfcnException {
    model.lockGsmChannelToArfcn(server, gsmChannel, arfn);
  }

  @Override
  public void unLockGsmChannelToArfcn(final ClientUID clientUID, final IServerData server, final ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, LockArfcnException {
    model.unLockGsmChannelToArfcn(server, gsmChannel);
  }

  @Override
  public List<SmsHistory> listSmsHistory(final ClientUID clientUID, final Date fromDate, final Date toDate, final List<String> iccids, final String clientIp) {
    return model.listSmsHistory(fromDate, toDate, iccids, clientIp);
  }

  @Override
  public List<SmsStatistic> listSmsStatistic(final ClientUID clientUID) {
    return alarisSmsHttpService.getSmsStatistic();
  }

  @Override
  public List<VoiceServerCallStatistic> listVoiceServerCallStatistic(final ClientUID clientUID, final Date fromDate, final Date toDate, final String prefix, final String server)
      throws NotPermittedException {
    return model.listVoiceServerCallStatistic(fromDate, toDate, prefix, server);
  }

  @Override
  public List<AlarisSmppSms> listAlarisSmppSmses(final ClientUID clientUID, final Date fromDate, final Date toDate, final String source, final String destination, final String username,
      final AlarisSms.Status status) throws NotPermittedException {
    return model.listAlarisSmppSmses(fromDate, toDate, source, destination, username, status);
  }

  @Override
  public List<AlarisHttpSms> listAlarisHttpSmses(final ClientUID clientUID, final Date fromDate, final Date toDate, final String ani, final String dnis, final String clientIp,
      final AlarisSms.Status status) throws NotPermittedException {
    return model.listAlarisHttpSmses(fromDate, toDate, ani, dnis, clientIp, status);
  }

  @Override
  public void executeNetworkSurvey(final ClientUID client, final IServerData server, final ChannelUID gsmChannel) throws NotPermittedException, NoSuchFieldException, NetworkSurveyException {
    model.executeNetworkSurvey(server, gsmChannel);
  }

  @Override
  public void sendSMS(final ClientUID client, final IServerData server, final ICCID simUID, final String number, final String smsText)
      throws NoSuchFieldException, SMSException {
    model.sendSMS(server, simUID, number, smsText);
  }

  @Override
  public void sendDTMF(final ClientUID client, final IServerData server, final ICCID simUID, final String dtmf) throws NoSuchFieldException, DTMFException {
    model.sendDTMF(server, simUID, dtmf);
  }

  @Override
  public void resetStatistic(final ClientUID client, final IServerData server) throws NotPermittedException, NoSuchFieldException {
    IVoiceServerStatus status = pingServersManager.getVoiceServerStatus(new Server(server.getName(), ServerType.VOICE_SERVER));
    for (IVoiceServerChannelsInfo info : status.getChannelsInfo()) {
      info.getMobileGatewayChannelInfo().reset();
    }

    model.resetStatistic(server);
  }

  public void start() {
    try {
      for (SIMGroup simGroup : configViewDAO.listSIMGroups()) {
        Revisions.getInstance().putSimGroupRevision(simGroup);
      }
    } catch (DataSelectionException e) {
      throw new IllegalStateException("Can't get list of sim groups", e);
    }

    simHistoryDAODeliveryServer.start();
    sessionDAO.cleanup();
    sessionsCleaner.start();
    simUnitManager.start();
    alarisSmsHttpService.start();
    alarisSmsSmppService.start();
    try {
      yateCallRouteEngine.start();
    } catch (IOException | YExtMException | InterruptedException e) {
      throw new IllegalStateException("Can't start yate call route engine", e);
    }
  }

  @Override
  public void shutdown(final ClientUID client) throws NotPermittedException {
    yateCallRouteEngine.stop();
    alarisSmsHttpService.stop();
    alarisSmsSmppService.stop();
    simHistoryDAODeliveryServer.terminate();
    actionExecutionManager.terminate();
    sessionsCleaner.interrupt();

    simUnitManager.stop();

    RemoteServiceExporter.destroyAll();
  }



  @Override
  public int getSessionsCount(final ClientUID clientUid) {
    int retval = 0;
    synchronized (sessionsCount) {
      for (ClientUID clientUID : sessionsCount.keySet()) {
        long lastRenewed = sessionsCount.get(clientUID).second();
        if (System.currentTimeMillis() - lastRenewed < CommonConstants.REFRESH_TIMEOUT * 2) {
          retval++;
        }
      }
    }
    return retval;
  }

  @Override
  public SessionParams[] getSessionInfos(final ClientUID clientUID) throws NotPermittedException {
    return sessionDAO.listSessions();
  }

  @Override
  public CDR[] listCDRs(final ClientUID client, final Date fromDate, final Date toDate, final String gsmGroup, final String simGroup, final String callerNumber, final String calledNumber,
      final ICCID simUid, final int pageSize, final int offset) throws NotPermittedException {
    return model.listCDRs(fromDate, toDate, gsmGroup, simGroup, callerNumber, calledNumber, simUid, pageSize, offset);

  }

  @Override
  public long getUptime(final ClientUID client) throws NotPermittedException {
    return System.currentTimeMillis() - startTime;
  }

  public ControlBeanImpl setModel(final ControlBeanModel model) {
    this.model = model;
    return this;
  }

  public ControlBeanImpl setPingServersManager(final PingServersManager pingServersManager) {
    this.pingServersManager = pingServersManager;
    return this;
  }

  public ControlBeanImpl setSimHistoryDAODeliveryServer(final GuaranteedDeliveryProxyServer simHistoryDAODeliveryServer) {
    this.simHistoryDAODeliveryServer = simHistoryDAODeliveryServer;
    return this;
  }

  public ControlBeanImpl setActionExecutionManager(final ActionExecutionManager actionExecutionManager) {
    this.actionExecutionManager = actionExecutionManager;
    return this;
  }

  public void setSimUnitManager(final SimUnitManager simUnitManager) {
    this.simUnitManager = simUnitManager;
  }

  public ControlBeanImpl setYateCallRouteEngine(final YateCallRouteEngine yateCallRouteEngine) {
    this.yateCallRouteEngine = yateCallRouteEngine;
    return this;
  }

  public ControlBeanImpl setAlarisSmsHttpService(final AlarisSmsHttpService alarisSmsHttpService) {
    this.alarisSmsHttpService = alarisSmsHttpService;
    return this;
  }

  public ControlBeanImpl setAlarisSmsSmppService(final AlarisSmsSmppService alarisSmsSmppService) {
    this.alarisSmsSmppService = alarisSmsSmppService;
    return this;
  }

}
