package com.flamesgroup.antrax.control.server.alaris.sms.smpp;

import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.smpp.SmppConstants;

public final class PduUtil {

  private PduUtil() {
  }

  public static String decodeShortMessage(final int dataCoding, final byte[] shortMessage) {
    return CharsetUtil.decode(shortMessage, mapDataCodingToCharset(dataCoding));
  }


  private static String mapDataCodingToCharset(final int dataCoding) {
    switch (dataCoding) {
      case SmppConstants.DATA_CODING_DEFAULT:
        return CharsetUtil.NAME_GSM;
      case SmppConstants.DATA_CODING_LATIN1:
        return CharsetUtil.NAME_ISO_8859_1;
      case SmppConstants.DATA_CODING_UCS2:
        return CharsetUtil.NAME_UCS_2;
      default:
        return CharsetUtil.NAME_ISO_8859_15;
    }
  }

}
