/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;

import com.flamesgroup.antrax.control.properties.AlarisMultiUser;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class AuthHttpFilter implements ContainerRequestFilter {

  private static final Logger logger = LoggerFactory.getLogger(AuthHttpFilter.class);

  @Override
  public void filter(final ContainerRequestContext requestContext) throws IOException {
    MultivaluedMap<String, String> queryParameters = requestContext.getUriInfo().getQueryParameters();
    logger.debug("[{}] - got queryParameters: {}", this, queryParameters);
    String username = queryParameters.getFirst("username");
    AlarisMultiUser alarisMultiUser = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerMultiUser().get(username);
    if (alarisMultiUser == null) {
      throw new AlarisSmsHttpException("Unknown username");
    }
    String password = queryParameters.getFirst("password");
    if (password == null) {
      throw new AlarisSmsHttpException("Unknown password");
    }
    if (!password.equals(alarisMultiUser.getPassword())) {
      throw new AlarisSmsHttpException("Incorrect password", Response.Status.UNAUTHORIZED);
    }
  }

}
