/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.properties;

import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

public class CallRouteErrorsProperties implements ServerProperties {

  private static final String VOIPANTISPAM_GRAYLIST_ERROR = "voipantispam.graylist.error";
  private static final String VOIPANTISPAM_GRAYLIST_REASON = "voipantispam.graylist.reason";

  private static final String VOIPANTISPAM_BLACKLIST_ERROR = "voipantispam.blacklist.error";
  private static final String VOIPANTISPAM_BLACKLIST_REASON = "voipantispam.blacklist.reason";

  private static final String CALL_NOROUTE_ERROR = "call.noroute.error";
  private static final String CALL_NOROUTE_REASON = "call.noroute.reason";


  private final AtomicReference<String> voipantispamGraylistError = new AtomicReference<>();
  private final AtomicReference<String> voipantispamGraylistReason = new AtomicReference<>();

  private final AtomicReference<String> voipantispamBlacklistError = new AtomicReference<>();
  private final AtomicReference<String> voipantispamBlacklistReason = new AtomicReference<>();

  private final AtomicReference<String> callNorouteError = new AtomicReference<>();
  private final AtomicReference<String> callNorouteReason = new AtomicReference<>();


  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);

    voipantispamGraylistError.set(propertiesLoader.getString(VOIPANTISPAM_GRAYLIST_ERROR));
    voipantispamGraylistReason.set(propertiesLoader.getString(VOIPANTISPAM_GRAYLIST_REASON));

    voipantispamBlacklistError.set(propertiesLoader.getString(VOIPANTISPAM_BLACKLIST_ERROR));
    voipantispamBlacklistReason.set(propertiesLoader.getString(VOIPANTISPAM_BLACKLIST_REASON));

    callNorouteError.set(propertiesLoader.getString(CALL_NOROUTE_ERROR));
    callNorouteReason.set(propertiesLoader.getString(CALL_NOROUTE_REASON));
  }

  public String getVoipantispamGraylistError() {
    return voipantispamGraylistError.get();
  }

  public String getVoipantispamGraylistReason() {
    return voipantispamGraylistReason.get();
  }

  public String getVoipantispamBlacklistError() {
    return voipantispamBlacklistError.get();
  }

  public String getVoipantispamBlacklistReason() {
    return voipantispamBlacklistReason.get();
  }

  public String getCallNorouteError() {
    return callNorouteError.get();
  }

  public String getCallNorouteReason() {
    return callNorouteReason.get();
  }

}
