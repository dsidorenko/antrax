/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.activity.ISimHistoryLogger;
import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.scripts.IMEIGeneratorScript;
import com.flamesgroup.antrax.automation.statefulscripts.IScriptDeserializer;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.communication.ISimChannelManager;
import com.flamesgroup.antrax.control.simserver.ISimActivityLogger;
import com.flamesgroup.antrax.control.simserver.SimRuntimeData;
import com.flamesgroup.antrax.plugins.cuncurrent.ScriptHolder;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.commons.impl.SimNoGroup;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.dao.impl.ScriptType;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.antrax.storage.state.CHVState;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.IMEI;
import com.flamesgroup.commons.IMEIException;
import com.flamesgroup.device.commonb.IndicationMode;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.rmi.RemoteException;

public class SimUnit {

  private static final Logger logger = LoggerFactory.getLogger(SimUnit.class);

  private abstract class SimFeederScriptHolder<T> extends ScriptHolder<T> {

    @SafeVarargs
    public SimFeederScriptHolder(final T... defaultScript) {
      super(defaultScript);
    }

    @Override
    protected void feedWithEvents(final T script) {
      if (taken && script instanceof SessionListener) {
        ((SessionListener) script).handleSessionStarted(currentGateway);
      }
      if (active && script instanceof ActivityListener) {
        ((ActivityListener) script).handleActivityStarted(currentGSMGroup);
      }
    }
  }

  private final ScriptHolder<FactorEvaluationScript> factorScript = new SimFeederScriptHolder<FactorEvaluationScript>(new DefaultSIMCardFactorScript()) {

    @Override
    protected void saveScripts(final FactorEvaluationScript[] scripts) {
      saver.saveScript(getUID(), ScriptType.FACTOR_SCRIPT, filter(scripts));
    }
  };

  private final ScriptHolder<GatewaySelectorScript> gatewayScript = new SimFeederScriptHolder<GatewaySelectorScript>(new DefaultGatewaySelectorScript()) {

    @Override
    protected void saveScripts(final GatewaySelectorScript[] scripts) {
      saver.saveScript(getUID(), ScriptType.GW_SELECTOR_SCRIPT, filter(scripts));
    }
  };

  private final ScriptHolder<ActivityPeriodScript> sessionScript = new SimFeederScriptHolder<ActivityPeriodScript>(new DefaultActivityPeriodScript()) {

    @Override
    protected void saveScripts(final ActivityPeriodScript[] scripts) {
      saver.saveScript(getUID(), ScriptType.SESSION_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final ActivityPeriodScript script) {
      super.feedWithEvents(script);
      if (taken && script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodStart();
      }
    }

  };
  private final ScriptHolder<ActivityPeriodScript> activityScript = new SimFeederScriptHolder<ActivityPeriodScript>(new DefaultActivityPeriodScript()) {

    @Override
    protected void saveScripts(final ActivityPeriodScript[] scripts) {
      saver.saveScript(getUID(), ScriptType.ACTIVITY_PERIOD_SCRIPT, filter(scripts));
    }

    @Override
    protected void feedWithEvents(final ActivityPeriodScript script) {
      super.feedWithEvents(script);
      if (active && script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodStart();
      }
    }

  };
  private final ScriptHolder<IMEIGeneratorScript> imeiGeneratorScript = new SimFeederScriptHolder<IMEIGeneratorScript>(new DefaultIMEIGeneratorScript()) {

    @Override
    protected void saveScripts(final IMEIGeneratorScript[] scripts) {
      saver.saveScript(getUID(), ScriptType.IMEI_GENERATOR_SCRIPT, filter(scripts));
    }

  };

  private final ChannelUID channelUID;
  private final ISimChannelManager simChannelManager;

  private final SimRuntimeData simRuntimeData = new SimRuntimeData();

  private IMSI imsi;
  private SimData simData;

  private volatile boolean taken;
  private volatile boolean forceSessionEnd;
  private ScriptStateSaver saver;
  private GSMGroup currentGSMGroup = new GsmNoGroup();
  private IServerData currentGateway;
  private boolean active;
  private boolean registration;
  private volatile boolean plugged = true;
  private ISimActivityLogger simActivityLogger;
  private ISimHistoryLogger simHistoryLogger;
  private volatile boolean reconfigured;
  private ISimpleConfigEditDAO simpleConfigEditDAO;
  private IConfigViewDAO configViewDAO;
  private String lastStatus = "";
  private final LogStatusHelper loggerStatusHelper = new LogStatusHelper();
  private CHVState chvState;

  private volatile boolean firstInserting;

  public SimUnit(final ChannelUID channelUID, final ISimChannelManager simChannelManager) {
    this.channelUID = channelUID;
    this.simChannelManager = simChannelManager;
  }

  public void forceSessionEnd() {
    logger.debug("[{}] - forcing session end", this);
    forceSessionEnd = true;
  }

  public long countFactor() {
    try {
      return factorScript.takeScriptsForUse()[0].countFactor();
    } finally {
      factorScript.untakeScripts();
    }
  }

  public boolean isTaken() {
    return taken;
  }

  public boolean appliesGateway(final IServerData gateway) {
    try {
      return gatewayScript.takeScriptsForUse()[0].isGatewayApplies(gateway);
    } finally {
      gatewayScript.untakeScripts();
    }
  }

  public boolean isReadyToStartSession() {
    if (simData.isLocked()) {
      return false;
    }

    try {
      return sessionScript.takeScriptsForUse()[0].isReadyToStartPeriod();
    } finally {
      sessionScript.untakeScripts();
    }
  }

  public boolean isReadyIMEIGenerator() {
    try {
      IMEIGeneratorScript script = imeiGeneratorScript.takeScriptsForUse()[0];
      if (script instanceof DefaultIMEIGeneratorScript) {
        return false;
      }
    } finally {
      imeiGeneratorScript.untakeScripts();
    }
    return true;
  }

  public synchronized void take(final GSMGroup gsmGroup, final IServerData gateway) {
    if (taken) {
      throw new IllegalStateException("Attempt to take already taken sim unit");
    }
    taken = true;
    fireSessionStarted(sessionScript, gateway);
    fireSessionStarted(gatewayScript, gateway);
    fireSessionStarted(factorScript, gateway);
    fireSessionStarted(activityScript, gateway);
    fireSessionStarted(imeiGeneratorScript, gateway);

    try {
      ActivityPeriodScript script = sessionScript.takeScriptsForUse()[0];
      if (script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodStart();
      }
    } finally {
      sessionScript.untakeScripts();
    }

    currentGSMGroup = gsmGroup;
    currentGateway = gateway;

    simRuntimeData.setCurrentGateway(gateway.getName());
    simHistoryLogger.handleEvent(SimEvent.simCardTaken(getUID(), gateway, gsmGroup));
    logSimStatus(true);
  }

  public synchronized void untake() {
    if (!taken) {
      return;
    }
    if (active) {
      stopActivity("stop session");
    }
    taken = false;
    forceSessionEnd = false;
    fireSessionEnded(sessionScript);
    fireSessionEnded(activityScript);
    fireSessionEnded(gatewayScript);
    fireSessionEnded(factorScript);
    fireSessionEnded(imeiGeneratorScript);
    try {
      ActivityPeriodScript script = sessionScript.takeScriptsForUse()[0];
      if (script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodEnd();
      }
    } finally {
      sessionScript.untakeScripts();
    }

    simRuntimeData.setPreviousGateway(currentGateway.getName()).setCurrentGateway(null);
    simHistoryLogger.handleEvent(SimEvent.simReturned(getUID(), currentGateway, currentGSMGroup));
    logSimStatus(true);
  }

  public synchronized void stopActivity(final String stopReason) {
    if (!active) {
      return;
    }

    active = false;
    if (!simData.isLocked()) {
      setIndicationMode(IndicationMode.READY);
    }

    fireActivityEnded(sessionScript);
    fireActivityEnded(activityScript);
    fireActivityEnded(factorScript);
    fireActivityEnded(gatewayScript);
    fireActivityEnded(imeiGeneratorScript);
    try {
      ActivityPeriodScript script = activityScript.takeScriptsForUse()[0];
      if (script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodEnd();
      }
    } finally {
      activityScript.untakeScripts();
    }

    simRuntimeData.setGsmHolder(null);
    simHistoryLogger.handleEvent(SimEvent.gsmUnregistered(getUID(), stopReason));
    logSimStatus(true);
  }

  public synchronized void startActivity(final ChannelUID gsmChannel, final GSMGroup gsmGroup) {
    if (active) {
      throw new IllegalStateException("Attempting to start already started activity");
    }

    active = true;
    setIndicationMode(IndicationMode.ACTIVE);

    fireActivityStarted(sessionScript, gsmGroup);
    fireActivityStarted(activityScript, gsmGroup);
    fireActivityStarted(factorScript, gsmGroup);
    fireActivityStarted(gatewayScript, gsmGroup);
    fireActivityStarted(imeiGeneratorScript, gsmGroup);

    try {
      ActivityPeriodScript script = activityScript.takeScriptsForUse()[0];
      if (script instanceof ActivityPeriodListener) {
        ((ActivityPeriodListener) script).handlePeriodStart();
      }
    } finally {
      activityScript.untakeScripts();
    }

    simRuntimeData.setGsmHolder(gsmChannel);
    logSimStatus(true);
  }

  public void handleGenericEvent(final String event, final Serializable... args) {
    fireGenericEvent(sessionScript, event, args);
    fireGenericEvent(activityScript, event, args);
    fireGenericEvent(factorScript, event, args);
    fireGenericEvent(gatewayScript, event, args);
    fireGenericEvent(imeiGeneratorScript, event, args);

    simHistoryLogger.handleEvent(SimEvent.genericEvent(getUID(), event, args));
    logSimStatus(true);
  }

  public void addUserMessage(final String message) {
    simRuntimeData.setUserMessageTime3(simRuntimeData.getUserMessageTime2());
    simRuntimeData.setUserMessageTime2(simRuntimeData.getUserMessageTime1());
    simRuntimeData.setUserMessageTime1(System.currentTimeMillis());
    simRuntimeData.setUserMessage3(simRuntimeData.getUserMessage2());
    simRuntimeData.setUserMessage2(simRuntimeData.getUserMessage1());
    simRuntimeData.setUserMessage1(message);
  }

  public IMSI getImsi() {
    return imsi;
  }

  public void setImsi(final IMSI imsi) {
    this.imsi = imsi;
  }

  public GSMGroup getCurrentGSMGroup() {
    return currentGSMGroup;
  }

  public ICCID getUID() {
    return getSimData().getUid();
  }

  public SIMGroup getSIMGroup() {
    return simData.getSimGroup();
  }

  public void setGatewaySelectorScript(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final GatewaySelectorScript gatewayScript) {
    this.gatewayScript.setNewInstance(scriptDeserializer, resetScriptStates, gatewayScript);
    reconfigured = true;
  }

  public void setFactorScript(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final FactorEvaluationScript factorScript) {
    this.factorScript.setNewInstance(scriptDeserializer, resetScriptStates, factorScript);
    reconfigured = true;
  }

  public void setSaver(final ScriptStateSaver saver) {
    this.saver = saver;
  }

  public void setSimActivityLogger(final ISimActivityLogger simActivityLogger) {
    this.simActivityLogger = simActivityLogger;
  }

  public void setSimHistoryLogger(final ISimHistoryLogger simHistoryLogger) {
    this.simHistoryLogger = simHistoryLogger;
  }

  public boolean isPlugged() {
    return plugged;
  }

  public boolean isFirstInserting() {
    return firstInserting;
  }

  public void setActivityScript(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final ActivityPeriodScript activityScript) {
    this.activityScript.setNewInstance(scriptDeserializer, resetScriptStates, activityScript);
    reconfigured = true;
  }

  public void setSimpleConfigEditDAO(final ISimpleConfigEditDAO simpleConfigEditDAO) {
    assert simpleConfigEditDAO != null;
    this.simpleConfigEditDAO = simpleConfigEditDAO;
  }

  public void setConfigViewDAO(final IConfigViewDAO configViewDAO) {
    this.configViewDAO = configViewDAO;
  }

  public void setSessionScript(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final ActivityPeriodScript sessionScript) {
    this.sessionScript.setNewInstance(scriptDeserializer, resetScriptStates, sessionScript);
    reconfigured = true;
  }

  public void setIMEIGeneratorScript(final IScriptDeserializer scriptDeserializer, final boolean resetScriptStates, final IMEIGeneratorScript imeiGeneratorScript) {
    this.imeiGeneratorScript.setNewInstance(scriptDeserializer, resetScriptStates, imeiGeneratorScript);
    reconfigured = true;
  }

  public ChannelUID getChannelUID() {
    return channelUID;
  }

  public ISimChannelManager getSimChannelManager() {
    return simChannelManager;
  }

  // TODO: check to refactor
  public void logFound() {
    if (simpleConfigEditDAO.ensureSIMUIDIsPresent(getUID())) {
      firstInserting = true;
      simHistoryLogger.handleEvent(SimEvent.simAdded(getUID()));
      simpleConfigEditDAO.updatePhoneNumber(getUID(), getSimData().getPhoneNumber());
    }
    simHistoryLogger.handleEvent(SimEvent.simFound(getUID(), getChannelUID()));
  }

  public void unplug() {
    if (plugged) {
      plugged = false;

      logSimStatus(true);
      if (getUID() != null) {
        simHistoryLogger.handleEvent(SimEvent.simLost(getUID()));
      }
    }
  }

  public synchronized boolean isReadyToStartActivity() {
    try {
      return activityScript.takeScriptsForUse()[0].isReadyToStartPeriod();
    } finally {
      activityScript.untakeScripts();
    }
  }

  public synchronized boolean shouldStopActivity() {
    try {
      return activityScript.takeScriptsForUse()[0].shouldStopPeriod();
    } finally {
      activityScript.untakeScripts();
    }
  }

  public synchronized boolean shouldStopSession() {
    if (forceSessionEnd) {
      logger.debug("[{}] - ending session because it was forced", this);
      return true;
    }
    if (simData.isLocked()) {
      logger.debug("[{}] - ending session because card is locked", this);
      return true;
    }
    try {
      if (sessionScript.takeScriptsForUse()[0].shouldStopPeriod()) {
        logger.debug("[{}] - ending session because script asked to do so", this);
        return true;
      }
    } finally {
      sessionScript.untakeScripts();
    }
    return false;
  }

  public synchronized void gsmRegistration(final int attemptNumber) {
    registration = true;
    simHistoryLogger.handleEvent(SimEvent.gsmRegistration(getUID(), attemptNumber));
  }

  public synchronized void gsmRegistered() {
    registration = false;
    simHistoryLogger.handleEvent(SimEvent.gsmRegistered(getUID(), simData.getSimGroup().getName(), currentGSMGroup.getName()));
  }

  public synchronized void setLocked(final boolean lockStatus, final String reason) {
    logger.debug("[{}] - set lock status to {} for {}", this, lockStatus, channelUID);
    simData.setLocked(lockStatus);
    if (lockStatus) {
      simData.setLockReason(reason).setLockTime(System.currentTimeMillis());
      simActivityLogger.logSimLocked(getUID(), reason);
      setIndicationMode(IndicationMode.BUSINESS_PROBLEM);
    } else {
      simData.setLockReason(reason).setLockTime(0);
      simActivityLogger.logSimUnlocked(getUID(), reason);
      setIndicationMode(IndicationMode.READY);
    }

    reconfigured = true;
    logSimStatus(true);
  }

  public void resetStatistic() {
    logger.debug("[{}] - reset statistic for {}", this, channelUID);
    simData.setCallDuration(0);
    simData.setIncomingCallDuration(0);
    simData.setIncomingSuccessfulCallsCount(0);
    simData.setIncomingTotalCallsCount(0);
    simData.setSuccessfulCallsCount(0);
    simData.setSuccessOutgoingSmsCount(0);
    simData.setTotalCallsCount(0);

    simData.setTotalOutgoingSmsCount(0);
    simData.setIncomingSMSCount(0);
    simData.setCountSmsStatuses(0);

    simActivityLogger.resetStatistic(getUID());
    simHistoryLogger.handleEvent(SimEvent.resetStatistic(getUID()));
  }

  public void setNote(final String note) {
    logger.debug("[{}] - set note to {} for {}", this, note, channelUID);
    simData.setNote(note);
    simActivityLogger.logSimNote(getUID(), note);
  }

  public void setTariffPlanEndDate(final long endDate) {
    simData.setTariffPlanEndDate(endDate);
    simActivityLogger.logTariffPlanEndDate(getUID(), endDate);
    simHistoryLogger.handleEvent(SimEvent.setTariffPlanEndDate(getUID(), endDate));
  }

  public void setBalance(final double balance) {
    simData.setBalance(balance);
    simActivityLogger.logBalance(getUID(), balance);
  }

  public void setIndicationMode(final IndicationMode indicationMode) {
    logger.info("[{}] - set indication mode [{}] for {}", this, indicationMode, channelUID);
    try {
      simChannelManager.setIndicationMode(channelUID, indicationMode);
    } catch (RemoteException | RuntimeException e) {
      logger.warn("[{}] - can't set user indication {} for {}", this, indicationMode, channelUID, e);
    }
  }

  public synchronized void setEnabled(final boolean enableStatus) {
    simData.setEnabled(enableStatus);
    if (enableStatus) {
      simActivityLogger.logSimEnabled(getUID());
    } else {
      simActivityLogger.logSimDisabled(getUID());
    }
    reconfigured = true;
  }

  public synchronized void setPhoneNumber(final PhoneNumber number) {
    simData.setPhoneNumber(number);
    reconfigured = true;
    simpleConfigEditDAO.updatePhoneNumber(getUID(), number);
  }

  public synchronized void handleCallEnd(final long callDuration) {
    simData.setCallDuration(simData.getCallDuration() + callDuration);
    simData.setTotalCallsCount(simData.getTotalCallsCount() + 1);
    if (callDuration > 0) {
      simData.setSuccessfulCallsCount(simData.getSuccessfulCallsCount() + 1);
    }

    simRuntimeData.setLastCallDuration(callDuration);
    simActivityLogger.logCallEnded(getUID(), callDuration);
    reconfigured = true;
    logSimStatus(true);
  }

  public synchronized void handleIncomingCallEnd(final long callDuration) {
    simData.setIncomingCallDuration(simData.getIncomingCallDuration() + callDuration);
    simData.setIncomingTotalCallsCount(simData.getIncomingTotalCallsCount() + 1);
    if (callDuration > 0) {
      simData.setIncomingSuccessfulCallsCount(simData.getIncomingSuccessfulCallsCount() + 1);
    }

    simActivityLogger.logIncomingCallEnded(getUID(), callDuration);
    reconfigured = true;
    logSimStatus(true);
  }

  public synchronized void handleSendSMS(final int parts) {
    simData.setSuccessOutgoingSmsCount(simData.getSuccessOutgoingSmsCount() + parts);
    simData.setTotalOutgoingSmsCount(simData.getTotalOutgoingSmsCount() + parts);
    simActivityLogger.logSentSMS(getUID(), parts, parts);
    reconfigured = true;
    logSimStatus(true);
  }

  public void handleFailSendSMS(final int totalSmsParts, final int successSendSmsParts) {
    simData.setTotalOutgoingSmsCount(simData.getTotalOutgoingSmsCount() + totalSmsParts);
    simData.setSuccessOutgoingSmsCount(simData.getSuccessOutgoingSmsCount() + successSendSmsParts);
    simActivityLogger.logSentSMS(getUID(), totalSmsParts, successSendSmsParts);
    reconfigured = true;
    logSimStatus(true);
  }

  public void handleSMSStatus() {
    simData.incCountSmsStatuses();
    simActivityLogger.logSMSStatus(getUID());
    reconfigured = true;
    logSimStatus(true);
  }

  public synchronized void handleIncomingSMS(final int parts) {
    simData.setIncomingSMSCount(simData.getIncomingSMSCount() + parts);
    simActivityLogger.logIncomingSMS(getUID(), parts);
    reconfigured = true;
    logSimStatus(true);
  }

  public void handleAllowedInternet(final boolean allowed) {
    simHistoryLogger.handleEvent(SimEvent.allowedInternetChanged(getUID(), allowed));
    simData.setAllowInternet(allowed);
    simActivityLogger.logAllowedInternet(getUID(), allowed);
    reconfigured = true;
    logSimStatus(true);
  }

  public synchronized boolean isReconfigured() {
    return reconfigured;
  }

  public synchronized void clearReconfiguredFlag() {
    reconfigured = false;
  }

  // TODO: review sync and updating sim data
  public synchronized SimData getSimData() {
    return simData;
  }

  public synchronized void setSimData(final SimData simData) {
    this.simData = simData;
    reconfigured = true;
  }

  public SimRuntimeData getSimRuntimeData() {
    return simRuntimeData;
  }

  @Override
  public String toString() {
    return channelUID.toString();
  }

  public void setCHVState(final CHVState chvState) {
    this.chvState = chvState;
  }

  public String getLastStatus() {
    return lastStatus;
  }

  public synchronized String getStatus() {
    return lastStatus = determineStatus();
  }

  private String determineStatus() {
    if (!plugged) {
      return "unplugged";
    }
    if (chvState != null) {
      switch (chvState) {
        case REQUIRES_CHV:
          return "REQUIRES PIN";
        case WRONG_CHV:
          return "WRONG PIN";
      }
    }

    if (getSIMGroup() instanceof SimNoGroup) {
      return "NO GROUP";
    }

    if (simData.isLocked()) {
      return "locked";
    }

    if (taken) {
      if (active) {
        return getActiveStatus();
      } else {
        return getInactiveStatus();
      }
    }
    if (isReadyToStartSession()) {
      if (!isReadyIMEIGenerator()) {
        return "NO IMEI GENERATOR SCRIPT";
      }

      return getReadyStatus();
    }
    return getWaitingStatus();
  }

  private String getWaitingStatus() {
    try {
      ActivityPeriodScript script = sessionScript.takeScriptsForUse()[0];
      if (script instanceof DefaultActivityPeriodScript) {
        return "NO SESSION SCRIPT";
      }

      StringBuilder retval = new StringBuilder("waiting");
      Prediction sessionStart;
      sessionStart = script.predictPeriodStart();

      if (sessionStart != null) {
        retval.append(" until: ");
        retval.append(sessionStart.toLocalizedString());
      }
      return retval.toString();
    } finally {
      sessionScript.untakeScripts();
    }
  }

  private String getReadyStatus() {
    StringBuilder retval = new StringBuilder("ready");
    try {
      Prediction gw = gatewayScript.takeScriptsForUse()[0].predictNextGateway();
      retval.append(" (");
      retval.append(gw.toLocalizedString());
      retval.append(')');
      return retval.toString();
    } finally {
      gatewayScript.untakeScripts();
    }
  }

  private String getInactiveStatus() {
    StringBuilder retval = new StringBuilder("inactive");
    if (registration) {
      retval.append(" until: registration");
      return retval.toString();
    }

    Prediction activityStart = null;
    Prediction sessionEnd = null;
    try {
      activityStart = activityScript.takeScriptsForUse()[0].predictPeriodStart();
    } finally {
      activityScript.untakeScripts();
    }
    try {
      sessionEnd = sessionScript.takeScriptsForUse()[0].predictPeriodStop();
    } finally {
      sessionScript.untakeScripts();
    }

    if (activityStart != null) {
      retval.append(" until: ");
      retval.append(activityStart.toLocalizedString());
    }
    if (sessionEnd != null) {
      retval.append(". Session finishes: ");
      retval.append(sessionEnd.toLocalizedString());
    }
    return retval.toString();
  }

  private String getActiveStatus() {
    StringBuilder retval = new StringBuilder("active");
    Prediction activityEnd = null;
    Prediction sessionEnd = null;

    try {
      activityEnd = activityScript.takeScriptsForUse()[0].predictPeriodStop();
    } finally {
      activityScript.untakeScripts();
    }
    try {
      sessionEnd = sessionScript.takeScriptsForUse()[0].predictPeriodStop();
    } finally {
      sessionScript.untakeScripts();
    }

    if (activityEnd != null || sessionEnd != null) {
      retval.append(" until: ");
    }

    if (activityEnd != null && sessionEnd != null) {
      retval.append(activityEnd.or(sessionEnd).toLocalizedString());
    } else if (activityEnd != null) {
      retval.append(activityEnd.toLocalizedString());
    } else if (sessionEnd != null) {
      retval.append(sessionEnd.toLocalizedString());
    }
    return retval.toString();
  }

  public boolean isPinEnabled() {
    return chvState != CHVState.NO_CHV;
  }

  private void fireSessionStarted(final ScriptHolder<?> script, final IServerData gateway) {
    try {
      Object s = script.takeScriptsForUse()[0];
      if (s instanceof SessionListener) {
        ((SessionListener) s).handleSessionStarted(gateway);
      }
    } finally {
      script.untakeScripts();
    }
  }

  private void fireSessionEnded(final ScriptHolder<?> script) {
    try {
      Object s = script.takeScriptsForUse()[0];
      if (s instanceof SessionListener) {
        ((SessionListener) s).handleSessionEnded();
      }
    } finally {
      script.untakeScripts();
    }
  }

  private void fireActivityEnded(final ScriptHolder<?> script) {
    try {
      Object s = script.takeScriptsForUse()[0];
      if (s instanceof ActivityListener) {
        ((ActivityListener) s).handleActivityEnded();
      }
    } finally {
      script.untakeScripts();
    }
  }

  private void fireActivityStarted(final ScriptHolder<?> script, final GSMGroup gsmGroup) {
    try {
      Object s = script.takeScriptsForUse()[0];
      if (s instanceof ActivityListener) {
        ((ActivityListener) s).handleActivityStarted(gsmGroup);
      }
    } finally {
      script.untakeScripts();
    }
  }

  private void fireGenericEvent(final ScriptHolder<?> script, final String event, final Serializable... args) {
    try {
      Object s = script.takeScriptsForUse()[0];
      if (s instanceof GenericEventListener) {
        ((GenericEventListener) s).handleGenericEvent(event, args);
      }
    } finally {
      script.untakeScripts();
    }
  }

  public void logSimStatus(final boolean urgent) {
    loggerStatusHelper.logStatus(urgent);
  }

  private class LogStatusHelper {

    private volatile long lastLogTime;

    public void logStatus(final boolean urgent) {
      String lastStatusLocal = getLastStatus();
      String status = getStatus();
      if (!status.equals(lastStatusLocal)) {
        simRuntimeData.setStatus(status).setStatusTime(System.currentTimeMillis());
        if (urgent || System.currentTimeMillis() - lastLogTime > 5 * 60 * 1000) {
          simHistoryLogger.handleEvent(SimEvent.simStatusChanged(getUID(), status));
          if (!urgent) {
            lastLogTime = System.currentTimeMillis();
          }
        }
      }
    }

  }

  public boolean hasSessionScript() {
    return sessionScript.hasScript();
  }

  public boolean hasGatewaySelectionScript() {
    return gatewayScript.hasScript();
  }

  public boolean hasFactorScript() {
    return factorScript.hasScript();
  }

  public boolean hasActivityScript() {
    return activityScript.hasScript();
  }

  public boolean hasIMEIGeneratorScript() {
    return imeiGeneratorScript.hasScript();
  }

  public void clearResetSsScriptsFlag() {
    simHistoryLogger.handleEvent(SimEvent.resetScripts(getUID()));
    simpleConfigEditDAO.updateResetSsScriptsFlag(getUID(), false);
  }

  public SIMGroup changeGroup(final long simGroupID, final ISimUnitScriptUpdater simUnitScriptUpdater) throws DataSelectionException, DataModificationException {
    SIMGroup simGroup = configViewDAO.getSimGroup(simGroupID);
    if (simData.getSimGroup().getID() == simGroupID) {
      return simGroup;
    }

    simpleConfigEditDAO.changeSimGroup(getUID(), simGroupID);

    simUnitScriptUpdater.updateScripts(this, simGroup, false);

    simData.setSimGroup(simGroup);
    simHistoryLogger.handleEvent(SimEvent.changeSimGroup(getUID(), new String[] {simGroup.getName(), String.valueOf(simGroup.getRevision())}));
    return simGroup;
  }

  public boolean shouldGenerateIMEI() {
    try {
      return imeiGeneratorScript.takeScriptsForUse()[0].shouldGenerateIMEI() || !simpleConfigEditDAO.isIMEIPresent(getUID());
    } finally {
      imeiGeneratorScript.untakeScripts();
    }
  }

  public IMEI generateIMEI() {
    IMEI imei;
    try {
      imei = imeiGeneratorScript.takeScriptsForUse()[0].generateIMEI();
    } catch (final IMEIException e) {
      logger.warn("[{}] - fail on generate IMEI for sim [{}]", this, getUID(), e);
      setLocked(true, "Fail on generate IMEI: " + e.getMessage());
      simHistoryLogger.handleEvent(SimEvent.simCardLocked(getUID(), "Fail on generate IMEI: " + e.getMessage()));
      return null;
    } finally {
      imeiGeneratorScript.untakeScripts();
    }

    if (imei != null) {
      simpleConfigEditDAO.updateIMEI(getUID(), imei);
      logger.debug("[{}] - {} was updated for sim [{}]", this, imei, getUID());
      simData.setImei(imei);
      simHistoryLogger.handleEvent(SimEvent.imeiGenerated(getUID(), imei));
    }
    return imei;
  }

  public void resetIMEI(final String message) {
    logger.debug("[{}] - reset IMEI for {} executed", this, getUID());
    simpleConfigEditDAO.updateIMEI(getUID(), null);
    simData.setImei(null);
    simHistoryLogger.handleEvent(SimEvent.resetIMEI(getUID(), message));
    logger.debug("[{}] - reset imei for sim [{}] finished", this, getUID());
  }

}
