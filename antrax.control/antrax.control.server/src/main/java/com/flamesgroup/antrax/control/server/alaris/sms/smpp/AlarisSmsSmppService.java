/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.smpp;

import com.flamesgroup.antrax.control.properties.AlarisMultiUser;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.alaris.sms.AlarisSendSmsService;
import com.flamesgroup.antrax.control.server.alaris.sms.FakeSmsStatusStatistic;
import com.flamesgroup.antrax.control.server.alaris.sms.IAlarisSendSmsHandler;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.dao.IAlarisSmppSmsDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AlarisSmsSmppService {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsSmppService.class);

  private static final EnumSet<AlarisSms.Status> DLR_STATUS = EnumSet.of(
      AlarisSms.Status.DELIVRD, AlarisSms.Status.EXPIRED, AlarisSms.Status.UNDELIV, AlarisSms.Status.DELETED, AlarisSms.Status.UNKNOWN, AlarisSms.Status.REJECTD, AlarisSms.Status.ACCEPTD
  );

  private final IAlarisSmppSmsDAO alarisSmsDAO;
  private final AlarisSendSmsService alarisSendSmsService;
  private final AlarisDlrSmppService alarisDlrSmppService;

  private final AlarisSendSmsHandler alarisSendSmsHandler = new AlarisSendSmsHandler();

  private final ThreadPoolExecutor threadPoolExecutor = new WaitThreadPoolExecutor(5, 100, 30, TimeUnit.SECONDS, new SynchronousQueue<>(), r -> new Thread(r, "SmppDlrExecutor"));
  private final Map<String, FakeSmsStatusStatistic> fakeSmsStatusStatistics = new ConcurrentHashMap<>();

  private Timer timer;

  public AlarisSmsSmppService(final IAlarisSmppSmsDAO alarisSmsDAO, final AlarisSendSmsService alarisSendSmsService, final AlarisDlrSmppService alarisDlrSmppService) {
    this.alarisSmsDAO = alarisSmsDAO;
    this.alarisSendSmsService = alarisSendSmsService;
    this.alarisDlrSmppService = alarisDlrSmppService;
  }

  public void start() {
    alarisSendSmsService.start();

    int resentAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppResentAmount();
    int resentErrorAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppResentErrorAmount();
    alarisSmsDAO.listSmsAtRouting().forEach(sms -> alarisSendSmsService.sendAlarisSms(sms, alarisSendSmsHandler, resentAmount, resentErrorAmount));
    alarisSmsDAO.listSmsDlr(DLR_STATUS).forEach(sms -> threadPoolExecutor.execute(() -> sendDlr(sms)));

    timer = new Timer();
    timer.scheduleAtFixedRate(new ResendDlrTask(), TimeUnit.MINUTES.toMillis(5), TimeUnit.MINUTES.toMillis(5));
  }

  public void stop() {
    threadPoolExecutor.shutdown();
    timer.cancel();
    timer.purge();

    alarisSendSmsService.stop();
  }

  public void sendAlarisSms(final AlarisSmppSms alarisSms) {
    alarisSms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.ENROUTE);
    try {
      alarisSmsDAO.insertSms(alarisSms);
    } catch (DataModificationException e) {
      throw new AlarisSmsSmppException("NO ROUTES", e);
    }

    int resentAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppResentAmount();
    int resentErrorAmount = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppResentErrorAmount();
    alarisSendSmsService.sendAlarisSms(alarisSms, alarisSendSmsHandler, resentAmount, resentErrorAmount);
  }

  public AlarisSmppSms.Status getAlarisSmsStatus(final UUID smsId) {
    AlarisSmppSms alarisSms = alarisSmsDAO.getById(smsId);
    if (alarisSms == null) {
      return null;
    } else {
      return alarisSms.getStatus();
    }
  }

  public void sendAlarisSmsDlr(final AlarisSmppSms alarisSms) {
    if (alarisSms.getDlrTime() != null) {
      logger.debug("[{}] - receive real status, but already sent fake status for alarisSms [{}]", this, alarisSms);
      return;
    }

    try {
      alarisSmsDAO.updateSms(alarisSms);
    } catch (DataModificationException e) {
      logger.warn("[{}] - can't update status of alaris sms: [{}]", this, alarisSms, e);
    }

    if (alarisSms.getRegisteredDelivery() > 0 && DLR_STATUS.contains(alarisSms.getStatus())) {
      threadPoolExecutor.execute(() -> sendDlr(alarisSms));
    }
  }

  private void sendDlr(final AlarisSmppSms alarisSms) {
    if (alarisDlrSmppService.sendDlr(alarisSms)) {
      alarisSms.setDlrTime(new Date());
      try {
        alarisSmsDAO.updateSms(alarisSms);
      } catch (DataModificationException e) {
        logger.warn("[{}] - can't update dlr time of alaris sms: [{}]", this, alarisSms, e);
      }
    }
  }

  private class AlarisSendSmsHandler implements IAlarisSendSmsHandler {

   @Override
    public void handleReject(final List<AlarisSms> alarisSmses) {
     alarisSmses.forEach(sms -> sendAlarisSmsDlr((AlarisSmppSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.REJECTD)));
    }

    @Override
    public void handleSending(final List<AlarisSms> alarisSmses) {
    }

    @Override
    public void handleSent(final List<AlarisSms> alarisSmses, final UUID smsId) {
      alarisSmses.forEach(sms -> {
        sendAlarisSmsDlr((AlarisSmppSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.SENT).setSmsId(smsId));

        FakeSmsStatusStatistic fakeSmsStatusStatistic = fakeSmsStatusStatistics.computeIfAbsent(sms.getUsername(), s -> new FakeSmsStatusStatistic());
        fakeSmsStatusStatistic.incTotalSmsStatus();

        AlarisMultiUser alarisMultiUser = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsSmppServerMultiUser().get(sms.getUsername());
        int res = (fakeSmsStatusStatistic.getTotalSmsStatus() * alarisMultiUser.getPercentageRealStatus()) / 100;
        if (res < fakeSmsStatusStatistic.getTotalRealSmsStatus()) {
          logger.debug("[{}] - return fake status [DELIVRD] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, sms.getUsername(), sms.getId(), fakeSmsStatusStatistic);
          sendAlarisSmsDlr((AlarisSmppSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.DELIVRD));
        } else {
          fakeSmsStatusStatistic.incTotalRealSmsStatus();
          logger.debug("[{}] - waiting real status [{}] for username: [{}], smsId: [{}], fakeSmsStatusStatistic: [{}]", this, sms.getUsername(), sms.getId(), fakeSmsStatusStatistic);
        }
      });
    }

    @Override
    public void handleUndeliv(final List<AlarisSms> alarisSmses) {
      alarisSmses.forEach(sms -> sendAlarisSmsDlr((AlarisSmppSms) sms.setLastUpdateTime(new Date()).setStatus(AlarisHttpSms.Status.UNDELIV)));
    }

    @Override
    public void handleFinalize(final List<AlarisSms> alarisSmses) {
    }

  }

  private class ResendDlrTask extends TimerTask {

    @Override
    public void run() {
      alarisSmsDAO.listSmsDlr(DLR_STATUS).forEach(sms -> threadPoolExecutor.execute(() -> sendDlr(sms)));
    }
  }

}
