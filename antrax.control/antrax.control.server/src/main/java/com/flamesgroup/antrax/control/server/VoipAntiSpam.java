/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.utils.VoipAntiSpamHelper;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.CdrAnalyzeNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.ListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.antrax.storage.dao.ICallDAO;
import com.flamesgroup.antrax.storage.dao.IVoipAntiSpamDAO;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.AsrConfig;
import com.flamesgroup.commons.voipantispam.BlackListConfig;
import com.flamesgroup.commons.voipantispam.CdrConfig;
import com.flamesgroup.commons.voipantispam.CdrFilterRule;
import com.flamesgroup.commons.voipantispam.FasConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

// TODO: need refactoring to remove duplicate code
public class VoipAntiSpam {

  private static final Logger logger = LoggerFactory.getLogger(VoipAntiSpam.class);

  private final IVoipAntiSpamDAO voipAntiSpamDAO;
  private final ICallDAO callDAO;

  public VoipAntiSpam(final DAOProvider daoProvider) {
    Objects.requireNonNull(daoProvider, "daoProvider mustn't be null");
    this.voipAntiSpamDAO = daoProvider.getVoipAntiSpamDAO();
    this.callDAO = daoProvider.getCallDAO();
  }

  public VoipAntiSpamAnalyzeResult isValidNumber(final String caller, final String called) {
    try {
      return checkValidNumber(caller, called);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check number", this, e);
      return VoipAntiSpamAnalyzeResult.ANALYZE_ERROR;
    }
  }

  private VoipAntiSpamAnalyzeResult checkValidNumber(final String caller, final String called) throws DataModificationException {
    Objects.requireNonNull(called, "number mustn't be null");
    final long currentTime = System.currentTimeMillis();
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
      logger.trace("[{}] - caller [{}] and called [{}] is valid: VoIp AntiSpam is disable", this, caller, called);
      return VoipAntiSpamAnalyzeResult.NORMAL_NUMBER;
    }

    logger.debug("[{}] - try to check for valid caller [{}] and called [{}], currentTime [{}]", this, caller, called, currentTime);
    voipAntiSpamDAO.insertHistoryRoutingRequest(caller, called, currentTime);

    if (caller != null && voipAntiSpamDAO.incrementRoutingRequestWhiteNumber(VoipAntiSpamNumberType.CALLER, caller)) {
      logger.debug("[{}] - caller [{}] is valid: Number at white list", this, caller);
      return VoipAntiSpamAnalyzeResult.WHITE_NUMBER;
    } else if (voipAntiSpamDAO.incrementRoutingRequestWhiteNumber(VoipAntiSpamNumberType.CALLED, called)) {
      logger.debug("[{}] - called [{}] is valid: Number at white list", this, caller);
      return VoipAntiSpamAnalyzeResult.WHITE_NUMBER;
    }

    int routingCount;
    if (caller != null && voipAntiSpamDAO.incrementRoutingRequestBlackNumber(VoipAntiSpamNumberType.CALLER, caller)) {
      logger.debug("[{}] - caller [{}] isn't valid: Number at black list", this, caller);
      return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
    } else if (voipAntiSpamDAO.incrementRoutingRequestBlackNumber(VoipAntiSpamNumberType.CALLED, called)) {
      logger.debug("[{}] - called [{}] isn't valid: Number at black list", this, caller);
      return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
    } else {
      for (BlackListConfig blackListConfig : voipAntiSpamConfiguration.getBlackListConfigs()) {
        VoipAntiSpamNumberType numberType = blackListConfig.getNumberType();
        if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
          continue;
        }

        String number;
        long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(blackListConfig.getPeriod());
        if (numberType == VoipAntiSpamNumberType.CALLER) {
          number = caller;
          if (blackListConfig.isCallToTheSameCalledNumbers()) {
            routingCount = voipAntiSpamDAO.getRoutingCount(caller, called, inPeriod);
          } else {
            routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);
          }
        } else {
          number = called;
          routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);
        }

        if (routingCount > blackListConfig.getMaxRoutingRequestPerPeriod()) {
          voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numberType, Collections.singleton(number));
          BlackListNumber blackListNumber = new BlackListNumber(numberType, number,
              VoipAntiSpamListNumbersStatus.ROUTING, VoipAntiSpamHelper.createDescriptionForBlackListRoutingFilter(blackListConfig), 0, new Date());
          voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(Collections.singleton(blackListNumber));
          logger.debug("[{}] - number [{}] isn't valid: Number routingCount [{}] > BlackListMaxRoutingRequestPerPeriod [{}] then inserted blackListNumber {}", this, called, routingCount,
              blackListConfig.getMaxRoutingRequestPerPeriod(), blackListNumber);
          return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
        }
      }
    }

    GrayListNumber callerGrayListNumber = caller == null ? null : voipAntiSpamDAO.incrementRoutingRequestGrayNumber(VoipAntiSpamNumberType.CALLER, caller);
    GrayListNumber calledGrayListNumber = voipAntiSpamDAO.incrementRoutingRequestGrayNumber(VoipAntiSpamNumberType.CALLED, called);
    for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
      VoipAntiSpamNumberType numberType = grayListConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      String number;
      GrayListNumber grayListNumber;
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(grayListConfig.getPeriod());
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
        grayListNumber = callerGrayListNumber;
        if (grayListConfig.isCallToTheSameCalledNumbers()) {
          routingCount = voipAntiSpamDAO.getRoutingCount(caller, called, inPeriod);
        } else {
          routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);
        }
      } else {
        number = called;
        grayListNumber = calledGrayListNumber;
        routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);
      }

      int blockCount = 0;
      int routingRequestCount = 0;
      long blockTimeLeftGrayList = -1;
      if (grayListNumber != null) {
        blockCount = grayListNumber.getBlockCount();
        blockTimeLeftGrayList = grayListNumber.getBlockTimeLeft().getTime();
        routingRequestCount = grayListNumber.getRoutingRequestCount();
      }

      boolean isBanned = false;
      if (blockTimeLeftGrayList > currentTime) {
        isBanned = true;
      }

      if (routingCount > grayListConfig.getMaxRoutingRequestPerPeriod()) {
        long blockTimeLeft = currentTime + TimeUnit.MINUTES.toMillis(grayListConfig.getBlockPeriod());
        voipAntiSpamDAO.insertHistoryBlockNumbers(numberType, number, blockTimeLeft);

        if (!isBanned) {
          blockCount++;
        }

        if (blockCount > grayListConfig.getMaxBlockCountBeforeMoveToBlackList()) {
          voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numberType, Collections.singleton(number));
          BlackListNumber blackListNumber = new BlackListNumber(numberType, number,
              VoipAntiSpamListNumbersStatus.ROUTING, VoipAntiSpamHelper.createDescriptionForBlackListBlockCountFilter(grayListConfig), 0, new Date());
          voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(Collections.singleton(blackListNumber));
          logger.debug("[{}] - number [{}] isn't valid: Number blockCount [{}] > BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted blackListNumber {}", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), blackListNumber);
          return VoipAntiSpamAnalyzeResult.BLACK_NUMBER;
        } else {
          if (grayListNumber == null) {
            voipAntiSpamDAO.deleteVoipAntiSpamWhiteListNumbers(numberType, Collections.singleton(number));
          }
          Set<GrayListNumber> grayListNumbers = new HashSet<>();
          grayListNumbers.add(new GrayListNumber(numberType, number, VoipAntiSpamListNumbersStatus.ROUTING, VoipAntiSpamHelper.createDescriptionForGrayListRoutingFilter(grayListConfig),
              routingRequestCount, new Date(currentTime), blockCount, new Date(blockTimeLeft)));
          voipAntiSpamDAO.insertVoipAntiSpamGrayListNumbers(grayListNumbers);
          logger.debug("[{}] - number [{}] isn't valid: Number blockCount [{}] <= BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted grayListNumbers {}", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), grayListNumbers);
          return VoipAntiSpamAnalyzeResult.GRAY_NUMBER;
        }
      }
    }
    return VoipAntiSpamAnalyzeResult.NORMAL_NUMBER;
  }

  public void analyzeAcd(final CDR cdr) {
    try {
      checkAcd(cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check acd", this, e);
    }
  }

  private void checkAcd(final CDR cdr) throws DataModificationException {
    String caller = cdr.getCallerPhoneNumber().isPrivate() ? null : cdr.getCallerPhoneNumber().getValue();
    String called = cdr.getCalledPhoneNumber().getValue();
    long duration = cdr.getDuration();

    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
      logger.debug("[{}] - analyze acd disable for caller [{}] and called [{}]", this, caller, called);
      return;
    }

    if (cdr.getDuration() <= 0) {
      logger.debug("[{}] - analyze acd: skip zero duration for caller [{}] and called [{}]", this, caller, called);
      return;
    }

    final long currentTime = System.currentTimeMillis();
    logger.debug("[{}] - try to analyze acd for caller [{}] and called [{}], duration [{}], currentTime [{}]", this, caller, called, currentTime);

    if (caller != null && voipAntiSpamDAO.getBlackListNumber(VoipAntiSpamNumberType.CALLER, caller) != null) {
      logger.debug("[{}] - analyze acd: caller [{}] at black list", this, caller);
      return;
    }
    if (voipAntiSpamDAO.getBlackListNumber(VoipAntiSpamNumberType.CALLED, called) != null) {
      logger.debug("[{}] - analyze acd: called [{}] at black list", this, called);
      return;
    }

    for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
      AcdConfig acdConfig = grayListConfig.getAcdConfig();
      if (acdConfig == null) {
        continue;
      }

      VoipAntiSpamNumberType numberType = grayListConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      String number;
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
      } else {
        number = called;
      }

      long minACD = TimeUnit.SECONDS.toMillis(acdConfig.getMinAcd());
      if (duration < minACD) {
        long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(acdConfig.getPeriod());
        int cdrCount = callDAO.getCountNumbersInPeriod(number, minACD, inPeriod);
        if (cdrCount >= acdConfig.getMaxMinAcdCallPerPeriod()) {
          int blockCount = 0;
          long blockTimeLeftGrayList = 0;
          int routingRequestCount = 0;

          GrayListNumber grayListNumber = voipAntiSpamDAO.getGrayListNumber(numberType, number);
          if (grayListNumber != null) {
            blockCount = grayListNumber.getBlockCount();
            blockTimeLeftGrayList = grayListNumber.getBlockTimeLeft().getTime();
            routingRequestCount = grayListNumber.getRoutingRequestCount();
          }

          if (blockTimeLeftGrayList < currentTime) {
            blockCount++;
            voipAntiSpamDAO.insertHistoryBlockNumbers(numberType, number, currentTime);
          }

          if (blockCount > grayListConfig.getMaxBlockCountBeforeMoveToBlackList()) {
            voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numberType, Collections.singleton(number));
            Set<BlackListNumber> blackListNumbers = new HashSet<>();
            blackListNumbers.add(new BlackListNumber(numberType, number, VoipAntiSpamListNumbersStatus.ACD,
                VoipAntiSpamHelper.createDescriptionForACDBlockCountFilter(acdConfig, grayListConfig.getMaxBlockCountBeforeMoveToBlackList()), routingRequestCount, new Date()));
            voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
            logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] > BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted blackListNumbers {} ", this, number, blockCount,
                grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), blackListNumbers);
            return;
          } else {
            long blockTimeLeft = currentTime + TimeUnit.MINUTES.toMillis(grayListConfig.getBlockPeriod());
            Set<GrayListNumber> grayListNumbers = new HashSet<>();
            grayListNumbers.add(new GrayListNumber(numberType, number, VoipAntiSpamListNumbersStatus.ACD, VoipAntiSpamHelper.createDescriptionForACDFilter(acdConfig), routingRequestCount,
                new Date(currentTime), blockCount, new Date(blockTimeLeft)));
            voipAntiSpamDAO.insertVoipAntiSpamGrayListNumbers(grayListNumbers);
            logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] <= BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted grayListNumbers {}", this, number, blockCount,
                grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), grayListNumbers);
            return;
          }
        }
      }
    }
  }

  public void analyzeAsr(final CDR cdr) {
    try {
      checkAsr(cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check acd", this, e);
    }
  }

  private void checkAsr(final CDR cdr) throws DataModificationException {
    String caller = cdr.getCallerPhoneNumber().isPrivate() ? null : cdr.getCallerPhoneNumber().getValue();
    String called = cdr.getCalledPhoneNumber().getValue();

    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable()) {
      logger.debug("[{}] - analyze asr disable for caller [{}] and called", this, caller, called);
      return;
    }

    final long currentTime = System.currentTimeMillis();
    logger.debug("[{}] - try to analyze asr for caller [{}] and called [{}], duration [{}], currentTime [{}]", this, caller, called, currentTime);

    if (caller != null && voipAntiSpamDAO.getBlackListNumber(VoipAntiSpamNumberType.CALLER, caller) != null) {
      logger.debug("[{}] - analyze asr: caller [{}] at black list", this, caller);
      return;
    }
    if (voipAntiSpamDAO.getBlackListNumber(VoipAntiSpamNumberType.CALLED, called) != null) {
      logger.debug("[{}] - analyze asr: called [{}] at black list", this, called);
      return;
    }

    for (GrayListConfig grayListConfig : voipAntiSpamConfiguration.getGrayListConfigs()) {
      AsrConfig asrConfig = grayListConfig.getAsrConfig();
      if (asrConfig == null) {
        continue;
      }

      VoipAntiSpamNumberType numberType = grayListConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      int minAsr = asrConfig.getMinAsr();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(asrConfig.getPeriod());

      String number;
      List<CDR> cdrs;
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
        cdrs = callDAO.getCDRByCallerNumberInPeriod(number, inPeriod);
      } else {
        number = called;
        cdrs = callDAO.getCDRByCalledNumberInPeriod(number, inPeriod);
      }

      long total = cdrs.size() + 1;
      long success = cdrs.stream().filter(c -> c.getStartTime() > 0).count() + cdr.getStartTime() > 0 ? 1 : 0;
      if (100 * success / total < minAsr) {
        int blockCount = 0;
        long blockTimeLeftGrayList = 0;
        int routingRequestCount = 0;

        GrayListNumber grayListNumber = voipAntiSpamDAO.getGrayListNumber(numberType, number);
        if (grayListNumber != null) {
          blockCount = grayListNumber.getBlockCount();
          blockTimeLeftGrayList = grayListNumber.getBlockTimeLeft().getTime();
          routingRequestCount = grayListNumber.getRoutingRequestCount();
        }

        if (blockTimeLeftGrayList < currentTime) {
          blockCount++;
          voipAntiSpamDAO.insertHistoryBlockNumbers(numberType, number, currentTime);
        }

        if (blockCount > grayListConfig.getMaxBlockCountBeforeMoveToBlackList()) {
          voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(numberType, Collections.singleton(number));
          Set<BlackListNumber> blackListNumbers = new HashSet<>();
          blackListNumbers.add(new BlackListNumber(numberType, number, VoipAntiSpamListNumbersStatus.ASR,
              VoipAntiSpamHelper.createDescriptionForASRBlockCountFilter(asrConfig, grayListConfig.getMaxBlockCountBeforeMoveToBlackList()), routingRequestCount, new Date()));
          voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
          logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] > BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted blackListNumbers {} ", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), blackListNumbers);
          return;
        } else {
          long blockTimeLeft = currentTime + TimeUnit.MINUTES.toMillis(grayListConfig.getBlockPeriod());
          Set<GrayListNumber> grayListNumbers = new HashSet<>();
          grayListNumbers.add(new GrayListNumber(numberType, number, VoipAntiSpamListNumbersStatus.ASR, VoipAntiSpamHelper.createDescriptionForASRFilter(asrConfig),
              routingRequestCount, new Date(currentTime), blockCount, new Date(blockTimeLeft)));
          voipAntiSpamDAO.insertVoipAntiSpamGrayListNumbers(grayListNumbers);
          logger.debug("[{}] - analyze acd: Number [{}] blockCount [{}] <= BlackListMaxBlockCountBeforeMoveToBlackList [{}] then inserted grayListNumbers {}", this, number, blockCount,
              grayListConfig.getMaxBlockCountBeforeMoveToBlackList(), grayListNumbers);
          return;
        }
      }
    }
  }

  public void analyzeCdr(final CdrConfig cdrConfig) {
    try {
      checkCdr(cdrConfig);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check cdr", this, e);
    }
  }

  private void checkCdr(final CdrConfig cdrConfig) throws DataModificationException {
    long time = System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(cdrConfig.getPeriod());
    logger.debug("[{}] - try analyze cdr type [{}], count [{}], time [{}]", this, cdrConfig.getNumberType(), cdrConfig.getMaxCallCount(), time);
    List<CdrAnalyzeNumber> numbers = voipAntiSpamDAO.getCdrAnalyzeNumbers(cdrConfig.getNumberType(), cdrConfig.getMaxCallCount(), time);
    if (numbers.isEmpty()) {
      logger.debug("[{}] - analyze cdr: numbers is empty", this);
      return;
    }

    switch (cdrConfig.getListType()) {
      case WHITE:
        Set<WhiteListNumber> whiteListNumbers = numbers.stream().filter(n -> filterCdrAnalyzeNumber(cdrConfig, n)).map(n -> new WhiteListNumber(
            cdrConfig.getNumberType(), n.getNumber().getValue(), VoipAntiSpamListNumbersStatus.CDR, VoipAntiSpamHelper.createDescriptionForCdrConfigFilter(cdrConfig), 0, new Date()
        )).collect(Collectors.toSet());
        if (whiteListNumbers.isEmpty()) {
          break;
        }
        voipAntiSpamDAO.insertVoipAntiSpamWhiteListNumbers(whiteListNumbers);
        voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(cdrConfig.getNumberType(), whiteListNumbers.stream().map(ListNumber::getNumber).collect(Collectors.toSet()));
        logger.debug("[{}] - analyze cdr: inserted whiteListNumbers {}", this, whiteListNumbers);
        break;
      case BLACK:
        Set<BlackListNumber> blackListNumbers = numbers.stream().filter(n -> filterCdrAnalyzeNumber(cdrConfig, n)).map(n -> new BlackListNumber(
            cdrConfig.getNumberType(), n.getNumber().getValue(), VoipAntiSpamListNumbersStatus.CDR, VoipAntiSpamHelper.createDescriptionForCdrConfigFilter(cdrConfig), 0, new Date()
        )).collect(Collectors.toSet());
        if (blackListNumbers.isEmpty()) {
          break;
        }
        voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(blackListNumbers);
        voipAntiSpamDAO.deleteVoipAntiSpamGrayListNumbers(cdrConfig.getNumberType(), blackListNumbers.stream().map(ListNumber::getNumber).collect(Collectors.toSet()));
        logger.debug("[{}] - analyze cdr: inserted blackListNumbers {}", this, blackListNumbers);
        break;
    }
  }

  private boolean filterCdrAnalyzeNumber(final CdrConfig cdrConfig, final CdrAnalyzeNumber cdrAnalyzeNumber) {
    CdrFilterRule cdrFilterRule = cdrConfig.getFilterRule();
    switch (cdrConfig.getListType()) {
      case WHITE:
        switch (cdrFilterRule.getRuleType()) {
          case ASR:
            return calculateAsr(cdrAnalyzeNumber) >= cdrFilterRule.getMinAsr();
          case ACD:
            return calculateAcd(cdrAnalyzeNumber) >= cdrFilterRule.getMinAcd();
          case ASR_AND_ACD:
            return calculateAsr(cdrAnalyzeNumber) >= cdrFilterRule.getMinAsr() && calculateAcd(cdrAnalyzeNumber) >= cdrFilterRule.getMinAcd();
        }
      case BLACK:
        switch (cdrFilterRule.getRuleType()) {
          case ASR:
            return calculateAsr(cdrAnalyzeNumber) <= cdrFilterRule.getMinAsr();
          case ACD:
            return calculateAcd(cdrAnalyzeNumber) <= cdrFilterRule.getMinAcd();
          case ASR_AND_ACD:
            return calculateAsr(cdrAnalyzeNumber) <= cdrFilterRule.getMinAsr() && calculateAcd(cdrAnalyzeNumber) <= cdrFilterRule.getMinAcd();
        }
    }
    return false;
  }

  private int calculateAsr(final CdrAnalyzeNumber cdrAnalyzeNumber) {
    return cdrAnalyzeNumber.getTotal() > 0 ? 100 * cdrAnalyzeNumber.getSuccess() / cdrAnalyzeNumber.getTotal() : 0;
  }

  private long calculateAcd(final CdrAnalyzeNumber cdrAnalyzeNumber) {
    return cdrAnalyzeNumber.getSuccess() > 0 ? cdrAnalyzeNumber.getDuration() / cdrAnalyzeNumber.getSuccess() : 0;
  }

  public void analyzeAlerting(final CDR cdr) {
    try {
      checkGsmAlerting(cdr);
      checkVoipAlerting(cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check alerting", this, e);
    }
  }

  private void checkGsmAlerting(final CDR cdr) throws DataModificationException {
    String caller = cdr.getCallerPhoneNumber().isPrivate() ? null : cdr.getCallerPhoneNumber().getValue();
    String called = cdr.getCalledPhoneNumber().getValue();

    logger.debug("[{}] - try analyze GSM alerting, cdr [{}] for caller [{}] and called [{}]", this, cdr, caller, called);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isGsmAlertingAnalyze()) {
      logger.debug("[{}] - analyze GSM alerting disable for caller [{}] and called [{}], voipAntiSpamConfiguration [{}]", this, caller, called, voipAntiSpamConfiguration);
      return;
    }

    if (cdr.getDropReason() != CdrDropReason.GSM) {
      logger.debug("[{}] - analyze GSM alerting, CdrDropReason: {} != GSM", this, cdr.getDropReason());
      return;
    }

    for (GsmAlertingConfig gsmAlertingConfig : voipAntiSpamConfiguration.getGsmAlertingConfigs()) {
      VoipAntiSpamNumberType numberType = gsmAlertingConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      String number;
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
      } else {
        number = called;
      }

      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(gsmAlertingConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);

      if (routingCount < gsmAlertingConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, gsmAlertingConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      if (cdr.getDropCode() == 17 && (cdr.getStopTime() - cdr.getSetupTime() - cdr.getPDD() < TimeUnit.SECONDS.toMillis(gsmAlertingConfig.getAlertingTime()))) {
        routingCount++;
        BlackListNumber blackListNumber = new BlackListNumber(numberType, number, VoipAntiSpamListNumbersStatus.GSM_ALERTING,
            VoipAntiSpamHelper.createDescriptionForGsmAlertingBlockCountFilter(gsmAlertingConfig), routingCount, new Date(currentTime));
        voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(Collections.singleton(blackListNumber));
        logger.debug("[{}] - analyze GSM alerting, add number: {} to black list", this, blackListNumber);
        return;
      }
    }
  }

  private void checkVoipAlerting(final CDR cdr) throws DataModificationException {
    String caller = cdr.getCallerPhoneNumber().isPrivate() ? null : cdr.getCallerPhoneNumber().getValue();
    String called = cdr.getCalledPhoneNumber().getValue();

    logger.debug("[{}] - try analyze VOIP alerting, cdr [{}] for caller [{}] and called [{}]", this, cdr, caller, called);
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isVoipAlertingAnalyze()) {
      logger.debug("[{}] - analyze VOIP alerting disable for caller [{}] and called [{}], voipAntiSpamConfiguration [{}]", this, caller, called, voipAntiSpamConfiguration);
      return;
    }

    if (cdr.getDropReason() != CdrDropReason.VOIP) {
      logger.debug("[{}] - analyze VOIP alerting, CdrDropReason: {} != VOIP", this, cdr.getDropReason());
      return;
    }

    for (VoipAlertingConfig voipAlertingConfig : voipAntiSpamConfiguration.getVoipAlertingConfigs()) {
      VoipAntiSpamNumberType numberType = voipAlertingConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      String number;
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
      } else {
        number = called;
      }

      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(voipAlertingConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);

      if (routingCount < voipAlertingConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, voipAlertingConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      if (cdr.getStopTime() - cdr.getSetupTime() - cdr.getPDD() < TimeUnit.SECONDS.toMillis(voipAlertingConfig.getAlertingTime())) {
        routingCount++;
        BlackListNumber blackListNumber = new BlackListNumber(numberType, number, VoipAntiSpamListNumbersStatus.VOIP_ALERTING,
            VoipAntiSpamHelper.createDescriptionForVoipAlertingBlockCountFilter(voipAlertingConfig), routingCount, new Date(currentTime));
        voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(Collections.singleton(blackListNumber));
        logger.debug("[{}] - analyze VOIP alerting, add number: {} to black list", this, blackListNumber);
        return;
      }
    }
  }

  public void analyzeFas(final CDR cdr) {
    try {
      checkFas(cdr);
    } catch (DataModificationException e) {
      logger.error("[{}] - error while check FAS", this, e);
    }
  }

  private void checkFas(final CDR cdr) throws DataModificationException {
    String caller = cdr.getCallerPhoneNumber().isPrivate() ? null : cdr.getCallerPhoneNumber().getValue();
    String called = cdr.getCalledPhoneNumber().getValue();

    logger.debug("[{}] - try analyze FAS, cdr [{}] for caller [{}] and called [{}]", this, cdr, caller, called);
    if (cdr.getDropReason() != CdrDropReason.LIMIT_FAS_ATTEMPTS) {
      return;
    }
    VoipAntiSpamConfiguration voipAntiSpamConfiguration = voipAntiSpamDAO.getVoipAntiSpamConfiguration();
    if (voipAntiSpamConfiguration == null || !voipAntiSpamConfiguration.isEnable() || !voipAntiSpamConfiguration.isFasAnalyze()) {
      logger.debug("[{}] - analyze FAS disable for caller [{}] and called [{}], voipAntiSpamConfiguration [{}]", this, caller, called, voipAntiSpamConfiguration);
      return;
    }

    for (FasConfig fasConfig : voipAntiSpamConfiguration.getFasConfigs()) {
      VoipAntiSpamNumberType numberType = fasConfig.getNumberType();
      if (caller == null && numberType == VoipAntiSpamNumberType.CALLER) {
        continue;
      }

      String number;
      if (numberType == VoipAntiSpamNumberType.CALLER) {
        number = caller;
      } else {
        number = called;
      }

      long currentTime = System.currentTimeMillis();
      long inPeriod = currentTime - TimeUnit.MINUTES.toMillis(fasConfig.getPeriod());
      int routingCount = voipAntiSpamDAO.getRoutingCount(numberType, number, inPeriod);

      if (routingCount < fasConfig.getMaxRoutingRequestPerPeriod()) {
        logger.debug("[{}] - routingCount [{}] of number [{}] less then configure [{}]", this, routingCount, number, fasConfig.getMaxRoutingRequestPerPeriod());
        continue;
      }

      routingCount++;
      BlackListNumber blackListNumber = new BlackListNumber(numberType, number, VoipAntiSpamListNumbersStatus.FAS, VoipAntiSpamHelper.createDescriptionForFasBlockCountFilter(fasConfig), routingCount,
          new Date(currentTime));
      voipAntiSpamDAO.insertVoipAntiSpamBlackListNumbers(Collections.singleton(blackListNumber));
      logger.debug("[{}] - analyze FAS, add number: {} to black list", this, blackListNumber);
      return;
    }
  }

}
