/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.smpp;

import static com.flamesgroup.antrax.control.server.alaris.sms.smpp.AlarisSmppSmsStatusMapper.mapToMessageState;

import com.cloudhopper.commons.util.windowing.WindowFuture;
import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.pdu.DeliverSm;
import com.cloudhopper.smpp.pdu.DeliverSmResp;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import com.cloudhopper.smpp.tlv.Tlv;
import com.cloudhopper.smpp.type.Address;
import com.cloudhopper.smpp.type.SmppInvalidArgumentException;
import com.cloudhopper.smpp.util.DeliveryReceipt;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class AlarisDlrSmppService {

  private static final Logger logger = LoggerFactory.getLogger(AlarisDlrSmppService.class);

  private final AlarisSmsSmppServer alarisSmsSmppServer;

  private final AtomicInteger sequenceNumberCounter = new AtomicInteger(1);

  public AlarisDlrSmppService(final AlarisSmsSmppServer alarisSmsSmppServer) {
    this.alarisSmsSmppServer = alarisSmsSmppServer;
  }

  public boolean sendDlr(final AlarisSmppSms alarisSms) {
    SmppSession smppSession = alarisSmsSmppServer.getSession(alarisSms.getUsername());
    if (smppSession == null) {
      return false;
    }
    if (smppSession.getConfiguration().getType() != SmppBindType.TRANSCEIVER) {
      return true;
    }

    WindowFuture<Integer, PduRequest, PduResponse> future;
    try {
      future = smppSession.sendRequestPdu(createDeliverySm(alarisSms), 10000, false);
    } catch (Exception e) {
      logger.error("[{}] - can't send dlr request for alarisSms {}", this, alarisSms, e);
      return false;
    }

    try {
      if (!future.await()) {
        logger.warn("[{}] - failed to receive response of dlr request within specified time for alarisSms {}", this, alarisSms);
        return false;
      }
    } catch (InterruptedException e) {
      return false;
    }

    if (!future.isSuccess()) {
      logger.warn("[{}] - failed to receive response [{}] of dlr request for alarisSms {}", this, future.getCause(), alarisSms);
      return false;
    }

    DeliverSmResp response = (DeliverSmResp) future.getResponse();
    logger.debug("[{}] - receive response [{}:{}] of dlr request for alarisSms {}", this, response.getCommandStatus(), response.getResultMessage(), alarisSms);
    return true;
  }

  private DeliverSm createDeliverySm(final AlarisSmppSms alarisSms) throws SmppInvalidArgumentException {
    DeliverSm deliverSm = new DeliverSm();
    deliverSm.setSequenceNumber(getSequenceNumber());
    deliverSm.setSourceAddress(new Address(alarisSms.getSourceTon().byteValue(), alarisSms.getSourceNpi().byteValue(), alarisSms.getSourceNumber()));
    deliverSm.setDestAddress(new Address(alarisSms.getDestTon().byteValue(), alarisSms.getDestNpi().byteValue(), alarisSms.getDestNumber()));
    deliverSm.setEsmClass(SmppConstants.ESM_CLASS_MT_SMSC_DELIVERY_RECEIPT);

    deliverSm.setProtocolId((byte) 0x00);
    deliverSm.setPriority((byte) 0x00);
    deliverSm.setScheduleDeliveryTime(null);
    deliverSm.setValidityPeriod(null);
    deliverSm.setRegisteredDelivery((byte) 0x00);
    deliverSm.setReplaceIfPresent((byte) 0x00);
    deliverSm.setDataCoding((byte) 0x00);
    deliverSm.setDefaultMsgId((byte) 0x00);

    String messageId = alarisSms.getId().toString();
    byte messageState = mapToMessageState(alarisSms.getStatus());
    DeliveryReceipt deliveryReceipt = new DeliveryReceipt(messageId, 1, 1, new DateTime(alarisSms.getCreateTime()), new DateTime(), messageState, 0, "-");
    deliverSm.setShortMessage(deliveryReceipt.toShortMessage().getBytes());

    deliverSm.addOptionalParameter(new Tlv(SmppConstants.TAG_RECEIPTED_MSG_ID, (messageId + '\0').getBytes()));
    deliverSm.addOptionalParameter(new Tlv(SmppConstants.TAG_MSG_STATE, new byte[] {messageState}));
    return deliverSm;
  }

  private int getSequenceNumber() {
    return sequenceNumberCounter.getAndUpdate(s -> s >= Integer.MAX_VALUE ? 1 : s + 1);
  }

}
