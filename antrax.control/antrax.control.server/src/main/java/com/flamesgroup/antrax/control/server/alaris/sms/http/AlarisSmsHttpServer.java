/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;

import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.properties.ControlServerProperties;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.security.tools.keytool.CertAndKeyGen;
import sun.security.x509.X500Name;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.UUID;

public class AlarisSmsHttpServer {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsHttpServer.class);

  private Server server;

  public void start() throws Exception {
    if (!ControlPropUtils.getInstance().getAlarisSmsProperties().isAlarisSmsHttpServerEnable()) {
      logger.info("[{}] - AlarisSmsHttpServer is disabled", this);
      return;
    }

    logger.info("[{}] - try to start AlarisSmsHttpServer", this);
    ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath("/");

    if (!ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerUseSsl()) {
      server = new Server(new InetSocketAddress(
          ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerHostname(),
          ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerPort())
      );
      server.getConnectors()[0].getConnectionFactory(HttpConnectionFactory.class);
    } else {
      server = new Server();
      server.setConnectors(new Connector[] {createSslServerConnector()});
    }
    server.setHandler(context);

    ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
    jerseyServlet.setInitOrder(0);

    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, AlarisSmsHttpExceptionMapper.class.getCanonicalName());
    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_CLASSNAMES, AuthHttpFilter.class.getCanonicalName());
    jerseyServlet.setInitParameter(ServerProperties.PROVIDER_PACKAGES, this.getClass().getPackage().getName());

    server.start();

    logger.info("[{}] - started AlarisSmsHttpServer", this);
  }

  public void stop() throws Exception {
    if (server == null || server.isStopped()) {
      if (ControlPropUtils.getInstance().getAlarisSmsProperties().isAlarisSmsHttpServerEnable()) {
        throw new IllegalStateException("AlarisSmsHttpServer isn't started");
      } else {
        return;
      }
    }

    logger.info("[{}] - try to stop AlarisSmsHttpServer", this);
    server.stop();
    server = null;
    logger.info("[{}] - stopped AlarisSmsHttpServer", this);
  }

  private ServerConnector createSslServerConnector() throws IOException, GeneralSecurityException {
    HttpConfiguration https = new HttpConfiguration();
    https.addCustomizer(new SecureRequestCustomizer());
    SslContextFactory sslContextFactory = new SslContextFactory();

    //Create SSL certificate
    KeyStore keyStore = KeyStore.getInstance("JKS");
    keyStore.load(null, null);
    CertAndKeyGen keyGen = new CertAndKeyGen("RSA", "SHA1WithRSA", null);
    keyGen.generate(1024);
    //Generate self signed certificate
    X509Certificate[] chain = new X509Certificate[1];
    chain[0] = keyGen.getSelfCertificate(new X500Name("CN=" + ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerHostname() + ",O=Antrax"), (long) 365 * 24 * 3600);
    String password = UUID.randomUUID().toString();
    keyStore.setKeyEntry("AlarisSms", keyGen.getPrivateKey(), password.toCharArray(), new Certificate[] {chain[0]});

    sslContextFactory.setKeyStore(keyStore);
    sslContextFactory.setKeyStorePassword(password);
    sslContextFactory.setKeyManagerPassword(password);

    ServerConnector sslConnector = new ServerConnector(server, new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(https));
    sslConnector.setPort(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerPort());
    sslConnector.setHost(ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsHttpServerHostname());
    return sslConnector;
  }


}
