/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.properties;

public class AlarisMultiUser {

  private final String login;
  private final String password;
  private final int percentageRealStatus;

  public AlarisMultiUser(final String login, final String password, final int percentageRealStatus) {
    this.login = login;
    this.password = password;
    this.percentageRealStatus = percentageRealStatus;
  }

  public String getLogin() {
    return login;
  }

  public String getPassword() {
    return password;
  }

  public int getPercentageRealStatus() {
    return percentageRealStatus;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[login:'" + login + "'" +
        " password:'" + password + "'" +
        " percentageRealStatus:" + percentageRealStatus +
        ']';
  }

}
