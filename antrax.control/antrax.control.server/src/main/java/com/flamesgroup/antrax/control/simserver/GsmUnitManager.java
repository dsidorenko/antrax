/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.control.simserver.core.SimUnitSessionPool;
import com.flamesgroup.antrax.control.voiceserver.IRawATSubChannels;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannels;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.CellMonitor;
import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.commons.impl.GsmNoGroup;
import com.flamesgroup.antrax.storage.commons.impl.NetworkSurvey;
import com.flamesgroup.antrax.storage.dao.ICellMonitorDAO;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.INetworkSurveyDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;
import com.flamesgroup.antrax.storage.enums.CellStatus;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CellInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GsmUnitManager implements IGsmUnitManger, IGsmChannelManagerHandler {

  private static final Logger logger = LoggerFactory.getLogger(GsmUnitManager.class);

  private final Map<ChannelUID, String> availableGsmChannels = new ConcurrentHashMap<>();

  private final Map<String, Pair<IRawATSubChannels, ISCEmulatorSubChannels>> subChannels = new ConcurrentHashMap<>();

  private final Map<String, Date> lastNetworkSurveyTime = new ConcurrentHashMap<>();

  private IConfigViewDAO configViewDAO;
  private ISimpleConfigEditDAO configEditDAO;
  private ICellMonitorDAO cellMonitorDAO;
  private INetworkSurveyDAO networkSurveyDAO;
  private SimUnitSessionPool simUnitSessionPool;
  private DeclaredChannelPool declaredChannelPool;

  @Override
  public void handleAvailableChannel(final String serverName, final ChannelUID channel) throws RemoteException {
    availableGsmChannels.putIfAbsent(channel, serverName);
  }

  @Override
  public void handleChannelCellInfos(final String serverName, final ChannelUID channel, final List<CellInfo> cellInfos) throws RemoteException {
    AtomicInteger cell = new AtomicInteger();
    Date handleTime = new Date(System.currentTimeMillis());
    List<CellMonitor> cellMonitors = cellInfos.stream().map(cellInfo -> new CellMonitor()
        .setGsmChannel(channel)
        .setCell((short) cell.getAndIncrement())
        .setBsic(cellInfo.getBsic())
        .setLac(cellInfo.getLac())
        .setCellId(cellInfo.getCellId())
        .setArfcn(cellInfo.getArfcn())
        .setRxLev(cellInfo.getRxLev())
        .setC1(cellInfo.getC1())
        .setC2(cellInfo.getC2())
        .setTa(cellInfo.getTa())
        .setRxQual((short) cellInfo.getRxQual())
        .setPlmn(cellInfo.getPlmn())
        .setAddTime(handleTime)).collect(Collectors.toList());
    cellMonitorDAO.saveCellMonitors(cellMonitors);
  }

  @Override
  public void handleChannelNetworkSurveys(final String serverName, final ChannelUID channel, final List<com.flamesgroup.unit.NetworkSurveyInfo> networkSurveys, final Date handleTime)
      throws RemoteException {
    IServerData server = configViewDAO.getServerByName(serverName);
    List<NetworkSurvey> dbNetworkSurveys = networkSurveys.stream().map(n -> new NetworkSurvey()
        .setArfcn(n.getArfcn())
        .setBsic(n.getBsic())
        .setRxLev(n.getRxLev())
        .setLac(n.getLac())
        .setBer(n.getBer())
        .setMcc(n.getMcc())
        .setMnc(n.getMnc())
        .setCellId(n.getCellId())
        .setCellStatus(n.getCellStatus() == null ? null : CellStatus.getStatusByValue(n.getCellStatus()))
        .setArfcns(n.getValidChannels())
        .setNeighboursArfcn(n.getNeighbourChannels())
        .setAddTime(handleTime)
        .setGsmChannel(channel)
        .setTrusted(false)
        .setServerId(server.getID())).collect(Collectors.toList());
    networkSurveyDAO.saveNetworkSurveys(dbNetworkSurveys);

    lastNetworkSurveyTime.put(serverName, handleTime);
  }

  @Override
  public void handleLostChannels(final String serverName, final List<ChannelUID> channels) throws RemoteException {
    availableGsmChannels.keySet().removeAll(channels);
  }

  @Override
  public void handleDeclaredChannels(final String serverName, final List<ChannelUID> channels) throws RemoteException {
    declaredChannelPool.addDeclaredChannels(channels);
    for (ChannelUID channel : channels) {
      if (configViewDAO.isGSMChannelStored(channel)) {
        continue;
      }

      GSMChannel gsmChannel = new GSMChannelImpl().setGsmGroup(new GsmNoGroup()).setChannelUID(channel);
      try {
        configEditDAO.updateGSMChannel(gsmChannel);
      } catch (DataModificationException e) {
        logger.warn("[{}] - can't update GSMChannel [{}]", this, gsmChannel, e);
      }
    }
  }

  @Override
  public void handleEmptyChannel(final String serverName, final ChannelUID channel, final ChannelConfig channelConfig) throws RemoteException {
  }

  @Override
  public void handleInvalidChannel(final String serverName, final ChannelUID channel, final ChannelConfig channelConfig) throws RemoteException {
  }

  @Override
  public void startVoiceServerSession(final String server, final IRawATSubChannels rawATSubChannels, final ISCEmulatorSubChannels scEmulatorSubChannels) throws RemoteException {
    simUnitSessionPool.startVoiceServerSession(server);
    this.subChannels.put(server, new Pair<>(rawATSubChannels, scEmulatorSubChannels));
  }

  @Override
  public void endVoiceServerSession(final String server) throws RemoteException {
    simUnitSessionPool.endVoiceServerSession(server);
    subChannels.remove(server);
  }

  @Override
  public IRawATSubChannels getRawATSubChannels(final ChannelUID channel) throws RemoteException {
    String serverName = availableGsmChannels.get(channel);
    if (serverName == null) {
      return null;
    } else {
      return subChannels.get(serverName).first();
    }
  }

  @Override
  public ISCEmulatorSubChannels getSCEmulatorSubChannels(final ChannelUID channel) throws RemoteException {
    String serverName = availableGsmChannels.get(channel);
    if (serverName == null) {
      return null;
    } else {
      return subChannels.get(serverName).second();
    }
  }

  public void setConfigViewDAO(final IConfigViewDAO configViewDAO) {
    this.configViewDAO = configViewDAO;
  }

  public void setConfigEditDAO(final ISimpleConfigEditDAO configEditDAO) {
    this.configEditDAO = configEditDAO;
  }

  public void setSimUnitSessionPool(final SimUnitSessionPool simUnitSessionPool) {
    this.simUnitSessionPool = simUnitSessionPool;
  }

  public void setDeclaredChannelPool(final DeclaredChannelPool declaredChannelPool) {
    this.declaredChannelPool = declaredChannelPool;
  }

  public void setCellMonitorDAO(final ICellMonitorDAO cellMonitorDAO) {
    this.cellMonitorDAO = cellMonitorDAO;
  }

  public void setNetworkSurveyDAO(final INetworkSurveyDAO networkSurveyDAO) {
    this.networkSurveyDAO = networkSurveyDAO;
  }

  public Map<String, Date> getLastNetworkSurveyTime() {
    List<IServerData> serversData = configViewDAO.listServers();
    for (IServerData serverData : serversData) {
      if (!lastNetworkSurveyTime.containsKey(serverData.getName())) {
        Date lastNetworkSurveyDate = networkSurveyDAO.getLastNetworkSurvey(serverData.getID());
        if (lastNetworkSurveyDate != null) {
          lastNetworkSurveyTime.put(serverData.getName(), lastNetworkSurveyDate);
        }
      }
    }
    return lastNetworkSurveyTime;
  }

}
