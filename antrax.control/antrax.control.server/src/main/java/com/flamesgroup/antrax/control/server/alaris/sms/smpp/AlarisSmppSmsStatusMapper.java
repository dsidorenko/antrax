/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.smpp;

import com.cloudhopper.smpp.SmppConstants;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;

public final class AlarisSmppSmsStatusMapper {

  private AlarisSmppSmsStatusMapper() {
  }

  public static byte mapToMessageState(final AlarisSmppSms.Status status) {
    switch (status) {
      case ENROUTE:
      case SENT:
        return SmppConstants.STATE_ENROUTE;
      case DELIVRD:
        return SmppConstants.STATE_DELIVERED;
      case EXPIRED:
        return SmppConstants.STATE_EXPIRED;
      case UNDELIV:
        return SmppConstants.STATE_UNDELIVERABLE;
      case ACCEPTD:
        return SmppConstants.STATE_ACCEPTED;
      case UNKNOWN:
        return SmppConstants.STATE_UNKNOWN;
      case REJECTD:
        return SmppConstants.STATE_REJECTED;
      case DELETED:
        return SmppConstants.STATE_DELETED;
      default:
        return SmppConstants.STATE_UNKNOWN;
    }
  }

}
