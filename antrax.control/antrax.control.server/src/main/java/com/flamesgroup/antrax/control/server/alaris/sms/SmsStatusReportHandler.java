/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms;

import com.flamesgroup.antrax.activity.ISmsStatusReportHandler;
import com.flamesgroup.antrax.control.server.alaris.sms.http.AlarisSmsHttpService;
import com.flamesgroup.antrax.control.server.alaris.sms.smpp.AlarisSmsSmppService;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.dao.IAlarisHttpSmsDAO;
import com.flamesgroup.antrax.storage.dao.IAlarisSmppSmsDAO;
import com.flamesgroup.unit.sms.SMSStatusReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class SmsStatusReportHandler implements ISmsStatusReportHandler {

  private static final Logger logger = LoggerFactory.getLogger(SmsStatusReportHandler.class);

  private final IAlarisHttpSmsDAO alarisSmsHttpDAO;
  private final IAlarisSmppSmsDAO alarisSmsSmppDAO;

  private AlarisSmsHttpService alarisSmsHttpService;
  private AlarisSmsSmppService alarisSmsSmppService;

  public SmsStatusReportHandler(final IAlarisHttpSmsDAO alarisSmsHttpDAO, final IAlarisSmppSmsDAO alarisSmsSmppDAO) {
    this.alarisSmsHttpDAO = alarisSmsHttpDAO;
    this.alarisSmsSmppDAO = alarisSmsSmppDAO;
  }

  @Override
  public void handleSmsStatusReport(final UUID smsId, final int numberOfSmsPart, final SMSStatusReport smsStatusReport) {
    // FIXME: ugly fix to resolve get dlr status before status of sending sms
    new Thread() {

      @Override
      public void run() {
        try {
          Thread.sleep(TimeUnit.SECONDS.toMillis(2)); // greater then waiting time in AlarisSmsService: Thread.sleep(TimeUnit.SECONDS.toMillis(1));
        } catch (InterruptedException e) {
          e.printStackTrace();
        }

        logger.info("[{}] - handle sms status report [{}] for sms id [{}] and number [{}]", this, smsStatusReport, smsId, numberOfSmsPart);
        AlarisHttpSms alarisHttpSms = alarisSmsHttpDAO.getBySmsIdAndPartNumber(smsId, numberOfSmsPart);
        AlarisSmppSms alarisSmppSms = alarisSmsSmppDAO.getBySmsIdAndPartNumber(smsId, numberOfSmsPart);
        if (alarisHttpSms == null && alarisSmppSms == null) {
          logger.info("[{}] - not found alaris sms for sms id [{}]", this, smsId);
          return;
        }

        if (alarisHttpSms != null) {
          logger.info("[{}] - receive status [{}] for alaris sms [{}]", this, smsStatusReport, alarisHttpSms);
          alarisHttpSms.setLastUpdateTime(new Date()).setStatus(mapToAlarisSmsStatus(smsStatusReport));
          alarisSmsHttpService.sendAlarisSmsDlr(alarisHttpSms);
        } else {
          logger.info("[{}] - receive status [{}] for alaris sms [{}]", this, smsStatusReport, alarisSmppSms);
          alarisSmppSms.setLastUpdateTime(new Date()).setStatus(mapToAlarisSmsStatus(smsStatusReport));
          alarisSmsSmppService.sendAlarisSmsDlr(alarisSmppSms);
        }
      }

    }.start();
  }

  public SmsStatusReportHandler setAlarisSmsHttpService(final AlarisSmsHttpService alarisSmsHttpService) {
    this.alarisSmsHttpService = alarisSmsHttpService;
    return this;
  }

  public SmsStatusReportHandler setAlarisSmsSmppService(final AlarisSmsSmppService alarisSmsSmppService) {
    this.alarisSmsSmppService = alarisSmsSmppService;
    return this;
  }

  private AlarisSms.Status mapToAlarisSmsStatus(final SMSStatusReport smsStatusReport) {
    switch (smsStatusReport.getStatus()) {
      case UNKNOWN:
        return AlarisSms.Status.UNKNOWN;
      case INPROGRESS:
      case KEEPTRYING:
        return AlarisSms.Status.ACCEPTD;
      case DELIVERED:
        return AlarisSms.Status.DELIVRD;
      case EXPIRED:
        return AlarisSms.Status.EXPIRED;
      case DELETED:
        return AlarisSms.Status.DELETED;
      case ABORTED:
        return AlarisSms.Status.REJECTD;
      default:
        return AlarisSms.Status.UNKNOWN;
    }
  }

}
