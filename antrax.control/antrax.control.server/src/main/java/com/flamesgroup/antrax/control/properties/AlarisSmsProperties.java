/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.properties;

import com.flamesgroup.properties.PropertiesLoader;
import com.flamesgroup.properties.ServerProperties;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class AlarisSmsProperties implements ServerProperties {

  private static final String ALARIS_SMS_WAIT_MULTIPART_TIMEOUT = "alaris.sms.wait.multipart.timeout";

  private static final String ALARIS_SMS_HTTP_SERVER_ENABLE = "alaris.sms.http.server.enable";
  private static final String ALARIS_SMS_HTTP_SERVER_HOSTNAME = "alaris.sms.http.server.hostname";
  private static final String ALARIS_SMS_HTTP_SERVER_PORT = "alaris.sms.http.server.port";
  private static final String ALARIS_SMS_HTTP_SERVER_USE_SSL = "alaris.sms.http.server.useSsl";
  private static final String ALARIS_SMS_HTTP_SERVER_MULTI_USER = "alaris.sms.http.server.multiUser";
  private static final String ALARIS_SMS_HTTP_RESENT_AMOUNT = "alaris.sms.http.resent.amount";
  private static final String ALARIS_SMS_HTTP_RESENT_ERROR_AMOUNT = "alaris.sms.http.resent.error.amount";
  private static final String ALARIS_SMS_HTTP_LIMIT_PEAR_DAY = "alaris.sms.http.limit.pear.day";
  private static final String ALARIS_SMS_HTTP_LIMIT_PEAR_DAY_BY_IP = "alaris.sms.http.limit.pear.day.by.ip";
  private static final String ALARIS_SMS_HTTP_ALLOW_LIMIT_PEAR_DAY_BY_IP = "alaris.sms.http.allow.limit.pear.day.by.ip";
  private static final String ALARIS_SMS_HTTP_SENDER_BUFFER = "alaris.sms.http.sender.buffer";
  private static final String ALARIS_SMS_HTTP_ALTERNATIVE_RESPONSE = "alaris.sms.http.alternative.response";

  private static final String ALARIS_SMS_SMPP_SERVER_ENABLE = "alaris.sms.smpp.server.enable";
  private static final String ALARIS_SMS_SMPP_SERVER_HOSTNAME = "alaris.sms.smpp.server.hostname";
  private static final String ALARIS_SMS_SMPP_SERVER_PORT = "alaris.sms.smpp.server.port";
  private static final String ALARIS_SMS_SMPP_SERVER_USE_SSL = "alaris.sms.smpp.server.useSsl";
  private static final String ALARIS_SMS_SMPP_SERVER_MAX_CONNECTION = "alaris.sms.smpp.server.max.connection";
  private static final String ALARIS_SMS_SMPP_SERVER_SEND_WINDOW_SIZE = "alaris.sms.smpp.server.send.window.size";
  private static final String ALARIS_SMS_SMPP_SERVER_REQUEST_EXPIRY_TIMEOUT = "alaris.sms.smpp.server.request.expiry.timeout";
  private static final String ALARIS_SMS_SMPP_SERVER_WINDOW_MONITOR_INTERVAL = "alaris.sms.smpp.server.window.monitor.interval";
  private static final String ALARIS_SMS_SMPP_SERVER_MULTI_USER = "alaris.sms.smpp.server.multiUser";
  private static final String ALARIS_SMS_SMPP_RESENT_AMOUNT = "alaris.sms.smpp.resent.amount";
  private static final String ALARIS_SMS_SMPP_RESENT_ERROR_AMOUNT = "alaris.sms.smpp.resent.error.amount";

  private final AtomicLong alarisSmsWaitMultipartTimeout = new AtomicLong(TimeUnit.MINUTES.toMillis(3));

  private final AtomicBoolean alarisSmsHttpServerEnable = new AtomicBoolean();
  private final AtomicReference<String> alarisSmsHttpServerHostname = new AtomicReference<>();
  private final AtomicInteger alarisSmsHttpServerPort = new AtomicInteger();
  private final AtomicBoolean alarisSmsHttpServerUseSsl = new AtomicBoolean();
  private final AtomicReference<Map<String, AlarisMultiUser>> alarisSmsHttpServerMultiUser = new AtomicReference<>();
  private final AtomicInteger alarisSmsHttpResentAmount = new AtomicInteger();
  private final AtomicInteger alarisSmsHttpResentErrorAmount = new AtomicInteger();
  private final AtomicInteger alarisSmsHttpLimitPearDay = new AtomicInteger();
  private final AtomicReference<Map<String, Integer>> alarisSmsHttpLimitPearDayByIp = new AtomicReference<>();
  private final AtomicBoolean alarisSmsHttpAllowLimitPearDayByIp = new AtomicBoolean();

  private final AtomicInteger alarisSmsHttpSenderBuffer = new AtomicInteger();
  private final AtomicBoolean alarisIsSmsHttpAlternativeResponse = new AtomicBoolean();

  private final AtomicBoolean alarisSmsSmppServerEnable = new AtomicBoolean();
  private final AtomicReference<String> alarisSmsSmppServerHostname = new AtomicReference<>();
  private final AtomicInteger alarisSmsSmppServerPort = new AtomicInteger();
  private final AtomicBoolean alarisSmsSmppServerUseSsl = new AtomicBoolean();
  private final AtomicInteger alarisSmsSmppServerMaxConnection = new AtomicInteger();
  private final AtomicInteger alarisSmsSmppServerSendWindowSize = new AtomicInteger();
  private final AtomicLong alarisSmsSmppServerRequestExpiryTimeout = new AtomicLong(TimeUnit.SECONDS.toMillis(30));
  private final AtomicLong alarisSmsSmppServerWindowMonitorInterval = new AtomicLong(TimeUnit.SECONDS.toMillis(15));
  private final AtomicReference<Map<String, AlarisMultiUser>> alarisSmsSmppServerMultiUser = new AtomicReference<>();
  private final AtomicInteger alarisSmsSmppResentAmount = new AtomicInteger();
  private final AtomicInteger alarisSmsSmppResentErrorAmount = new AtomicInteger();

  @Override
  public void load(final Properties properties) {
    PropertiesLoader propertiesLoader = new PropertiesLoader(properties);

    alarisSmsWaitMultipartTimeout.set(propertiesLoader.getPeriod(ALARIS_SMS_WAIT_MULTIPART_TIMEOUT));

    // Load HTTP configuration
    alarisSmsHttpServerEnable.set(propertiesLoader.getBoolean(ALARIS_SMS_HTTP_SERVER_ENABLE));
    alarisSmsHttpServerHostname.set(propertiesLoader.getString(ALARIS_SMS_HTTP_SERVER_HOSTNAME));
    alarisSmsHttpServerPort.set(propertiesLoader.getInt(ALARIS_SMS_HTTP_SERVER_PORT));
    alarisSmsHttpServerUseSsl.set(propertiesLoader.getBoolean(ALARIS_SMS_HTTP_SERVER_USE_SSL));

    alarisSmsHttpServerMultiUser.set(mapAlarisMultiUsers(ALARIS_SMS_HTTP_SERVER_MULTI_USER, propertiesLoader));

    alarisSmsHttpResentAmount.set(propertiesLoader.getInt(ALARIS_SMS_HTTP_RESENT_AMOUNT));
    alarisSmsHttpResentErrorAmount.set(propertiesLoader.getInt(ALARIS_SMS_HTTP_RESENT_ERROR_AMOUNT));
    alarisSmsHttpLimitPearDay.set(propertiesLoader.getInt(ALARIS_SMS_HTTP_LIMIT_PEAR_DAY));
    String limitByIp = propertiesLoader.getString(ALARIS_SMS_HTTP_LIMIT_PEAR_DAY_BY_IP);
    String[] limitByIpSplit = limitByIp.split(";");
    if (limitByIpSplit.length == 0) {
      throw new IllegalArgumentException(ALARIS_SMS_HTTP_LIMIT_PEAR_DAY_BY_IP + " must be contains at least one record");
    }
    Map<String, Integer> alarisLimitByIp = new HashMap<>();
    for (String s : limitByIpSplit) {
      String[] multiIpRecord = s.split("@");
      if (multiIpRecord.length != 2) {
        throw new IllegalArgumentException(ALARIS_SMS_HTTP_LIMIT_PEAR_DAY_BY_IP + " must be contains at least one record, with contains X@Y");
      }
      if (multiIpRecord[0] != null && !multiIpRecord[0].isEmpty()) {
        alarisLimitByIp.put(multiIpRecord[0], Integer.valueOf(multiIpRecord[1]));
      }
    }
    int allLimitsPerDay = 0;
    for (Integer limit : alarisLimitByIp.values()) {
      allLimitsPerDay += limit;
    }
    if (allLimitsPerDay != alarisSmsHttpLimitPearDay.get()) {
      throw new IllegalArgumentException(ALARIS_SMS_HTTP_LIMIT_PEAR_DAY_BY_IP + " all limits must be sum of " + ALARIS_SMS_HTTP_LIMIT_PEAR_DAY);
    }

    alarisSmsHttpAllowLimitPearDayByIp.set(propertiesLoader.getBoolean(ALARIS_SMS_HTTP_ALLOW_LIMIT_PEAR_DAY_BY_IP));
    alarisSmsHttpLimitPearDayByIp.set(alarisLimitByIp);

    alarisSmsHttpSenderBuffer.set(propertiesLoader.getInt(ALARIS_SMS_HTTP_SENDER_BUFFER));
    alarisIsSmsHttpAlternativeResponse.set(propertiesLoader.getBoolean(ALARIS_SMS_HTTP_ALTERNATIVE_RESPONSE));

    // Load SMPP configuration
    alarisSmsSmppServerEnable.set(propertiesLoader.getBoolean(ALARIS_SMS_SMPP_SERVER_ENABLE));
    alarisSmsSmppServerHostname.set(propertiesLoader.getString(ALARIS_SMS_SMPP_SERVER_HOSTNAME));
    alarisSmsSmppServerPort.set(propertiesLoader.getInt(ALARIS_SMS_SMPP_SERVER_PORT));
    alarisSmsSmppServerUseSsl.set(propertiesLoader.getBoolean(ALARIS_SMS_SMPP_SERVER_USE_SSL));

    alarisSmsSmppServerMaxConnection.set(propertiesLoader.getInt(ALARIS_SMS_SMPP_SERVER_MAX_CONNECTION));
    alarisSmsSmppServerSendWindowSize.set(propertiesLoader.getInt(ALARIS_SMS_SMPP_SERVER_SEND_WINDOW_SIZE));

    alarisSmsSmppServerRequestExpiryTimeout.set(propertiesLoader.getPeriod(ALARIS_SMS_SMPP_SERVER_REQUEST_EXPIRY_TIMEOUT));
    alarisSmsSmppServerWindowMonitorInterval.set(propertiesLoader.getPeriod(ALARIS_SMS_SMPP_SERVER_WINDOW_MONITOR_INTERVAL));

    alarisSmsSmppServerMultiUser.set(mapAlarisMultiUsers(ALARIS_SMS_SMPP_SERVER_MULTI_USER, propertiesLoader));

    alarisSmsSmppResentAmount.set(propertiesLoader.getInt(ALARIS_SMS_SMPP_RESENT_AMOUNT));
    alarisSmsSmppResentErrorAmount.set(propertiesLoader.getInt(ALARIS_SMS_SMPP_RESENT_ERROR_AMOUNT));
  }

  private Map<String, AlarisMultiUser> mapAlarisMultiUsers(final String property, final PropertiesLoader propertiesLoader) {
    String alarisMultiUserProperty = propertiesLoader.getString(property);
    String[] alarisMultiUserSplit = alarisMultiUserProperty.split(";");
    if (alarisMultiUserProperty.length() == 0) {
      throw new IllegalArgumentException(property + " must be contains at least one record");
    }

    Map<String, AlarisMultiUser> alarisMultiUsers = new HashMap<>();
    for (String alarisMultiUser : alarisMultiUserSplit) {
      String[] alarisMultiUserRecord = alarisMultiUser.split("@");
      if (alarisMultiUserRecord.length != 3) {
        throw new IllegalArgumentException(property + " must be contains at least one record, with contains X@Y@Z");
      }
      alarisMultiUsers.put(alarisMultiUserRecord[0], new AlarisMultiUser(alarisMultiUserRecord[0], alarisMultiUserRecord[1], Integer.valueOf(alarisMultiUserRecord[2])));
    }

    return alarisMultiUsers;
  }

  public long getAlarisSmsWaitMultipartTimeout() {
    return alarisSmsWaitMultipartTimeout.get();
  }

  public boolean isAlarisSmsHttpServerEnable() {
    return alarisSmsHttpServerEnable.get();
  }

  public String getAlarisSmsHttpServerHostname() {
    return alarisSmsHttpServerHostname.get();
  }

  public int getAlarisSmsHttpServerPort() {
    return alarisSmsHttpServerPort.get();
  }

  public boolean getAlarisSmsHttpServerUseSsl() {
    return alarisSmsHttpServerUseSsl.get();
  }

  public Map<String, AlarisMultiUser> getAlarisSmsHttpServerMultiUser() {
    return alarisSmsHttpServerMultiUser.get();
  }

  public int getAlarisSmsHttpResentAmount() {
    return alarisSmsHttpResentAmount.get();
  }

  public int getAlarisSmsHttpResentErrorAmount() {
    return alarisSmsHttpResentErrorAmount.get();
  }

  public int getAlarisSmsHttpLimitPearDay() {
    return alarisSmsHttpLimitPearDay.get();
  }

  public Map<String, Integer> getAlarisSmsHttpLimitPearDayByIp() {
    return alarisSmsHttpLimitPearDayByIp.get();
  }

  public boolean getAlarisSmsHttpAllowLimitPearDayByIp() {
    return alarisSmsHttpAllowLimitPearDayByIp.get();
  }

  public int getAlarisSmsHttpSenderBuffer() {
    return alarisSmsHttpSenderBuffer.get();
  }

  public boolean isSmsHttpAlternativeResponse() {
    return alarisIsSmsHttpAlternativeResponse.get();
  }

  public boolean isAlarisSmsSmppServerEnable() {
    return alarisSmsSmppServerEnable.get();
  }

  public String getAlarisSmsSmppServerHostname() {
    return alarisSmsSmppServerHostname.get();
  }

  public int getAlarisSmsSmppServerPort() {
    return alarisSmsSmppServerPort.get();
  }

  public boolean getAlarisSmsSmppServerUseSsl() {
    return alarisSmsSmppServerUseSsl.get();
  }

  public int getAlarisSmsSmppServerMaxConnection() {
    return alarisSmsSmppServerMaxConnection.get();
  }

  public int getAlarisSmsSmppServerSendWindowSize() {
    return alarisSmsSmppServerSendWindowSize.get();
  }

  public long getAlarisSmsSmppServerRequestExpiryTimeout() {
    return alarisSmsSmppServerRequestExpiryTimeout.get();
  }

  public long getAlarisSmsSmppServerWindowMonitorInterval() {
    return alarisSmsSmppServerWindowMonitorInterval.get();
  }

  public Map<String, AlarisMultiUser> getAlarisSmsSmppServerMultiUser() {
    return alarisSmsSmppServerMultiUser.get();
  }

  public int getAlarisSmsSmppResentAmount() {
    return alarisSmsSmppResentAmount.get();
  }

  public int getAlarisSmsSmppResentErrorAmount() {
    return alarisSmsSmppResentErrorAmount.get();
  }

}
