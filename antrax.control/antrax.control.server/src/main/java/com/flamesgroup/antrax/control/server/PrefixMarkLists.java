/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.antrax.storage.dao.IPhoneNumberPrefixDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrefixMarkLists {

  private static final Logger logger = LoggerFactory.getLogger(PrefixMarkLists.class);

  private final IPhoneNumberPrefixDAO phoneNumberPrefixDAO;

  public PrefixMarkLists(final IPhoneNumberPrefixDAO phoneNumberPrefixDAO) {
    this.phoneNumberPrefixDAO = phoneNumberPrefixDAO;
  }

  public String markPhoneNumber(final String phoneNumber) {
    logger.debug("[{}] - start mark number: [{}]", this, phoneNumber);
    long startTime = System.currentTimeMillis();
    PhoneNumberPrefixConfig prefixConfig = phoneNumberPrefixDAO.getPrefixConfig();
    if (prefixConfig == null || !prefixConfig.isEnable()) {
      logger.debug("[{}] - marked prefix is disabled", this);
      return phoneNumber;
    }

    String prefix = phoneNumberPrefixDAO.findPhoneNumberPrefixList(phoneNumber);

    String phoneNumberWithPrefix;
    if (prefix != null) {
      phoneNumberWithPrefix = prefix + phoneNumber;
    } else {
      logger.debug("[{}] - can't find prefix for number: [{}], use default prefix: [{}]", this, phoneNumber, prefixConfig.getDefaultPrefix());
      phoneNumberWithPrefix = prefixConfig.getDefaultPrefix() + phoneNumber;
    }

    long endTime = System.currentTimeMillis();
    logger.debug("[{}] - end mark number: [{}], marking time: [{}]", this, phoneNumber, endTime - startTime);
    return phoneNumberWithPrefix;
  }

}
