/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.tool;

import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannelsHandler;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannelHandler;
import com.flamesgroup.device.gsmb.SCEmulatorError;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.sc.APDUCommand;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SCEmulatorSubChannelsHandler implements ISCEmulatorSubChannelsHandler {

  private final Map<ChannelUID, ISCEmulatorSubChannelHandler> scEmulatorSubChannelHandlers = new ConcurrentHashMap<>();

  @Override
  public void handleAPDUCommand(final ChannelUID channel, final APDUCommand command) throws RemoteException, IllegalChannelException {
    ISCEmulatorSubChannelHandler handler = scEmulatorSubChannelHandlers.get(channel);
    if (handler == null) {
      throw new IllegalChannelException(channel);
    } else {
      handler.handleAPDUCommand(command);
    }
  }

  @Override
  public void handleErrorState(final ChannelUID channel, final SCEmulatorError error, final SCEmulatorState state) throws RemoteException, IllegalChannelException {
    ISCEmulatorSubChannelHandler handler = scEmulatorSubChannelHandlers.get(channel);
    if (handler == null) {
      throw new IllegalChannelException(channel);
    } else {
      handler.handleErrorState(error, state);
    }
  }

  public void putSCEmulatorSubChannelHandler(final ChannelUID channel, final ISCEmulatorSubChannelHandler handler) {
    scEmulatorSubChannelHandlers.put(channel, handler);
  }

  public void removeSCEmulatorSubChannelHandler(final ChannelUID channel) {
    scEmulatorSubChannelHandlers.remove(channel);
  }

}
