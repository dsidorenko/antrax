/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.dao;

import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.LinkWithSIMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigViewDAO;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.utils.codebase.ScriptFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ConfigurationDAOImpl implements ConfigurationDAO {

  private static final Logger logger = LoggerFactory.getLogger(ConfigurationDAOImpl.class);

  private final IConfigViewDAO configViewDAO;
  private final ISimpleConfigViewDAO simpleConfigViewDAO;

  public ConfigurationDAOImpl(final DAOProvider daoProvider) {
    configViewDAO = daoProvider.getConfigViewDAO();
    simpleConfigViewDAO = daoProvider.getSimpleConfigViewDAO();
  }

  @Override
  public byte[] getActivityScript(final ICCID simUid) {
    return simpleConfigViewDAO.getActivityPeriodScript(simUid);
  }

  @Override
  public byte[] getFactorEvaluationScript(final ICCID simUid) {
    return simpleConfigViewDAO.getFactorScript(simUid);
  }

  @Override
  public byte[] getGatewaySelectorScript(final ICCID simUid) {
    return simpleConfigViewDAO.getGwSelectorScript(simUid);
  }

  @Override
  public byte[] getSessionScript(final ICCID simUid) {
    return simpleConfigViewDAO.getSessionScript(simUid);
  }

  @Override
  public byte[] getIMEIGeneratorScript(final ICCID simUID) {
    return simpleConfigViewDAO.getIMEIGeneratorScript(simUID);
  }

  @Override
  public boolean shouldResetSsScripts(final ICCID simUid) {
    return simpleConfigViewDAO.shouldResetSsScripts(simUid);
  }

  @Override
  public Map<GSMGroup, Set<SIMGroup>> loadGsmToSimLinks() {
    Map<GSMGroup, Set<SIMGroup>> groupLinks = new HashMap<>();
    try {
      for (LinkWithSIMGroup link : configViewDAO.listSIMGSMLinks()) {
        GSMGroup gsmGroup = link.getGSMGroup();
        if (!groupLinks.containsKey(gsmGroup)) {
          groupLinks.put(gsmGroup, new HashSet<>());
        }
        groupLinks.get(gsmGroup).add(link.getSIMGroup());
      }
    } catch (DataSelectionException e) {
      logger.warn("[{}] - failed to get list of gsm to sim group links", this, e);
    }
    return groupLinks;
  }

  @Override
  public Map<String, IServerData> loadGatewaysData() {
    Map<String, IServerData> gateways = new HashMap<>();
    for (IServerData server : configViewDAO.listServers()) {
      gateways.put(server.getName(), server);
    }
    return gateways;
  }

  @Override
  public ScriptFile getLastScriptFile() {
    return configViewDAO.getLastScriptFile();
  }

  @Override
  public SimData loadSimData(final ICCID simUid) {
    try {
      return configViewDAO.getSimData(simUid);
    } catch (DataSelectionException e) {
      logger.error("[{}] - failed to load sim data by iccid {}", this, simUid, e);
      return null;
    }
  }

}
