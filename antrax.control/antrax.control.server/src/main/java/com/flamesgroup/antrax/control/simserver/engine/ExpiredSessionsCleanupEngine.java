/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.engine;

import com.flamesgroup.antrax.control.commons.BaseEngine;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.simserver.core.SimUnitSessionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExpiredSessionsCleanupEngine extends BaseEngine {

  private static final Logger logger = LoggerFactory.getLogger(ExpiredSessionsCleanupEngine.class);

  private final SimUnitSessionPool unitSessionPool;
  private final long timeout;

  public ExpiredSessionsCleanupEngine(final SimUnitSessionPool unitSessionPool) {
    super("ExpiredSessionsCleanupEngine");
    this.unitSessionPool = unitSessionPool;
    this.timeout = ControlPropUtils.getInstance().getControlServerProperties().getSessionExpireCheckTimeout();
  }

  @Override
  protected long countSleepTime() {
    return timeout;
  }

  @Override
  protected void invokeRoutine() throws Exception {
    logger.trace("[{}] - calling cleanup on sessions pool", this);
    int removed = unitSessionPool.cleanup();
    logger.trace("[{}] - unit cleanup is finished. Removed {} expired sessions", this, removed);
  }

}
