/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms;

import com.flamesgroup.antrax.configuration.IConfigurationHandler;
import com.flamesgroup.antrax.control.commons.VsSmsStatus;
import com.flamesgroup.antrax.control.communication.IVoiceServerChannelsInfo;
import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.communication.ServerStatus;
import com.flamesgroup.antrax.control.properties.ControlPropUtils;
import com.flamesgroup.antrax.control.server.ControlBeanModel;
import com.flamesgroup.antrax.control.server.PingServersManager;
import com.flamesgroup.antrax.control.utils.WaitThreadPoolExecutor;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SmsAllocationAlgorithm;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.rmi.exception.RemoteAccessException;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AlarisSendSmsService implements IConfigurationHandler {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSendSmsService.class);

  private final ControlBeanModel controlBeanModel;
  private final PingServersManager pingServersManager;

  private final Lock routeLock = new ReentrantLock();
  private List<IServerData> serverData = new ArrayList<>();

  private final AtomicReference<SmsAllocationAlgorithm> smsAllocationAlgorithm = new AtomicReference<>();

  private final Lock multiPartLock = new ReentrantLock();
  private final Semaphore multiPartSemaphore = new Semaphore(0);
  private final Map<String, List<AlarisSms>> multiPartAlarisSms = new HashMap<>();
  private final Map<String, WaitAlarisSms> waitMultiPartAlarisSms = new HashMap<>();

  private final ThreadPoolExecutor threadPoolExecutor = new WaitThreadPoolExecutor(5, 100, 30, TimeUnit.SECONDS, new SynchronousQueue<>(), r -> new Thread(r, "AlarisSendSmsExecutor"));

  private volatile long waitMultipartTimeout;
  private volatile Thread waitSmsMultiPartThread;

  public AlarisSendSmsService(final ControlBeanModel controlBeanModel, final PingServersManager pingServersManager) {
    this.controlBeanModel = controlBeanModel;
    this.pingServersManager = pingServersManager;
  }

  public void start() {
    waitMultipartTimeout = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsWaitMultipartTimeout();
    waitSmsMultiPartThread = new Thread(new WaitSmsMultiPart(), "WaitSmsMultiPartThread");
    waitSmsMultiPartThread.start();
  }

  public void stop() {
    Thread waitSmsMultiPartThreadLocal = waitSmsMultiPartThread;
    waitSmsMultiPartThread = null;
    multiPartSemaphore.release();
    try {
      waitSmsMultiPartThreadLocal.join();
    } catch (InterruptedException e) {
      logger.warn("[{}] - while join thread was interrupted", this, e);
    }
  }

  public void sendAlarisSms(final AlarisSms alarisSms, final IAlarisSendSmsHandler alarisSendSmsHandler, final int alarisSmsResentAmount, final int alarisSmsResentErrorAmount) {
    if (alarisSms.getSar() == null) {
      threadPoolExecutor.execute(new ProcessAlarisSms(Collections.singletonList(alarisSms), alarisSendSmsHandler, alarisSmsResentAmount, alarisSmsResentErrorAmount));
    } else {
      List<AlarisSms> alarisSmses;
      multiPartLock.lock();
      try {
        waitMultiPartAlarisSms.put(alarisSms.getMultiPartSmsKey(), new WaitAlarisSms(alarisSendSmsHandler, alarisSmsResentAmount, alarisSmsResentErrorAmount, System.currentTimeMillis()));
        alarisSmses = multiPartAlarisSms.computeIfAbsent(alarisSms.getMultiPartSmsKey(), s -> new ArrayList<>());
        alarisSmses.add(alarisSms);
        if (alarisSmses.size() != alarisSms.getSar().getParts()) {
          return; // wait all parts of sms to send
        }
        multiPartAlarisSms.remove(alarisSms.getMultiPartSmsKey());
      } finally {
        multiPartLock.unlock();
      }

      final AtomicBoolean correctMessageLength = new AtomicBoolean(true);
      alarisSmses.forEach(sms -> correctMessageLength.set(checkMessageEncodingLength(sms)));
      if (correctMessageLength.get()) {
        threadPoolExecutor.execute(new ProcessAlarisSms(alarisSmses, alarisSendSmsHandler, alarisSmsResentAmount, alarisSmsResentErrorAmount));
      } else {
        alarisSendSmsHandler.handleReject(alarisSmses);
      }
    }
  }

  @Override
  public void reconfigure(final IConfigViewDAO configViewDAO) {
    RouteConfig routeConfig = configViewDAO.getRouteConfig();
    if (routeConfig == null) {
      logger.warn("[{}] - no route config found for servers", this);
      return;
    }

    smsAllocationAlgorithm.set(routeConfig.getSmsAllocationAlgorithm());
    routeLock.lock();
    try {
      serverData = configViewDAO.listServers().stream().filter(e -> e.isVoiceServerEnabled() && e.getVoiceServerConfig().getSmsRouteConfig() != null).collect(Collectors.toList());
      logger.info("[{}] - servers for sms routing: {}", this, serverData.stream().map(IServerData::getName).collect(Collectors.toList()));
    } finally {
      routeLock.unlock();
    }

    waitMultipartTimeout = ControlPropUtils.getInstance().getAlarisSmsProperties().getAlarisSmsWaitMultipartTimeout();
  }

  private boolean checkMessageEncodingLength(final AlarisSms sms) {
    String message = sms.getMessage();
    SMSEncoding encoding = SMSCodec.getTextEncoding(message);
    if (message.length() > encoding.getConcatLength()) {
      logger.info("[{}] - incorrect message length [{}], it must be lower or equal [{}], for encoding [{}]. Skip this message [{}]", this, message.length(), encoding.getLength(), encoding, sms);
      return false;
    }
    return true;
  }

  private class WaitSmsMultiPart implements Runnable {

    @Override
    public void run() {
      Thread currentThread = Thread.currentThread();
      try {
        while (currentThread == waitSmsMultiPartThread) {
          if (multiPartSemaphore.tryAcquire(waitMultipartTimeout, TimeUnit.MILLISECONDS)) {
            continue;
          }

          Map<String, WaitAlarisSms> elapsedAlarisSmses;
          multiPartLock.lock();
          try {
            elapsedAlarisSmses = waitMultiPartAlarisSms.entrySet().stream()
                .filter(e -> System.currentTimeMillis() - e.getValue().getReceiveTime() > waitMultipartTimeout)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            elapsedAlarisSmses.forEach((multiPartSmsKey, waitAlarisSms) -> {
              waitMultiPartAlarisSms.remove(multiPartSmsKey);
              waitAlarisSms.setAlarisSmses(multiPartAlarisSms.remove(multiPartSmsKey));
            });
          } finally {
            multiPartLock.unlock();
          }

          elapsedAlarisSmses.values().forEach(waitAlarisSms -> {
            List<AlarisSms> alarisSmses = waitAlarisSms.getAlarisSmses();
            if (alarisSmses == null) {
              return; // should never reach
            }

            final AtomicBoolean correctMessageLength = new AtomicBoolean(true);
            alarisSmses.forEach(sms -> correctMessageLength.set(checkMessageEncodingLength(sms)));
            if (correctMessageLength.get()) {
              threadPoolExecutor.execute(new ProcessAlarisSms(alarisSmses, waitAlarisSms.getSendSmsHandler(), waitAlarisSms.getResentAmount(), waitAlarisSms.getResentErrorAmount()));
            } else {
              IAlarisSendSmsHandler sendSmsHandler = waitAlarisSms.getSendSmsHandler();
              sendSmsHandler.handleReject(alarisSmses);
            }
          });
        }
      } catch (InterruptedException e) {
        logger.warn(String.format("[%s] - thread was interrupted", WaitSmsMultiPart.this), e);
      }
    }

  }

  private class ProcessAlarisSms implements Runnable {

    private final Long WAITING_TIME = TimeUnit.SECONDS.toMillis(5);

    private final List<AlarisSms> alarisSmses;
    private final IAlarisSendSmsHandler processHandler;
    private final int alarisSmsResentAmount;
    private final int alarisSmsResentErrorAmount;

    private final UUID objectUuid = UUID.randomUUID();

    public ProcessAlarisSms(final List<AlarisSms> alarisSmses, final IAlarisSendSmsHandler processHandler, final int alarisSmsResentAmount, final int alarisSmsResentErrorAmount) {
      this.alarisSmses = alarisSmses;
      this.processHandler = processHandler;
      this.alarisSmsResentAmount = alarisSmsResentAmount;
      this.alarisSmsResentErrorAmount = alarisSmsResentErrorAmount;
    }

    @Override
    public void run() {
      int countIteration = 1;
      int errorCountIteration = 1;
      VsSmsStatus vsSmsStatus = new VsSmsStatus();
      alarisSmses.sort(Comparator.comparingInt(o -> o.getSar().getPartNumber()));

      processHandler.handleSending(alarisSmses);
      while (true) {
        try {
          logger.info("[{}] - try send alaris sms: [{}], objectUuid: [{}]", this, alarisSmses, objectUuid);
          List<IServerData> localServerData;
          routeLock.lock();
          try {
            localServerData = serverData;
          } finally {
            routeLock.unlock();
          }
          List<String> workingServers = pingServersManager.getVoiceServersStatus().entrySet().stream()
              .filter(svs -> svs.getValue().getShortInfo().getStatus() == ServerStatus.STARTED).map(svs -> svs.getKey().getName())
              .collect(Collectors.toList());

          List<IServerData> routerServerData = localServerData.stream().filter(createFilterPredicate(workingServers)).collect(Collectors.toList());
          switch (smsAllocationAlgorithm.get()) {
            case RANDOM:
              Collections.shuffle(routerServerData);
              break;
            case UNIFORMLY_TOTAL:
              routerServerData.sort(createFactorComparator(this::countUniformlyTotalFactor));
              break;
            case UNIFORMLY_SUCCESS:
              routerServerData.sort(createFactorComparator(this::countUniformlySuccessFactor));
              break;
          }

          logger.debug("[{}] - routerServerData: {}", this, routerServerData);

          for (IServerData serverData : routerServerData) {
            try {
              logger.debug("[{}] - try send alaris sms: [{}], via server: [{}], objectUuid: [{}]", this, alarisSmses, serverData.getName(), objectUuid);
              vsSmsStatus = controlBeanModel.sendSMS(serverData, alarisSmses, objectUuid);

              for (int i = 0; i < 300; i++) {
                if (vsSmsStatus.getStatus() != VsSmsStatus.Status.SENDING) {
                  break;
                }
                logger.debug("[{}] - waiting SMS will be sent", this);
                Thread.sleep(TimeUnit.SECONDS.toMillis(1));
                vsSmsStatus = controlBeanModel.getSMSStatus(serverData, objectUuid);
              }

              if (vsSmsStatus.getStatus() == VsSmsStatus.Status.SENT) {
                logger.debug("[{}] - sent alaris sms: [{}], via server: [{}]", this, alarisSmses, serverData.getName());
                controlBeanModel.removeSMSStatus(serverData, objectUuid);
                break;
              }
              logger.debug("[{}] - last SMS status is: [{}], with message: [{}]", this, vsSmsStatus.getStatus(), vsSmsStatus.getMessage());
            } catch (RemoteAccessException e) {
              logger.warn("[{}] - connection was lost with server: [{}]", this, serverData.getName());
              logger.debug("[{}] - ", this, e);
            }
          }

          if (vsSmsStatus.getStatus() == VsSmsStatus.Status.SENT) {
            processHandler.handleSent(alarisSmses, vsSmsStatus.getUuid());
            break;
          } else if (vsSmsStatus.getStatus() == VsSmsStatus.Status.ERROR) {
            if (errorCountIteration >= alarisSmsResentErrorAmount) {
              processHandler.handleUndeliv(alarisSmses);
              break;
            }
            logger.info("[{}] - can't send alaris sms: [{}], SEND ERROR. Try sent at next iteration", this, alarisSmses);
            errorCountIteration++;
          }

          if (countIteration >= alarisSmsResentAmount) {
            processHandler.handleUndeliv(alarisSmses);
            break;
          }

          countIteration++;
          logger.info("[{}] - can't send alaris sms: [{}], NO ROUTE. Try sent at next iteration", this, alarisSmses);
          Thread.sleep(WAITING_TIME);
        } catch (InterruptedException e) {
          logger.error("[{}] - unexpected InterruptedException - end executing", this, e);
          break;
        }
        processHandler.handleFinalize(alarisSmses);
      }
    }

    private Comparator<IServerData> createFactorComparator(final Function<IServerData, Integer> factorFunction) {
      return (o1, o2) -> {
        int priority1 = o1.getVoiceServerConfig().getSmsRouteConfig().getPriority();
        int priority2 = o2.getVoiceServerConfig().getSmsRouteConfig().getPriority();

        int cmp = Integer.compare(priority2, priority1); // sort descending
        return cmp != 0 ? cmp : Integer.compare(priority1 + factorFunction.apply(o1), priority2 + factorFunction.apply(o2));
      };
    }

    private int countUniformlyTotalFactor(final IServerData serverData) {
      return pingServersManager.getVoiceServerStatus(new Server(serverData.getName(), ServerType.VOICE_SERVER)).getChannelsInfo().stream()
          .map(IVoiceServerChannelsInfo::getMobileGatewayChannelInfo).mapToInt(MobileGatewayChannelInformation::getTotalOutgoingSmsCount).sum();
    }

    private int countUniformlySuccessFactor(final IServerData serverData) {
      return pingServersManager.getVoiceServerStatus(new Server(serverData.getName(), ServerType.VOICE_SERVER)).getChannelsInfo().stream()
          .map(IVoiceServerChannelsInfo::getMobileGatewayChannelInfo).mapToInt(MobileGatewayChannelInformation::getSuccessOutgoingSmsCount).sum();
    }

    private Predicate<IServerData> createFilterPredicate(final List<String> workingServers) {
      return r -> workingServers.contains(r.getName()) && alarisSmses.get(0).getSourceNumber().matches(r.getVoiceServerConfig().getSmsRouteConfig().getDnisRegex());
    }

  }

}
