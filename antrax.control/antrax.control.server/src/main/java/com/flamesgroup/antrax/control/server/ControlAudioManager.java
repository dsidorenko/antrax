/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.IControlAudioManager;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ControlAudioManager implements IControlAudioManager {

  private final ControlBeanModel model;

  public ControlAudioManager(final ControlBeanModel model) {
    this.model = model;
  }

  @Override
  public List<AudioFile> getAudioFiles(final ClientUID clientUID, final IServerData selectedServer) throws NotPermittedException {
    return model.getAudioFiles(selectedServer);
  }

  @Override
  public byte[] downloadAudioFileFromServer(final ClientUID clientUID, final IServerData selectedServer, final String fileName) throws NotPermittedException {
    return model.downloadAudioFileFromServer(selectedServer, fileName);
  }

  @Override
  public List<String> getIvrTemplatesSimGroups(final ClientUID clientUID, final IServerData selectedServer) throws NotPermittedException {
    return model.getIvrTemplatesSimGroups(selectedServer);
  }

  @Override
  public List<IvrTemplateWrapper> getIvrTemplates(final ClientUID clientUID, final IServerData selectedServer, final String simGroup) throws NotPermittedException {
    return model.getIvrTemplates(selectedServer, simGroup);
  }

  @Override
  public void createIvrTemplateSimGroup(final ClientUID clientUID, final IServerData selectedServer, final String simGroup) throws NotPermittedException, IvrTemplateException {
    model.createIvrTemplateSimGroup(selectedServer, simGroup);
  }

  @Override
  public void removeIvrTemplateSimGroups(final ClientUID clientUID, final IServerData selectedServer, final List<String> simGroups) throws NotPermittedException, IvrTemplateException {
    model.removeIvrTemplateSimGroups(selectedServer, simGroups);
  }

  @Override
  public Map<String, Integer> getCallDropReasons(final ClientUID clientUID, final IServerData selectedServer) throws NotPermittedException {
    return model.getCallDropReasons(selectedServer);
  }

  @Override
  public void uploadIvrTemplate(final ClientUID clientUID, final IServerData selectedServer, final String selectedSimGroup, final byte[] bytes, final String fileName)
      throws NotPermittedException, IvrTemplateException {
    model.uploadIvrTemplate(selectedServer, selectedSimGroup, bytes, fileName);
  }

  @Override
  public void removeIvrTemplate(final ClientUID clientUID, final IServerData selectedServer, final String selectedSimGroup, final List<IvrTemplateWrapper> selectedIvrTemplates)
      throws NotPermittedException, IvrTemplateException {
    model.removeIvrTemplate(selectedServer, selectedSimGroup, selectedIvrTemplates);
  }

  @Override
  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return false;
  }
}
