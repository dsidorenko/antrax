/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.rmi;

import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.control.server.PingServersManager;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.voiceserver.IVoiceServerAudioManager;
import com.flamesgroup.antrax.voiceserver.IVoiceServerManager;
import com.flamesgroup.rmi.RemoteAccessor;
import com.flamesgroup.rmi.RemoteProxyFactory;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class VoiceServerManagersPool {

  private final Map<IServerData, RemoteVoiceServerManager> managers = new ConcurrentHashMap<>();
  private final IConfigViewDAO configViewDAO;
  private final PingServersManager pingServersManager;

  public VoiceServerManagersPool(final IConfigViewDAO configViewDAO, final PingServersManager pingServersManager) {
    this.configViewDAO = configViewDAO;
    this.pingServersManager = pingServersManager;
  }

  public List<IServerData> listVoiceServers() {
    List<IServerData> voiceServers = configViewDAO.listServers().stream().filter(IServerData::isVoiceServerEnabled).collect(Collectors.toList());

    if (voiceServers.isEmpty()) {
      managers.clear();
    } else {
      managers.keySet().stream().filter(server -> !voiceServers.contains(server)).forEach(managers::remove);
    }
    return voiceServers;
  }

  public RemoteVoiceServerManager getManager(final IServerData server) {
    if (!managers.containsKey(server)) {
      String host = server.getPublicAddress().getHostAddress();
      IVoiceServerStatus status = pingServersManager.getVoiceServerStatus(new Server(server.getName(), ServerType.VOICE_SERVER));
      if (status == null || status.getRmiRegistryPort() < 0) {
        return null;
      }

      IVoiceServerManager voiceServerManager = RemoteProxyFactory.create(new RemoteAccessor.Builder<>(host, status.getRmiRegistryPort(), IVoiceServerManager.class).build());
      IVoiceServerAudioManager voiceServerAudioManager = RemoteProxyFactory.create(new RemoteAccessor.Builder<>(host, status.getRmiRegistryPort(), IVoiceServerAudioManager.class).build());
      managers.put(server, new RemoteVoiceServerManager(voiceServerManager, voiceServerAudioManager));
    }
    return managers.get(server);
  }

}
