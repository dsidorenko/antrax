/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;

import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;

import java.util.Date;
import java.util.UUID;

public final class AlarisSmsHttpRestService {

  private static final AlarisSmsHttpRestService instance = new AlarisSmsHttpRestService();

  private AlarisSmsHttpService alarisSmsHttpService;

  private AlarisSmsHttpRestService() {
  }

  public UUID sendHttpSms(final String ani, final String dnis, final String message, final String serviceType, final AlarisHttpSms.LongMessageMode longMessageMode, final Integer dataCoding,
      final AlarisHttpSms.Sar sar, final String dlrUrl, final String username, final String clientIp) {

    AlarisHttpSms alarisSms = new AlarisHttpSms(UUID.randomUUID(), ani, dnis, message, serviceType, longMessageMode, dataCoding, new Date(), sar, dlrUrl, username, clientIp);
    alarisSmsHttpService.sendAlarisSms(alarisSms);

    return alarisSms.getId();
  }

  public String getSmsStatus(final UUID smsId) {
    AlarisHttpSms.Status status = alarisSmsHttpService.getAlarisSmsStatus(smsId);
    return status == null ? null : status.toString();
  }

  public void setAlarisSmsHttpService(final AlarisSmsHttpService alarisSmsHttpService) {
    this.alarisSmsHttpService = alarisSmsHttpService;
  }

  public static AlarisSmsHttpRestService getInstance() {
    return instance;
  }

}
