/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.alaris.sms.http;


import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.unit.PhoneNumber;
import com.flamesgroup.unit.sms.SMSCodec;
import com.flamesgroup.unit.sms.SMSEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

@Path("/api")
public class AlarisSmsHttpResource {

  private static final Logger logger = LoggerFactory.getLogger(AlarisSmsHttpResource.class);

  private static final AlarisSmsHttpRestService alarisSmsHttpRestService = AlarisSmsHttpRestService.getInstance();

  private static final String NULL_CHARACTER_TEMPLATE = "[\\000]+";
  private static final Pattern nullCharacterPattern = Pattern.compile(NULL_CHARACTER_TEMPLATE);

  private static final String SAR_TEMPLATE = "(?<id>\\p{XDigit}+)/(?<parts>\\d+)/(?<partNumber>\\d+)";
  private static final Pattern sarResponsePattern = Pattern.compile(SAR_TEMPLATE);

  @GET
  @Produces("text/html;charset=UTF-8")
  public String get(@Context final UriInfo ui, @Context final HttpServletRequest httpServletRequest) throws AlarisSmsHttpException {
    MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    if (queryParams.isEmpty()) {
      throw new AlarisSmsHttpException("GET params are empty");
    }
    String clientIp = httpServletRequest.getRemoteAddr();
    String command = getQueryParam(queryParams, "command");
    switch (command) {
      case "submit":
        return processSubmitCommand(queryParams, clientIp);
      case "query":
        return processQueryCommand(queryParams);
      default:
        throw new AlarisSmsHttpException(String.format("Unsupported command: [%s]", command));
    }
  }

  private String processSubmitCommand(final MultivaluedMap<String, String> queryParams, final String clientIp) {
    logger.debug("[{}] - try process submit command, queryParams: {}", this, queryParams);
    String ani = getQueryParam(queryParams, "ani");
    String dnis = getQueryParam(queryParams, "dnis");
    String message = getQueryParam(queryParams, "message");
    String serviceType = getQueryParam(queryParams, "serviceType");
    String longMessageModeString = queryParams.getFirst("longMessageMode");
    String sar = queryParams.getFirst("sar");
    String dlrUrl = queryParams.getFirst("dlrUrl");
    String username = queryParams.getFirst("username");
    Integer dataCoding = null;
    AlarisHttpSms.LongMessageMode longMessageMode = AlarisHttpSms.LongMessageMode.CUT;

    if (!PhoneNumber.isValid(dnis) || dnis.length() > 15) {
      throw new AlarisSmsHttpException(String.format("Invalid phoneNumber: [%s] of param: [dnis]", dnis));
    }

    if (longMessageModeString != null) {
      try {
        longMessageMode = AlarisHttpSms.LongMessageMode.valueOf(longMessageModeString.toUpperCase());
      } catch (IllegalArgumentException e) {
        throw new AlarisSmsHttpException(String.format("Unsupported value: [%s] of param: [longMessageMode], must be one of: [%s] ", longMessageModeString,
            Arrays.toString(AlarisHttpSms.LongMessageMode.values())), e);
      }
    }

    if (longMessageMode == AlarisHttpSms.LongMessageMode.SPLIT || longMessageMode == AlarisHttpSms.LongMessageMode.SPLIT_SAR) {
      try {
        dataCoding = Integer.parseInt(getQueryParam(queryParams, "dataCoding"));
      } catch (NumberFormatException e) {
        throw new AlarisSmsHttpException(String.format("Unsupported value: [%s] of param: [dataCoding], must be only digits", getQueryParam(queryParams, "dataCoding")), e);
      }
      if (dataCoding < 0 || dataCoding > 8) {
        throw new AlarisSmsHttpException(String.format("Unsupported value: [%s] of param: [dataCoding], must be from 0 to 8", dataCoding));
      }
    }

    AlarisHttpSms.Sar alarisSmsSar = null;
    if (sar != null && !sar.isEmpty()) {
      Matcher matcher = sarResponsePattern.matcher(sar);
      if (!matcher.matches()) {
        throw new AlarisSmsHttpException(String.format("can't match sar: [%s] to SAR_TEMPLATE: [%s]", sar, SAR_TEMPLATE));
      }
      String id = matcher.group("id");
      try {
        int parts = Integer.parseInt(matcher.group("parts"));
        int partNumber = Integer.parseInt(matcher.group("partNumber"));
        alarisSmsSar = new AlarisHttpSms.Sar(id, parts, partNumber);
      } catch (NumberFormatException e) {
        throw new AlarisSmsHttpException("Bad parts or partNumber parameter", e);
      }
    }

    Matcher nullCharacterMatcher = nullCharacterPattern.matcher(message);
    if (nullCharacterMatcher.find()) {
      logger.warn("[{}] - find NULL(0x0) character at message [{}], delete it from message", this, message);
      message = nullCharacterMatcher.replaceAll("");
    }

    SMSEncoding messageEncoding = SMSCodec.getTextEncoding(message);
    if (alarisSmsSar != null && message.length() > messageEncoding.getConcatLength()) {
      throw new AlarisSmsHttpException(String.format("Incorrect message length [%d], it must be lower or equal [%d], for encoding [%s]", message.length(), messageEncoding.getLength(), messageEncoding));
    }

    UUID uuid = alarisSmsHttpRestService.sendHttpSms(ani, dnis, message, serviceType, longMessageMode, dataCoding, alarisSmsSar, dlrUrl, username, clientIp);
    logger.debug("[{}] - end process submit command, message_id: [{}]", this, uuid);
    return "{\"message_id\": \"" + uuid + "\"}";
  }

  private String processQueryCommand(final MultivaluedMap<String, String> queryParams) {
    logger.debug("[{}] - try process query command, queryParams: {}", this, queryParams);
    String messageId = getQueryParam(queryParams, "messageId");

    UUID smsId;
    try {
      smsId = UUID.fromString(messageId);
    } catch (IllegalArgumentException ignore) {
      throw new AlarisSmsHttpException("Incorrect message ID format");
    }

    String status = alarisSmsHttpRestService.getSmsStatus(smsId);
    logger.debug("[{}] - end process query command messageId: [{}], status: [{}]", this, messageId, status);
    return "{\"status\":\"" + (status != null ? status : "message ID " + messageId + " not found") + "\"}";
  }

  private String getQueryParam(final MultivaluedMap<String, String> queryParams, final String param) {
    String res = queryParams.getFirst(param);
    if (res == null) {
      throw new AlarisSmsHttpException(String.format("Can't get param: [%s] from query", param));
    }
    return res;
  }

}
