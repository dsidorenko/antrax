/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.control.guiclient.domain.SignalQuality;

import java.io.Serializable;
import java.util.Comparator;

public class SignalQualityComparator implements Comparator<SignalQuality>, Serializable {

  private static final long serialVersionUID = -1125805797837862716L;

  @Override
  public int compare(final SignalQuality o1, final SignalQuality o2) {
    if (o1 == null) {
      return (o2 == null) ? 0 : 1;
    } else if (o2 == null) {
      return -1;
    }

    if (!o1.isSignalStrengthDetectable()) {
      return (o2.isSignalStrengthDetectable()) ? -1 : compareBER(o1, o2);
    }

    if (!o2.isSignalStrengthDetectable()) {
      return compareBER(o1, o2);
    }

    int cmp = o1.getSignalStrength() - o2.getSignalStrength();
    if (cmp == 0) {
      cmp = compareBER(o1, o2);
    }

    return cmp;
  }

  private int compareBER(final SignalQuality o1, final SignalQuality o2) {
    if (!o1.isBERDetectable()) {
      return (o2.isBERDetectable()) ? -1 : 0;
    }

    if (!o2.isBERDetectable()) {
      return 1;
    }

    return o1.getBER() - o2.getBER();
  }

}
