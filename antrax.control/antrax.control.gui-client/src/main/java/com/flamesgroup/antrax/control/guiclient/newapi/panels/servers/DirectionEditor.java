/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.Direction;

import java.awt.*;
import java.util.HashSet;
import java.util.stream.Collectors;

import javax.swing.*;

public class DirectionEditor extends JPanel {

  private static final long serialVersionUID = 7036479118938537381L;

  private final DirectionTable table = new DirectionTable();

  private Component addButton;
  private Component removeButton;
  private Component editButton;

  public DirectionEditor() {
    super(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    add(new JScrollPane(table), BorderLayout.CENTER);
    setPreferredSize(new Dimension(200, 200));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
  }

  public java.util.List<Direction> getDirections() {
    return table.getElems().stream().collect(Collectors.toList());
  }

  public void setDirections(final java.util.List<Direction> directions) {
    table.setData(directions.toArray(new Direction[directions.size()]));
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");

    retval.addActionListener(e -> {
      table.getSelectedElems().forEach(table::removeElem);
    });

    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");

    retval.addActionListener(e -> {
      HashSet<String> directions = table.getElems().stream().map(Direction::getAddress).collect(Collectors.toCollection(HashSet::new));
      Direction direction = DirectionDialog.createNewDirection(retval, directions);
      if (direction != null) {
        table.insertElem(direction);
      }
    });

    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");

    retval.addActionListener(e -> {
      Direction direction = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (direction == null) {
        return;
      }

      Direction editDirection = DirectionDialog.createEditDirection(retval, direction);
      table.updateElemAt(editDirection, selectedRow);
    });

    return retval;
  }

  private class DirectionTable extends JUpdatableTable<Direction, String> {

    private static final long serialVersionUID = -4264428104112138368L;

    DirectionTable() {
      super(new TableBuilder<Direction, String>() {
        @Override
        public String getUniqueKey(final Direction src) {
          return src.getAddress();
        }

        @Override
        public void buildRow(final Direction src, final ColumnWriter<Direction> dest) {
          dest.writeColumn(src.getAddress());
          dest.writeColumn(src.getPriority());
          dest.writeColumn(src.getCapacity());
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Address", String.class);
          columns.addColumn("Priority", Integer.class);
          columns.addColumn("Capacity", Integer.class);
        }
      });
    }
  }

}
