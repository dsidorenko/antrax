/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;

import java.awt.*;

import javax.swing.*;

public class ScriptsConflictPanel extends JPanel {

  private static final long serialVersionUID = 2076283052093862872L;
  private final CardLayout cardLayout;
  private final SimGroupScriptConflictEditorPanel simGroupConflictsEditor;
  private ConflictApplier currConflictApplier;

  private final SimGroupConflictApplier simGroupConflictApplier = new SimGroupConflictApplier();

  public ScriptsConflictPanel() {
    cardLayout = new CardLayout();
    setLayout(cardLayout);

    simGroupConflictsEditor = new SimGroupScriptConflictEditorPanel();

    add(new JPanel(), "nothing");
    add(simGroupConflictsEditor, "simGroup");
  }

  public void setConflict(final ScriptConflict conflict) {
    if (conflict == null) {
      cardLayout.show(this, "nothing");
    } else if (conflict instanceof SimGroupScriptModificationConflict) {
      currConflictApplier = simGroupConflictApplier;
      simGroupConflictsEditor.setConflict((SimGroupScriptModificationConflict) conflict);
      cardLayout.show(this, "simGroup");
    } else {
      throw new UnsupportedOperationException("Unsupported conflict type: " + conflict);
    }
  }

  public void initialize(final AntraxPluginsStore leftPS, final AntraxPluginsStore rightPS, final RegistryAccess registry) {
    simGroupConflictsEditor.initialize(leftPS, rightPS, registry);
  }

  public void applyConflict() {
    if (currConflictApplier != null) {
      currConflictApplier.applyConflict();
    }
  }

  private interface ConflictApplier {
    void applyConflict();
  }

  private class SimGroupConflictApplier implements ConflictApplier {
    @Override
    public void applyConflict() {
      simGroupConflictsEditor.applyConflict();
    }
  }

}
