/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;

public interface BeanCommunication<T> {
  T updateElem(T elem, BeansPool pool, int transactionId) throws Exception;

  void removeElem(T elem, BeansPool pool, int transactionId) throws Exception;

  /**
   * Should check whether remote server has changes, so they should be read
   *
   * @param pool
   * @param transactionId
   * @return
   * @throws NotPermittedException
   * @throws TransactionException
   * @throws StorageException
   */
  boolean checkReadRequired(BeansPool pool, int transactionId) throws Exception;

  T[] listElems(BeansPool pool, int transactionId) throws Exception;

  /**
   * Creates a new element and should not return the value unique fields of
   * which contain the {@code null} value.
   *
   * @param beansPool
   * @param transactionId
   * @return new element instance
   * @throws TransactionException
   * @throws NotPermittedException
   */
  T createElem(BeansPool beansPool, int transactionId) throws Exception;

  T copyElement(T srcElem, T dstElem);

  boolean refreshForced();
}
