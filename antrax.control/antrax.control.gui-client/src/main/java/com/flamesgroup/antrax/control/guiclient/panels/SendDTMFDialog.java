/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class SendDTMFDialog extends JDialog {

  private static final long serialVersionUID = -7135789830295450713L;

  private final JTextField txtDTMF = new JTextField(30);

  private final JButton btnSend = new JButton("Send");
  private final JButton btnSendAndClose = new JButton("Send&Close");
  private final JButton btnCancel = new JButton("Cancel");

  public SendDTMFDialog(final Window window, final ActionListener sendActionListener) {
    super(window);
    setContentPane(createPanel());
    initializeEvents(sendActionListener);
    pack();
    setResizable(false);
  }

  private JPanel createPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);

    JLabel lblDTMF = new JLabel("DTMF:", JLabel.TRAILING);
    lblDTMF.setLabelFor(txtDTMF);

    lblDTMF.setPreferredSize(new Dimension(lblDTMF.getPreferredSize().width, txtDTMF.getPreferredSize().height));
    resizeToWider(btnSend, btnSendAndClose, btnSendAndClose.getPreferredSize().height);
    resizeToWider(btnSendAndClose, btnCancel, btnSendAndClose.getPreferredSize().height);
    btnSend.setEnabled(false);
    btnSendAndClose.setEnabled(false);

    JPanel buttonPanel = new JPanel();
    buttonPanel.add(btnSend);
    buttonPanel.add(btnSendAndClose);
    buttonPanel.add(btnCancel);

    layout.putConstraint(SpringLayout.NORTH, lblDTMF, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.NORTH, txtDTMF, 5, SpringLayout.NORTH, retval);

    layout.putConstraint(SpringLayout.WEST, lblDTMF, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.WEST, txtDTMF, 5, SpringLayout.EAST, lblDTMF);

    layout.putConstraint(SpringLayout.NORTH, buttonPanel, 5, SpringLayout.SOUTH, txtDTMF);
    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, buttonPanel, 0, SpringLayout.HORIZONTAL_CENTER, retval);

    layout.putConstraint(SpringLayout.EAST, retval, 5, SpringLayout.EAST, txtDTMF);
    layout.putConstraint(SpringLayout.SOUTH, retval, 5, SpringLayout.SOUTH, buttonPanel);

    retval.add(buttonPanel);
    retval.add(lblDTMF);
    retval.add(txtDTMF);

    ActionListener listener = e -> btnCancel.doClick();
    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    retval.registerKeyboardAction(listener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  private void resizeToWider(final Component a, final Component b, final int height) {
    int maxWidth = Math.max(a.getPreferredSize().width, b.getPreferredSize().width);
    Dimension size = new Dimension(maxWidth, height);
    a.setPreferredSize(size);
    b.setPreferredSize(size);
  }

  private void initializeEvents(final ActionListener sendActionListener) {
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentShown(final ComponentEvent e) {
        txtDTMF.requestFocus();
      }
    });

    DocumentListener documentListener = new DocumentListener() {
      private void onChange() {
        if (txtDTMF.getText().length() > 0) {
          btnSend.setEnabled(true);
          btnSendAndClose.setEnabled(true);
        } else {
          btnSend.setEnabled(false);
          btnSendAndClose.setEnabled(false);
        }
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        onChange();
      }

      @Override
      public void changedUpdate(final DocumentEvent e) {
        onChange();
      }
    };
    txtDTMF.getDocument().addDocumentListener(documentListener);

    ActionListener actionListener = e -> btnSend.doClick();
    txtDTMF.addActionListener(actionListener);



    btnSend.addActionListener(sendActionListener);
    btnSendAndClose.addActionListener(sendActionListener);

    btnCancel.addActionListener(l -> defaultCloseOperation());
    btnSendAndClose.addActionListener(l -> defaultCloseOperation());
  }

  public void clear() {
    txtDTMF.setText("");
  }

  public String getDTMFText() {
    return txtDTMF.getText();
  }

  private void defaultCloseOperation() {
    if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
      setVisible(false);
    } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
      dispose();
    }
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(() -> {
      SendDTMFDialog dialog = new SendDTMFDialog(null, l -> {});
      dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
      dialog.setModalityType(ModalityType.DOCUMENT_MODAL);
      dialog.setVisible(true);
    });
  }

}
