/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.device.protocol.cudp.ExpireTime;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class ChannelConfigRenderer extends DefaultTableCellRenderer {

  private final Icon licenseCorrectIcon = IconPool.getShared("/img/channels/license_correct.png");
  private final Icon outdatedFirmwareVersionIcon = IconPool.getShared("/img/channels/outdated_firmware_version.png");
  private final Icon ipConfigParametersInvalidIcon = IconPool.getShared("/img/channels/ip_config_parameters_invalid.png");
  private final Icon licenseInvalidIcon = IconPool.getShared("/img/channels/license_invalid.png");
  private final Icon licenseExpireIcon = IconPool.getShared("/img/channels/license_expire.png");
  private final Icon licenseExpiringIcon = IconPool.getShared("/img/channels/license_expiring.png");
  private final Icon setIPConfigErrorIcon = IconPool.getShared("/img/channels/set_ip_config_error.png");

  private static final long serialVersionUID = 6537161093039714442L;

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value instanceof Pair<?, ?>) {
      setText(null);
      String text;

      ChannelConfig channelConfig = (ChannelConfig) ((Pair) value).first();
      if (channelConfig == null) {
        setIcon(null);
        setToolTipText(null);
        setText(null);
        return component;
      }
      ChannelConfig.ChannelState channelState = channelConfig.getChannelState();
      String expireTimeString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(channelConfig.getExpireTime().getValueInDate());
      if (channelConfig.getExpireTime().isUnlimited()) {
        text = channelState.getValue() + "; expire time=unlimited";
      } else {
        text = channelState.getValue() + "; expire time=" + expireTimeString;
      }

      switch (channelState) {
        case ACTIVE:
          Calendar calendar = new GregorianCalendar();
          calendar.setTimeInMillis((Long) ((Pair) value).second());
          calendar.add(Calendar.DAY_OF_YEAR, 3);

          ExpireTime expireTime = channelConfig.getExpireTime();
          if (channelConfig.getExpireTime().isUnlimited() || expireTime.getValue() > calendar.getTimeInMillis()) {
            setIcon(licenseCorrectIcon);
          } else {
            text = "the license of the channel expires after 3 days; expire time=" + expireTimeString;
            setIcon(licenseExpiringIcon);
          }
          break;
        case LICENSE_INVALID:
          setIcon(licenseInvalidIcon);
          break;
        case LICENSE_EXPIRE:
          setIcon(licenseExpireIcon);
          break;
        case IP_CONFIG_PARAMETERS_INVALID:
          setIcon(ipConfigParametersInvalidIcon);
          break;
        case SET_IP_CONFIG_ERROR:
          setIcon(setIPConfigErrorIcon);
          break;
        case LICENSE_LOCK:
          setIcon(outdatedFirmwareVersionIcon);
          break;
      }

      setToolTipText(text);
    } else {
      setIcon(null);
      setToolTipText(null);
      setText(null);
    }

    return component;
  }

}
