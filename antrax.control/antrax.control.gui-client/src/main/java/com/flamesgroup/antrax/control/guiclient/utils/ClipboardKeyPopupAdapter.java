/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class ClipboardKeyPopupAdapter extends KeyAdapter implements PopupMenuListener {

  private static final String LINE_BREAK = "\n";
  private static final String CELL_BREAK = "\t";
  private static final Clipboard CLIPBOARD = Toolkit.getDefaultToolkit().getSystemClipboard();

  private final JTable table;
  private final JPopupMenu popupMenu;

  private ClipboardKeyPopupAdapter(final JTable table, final JPopupMenu popupMenu) {
    this.table = table;
    this.popupMenu = popupMenu;
  }

  public static void addKeyPopupListeners(final JTable table) {
    JPopupMenu popupMenu = new JPopupMenu();
    ClipboardKeyPopupAdapter clipboardKeyPopupAdapter = new ClipboardKeyPopupAdapter(table, popupMenu);

    table.setComponentPopupMenu(popupMenu);
    table.addKeyListener(clipboardKeyPopupAdapter);

    popupMenu.add(new CopyAction(table));
    popupMenu.addPopupMenuListener(clipboardKeyPopupAdapter);
  }

  @Override
  public void keyReleased(final KeyEvent event) {
    if (event.isControlDown() && event.getKeyCode() == KeyEvent.VK_C) {
      if (table.getCellEditor() != null) {
        table.getCellEditor().cancelCellEditing();
      }

      int numCols = table.getSelectedColumnCount();
      int numRows = table.getSelectedRowCount();
      int[] rowsSelected = table.getSelectedRows();
      int[] colsSelected = table.getSelectedColumns();
      if (numRows != rowsSelected[rowsSelected.length - 1] - rowsSelected[0] + 1 || numRows != rowsSelected.length ||
          numCols != colsSelected[colsSelected.length - 1] - colsSelected[0] + 1 || numCols != colsSelected.length) {

        JOptionPane.showMessageDialog(null, "Invalid Copy Selection", "Invalid Copy Selection", JOptionPane.ERROR_MESSAGE);
        return;
      }

      StringBuilder excelStr = new StringBuilder();
      for (int i = 0; i < numRows; i++) {
        for (int j = 0; j < numCols; j++) {
          excelStr.append(escape(table.getValueAt(rowsSelected[i], colsSelected[j])));
          if (j < numCols - 1) {
            excelStr.append(CELL_BREAK);
          }
        }
        excelStr.append(LINE_BREAK);
      }

      StringSelection sel = new StringSelection(excelStr.toString());
      CLIPBOARD.setContents(sel, sel);
    }
  }

  private String escape(final Object cell) {
    return cell.toString().replace(LINE_BREAK, " ").replace(CELL_BREAK, " ");
  }

  @Override
  public void popupMenuWillBecomeVisible(final PopupMenuEvent e) {
    SwingUtilities.invokeLater(() -> {
      int rowAtPoint = table.rowAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), table));
      int colAtPoint = table.columnAtPoint(SwingUtilities.convertPoint(popupMenu, new Point(0, 0), table));
      if (rowAtPoint > -1 && colAtPoint > -1) {
        table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
        table.setColumnSelectionInterval(colAtPoint, colAtPoint);
      }
    });
  }

  @Override
  public void popupMenuWillBecomeInvisible(final PopupMenuEvent e) {
  }

  @Override
  public void popupMenuCanceled(final PopupMenuEvent e) {
  }

  private static class CopyAction extends AbstractAction {

    private static final long serialVersionUID = -7979863300143203310L;

    private final JTable table;

    private CopyAction(final JTable table) {
      this.table = table;
      putValue(NAME, "Copy");
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
      int row = table.getSelectedRow();
      int col = table.getSelectedColumn();
      if (row > -1 && col > -1) {
        Object valueAt = table.getValueAt(row, col);
        if (valueAt != null) {
          StringSelection sel = new StringSelection(valueAt.toString());
          CLIPBOARD.setContents(sel, sel);
        }
      }
    }

  }

}
