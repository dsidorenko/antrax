/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class ServerConfigTableTooltips {

  private ServerConfigTableTooltips() {
  }

  public static String getNameTooltip() {
    return new TooltipHeaderBuilder("Name of server").build();
  }

  public static String getPublicInterfaceTooltip() {
    return new TooltipHeaderBuilder("Ip address of server").build();
  }

  public static String getVoiceServerTooltip() {
    return new TooltipHeaderBuilder("Voice server is enabled on this server").build();
  }

  public static String getSimServerTooltip() {
    return new TooltipHeaderBuilder("Sim server is enabled on this server").build();
  }

  public static String getAudioCaptureTooltip() {
    return new TooltipHeaderBuilder("Audio capture is enabled on this voice server").build();
  }

}
