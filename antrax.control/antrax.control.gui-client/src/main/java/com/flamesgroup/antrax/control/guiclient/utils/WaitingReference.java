/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import java.util.concurrent.atomic.AtomicReference;

public class WaitingReference<V> {

  private final AtomicReference<V> retval;
  private Throwable caught;

  public WaitingReference(final V value) {
    retval = new AtomicReference<>(value);
  }

  public V get() {
    return retval.get();
  }

  public void setAndRelease(final V value) {
    synchronized (retval) {
      retval.set(value);
      retval.notifyAll();
    }
  }

  public void release() {
    synchronized (retval) {
      retval.notifyAll();
    }
  }

  public void release(final Throwable caught) {
    synchronized (retval) {
      this.caught = caught;
      retval.notifyAll();
    }
  }

  public void waitValue() {
    synchronized (retval) {
      if (retval.get() == null) {
        try {
          retval.wait();
        } catch (InterruptedException ignored) {
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  public Throwable getErrorCaught() {
    return caught;
  }

}
