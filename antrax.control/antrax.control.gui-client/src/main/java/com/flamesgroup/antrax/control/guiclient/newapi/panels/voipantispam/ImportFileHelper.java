/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.utils.UserProperty;

import java.awt.*;
import java.io.File;

import javax.swing.*;

public class ImportFileHelper {

  private static final UserProperty SOURCE_DIRECTORY = new UserProperty(System.getProperty("user.home"), "scripts.source-dir");


  private ImportFileHelper() {
  }

  public static File selectFileToImport(final Component component) {
    File current = new File(SOURCE_DIRECTORY.getValue());
    JFileChooser chooser = new JFileChooser();
    chooser.setDialogTitle("Select file for import");
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setAcceptAllFileFilterUsed(false);
    chooser.setSelectedFile(current);

    if (chooser.showOpenDialog(component) == JFileChooser.APPROVE_OPTION) {
      SOURCE_DIRECTORY.setValue(chooser.getCurrentDirectory().getAbsolutePath());
      return chooser.getSelectedFile();
    } else {
      return null;
    }
  }

}
