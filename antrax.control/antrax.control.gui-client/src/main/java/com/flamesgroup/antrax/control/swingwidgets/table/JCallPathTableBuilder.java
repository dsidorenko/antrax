/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.swingwidgets.table;

import com.flamesgroup.antrax.control.guiclient.panels.transfer.CallPathTransfer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.ChannelUidTCRenderer;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.storage.commons.impl.CallPath;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.commons.ChannelUID;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class JCallPathTableBuilder implements TableBuilder<CallPath, String> {

  private final JTimePeriodSelector timePeriodSelector;

  final Format formatter = new SimpleDateFormat("HH:mm:ss dd.MM.yy");
  final Calendar cal = Calendar.getInstance();
  final ChannelUidTCRenderer channelUidTCRenderer = new ChannelUidTCRenderer();

  public JCallPathTableBuilder(final JTimePeriodSelector timePeriodSelector) {
    this.timePeriodSelector = timePeriodSelector;
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    columns.addColumn("sim channel", ChannelUID.class).setRenderer(channelUidTCRenderer);
    columns.addColumn("gsm channel", ChannelUID.class).setRenderer(channelUidTCRenderer);
    columns.addColumn("simUID", String.class);
    columns.addColumn("simGroup", String.class);
    columns.addColumn("gsmGroup", String.class);
    columns.addColumn("startTime", Long.class);
    columns.addColumn("dropReason", CdrDropReason.class);
    columns.addColumn("dropCode", Short.class);
  }

  @Override
  public void buildRow(final CallPath src, final ColumnWriter<CallPath> dest) {
    dest.writeColumn(src.getSimChannelUID(), new CallPathTransfer(src, timePeriodSelector.getFromDate(), timePeriodSelector.getToDate()));
    dest.writeColumn(src.getGsmChannelUID());
    dest.writeColumn(src.getSimUID() == null ? "" : src.getSimUID().getValue());
    dest.writeColumn(src.getSimGroupName());
    dest.writeColumn(src.getGsmGroupName());
    dest.writeColumn(calculateTime(src.getStartTime()));
    dest.writeColumn(src.getCdrDropReason());
    dest.writeColumn(src.getCdrDropCode());
  }

  private String calculateTime(final long time) {
    if (time == 0) {
      return " - no - ";
    } else {
      cal.setTimeInMillis(time);
      return formatter.format(cal.getTime());
    }
  }

  @Override
  public String getUniqueKey(final CallPath src) {
    return src.toString();
  }

}
