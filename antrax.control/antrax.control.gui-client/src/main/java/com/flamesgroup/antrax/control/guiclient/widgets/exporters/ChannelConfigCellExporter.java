/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.exporters;

import com.flamesgroup.antrax.control.swingwidgets.table.CellExporter;
import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.commons.Pair;

import java.text.SimpleDateFormat;

public class ChannelConfigCellExporter implements CellExporter {

  @Override
  public String getExportValue(final Object value) {
    if (value == null) {
      return "";
    }
    String info;
    ChannelConfig channelConfig = (ChannelConfig) ((Pair<?, ?>) value).first();
    if (channelConfig == null) {
      info = "unavailable channel";
    } else {
      ChannelConfig.ChannelState channelState = channelConfig.getChannelState();
      if (channelConfig.getExpireTime().isUnlimited()) {
        info = channelState.getValue() + "; expire time=unlimited";
      } else {
        String expireTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(channelConfig.getExpireTime().getValueInDate());
        info = channelState.getValue() + "; expire time=" + expireTime;
      }
    }
    return info;
  }

}
