/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.swingwidgets.DialogUtil;
import com.flamesgroup.antrax.control.swingwidgets.editor.IPAddressField;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditPanel;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.ServerData;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class AddServerPreprocessingDialog extends JDialog {

  private static final long serialVersionUID = -4739536054137675255L;

  private JTextField serverNameField;
  private IPAddressField publicInterface;

  private JButton okButton;
  private JButton cancelButton;

  private IServerData data;
  private boolean applied = false;

  public AddServerPreprocessingDialog(final Component invoker) {
    super(DialogUtil.findParentWindow(invoker), true);
    initGUIComponents();
    setupListeners();
  }

  public IServerData getServerCreationData(final IServerData server) {
    this.data = server;
    setVisible(true);
    if (applied) {
      return data;
    }
    return null;
  }

  private void initGUIComponents() {
    setTitle("Add new server");
    getContentPane().add(createDialogPanel());
    pack();
    setLocationRelativeTo(getParent());
    setResizable(false);
  }

  private void adjustButtons() {
    JButton cb = getCancelButton();
    JButton okb = getOKButton();

    int w = Math.max(cb.getPreferredSize().width, okb.getPreferredSize().width);
    Dimension size = new Dimension(w, cb.getPreferredSize().height);
    cb.setPreferredSize(size);
    okb.setPreferredSize(size);
  }

  private Component createDialogPanel() {
    JEditPanel editorPanel = new JEditPanel();
    GroupLayout layout = new GroupLayout(editorPanel);
    editorPanel.setLayout(layout);
    editorPanel.setPreferredSize(new Dimension(420, 100));

    // Turn on automatically adding gaps between components
    layout.setAutoCreateGaps(true);

    // Turn on automatically creating gaps between components that touch
    // the edge of the container and the container.
    layout.setAutoCreateContainerGaps(true);

    JEditLabel description = new JEditLabel("Specify server public net interface");
    JEditLabel labelServerName = new JEditLabel("Server name");
    JEditLabel labelPublicInterfaceName = new JEditLabel("Public interface");
    serverNameField = new JTextField();
    publicInterface = new IPAddressField();

    GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
    hGroup.addGroup(layout.createParallelGroup()
    /*    */.addComponent(description)
    /*    */.addGroup(layout.createSequentialGroup()
    /*        */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
    /*            */.addComponent(labelServerName)
    /*            */.addComponent(labelPublicInterfaceName))
    /*        */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
    /*            */.addComponent(serverNameField)
    /*            */.addComponent(publicInterface))));
    layout.setHorizontalGroup(hGroup);

    GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
    vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
    /*    */.addComponent(description));
    vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
    /*    */.addComponent(labelServerName)
    /*    */.addComponent(serverNameField));
    vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
    /*    */.addComponent(labelPublicInterfaceName)
    /*    */.addComponent(publicInterface));
    layout.setVerticalGroup(vGroup);

    JEditPanel buttonsPanel = new JEditPanel(new FlowLayout(FlowLayout.RIGHT));
    buttonsPanel.add(getCancelButton());
    buttonsPanel.add(getOKButton());

    JPanel panel = new JPanel(new BorderLayout());
    panel.add(editorPanel, BorderLayout.CENTER);
    panel.add(buttonsPanel, BorderLayout.SOUTH);

    adjustButtons();

    return panel;
  }

  private JButton createDialogButton(final String text) {
    JButton button = new JButton(text);
    button.putClientProperty("JButton.buttonType", "textured");
    return button;
  }

  private JButton getCancelButton() {
    if (cancelButton == null) {
      cancelButton = createDialogButton("Cancel");
      cancelButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          setVisible(false);
        }
      });
    }
    return cancelButton;
  }

  private JButton getOKButton() {
    if (okButton == null) {
      okButton = createDialogButton("OK");
      okButton.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent e) {
          data.setName(serverNameField.getText());
          data.setPublicAddress(publicInterface.getAddress());
          applied = true;
          setVisible(false);
        }
      });
      okButton.setEnabled(false);
    }
    return okButton;
  }

  private void fireDataChanged() {
    boolean enable = !serverNameField.getText().trim().isEmpty();
    getOKButton().setEnabled(enable);
  }

  private void setupListeners() {
    serverNameField.getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void changedUpdate(final DocumentEvent e) {
        fireDataChanged();
      }

      @Override
      public void insertUpdate(final DocumentEvent e) {
        fireDataChanged();
      }

      @Override
      public void removeUpdate(final DocumentEvent e) {
        fireDataChanged();
      }

    });

  }

  public static void main(final String[] args) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);

    AddServerPreprocessingDialog dialog = new AddServerPreprocessingDialog(frame);
    dialog.getServerCreationData(new ServerData());
  }

}
