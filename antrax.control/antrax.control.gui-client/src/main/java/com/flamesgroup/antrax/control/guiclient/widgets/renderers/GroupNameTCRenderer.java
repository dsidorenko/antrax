/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.control.guiclient.utils.UIConstants;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public final class GroupNameTCRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 2220789496489081956L;

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value == null) {
      setText("NO GROUP");
      setForeground(isSelected ? UIConstants.COLOR_NO_GROUP.brighter() : UIConstants.COLOR_NO_GROUP);
    } else {
      setText(value.toString());
      setForeground(isSelected ? Color.BLACK.brighter() : Color.BLACK);
    }
    return component;
  }

}
