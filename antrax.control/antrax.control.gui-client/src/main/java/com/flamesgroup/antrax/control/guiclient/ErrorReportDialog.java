/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient;

import java.awt.*;
import java.awt.Desktop.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ErrorReportDialog extends JDialog {

  private static final long serialVersionUID = -6009647823452180080L;

  private static final String SHOW_DETAILED_MESSAGE = "Detailed >>";
  private static final String HIDE_DETAILED_MESSAGE = "<< Detailed";

  private final JPanel contentPanel = new JPanel();
  private final JButton detailButton;
  private final JButton mailButton;
  private final JButton cancelButton;
  private final JTextArea textAreaError = new JTextArea();

  private boolean errorDetailedMode = false;

  private boolean result = false;

  public ErrorReportDialog(final String error) {
    setResizable(false);
    setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    setAlwaysOnTop(true);
    setModal(true);
    setTitle("An error occured in antrax manager");
    setBounds(0, 0, 600, 82);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(new BorderLayout(0, 0));
    {
      Box verticalBox = Box.createVerticalBox();
      contentPanel.add(verticalBox, BorderLayout.CENTER);
      {
        JLabel messageLabel = new JLabel("Do you want to send an email with error to the flamesgroup?");
        messageLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        messageLabel.setHorizontalAlignment(SwingConstants.LEFT);
        verticalBox.add(messageLabel);
      }
      {
        Component verticalStrut = Box.createVerticalStrut(5);
        verticalBox.add(verticalStrut);
      }
      JPanel buttonPane = new JPanel();
      buttonPane.setSize(600, 50);
      verticalBox.add(buttonPane);
      {
        cancelButton = new JButton("No");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            ErrorReportDialog.this.setVisible(false);
          }
        });
      }
      {
        mailButton = new JButton("YES");
        mailButton.setActionCommand("OK");
        mailButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Action.MAIL)) {
              try {
                desktop.mail(new URI("mailto", "support@flamesgroup.com?subject=AntraxManager crash&body=" + error, null));
                result = true;
              } catch (Exception ignored) {
                result = false;
              } finally {
                ErrorReportDialog.this.setVisible(false);
              }
            }
          }
        });
      }
      getRootPane().setDefaultButton(mailButton);
      buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
      {
        Component horizontalStrut = Box.createHorizontalStrut(235);
        buttonPane.add(horizontalStrut);
      }
      buttonPane.add(mailButton);
      {
        Component horizontalStrut = Box.createHorizontalStrut(5);
        buttonPane.add(horizontalStrut);
      }
      buttonPane.add(cancelButton);
      {
        Component horizontalGlue = Box.createHorizontalGlue();
        buttonPane.add(horizontalGlue);
      }
      {
        detailButton = new JButton(SHOW_DETAILED_MESSAGE);
        detailButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        detailButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            if (errorDetailedMode) {
              ErrorReportDialog.this.setSize(600, 82);
              detailButton.setText(SHOW_DETAILED_MESSAGE);
            } else {
              ErrorReportDialog.this.setSize(600, 400);
              detailButton.setText(HIDE_DETAILED_MESSAGE);
            }
            errorDetailedMode = !errorDetailedMode;
          }
        });
        buttonPane.add(detailButton);
      }
      {
        Component verticalStrut = Box.createVerticalStrut(8);
        verticalBox.add(verticalStrut);
      }
      textAreaError.setEditable(false);
      textAreaError.setText(error);
      textAreaError.setAutoscrolls(true);
      textAreaError.setSize(590, 230);
      verticalBox.add(textAreaError);
    }
  }

  public boolean getResult() {
    return result;
  }

}
