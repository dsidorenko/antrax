/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.tablebuilders;

import com.flamesgroup.commons.ChannelUID;

import java.util.Objects;

public class SimChannelKey implements Comparable<SimChannelKey> {

  private final String gateway;
  private final ChannelUID simChannel;
  private final ChannelUID gsmChannel;

  public SimChannelKey(final String gateway, final ChannelUID simChannel, final ChannelUID gsmChannel) {
    this.gateway = gateway;
    this.simChannel = simChannel;
    this.gsmChannel = gsmChannel;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof SimChannelKey)) {
      return false;
    }
    SimChannelKey that = (SimChannelKey) object;

    return Objects.equals(gateway, that.gateway) && simChannel.equals(that.simChannel) && Objects.equals(gsmChannel, that.gsmChannel);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(gateway);
    result = prime * result + simChannel.hashCode();
    result = prime * result + Objects.hashCode(gsmChannel);
    return result;
  }

  @Override
  public int compareTo(final SimChannelKey o) {
    int cmp = compare(gateway, o.gateway);
    if (cmp == 0) {
      cmp = compare(simChannel, o.simChannel);
      if (cmp == 0) {
        cmp = compare(gsmChannel, o.gsmChannel);
      }
    }
    return cmp;
  }

  public <T extends Comparable<T>> int compare(final T o1, final T o2) {
    if (o1 == null && o2 == null) {
      return 0;
    } else if (o1 == null) {
      return 1;
    } else if (o2 == null) {
      return -1;
    } else {
      return o1.compareTo(o2);
    }
  }

}
