/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class CdrAnalyzeConfigTableTooltips {

  private CdrAnalyzeConfigTableTooltips() {
  }

  public static String getNumberTooltip() {
    return new TooltipHeaderBuilder("Type of number to which the filter configuration is set").build();
  }

  public static String getPeriodTooltip() {
    return new TooltipHeaderBuilder("The period of time in which counting calls, in minutes").build();
  }

  public static String getMaxCallPerPeriodTooltip() {
    return new TooltipHeaderBuilder("The number of calls which lead to the moving in black or white list after it").build();
  }

  public static String getListTypeTooltip() {
    return new TooltipHeaderBuilder("Type of list in which number will be moved").build();
  }

  public static String getFilterRuleTooltip() {
    return new TooltipHeaderBuilder("Rule to filter number from CDR").build();
  }

}
