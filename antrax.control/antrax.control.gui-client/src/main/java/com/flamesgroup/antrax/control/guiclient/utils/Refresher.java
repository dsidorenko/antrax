/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class Refresher extends Thread {

  private static final List<Refresher> refreshers = Collections.synchronizedList(new LinkedList<>());

  public static void interrupAll() {
    for (Refresher r : refreshers) {
      r.interrupt();
    }
    for (Refresher r : refreshers) {
      try {
        r.join();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    refreshers.clear();
  }

  private final List<RefreshingFinishedListener> listeners = new LinkedList<>();
  private BeansPool beansPool;

  /**
   * Long lasting operation
   */
  protected abstract void refreshInformation(BeansPool beansPool) throws Exception;

  /**
   * Quick operation
   */
  public abstract void updateGuiElements();

  public abstract void handleEnabled(BeansPool beansPool);

  public abstract void handleDisabled(BeansPool beansPool);

  public Refresher(final String name) {
    super(name);
    refreshers.add(this);
    start();
  }

  public void postInitialize(final BeansPool beansPool) {
    this.beansPool = beansPool;
  }

  @Override
  public final void run() {
    while (!interrupted()) {
      try {
        synchronized (this) {
          if (listeners.size() <= 0) {
            this.wait();
          }
        }
      } catch (InterruptedException ignored) {
        break;
      }

      try {
        refreshInformation(beansPool);
        fireRefreshFinished();
      } catch (InterruptedException e) {
        fireRefreshFailed(e);
        break;
      } catch (Exception e) {
        fireRefreshFailed(e);
      }
    }
  }

  private void fireRefreshFailed(final Exception e) {
    synchronized (this) {
      for (RefreshingFinishedListener l : listeners) {
        l.handleRefreshFailed(this, e);
      }
      listeners.clear();
    }
  }

  private void fireRefreshFinished() {
    synchronized (this) {
      for (RefreshingFinishedListener l : listeners) {
        l.handleRefreshFinished(this);
      }
      listeners.clear();
    }
  }

  public synchronized final void refreshData(final RefreshingFinishedListener listener) {
    this.listeners.add(listener);
    this.notifyAll();
  }
}
