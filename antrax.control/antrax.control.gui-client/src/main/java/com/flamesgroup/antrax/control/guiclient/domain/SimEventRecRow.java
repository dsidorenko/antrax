/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.domain;

import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.storage.commons.impl.SIMEventRec;

public class SimEventRecRow {

  private final SIMEventRec record;
  private CDR cdr;
  private boolean expanded;

  public SimEventRecRow(final SIMEventRec record) {
    this.record = record;
    if (record == null) {
      this.expanded = false;
    } else {
      this.expanded = record.getEvent().hasChildren();
    }
  }

  public SIMEventRec getRecord() {
    return record;
  }

  public boolean isExpanded() {
    return expanded;
  }

  public void setExpanded(final boolean expanded) {
    this.expanded = expanded;
  }

  public CDR getCDR() {
    return cdr;
  }

  public void setCDR(final CDR cdr) {
    this.cdr = cdr;
  }

}
