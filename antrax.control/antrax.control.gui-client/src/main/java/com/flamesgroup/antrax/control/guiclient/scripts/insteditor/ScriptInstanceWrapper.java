/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;

public class ScriptInstanceWrapper {
  private ScriptInstance instance;
  private int index;

  public ScriptInstanceWrapper(final ScriptDefinition def) {
    assert def != null;
    this.instance = def.createInstance();
    this.index = 0;
    // We have to update all parameters values, so that they return true on hasValue
    for (ScriptParameter param : instance.getParameters()) {
      param.setSerializedValue(param.getSerializedValue());
    }
  }

  public ScriptInstanceWrapper(final ScriptInstance scriptInstance) {
    this(scriptInstance, 0);
  }

  public ScriptInstanceWrapper(final ScriptInstance scriptInstance, final int index) {
    this.instance = scriptInstance;
    this.index = index;
  }

  public ScriptInstance getInstance() {
    return instance;
  }

  public int getIndex() {
    return index;
  }

  @Override
  public String toString() {
    return instance.getDefinition().getName();
  }

  public void setScriptInstance(final ScriptInstance instance) {
    this.instance = instance;
  }

  public static ScriptInstanceWrapper[] wrap(final ScriptInstance[] scriptInstances) {
    if (scriptInstances == null) {
      return null;
    }
    ScriptInstanceWrapper[] retval = new ScriptInstanceWrapper[scriptInstances.length];
    for (int i = 0; i < retval.length; ++i) {
      retval[i] = new ScriptInstanceWrapper(scriptInstances[i], i);
    }
    return retval;
  }

  public void setIndex(final int index) {
    this.index = index;
  }
}
