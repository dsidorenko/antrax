/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.refreshers;

import com.flamesgroup.antrax.control.communication.MobileGatewayChannelInformation;
import com.flamesgroup.antrax.control.guiclient.StatisticVoiceServersTable;
import com.flamesgroup.antrax.control.guiclient.commadapter.StatisticGatewaysAdapter;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StatisticGatewaysRefresher extends ExecutiveRefresher {

  public StatisticGatewaysRefresher(final RefresherThread refresherThread, final TransactionManager transactionManager, final Component invoker) {
    super("StatisticGatewaysRefresher", refresherThread, transactionManager, invoker);
  }

  public void initializePermanentRefresher(final StatisticVoiceServersTable statisticVoiceServersTable) {
    addPermanentExecution(new RefresherExecution() {

      private List<StatisticGatewaysAdapter> data;

      @Override
      public String describeExecution() {
        return "voice servers statistic";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        List<IServerData> servers = beansPool.getControlBean().listVoiceServers();
        List<StatisticGatewaysAdapter> adapters = new ArrayList<>(servers.size());
        for (IServerData server : servers) {
          List<MobileGatewayChannelInformation> mobileChannels = beansPool.getControlBean().listVoiceServerChannelsStatistic(server);

          Collections.sort(mobileChannels, (o1, o2) -> o1.getGSMChannelUID().compareTo(o2.getGSMChannelUID()));
          adapters.add(new StatisticGatewaysAdapter(server, mobileChannels.toArray(new MobileGatewayChannelInformation[mobileChannels.size()])));
        }

        data = adapters;
      }

      @Override
      public void updateGUIComponent() {
        statisticVoiceServersTable.refreshData(data);
      }
    });
  }

  public void resetStatistic(final IServerData server) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "reset voice servers statistic";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        beansPool.getControlBean().resetStatistic(server);
      }

      @Override
      public void updateGUIComponent() {
      }
    });
  }

}
