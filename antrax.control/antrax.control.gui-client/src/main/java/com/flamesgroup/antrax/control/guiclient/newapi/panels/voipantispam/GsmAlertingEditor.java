/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.GsmAlertingAnalyzeConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.GsmAlertingConfig;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class GsmAlertingEditor extends JPanel {

  private static final long serialVersionUID = -2808863836913304676L;

  private final JCheckBox enable = new JCheckBox("Enable");
  private final GsmAlertingTable table = new GsmAlertingTable();
  private final JScrollPane tablePane = new JScrollPane(table);

  private Component addButton;
  private Component removeButton;
  private Component editButton;
  private Component exportButton;
  private Component importButton;


  private final String[] columnToolTips = {
      GsmAlertingAnalyzeConfigTableTooltips.getNumberTooltip(),
      GsmAlertingAnalyzeConfigTableTooltips.getGsmAlertingTimeTooltip(),
      GsmAlertingAnalyzeConfigTableTooltips.getPeriodTooltip(),
      GsmAlertingAnalyzeConfigTableTooltips.getMaxRoutingRequestPerPeriodTooltip()
  };

  public GsmAlertingEditor() {
    super(new BorderLayout());

    JReflectiveBar bar = createToolbar();
    add(bar, BorderLayout.NORTH);

    setEditorVisible(false);
    enable.addItemListener(e -> setEditorVisible(e.getStateChange() == ItemEvent.SELECTED));
    add(tablePane, BorderLayout.CENTER);
    table.setPreferredScrollableViewportSize(new Dimension(table.getPreferredSize().width, 80));
    table.setFillsViewportHeight(true);
  }

  private void setEditorVisible(final boolean visible) {
    addButton.setVisible(visible);
    removeButton.setVisible(visible);
    editButton.setVisible(visible);
    tablePane.setVisible(visible);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());
    retval.addToLeft(exportButton = createExportButton());
    retval.addToLeft(importButton = createImportButton());
    retval.addToRight(enable);
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      GsmAlertingConfig gsmAlertingConfig = GsmAlertingDialog.createNewGsmAlertingConfig(retval);
      if (gsmAlertingConfig != null) {
        table.insertElem(gsmAlertingConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      GsmAlertingConfig gsmAlertingConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (gsmAlertingConfig == null) {
        return;
      }
      GsmAlertingConfig editGsmAlertingConfig = GsmAlertingDialog.createEditGsmAlertingConfig(retval, gsmAlertingConfig);
      table.updateElemAt(editGsmAlertingConfig, selectedRow);
    });

    return retval;
  }

  private Component createExportButton() {
    return new JExportCsvButton(table, "gsmAlerting");
  }

  private Component createImportButton() {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/import.png")).build();
    retval.setToolTipText("Import");
    retval.addActionListener(e -> {
      File file = ImportFileHelper.selectFileToImport(GsmAlertingEditor.this);
      if (file == null) {
        return;
      }

      try {
        List<GsmAlertingConfig> gsmAlertingConfigs = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
          if (scanner.hasNext()) {
            scanner.nextLine();
          }
          while (scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            nextLine = nextLine.replaceAll("\"", "");
            String[] split = nextLine.split(",");
            gsmAlertingConfigs.add(parseGsmAlertingConfig(split));
          }
        }

        table.clearData();
        for (GsmAlertingConfig gsmAlertingConfig : gsmAlertingConfigs) {
          table.insertElem(gsmAlertingConfig);
        }
        MessageUtils.showInfo(GsmAlertingEditor.this, MainApp.getAppName(), "Import completed");
      } catch (Exception ex) {
        MessageUtils.showError(GsmAlertingEditor.this, "Error while, import", ex);
        ex.printStackTrace();
      }
    });
    return retval;
  }

  private GsmAlertingConfig parseGsmAlertingConfig(final String[] values) {
    VoipAntiSpamNumberType numberType = VoipAntiSpamNumberType.valueOf(values[0]);
    int alertingTime = Integer.parseInt(values[1]);
    int period = Integer.parseInt(values[2]);
    int maxRoutingRequestPerPeriod = Integer.parseInt(values[3]);
    return new GsmAlertingConfig(numberType, alertingTime, period, maxRoutingRequestPerPeriod);
  }

  public boolean isGsmAlertingEnabled() {
    return enable.isSelected();
  }

  public void setGsmAlertingEnabled(final boolean enabled) {
    setEditorVisible(enabled);
    enable.setSelected(enabled);
  }

  public java.util.List<GsmAlertingConfig> getGsmAlertingConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setGsmAlertingConfigs(final java.util.List<GsmAlertingConfig> gsmAlertingConfigs) {
    table.setData(gsmAlertingConfigs.toArray(new GsmAlertingConfig[gsmAlertingConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    exportButton.setEnabled(enabled);
    importButton.setEnabled(enabled);
    enable.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class GsmAlertingTable extends JUpdatableTable<GsmAlertingConfig, Integer> {

    private static final long serialVersionUID = -4155647707894299462L;

    public GsmAlertingTable() {
      super(new TableBuilder<GsmAlertingConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final GsmAlertingConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final GsmAlertingConfig src, final ColumnWriter<GsmAlertingConfig> dest) {
          dest.writeColumn(src.getNumberType());
          dest.writeColumn(src.getAlertingTime());
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxRoutingRequestPerPeriod());
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Number", VoipAntiSpamNumberType.class);
          columns.addColumn("GSM Alerting time", Integer.class);
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max routing request per period", Integer.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = 113342782639395221L;

        public String getToolTipText(final MouseEvent e) {
          java.awt.Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }

  }

}
