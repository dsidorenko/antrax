/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.table.DefaultTableCellRenderer;

public class DateTableCellRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 1535020148995382952L;

  private DateFormat formatter;
  private final String pattern;
  private String htmlFormat;

  public DateTableCellRenderer(final String pattern) {
    super();
    this.pattern = pattern;
  }

  public void setHtmlFormat(final String htmlFormat) {
    this.htmlFormat = htmlFormat;
  }

  @Override
  public void setValue(final Object value) {
    if (formatter == null) {
      formatter = new SimpleDateFormat(pattern);
    }

    String text;
    if (value == null) {
      text = "";
    } else {
      String dateStr = formatter.format(value);
      if (htmlFormat == null) {
        text = dateStr;
      } else {
        text = String.format(htmlFormat, dateStr);
      }
    }
    setText(text);
  }

}
