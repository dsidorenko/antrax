/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.exporters;

import com.flamesgroup.antrax.control.swingwidgets.table.CellExporter;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;

public class LockArfcnCellExporter implements CellExporter {

  @Override
  public String getExportValue(final Object value) {
    if (value == null) {
      return "";
    }
    Integer arfcn = (Integer) ((Pair<?, ?>) value).first();
    ChannelUID channelUID = (ChannelUID) ((Pair<?, ?>) value).second();
    switch (channelUID.getDeviceUID().getDeviceType()) {
      case GSMB3:
      case GSMBOX4:
      case GSMBOX8:
        if (arfcn == null) {
          return "Default";
        } else {
          return String.valueOf(arfcn);
        }
      default:
        return "Unsupported";
    }
  }

}
