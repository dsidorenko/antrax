/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ScriptInstancesListEditor extends JPanel implements SharedReferenceContainer {
  private static final long serialVersionUID = -5903759486867415350L;

  private final ScriptInstancesListTable table = new ScriptInstancesListTable();
  private final ScriptInstanceEditor editor;
  private JButton addBtn;
  private JButton removeBtn;
  private JButton upBtn;

  private JButton downBtn;

  private ScriptCommonsSource scriptCommonsSource;

  private ScriptInstanceWrapper currWrapper;
  private AntraxPluginsStore pluginsStore;
  private boolean editable;

  public ScriptInstancesListEditor(final ScriptType scriptType) {
    assert scriptType != null;
    editor = new ScriptInstanceEditor(scriptType);
    editor.setEditable(false);

    JReflectiveBar toolbar = new JReflectiveBar();
    toolbar.addToLeft(getAddBtn());
    toolbar.addToLeft(getRemoveBtn());
    toolbar.addToLeft(getDownBtn());
    toolbar.addToLeft(getUpBtn());

    setLayout(new BorderLayout());
    add(toolbar, BorderLayout.NORTH);
    /*
     * JScrollPane leftScroll = new JScrollPane(table); JScrollPane
     * rightScroll = new JScrollPane(editor);
     * leftScroll.setPreferredSize(new Dimension(100, 100));
     * rightScroll.setPreferredSize(new Dimension(100, 100));
     *
     * add(new JSplitPane(JSplitPane.VERTICAL_SPLIT, leftScroll,
     * rightScroll), BorderLayout.CENTER);
     */
    add(table, BorderLayout.CENTER);
    add(editor, BorderLayout.SOUTH);

    table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(final ListSelectionEvent e) {
        selectScriptInstance(table.getSelectedElem());
      }
    });

    editor.addScriptInstanceChangeListener(new ScriptInstanceChangeListener() {

      @Override
      public void handleInstanceChanged(final ScriptInstance newInstance, final ScriptInstance oldInstance) {
        if (currWrapper != null) {
          currWrapper.setScriptInstance(newInstance);
        }
        refreshData();
      }

    });
  }

  public ScriptInstancesListEditor(final String title, final ScriptType scriptType) {
    this(scriptType);
    setBorder(BorderFactory.createTitledBorder(title));
  }

  public void initialize(final AntraxPluginsStore pluginsStore, final PropertyEditorsSource propEditors, final ScriptDefinition[] allDefinitions) {
    table.setAntraxPluginsStore(pluginsStore);
    editor.initialize(pluginsStore, propEditors, allDefinitions);
    this.pluginsStore = pluginsStore;
  }

  public void setScriptInstances(final ScriptInstance[] scriptInstances, final ScriptCommonsSource scriptCommonsSource) {
    this.scriptCommonsSource = scriptCommonsSource;
    setScriptInstances(ScriptInstanceWrapper.wrap(scriptInstances));
  }

  public ScriptInstance[] getScriptInstances() {
    Iterator<ScriptInstanceWrapper> iter = table.getElems().iterator();
    List<ScriptInstance> retval = new LinkedList<>();
    while (iter.hasNext()) {
      ScriptInstance inst = iter.next().getInstance();
      if (inst != null) {
        retval.add(inst);
      }
    }
    return retval.toArray(new ScriptInstance[retval.size()]);
  }

  @Override
  public SharedReference[] listSharedReferences() {
    List<SharedReference> retval = new LinkedList<>();
    for (ScriptInstance inst : getScriptInstances()) {
      if (inst == null) {
        continue;
      }
      for (ScriptParameter param : inst.getParameters()) {
        Object value = param.getValue(pluginsStore.getClassLoader());
        if (value instanceof SharedReference) {
          retval.add((SharedReference) value);
        }
      }
    }
    return retval.toArray(new SharedReference[retval.size()]);
  }

  @Override
  public void handleScriptCommonsRemoved(final ScriptCommons ref) {
    editor.handleScriptCommonsRemoved(ref);
  }

  public void setEditable(final boolean editable) {
    this.editable = editable;
    getAddBtn().setEnabled(editable);
    getRemoveBtn().setEnabled(editable);
    getUpBtn().setEnabled(editable);
    getDownBtn().setEnabled(editable);
    editor.setEditable(table.getSelectedElem() != null && editable);
  }

  private void setScriptInstances(final ScriptInstanceWrapper[] wrappedInstances) {
    if (wrappedInstances == null) {
      table.setData();
    } else {
      table.setData(wrappedInstances);
    }
  }

  private void selectScriptInstance(final ScriptInstanceWrapper scriptInstanceWrapper) {
    currWrapper = scriptInstanceWrapper;
    if (scriptInstanceWrapper == null) {
      editor.setScriptInstance(null, scriptCommonsSource);
      editor.setEditable(false);
    } else {
      editor.setScriptInstance(scriptInstanceWrapper.getInstance(), scriptCommonsSource);
      editor.setEditable(editable);
    }

  }

  private JButton getDownBtn() {
    if (downBtn == null) {
      downBtn = createToolbarButton(IconPool.getShared("/img/arrow_downline.png"));
      downBtn.setToolTipText("Move Script Down");
      downBtn.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          int row = table.getSelectedRow();
          if (row >= table.getRowCount() - 1) {
            return;
          }
          table.getElemAt(row).setIndex(row + 1);
          table.getElemAt(row + 1).setIndex(row);
          refreshData();
        }
      });
    }
    return downBtn;
  }

  private JButton getUpBtn() {
    if (upBtn == null) {
      upBtn = createToolbarButton(IconPool.getShared("/img/arrow_upline.png"));
      upBtn.setToolTipText("Move Script Up");
      upBtn.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          int row = table.getSelectedRow();
          if (row <= 0) {
            return;
          }
          table.getElemAt(row).setIndex(row - 1);
          table.getElemAt(row - 1).setIndex(row);
          refreshData();
        }
      });
    }
    return upBtn;
  }

  private JButton getRemoveBtn() {
    if (removeBtn == null) {
      removeBtn = createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"));
      removeBtn.setToolTipText("Remove Script");
      removeBtn.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          if (table.getSelectedElem() == null) {
            MessageUtils.showWarn(removeBtn, "Nothing to remove", "Select script you want to delete before deleting");
            return;
          }
          table.removeElem(table.getSelectedElem());
          selectScriptInstance(null);
        }
      });
    }
    return removeBtn;
  }

  private JButton getAddBtn() {
    if (addBtn == null) {
      addBtn = createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"));
      addBtn.setToolTipText("Add");
      addBtn.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          ScriptInstanceWrapper elem = new ScriptInstanceWrapper(null, table.getRowCount());
          table.insertElem(elem);
          table.selectElem(elem);
        }

      });
    }
    return addBtn;
  }

  private JButton createToolbarButton(final Icon icon) {
    return (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
  }

  private void refreshData() {
    Collection<ScriptInstanceWrapper> elems = table.getElems();
    setScriptInstances(elems.toArray(new ScriptInstanceWrapper[elems.size()]));
  }


}
