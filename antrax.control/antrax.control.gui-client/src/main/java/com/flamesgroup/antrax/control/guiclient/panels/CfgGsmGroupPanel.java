/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.GsmGroupEditInfo;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.commadapter.GsmGroupEditAdapter;
import com.flamesgroup.antrax.control.guiclient.panels.preprocessors.GsmGroupEditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.tablebuilders.GsmGroupEditAdapterRowBuilder;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.exceptions.DataSelectionException;

import java.util.Collections;
import java.util.List;

public class CfgGsmGroupPanel extends BaseEditorPanel<GsmGroupEditAdapter, Long> {

  private static final long serialVersionUID = -4652481743112090347L;

  public CfgGsmGroupPanel() {
    enableCopy(true);
  }

  @Override
  protected BeanCommunication<GsmGroupEditAdapter> createBeanCommunication() {
    return new GsmGroupBeanCommunication();
  }

  @Override
  protected EditingPreprocessor<GsmGroupEditAdapter> createEditingPreprocessor() {
    return new GsmGroupEditingPreprocessor(getTable());
  }

  @Override
  protected BaseEditorBox<GsmGroupEditAdapter> createEditorBox(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    return new GsmGroupEditorBox();
  }

  @Override
  protected TableBuilder<GsmGroupEditAdapter, Long> createTableBuilder() {
    return new GsmGroupEditAdapterRowBuilder();
  }

  @Override
  protected boolean isEditorBoxStretched() {
    return false;
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    transactionManager.addListener(new TransactionListener() {
      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        beansPool.getConfigBean().incGsmToSimGroupLinksRevision(MainApp.clientUID);
        return Collections.emptyList();
      }
    });

    super.postInitialize(refresher, transactionManager);
  }

  /**
   * Class that implements GSM group CRUD operations.
   */
  private static class GsmGroupBeanCommunication implements BeanCommunication<GsmGroupEditAdapter> {

    private SIMGroup[] lastSimGroups;

    private long lastRevision = -1;

    public synchronized SIMGroup[] getLastSimGroups() {
      return lastSimGroups;
    }

    public synchronized void setLastSimGroups(final SIMGroup[] lastSimGroups) {
      this.lastSimGroups = lastSimGroups;
    }

    @Override
    public boolean checkReadRequired(final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      long rev = pool.getConfigBean().getGSMGroupsRevision(MainApp.clientUID, transactionId);
      return rev > lastRevision;
    }

    @Override
    public GsmGroupEditAdapter createElem(final BeansPool beansPool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      GsmGroupEditInfo gsmGroupEditInfo = beansPool.getConfigBean().createGsmGroupEditInfo(MainApp.clientUID, transactionId);
      gsmGroupEditInfo.getGsmGroup().setName("New group");
      return new GsmGroupEditAdapter(gsmGroupEditInfo, getLastSimGroups());
    }

    @Override
    public GsmGroupEditAdapter[] listElems(final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      lastRevision = pool.getConfigBean().getGSMGroupsRevision(MainApp.clientUID, transactionId);
      GsmGroupEditInfo[] infos = pool.getConfigBean().listGsmGroupEditInfo(MainApp.clientUID);
      SIMGroup[] simGroups;
      try {
        simGroups = pool.getConfigBean().listSIMGroups(MainApp.clientUID);
      } catch (DataSelectionException ignored) {
        simGroups = new SIMGroup[0];
      }
      setLastSimGroups(simGroups);

      GsmGroupEditAdapter[] adapters = new GsmGroupEditAdapter[infos.length];
      int i = 0;
      for (GsmGroupEditInfo info : infos) {
        adapters[i++] = new GsmGroupEditAdapter(info, simGroups);
      }
      return adapters;
    }

    @Override
    public void removeElem(final GsmGroupEditAdapter adapter, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      pool.getConfigBean().deleteGsmGroupEditInfo(MainApp.clientUID, transactionId, adapter.getGsmGroupInfo());
    }

    @Override
    public GsmGroupEditAdapter updateElem(final GsmGroupEditAdapter adapter, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      GsmGroupEditInfo info = pool.getConfigBean().updateGsmGroupEditInfo(MainApp.clientUID, transactionId, adapter.getGsmGroupInfo());
      adapter.setGsmGroupInfo(info);
      return adapter;
    }

    @Override
    public GsmGroupEditAdapter copyElement(final GsmGroupEditAdapter srcElem, final GsmGroupEditAdapter dstElem) {
      String copyGroupName = srcElem.getGsmGroup().getName() + "-copy";
      dstElem.getGsmGroup().setName(copyGroupName);
      dstElem.getGsmGroupInfo().setLinkedSimGroups(srcElem.getGsmGroupInfo().getLinkedSimGroups());
      return dstElem;
    }

    @Override
    public boolean refreshForced() {
      return false;
    }

  }

}
