/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PropertyEditorsSource;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptInstanceEditor;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptInstancesListEditor;
import com.flamesgroup.antrax.control.guiclient.scripts.panels.ScriptCommonsEditor;
import com.flamesgroup.antrax.control.guiclient.widgets.TimeoutField;
import com.flamesgroup.antrax.control.swingwidgets.editor.JCodeEditor;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditCheckBox;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.field.StringField;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.utils.OperatorsStorage;

import java.awt.*;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.text.JTextComponent;

public class SimGroupEditorBox extends BaseEditorBox<SimGroupWrapper> {

  private static final long serialVersionUID = 5033165799322054515L;

  private JTextField name;

  private JRadioButton autoSelection;
  private JRadioButton homeSelection;
  private JRadioButton customSelection;
  private JComboBox<OperatorsStorage.Operator> customOperatorSelection;

  private TimeoutField idleAfterRegistration;
  private TimeoutField idleBetweenCalls;
  private TimeoutField idleBeforeUnregistration;
  private TimeoutField idleAfterZeroCall;
  private TimeoutField idleBeforeSelfCall;
  private TimeoutField idleAfterSelfCall;
  private JEditIntField selfDialFactor;
  private JEditIntField selfDialChance;
  private JEditIntField registrationPeriod;
  private JEditIntField registrationCount;

  private ScriptInstancesListEditor scriptsBusinessActivity;
  private ScriptInstancesListEditor scriptsActionProvider;

  private JCheckBox canBeAppointed;
  private JCheckBox enableSyntheticRinging;
  private JCheckBox fasDetection;
  private JCheckBox smsDeliveryReport;
  private JCheckBox suppressIncomingCallSignal;
  private JLabel descriptionLabel;
  private JCodeEditor description;

  private ScriptInstanceEditor scriptFactorTL;
  private ScriptInstanceEditor scriptGatewaySelectorTR;
  private ScriptInstanceEditor scriptVSFactorTM;
  private ScriptInstanceEditor scriptIncomingCallManagementBL;
  private ScriptInstanceEditor scriptActivityPeriodBM;
  private ScriptInstanceEditor scriptSessionBR;
  private ScriptInstancesListEditor scriptCallFilter;
  private ScriptInstancesListEditor scriptSmsFilters;
  private ScriptInstanceEditor scriptIMEIGenerator;

  private JPanel scriptsPanel;

  private ScriptCommonsEditor scriptsCommonsEditor;

  private JPanel configPanel;

  @Override
  protected void createGUIElements() {
    scriptsCommonsEditor = new ScriptCommonsEditor();

    name = new StringField(8, true, 1, 100000);

    autoSelection = new JRadioButton("auto");
    homeSelection = new JRadioButton("home");
    customSelection = new JRadioButton("custom");
    customOperatorSelection = createCustomOperatorSelectionComboBox();

    autoSelection.addItemListener(e -> {
      customOperatorSelection.setEnabled(false);
      customOperatorSelection.setSelectedIndex(-1);
    });
    homeSelection.addItemListener(e -> {
      customOperatorSelection.setEnabled(false);
      customOperatorSelection.setSelectedIndex(-1);
    });
    customSelection.addItemListener(e -> {
      customOperatorSelection.setEnabled(customSelection.isEnabled());
    });

    descriptionLabel = new JEditLabel("Description");

    idleAfterRegistration = new TimeoutField();
    idleBetweenCalls = new TimeoutField();
    idleBeforeUnregistration = new TimeoutField();
    idleBeforeSelfCall = new TimeoutField();
    idleAfterSelfCall = new TimeoutField();
    idleAfterZeroCall = new TimeoutField();

    selfDialFactor = new JEditIntField(0, 1000000);
    selfDialChance = new JEditIntField(0, 100);

    registrationPeriod = new JEditIntField(0, 24 * 60);
    registrationCount = new JEditIntField(0, 100000);

    canBeAppointed = new JEditCheckBox("Can be appointed");
    enableSyntheticRinging = new JEditCheckBox("Enable synthetic ringing");
    fasDetection = new JEditCheckBox("FAS Detection");
    suppressIncomingCallSignal = new JEditCheckBox("Suppress incoming call signal");
    smsDeliveryReport = new JCheckBox("Sms delivery report");

    description = createScriptEditor();

    scriptsBusinessActivity = new ScriptInstancesListEditor("Business Activity Scripts", ScriptType.BUSINESS_ACTIVITY);
    scriptsActionProvider = new ScriptInstancesListEditor("Action Provider Scripts", ScriptType.ACTION_PROVIDER);

    scriptFactorTL = new ScriptInstanceEditor("SIM Server Factor Script", ScriptType.FACTOR);
    scriptGatewaySelectorTR = new ScriptInstanceEditor("Gateway Selector Script", ScriptType.GATEWAY_SELECTOR);
    scriptVSFactorTM = new ScriptInstanceEditor("Voice Server Factor Script", ScriptType.FACTOR);
    scriptIncomingCallManagementBL = new ScriptInstanceEditor("Incoming Call Management Script", ScriptType.INCOMING_CALL_MANAGEMENT);
    scriptActivityPeriodBM = new ScriptInstanceEditor("Activity Period Script", ScriptType.ACTIVITY_PERIOD);
    scriptSessionBR = new ScriptInstanceEditor("Session Period Script", ScriptType.ACTIVITY_PERIOD);

    scriptCallFilter = new ScriptInstancesListEditor("Call Filter", ScriptType.CALL_FILTER);

    scriptSmsFilters = new ScriptInstancesListEditor("SMS Filter", ScriptType.SMS_FILTER);

    scriptIMEIGenerator = new ScriptInstanceEditor("IMEI Generator Script", ScriptType.IMEI_GENERATOR);

    scriptsCommonsEditor.registerSharedReferenceContainers(scriptsBusinessActivity, scriptsActionProvider, scriptFactorTL, scriptGatewaySelectorTR, scriptVSFactorTM,
        scriptIncomingCallManagementBL,
        scriptActivityPeriodBM, scriptSessionBR, scriptCallFilter, scriptIMEIGenerator);

    layoutGUIElements();
  }

  private void initialize(final ScriptDefinition[] definitions, final AntraxPluginsStore pluginsStore, final PropertyEditorsSource propEditors) {
    scriptsCommonsEditor.initialize(pluginsStore, propEditors);
    scriptFactorTL.initialize(pluginsStore, propEditors, definitions);

    scriptGatewaySelectorTR.initialize(pluginsStore, propEditors, definitions);
    scriptVSFactorTM.initialize(pluginsStore, propEditors, definitions);
    scriptIncomingCallManagementBL.initialize(pluginsStore, propEditors, definitions);
    scriptActivityPeriodBM.initialize(pluginsStore, propEditors, definitions);
    scriptSessionBR.initialize(pluginsStore, propEditors, definitions);
    scriptCallFilter.initialize(pluginsStore, propEditors, definitions);
    scriptSmsFilters.initialize(pluginsStore, propEditors, definitions);
    scriptIMEIGenerator.initialize(pluginsStore, propEditors, definitions);

    scriptsBusinessActivity.initialize(pluginsStore, propEditors, definitions);
    scriptsActionProvider.initialize(pluginsStore, propEditors, definitions);
  }

  private JCodeEditor createScriptEditor() {
    JCodeEditor codeEditor = new JCodeEditor();
    codeEditor.setColumns(10);
    codeEditor.setRows(10);
    return codeEditor;
  }

  private void layoutGUIElements() {
    configPanel = createConfigPanel();
    scriptsPanel = createScriptsPanel();
    GridBagLayout layout = new GridBagLayout();
    layout.columnWeights = new double[] {1, 0, 1};
    layout.columnWidths = new int[] {1, 500, 1};
    layout.rowHeights = new int[] {1, 1};
    layout.rowWeights = new double[] {0, 1};
    setLayout(layout);
    add(configPanel, new GridBagConstraints(0, 0, 3, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 1, 1));
    add(scriptsPanel, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 1, 1));
  }

  private JPanel createConfigPanel() {
    JPanel configPanel = new JPanel();
    configPanel.setOpaque(false);

    ButtonGroup group = new ButtonGroup();
    group.add(autoSelection);
    group.add(homeSelection);
    group.add(customSelection);

    JLabel labelName = new JEditLabel("Name");
    JLabel labelOperatorSelection = new JEditLabel("Operator selection: ");
    JLabel labelIdleBetweenCalls = new JEditLabel("Idle after successful call");
    JLabel labelIdleAfterZeroCalls = new JEditLabel("Idle after zero call"); // TODO: put editor into place
    JLabel labelIdleBeforeUnregistration = new JEditLabel("Idle before unregistration");
    JLabel labelIdleAfterRegistration = new JEditLabel("Idle after registration");
    JLabel labelIdleBeforeSelfCall = new JEditLabel("Idle before self call");
    JLabel labelIdleAfterSelfCall = new JEditLabel("Idle after self call");
    JLabel labelSelfDialFactor = new JEditLabel("Self dial factor");
    JLabel labelSelfDialChance = new JEditLabel("Self dial chance, %");
    JLabel labelRegistrationPeriod = new JEditLabel("Registration period, minutes");
    JLabel labelRegistrationCount = new JEditLabel("Registration count by period");

    GroupLayout layout = new GroupLayout(configPanel);
    configPanel.setLayout(layout);
    layout.setHorizontalGroup(
    /*    */layout.createParallelGroup()
    /*        */.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
    /*            */.addContainerGap()
    /*            */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
    /*                */.addGroup(layout.createSequentialGroup()
    /*                    */.addGroup(layout.createParallelGroup()
    /*                        */.addGroup(layout.createSequentialGroup()
    /*                            */.addComponent(canBeAppointed)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addComponent(enableSyntheticRinging)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addComponent(fasDetection)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addComponent(suppressIncomingCallSignal)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addComponent(smsDeliveryReport))
    /*                        */.addGroup(layout.createSequentialGroup()
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(labelName))
    /*                            */.addGap(10, 10, 10)
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(name, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)))
    /*                        */.addGroup(layout.createParallelGroup()
    /*                            */.addGroup(layout.createSequentialGroup()
    /*                                */.addComponent(labelOperatorSelection)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                                */.addComponent(autoSelection)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                                */.addComponent(homeSelection)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                                */.addComponent(customSelection)
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                                */.addComponent(customOperatorSelection)))
    /*                        */.addGroup(layout.createSequentialGroup()
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(labelRegistrationPeriod)
    /*                                */.addComponent(labelSelfDialFactor)
    /*                                */.addComponent(labelIdleBeforeUnregistration)
    /*                                */.addComponent(labelIdleBetweenCalls)
    /*                                */.addComponent(labelIdleBeforeSelfCall))
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(registrationPeriod, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(selfDialFactor, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleBeforeSelfCall, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleBeforeUnregistration, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleBetweenCalls, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(labelRegistrationCount)
    /*                                */.addComponent(labelIdleAfterZeroCalls)
    /*                                */.addComponent(labelSelfDialChance)
    /*                                */.addComponent(labelIdleAfterSelfCall)
    /*                                */.addComponent(labelIdleAfterRegistration))
    /*                            */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                            */.addGroup(layout.createParallelGroup()
    /*                                */.addComponent(registrationCount, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleAfterZeroCall, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(selfDialChance, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleAfterSelfCall, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
    /*                                */.addComponent(idleAfterRegistration, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))))
    /*                    */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 117, Short.MAX_VALUE)))
    /*            */.addContainerGap()));

    layout.linkSize(SwingConstants.HORIZONTAL, idleAfterRegistration, idleAfterSelfCall, idleBeforeSelfCall, idleBeforeUnregistration, idleBetweenCalls, selfDialChance, selfDialFactor,
        registrationPeriod, registrationCount);

    layout.setVerticalGroup(layout.createParallelGroup()
    /*    */.addGroup(layout.createSequentialGroup()
    /*        */.addContainerGap()
    /*        */.addGroup(layout.createParallelGroup()
    /*            */.addGroup(layout.createSequentialGroup()
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                    */.addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(labelName))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                    */.addComponent(labelOperatorSelection)
    /*                    */.addComponent(autoSelection)
    /*                    */.addComponent(homeSelection)
    /*                    */.addComponent(customSelection)
    /*                    */.addComponent(customOperatorSelection))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
    /*                    */.addComponent(enableSyntheticRinging)
    /*                    */.addComponent(canBeAppointed)
    /*                    */.addComponent(fasDetection)
    /*                    */.addComponent(suppressIncomingCallSignal)
    /*                    */.addComponent(smsDeliveryReport))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                    */.addComponent(labelIdleBetweenCalls)
    /*                    */.addComponent(idleBetweenCalls, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(labelIdleAfterZeroCalls)
    /*                    */.addComponent(idleAfterZeroCall, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                    */.addComponent(labelIdleBeforeUnregistration)
    /*                    */.addComponent(idleBeforeUnregistration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(idleAfterRegistration, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(labelIdleAfterRegistration))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                    */.addComponent(labelIdleBeforeSelfCall)
    /*                    */.addComponent(idleBeforeSelfCall, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(idleAfterSelfCall, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                    */.addComponent(labelIdleAfterSelfCall))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                        */.addComponent(labelSelfDialFactor)
    /*                        */.addComponent(selfDialFactor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                        */.addComponent(selfDialChance, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                        */.addComponent(labelSelfDialChance))
    /*                */.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    /*                */.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
    /*                        */.addComponent(labelRegistrationPeriod)
    /*                        */.addComponent(registrationPeriod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
    /*                        */.addComponent(labelRegistrationCount)
    /*                        */.addComponent(registrationCount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
    /*        */.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
    /*        */.addContainerGap()));
    return configPanel;
  }

  private JPanel createScriptsPanel() {
    JPanel panel = new JPanel();
    panel.setOpaque(false);
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
    panel.add(scriptsCommonsEditor);
    panel.add(scriptIMEIGenerator);
    panel.add(scriptCallFilter);
    panel.add(scriptSmsFilters);
    panel.add(scriptsBusinessActivity);
    panel.add(scriptsActionProvider);
    panel.add(scriptFactorTL);
    panel.add(scriptVSFactorTM);
    panel.add(scriptGatewaySelectorTR);
    panel.add(scriptIncomingCallManagementBL);
    panel.add(scriptActivityPeriodBM);
    panel.add(scriptSessionBR);
    panel.add(descriptionLabel);
    panel.add(ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(description));
    return panel;
  }

  @Override
  protected void readEditorsToElem(final SimGroupWrapper d) {
    SIMGroup dest = d.getSimGroup();
    dest.setName(name.getText());

    String operatorSelection;
    if (autoSelection.isSelected()) {
      operatorSelection = "auto";
    } else if (homeSelection.isSelected()) {
      operatorSelection = "home";
    } else {
      OperatorsStorage.Operator operator = (OperatorsStorage.Operator) customOperatorSelection.getSelectedItem();
      if (operator == null) {
        operatorSelection = dest.getOperatorSelection();
      } else {
        operatorSelection = ((OperatorsStorage.Operator) customOperatorSelection.getSelectedItem()).getCode();
      }
    }
    dest.setOperatorSelection(operatorSelection);

    dest.setIdleAfterRegistration(idleAfterRegistration.getTimeout());
    dest.setIdleAfterSuccessfullCall(idleBetweenCalls.getTimeout());
    dest.setIdleAfterZeroCall(idleAfterZeroCall.getTimeout());
    dest.setIdleBeforeUnregistration(idleBeforeUnregistration.getTimeout());
    dest.setIdleBeforeSelfCall(idleBeforeSelfCall.getTimeout());
    dest.setIdleAfterSelfCallTimeout(idleAfterSelfCall.getTimeout());

    dest.setSelfDialFactor(selfDialFactor.getIntValue());
    dest.setSelfDialChance(selfDialChance.getIntValue());

    dest.setRegistrationPeriod(registrationPeriod.getIntValue());
    dest.setRegistrationCount(registrationCount.getIntValue());

    dest.setCanBeAppointed(canBeAppointed.isSelected());
    dest.setSyntheticRinging(enableSyntheticRinging.isSelected());
    dest.setFASDetection(fasDetection.isSelected());
    dest.setSuppressIncomingCallSignal(suppressIncomingCallSignal.isSelected());
    dest.setSmsDeliveryReport(smsDeliveryReport.isSelected());
    dest.setDescription(description.getText());

    dest.setBusinessActivityScripts(scriptsBusinessActivity.getScriptInstances());
    dest.setActionProviderScripts(scriptsActionProvider.getScriptInstances());
    dest.setIMEIGeneratorScript(scriptIMEIGenerator.getScriptInstance());

    dest.setCallFilterScripts(scriptCallFilter.getScriptInstances());
    dest.setSmsFilterScripts(scriptSmsFilters.getScriptInstances());
    dest.setFactorScript(scriptFactorTL.getScriptInstance());
    dest.setVSFactorScript(scriptVSFactorTM.getScriptInstance());
    dest.setGWSelectorScript(scriptGatewaySelectorTR.getScriptInstance());
    dest.setIncomingCallManagementScript(scriptIncomingCallManagementBL.getScriptInstance());
    dest.setActivityPeriodScript(scriptActivityPeriodBM.getScriptInstance());
    dest.setSessionScript(scriptSessionBR.getScriptInstance());

    dest.setScriptCommons(scriptsCommonsEditor.listScriptCommons());
  }

  @Override
  protected void writeElemToEditors(final SimGroupWrapper wrapper) {
    initialize(wrapper.getDefinitions(), wrapper.getPluginsStore(), wrapper.getPropEditorsSource());
    SIMGroup group = wrapper.getSimGroup();
    name.setText(group.getName());

    String operatorSelection = group.getOperatorSelection();
    if (operatorSelection.equals("auto")) {
      autoSelection.setSelected(true);
    } else if (operatorSelection.equals("home")) {
      homeSelection.setSelected(true);
    } else {
      customSelection.setSelected(true);
      customOperatorSelection.setSelectedItem(OperatorsStorage.getInstance().findOperatorByCode(operatorSelection));
    }

    idleAfterRegistration.setTimeout(group.getIdleAfterRegistration());
    idleBetweenCalls.setTimeout(group.getIdleAfterSuccessfullCall());
    idleAfterZeroCall.setTimeout(group.getIdleAfterZeroCall());
    idleBeforeUnregistration.setTimeout(group.getIdleBeforeUnregistration());
    idleBeforeSelfCall.setTimeout(group.getIdleBeforeSelfCall());
    idleAfterSelfCall.setTimeout(group.getIdleAfterSelfCallTimeout());

    selfDialFactor.setValue(group.getSelfDialFactor());
    selfDialChance.setValue(group.getSelfDialChance());

    registrationPeriod.setValue(group.getRegistrationPeriod());
    registrationCount.setValue(group.getRegistrationCount());

    canBeAppointed.setSelected(group.canBeAppointed());
    enableSyntheticRinging.setSelected(group.isSyntheticRinging());
    fasDetection.setSelected(group.isFASDetection());
    suppressIncomingCallSignal.setSelected(group.isSuppressIncomingCallSignal());
    smsDeliveryReport.setSelected(group.isSmsDeliveryReport());
    description.setText(group.getDescription());

    scriptsCommonsEditor.setScriptCommons(group.listScriptCommons());

    // Scripts ribbon
    scriptsBusinessActivity.setScriptInstances(group.getBusinessActivityScripts(), scriptsCommonsEditor);
    scriptsActionProvider.setScriptInstances(group.getActionProviderScripts(), scriptsCommonsEditor);
    scriptIMEIGenerator.setScriptInstance(group.getIMEIGeneratorScript(), scriptsCommonsEditor);

    // Scripts
    scriptCallFilter.setScriptInstances(group.getCallFilterScripts(), scriptsCommonsEditor);
    scriptSmsFilters.setScriptInstances(group.getSmsFilterScripts(), scriptsCommonsEditor);
    scriptFactorTL.setScriptInstance(group.getFactorScript(), scriptsCommonsEditor);
    scriptVSFactorTM.setScriptInstance(group.getVSFactorScript(), scriptsCommonsEditor);
    scriptGatewaySelectorTR.setScriptInstance(group.getGWSelectorScript(), scriptsCommonsEditor);
    scriptIncomingCallManagementBL.setScriptInstance(group.getIncomingCallManagementScript(), scriptsCommonsEditor);
    scriptActivityPeriodBM.setScriptInstance(group.getActivityPeriodScript(), scriptsCommonsEditor);
    scriptSessionBR.setScriptInstance(group.getSessionScript(), scriptsCommonsEditor);

    resetCaretPosition(name);
    resetCaretPosition(description);
  }

  private void resetCaretPosition(final JTextComponent component) {
    component.setCaretPosition(0);
  }

  @Override
  public void setEditable(final boolean editable) {
    super.setEditable(editable);

    name.setEditable(editable);
    autoSelection.setEnabled(editable);
    homeSelection.setEnabled(editable);
    customSelection.setEnabled(editable);
    customOperatorSelection.setEnabled(editable && customSelection.isSelected());

    idleAfterRegistration.setEditable(editable);
    idleBetweenCalls.setEditable(editable);
    idleAfterZeroCall.setEditable(editable);
    idleBeforeUnregistration.setEditable(editable);
    idleBeforeSelfCall.setEditable(editable);
    idleAfterSelfCall.setEditable(editable);
    selfDialFactor.setEditable(editable);
    selfDialChance.setEditable(editable);
    registrationPeriod.setEditable(editable);
    registrationCount.setEditable(editable);

    canBeAppointed.setEnabled(editable);
    enableSyntheticRinging.setEnabled(editable);
    fasDetection.setEnabled(editable);
    suppressIncomingCallSignal.setEnabled(editable);
    smsDeliveryReport.setEnabled(editable);

    descriptionLabel.setEnabled(editable);
    description.setEditable(editable);
    scriptFactorTL.setEditable(editable);
    scriptCallFilter.setEditable(editable);
    scriptSmsFilters.setEditable(editable);
    scriptVSFactorTM.setEditable(editable);
    scriptGatewaySelectorTR.setEditable(editable);
    scriptIncomingCallManagementBL.setEditable(editable);
    scriptActivityPeriodBM.setEditable(editable);
    scriptSessionBR.setEditable(editable);

    scriptsBusinessActivity.setEditable(editable);
    scriptsActionProvider.setEditable(editable);
    scriptIMEIGenerator.setEditable(editable);

    scriptsCommonsEditor.setEditable(editable);
  }

  @Override
  protected void clearCustomComponent(final Component component) {
    super.clearCustomComponent(component);

    if (component instanceof ScriptInstanceEditor) {
      ((ScriptInstanceEditor) component).setScriptInstance(null, scriptsCommonsEditor);
    } else if (component instanceof ScriptInstancesListEditor) {
      ((ScriptInstancesListEditor) component).setScriptInstances(null, scriptsCommonsEditor);
    } else if (component == configPanel) {
      canBeAppointed.setSelected(false);
      enableSyntheticRinging.setSelected(false);
      name.setText("");
      autoSelection.setSelected(false);
      homeSelection.setSelected(false);
      customSelection.setSelected(false);
      customOperatorSelection.setSelectedIndex(-1);
      selfDialFactor.setValue(0);
      idleBeforeSelfCall.setTimeout(null);
      idleBeforeUnregistration.setTimeout(null);
      idleBetweenCalls.setTimeout(null);
      idleAfterZeroCall.setTimeout(null);
      selfDialChance.setValue(0);
      idleAfterSelfCall.setTimeout(null);
      idleAfterRegistration.setTimeout(null);
      registrationPeriod.setValue(0);
      registrationCount.setValue(0);
    } else if (component == scriptsPanel) {
      description.setText("");
      scriptsCommonsEditor.clear();
      scriptFactorTL.setScriptInstance(null, scriptsCommonsEditor);
      scriptVSFactorTM.setScriptInstance(null, scriptsCommonsEditor);
      scriptGatewaySelectorTR.setScriptInstance(null, scriptsCommonsEditor);
      scriptIncomingCallManagementBL.setScriptInstance(null, scriptsCommonsEditor);
      scriptActivityPeriodBM.setScriptInstance(null, scriptsCommonsEditor);
      scriptSessionBR.setScriptInstance(null, scriptsCommonsEditor);
      scriptsBusinessActivity.setScriptInstances(null, scriptsCommonsEditor);
      scriptsActionProvider.setScriptInstances(null, scriptsCommonsEditor);
      scriptIMEIGenerator.setScriptInstance(null, scriptsCommonsEditor);
      scriptCallFilter.setScriptInstances(null, scriptsCommonsEditor);
      scriptSmsFilters.setScriptInstances(null, scriptsCommonsEditor);
    }
  }

  @Override
  protected Object getCustomComponentValue(final Component component) {
    if (component instanceof JCodeEditor) {
      JCodeEditor codeEditor = (JCodeEditor) component;
      return codeEditor.getText();
    } else if (component instanceof ScriptInstanceEditor) {
      ScriptInstanceEditor instanceEditor = (ScriptInstanceEditor) component;
      return new InstanceParametersInfo(instanceEditor.getScriptInstance());
    } else if (component instanceof ScriptInstancesListEditor) {
      ScriptInstancesListEditor ribbon = (ScriptInstancesListEditor) component;
      return new InstanceParametersInfo(ribbon.getScriptInstances());
    } else if (component instanceof ScriptCommonsEditor) {
      ScriptCommonsEditor editor = (ScriptCommonsEditor) component;
      return new ScriptCommonsValue(editor.listScriptCommons());
    }
    return super.getCustomComponentValue(component);
  }

  private JComboBox<OperatorsStorage.Operator> createCustomOperatorSelectionComboBox() {
    JComboBox<OperatorsStorage.Operator> comboBox = new JComboBox<>();
    comboBox.setRenderer(new DefaultListCellRenderer() {

      private static final long serialVersionUID = -4342132784719818695L;

      @Override
      public Component getListCellRendererComponent(final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus) {
        if (value instanceof OperatorsStorage.Operator) {
          return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        } else {
          JPanel separatorPanel = new JPanel(new BorderLayout());
          separatorPanel.add(super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus), BorderLayout.CENTER);
          separatorPanel.add(new JSeparator(), BorderLayout.SOUTH);
          return separatorPanel;
        }
      }
    });

    final AtomicReference<String> previousMCC = new AtomicReference<>();
    OperatorsStorage.getInstance().getOperators().forEach(operator -> {
      String mcc = operator.getCode().substring(0, 3);
      if (previousMCC.get() == null) {
        previousMCC.set(mcc);
      }

      if (!previousMCC.get().equals(mcc)) {
        comboBox.addItem(null);
      }

      previousMCC.set(mcc);
      comboBox.addItem(operator);
    });

    comboBox.setPreferredSize(comboBox.getPreferredSize());
    comboBox.setMinimumSize(comboBox.getPreferredSize());
    comboBox.setMaximumSize(comboBox.getPreferredSize());
    return comboBox;
  }

  /**
   * Script instance values buffer
   */
  private static class InstanceParametersInfo {

    private final String[] definitions;
    private final byte[][][] serializedParams;

    public InstanceParametersInfo(final ScriptInstance... scripts) {
      this.definitions = new String[scripts.length];
      this.serializedParams = new byte[scripts.length][][];
      for (int i = 0; i < definitions.length; ++i) {
        if (scripts[i] == null) {
          definitions[i] = null;
        } else {
          definitions[i] = scripts[i].getDefinition().getName();
          ScriptParameter[] parameters = scripts[i].getParameters();
          serializedParams[i] = new byte[parameters.length][];
          for (int j = 0; j < parameters.length; ++j) {
            serializedParams[i][j] = parameters[j].getSerializedValue();
          }
        }
      }
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
        return true;
      }

      if (obj instanceof InstanceParametersInfo) {
        InstanceParametersInfo that = (InstanceParametersInfo) obj;

        if (this.definitions == null) {
          return that.definitions == null;
        }
        if (that.definitions == null) {
          return false;
        }
        if (this.definitions.length != that.definitions.length) {
          return false;
        }

        for (int i = 0; i < this.definitions.length; i++) {
          if (this.definitions[i] == null) {
            if (that.definitions[i] != null) {
              return false;
            } else {
              continue;
            }
          }

          if (!this.definitions[i].equals(that.definitions[i])) {
            return false;
          }

          if (!equals(this.serializedParams, that.serializedParams)) {
            return false;
          }

        }
        return true;
      }

      return false;
    }

    private boolean equals(final byte[][][] a, final byte[][][] b) {
      if (a == null && b == null) {
        return true;
      }
      if (a == null || b == null) {
        return false;
      }
      if (a.length != b.length) {
        return false;
      }
      for (int i = 0; i < a.length; ++i) {
        if (!equals(a[i], b[i])) {
          return false;
        }
      }
      return true;
    }

    private boolean equals(final byte[][] a, final byte[][] b) {
      if (a == null && b == null) {
        return true;
      }
      if (a == null || b == null) {
        return false;
      }
      if (a.length != b.length) {
        return false;
      }
      for (int i = 0; i < a.length; ++i) {
        if (!Arrays.equals(a[i], b[i])) {
          return false;
        }
      }
      return true;
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(definitions) + Arrays.hashCode(serializedParams);
    }

    @Override
    public String toString() {
      return getClass() + " " + Arrays.toString(definitions);
    }
  }

  public class ScriptCommonsValue {

    private final ScriptCommons[] commons;

    public ScriptCommonsValue(final ScriptCommons[] commons) {
      this.commons = commons;
    }

    @Override
    public boolean equals(final Object obj) {
      if (!(obj instanceof ScriptCommonsValue)) {
        return false;
      }
      ScriptCommonsValue that = (ScriptCommonsValue) obj;
      if (this.commons.length != that.commons.length) {
        return false;
      }
      for (int i = 0; i < commons.length; ++i) {
        if (!equals(this.commons[i], that.commons[i])) {
          return false;
        }
      }
      return true;
    }

    private boolean equals(final ScriptCommons a, final ScriptCommons b) {
      if (a == null) {
        return b == null;
      } else if (b == null) {
        return false;
      }
      return a.getName().equals(b.getName()) && Arrays.equals(a.getSerializedValue(), b.getSerializedValue());
    }

    @Override
    public int hashCode() {
      return Arrays.hashCode(commons);
    }

  }

}
