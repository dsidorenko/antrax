/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.communication.SimViewData;
import com.flamesgroup.antrax.control.communication.SmsHistory;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.panels.transfer.SmsPanelTransfer;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.ClipboardKeyPopupAdapter;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.control.transfer.AntraxTransferHandler;
import com.flamesgroup.unit.ICCID;

import java.awt.*;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.swing.*;

public class SmsHistoryPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = -7666435805903615020L;

  private static final long MULTI_DIFF_TIME = 100;
  private static final SmsHistory[] EMPTY_SMS_LIST = new SmsHistory[0];

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final JTimePeriodSelector timePeriodSelector = new JTimePeriodSelector();
  private final JUpdatableTable<SmsHistory, String> table = new JUpdatableTable<>(new SmsHistoryTableBuilder(timePeriodSelector));
  private final TextField simUIDs = new TextField();
  private final TextField clientIp = new TextField();
  private final JEditLabel totalCertainTime = new JEditLabel("0");
  private final JEditLabel totalAllTime = new JEditLabel("0");
  private final Map<String, SmsHistory> smsHistory = new HashMap<>();
  private long lastImport;

  public class SmsHistoryPanelTransferHandler extends AntraxTransferHandler {

    private static final long serialVersionUID = 2976696116372063102L;

    public void importObject(final SimViewData simViewData) {
      ICCID uid = simViewData.getUid();
      if (uid == null) {
        return;
      }
      if ((System.currentTimeMillis() - MULTI_DIFF_TIME) > lastImport) {
        simUIDs.setText(uid.getValue());
      } else {
        simUIDs.setText(simUIDs.getText() + "," + uid.getValue());
      }
      lastImport = System.currentTimeMillis();
    }
  }

  public SmsHistoryPanel() {
    setLayout(new BorderLayout());
    table.setName(getClass().getSimpleName() + "_SMS");
    table.getUpdatableTableProperties().restoreProperties();
    table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(table);
    JReflectiveBar searchPanel = new JReflectiveBar();
    searchPanel.addToRight(searchField);
    JExportCsvButton jExportCsvButton = new JExportCsvButton(table, "sms_history");
    jExportCsvButton.setText("Export");
    searchPanel.addToLeft(jExportCsvButton);

    JPanel headerPanel = new JPanel();
    headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.Y_AXIS));
    headerPanel.add(searchPanel);

    JReflectiveBar firstControlPanel = new JReflectiveBar();

    timePeriodSelector.setOpaque(false);
    simUIDs.setColumns(20);
    clientIp.setColumns(20);
    JEditLabel lblSimUID = new JEditLabel("SIM UID(s)");
    lblSimUID.setToolTipText("SIM UIDs separated by comma(',')");

    firstControlPanel.addToLeft(timePeriodSelector);
    firstControlPanel.addToLeft(lblSimUID);
    firstControlPanel.addToLeft(simUIDs);
    firstControlPanel.addToLeft(new JEditLabel("Client IP"));
    firstControlPanel.addToLeft(clientIp);
    JButton findButton = new JButton("Find");
    firstControlPanel.addToLeft(findButton);

    headerPanel.add(firstControlPanel);

    JReflectiveBar secondControlPanel = new JReflectiveBar();
    JButton clearButton = new JButton("Clear");
    secondControlPanel.addToLeft(clearButton);
    JButton removeButton = new JButton("Remove");
    secondControlPanel.addToLeft(removeButton);
    headerPanel.add(secondControlPanel);

    add(headerPanel, BorderLayout.NORTH);
    add(new JScrollPane(table), BorderLayout.CENTER);

    JReflectiveBar bottomPanel = new JReflectiveBar();
    JEditLabel totalCertainTimeLabel = new JEditLabel("Total of certain time: ");
    Font f = totalCertainTimeLabel.getFont();
    totalCertainTimeLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
    bottomPanel.addToRight(totalCertainTimeLabel);
    bottomPanel.addToRight(totalCertainTime);

    JEditLabel totalAllTimeLabel = new JEditLabel("Total of all time: ");
    totalAllTimeLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
    bottomPanel.addToRight(totalAllTimeLabel);
    bottomPanel.addToRight(totalAllTime);

    add(bottomPanel, BorderLayout.SOUTH);

    findButton.addActionListener(e -> {
      refresherThread.get().addRefresher(new ActionRefresher<SmsHistory[]>("find sms history", refresherThread.get(), getHandler()) {

        @Override
        protected SmsHistory[] performAction(final BeansPool beansPool) throws Exception {
          Date fromDate = timePeriodSelector.getFromDate();
          Date toDate = timePeriodSelector.getToDate();
          List<String> iccids = Arrays.asList(simUIDs.getText().trim().split(","));
          iccids = iccids.stream().map(String::trim).filter(e -> !e.isEmpty()).collect(Collectors.toList());
          iccids.forEach(ICCID::new);// check on valid ICCID
          simUIDs.setText("");

          iccids.forEach(el -> smsHistory.putIfAbsent(el, new SmsHistory(el, 0, 0)));

          List<SmsHistory> smsHistories = beansPool.getControlBean().listSmsHistory(fromDate, toDate, smsHistory.keySet().stream().collect(Collectors.toList()), clientIp.getText());

          smsHistories.forEach(el-> smsHistory.put(el.getIccid(), el));
          
          return smsHistory.values().toArray(new SmsHistory[0]);
        }
      });
      refresherThread.get().forceRefresh();
    });

    clearButton.addActionListener(e -> {
      table.clearData();
      smsHistory.clear();
    });

    removeButton.addActionListener(e -> {
      List<SmsHistory> selectedElems = table.getSelectedElems();
      selectedElems.forEach(el -> {
        table.removeElem(el);
        smsHistory.remove(el.getIccid());
      });
    });

    setTransferHandler(new SmsHistoryPanelTransferHandler());

    ClipboardKeyPopupAdapter.addKeyPopupListeners(table);
  }

  private ActionCallbackHandler<SmsHistory[]> getHandler() {
    return new ActionCallbackHandler<SmsHistory[]>() {

      private SmsHistory[] sms;

      @Override
      public void onRefreshUI(final Refresher refresher) {
        table.setData(sms);
        List<SmsHistory> smsHistories = Arrays.asList(sms);
        int totalForCertainTime = smsHistories.stream().mapToInt(SmsHistory::getAmountForCertainTime).sum();
        int totalForAllTime = smsHistories.stream().mapToInt(SmsHistory::getNumberOfAllTime).sum();
        totalCertainTime.setText(String.valueOf(totalForCertainTime));
        totalAllTime.setText(String.valueOf(totalForAllTime));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final SmsHistory[] result) {
        sms = result;
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(SmsHistoryPanel.this, "Error", caught.get(0));
        sms = EMPTY_SMS_LIST;
      }

    };

  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {

  }

  @Override
  public void setActive(final boolean active) {

  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
  }

  @Override
  public void release() {
    table.getUpdatableTableProperties().saveProperties();
  }

  private class SmsHistoryTableBuilder implements TableBuilder<SmsHistory, String> {

    private final JTimePeriodSelector timePeriodSelector;

    private SmsHistoryTableBuilder(final JTimePeriodSelector timePeriodSelector) {
      this.timePeriodSelector = timePeriodSelector;
    }

    @Override
    public String getUniqueKey(final SmsHistory src) {
      return src.getIccid();
    }

    @Override
    public void buildRow(final SmsHistory src, final ColumnWriter<SmsHistory> dest) {
      dest.writeColumn(src.getIccid(), new SmsPanelTransfer(new ICCID(src.getIccid()), timePeriodSelector.getFromDate(), timePeriodSelector.getToDate()));
      dest.writeColumn(src.getAmountForCertainTime());
      dest.writeColumn(src.getNumberOfAllTime());
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      columns.addColumn("ICCID", String.class);
      columns.addColumn("The amount for a certain time", Integer.class);
      columns.addColumn("The number of all time", Integer.class);
    }

  }

}
