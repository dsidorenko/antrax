/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.editors.DefaultPropertyTableRenderer;
import com.flamesgroup.antrax.automation.meta.NotFoundScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;

public class ScriptInstancesListTableBuilder implements TableBuilder<ScriptInstanceWrapper, Integer> {

  private AntraxPluginsStore pluginsStore;

  public void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
    this.pluginsStore = pluginsStore;
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    TableCellRenderer renderer = wrap(new DefaultPropertyTableRenderer<>());
    columns.addColumn("Script", String.class).setPreferredWidth(100).setRenderer(renderer);
    columns.addColumn("Values", String.class).setPreferredWidth(500).setRenderer(renderer);
  }

  private TableCellRenderer wrap(final DefaultPropertyTableRenderer<Object> renderer) {
    return new TableCellRenderer() {
      @Override
      public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
        return renderer.getRendererComponent(value);
      }
    };
  }

  @Override
  public void buildRow(final ScriptInstanceWrapper src, final ColumnWriter<ScriptInstanceWrapper> dest) {
    if (src.getInstance() == null || src.getInstance().getDefinition() instanceof NotFoundScriptDefinition) {
      dest.writeColumn("-");
      dest.writeColumn("-");
    } else {
      dest.writeColumn(src.getInstance().getDefinition().getName());
      dest.writeColumn(writeParams(src.getInstance().getParameters()));
    }
  }

  @Override
  public Integer getUniqueKey(final ScriptInstanceWrapper src) {
    return src.getIndex();
  }

  private Object writeParams(final ScriptParameter[] parameters) {
    StringBuilder str = new StringBuilder();

    str.append("<html><body>");

    for (ScriptParameter p : parameters) {
      str.append(p.getDefinition().getName());
      str.append(" = \"");
      str.append("<b>");
      str.append(p.getValue(pluginsStore.getClassLoader()));
      str.append("</b>");
      str.append("\"");
      str.append(" ");
    }

    str.append("</html></body>");
    return str.toString();
  }

}
