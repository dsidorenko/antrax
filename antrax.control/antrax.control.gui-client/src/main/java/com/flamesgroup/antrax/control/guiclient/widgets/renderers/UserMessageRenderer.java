/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.control.guiclient.panels.SimRuntimeUserMessageData;
import com.flamesgroup.commons.TimeUtils;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class UserMessageRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 61011351381323555L;

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    if (value instanceof SimRuntimeUserMessageData) {
      SimRuntimeUserMessageData data = (SimRuntimeUserMessageData) value;
      if (data.toString() != null) {
        setText(data.toString());
      } else {
        setText("");
      }
      setToolTipString(data);
    }

    return component;
  }

  private void setToolTipString(final SimRuntimeUserMessageData data) {
    StringBuilder sb = new StringBuilder();
    String msg1 = data.toString();
    String msg2 = data.getUserMsg2();
    String msg3 = data.getUserMsg3();
    if (msg2 == null && msg1 == null && msg3 == null) {
      return;
    }
    sb.append("<html><body>");
    appendMsg(data.getUserMsg1Timeout(), sb, msg1);
    appendMsg(data.getUserMsg2Timeout(), sb, msg2);
    appendMsg(data.getUserMsg3Timeout(), sb, msg3);
    sb.append("</body></html>");
    setToolTipText(sb.toString());
  }

  private void appendMsg(final long l, final StringBuilder sb, final String msg) {
    if (msg != null) {
      sb.append("<b>").append(TimeUtils.writeTime(l)).append(" </b>");
      sb.append(msg);
      sb.append("<br>");
    }
  }

}
