/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.CdrAnalyzeConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.CdrConfig;
import com.flamesgroup.commons.voipantispam.CdrFilterRule;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class CdrEditor extends JPanel {

  private static final long serialVersionUID = -5714193512328327620L;

  private final JCheckBox enable = new JCheckBox("Enable");
  private final CdrTable table = new CdrTable();
  private final JScrollPane tablePane = new JScrollPane(table);

  private Component addButton;
  private Component removeButton;
  private Component editButton;
  private Component exportButton;
  private Component importButton;

  private static final Pattern ASR_RULE_PATTERN = Pattern.compile("ASR: (?<asr>\\d+)%");
  private static final Pattern ACD_RULE_PATTERN = Pattern.compile("ACD: (?<acd>\\d+) sec");

  private final String[] columnToolTips = {
      CdrAnalyzeConfigTableTooltips.getNumberTooltip(),
      CdrAnalyzeConfigTableTooltips.getPeriodTooltip(),
      CdrAnalyzeConfigTableTooltips.getMaxCallPerPeriodTooltip(),
      CdrAnalyzeConfigTableTooltips.getListTypeTooltip(),
      CdrAnalyzeConfigTableTooltips.getFilterRuleTooltip()
  };

  public CdrEditor() {
    super(new BorderLayout());

    JReflectiveBar bar = createToolbar();
    add(bar, BorderLayout.NORTH);

    setEditorVisible(false);
    enable.addItemListener(e -> setEditorVisible(e.getStateChange() == ItemEvent.SELECTED));
    add(tablePane, BorderLayout.CENTER);
    table.setPreferredScrollableViewportSize(new Dimension(table.getPreferredSize().width, 80));
    table.setFillsViewportHeight(true);
  }

  private void setEditorVisible(final boolean visible) {
    addButton.setVisible(visible);
    removeButton.setVisible(visible);
    editButton.setVisible(visible);
    tablePane.setVisible(visible);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());
    retval.addToLeft(exportButton = createExportButton());
    retval.addToLeft(importButton = createImportButton());
    retval.addToRight(enable);
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      CdrConfig cdrConfig = CdrDialog.createNewCdrConfig(retval);
      if (cdrConfig != null) {
        table.insertElem(cdrConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      CdrConfig cdrConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (cdrConfig == null) {
        return;
      }
      CdrConfig editCdrConfig = CdrDialog.createEditCdrConfig(retval, cdrConfig);
      table.updateElemAt(editCdrConfig, selectedRow);
    });
    return retval;
  }

  private Component createExportButton() {
    return new JExportCsvButton(table, "cdr");
  }

  private Component createImportButton() {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/import.png")).build();
    retval.setToolTipText("Import");
    retval.addActionListener(e -> {
      File file = ImportFileHelper.selectFileToImport(CdrEditor.this);
      if (file == null) {
        return;
      }

      try {
        List<CdrConfig> cdrConfigs = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
          if (scanner.hasNext()) {
            scanner.nextLine();
          }
          while (scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            nextLine = nextLine.replaceAll("\"", "");
            String[] split = nextLine.split(",");
            cdrConfigs.add(parseCdrConfig(split));
          }
        }

        table.clearData();
        for (CdrConfig cdrConfig : cdrConfigs) {
          table.insertElem(cdrConfig);
        }
        MessageUtils.showInfo(CdrEditor.this, MainApp.getAppName(), "Import completed");
      } catch (Exception ex) {
        MessageUtils.showError(CdrEditor.this, "Error while, import", ex);
        ex.printStackTrace();
      }
    });
    return retval;
  }

  private CdrConfig parseCdrConfig(final String[] values) {
    VoipAntiSpamNumberType numberType = VoipAntiSpamNumberType.valueOf(values[0]);
    int period = Integer.parseInt(values[1]);
    int maxCallCount = Integer.parseInt(values[2]);
    CdrConfig.CdrListType listType = CdrConfig.CdrListType.valueOf(values[3]);
    CdrFilterRule cdrFilterRule = parseCdrFilterRule(values[4]);
    return new CdrConfig(numberType, period, maxCallCount, listType, cdrFilterRule);
  }

  private CdrFilterRule parseCdrFilterRule(final String rule) {
    CdrFilterRule.CdrFilterRuleType ruleType;
    int minAsr = 0;
    int minAcd = 0;
    if (rule.contains("ASR") && rule.contains("ACD")) {
      ruleType = CdrFilterRule.CdrFilterRuleType.ASR_AND_ACD;
      Matcher matcher = ASR_RULE_PATTERN.matcher(rule);
      if (matcher.find()) {
        minAsr = Integer.parseInt(matcher.group("asr"));
      } else {
        throw new IllegalArgumentException("Incorrect value for cdr filter rule: " + rule);
      }
      matcher = ACD_RULE_PATTERN.matcher(rule);
      if (matcher.find()) {
        minAcd = Integer.parseInt(matcher.group("acd"));
      } else {
        throw new IllegalArgumentException("Incorrect value for cdr filter rule: " + rule);
      }
    } else if (rule.contains("ASR")) {
      ruleType = CdrFilterRule.CdrFilterRuleType.ASR;
      Matcher matcher = ASR_RULE_PATTERN.matcher(rule);
      if (matcher.matches()) {
        minAsr = Integer.parseInt(matcher.group("asr"));
      } else {
        throw new IllegalArgumentException("Incorrect value for cdr filter rule: " + rule);
      }
    } else {
      ruleType = CdrFilterRule.CdrFilterRuleType.ACD;
      Matcher matcher = ACD_RULE_PATTERN.matcher(rule);
      if (matcher.matches()) {
        minAcd = Integer.parseInt(matcher.group("acd"));
      } else {
        throw new IllegalArgumentException("Incorrect value for cdr filter rule: " + rule);
      }
    }
    return new CdrFilterRule(ruleType, minAsr, minAcd);
  }


  public boolean isCdrEnabled() {
    return enable.isSelected();
  }

  public void setCdrEnabled(final boolean enabled) {
    setEditorVisible(enabled);
    enable.setSelected(enabled);
  }

  public java.util.List<CdrConfig> getCdrConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setCdrConfigs(final java.util.List<CdrConfig> cdrConfigs) {
    table.setData(cdrConfigs.toArray(new CdrConfig[cdrConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    exportButton.setEnabled(enabled);
    importButton.setEnabled(enabled);
    enable.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class CdrTable extends JUpdatableTable<CdrConfig, Integer> {

    private static final long serialVersionUID = -6794298112504477217L;

    public CdrTable() {
      super(new TableBuilder<CdrConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final CdrConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final CdrConfig src, final ColumnWriter<CdrConfig> dest) {
          dest.writeColumn(src.getNumberType());
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxCallCount());
          dest.writeColumn(src.getListType());
          dest.writeColumn(src.getFilterRule());
        }

        @Override

        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Number", VoipAntiSpamNumberType.class);
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max call per period", Integer.class);
          columns.addColumn("List Type", CdrConfig.CdrListType.class);
          columns.addColumn("Filter rule", CdrFilterRule.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = 5347856001345543454L;

        public String getToolTipText(final MouseEvent e) {
          Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }

  }

}
