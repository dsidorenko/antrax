/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;

import java.awt.*;
import java.io.File;

public class ImportJavaFilesHelper {

  private final File jarFile;
  private final Component parentComponent;
  private ScriptConflictResolver scriptConflictResolver;

  public ImportJavaFilesHelper(final File jarFile, final Component parentComponent) {
    this.jarFile = jarFile;
    this.parentComponent = parentComponent;
  }

  public void execute(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
    setupConflictsResolver(beansPool, transactionManager);
  }

  public ScriptConflictResolver getConflictsResolver() {
    return scriptConflictResolver;
  }

  private void setupConflictsResolver(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
    scriptConflictResolver = new ScriptConflictResolver(parentComponent, beansPool, transactionManager, jarFile);
  }

  public File getJarFile() {
    return jarFile;
  }

}
