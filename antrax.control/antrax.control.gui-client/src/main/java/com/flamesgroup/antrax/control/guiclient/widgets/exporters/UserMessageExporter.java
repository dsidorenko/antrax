/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.exporters;

import com.flamesgroup.antrax.control.guiclient.panels.SimRuntimeUserMessageData;
import com.flamesgroup.antrax.control.swingwidgets.table.CellExporter;
import com.flamesgroup.commons.TimeUtils;

public class UserMessageExporter implements CellExporter {

  @Override
  public String getExportValue(final Object value) {
    SimRuntimeUserMessageData data = (SimRuntimeUserMessageData) value;
    StringBuilder sb = new StringBuilder();

    String msg1 = data.toString();
    String msg2 = data.getUserMsg2();
    String msg3 = data.getUserMsg3();

    appendMsg(data.getUserMsg1Timeout(), sb, msg1);
    appendMsg(data.getUserMsg2Timeout(), sb, msg2);
    appendMsg(data.getUserMsg3Timeout(), sb, msg3);
    return sb.toString();
  }

  private void appendMsg(final long l, final StringBuilder sb, final String msg) {
    if (msg != null) {
      sb.append(TimeUtils.writeTime(l)).append(":");
      sb.append(msg);
      sb.append(";");
    }
  }

}
