/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PrimitiveTypesHelper;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PropertyEditorsSource;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.ScriptCommonsSource;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.SharedReferenceContainer;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.BooleanPropertyEditor;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.NumericPropertyEditor;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.StringPropertyEditor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

public class ScriptCommonsEditor extends JPanel implements ScriptCommonsSource {

  private static final long serialVersionUID = 3456699901808707945L;
  private final ScriptCommonsTable table;
  private PropertyEditor<?>[] editors;
  private JButton btnAdd;
  private JButton btnRemove;
  private AntraxPluginsStore pluginsStore;
  private final Logger logger = LoggerFactory.getLogger(ScriptCommonsEditor.class);
  private SharedReferenceContainer[] sharedReferenceContainer = new SharedReferenceContainer[0];

  public ScriptCommonsEditor() {
    setLayout(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    JScrollPane scrollPane = new JScrollPane(table = createTable());
    scrollPane.setPreferredSize(new Dimension(200, 200));
    add(scrollPane);
    setBorder(BorderFactory.createTitledBorder("Script Commons"));
  }

  public void initialize(final AntraxPluginsStore ps, final PropertyEditorsSource propEditors) {
    setAntraxPluginsStore(ps);
    setPropertyEditors(propEditors.listAllEditors());
  }

  public void setEditable(final boolean editable) {
    btnAdd.setEnabled(editable);
    btnRemove.setEnabled(editable);
  }

  public void registerSharedReferenceContainers(final SharedReferenceContainer... containers) {
    this.sharedReferenceContainer = containers;
  }

  public void setScriptCommons(final ScriptCommons[] scriptCommons) {
    if (scriptCommons == null) {
      table.clearData();
      return;
    }

    table.setData(scriptCommons);
  }

  public ScriptCommons[] listScriptCommons() {
    Collection<ScriptCommons> elems = table.getElems();
    return elems.toArray(new ScriptCommons[elems.size()]);
  }

  private void setPropertyEditors(final PropertyEditor<?>... editors) {
    this.editors = editors;
  }

  private void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
    this.pluginsStore = pluginsStore;
    table.setAntraxPluginsStore(pluginsStore);
  }

  @Override
  public void addScriptCommon(final ScriptCommons common) {
    table.insertElem(common);
  }

  @Override
  public ScriptCommons getCommonsByName(final String name) {
    for (ScriptCommons c : table.getElems()) {
      if (c.getName().equals(name)) {
        return c;
      }
    }
    return null;
  }

  @Override
  public ScriptCommons[] listAcceptableCommons(String type) {
    type = PrimitiveTypesHelper.primitiveToClass(type);
    List<ScriptCommons> retval = new LinkedList<>();
    try {
      Class<?> reqType = pluginsStore.getClassByName(type);
      for (ScriptCommons sc : table.getElems()) {
        if (reqType.isAssignableFrom(sc.getValue(pluginsStore.getClassLoader()).getClass())) {
          retval.add(sc);
        }
      }
    } catch (Exception e) {
      logger.warn("[{}] - failed to determine list of acceptable commons for {}", this, type, e);
    }

    return retval.toArray(new ScriptCommons[retval.size()]);
  }

  private ScriptCommonsTable createTable() {
    return new ScriptCommonsTable();
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(Box.createHorizontalBox());
    retval.addToLeft(btnAdd = createAddButton());
    retval.addToLeft(btnRemove = createRemoveButton());
    return retval;
  }

  private JButton createRemoveButton() {
    final JButton retval = createToolbarButton(IconPool.getShared("/img/buttons/button-remove.png"));
    retval.setToolTipText("Remove");

    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        ScriptCommons elem = table.getSelectedElem();
        if (elem == null) {
          MessageUtils.showWarn(retval, "Nothing to remove", "Select common you want to delete before deleting");
          return;
        }
        for (SharedReferenceContainer referenceContainer : sharedReferenceContainer) {
          if (referenceUsed(elem.getName(), referenceContainer.listSharedReferences())) {
            MessageUtils.showWarn(retval, "Remove failed", "Can't delete this reference: it is already in use");
            return;
          }
        }

        table.removeElem(elem);
        for (SharedReferenceContainer referenceContainer : sharedReferenceContainer) {
          referenceContainer.handleScriptCommonsRemoved(elem);
        }
      }

      private boolean referenceUsed(final String name, final SharedReference[] sharedReferences) {
        for (SharedReference ref : sharedReferences) {
          if (name.equals(ref.getName())) {
            return true;
          }
        }
        return false;
      }
    });

    return retval;
  }

  private JButton createAddButton() {
    final JButton retval = createToolbarButton(IconPool.getShared("/img/buttons/button-add.gif"));
    retval.setToolTipText("Add");

    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        if (editors == null) {
          JOptionPane.showMessageDialog(retval, "There is no any editors, so nothing to add");
          return;
        }
        ScriptCommons common = ScriptCommonsCreateDialog.askForCommon(retval, ScriptCommonsEditor.this, editors);
        if (common == null) {
          return;
        }

        addScriptCommon(common);
      }
    });

    return retval;
  }

  private JButton createToolbarButton(final Icon icon) {
    return (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(icon).build();
  }

  public void clear() {
    table.setData();
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    ScriptCommonsEditor editor = new ScriptCommonsEditor();
    frame.setContentPane(editor);
    editor.setPropertyEditors(new BooleanPropertyEditor(), new NumericPropertyEditor<>(Integer.class), new StringPropertyEditor());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

}
