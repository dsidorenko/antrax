/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.domain;

import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.ChannelUID;

public class ServerChannelRowKey {

  private final IServerData server;
  private final ChannelUID channelUID;

  public ServerChannelRowKey(final IServerData server, final ChannelUID channelUID) {
    this.server = server;
    this.channelUID = channelUID;
  }

  @Override
  public int hashCode() {
    return (server.toString() + channelUID.toString()).hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof ServerChannelRowKey)) {
      return false;
    }
    ServerChannelRowKey that = (ServerChannelRowKey) obj;

    return equals(server, that.server)
        && equals(channelUID, that.channelUID);
  }

  private boolean equals(final Object o1, final Object o2) {
    if (o1 == null) {
      return o2 == null;
    }
    return o1.equals(o2);
  }

  @Override
  public String toString() {
    return String.format("%s-%s", server.toString(), channelUID.toString());
  }

}
