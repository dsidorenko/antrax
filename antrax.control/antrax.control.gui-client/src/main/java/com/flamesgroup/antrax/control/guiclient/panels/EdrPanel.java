/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.ClipboardKeyPopupAdapter;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.widgets.PhoneNumberField;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.DateCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.SarCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.DateTableCellRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.SarTCRenderer;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.calendar.JTimePeriodSelector;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.impl.AlarisHttpSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSmppSms;
import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;

import java.awt.*;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;

public class EdrPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 6846205403072147719L;

  private static final AlarisSms[] EMPTY_ALARIS_SMSES = new AlarisSms[0];

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final JUpdatableTable<AlarisSms, Integer> table = new JUpdatableTable<>(new AlarisSmsTableBuilder());

  private final JTimePeriodSelector timePeriodSelector = new JTimePeriodSelector();

  private final JComboBox<String> protocolField = new JComboBox(new String[] {"SMPP", "HTTP"});

  private final JTextField sourceNumberField = new JTextField();
  private final PhoneNumberField destinationNumberField = new PhoneNumberField();

  private final JTextField senderField = new JTextField();
  private final JComboBox<String> statusField = new JComboBox();

  public EdrPanel() {
    setLayout(new BorderLayout());

    table.setName(getClass().getSimpleName() + "_Edr");
    table.getUpdatableTableProperties().restoreProperties();
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

    timePeriodSelector.setOpaque(false);

    EnumSet.allOf(AlarisSmppSms.Status.class).forEach(s -> statusField.addItem(s.name()));
    statusField.setEditable(true);
    statusField.getEditor().setItem(null);

    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(table);
    JReflectiveBar searchPanel = new JReflectiveBar();
    searchPanel.addToRight(searchField);

    JExportCsvButton exportButton = new JExportCsvButton(table, "edr");
    exportButton.setText("Export");

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToLeft(exportButton);
    bar.addToRight(searchField);

    add(bar, BorderLayout.PAGE_START);

    GridBagLayout callReportQueryLayout = new GridBagLayout();
    callReportQueryLayout.columnWidths = new int[] {0, 155, 0, 55, 0, 155, 0, 155, 0, 0, 0};
    callReportQueryLayout.rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
    callReportQueryLayout.columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
    callReportQueryLayout.rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

    JPanel smsEdrQueryPanel = new JPanel(callReportQueryLayout);
    JReflectiveBar reflectiveBar = new JReflectiveBar();
    reflectiveBar.addToLeft(timePeriodSelector);
    reflectiveBar.addToLeft(protocolField);

    smsEdrQueryPanel.add(reflectiveBar, new GridBagConstraints(0, 0, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 0, 5, 5), 0, 0));

    smsEdrQueryPanel.add(new JEditLabel("Source"), new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));
    smsEdrQueryPanel.add(sourceNumberField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    smsEdrQueryPanel.add(new JEditLabel("Destination"), new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    smsEdrQueryPanel.add(destinationNumberField, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    smsEdrQueryPanel.add(new JEditLabel("Sender"), new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    smsEdrQueryPanel.add(senderField, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    smsEdrQueryPanel.add(new JEditLabel("Status"), new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
    smsEdrQueryPanel.add(statusField, new GridBagConstraints(7, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

    JButton findButton = new JButton("Find");
    smsEdrQueryPanel.add(findButton, new GridBagConstraints(8, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

    JScrollPane smsEdrPanel = new JScrollPane(table);

    JPanel callReportPanel = new JPanel();
    callReportPanel.setLayout(new BorderLayout());

    callReportPanel.add(smsEdrQueryPanel, BorderLayout.NORTH);
    callReportPanel.add(smsEdrPanel, BorderLayout.CENTER);

    add(callReportPanel);

    findButton.addActionListener(e -> refresherThread.get().addRefresher(new ActionRefresher<AlarisSms[]>("find edr", refresherThread.get(), getHandler()) {

      @Override
      protected AlarisSms[] performAction(final BeansPool beansPool) throws Exception {
        Date fromDate = timePeriodSelector.getFromDate();
        Date toDate = timePeriodSelector.getToDate();

        String source = sourceNumberField.getText();
        String destination = destinationNumberField.getContent();
        String sender = senderField.getText();
        String statusName = (String) statusField.getEditor().getItem();
        AlarisSms.Status status = statusName.isEmpty() ? null : AlarisSms.Status.valueOf(statusName);

        String protocol = (String) protocolField.getSelectedItem();
        if (protocol.equals("SMPP")) {
          List<AlarisSmppSms> alarisSmppSmses = beansPool.getControlBean().listAlarisSmppSmses(fromDate, toDate, source, destination, sender, status);
          return alarisSmppSmses.toArray(new AlarisSms[alarisSmppSmses.size()]);
        } else if (protocol.equals("HTTP")) {
          List<AlarisHttpSms> alarisHttpSmses = beansPool.getControlBean().listAlarisHttpSmses(fromDate, toDate, source, destination, sender, status);
          return alarisHttpSmses.toArray(new AlarisSms[alarisHttpSmses.size()]);
        } else {
          return EMPTY_ALARIS_SMSES;
        }
      }

    }));

    ClipboardKeyPopupAdapter.addKeyPopupListeners(table);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refreshVoiceServers();
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
  }

  @Override
  public void release() {
    table.getUpdatableTableProperties().saveProperties();
  }

  private void refreshVoiceServers() {
    ActionCallbackHandler<String[]> handler = new ActionCallbackHandler<String[]>() {

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(EdrPanel.this, "Error", caught.get(0));
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final String[] result) {
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }

    };

    new ActionRefresher<String[]>("SmppEdrRefresher", refresherThread.get(), handler) {
      @Override
      protected String[] performAction(final BeansPool beansPool) throws Exception {
        return beansPool.getControlBean().listVoiceServers().stream().map(IServerData::getName).toArray(String[]::new);
      }
    }.execute();

  }

  private class AlarisSmsTableBuilder implements TableBuilder<AlarisSms, Integer> {

    @Override
    public Integer getUniqueKey(final AlarisSms src) {
      return src.hashCode();
    }

    @Override
    public void buildRow(final AlarisSms src, final ColumnWriter<AlarisSms> dest) {
      dest.writeColumn(src.getId());
      dest.writeColumn(src.getSourceNumber());
      dest.writeColumn(src.getDestNumber());
      if (src instanceof AlarisSmppSms) {
        dest.writeColumn(((AlarisSmppSms) src).getUsername());
      } else if (src instanceof AlarisHttpSms) {
        dest.writeColumn(((AlarisHttpSms) src).getClientIp());
      } else {
        dest.writeColumn("");
      }
      dest.writeColumn(src.getMessage());
      dest.writeColumn(src.getCreateTime());
      dest.writeColumn(src.getStatus());
      dest.writeColumn(src.getLastUpdateTime());
      dest.writeColumn(src.getSar());
      dest.writeColumn(src.getSmsId());
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      columns.addColumn("Id", UUID.class).setPreferredWidth(180);
      columns.addColumn("Src number", String.class).setMaxWidth(100).setPreferredWidth(100);
      columns.addColumn("Dst number", String.class).setMaxWidth(100).setPreferredWidth(100);
      columns.addColumn("Sender", String.class).setPreferredWidth(110);
      columns.addColumn("Message", String.class);
      columns.addColumn("Create time", String.class).setPreferredWidth(90).setCellExporter(new DateCellExporter()).setRenderer(new DateTableCellRenderer("dd.MM.yy HH:mm:ss"));
      columns.addColumn("Status", String.class).setMaxWidth(80).setPreferredWidth(80);
      columns.addColumn("Dlr time", String.class).setPreferredWidth(90).setCellExporter(new DateCellExporter()).setRenderer(new DateTableCellRenderer("dd.MM.yy HH:mm:ss"));
      columns.addColumn("Sar", AlarisSms.Sar.class).setMaxWidth(70).setPreferredWidth(70).setCellExporter(new SarCellExporter()).setRenderer(new SarTCRenderer());
      columns.addColumn("Sms id", UUID.class).setPreferredWidth(180);
    }

  }

  private ActionCallbackHandler<AlarisSms[]> getHandler() {
    return new ActionCallbackHandler<AlarisSms[]>() {

      private AlarisSms[] alarisSmses;

      @Override
      public void onRefreshUI(final Refresher refresher) {
        table.setData(alarisSmses);
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final AlarisSms[] result) {
        alarisSmses = result;
      }

      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        MessageUtils.showError(EdrPanel.this, "Error", caught.get(0));
        alarisSmses = EMPTY_ALARIS_SMSES;
      }

    };

  }

}
