/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets;

import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.control.swingwidgets.field.AbstractField;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TimeoutField extends AbstractField {
  private static final long serialVersionUID = -1206178141537465327L;

  public TimeoutField() {
    setColumns(1);
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(final KeyEvent e) {
        char c = e.getKeyChar();
        if (!((c >= '0') && (c <= '9') || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
          e.consume();
        }
      }
    });
    setRequired(true);
  }

  public void setTimeout(final Timeout value) {
    if (value == null) {
      setContent("0" + InvariableTimeout.DELIMITER + "0");
    } else {
      setContent(String.format("%d%s%d", value.getMediana(), InvariableTimeout.DELIMITER, value.getDelta()));
    }
  }

  public Timeout getTimeout() {
    try {
      return InvariableTimeout.parseTimeout(getText());
    } catch (RuntimeException ignored) {
      return new InvariableTimeout(0, 0);
    }
  }

  @Override
  protected boolean isValid(final String text) {
    String[] parts = text.split(InvariableTimeout.DELIMITER);
    try {
      int first = Integer.parseInt(parts[0].length() == 0 ? "0" : parts[0]);
      int second = Integer.parseInt(parts[1].length() == 0 ? "0" : parts[1]);
      boolean res = first >= second;
      if (res) {
        setToolTipText("");
      } else {
        setToolTipText("delta must be less or equal than mediana");
      }
      return res;
    } catch (IndexOutOfBoundsException ignored) {
      return true;
    }
  }

}
