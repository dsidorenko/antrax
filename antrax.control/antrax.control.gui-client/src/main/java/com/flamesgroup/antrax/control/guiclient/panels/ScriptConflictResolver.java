/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.meta.SimGroupScriptModificationConflict;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.scripts.panels.ScriptConflictsResolverDialog;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.plugins.core.ScriptsHelper;
import com.flamesgroup.utils.codebase.ScriptFile;

import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;

public class ScriptConflictResolver {

  private final List<ScriptConflict> resolved = new ArrayList<>();
  private final List<ScriptConflict> unresolved = new ArrayList<>();
  private final ScriptConflictsResolverDialog conflictsDialog;
  private final File jarFile;

  public ScriptConflictResolver(final Component parentComponent, final BeansPool beansPool, final TransactionManager transactionManager, final File jarFile) throws Exception {
    this.jarFile = jarFile;
    byte[] jarByteArray = Files.readAllBytes(jarFile.toPath());
    ScriptConflict[] scriptConflicts = beansPool.getConfigBean().getScriptConflicts(MainApp.clientUID, transactionManager.getGuaranteedTransactionId(), jarByteArray);
    splitConflicts(scriptConflicts);
    conflictsDialog = new ScriptConflictsResolverDialog(SwingUtilities.getWindowAncestor(parentComponent));
    AntraxPluginsStore newPS = beansPool.getConfigBean().analyzeJavaFileClasses(MainApp.clientUID, transactionManager.getGuaranteedTransactionId(), jarByteArray);
    AntraxPluginsStore oldPS;
    try {
      oldPS = beansPool.getConfigBean().getPluginsStore(MainApp.clientUID, transactionManager.getGuaranteedTransactionId());
    } catch (Exception ignored) {
      oldPS = newPS;
    }

    conflictsDialog.initialize(oldPS, newPS, beansPool.getRegistryAccess());
  }

  private void splitConflicts(final ScriptConflict[] scriptConflicts) {
    for (ScriptConflict c : scriptConflicts) {
      if (c.isFullyResolved()) {
        resolved.add(c);
      } else {
        unresolved.add(c);
      }
    }
  }

  public boolean resolveConflicts() {
    if (unresolved.size() == 0) {
      int res = JOptionPane.showConfirmDialog(null, "All conflict of scripts resolved. Apply changes?", "Scripts conflict", JOptionPane.YES_NO_OPTION);
      return res == 0;
    }
    conflictsDialog.setConflicts(unresolved.toArray(new ScriptConflict[unresolved.size()]));
    if (conflictsDialog.showDialog()) {
      resolved.addAll(Arrays.asList(conflictsDialog.getResolvedConflicts()));
      unresolved.clear();
      return true;
    }
    return false;
  }

  public void commit(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
    ScriptFile scriptFile = new ScriptFile(jarFile.getName(), Files.readAllBytes(jarFile.toPath()), ScriptsHelper.getScriptFileInfo(jarFile));
    beansPool.getConfigBean().commitCodeBase(MainApp.clientUID, transactionManager.getGuaranteedTransactionId(), scriptFile, filterSimGroupConflicts(resolved));
  }

  private SimGroupScriptModificationConflict[] filterSimGroupConflicts(final List<ScriptConflict> conflicts) {
    List<SimGroupScriptModificationConflict> retval = conflicts.stream().filter(c -> c instanceof SimGroupScriptModificationConflict)
        .map(c -> (SimGroupScriptModificationConflict) c).collect(Collectors.toCollection(LinkedList::new));
    return retval.toArray(new SimGroupScriptModificationConflict[retval.size()]);
  }
}
