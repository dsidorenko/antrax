/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.commons.voipantispam.CdrConfig;
import com.flamesgroup.commons.voipantispam.CdrFilterRule;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class CdrDialog extends JDialog {

  private static final long serialVersionUID = -57629028006345336L;

  private final JRadioButton callerNumberType;
  private final JRadioButton calledNumberType;

  private final JEditIntField period;
  private final JEditIntField maxCallPerPeriod;

  private final JRadioButton whiteList;
  private final JRadioButton blackList;

  private final JRadioButton asrRule;
  private final JRadioButton acdRule;
  private final JRadioButton asrAndAcdRule;

  private final JEditIntField minAsrField;
  private final JEditIntField minAcdField;

  private final JButton cancelButton;
  private final JButton okButton;

  private int componentsIndex;
  private boolean approved;

  public CdrDialog(final Window owner, final CdrConfig cdrConfig) {
    this(owner);

    switch (cdrConfig.getNumberType()) {
      case CALLER:
        callerNumberType.setSelected(true);
        break;
      case CALLED:
        calledNumberType.setSelected(true);
        break;
    }

    period.setValue(cdrConfig.getPeriod());
    maxCallPerPeriod.setValue(cdrConfig.getMaxCallCount());

    switch (cdrConfig.getListType()) {
      case WHITE:
        whiteList.setSelected(true);
        break;
      case BLACK:
        blackList.setSelected(true);
        break;
    }

    CdrFilterRule cdrFilterRule = cdrConfig.getFilterRule();
    switch (cdrFilterRule.getRuleType()) {
      case ASR:
        asrRule.setSelected(true);
        minAsrField.setEnabled(true);
        minAcdField.setEnabled(false);
        minAsrField.setValue(cdrFilterRule.getMinAsr());
        break;
      case ACD:
        acdRule.setSelected(true);
        minAsrField.setEnabled(false);
        minAcdField.setEnabled(true);
        minAcdField.setValue(cdrFilterRule.getMinAcd());
        break;
      case ASR_AND_ACD:
        asrAndAcdRule.setSelected(true);
        minAsrField.setEnabled(true);
        minAcdField.setEnabled(true);
        minAsrField.setValue(cdrFilterRule.getMinAsr());
        minAcdField.setValue(cdrFilterRule.getMinAcd());
        break;
    }
  }

  public CdrDialog(final Window owner) {
    super(owner);

    cancelButton = createCancelBtn();
    okButton = createOkBtn();

    GridBagLayout layout = new GridBagLayout();
    getContentPane().setLayout(layout);
    layout.rowWeights = new double[] {0, 0, 0, 1};

    ButtonGroup numberTypeGroup = new ButtonGroup();
    numberTypeGroup.add(callerNumberType = new JRadioButton("Caller(A-number)"));
    numberTypeGroup.add(calledNumberType = new JRadioButton("Called(B-number)"));
    calledNumberType.setSelected(true);

    ButtonGroup listTypeGroup = new ButtonGroup();
    listTypeGroup.add(whiteList = new JRadioButton("White"));
    listTypeGroup.add(blackList = new JRadioButton("Black"));
    blackList.setSelected(true);

    ButtonGroup filterRuleGroup = new ButtonGroup();
    filterRuleGroup.add(asrRule = new JRadioButton("ASR"));
    filterRuleGroup.add(acdRule = new JRadioButton("ACD"));
    filterRuleGroup.add(asrAndAcdRule = new JRadioButton("ASR and ACD"));

    add(new JLabel("Number: ", JLabel.RIGHT), createNumberTypeRadioButtonsPanel());

    add(new JLabel("Period: ", JLabel.RIGHT), period = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Max call per period: ", JLabel.RIGHT), maxCallPerPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("List Type: ", JLabel.RIGHT), createListTypeRadioButtonsPanel());

    add(new JLabel("Filter Rule: ", JLabel.RIGHT), createFilterRuleRadioButtonsPanel());
    add(new JLabel("Min ASR: ", JLabel.RIGHT), minAsrField = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Min ACD: ", JLabel.RIGHT), minAcdField = new JEditIntField(0, Integer.MAX_VALUE));

    add(createButtonsPanel(), new GridBagConstraints(0, componentsIndex, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

    setTitle("CDR config");
    setMinimumSize(new Dimension(350, 150));
    setResizable(false);
    pack();

    ActionListener cancelListener = e -> cancelButton.doClick();
    ActionListener okListener = e -> okButton.doClick();

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    period.addActionListener(okListener);
    maxCallPerPeriod.addActionListener(okListener);

    asrRule.addActionListener(e -> {
      minAsrField.setEnabled(true);
      minAcdField.setEnabled(false);
    });
    acdRule.addActionListener(e -> {
      minAsrField.setEnabled(false);
      minAcdField.setEnabled(true);
    });
    asrAndAcdRule.addActionListener(e -> {
      minAsrField.setEnabled(true);
      minAcdField.setEnabled(true);
    });
    asrRule.setSelected(true);
    minAsrField.setEnabled(true);
    minAcdField.setEnabled(false);
  }

  private void add(final JLabel lbl, final Component editor) {
    getContentPane().add(lbl, new GridBagConstraints(0, componentsIndex, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    getContentPane().add(editor, new GridBagConstraints(1, componentsIndex, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    componentsIndex++;
  }

  private JPanel createNumberTypeRadioButtonsPanel() {
    JPanel retval = new JPanel();
    retval.add(callerNumberType);
    retval.add(calledNumberType);
    return retval;
  }

  private JPanel createListTypeRadioButtonsPanel() {
    JPanel retval = new JPanel();
    retval.add(whiteList);
    retval.add(blackList);
    return retval;
  }

  private JPanel createFilterRuleRadioButtonsPanel() {
    JPanel retval = new JPanel();
    retval.add(asrRule);
    retval.add(acdRule);
    retval.add(asrAndAcdRule);
    return retval;
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    retval.add(okButton);
    retval.add(cancelButton);
    layout.putConstraint(SpringLayout.EAST, okButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, cancelButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, okButton, 0, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, cancelButton, 0, SpringLayout.SOUTH, retval);

    retval.setPreferredSize(okButton.getPreferredSize());
    return retval;
  }

  private JButton createCancelBtn() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(e -> setVisible(false));
    return retval;
  }

  private JButton createOkBtn() {
    final JButton retval = new JButton("OK");
    retval.addActionListener(e -> {
      approved = true;
      setVisible(false);
    });
    return retval;
  }

  private VoipAntiSpamNumberType getNumberType() {
    if (callerNumberType.isSelected()) {
      return VoipAntiSpamNumberType.CALLER;
    } else if (calledNumberType.isSelected()) {
      return VoipAntiSpamNumberType.CALLED;
    } else {
      throw new IllegalArgumentException("Incorrect selected number type");
    }
  }

  private CdrConfig.CdrListType getCdrListType() {
    if (whiteList.isSelected()) {
      return CdrConfig.CdrListType.WHITE;
    } else if (blackList.isSelected()) {
      return CdrConfig.CdrListType.BLACK;
    } else {
      throw new IllegalArgumentException("Incorrect selected cdr list type");
    }
  }

  private CdrConfig getCdrConfig() {
    if (approved) {
      return new CdrConfig(getNumberType(), period.getIntValue(), maxCallPerPeriod.getIntValue(), getCdrListType(), getCdrFilterRule());
    } else {
      return null;
    }
  }

  private CdrFilterRule getCdrFilterRule() {
    if (asrRule.isSelected()) {
      return new CdrFilterRule(CdrFilterRule.CdrFilterRuleType.ASR, minAsrField.getIntValue(), 0);
    } else if (acdRule.isSelected()) {
      return new CdrFilterRule(CdrFilterRule.CdrFilterRuleType.ACD, 0, minAcdField.getIntValue());
    } else if (asrAndAcdRule.isSelected()) {
      return new CdrFilterRule(CdrFilterRule.CdrFilterRuleType.ASR_AND_ACD, minAsrField.getIntValue(), minAcdField.getIntValue());
    } else {
      throw new IllegalArgumentException("Incorrect selected cdr filter rule");
    }
  }

  private static CdrConfig getCdrConfig(final CdrDialog cdrDialog) {
    cdrDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    cdrDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    cdrDialog.setLocationRelativeTo(cdrDialog.getOwner());
    cdrDialog.setVisible(true);
    cdrDialog.dispose();

    return cdrDialog.getCdrConfig();
  }

  public static CdrConfig createNewCdrConfig(final Component parent) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getCdrConfig(new CdrDialog(windowAncestor));
  }

  public static CdrConfig createEditCdrConfig(final Component parent, final CdrConfig cdrConfig) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getCdrConfig(new CdrDialog(windowAncestor, cdrConfig));
  }

}
