/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.tablebuilders;

import com.flamesgroup.antrax.control.guiclient.commadapter.GsmGroupEditAdapter;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.Collection;

public class GsmGroupEditAdapterRowBuilder implements TableBuilder<GsmGroupEditAdapter, Long> {

  @Override
  public void buildRow(final GsmGroupEditAdapter adpater, final ColumnWriter<GsmGroupEditAdapter> dest) {
    GSMGroup gsmGroup = adpater.getGsmGroupInfo().getGsmGroup();
    Collection<SIMGroup> linkedSimGroups = adpater.getGsmGroupInfo().getLinkedSimGroups();
    String strLinkedSimGroups = "";
    if (!linkedSimGroups.isEmpty()) {
      int i = 0;
      for (SIMGroup simGroup : linkedSimGroups) {
        if (i > 0) {
          strLinkedSimGroups += ", ";
        }
        strLinkedSimGroups += simGroup.getName();
        i++;
      }
    }

    dest.writeColumn(gsmGroup.getName(), gsmGroup, gsmGroup.getName());
    dest.writeColumn(strLinkedSimGroups);
  }

  @Override
  public Long getUniqueKey(final GsmGroupEditAdapter adapter) {
    return adapter.getGsmGroupInfo().getID();
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    columns.addColumn("GSM group", String.class).setMaxWidth(250).setPreferredWidth(250);
    columns.addColumn("Linked SIM groups", String.class);
  }

}
