/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ExecutiveRefresher extends Refresher {
  private final List<RefresherExecution> executions = new LinkedList<>();
  private final List<RefresherExecution> permanentExecutions = new LinkedList<>();
  private final List<RefresherExecution> executedExecutions = new LinkedList<>();
  private final ReentrantLock executionsLock = new ReentrantLock();
  private final List<Pair<RefresherExecution, Exception>> failedExceptions = new LinkedList<>();
  private final RefresherThread refresherThread;
  private final TransactionManager transactionManager;
  private final Component invoker;

  public ExecutiveRefresher(final String name, final RefresherThread refresherThread,
      final TransactionManager transactionManager, final Component invoker) {
    super(name);
    this.refresherThread = refresherThread;
    this.transactionManager = transactionManager;
    this.invoker = invoker;
  }

  public final void addExecution(final RefresherExecution e) {
    executionsLock.lock();
    try {
      executions.add(e);
    } finally {
      executionsLock.unlock();
    }
    refresherThread.forceRefresh();
  }

  public final void addPermanentExecution(final RefresherExecution e) {
    executionsLock.lock();
    try {
      permanentExecutions.add(e);
    } finally {
      executionsLock.unlock();
    }
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  protected final void refreshInformation(final BeansPool beansPool)
      throws Exception {
    executionsLock.lock();
    try {
      executions.addAll(permanentExecutions);
      for (RefresherExecution e : executions) {
        try {
          e.invokeExecution(beansPool, transactionManager);
        } catch (Exception exc) {
          failedExceptions
              .add(new Pair<>(e, exc));
        }
      }
      executedExecutions.addAll(executions);
      executions.clear();
    } finally {
      executionsLock.unlock();
    }
  }

  @Override
  public final void updateGuiElements() {
    List<RefresherExecution> tmpExecutions = new LinkedList<>(executedExecutions);
    executedExecutions.clear();

    for (RefresherExecution e : tmpExecutions) {
      e.updateGUIComponent();
    }

    if (!failedExceptions.isEmpty()) {
      List<Pair<RefresherExecution, Exception>> tmpFailedExceptions = new LinkedList<>(failedExceptions);
      failedExceptions.clear();

      StringBuilder msg = new StringBuilder();
      msg.append("Following executions was failed on the server: \n");

      for (Pair<RefresherExecution, Exception> e : tmpFailedExceptions) {
        msg.append(e.first().describeExecution()).append(": ").append(
            e.second()).append('\n');
      }

      MessageUtils.showWarn(invoker, "Filed executions", msg.toString());
    }
  }

}
