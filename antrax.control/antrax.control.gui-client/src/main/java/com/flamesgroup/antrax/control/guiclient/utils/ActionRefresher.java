/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;


/**
 * @param <R>
 */
public abstract class ActionRefresher<R> extends CallBackRefresher<R> {

  private final RefresherThread refresherThread;

  public ActionRefresher(final String name, final RefresherThread refresherThread, final ActionCallbackHandler<R> actionHandler) {
    super(name, actionHandler);

    if (refresherThread == null) {
      throw new IllegalStateException("Action refresher '" + name + "' refresherThread is null");
    }

    this.refresherThread = refresherThread;
  }

  public void execute() {
    refresherThread.addRefresher(this);
    refresherThread.forceRefresh();
  }

  public void breakOff() {
    if (refresherThread != null) {
      refresherThread.removeRefresher(this);
    }
    interrupt();
  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {
  }

  @Override
  protected void refreshInformation(final BeansPool beansPool) throws RuntimeException {
    try {
      if (refresherThread != null) {
        refresherThread.removeRefresher(this);
      }

      super.refreshInformation(beansPool);
    } finally {
      breakOff();
    }
  }

}
