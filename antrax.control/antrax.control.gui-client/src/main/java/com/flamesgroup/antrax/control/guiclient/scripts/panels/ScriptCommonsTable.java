/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

public class ScriptCommonsTable extends JUpdatableTable<ScriptCommons, String> {

  private static final class ScriptCommonsTableBuilder implements TableBuilder<ScriptCommons, String> {
    private AntraxPluginsStore pluginsStore;

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      columns.addColumn("Name", String.class);
      columns.addColumn("Type", String.class).setRenderer(crateClassRenderer());
      columns.addColumn("Value", Object.class).setRenderer(createPropertyRenderer());
    }

    @Override
    public void buildRow(final ScriptCommons src, final ColumnWriter<ScriptCommons> dest) {
      assert src != null;
      dest.writeColumn(src.getName());
      Object value = src.getValue(pluginsStore.getClassLoader());
      dest.writeColumn(value != null ? value.getClass().getSimpleName() : "?");
      dest.writeColumn(value);
    }

    @Override
    public String getUniqueKey(final ScriptCommons src) {
      return src.getName();
    }

    public void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
      this.pluginsStore = pluginsStore;
    }
  }

  public ScriptCommonsTable() {
    super(createTableBuilder());
    getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  private static TableBuilder<ScriptCommons, String> createTableBuilder() {
    return new ScriptCommonsTableBuilder();
  }

  public void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
    ((ScriptCommonsTableBuilder) getTableBuilder()).setAntraxPluginsStore(pluginsStore);
  }

  private static TableCellRenderer crateClassRenderer() {
    return new DefaultTableCellRenderer() {

      private static final long serialVersionUID = -2131290893166933043L;

      @Override
      public Component getTableCellRendererComponent(final JTable table, Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
        if (value instanceof Class<?>) {
          value = ((Class<?>) value).getSimpleName();
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }

    };
  }

  private static TableCellRenderer createPropertyRenderer() {
    return new DefaultTableCellRenderer() {
      private static final long serialVersionUID = 8631983904112880873L;

      @Override
      public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      }
    };
  }

  private static final long serialVersionUID = 4500544961459530631L;

}
