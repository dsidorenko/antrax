/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.ui.guiclient.widgets.table.SimGroupReferencePropertyEditor;

public class PropertyEditorsSource {
  private final PropertyEditor<?>[] editors;

  public PropertyEditorsSource(final AntraxPluginsStore pluginsStore, final RegistryAccess registryAccess) {
    assert pluginsStore != null;
    assert registryAccess != null;
    editors = pluginsStore.listPropertyEditors();
    for (PropertyEditor<?> e : editors) {
      if (e instanceof RegistryAccessListener) {
        ((RegistryAccessListener) e).setRegistryAccess(registryAccess);
      }
    }
  }

  public PropertyEditor<?> getEditorFor(final Object value, String type) {
    type = PrimitiveTypesHelper.primitiveToClass(type);
    for (PropertyEditor<?> e : editors) {
      if (e.getType().getCanonicalName().equals(type)) {
        return e;
      }
    }
    throw new IllegalStateException("Can't find editor for " + type);
  }

  public PropertyEditor<?>[] listAllEditors() {
    return editors;
  }

  public void handleGroupReferencesChanged(final SimGroupReference[] groupReferences) {
    for (PropertyEditor<?> e : editors) {
      if (e instanceof SimGroupReferencePropertyEditor) {
        SimGroupReferencePropertyEditor groupEditor = (SimGroupReferencePropertyEditor) e;
        groupEditor.setSimGroupReferences(groupReferences);
      }
    }
  }

}
