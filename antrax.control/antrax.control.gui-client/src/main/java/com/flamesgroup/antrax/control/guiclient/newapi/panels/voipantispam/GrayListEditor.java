/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CheckBoxRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.GrayListConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveButton;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.commons.voipantispam.AcdConfig;
import com.flamesgroup.commons.voipantispam.AsrConfig;
import com.flamesgroup.commons.voipantispam.GrayListConfig;
import com.flamesgroup.commons.voipantispam.VoipAntiSpamNumberType;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.table.JTableHeader;

public class GrayListEditor extends JPanel {

  private static final long serialVersionUID = -1412815553024850346L;

  private final GrayListTable table = new GrayListTable();

  private Component addButton;
  private Component removeButton;
  private Component editButton;
  private Component exportButton;
  private Component importButton;

  private final String[] columnToolTips = {
      GrayListConfigTableTooltips.getNumberTooltip(),
      GrayListConfigTableTooltips.getPeriodTooltip(),
      GrayListConfigTableTooltips.getMaxRoutingRequestPerPeriod(),
      GrayListConfigTableTooltips.getBlockPeriodTooltip(),
      GrayListConfigTableTooltips.getMaxBlockCountBeforeMoveToBlackListTooltip(),
      GrayListConfigTableTooltips.getSameCalledTooltip(),
      GrayListConfigTableTooltips.getAcdPeriodTooltip(),
      GrayListConfigTableTooltips.getMaxMinAcdCallPerPeriodTooltip(),
      GrayListConfigTableTooltips.getMinAcdTooltip(),
      GrayListConfigTableTooltips.getAsrPeriodTooltip(),
      GrayListConfigTableTooltips.getMinAsrTooltip()
  };

  public GrayListEditor() {
    super(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    JScrollPane scrollPane = new JScrollPane(table);
    table.setPreferredScrollableViewportSize(new Dimension(table.getPreferredSize().width, 100));
    table.setFillsViewportHeight(true);
    add(scrollPane, BorderLayout.CENTER);
  }

  private JReflectiveBar createToolbar() {
    JReflectiveBar retval = new JReflectiveBar();
    retval.addToLeft(addButton = createAddButton());
    retval.addToLeft(removeButton = createRemoveButton());
    retval.addToLeft(editButton = createEditButton());
    retval.addToLeft(exportButton = createExportButton());
    retval.addToLeft(importButton = createImportButton());
    return retval;
  }

  private Component createAddButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-add.gif")).build();
    retval.setToolTipText("Add");
    retval.addActionListener(e -> {
      GrayListConfig grayListConfig = GrayListDialog.createNewGrayListConfig(retval);
      if (grayListConfig != null) {
        table.insertElem(grayListConfig);
      }
    });
    return retval;
  }

  private Component createRemoveButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-remove.png")).build();
    retval.setToolTipText("Remove");
    retval.addActionListener(e -> table.getSelectedElems().forEach(table::removeElem));
    return retval;
  }

  private Component createEditButton() {
    final JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/button-edit.gif")).build();
    retval.setToolTipText("Edit");
    retval.addActionListener(e -> {
      GrayListConfig grayListConfig = table.getSelectedElem();
      int selectedRow = table.getSelectedRow();
      if (grayListConfig == null) {
        return;
      }
      GrayListConfig editGrayListConfig = GrayListDialog.createEditGrayListConfig(retval, grayListConfig);
      table.updateElemAt(editGrayListConfig, selectedRow);
    });
    return retval;
  }

  private Component createExportButton() {
    return new JExportCsvButton(table, "grayList");
  }

  private Component createImportButton() {
    JButton retval = (new JReflectiveButton.JReflectiveButtonBuilder()).setIcon(IconPool.getShared("/img/buttons/import.png")).build();
    retval.setToolTipText("Import");
    retval.addActionListener(e -> {
      File file = ImportFileHelper.selectFileToImport(GrayListEditor.this);
      if (file == null) {
        return;
      }

      try {
        List<GrayListConfig> grayListConfigs = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
          if (scanner.hasNext()) {
            scanner.nextLine();
          }
          while (scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            nextLine = nextLine.replaceAll("\"", "");
            String[] split = nextLine.split(",");
            grayListConfigs.add(parseGrayListConfig(split));
          }
        }

        table.clearData();
        for (GrayListConfig grayListConfig : grayListConfigs) {
          table.insertElem(grayListConfig);
        }
        MessageUtils.showInfo(GrayListEditor.this, MainApp.getAppName(), "Import completed");
      } catch (Exception ex) {
        MessageUtils.showError(GrayListEditor.this, "Error while, import", ex);
        ex.printStackTrace();
      }
    });
    return retval;
  }

  private GrayListConfig parseGrayListConfig(final String[] values) {
    VoipAntiSpamNumberType numberType = VoipAntiSpamNumberType.valueOf(values[0]);
    int period = Integer.parseInt(values[1]);
    int maxRoutingRequestPerPeriod = Integer.parseInt(values[2]);
    int blockPeriod = Integer.parseInt(values[3]);
    int maxBlockCountBeforeMoveToBlackList = Integer.parseInt(values[4]);
    boolean callToTheSameCalledNumbers = Boolean.parseBoolean(values[5]);

    AcdConfig acdConfig = null;
    AsrConfig asrConfig = null;
    if (values.length > 6 && values[6].length() > 0) {
      int acdPeriod = Integer.parseInt(values[6]);
      int acdMaxMinAcdCallPerPeriod = Integer.parseInt(values[7]);
      int minAcd = Integer.parseInt(values[8]);
      acdConfig = new AcdConfig(acdPeriod, acdMaxMinAcdCallPerPeriod, minAcd);
    }
    if (values.length > 9 && values[9].length() > 0) {
      int asrPeriod = Integer.parseInt(values[9]);
      int minAsr = Integer.parseInt(values[10]);
      asrConfig = new AsrConfig(asrPeriod, minAsr);
    }

    return new GrayListConfig(numberType, period, maxRoutingRequestPerPeriod, blockPeriod, maxBlockCountBeforeMoveToBlackList, callToTheSameCalledNumbers, acdConfig, asrConfig);
  }

  public java.util.List<GrayListConfig> getGrayListConfigs() {
    return new ArrayList<>(table.getElems());
  }

  public void setGrayListConfigs(final java.util.List<GrayListConfig> grayListConfigs) {
    table.setData(grayListConfigs.toArray(new GrayListConfig[grayListConfigs.size()]));
  }

  @Override
  public void setEnabled(final boolean enabled) {
    super.setEnabled(enabled);
    addButton.setEnabled(enabled);
    removeButton.setEnabled(enabled);
    editButton.setEnabled(enabled);
    exportButton.setEnabled(enabled);
    importButton.setEnabled(enabled);
    table.setEnabled(enabled);
  }

  private class GrayListTable extends JUpdatableTable<GrayListConfig, Integer> {

    private static final long serialVersionUID = -36953194482222736L;

    public GrayListTable() {
      super(new TableBuilder<GrayListConfig, Integer>() {
        @Override
        public Integer getUniqueKey(final GrayListConfig src) {
          return src.hashCode();
        }

        @Override
        public void buildRow(final GrayListConfig src, final ColumnWriter<GrayListConfig> dest) {
          dest.writeColumn(src.getNumberType());
          dest.writeColumn(src.getPeriod());
          dest.writeColumn(src.getMaxRoutingRequestPerPeriod());
          dest.writeColumn(src.getBlockPeriod());
          dest.writeColumn(src.getMaxBlockCountBeforeMoveToBlackList());

          dest.writeColumn(src.isCallToTheSameCalledNumbers());

          AcdConfig acdConfig = src.getAcdConfig();
          AsrConfig asrConfig = src.getAsrConfig();
          if (acdConfig == null && asrConfig == null) {
            return;
          }

          if (acdConfig == null) {
            dest.writeColumn(null);
            dest.writeColumn(null);
            dest.writeColumn(null);
          } else {
            dest.writeColumn(acdConfig.getPeriod());
            dest.writeColumn(acdConfig.getMaxMinAcdCallPerPeriod());
            dest.writeColumn(acdConfig.getMinAcd());
          }

          if (asrConfig != null) {
            dest.writeColumn(asrConfig.getPeriod());
            dest.writeColumn(asrConfig.getMinAsr());
          }
        }

        @Override
        public void buildColumns(final UpdateTableColumnModel columns) {
          columns.addColumn("Number", VoipAntiSpamNumberType.class);
          columns.addColumn("Period", Integer.class);
          columns.addColumn("Max routing request per period", Integer.class);
          columns.addColumn("Block period", Integer.class);
          columns.addColumn("Max block count before move to black list", Integer.class);

          columns.addColumn("Same Called(B-Numbers)", Boolean.class).setRenderer(new CheckBoxRenderer());

          columns.addColumn("Acd period", Integer.class);
          columns.addColumn("Max min acd call per period", Integer.class);
          columns.addColumn("Min acd", Integer.class);

          columns.addColumn("Asr period", Integer.class);
          columns.addColumn("Min asr", Integer.class);
        }
      });
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
      return new JTableHeader(columnModel) {
        private static final long serialVersionUID = -6175489835721540919L;

        public String getToolTipText(final MouseEvent e) {
          java.awt.Point p = e.getPoint();
          int index = columnModel.getColumnIndexAtX(p.x);
          int realIndex = columnModel.getColumn(index).getModelIndex();
          return columnToolTips[realIndex];
        }
      };
    }
  }

}
