/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public final class ImportParserHelper {

  private ImportParserHelper() {
  }

  public static Map<ICCID, PhoneNumber> parseSimUIDs(final InputStream in) throws IOException {
    int codeChar;
    Queue<ICCID> queue = new LinkedList<>();
    StringBuilder builder = new StringBuilder();

    Map<ICCID, PhoneNumber> simUIDs = new HashMap<>();
    do {
      codeChar = in.read();
      if (Character.isDigit(codeChar)) {
        builder.append((char) codeChar);
        continue;
      }

      if (builder.length() > 0) {
        ICCID uid = queue.poll();
        if (uid == null) {
          queue.offer(new ICCID(builder.toString()));
        } else {
          simUIDs.put(uid, new PhoneNumber(builder.toString()));
        }
        builder = new StringBuilder();
      }
    } while (codeChar != -1);

    return simUIDs;
  }

}
