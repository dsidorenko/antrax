/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.insteditor;

import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;

public class ScriptParamsTableBuilder implements TableBuilder<ScriptParamWrapper, Integer> {

  private ScriptCommonsSource scriptCommons = null;
  private AntraxPluginsStore pluginsStore;

  public void setScriptCommonsSorce(final ScriptCommonsSource scriptCommons) {
    assert scriptCommons != null;
    this.scriptCommons = scriptCommons;
  }

  public void setAntraxPluginsStore(final AntraxPluginsStore pluginsStore) {
    assert pluginsStore != null;
    this.pluginsStore = pluginsStore;
  }

  @Override
  public void buildColumns(final UpdateTableColumnModel columns) {
    columns.addColumn("", ScriptParamWrapper.class).setRenderer(new ArrayOperationRenderer()).setMaxWidth(20).setMinWidth(20);
    columns.addColumn("Param", ScriptParameter.class).setRenderer(new ScriptParameterRenderer());
    columns.addColumn("Ref", ScriptCommons.class);
    columns.addColumn("Value", Object.class);
  }

  @Override
  public void buildRow(final ScriptParamWrapper src, final ColumnWriter<ScriptParamWrapper> dest) {
    dest.writeColumn(src);
    dest.writeColumn(src.getParam());
    if (src.getParam().getValue(pluginsStore.getClassLoader()) instanceof SharedReference) {
      buildWithRef(src, dest);
    } else {
      buildWithValue(src, dest);
    }
  }

  private void buildWithRef(final ScriptParamWrapper src, final ColumnWriter<ScriptParamWrapper> dest) {
    SharedReference sharedRef = (SharedReference) src.getParam().getValue(pluginsStore.getClassLoader());
    assert pluginsStore != null;
    assert sharedRef != null;
    assert scriptCommons != null;
    ScriptCommons commons = scriptCommons.getCommonsByName(sharedRef.getName());
    dest.writeColumn(commons);
    dest.writeColumn(commons.getValue(pluginsStore.getClassLoader()));
  }

  private void buildWithValue(final ScriptParamWrapper src, final ColumnWriter<ScriptParamWrapper> dest) {
    dest.writeColumn(null);
    dest.writeColumn(src.getParam().getValue(pluginsStore.getClassLoader()));
  }

  @Override
  public Integer getUniqueKey(final ScriptParamWrapper src) {
    return src.getIndex();
  }

}
