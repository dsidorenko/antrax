/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

public class GsmViewTableTooltips {

  private GsmViewTableTooltips() {
  }

  public static String getServingTooltip() {
    return new TooltipHeaderBuilder("Enable registration on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getTrustedTooltip() {
    return new TooltipHeaderBuilder("Enable LAC-CELL-BASIC-ARFCN as trusted station").addText("Will blocked by imsi-catcher rules if disabled").build();
  }

  public static String getLacTooltip() {
    return new TooltipHeaderBuilder("Local Area Code").build();
  }

  public static String getCellTooltip() {
    return new TooltipHeaderBuilder("Cell ID").build();
  }

  public static String getBsicTooltip() {
    return new TooltipHeaderBuilder("Base station identification code").build();
  }

  public static String getArfcnTooltip() {
    return new TooltipHeaderBuilder("Absolute radio frequency channel number").build();
  }

  public static String getNetworkSurveyLastRxLevelTooltip() {
    return new TooltipHeaderBuilder("Last signal level by network survey").build();
  }

  public static String getCellMonitoringLastRxLevelTooltip() {
    return new TooltipHeaderBuilder("Last signal level by cell monitoring").build();
  }

  public static String getOperatorTooltip() {
    return new TooltipHeaderBuilder("Name of operator").build();
  }

  public static String getNetworkSurveyNumberOccurrencesTooltip() {
    return new TooltipHeaderBuilder("Number of LAC-CELL-BASIC-ARFCN appearances in GSM network by network survey").build();
  }

  public static String getServingCellMonitoringNumberOccurrencesTooltip() {
    return new TooltipHeaderBuilder("Number of LAC-CELL-BASIC-ARFCN appearances as serving cell in GSM network by cell monitoring").build();
  }

  public static String getCellMonitoringNumberOccurrencesTooltip() {
    return new TooltipHeaderBuilder("Number of LAC-CELL-BASIC-ARFCN appearances in GSM network by cell monitoring").build();
  }

  public static String getSuccessfulCallsTooltip() {
    return new TooltipHeaderBuilder("Successful calls count on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getTotalCallsTooltip() {
    return new TooltipHeaderBuilder("Total calls count on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getAveragePddTooltip() {
    return new TooltipHeaderBuilder("Post dial delay on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getCallsDurationTooltip() {
    return new TooltipHeaderBuilder("Calls duration on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getAsrTooltip() {
    return new TooltipHeaderBuilder("Answer seizure ratio on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getAcdTooltip() {
    return new TooltipHeaderBuilder("Average call duration on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getOutgoingSmsCountTooltip() {
    return new TooltipHeaderBuilder("Outgoing SMS count on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getIncomingSmsCountTooltip() {
    return new TooltipHeaderBuilder("Incoming SMS count on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getUssdCountTooltip() {
    return new TooltipHeaderBuilder("USSD count on LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getFirstAppearanceTooltip() {
    return new TooltipHeaderBuilder("Time of first appearance of LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getLastAppearanceTooltip() {
    return new TooltipHeaderBuilder("Time of last appearance of LAC-CELL-BASIC-ARFCN").build();
  }

  public static String getServerTooltip() {
    return new TooltipHeaderBuilder("Name of voice server").build();
  }

  public static String getNotesTooltip() {
    return new TooltipHeaderBuilder("User notes for LAC-CELL-BASIC-ARFCN").build();
  }

}
