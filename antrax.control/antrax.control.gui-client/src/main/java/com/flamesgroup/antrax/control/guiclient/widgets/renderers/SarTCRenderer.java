/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.antrax.storage.commons.impl.AlarisSms;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public final class SarTCRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = -2670176420901361694L;

  public SarTCRenderer() {
    setHorizontalAlignment(SwingConstants.CENTER);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    String labelText;
    if (value instanceof AlarisSms.Sar) {
      AlarisSms.Sar sar = (AlarisSms.Sar) value;
      labelText = String.format("%s/%d/%d", sar.getId(), sar.getParts(), sar.getPartNumber());
    } else {
      labelText = (value == null) ? "" : value.toString();
    }
    setText(labelText);
    return component;
  }

}
