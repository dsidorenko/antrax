/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;

public class BlackListStatisticTableTooltips {

  private BlackListStatisticTableTooltips() {
  }

  public static String getNumberTypeTooltip() {
    return new TooltipHeaderBuilder("Type of number to which the filter configuration is set").build();
  }

  public static String getNumberTooltip() {
    return new TooltipHeaderBuilder("Black number").build();
  }

  public static String getStatusTooltip() {
    TooltipHeaderBuilder builder = new TooltipHeaderBuilder("The reason for which the number is in the table");
    TooltipHeaderBuilder.TextTooltipHeader textTable = builder.createTextTable();
    textTable.addValue(VoipAntiSpamListNumbersStatus.MANUAL.name(), "Added manually by user")
        .addValue(VoipAntiSpamListNumbersStatus.ROUTING.name(), "Added by routing filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.ACD.name(), "Added by acd filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.ASR.name(), "Added by asr filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.GSM_ALERTING.name(), "Added by gsm alerting filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.VOIP_ALERTING.name(), "Added by voip alerting filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.FAS.name(), "Added by fas filter configs")
        .addValue(VoipAntiSpamListNumbersStatus.IVR.name(), "Added by ivr detect")
        .addValue(VoipAntiSpamListNumbersStatus.CDR.name(), "Added by cdr filter configs");
    return builder.build();
  }

  public static String getAddTimeTooltip() {
    return new TooltipHeaderBuilder("Time of adding number to table").build();
  }

  public static String getRoutingRequestCountTooltip() {
    return new TooltipHeaderBuilder("Count of requests on routing for number").build();
  }

}
