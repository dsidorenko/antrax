/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class SelectPathDialog extends JDialog {
  private static final long serialVersionUID = 8726151574438620921L;

  private final JComboBox paths = new JComboBox();

  private final JButton btnOk = new JButton("OK");

  private final JButton btnCancel = new JButton("Cancel");

  private boolean applied = false;

  public SelectPathDialog(final Window parent) {
    super(parent);
    setContentPane(createPane());
    paths.setEditable(true);
    paths.setPreferredSize(new Dimension(200, paths.getPreferredSize().height));
    setPreferredSize(new Dimension(320, 100));
    pack();
    setResizable(false);
    initializeListeners();
  }

  private void initializeListeners() {
    ActionListener actionListener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        applied = e.getSource() == btnOk;
        if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) {
          setVisible(false);
        } else if (getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
          dispose();
        } else if (getDefaultCloseOperation() == JDialog.DO_NOTHING_ON_CLOSE) {
        }

      }
    };
    btnOk.addActionListener(actionListener);
    btnCancel.addActionListener(actionListener);
  }

  private JRootPane createPane() {
    SpringLayout layout = new SpringLayout();
    JRootPane retval = new JRootPane();
    retval.setLayout(layout);
    JLabel label = new JLabel("Select New Path:");

    btnOk.setPreferredSize(btnCancel.getPreferredSize());
    label.setPreferredSize(new Dimension(label.getPreferredSize().width, paths.getPreferredSize().height));

    retval.add(label);
    retval.add(paths);
    retval.add(btnOk);
    retval.add(btnCancel);

    layout.putConstraint(SpringLayout.WEST, label, 5, SpringLayout.WEST, retval);
    layout.putConstraint(SpringLayout.NORTH, label, 5, SpringLayout.NORTH, retval);
    layout.putConstraint(SpringLayout.WEST, paths, 5, SpringLayout.EAST, label);
    layout.putConstraint(SpringLayout.NORTH, paths, 5, SpringLayout.NORTH, retval);

    layout.putConstraint(SpringLayout.EAST, btnOk, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, btnCancel, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, btnOk, -5, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, btnCancel, -5, SpringLayout.SOUTH, retval);

    ActionListener cancelListener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        btnCancel.doClick();
      }
    };
    ActionListener okListener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        btnOk.doClick();
      }
    };

    KeyStroke escStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    KeyStroke enterStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
    retval.registerKeyboardAction(cancelListener, escStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);
    retval.registerKeyboardAction(okListener, enterStroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    return retval;
  }

  public boolean showDialog(final String path, final String... paths) {
    this.paths.removeAllItems();
    DefaultComboBoxModel model = (DefaultComboBoxModel) this.paths.getModel();
    for (String p : paths) {
      model.addElement(p);
    }
    this.paths.setSelectedItem(path);
    setVisible(true);
    return applied;
  }

  public String getPath() {
    return (String) paths.getSelectedItem();
  }

  public static void main(final String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        SelectPathDialog dialog = new SelectPathDialog(null);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.showDialog("hello", "buy");
      }
    });
  }

}
