/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.IControlAudioManager;
import com.flamesgroup.antrax.control.authorization.ClientUID;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;

import java.util.List;
import java.util.Map;

public class ControlAudioManagerAccess {

  private final IControlAudioManager controlAudioManager;

  public boolean checkPermission(final ClientUID client, final String action) throws NotPermittedException {
    return controlAudioManager.checkPermission(client, action);
  }

  public ControlAudioManagerAccess(final IControlAudioManager controlAudioManager) {
    this.controlAudioManager = controlAudioManager;
  }

  public List<AudioFile> getAudioFiles(final IServerData selectedServer) throws NotPermittedException {
    return controlAudioManager.getAudioFiles(MainApp.clientUID, selectedServer);
  }

  public List<String> getIvrTemplatesSimGroups(final IServerData selectedServer) throws NotPermittedException {
    return controlAudioManager.getIvrTemplatesSimGroups(MainApp.clientUID, selectedServer);
  }

  public List<IvrTemplateWrapper> getIvrTemplates(final IServerData selectedServer, final String simGroup) throws NotPermittedException {
    return controlAudioManager.getIvrTemplates(MainApp.clientUID, selectedServer, simGroup);
  }

  public byte[] downloadAudioFileFromServer(final IServerData selectedServer, final String fileName) throws NotPermittedException {
    return controlAudioManager.downloadAudioFileFromServer(MainApp.clientUID, selectedServer, fileName);
  }

  public void createIvrTemplateSimGroup(final IServerData selectedServer, final String simGroup) throws NotPermittedException, IvrTemplateException {
    controlAudioManager.createIvrTemplateSimGroup(MainApp.clientUID, selectedServer, simGroup);
  }

  public void removeIvrTemplateSimGroups(final IServerData selectedServer, final List<String> simGroups) throws NotPermittedException, IvrTemplateException {
    controlAudioManager.removeIvrTemplateSimGroups(MainApp.clientUID, selectedServer, simGroups);
  }

  public Map<String, Integer> getCallDropReasons(final IServerData selectedServer) throws NotPermittedException {
    return controlAudioManager.getCallDropReasons(MainApp.clientUID, selectedServer);
  }

  public void uploadIvrTemplate(final IServerData selectedServer, final String selectedSimGroup, final byte[] bytes, final String fileName) throws IvrTemplateException, NotPermittedException {
    controlAudioManager.uploadIvrTemplate(MainApp.clientUID, selectedServer, selectedSimGroup, bytes, fileName);
  }

  public void removeIvrTemplate(final IServerData selectedServer, final String selectedSimGroup, final List<IvrTemplateWrapper> selectedIvrTemplates)
      throws NotPermittedException, IvrTemplateException {
    controlAudioManager.removeIvrTemplate(MainApp.clientUID, selectedServer, selectedSimGroup, selectedIvrTemplates);
  }

}
