/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets.tooltip;

import com.flamesgroup.commons.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class TooltipHeaderBuilder {

  private final String caption;

  private final List<Consumer<StringBuilder>> consumers = new ArrayList<>();

  public TooltipHeaderBuilder(final String caption) {
    this.caption = caption;
  }

  public TooltipHeaderBuilder addText(final String text) {
    consumers.add(stringBuilder -> stringBuilder.append("<p>").append(text).append("</p>"));
    return this;
  }

  public ImageTable createImageTable() {
    ImageTable imageTable = new ImageTable();
    consumers.add(imageTable::accept);
    return imageTable;
  }

  public TextTooltipHeader createTextTable() {
    TextTooltipHeader textTooltipHeader = new TextTooltipHeader();
    consumers.add(textTooltipHeader::accept);
    return textTooltipHeader;
  }

  public String build() {
    StringBuilder builder = new StringBuilder("<html>");
    builder.append("<p align=\"center\">").append("<b>").append(caption).append("</b>").append("</p>");
    consumers.forEach(c -> c.accept(builder));
    return builder.toString();
  }

  public class ImageTable {

    private final List<Pair<String, String>> imageValues = new ArrayList<>();

    private ImageTable() {
    }

    public ImageTable addValue(final String path, final String description) {
      imageValues.add(new Pair<>(path, description));
      return this;
    }

    private void accept(final StringBuilder stringBuilder) {
      stringBuilder.append("<table style=\"width:100%\">");
      imageValues.forEach((pair) ->
          stringBuilder.append("<tr>")
              .append("<td>").append("<img src=\"").append(pair.first()).append("\" style=\"height:16px;width:16px;\"/>").append("</td>")
              .append("<td>").append(pair.second()).append("</td>")
              .append("</tr>")
      );
      stringBuilder.append("</table>");
    }
  }

  public class TextTooltipHeader {

    private final List<Pair<String, String>> textValues = new ArrayList<>();

    private TextTooltipHeader() {
    }

    public TextTooltipHeader addValue(final String text, final String description) {
      textValues.add(new Pair<>(text, description));
      return this;
    }

    private void accept(final StringBuilder stringBuilder) {
      stringBuilder.append("<table style=\"width:100%\">");
      textValues.forEach((pair) ->
          stringBuilder.append("<tr>")
              .append("<td>").append(pair.first()).append("</td>")
              .append("<td>").append(pair.second()).append("</td>")
              .append("</tr>")
      );
      stringBuilder.append("</table>");
    }

  }

}
