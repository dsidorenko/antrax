/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.commadapter;

import com.flamesgroup.antrax.commons.TimePeriodWriter;
import com.flamesgroup.antrax.storage.state.CallState;

import java.util.Comparator;

public class CallStateTimeComparator implements Comparator<CallState> {

  public static long calculateStateTime(final CallState state) {

    return (Long) state.writeTime(new TimePeriodWriter() {

      @Override
      public Object writeLeftTime(final long time1) {
        return -time1;
      }

      @Override
      public Object writePassedTime(final long time1) {
        return time1;
      }

    });
  }

  @Override
  public int compare(final CallState state1, final CallState state2) {
    if (state1 == null) {
      return (state2 == null) ? 0 : 1;
    } else if (state2 == null) {
      return -1;
    }
    long dif = (calculateStateTime(state1) - calculateStateTime(state2));
    return (dif > 0) ? +1 : ((dif < 0) ? -1 : 0);
  }

}
