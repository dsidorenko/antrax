/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanelProvider;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.storage.commons.impl.CallAllocationAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.RouteConfig;
import com.flamesgroup.antrax.storage.commons.impl.SmsAllocationAlgorithm;

import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;

public class ServersPanelProvider implements AppPanelProvider {

  @Override
  public boolean checkPermissions(final BeansPool pool) throws NotPermittedException {
    return check(pool, "createServer") && check(pool, "getServersRevision") && check(pool, "listServers") && check(pool, "deleteServer") && check(pool, "updateServer") && check(pool,
        "getServersRevision");
  }

  private boolean check(final BeansPool pool, final String action) throws NotPermittedException {
    return pool.getConfigBean().checkPermission(MainApp.clientUID, action);
  }

  @Override
  public AppPanel createPanel() {
    JPanel panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    CfgServersPanel cfgServersPanel = new CfgServersPanel();
    cfgServersPanel.setBorder(BorderFactory.createTitledBorder("Servers Config"));

    RouteConfigPanel routeConfigPanel = new RouteConfigPanel();

    panel.add(routeConfigPanel);
    panel.add(cfgServersPanel);

    return new AppPanel() {
      @Override
      public JComponent getComponent() {
        return panel;
      }

      @Override
      public void setEditable(final boolean editable) {
        routeConfigPanel.setEditable(editable);
        cfgServersPanel.setEditable(editable);
      }

      @Override
      public void setActive(final boolean active) {
        routeConfigPanel.setActive(active);
        cfgServersPanel.setActive(active);
      }

      @Override
      public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
        routeConfigPanel.postInitialize(refresher, transactionManager);
        cfgServersPanel.postInitialize(refresher, transactionManager);
      }

      @Override
      public void release() {
        routeConfigPanel.release();
        cfgServersPanel.release();
      }
    };
  }

  private class CallRouteConfigPanel extends JPanel {

    private static final long serialVersionUID = 5726842045245949740L;

    final JRadioButton randomButton = new JRadioButton("Random");
    final JRadioButton uniformlyActiveButton = new JRadioButton("Uniformly Active Calls");
    final JRadioButton uniformlyLoadButton = new JRadioButton("Uniformly Load Channels");

    public CallRouteConfigPanel() {
      setLayout(new GridBagLayout());

      randomButton.setPreferredSize(new Dimension(100, randomButton.getPreferredSize().height));
      uniformlyActiveButton.setPreferredSize(new Dimension(180, uniformlyActiveButton.getPreferredSize().height));
      uniformlyLoadButton.setPreferredSize(new Dimension(180, uniformlyLoadButton.getPreferredSize().height));

      ButtonGroup group = new ButtonGroup();
      group.add(randomButton);
      group.add(uniformlyActiveButton);
      group.add(uniformlyLoadButton);
      randomButton.setSelected(true);

      add(randomButton, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      add(uniformlyActiveButton, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      add(uniformlyLoadButton, new GridBagConstraints(2, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public void setEnabled(final boolean enabled) {
      super.setEnabled(enabled);
      randomButton.setEnabled(enabled);
      uniformlyActiveButton.setEnabled(enabled);
      uniformlyLoadButton.setEnabled(enabled);
    }

    public CallAllocationAlgorithm getCallAllocationAlgorithm() {
      if (randomButton.isSelected()) {
        return CallAllocationAlgorithm.RANDOM;
      } else if (uniformlyActiveButton.isSelected()) {
        return CallAllocationAlgorithm.UNIFORMLY_ACTIVE_CALLS;
      } else if (uniformlyLoadButton.isSelected()) {
        return CallAllocationAlgorithm.UNIFORMLY_LOAD_CHANNELS;
      } else {
        throw new IllegalArgumentException("Unspecified Call Allocation Algorithm");
      }
    }

    public void setCallAllocationAlgorithm(final CallAllocationAlgorithm callAllocationAlgorithm) {
      if (callAllocationAlgorithm == null) {
        randomButton.setSelected(true);
        return;
      }

      switch (callAllocationAlgorithm) {
        case RANDOM:
          randomButton.setSelected(true);
          break;
        case UNIFORMLY_ACTIVE_CALLS:
          uniformlyActiveButton.setSelected(true);
          break;
        case UNIFORMLY_LOAD_CHANNELS:
          uniformlyLoadButton.setSelected(true);
          break;
        default:
          throw new IllegalArgumentException("Unspecified Call Allocation Algorithm");
      }
    }

  }

  private class SmsRouteConfigPanel extends JPanel {

    private static final long serialVersionUID = 5726842045245949740L;

    final JRadioButton randomButton = new JRadioButton("Random");
    final JRadioButton uniformlyTotalButton = new JRadioButton("Uniformly Total SMS");
    final JRadioButton uniformlySuccessButton = new JRadioButton("Uniformly Success SMS");

    public SmsRouteConfigPanel() {
      setLayout(new GridBagLayout());

      randomButton.setPreferredSize(new Dimension(100, randomButton.getPreferredSize().height));
      uniformlyTotalButton.setPreferredSize(new Dimension(180, uniformlyTotalButton.getPreferredSize().height));
      uniformlySuccessButton.setPreferredSize(new Dimension(180, uniformlySuccessButton.getPreferredSize().height));

      ButtonGroup group = new ButtonGroup();
      group.add(randomButton);
      group.add(uniformlyTotalButton);
      group.add(uniformlySuccessButton);
      randomButton.setSelected(true);

      add(randomButton, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      add(uniformlyTotalButton, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      add(uniformlySuccessButton, new GridBagConstraints(2, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }

    @Override
    public void setEnabled(final boolean enabled) {
      super.setEnabled(enabled);
      randomButton.setEnabled(enabled);
      uniformlyTotalButton.setEnabled(enabled);
      uniformlySuccessButton.setEnabled(enabled);
    }

    public SmsAllocationAlgorithm getSmsAllocationAlgorithm() {
      if (randomButton.isSelected()) {
        return SmsAllocationAlgorithm.RANDOM;
      } else if (uniformlyTotalButton.isSelected()) {
        return SmsAllocationAlgorithm.UNIFORMLY_TOTAL;
      } else if (uniformlySuccessButton.isSelected()) {
        return SmsAllocationAlgorithm.UNIFORMLY_SUCCESS;
      } else {
        throw new IllegalArgumentException("Unspecified SMS Allocation Algorithm");
      }
    }

    public void setSmsAllocationAlgorithm(final SmsAllocationAlgorithm smsAllocationAlgorithm) {
      if (smsAllocationAlgorithm == null) {
        randomButton.setSelected(true);
        return;
      }

      switch (smsAllocationAlgorithm) {
        case RANDOM:
          randomButton.setSelected(true);
          break;
        case UNIFORMLY_TOTAL:
          uniformlyTotalButton.setSelected(true);
          break;
        case UNIFORMLY_SUCCESS:
          uniformlySuccessButton.setSelected(true);
          break;
        default:
          throw new IllegalArgumentException("Unspecified SMS Allocation Algorithm");
      }
    }

  }

  private class RouteConfigPanel extends JPanel implements AppPanel {

    private static final long serialVersionUID = -4751582337810843435L;

    private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
    private final AtomicReference<TransactionManager> transactionManager = new AtomicReference<>();
    private final AtomicReference<Refresher> refresher = new AtomicReference<>();

    final CallRouteConfigPanel callAllocationAlgorithmPanel = new CallRouteConfigPanel();
    final SmsRouteConfigPanel smsAllocationAlgorithmPanel = new SmsRouteConfigPanel();

    final JLabel callAllocationAlgorithmLabel = new JEditLabel("Call Allocation Algorithm:");
    final JLabel smsAllocationAlgorithmLabel = new JEditLabel("SMS Allocation Algorithm:");

    RouteConfig routeConfig;

    public RouteConfigPanel() {
      setBorder(BorderFactory.createTitledBorder("Route Config"));
      setLayout(new BorderLayout());
      setMaximumSize(new Dimension(getMaximumSize().width, 50));

      JPanel algorithmPanel = new JPanel();
      algorithmPanel.setLayout(new GridBagLayout());

      algorithmPanel.add(callAllocationAlgorithmLabel, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 20), 0, 0));
      algorithmPanel.add(callAllocationAlgorithmPanel, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

      algorithmPanel.add(smsAllocationAlgorithmLabel, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 20), 0, 0));
      algorithmPanel.add(smsAllocationAlgorithmPanel, new GridBagConstraints(1, 1, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

      add(algorithmPanel, BorderLayout.WEST);
    }

    @Override
    public void setEnabled(final boolean enabled) {
      callAllocationAlgorithmLabel.setEnabled(enabled);
      smsAllocationAlgorithmLabel.setEnabled(enabled);
      callAllocationAlgorithmPanel.setEnabled(enabled);
      smsAllocationAlgorithmPanel.setEnabled(enabled);
    }

    public RouteConfig getRouteConfig() {
      RouteConfig routeConfigLocal = new RouteConfig(callAllocationAlgorithmPanel.getCallAllocationAlgorithm(), smsAllocationAlgorithmPanel.getSmsAllocationAlgorithm());
      if (routeConfig != null) {
        routeConfigLocal.setID(routeConfig.getID());
      }
      return routeConfigLocal;
    }

    public void setRouteConfig(final RouteConfig routeConfigLocal) {
      routeConfig = routeConfigLocal;
      if (routeConfig == null) {
        callAllocationAlgorithmPanel.setCallAllocationAlgorithm(null);
        smsAllocationAlgorithmPanel.setSmsAllocationAlgorithm(null);
      } else {
        callAllocationAlgorithmPanel.setCallAllocationAlgorithm(routeConfig.getCallAllocationAlgorithm());
        smsAllocationAlgorithmPanel.setSmsAllocationAlgorithm(routeConfig.getSmsAllocationAlgorithm());
      }
    }

    @Override
    public JComponent getComponent() {
      return this;
    }

    @Override
    public void setEditable(final boolean editable) {
      setEnabled(editable);
    }

    @Override
    public void setActive(final boolean active) {
      if (active) {
        refresherThread.get().addRefresher(refresher.get());
      } else {
        refresherThread.get().removeRefresher(refresher.get());
      }
    }

    @Override
    public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
      this.refresherThread.set(refresher);
      this.refresher.set(createRefresher());

      this.transactionManager.set(transactionManager);
      this.transactionManager.get().addListener(new TransactionListener() {

        @Override
        public void onTransactionStateChanged(final TransactionState newState) {
          refresherThread.get().forceRefresh();
        }

        @Override
        public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
          try {
            beansPool.getConfigBean().updateRouteConfig(MainApp.clientUID, transactionId, getRouteConfig());
          } catch (NotPermittedException | TransactionException | StorageException e) {
            return Collections.singletonList(e);
          }
          return Collections.emptyList();
        }

      });
    }

    @Override
    public void release() {
      if (refresher.get() != null) {
        this.refresher.get().interrupt();
      }
    }

    private Refresher createRefresher() {
      return new Refresher("RouteConfigPanelRefresher") {

        @Override
        protected void refreshInformation(final BeansPool beansPool) throws Exception {
          if (transactionManager.get().isEditTransactionStarted() && routeConfig != null) {
            return;
          }

          setRouteConfig(beansPool.getConfigBean().getRouteConfig(MainApp.clientUID));
        }

        @Override
        public void updateGuiElements() {
        }

        @Override
        public void handleEnabled(final BeansPool beansPool) {
        }

        @Override
        public void handleDisabled(final BeansPool beansPool) {
        }

      };
    }
  }

}
