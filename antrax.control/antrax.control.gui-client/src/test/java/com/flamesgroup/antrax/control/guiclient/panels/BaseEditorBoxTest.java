/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import static org.junit.Assert.assertFalse;

import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;

@RunWith(JMockit.class)
public class BaseEditorBoxTest {
  private JFrame frame;

  public void createUI(final JComponent... comps) {
    JPanel container = new JPanel();
    frame = new JFrame();
    frame.setContentPane(container);
    frame.setVisible(true);

    for (JComponent c : comps) {
      container.add(c);
    }
  }

  public void disposeUI() {
    frame.setVisible(false);
    frame.dispose();
  }

  private static class StringContainer {
    private String str;

    public StringContainer(final String str) {
      this.str = str;
    }

    public String get() {
      return str;
    }

    public void set(final String str) {
      this.str = str;
    }

    @Override
    public String toString() {
      return str;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj instanceof StringContainer) {
        StringContainer that = (StringContainer) obj;
        return this.str.equals(that.str);
      }
      return false;
    }

    @Override
    public int hashCode() {
      return str.hashCode() * 5;
    }
  }

  private static class BoxWithOneField extends BaseEditorBox<StringContainer> {
    private static final long serialVersionUID = 1L;

    private JTextField field;

    @Override
    protected void createGUIElements() {
      add(field = new JTextField());
    }

    @Override
    protected void readEditorsToElem(final StringContainer dest) {
      dest.set(field.getText());
    }

    @Override
    protected void writeElemToEditors(final StringContainer src) {
      field.setText(src.get());
    }

    public JTextField getField() {
      return field;
    }

  }

  @Test
  public void testTextChangesWithoutFocusLostIsIgnored() {
    final AtomicBoolean called = new AtomicBoolean(false);
    JTextField alienField = new JTextField();
    BoxWithOneField box = new BoxWithOneField();
    box.setData(createData(new StringContainer("Test")));

    final ElementModifiedEventHandler<StringContainer> handler = new ElementModifiedEventHandler<StringContainer>() {
      @Override
      public void handleElementModified(final StringContainer elem) {
        called.set(true);
      }
    };
    box.addElementModifiedHandler(handler);

    createUI(box, alienField);

    try {
      box.getField().requestFocus();
      box.getField().setText("AnotherText");
      box.getField().requestFocus();
      assertFalse(called.get());
    } finally {
      disposeUI();
    }
  }

  private List<StringContainer> createData(final StringContainer stringContainer) {
    ArrayList<StringContainer> retval = new ArrayList<>();
    retval.add(stringContainer);
    return retval;
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testFocusLooseWithoutTextChangesIsIgnored(@Mocked final ElementModifiedEventHandler handler) {
    JTextField alienField = new JTextField();
    BoxWithOneField box = new BoxWithOneField();
    box.setData(createData(new StringContainer("Test")));

    box.addElementModifiedHandler(handler);

    createUI(box, alienField);

    try {
      box.getField().requestFocus();
      box.getField().setText("Test");
      alienField.requestFocus();
    } finally {
      disposeUI();
    }
  }

}
