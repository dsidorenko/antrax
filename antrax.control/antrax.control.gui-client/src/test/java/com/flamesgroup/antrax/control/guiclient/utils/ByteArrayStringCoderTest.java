/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Arrays;

public class ByteArrayStringCoderTest {

  @Test
  public void encodePositiveTest() {
    byte data[] = new byte[] {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16
    };
    String str = ByteArrayStringCoder.encode(data);
    assertEquals("0x00 0x01 0x02 0x03 0x04 0x05 0x06 0x07 0x08 0x09 0x0a 0x0b 0x0c 0x0d 0x0e 0x0f 0x10", str);
  }

  @Test
  public void encodeNegativeTest() {
    byte data[] = new byte[] {
        -16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0
    };
    String str = ByteArrayStringCoder.encode(data);
    assertEquals("-0x10 -0x0f -0x0e -0x0d -0x0c -0x0b -0x0a -0x09 -0x08 -0x07 -0x06 -0x05 -0x04 -0x03 -0x02 -0x01 0x00", str);
  }

  @Test
  public void encodeDecodeTest() {
    byte[] data = new byte[Byte.MAX_VALUE - Byte.MIN_VALUE + 1];

    int i = 0;
    for (Integer b = (int) Byte.MIN_VALUE; b <= Byte.MAX_VALUE; b++, i++) {
      data[i] = b.byteValue();
    }

    byte[] decodedData = ByteArrayStringCoder.decode(ByteArrayStringCoder.encode(data));

    assertTrue(Arrays.equals(data, decodedData));
  }

}
