/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;

import java.util.List;
import java.util.Map;

public interface IVoiceServerAudioManager {

  List<AudioFile> getAudioFiles();

  byte[] downloadAudioFileFromServer(String fileName);

  List<String> getIvrTemplatesSimGroups();

  List<IvrTemplateWrapper> getIvrTemplates(String simGroup);

  void createIvrTemplateSimGroup(String simGroup) throws IvrTemplateException;

  void removeIvrTemplateSimGroups(List<String> simGroups) throws IvrTemplateException;

  Map<String, Integer> getCallDropReasons();

  void uploadIvrTemplate(String simGroup, byte[] bytes, String fileName) throws IvrTemplateException;

  void removeIvrTemplate(String simGroup, List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException;

}
