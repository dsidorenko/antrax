# antrax

Solution to termination of voice traffic from VoIP to GSM network.

## Overview

**antrax** consists of the next main components:
* _control server_ - central component of the system, could be only one
* _sim server_ - control SIMB, SIMBOX60 and SIMBOX120 devices, could be one or more
* _voice server_ - control GSMB, GSMB2, GSMB3, GSMBOX4 and GSMBOX8 devices and process VoIP, could be one or more
* _gui client_ - application which allows configure and monitor the system, could run as many instances as necessary

**antrax** components communicate with each other by [RMI](https://en.wikipedia.org/wiki/Java_remote_method_invocation).

### control-server

As already mentioned _control server_ is a central component of the system.
Main goal of _control server_ is to connect SIM card with GSM module based on given logic.
Also it stores all configuration and statistics in database, communicates with _gui client_ and performs routing of VoIP traffic on _soft switch_.

### sim-server

Main function of _sim-server_ is to find new SIM card, read necessary information and then process requests on it.

### voice-server

Main function of _voice-server_ is to find unused GSM module and then connect it with provided SIM card. Unions of GSM module and SIM card called `call channel` and they are using to process 
termination and origination calls.

### gui-client

_gui client_ installed from _control server_ by using [Java Web Start](https://en.wikipedia.org/wiki/Java_Web_Start) technology. It communicates with _control server_ from which it was installed and
 allows to configure and monitor current system.

### scripts

**antrax** declares special _plugins api_ to separate system from user business logic. _plugins api_ provide next main API groups: 
* _IMEI generator_ - rules for generating IMEI of GSM modules
* _call filter_ - rules for filtering call numbers
* _sms filter_ - rules for filtering sms numbers
* _business activity_ - rules for behavior of SIM card on one voice server
* _action provider_ - rules for behavior of SIM card on different voice servers
* _sim server factor_ - rules for taking order of SIM card from control server
* _voice server factor_ - rules for taking order of call channels for terminating 
* _gateway selector_ - rules for choosing order of voice servers for taking SIM card
* _incoming call management_ - rules for processing incoming calls
* _activity period_ - rules for activity limitations of SIM card on one voice server
* _session period_ - rules for activity limitations of SIM card on different voice servers

Module _scripts_ is a reference implementation of _plugins api_ with all available for now generators, filters, factors and other logics.

### soft switch

Currently as _soft switch_ in **antrax** system uses [Yate](http://www.yate.ro/opensource.php).
On Yate side interfacing of different VoIP protocols is accomplished and transcoding of audio codecs (if necessary).
Coordination of different VoIP protocols as well as transcoding of audio codecs (if necessary) is fulfilled on Yate side.
In its turn _control server_ uses [Yate external module protocol](http://docs.yate.ro/wiki/External_module_command_flow) to communicate with Yate.

## Structure

**antrax** uses multiple modules [Maven](https://maven.apache.org) project layout.

In addition there is a storage module with DAO layer and migration scripts. 

## Build

**antrax** could be compiled, installed and deployed with regular [Maven commands](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html).

**antrax** requires [PostgreSQL](https://www.postgresql.org/) version 9.5 or higher to be installed. 

**antrax** uses [Flyway](https://flywaydb.org) to implement database migration mechanism and [jOOQ](https://www.jooq.org) to generate DAO layer from database.
That is why if some database modification was made by migration next commands must be executed:
```
$ mvn compile
$ mvn compile -P sync-db
```
Before execution of previous command, development database must be created and configured in [Maven setting file](https://maven.apache.org/settings.html) (usually `$USER_HOME/.m2/settings.xml`):
```
<properties>
  <antrax.db.config.url>jdbc:postgresql://127.0.0.1:5432/antrax</antrax.db.config.url>
  <antrax.db.config.username>antrax</antrax.db.config.username>
  <antrax.db.config.password>antrax</antrax.db.config.password>
</properties>

```

To build **antrax** `rpm` and `deb` packages use [antrax-osp](https://gitlab.com/flamesgroup/antrax-osp.git).

How to install and configure **antrax** project see `ANTRAX instalation manual`.

## Questions

If you have any questions about **antrax**, feel free to create issue with it.
