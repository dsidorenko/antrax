/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.unit.ICCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExternalCommandsProcessor extends Thread {
  private final Logger logger = LoggerFactory.getLogger(ExternalCommandsProcessor.class);

  private final VoiceServer antrax;
  private final VSStatus antraxStatus;

  public ExternalCommandsProcessor(final VoiceServer antrax, final VSStatus antraxStatus) {
    super("commandProcessor");
    setDaemon(false);
    this.antrax = antrax;
    this.antraxStatus = antraxStatus;
  }

  @Override
  public void run() {
    while (!antraxStatus.terminating) {
      try {
        if (System.in.available() > 0) {
          processCommand(readLine());
        } else {
          synchronized (antraxStatus) {
            antraxStatus.wait(200);
          }
        }
      } catch (IOException e) {
        logger.warn("[{}] - while reading input", this, e);
        antrax.terminate();
      } catch (InterruptedException e) {
        logger.warn("[{}] - interrupted", this, e);
      }
    }
  }

  private String readLine() throws IOException {
    StringBuilder buf = new StringBuilder();
    while (true) {
      int c = System.in.read();
      if (c == '\n') {
        break;
      }
      buf.append((char) c);
    }
    return buf.toString();
  }

  private void processCommand(final String cmd) {
    logger.debug("> {}", cmd);
    if ("stop".equals(cmd)) {
      antrax.terminate();
    } else if (cmd.startsWith("disable")) {
      processDisable(cmd);
    } else if (cmd.startsWith("enable")) {
      processEnable(cmd);
    } else if (cmd.startsWith("sendUSSD")) {
      processUSSD(cmd);
    } else if (cmd.startsWith("lockGsmChannel")) {
      processLock(cmd);
    } else if (cmd.startsWith("unlock")) {
      processUnlock(cmd);
    }
  }

  private void processUnlock(final String cmd) {
    logger.debug("[{}] - unlock: {}", this, cmd);
    String[] cmds = cmd.split("\\s+");
    antrax.unlock(new ICCID(cmds[1]));
  }

  private void processLock(final String cmd) {
    logger.debug("[{}] - lockGsmChannel: {}", this, cmd);
    String[] cmds = cmd.split("\\s+");
    antrax.lock(new ICCID(cmds[1]));
  }

  void processUSSD(final String cmd) {
    logger.debug("[{}] - process USSD: {}", this, cmd);
    Matcher matcher = Pattern.compile("send-USSD ([^ ]+) (.+)").matcher(cmd);
    if (matcher.find()) {
      final String simUID = matcher.group(1);
      final String ussd = matcher.group(2);
      new Thread() {
        @Override
        public void run() {
          antrax.sendUSSD(new ICCID(simUID), ussd);
        }
      }.start();
    } else {
      logger.warn("[{}] - {} is illegal command", this, cmd);
    }
  }

  void processEnable(final String cmd) {
    logger.debug("[{}] - process enable: {}", this, cmd);
    String[] cmds = cmd.split("\\s+");
    antrax.enable(new ICCID(cmds[1]));
  }

  void processDisable(final String cmd) {
    logger.debug("[{}] - process disable: {}", this, cmd);
    String[] cmds = cmd.split("\\s+");
    antrax.disable(new ICCID(cmds[1]));
  }

}
