/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager.rtstat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.control.voiceserver.ActivityLogger;
import com.flamesgroup.antrax.storage.commons.impl.GSMChannelImpl;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.DeviceType;
import com.flamesgroup.device.DeviceUID;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.junit.Assert;
import org.junit.Test;

public class ChannelsRuntimeTest {

  @Test
  public void testGsmChannels() {
    IActivityLogger channelsRuntime = new ActivityLogger();
    ChannelUID gsm11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID gsm12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getVoiceServerChannelsInfos().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm11));
    assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().size());
    Assert.assertEquals(gsm11, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm12));
    assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().size());
    Assert.assertEquals(gsm11, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(gsm12, channelsRuntime.getVoiceServerChannelsInfos().get(1).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
  }

  @Test
  public void testCallChannels() throws Exception {
    IActivityLogger channelsRuntime = new ActivityLogger();
    ChannelUID gsm11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID gsm12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    ChannelUID sim11 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1);
    ChannelUID sim12 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getVoiceServerChannelsInfos().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm11));
    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm12));
    channelsRuntime.logSIMTakenFromSimServer(sim11, new ICCID("11"));
    channelsRuntime.logSIMTakenFromSimServer(sim12, new ICCID("12"));

    channelsRuntime.logCallChannelCreated(gsm11, sim11);
    assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().size());
    Assert.assertEquals(gsm11, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(sim11, channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getSimChannelUID());
    Assert.assertEquals(new ICCID("11"), channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getSimUID());
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());

    channelsRuntime.logCallChannelChangedState(sim11, CallChannelState.State.READY_TO_CALL, "", 0);
    Assert.assertEquals(CallChannelState.State.READY_TO_CALL, channelsRuntime.getVoiceServerChannelsInfos().get(0).getChannelState().getState());

    channelsRuntime.logCallStarted(sim11, new PhoneNumber("111"));
    channelsRuntime.logCallChangeState(sim11, CallState.State.DIALING);
    channelsRuntime.logCallChangeState(sim11, CallState.State.ACTIVE);
    Assert.assertEquals(CallState.State.ACTIVE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());
    Thread.sleep(200);

    channelsRuntime.logCallEnded(sim11, 10);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());
    Assert.assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());
    assertTrue(channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getCallsDuration() > 0);

    channelsRuntime.logCallStarted(sim11, new PhoneNumber("111"));
    channelsRuntime.logCallEnded(sim11, 0);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());
    Assert.assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());

    channelsRuntime.logCallChannelReleased(sim11);
    assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().size());
    assertNull(channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo());
    Assert.assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());

    channelsRuntime.logCallChannelCreated(gsm11, sim12);
    Assert.assertEquals(gsm11, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getGSMChannel().getChannelUID());
    Assert.assertEquals(sim12, channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getSimChannelUID());
    Assert.assertEquals(new ICCID("12"), channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getSimUID());
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());

    channelsRuntime.logCallStarted(sim12, new PhoneNumber("111"));
    channelsRuntime.logCallEnded(sim12, 0);
    Assert.assertEquals(CallState.State.IDLE, channelsRuntime.getVoiceServerChannelsInfos().get(0).getCallState().getState());
    Assert.assertEquals(3, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getTotalCallsCount());
    Assert.assertEquals(1, channelsRuntime.getVoiceServerChannelsInfos().get(0).getMobileGatewayChannelInfo().getSuccessfulCallsCount());
  }

  @Test
  public void testUSSD() throws Exception {
    IActivityLogger channelsRuntime = new ActivityLogger();
    ChannelUID gsm11 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 1);
    ChannelUID gsm12 = new ChannelUID(new DeviceUID(DeviceType.GSMB, 1), (byte) 2);
    ChannelUID sim11 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 1);
    ChannelUID sim12 = new ChannelUID(new DeviceUID(DeviceType.SIMB, 1), (byte) 2);
    assertEquals(0, channelsRuntime.getVoiceServerChannelsInfos().size());

    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm11));
    channelsRuntime.logDeclaredGsmUnit(new GSMChannelImpl().setChannelUID(gsm12));
    channelsRuntime.logSIMTakenFromSimServer(sim11, new ICCID("11"));
    channelsRuntime.logSIMTakenFromSimServer(sim12, new ICCID("12"));

    channelsRuntime.logCallChannelCreated(gsm11, sim11);
    assertEquals(2, channelsRuntime.getVoiceServerChannelsInfos().size());
    assertNull(channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getLastUSSDResponse());
    Assert.assertEquals(0, channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getLastUSSDTimeout());
    channelsRuntime.logCallChannelSendUSSD(sim11, "NEW USSD REQUEST", "NEW USSD RESPONSE");
    Thread.sleep(200);
    Assert.assertEquals("NEW USSD RESPONSE", channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getLastUSSDResponse());
    assertTrue(channelsRuntime.getVoiceServerChannelsInfos().get(0).getSimChannelInfo().getLastUSSDTimeout() > 0);

    channelsRuntime.logCallChannelReleased(sim11);
    channelsRuntime.logCallChannelCreated(gsm12, sim11);
    Assert.assertEquals("NEW USSD RESPONSE", channelsRuntime.getVoiceServerChannelsInfos().get(1).getSimChannelInfo().getLastUSSDResponse());
    assertTrue(channelsRuntime.getVoiceServerChannelsInfos().get(1).getSimChannelInfo().getLastUSSDTimeout() > 0);
    channelsRuntime.logCallChannelSendUSSD(sim11, "NEW USSD REQUEST", "YET ANOTHER NEW USSD RESPONSE");
    Assert.assertEquals("YET ANOTHER NEW USSD RESPONSE", channelsRuntime.getVoiceServerChannelsInfos().get(1).getSimChannelInfo().getLastUSSDResponse());
  }

}
