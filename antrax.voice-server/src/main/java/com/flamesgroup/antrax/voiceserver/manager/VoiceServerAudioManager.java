/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.manager;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.voiceserver.IVoiceServerAudioManager;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplate;
import com.flamesgroup.antrax.voiceserver.ivr.correlation.IvrTemplateManager;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.commons.IvrTemplateException;
import com.flamesgroup.commons.IvrTemplateWrapper;
import com.flamesgroup.jiax2.CallDropReason;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class VoiceServerAudioManager implements IVoiceServerAudioManager {

  private static final Logger logger = LoggerFactory.getLogger(VoiceServerAudioManager.class);

  private final IvrTemplateManager ivrTemplateManager;

  public VoiceServerAudioManager(final IvrTemplateManager ivrTemplateManager) {
    this.ivrTemplateManager = ivrTemplateManager;
  }

  @Override
  public List<AudioFile> getAudioFiles() {
    Path voiceServerAudioCapturePath = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
    logger.info("[{}] - try load list of audio files from: [{}]", this, voiceServerAudioCapturePath);
    File[] files = new File(voiceServerAudioCapturePath.toUri()).listFiles();
    if (files == null) {
      logger.info("[{}] - not found files at path: {}", this, voiceServerAudioCapturePath);
      return Collections.emptyList();
    }
    return Arrays.stream(files).map(f -> new AudioFile(f.getName(), f.lastModified(), f.length())).collect(Collectors.toList());
  }

  @Override
  public byte[] downloadAudioFileFromServer(final String fileName) {
    Path voiceServerAudioCapturePath = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getVoiceServerAudioCapturePath();
    logger.info("[{}] - try read audio file: [{}] from: [{}]", this, fileName, voiceServerAudioCapturePath);

    try {
      return Files.readAllBytes(voiceServerAudioCapturePath.resolve(fileName));
    } catch (IOException e) {
      logger.error("[{}] - can't read file: [{}]", this, fileName, e);
    }
    return new byte[0];
  }

  @Override
  public List<String> getIvrTemplatesSimGroups() {
    return ivrTemplateManager.getSimGroupNames();
  }

  @Override
  public List<IvrTemplateWrapper> getIvrTemplates(final String simGroup) {
    List<IvrTemplate> ivrTemplates = ivrTemplateManager.getIvrTemplates(simGroup);
    return ivrTemplates.stream().map(e -> new IvrTemplateWrapper(e.getName(), e.getCallDropReason().name(), String.valueOf(e.getCallDropReason().getCauseCode() & 0xFF)))
        .collect(Collectors.toList());
  }

  @Override
  public void createIvrTemplateSimGroup(final String simGroup) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for create a new SIM group");
    }
    ivrTemplateManager.createSimGroup(simGroup);
  }

  @Override
  public void removeIvrTemplateSimGroups(final List<String> simGroups) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for remove SIM group");
    }
    ivrTemplateManager.removeIvrTemplateSimGroups(simGroups);
  }

  @Override
  public Map<String, Integer> getCallDropReasons() {
    return Arrays.stream(CallDropReason.values()).collect(Collectors.toMap(CallDropReason::name, e-> e.getCauseCode() & 0xFF));
  }

  @Override
  public void uploadIvrTemplate(final String simGroup, final byte[] bytes, final String fileName) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for upload ivr template");
    }
    ivrTemplateManager.uploadIvrTemplate(simGroup, bytes, fileName);
  }

  @Override
  public void removeIvrTemplate(final String simGroup, final List<IvrTemplateWrapper> ivrTemplates) throws IvrTemplateException {
    if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().getIvrCorrelationEnable()) {
      throw new IvrTemplateException("IVR correlation is disable. Please enable it for upload ivr template");
    }
    ivrTemplateManager.removeIvrTemplate(simGroup, ivrTemplates);
  }

}
