/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannels;
import com.flamesgroup.antrax.control.voiceserver.ISCEmulatorSubChannelsHandler;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.gsmb.IGSMEntry;
import com.flamesgroup.device.gsmb.ISCEmulatorChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannel;
import com.flamesgroup.device.gsmb.ISCEmulatorSubChannelHandler;
import com.flamesgroup.device.gsmb.SCEmulatorError;
import com.flamesgroup.device.gsmb.SCEmulatorState;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.APDUResponse;
import com.flamesgroup.device.sc.ATR;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

// work only for Siemens modules
public class SCEmulatorSubChannels implements ISCEmulatorSubChannels {

  private static final Logger logger = LoggerFactory.getLogger(SCEmulatorSubChannels.class);

  private final Map<ChannelUID, IGSMEntry> gsmEntries = new ConcurrentHashMap<>();

  @Override
  public void start(final ChannelUID channel, final ATR atr, final ISCEmulatorSubChannelsHandler scEmulatorSubChannelHandler)
      throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    ISCEmulatorChannel scEmulatorChannel = getSCEmulatorChannel(channel);
    scEmulatorChannel.enslave();

    ISCEmulatorSubChannel scEmulatorSubChannel = scEmulatorChannel.getSCEmulatorSubChannel();
    try {
      scEmulatorSubChannel.start(atr, new SCEmulatorSubChannelHandler(channel, scEmulatorSubChannelHandler));
    } catch (ChannelException | InterruptedException e) {
      scEmulatorChannel.free();
      throw e;
    }
  }

  @Override
  public void stop(final ChannelUID channel) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    ISCEmulatorChannel scEmulatorChannel = getSCEmulatorChannel(channel);
    try {
      scEmulatorChannel.getSCEmulatorSubChannel().stop();
    } finally {
      scEmulatorChannel.free();
    }
  }

  @Override
  public void writeAPDUResponse(final ChannelUID channel, final APDUResponse response) throws RemoteException, IllegalChannelException, ChannelException, InterruptedException {
    ISCEmulatorChannel scEmulatorChannel = getSCEmulatorChannel(channel);
    scEmulatorChannel.getSCEmulatorSubChannel().writeAPDUResponse(response);
  }

  public void addGsmEntry(final ChannelUID channel, final IGSMEntry gsmEntry) {
    gsmEntries.put(channel, gsmEntry);
  }

  public void removeGsmEntry(final ChannelUID channel) {
    gsmEntries.remove(channel);
  }

  private ISCEmulatorChannel getSCEmulatorChannel(final ChannelUID channel) throws IllegalChannelException {
    IGSMEntry gsmEntry = gsmEntries.get(channel);
    if (gsmEntry == null) {
      throw new IllegalChannelException(channel);
    }

    return gsmEntry.getSCEmulatorChannel();
  }

  private class SCEmulatorSubChannelHandler implements ISCEmulatorSubChannelHandler {

    private final ChannelUID channel;
    private final ISCEmulatorSubChannelsHandler scEmulatorSubChannelsHandler;

    public SCEmulatorSubChannelHandler(final ChannelUID channel, final ISCEmulatorSubChannelsHandler scEmulatorSubChannelsHandler) {
      this.channel = channel;
      this.scEmulatorSubChannelsHandler = scEmulatorSubChannelsHandler;
    }

    @Override
    public void handleAPDUCommand(final APDUCommand command) {
      try {
        scEmulatorSubChannelsHandler.handleAPDUCommand(channel, command);
      } catch (RemoteException | IllegalChannelException e) {
        logger.warn("[{}] - can't handle APDU command for [{}]", this, channel, e);
      }
    }

    @Override
    public void handleErrorState(final SCEmulatorError error, final SCEmulatorState state) {
      try {
        scEmulatorSubChannelsHandler.handleErrorState(channel, error, state);
      } catch (RemoteException | IllegalChannelException e) {
        logger.warn("[{}] - can't handle Error State for [{}]", this, channel, e);
      }
    }

  }

}
