/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.timemachine.ConditionalState;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;

public class CallingState implements ConditionalState {

  private int callNumberAfterLastSelfCall = 0;
  private final CallChannel callChannel;

  private State idleAfterSuccessfulCallState;
  private State idleAfterZeroCallState;
  private State idleBeforeUnregistrationState;

  private State idleBeforeSelfCallState;
  private long initialCallDuration;

  public CallingState(final CallChannel callChannel) {
    assert callChannel != null : "callChannel couldn't be null";
    this.callChannel = callChannel;
  }

  @Override
  public void enterState() {
    assert idleAfterSuccessfulCallState != null : "idleBetweenCallsState is null";
    assert idleBeforeSelfCallState != null : "idleBeforeSelfCallState is null";

    ++callNumberAfterLastSelfCall;
    initialCallDuration = callChannel.getCallDuration();
    callChannel.changeChannelState(CallChannelState.State.OUTGOING_CALL, -1);
  }

  @Override
  public boolean isFinished() {
    return callChannel.isOutgoingCallReleased();
  }

  @Override
  public State getNextState() {
    if (callChannel.getCallDuration() - initialCallDuration <= 0) {
      return idleAfterZeroCallState;
    }
    if (incomingCallTimeComes()) {
      callNumberAfterLastSelfCall = 0;
      return idleBeforeSelfCallState;
    }
    if (callChannel.getSIMUnit().forceShouldStopActivity() || callChannel.getSIMUnit().forceShouldStopSession()) {
      return idleBeforeUnregistrationState;
    }
    return idleAfterSuccessfulCallState;
  }

  private boolean incomingCallTimeComes() {
    SIMGroup group = callChannel.getSIMUnit().getSimData().getSimGroup();
    if (group.getSelfDialFactor() > 0) {
      return ((callNumberAfterLastSelfCall / group.getSelfDialFactor()) - Math.random()) > (1 - (group.getSelfDialChance() / 100.0d));
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "CallingState";
  }

  @Override
  public void tickRoutine() {
  }

  public void initialize(final StatesBuilder b) {
    idleAfterSuccessfulCallState = b.getIdleAfterSuccessfulCallState();
    idleBeforeSelfCallState = b.getIdleBeforeSelfCallState();
    idleBeforeUnregistrationState = b.getIdleBeforeUnregistrationState();
    idleAfterZeroCallState = b.getIdleAfterZeroCallState();
  }

}
