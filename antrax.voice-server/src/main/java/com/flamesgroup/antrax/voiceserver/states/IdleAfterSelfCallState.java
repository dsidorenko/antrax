/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.timemachine.TransientState;
import com.flamesgroup.antrax.voiceserver.CallChannel;

public class IdleAfterSelfCallState implements TransientState {

  private final CallChannel cc;

  private State waitingForCallState;
  private long period;

  public IdleAfterSelfCallState(final CallChannel cc) {
    this.cc = cc;
  }

  @Override
  public long getPeriod() {
    return period;
  }

  @Override
  public void enterState() {
    period = cc.getSIMUnit().getSimData().getSimGroup().getIdleAfterSelfCallTimeout().getTimeout();
    assert waitingForCallState != null : "waitingForCallState was not set";
    cc.changeChannelState(CallChannelState.State.IDLE_AFTER_SELF_CALL, period);
  }

  @Override
  public State getNextState() {
    return waitingForCallState;
  }

  @Override
  public void tickRoutine() {
  }

  public void initialize(final StatesBuilder b) {
    waitingForCallState = b.getWaitingForCallState();
  }

}
