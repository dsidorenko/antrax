/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.voiceserver.call.ICallStatusListener;
import com.flamesgroup.antrax.voiceserver.channels.CyclicBuffer;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;

import java.util.concurrent.atomic.AtomicBoolean;

public class FakeRingingMediaStream implements IMediaStream, ICallStatusListener {

  private final IMediaStream mediaStream;
  private final CyclicBuffer ringingBuffer;

  private final AtomicBoolean fakeRinging = new AtomicBoolean(true);

  public FakeRingingMediaStream(final IMediaStream mediaStream, final CyclicBuffer ringingBuffer) {
    this.mediaStream = mediaStream;
    this.ringingBuffer = ringingBuffer;
  }

  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    mediaStream.open(mediaStreamHandler);
  }

  @Override
  public void close() {
    mediaStream.close();
  }

  @Override
  public boolean isOpen() {
    return mediaStream.isOpen();
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    if (fakeRinging.get()) {
      ringingBuffer.copySlice(data, offset, length);
    }
    mediaStream.write(timestamp, data, offset, length);
  }

  @Override
  public void handleAlerting() {
  }

  @Override
  public void handleAnswer() {
    fakeRinging.set(false);
  }

  @Override
  public void handleHangup() {
  }

}
