/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.activity.IGsmViewManager;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.control.distributor.IGsmChannelManagerHandler;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.IMSI;
import com.flamesgroup.unit.NetworkSurveyInfo;
import com.flamesgroup.utils.AntraxProperties;
import com.flamesgroup.utils.OperatorsStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ImsiCatcher {

  private static final Logger logger = LoggerFactory.getLogger(ImsiCatcher.class);

  private final IGsmViewManager gsmViewManager;
  private final IGsmChannelManagerHandler channelManagerHandler;
  private final Map<CellKey, GsmView> gsmViews = new ConcurrentHashMap<>();
  private Long startTime;
  private final AtomicLong lastUpdateGsmViewsTime = new AtomicLong(0);
  private final BlockingQueue<CellInfoData> cellInfoQueue = new LinkedBlockingQueue<>();

  private final Lock gsmViewLock = new ReentrantLock();

  public ImsiCatcher(final IGsmViewManager gsmViewManager, final IGsmChannelManagerHandler channelManagerHandler) {
    this.gsmViewManager = gsmViewManager;
    this.channelManagerHandler = channelManagerHandler;
  }

  public void start() {
    try {
      Date startNetworkSurvey = gsmViewManager.getStartNetworkSurvey(AntraxProperties.SERVER_NAME);
      if (startNetworkSurvey != null) {
        startTime = startNetworkSurvey.getTime();
      } else {
        startTime = System.currentTimeMillis();
      }
    } catch (RemoteException e) {
      logger.warn("[{}] - can't get start network survey", this, e);
      return;
    }

    List<GsmView> gsmViews;
    try {
      gsmViews = gsmViewManager.listGsmView(AntraxProperties.SERVER_NAME);
      String logInfo = gsmViews.stream().map(gsmView -> gsmView.getCellKey().toString()).reduce((s1, s2) -> s1 + System.lineSeparator() + s2).orElse("");
      logger.info("[{}] - got cell keys: [{}]", this, logInfo);
    } catch (RemoteException e) {
      logger.warn("[{}] - can't get gsm views", this, e);
      return;
    }

    this.gsmViews.putAll(gsmViews.stream().collect(Collectors.toMap(GsmView::getCellKey, Function.identity())));
    new GsmViewReload().start();
    new CellInfoUpdate().start();
  }

  public boolean isActive() {
    return ((System.currentTimeMillis() - startTime) > VoiceServerPropUtils.getInstance().getVoiceServerProperties().getNetworkSurveyDuration())
        && VoiceServerPropUtils.getInstance().getVoiceServerProperties().getImsiCatcherEnabled();
  }

  public void updateGsmViews(final List<NetworkSurveyInfo> validNetworkSurveyInfo, final Date handleTime) {
    gsmViewLock.lock();
    try {
      boolean canUpdateCellAdjustments = false;
      for (NetworkSurveyInfo surveyInfo : validNetworkSurveyInfo) {
        CellKey cellKey = new CellKey(surveyInfo.getLac(), surveyInfo.getCellId(), surveyInfo.getBsic(), surveyInfo.getArfcn());
        GsmView gsmView = new GsmView();
        gsmView.setCellKey(cellKey)
            .setServingCell(false)
            .setTrusted(false)
            .setNetworkSurveyLastRxLev(surveyInfo.getRxLev())
            .setCellInfoLastRxLev(0)
            .setLastMcc(surveyInfo.getMcc())
            .setLastMnc(surveyInfo.getMnc())
            .setNetworkSurveyNumberOccurrences(1)
            .setServingNumberOccurrences(0)
            .setNeighborsNumberOccurrences(0)
            .setFirstAppearance(handleTime)
            .setLastAppearance(handleTime);
        GsmView gsmViewPresent = gsmViews.putIfAbsent(cellKey, gsmView);
        if (gsmViewPresent == null) {
          canUpdateCellAdjustments = true;
          logger.info("[{}] - add cell key: [{}]", this, cellKey);
        } else {
          gsmViewPresent.setNetworkSurveyLastRxLev(surveyInfo.getRxLev())
              .setLastMcc(surveyInfo.getMcc())
              .setLastMnc(surveyInfo.getMnc())
              .setLastAppearance(handleTime)
              .incrementNetworkSurveyNumberOccurrences();
        }
      }
      gsmViewManager.updateGsmView(AntraxProperties.SERVER_NAME, gsmViews.values().stream().collect(Collectors.toList()));
      if (canUpdateCellAdjustments) {
        gsmViewManager.setServerForUpdateCellAdjustments(AntraxProperties.SERVER_NAME, true);
      }
    } catch (RemoteException e) {
      logger.warn("[{}] - can't update gsm views", this, e);
    } finally {
      gsmViewLock.unlock();
    }
  }

  public void handleCellInfos(final CellInfo[] cellInfos, final IMSI imsi, final ChannelUID channelUID) {
    if (!cellInfoQueue.offer(new CellInfoData(cellInfos, imsi, channelUID))) {
      logger.warn("[{}] - can't offer cellInfoQueue", this);
    }
  }

  public Pair<Boolean, String> checkImsiCatcher(final CellInfo[] cellInfos) {
    Pair<Boolean, String> isImsiCatcher = new Pair<>(false, "");

    if (cellInfos.length <= 0) {
      logger.info("[{}] - receive empty cellInfos to process IMSI catcher", this);
      return isImsiCatcher;
    }

    if (!isActive() || gsmViews.isEmpty()) {
      return isImsiCatcher;
    }

    CellInfo servingCell = cellInfos[0];
    CellKey servingCellKey = new CellKey(servingCell.getLac(), servingCell.getCellId(), servingCell.getBsic(), servingCell.getArfcn());
    GsmView servingGsmView = gsmViews.get(servingCellKey);
    if (servingGsmView == null) {
      return new Pair<>(true, String.format("IMSI catcher detected: didn't found cell key %s of serving cell in Gsm-view", servingCellKey));
    } else if (!servingGsmView.isTrusted()) {
      return new Pair<>(true, String.format("IMSI catcher detected: serving cell %s isn't trusted", servingCellKey));
    }

    // check IMSI catcher at neighbours cells
    if (VoiceServerPropUtils.getInstance().getVoiceServerProperties().getImsiCatcherCheckNeighboursEnabled()) {
      for (int i = 1; i < cellInfos.length; i++) {
        CellInfo neighbourCellInfo = cellInfos[i];
        if (neighbourCellInfo.getCellId() > 0 && neighbourCellInfo.getLac() > 0 && neighbourCellInfo.getBsic() > 0) {
          CellKey neighbourCellKey = new CellKey(neighbourCellInfo.getLac(), neighbourCellInfo.getCellId(), neighbourCellInfo.getBsic(), neighbourCellInfo.getArfcn());
          GsmView neighbourGsmView = gsmViews.get(neighbourCellKey);
          if (neighbourGsmView == null) {
            return new Pair<>(true, String.format("IMSI catcher detected: didn't found cell key %s of neighbour cell in Gsm-view", neighbourCellKey));
          } else if (!neighbourGsmView.isTrusted()) {
            return new Pair<>(true, String.format("IMSI catcher detected: neighbour cell %s isn't trusted", neighbourCellKey));
          }
        }
      }
    }
    return isImsiCatcher;
  }

  public AtomicLong getLastUpdateGsmViewsTime() {
    return lastUpdateGsmViewsTime;
  }

  private class CellInfoUpdate extends Thread {

    public CellInfoUpdate() {
      setName(CellInfoUpdate.class.getSimpleName());
      setDaemon(true);
    }

    @Override
    public void run() {
      while (true) {
        try {
          updateCellInfo(cellInfoQueue.take());
        } catch (InterruptedException e) {
          logger.error("[{}] - can't waiting take", this, e);
          return;
        }
      }
    }

    private void updateCellInfo(final CellInfoData cellInfoData) {
      CellInfo[] cellInfos = cellInfoData.getCellInfos();
      ChannelUID channelUID = cellInfoData.channelUID;
      try {
        channelManagerHandler.handleChannelCellInfos(AntraxProperties.SERVER_NAME, channelUID, Arrays.asList(cellInfos));
      } catch (RemoteException e) {
        logger.warn("[{}] - can't handle cell infos for gsm channel [{}]", this, channelUID, e);
      }

      if (!VoiceServerPropUtils.getInstance().getVoiceServerProperties().geNetworkSurveyEnabled()) {
        return;
      }
      gsmViewLock.lock();
      try {
        boolean canUpdateCellAdjustments = false;
        short mcc = 0;
        short mnc = 0;
        IMSI imsi = cellInfoData.getImsi();
        OperatorsStorage.Operator operatorByCode = OperatorsStorage.getInstance().findOperatorByCode(cellInfos[0].getPlmn());
        if (operatorByCode == null && imsi != null) {
          operatorByCode = OperatorsStorage.getInstance().findOperatorByImsi(imsi);
        }
        if (operatorByCode != null) {
          mcc = operatorByCode.getMccShort();
          mnc = operatorByCode.getMncShort();
        }
        for (int i = 0; i < cellInfos.length; i++) {
          CellInfo cellInfo = cellInfos[i];
          CellKey cellKey = new CellKey(cellInfo.getLac(), cellInfo.getCellId(), cellInfo.getBsic(), cellInfo.getArfcn());
          GsmView gsmView = new GsmView();
          Date handleTime = new Date();
          gsmView.setCellKey(cellKey)
              .setServingCell(false)
              .setTrusted(false)
              .setCellInfoLastRxLev(cellInfo.getRxLev())
              .setNetworkSurveyLastRxLev(0)
              .setLastMcc(mcc)
              .setLastMnc(mnc)
              .setNetworkSurveyNumberOccurrences(0)
              .setServingNumberOccurrences(i == 0 ? 1 : 0)
              .setNeighborsNumberOccurrences(i != 0 ? 1 : 0)
              .setFirstAppearance(handleTime)
              .setLastAppearance(handleTime);
          GsmView gsmViewPresent = gsmViews.putIfAbsent(cellKey, gsmView);
          if (gsmViewPresent == null) {
            canUpdateCellAdjustments = true;
            logger.info("[{}] - add cell key: [{}]", this, cellKey);
          } else {
            gsmViewPresent.setCellInfoLastRxLev(cellInfo.getRxLev())
                .setLastMcc(mcc)
                .setLastMnc(mnc)
                .setLastAppearance(handleTime);

            if (i == 0) {
              gsmViewPresent.incrementServingNumberOccurrences();
            } else {
              gsmViewPresent.incrementNeighborsNumberOccurrences();
            }
          }
        }
        gsmViewManager.updateGsmView(AntraxProperties.SERVER_NAME, gsmViews.values().stream().collect(Collectors.toList()));
        if (canUpdateCellAdjustments) {
          gsmViewManager.setServerForUpdateCellAdjustments(AntraxProperties.SERVER_NAME, true);
        }
      } catch (RemoteException e) {
        logger.warn("[{}] - can't update gsm views", this, e);
      } finally {
        gsmViewLock.unlock();
      }
    }

  }

  private class GsmViewReload extends Thread {

    private final Long WAITING_TIME = TimeUnit.SECONDS.toMillis(10);

    public GsmViewReload() {
      setName(GsmViewReload.class.getSimpleName());
      setDaemon(true);
    }

    @Override
    public void run() {
      while (true) {
        try {
          if (gsmViewManager.isUpdateImsiCatcher(AntraxProperties.SERVER_NAME)) {
            gsmViewLock.lock();
            try {
              gsmViews.clear();
              gsmViews.putAll(gsmViewManager.listGsmView(AntraxProperties.SERVER_NAME).stream().collect(Collectors.toMap(GsmView::getCellKey, Function.identity())));
              gsmViewManager.setServerForUpdateImsiCatcher(AntraxProperties.SERVER_NAME, false);
            } catch (RemoteException e) {
              logger.warn("[{}] - can't get list GsmView", this, e);
            } finally {
              gsmViewLock.unlock();
            }
          }
        } catch (RemoteException e) {
          logger.warn("[{}] - can't get update ImsiCatcher", this, e);
          throw new ControlServerPingException(e);
        }

        try {
          Thread.sleep(WAITING_TIME);
        } catch (InterruptedException e) {
          logger.error("[{}] - can't waiting", this, e);
          return;
        }
      }
    }

  }

  private class CellInfoData {

    private final CellInfo[] cellInfos;
    private final IMSI imsi;
    private final ChannelUID channelUID;

    public CellInfoData(final CellInfo[] cellInfos, final IMSI imsi, final ChannelUID channelUID) {
      this.cellInfos = cellInfos;
      this.imsi = imsi;
      this.channelUID = channelUID;
    }

    public CellInfo[] getCellInfos() {
      return cellInfos;
    }

    public IMSI getImsi() {
      return imsi;
    }

    public ChannelUID getChannelUID() {
      return channelUID;
    }

  }

}
