/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

public class CyclicBuffer {

  private final byte[] buffer;

  private int marker;

  public CyclicBuffer(final byte[] buffer) {
    this.buffer = buffer;
  }

  public void copySlice(final byte[] data, final int off, final int size) {
    int written = 0;
    while (written < size) {
      int shouldWrite = buffer.length - marker;
      if (shouldWrite > size - written)
        shouldWrite = size - written;
      System.arraycopy(buffer, marker, data, off + written, shouldWrite);
      written += shouldWrite;
      marker += shouldWrite;
      if (marker >= buffer.length)
        marker -= buffer.length;
    }
  }

}
