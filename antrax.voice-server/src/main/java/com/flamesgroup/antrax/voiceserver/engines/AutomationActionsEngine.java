/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.engines;

import com.flamesgroup.antrax.autoactions.ActionExecutionStatus;
import com.flamesgroup.antrax.autoactions.IActionExecutionManager;
import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.CallChannelPool;
import com.flamesgroup.antrax.voiceserver.CallChannelPool.BulkIterator;
import com.flamesgroup.antrax.voiceserver.VSStatus;
import com.flamesgroup.commons.Pair;
import com.flamesgroup.utils.AntraxProperties;
import com.flamesgroup.utils.FailureReactTimeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.UUID;

public class AutomationActionsEngine extends Thread {

  private final FailureReactTimeout timeout = new FailureReactTimeout(5000, 60000);

  private final Logger logger = LoggerFactory.getLogger(AutomationActionsEngine.class);

  private final VSStatus status;
  private final IActionExecutionManager actionExecutionManager;
  private final CallChannelPool pool;

  public AutomationActionsEngine(final VSStatus status, final IActionExecutionManager actionExecutionManager, final CallChannelPool pool) {
    super("AutoActionsEngine");
    this.status = status;
    this.actionExecutionManager = actionExecutionManager;
    this.pool = pool;
  }

  @Override
  public void run() {
    while (!interrupted() && !status.terminating) {

      try {
        while (requestForActionToExecute()) {
        }
        timeout.register(true);
      } catch (Exception e) {
        logger.warn("[{}] - failed to request execution", this, e);
        timeout.register(false);
      }

      synchronized (status) {
        try {
          if (!status.terminating) {
            timeout.sleep(status);
          }
        } catch (InterruptedException e) {
          logger.trace("[{}] - interrupted while sleeping. Breaking loop", this, e);
          break;
        }
      }
    }
  }

  private boolean requestForActionToExecute() {
    logger.trace("[{}] - attempting to request new action to execute", this);
    Pair<UUID, Action> execution;
    try {
      execution = actionExecutionManager.getExecution(AntraxProperties.SERVER_NAME);
    } catch (RemoteException e) {
      throw new ControlServerPingException(e);
    }

    if (execution == null) {
      logger.trace("[{}] - no actions were found", this);
      return false;
    }

    UUID executionID = execution.first();
    Action action = execution.second();
    logger.trace("[{}] - Got {}", this, executionID);
    BulkIterator bulkIterator = pool.takeBulk(action);
    if (bulkIterator.hasNext()) {
      startExecutionThread(bulkIterator, executionID, action);
    } else {
      logger.trace("[{}] - can't execute action: no channels to execute action", this);
      try {
        actionExecutionManager.handleActionExecutionStatus(AntraxProperties.SERVER_NAME, executionID, ActionExecutionStatus.CANNOT_EXECUTE);
      } catch (RemoteException e) {
        throw new ControlServerPingException(e);
      }
    }
    return true;
  }

  private void startExecutionThread(final BulkIterator bulkIterator, final UUID executionID, final Action action) {
    new Thread("ActExec " + executionID) {
      @Override
      public void run() {
        logger.trace("[{}] - starting execution", this);
        ActionExecutionStatus actionExecutionStatus;
        while (true) {
          CallChannel callChannel = bulkIterator.next();
          if (callChannel == null) {
            logger.trace("[{}] - can't execute action: no free channels left", this);
            actionExecutionStatus = ActionExecutionStatus.BUSY;
            break;
          }

          if (callChannel.owned()) {
            logger.trace("[{}] - {} owned, can't execute {} now", this, callChannel, executionID);
            callChannel.untake();
            continue;
          }

          logger.trace("[{}] - testing whether {} can execute {}", this, callChannel, executionID);
          ActionProviderScript script;
          try {
            script = callChannel.getActionProvider(action, callChannel.toRegisteredInGSMChannel());
          } catch (Exception e) {
            logger.warn("[{}] - failed to check whether action can be executed", this, e);
            callChannel.untake();
            continue;
          }

          if (script == null) {
            logger.trace("[{}] - {} can't execute {} now", this, callChannel, executionID);
            callChannel.untake();
            continue;
          }

          logger.trace("[{}] - {} can execute {}", this, callChannel, executionID);
          try {
            callChannel.executeAction(script, action, callChannel.toRegisteredInGSMChannel(), executionID);
            logger.trace("[{}] - {} was successfully executed by {}", this, executionID, callChannel);
            actionExecutionStatus = ActionExecutionStatus.FINISHED;
          } catch (Exception e) {
            logger.debug("[{}] - failed to execute {}", this, executionID, e);
            actionExecutionStatus = ActionExecutionStatus.FAILED;
          }

          callChannel.untake();
          callChannel.toRegisteredInGSMChannel().dropCall(); // If somebody forget to release call
          break;
        }

        try {
          actionExecutionManager.handleActionExecutionStatus(AntraxProperties.SERVER_NAME, executionID, actionExecutionStatus);
        } catch (RemoteException e) {
          throw new ControlServerPingException(e);
        }
      }
    }.start();
  }

}
