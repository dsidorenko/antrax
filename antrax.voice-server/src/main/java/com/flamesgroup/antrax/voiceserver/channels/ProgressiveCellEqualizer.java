/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

class ProgressiveCellEqualizer extends Thread {

  private static final Logger logger = LoggerFactory.getLogger(ProgressiveCellEqualizer.class);

  private static final int MAX_RX_LEV_DELTA = 10;
  private static final int MIN_RX_LEV_DELTA = 1;
  private static final long CELL_MONITORING_PERIOD = TimeUnit.SECONDS.toMillis(90); //сell monitor executes every 1 minute, but wait 1 minute 30 seconds for guarantee real cell monitor information

  private final GSMUnit gsmUnit;
  private final CellEqualizerManager cellEqualizerManager;
  private final ProgressiveCellEqualizerData pced;

  private int countRxLevDelta;

  private volatile Thread currentThread;

  public ProgressiveCellEqualizer(final GSMUnit gsmUnit, final CellEqualizerManager cellEqualizerManager, final ProgressiveCellEqualizerData pced) {
    this.gsmUnit = gsmUnit;
    this.cellEqualizerManager = cellEqualizerManager;
    this.pced = pced;
    setName("ProgressiveCellEqualizer@" + hashCode() + " for " + this.gsmUnit.getChannelUID());
    setDaemon(true);
  }

  @Override
  public void run() {
    logger.debug("[{}] - start progressive cellEqualizer", this);
    Random random = new Random();

    Map<Integer, Integer> cellAdjustments = new HashMap<>(cellEqualizerManager.getServerCellAdjustments());

    Thread thisThread = Thread.currentThread();
    currentThread = thisThread;
    while (currentThread == thisThread) {
      if (pced.getCurrentRxLev() >= pced.getDestinationRxLev() && pced.getPrevCountRxLevDelta() == 0) {
        logger.debug("[{}] - waiting real cell monitor will be got: {} mils", this, CELL_MONITORING_PERIOD);
        try {
          Thread.sleep(CELL_MONITORING_PERIOD);
        } catch (InterruptedException e) {
          logger.error("[{}] - can't wait real cell monitor information", this, e);
          Thread.currentThread().interrupt();
        }
        logger.debug("[{}] - achieve the goal", this);
        break;
      }

      if (pced.getCurrentRxLev() < pced.getDestinationRxLev()) {
        int curRxLev = random.nextInt(MAX_RX_LEV_DELTA - MIN_RX_LEV_DELTA + 1) + MIN_RX_LEV_DELTA;
        countRxLevDelta += curRxLev;
        if (countRxLevDelta > 60) {
          countRxLevDelta = 60;
          pced.setCurrentRxLev(pced.getCurrentRxLev() + (60 - (countRxLevDelta - curRxLev)));
        } else {
          pced.setCurrentRxLev(pced.getCurrentRxLev() + curRxLev);
        }
        cellAdjustments.put(pced.getCurrentArfcn(), countRxLevDelta);
      }

      if (pced.getPrevArfcn() < 0 || pced.getPrevCountRxLevDelta() == 0) {
        cellAdjustments.remove(pced.getPrevArfcn());
      } else {
        int prevRxLev = random.nextInt(MAX_RX_LEV_DELTA - MIN_RX_LEV_DELTA + 1) + MIN_RX_LEV_DELTA;
        pced.setPrevCountRxLevDelta(pced.getPrevCountRxLevDelta() - prevRxLev);
        if (pced.getPrevCountRxLevDelta() < 0) {
          pced.setPrevCountRxLevDelta(0);
        }
        cellAdjustments.put(pced.getPrevArfcn(), pced.getPrevCountRxLevDelta());
      }

      try {
        gsmUnit.executeCellEqualizer(cellAdjustments);
        logger.debug("[{}] - executed cellEqualizer", this);
        logger.trace("[{}] - cellAdjustments: [{}]", this, cellAdjustments);
      } catch (GsmHardwareError e) {
        logger.error("[{}] - can't execute cellEqualizer, try again late", this, e);
        break;
      }

      if (currentThread != thisThread) {
        continue;
      }

      try {
        Thread.sleep(TimeUnit.SECONDS.toMillis(random.nextInt(pced.getMaxWaitingTime() - pced.getMinWaitingTime() + 1) + pced.getMinWaitingTime()));
      } catch (InterruptedException e) {
        logger.error("[{}] - can't wait nex iteration", this, e);
        Thread.currentThread().interrupt();
      }
    }
    pced.setPrevArfcn(pced.getCurrentArfcn());
    pced.setPrevCountRxLevDelta(countRxLevDelta);
    logger.debug("[{}] - end progressive cellEqualizer", this);
  }

  public void terminate() {
    currentThread = null;
  }

}
