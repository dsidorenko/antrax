/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.call;

import com.flamesgroup.antrax.activity.SimEvent;
import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.storage.commons.impl.CDR;
import com.flamesgroup.antrax.voiceserver.ITerminatingCallFASHandler;
import com.flamesgroup.antrax.voiceserver.SilentMediaStream;
import com.flamesgroup.antrax.voiceserver.sim.SIMUnit;
import com.flamesgroup.jiax2.CallDropReason;
import com.flamesgroup.jiax2.CallState;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.ITerminatingCall;
import com.flamesgroup.media.codec.audio.IAudioCodec;

import java.util.Collections;
import java.util.LinkedHashMap;

public class ScriptTerminatingCallAdapter implements ITerminatingCall, ITerminatingCallFASHandler {

  private final String called;
  private final CallStateChangeHandler handler;
  private final SIMUnit simUnit;

  private final LinkedHashMap<CallStateChangeHandler.CallState, Long> states = new LinkedHashMap<>();
  private volatile long lastStateChangeTime = 0;
  private volatile CallStateChangeHandler.CallState prevState = null;

  public ScriptTerminatingCallAdapter(final String called, final CallStateChangeHandler handler, final SIMUnit simUnit) {
    this.called = called;
    this.handler = handler;
    this.simUnit = simUnit;
  }

  @Override
  public void ringing() {
    fireStateChange(CallStateChangeHandler.CallState.ALERTING);
  }

  @Override
  public void answer() {
    fireStateChange(CallStateChangeHandler.CallState.ACTIVE);
  }

  @Override
  public CallState getState() {
    throw new UnsupportedOperationException();
  }

  @Override
  public String getCaller() {
    return null;
  }

  @Override
  public String getCalled() {
    return called;
  }

  @Override
  public String getCalledContext() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IMediaStream getAudioMediaStream() {
    return new SilentMediaStream();
  }

  @Override
  public long getMediaFormat() {
    throw new UnsupportedOperationException();
  }

  @Override
  public IAudioCodec getAudioCodec() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void hangup(final CallDropReason callDropReason) {
    fireStateChange(CallStateChangeHandler.CallState.DROP);
  }

  @Override
  public void sendDtmf(final char dtmf) {
  }

  private void fireStateChange(final CallStateChangeHandler.CallState newState) {
    if (prevState != null) {
      states.put(prevState, System.currentTimeMillis() - lastStateChangeTime);
    }
    lastStateChangeTime = System.currentTimeMillis();
    prevState = newState;
    handler.handleStateChange(newState, Collections.unmodifiableMap(states));
  }

  @Override
  public void handleFAS(final ITerminatingCall terminatingCall, final CDR cdr) {
    fireStateChange(CallStateChangeHandler.CallState.DROP);
    simUnit.handleEvent(SimEvent.callEnded(cdr));
  }

}
