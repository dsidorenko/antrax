/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.statuses;

import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.commons.TimeUtils;
import com.flamesgroup.daemonext.IDaemonExtStatus;

import java.util.Objects;

public class VoiceServerStatus implements IDaemonExtStatus {

  private final IActivityLogger activityLogger;

  public VoiceServerStatus(final IActivityLogger activityLogger) {
    Objects.requireNonNull(activityLogger, "activityLogger mustn't be null");
    this.activityLogger = activityLogger;
  }

  @Override
  public String getName() {
    return "status";
  }

  @Override
  public String[] getDescription() {
    return new String[] {"show voice server status"};
  }

  @Override
  public String execute(final String[] args) {
    return String.format("Voice server running : %s", TimeUtils.writeTimeMs(activityLogger.getLongServerInfo().getUptimeMs()));
  }

}
