### Content

* [Technical documentation](manual/README.md)
* [Support documentation](support/README.md)

### Note

All vector images of documentation draw (or will be redraw) with [draw.io](https://www.draw.io) web application. Currently, because of the issue with displaying internal SVG files in GitLab, [draw.io](https://www.draw.io) files export to PNG (`Export as -> PNG...` with `Include a copy of my diagram` flag enabled). But when this issue is resolved, all exported PNG files must be replaced by SVG (`Export as -> SVG...` with `Zoom: 100%`, `Borderwidth: 10`, `Crop` and `Include a copy of my diagram` flags enabled).

Enabling `Include a copy of my diagram` flag on export allow to open and edit this PNG/SVG file in [draw.io](https://www.draw.io) web application in future.
