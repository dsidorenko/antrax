## Configuration of SMS termination

Antrax can send sms via Alaris SMS platform http://www.alarislabs.com/

### Configuration of Antrax

Need to change configuration in file by path:
/opt/antrax/control-server/etc/control-server.properties
Section for setup SMS termination

    alaris.sms.server.hostname=127.0.0.1
    alaris.sms.server.port=8888
    alaris.sms.server.useSsl=true

    # Multi user consists of records = X@Y@Z
    # X - login
    # Y - password
    # Z - percentage of real sms statuses
    # Record must be divided by ";"
    alaris.sms.server.multiUser=admin@password@20;user@password@20

    # Amount of trying send sms
    alaris.sms.resent.amount=100
    alaris.sms.limit.pear.day=10000
    alaris.sms.sender.buffer=100

    # Enable or disable sms delivery report
    alaris.sms.delivery.report=true

In field **alaris.sms.server.hostname** need type IP address that will listen for incoming SMS traffic.<br>
In field **alaris.sms.server.port** need type port that will listen for incoming SMS traffic.<br>
In field **alaris.sms.server.useSsl** configure enable/disable ssl protocol<br>
«true» value is enable ssl (default).<br>
«false» value is disable ssl.<br>
Section **alaris.sms.server.multiUser** is for adding information about user authentication. User authentication data are entered in the format: admin@password@20;user@password@20, where «admin» - name of user, «password» - password for authentication, «20»- the percentage of successful delivery statuses of sending sms. User data for authentication are separated special character «@», and data of different users are separated by special character «;».<br>
In field **alaris.sms.resent.amount** indicates the number of attempts to send SMS.<br>
In field **alaris.sms.limit.pear.day** indicates the capacity of sms per day.<br>
In field **alaris.sms.sender.buffer** specifies the number of buffers for SMS, awaiting dispatch system.<br>
In field **alaris.sms.delivery.report** customizable enable\disable the use of the SMS delivery report.<br>
«true» value means the use of SMS delivery report (default).<br>
«false» value means disabling of the SMS delivery report.<br>

### The algorithm of the SMS sending service

1. Incoming SMS via HTTP queued for sending. Them set status ENROUTE
2. Service takes the first SMS and looking for a server to send it, by checking scripts are installed on the server (SMS Filter).
3. If the message is sent it is set SENT status.
4. After sending SMS the algorithm asynchronously waits for the status of the sent message.

### Description of HTTP Server

The server can accept two types of requests:

1. Request for sending a message:

    /api?ani=123&dnis=380507023132&username=admin&password=password&message=Test_status&command=submit&serviceType=

2. Requests for status of messages:

    api?username=admin&password=password&messageId=847ed9cd-4f1a-44c9-99dd-992c0e85d031&command=query

Detailed description of HTTP interaction described in section **HTTP(S) API.**

### HTTP(S) API

#### Introduction

This document provides the HTTP API description for external systems integration with Alaris SMS platform.
HTTPS API enables SMS submission and sms delivery status request.
Authentication information (login/password) for connecting to Alaris SMS platform must be requested from the owner of Alaris SMS platform for each new interconnection.
Credentials must be sent with each API request otherwise one will be rejected as non-authorized. Request can be sent either with GET or POST method to URL provided by the system owner in the format:

    http(s)://<IP_address>:<port>/api? (e.g. https://1.1.1.1:8443/api? or http://1.1.1.1:443/api?)

#### SMS submission

Request format:

    https://1.1.1.1:8443/api?username=<username>&password=<password>&ani=<ani>&dnis=<dnis>&message=<message>&command=submit&serviceType=<serviceType>&longMessageMode=<longMessageMode>

| Parameter | Description|
| -------- | -------- |
| username | Login |
| ani| Caller ID. Technical limitation - alpha-numeric up to 32 symbols. Additional limitation can be provided based on the destination route peculiarities |
| dnis | Destination number. Must be sent in international E.164 format (up to 15 digits allowed) |
| message | Message text |
| command | Request type. Must be set to “submit” value |
| serviceType | Service type, provided by the system owner for the registered interconnection. Can be blank |
| | Type of long messages processing. The following values allowed:|
| | 1 Cut (trim message text to 140 bytes) - shortens the message leaving only first 140 bytes to be sent. |
| | 2 Split and 3 Split SAR - split the message according to the logics described below. The difference between them is in the header to be used, for Split it is UDH header, for Split SAR it is SAR accordingly. |
| longMessageMode | 4 Payload - message_payload field is used for sending the message text |
| | The splitting (options 2/3) depends on the coding: |
| | - dataCoding = 0: one message can contain up to 160 symbols, if more – segment count = message length / 152 |
| | - dataCoding from 1 to 7: one message can contain up to 140 symbols, if more – segment count = message length / 134 |
| | - dataCoding = 8: one message can contain up to 70 symbols (140 bytes), if more – segment count = message length / 67 (134 bytes) |

All parameters except of longMessageMode are obligatory, the default value for longMessageMode is 1 (Cut).

##### Response format

In case of successful processing, status in the header of the HTTP response is 200 OK.
Response body contains the message_id.
Sample of a response in JSON format:

    HTTP/1.1 200 OK
    Content-Type: text/html; charset=UTF-8
    {"message_id":"alss-a1b2c3d4-e5f67890"}

In case of rejected sms (no compatible routes found or authentication data is incorrect), the HTTP response status is - 400 Bad Request. Response body contains the string describing the reason for rejection e.g. NO ROUTES or Unknown username.

    HTTP/1.1 400 Bad Request
    Content-Type: text/html; charset=UTF-8
    Unknown username

In case incorrect password is provided HTTP status is 401 Unauthorized. Response body contains the string describing the reason for rejection.

    HTTP/1.1 401 Unauthorized
    Content-Type: text/html; charset=UTF-8
    Incorrect password

### FYA

To set sending SMS through certain SIM group need to set up the script «SMS Filter».
If the network topology is to use VPS, the VPS on the need to install nginx, which will forward requests to the local server. Or set up port forwarding.
If you change the settings Activity Period Script and Session Period Script these limits will be reset to the initial but the numeric values change existing.




