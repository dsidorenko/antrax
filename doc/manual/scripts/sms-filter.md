## SMS Filter

With the help of SMS filter parameters for out coming calls to GSM network are set. Sample parameters are set via regular expressions.

### Complex SMS Filter

![complex_sms_filter](sms-filter.assets/sms_filter_complex.png)

| Name | Description|
| -------- | -------- |
| rule | rules that establish parameters of filter operation |
| text replace pattern | A typical value that is part of the text |
| text replace by | text for replace |
| allowed B number | pattern for allowed numbers B numbers|
| denied B number | pattern for denied numbers B numbers|
| replace B number pattern | pattern of B number for SMS |
| denied text pattern | denied pattern of SMS text, it is set via regex |
| allowed text pattern  | allowed pattern of SMS text, it is set via regex |
| limit per a day | limit of SMS sent per day |

### Pattern based SMS Filter

![sms_filter_pattern](sms-filter.assets/sms_filter_pattern.png)

| Name | Description|
| -------- | -------- |
| text replace pattern | A typical value that is part of the text |
| text replace by | text for replace |
| allowed B number | pattern for allowed numbers B numbers|
| denied B number | pattern for denied numbers B numbers|
| replace B number pattern | pattern of B number for SMS |
| denied text pattern | denied pattern of SMS text, it is set via regex |
| allowed text pattern  | allowed pattern of SMS text, it is set via regex |
| limit per a day | limit of SMS sent per day |

