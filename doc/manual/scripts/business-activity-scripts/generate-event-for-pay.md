## generate event for pay

Generate event for pay from other SIM card. This script generates event with phone number and amount for pay, this event can be caught by another SIM card.

![g_e_pay](generate-event-for-pay.assets/generate-event-for-pay.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | initial event essential to activate the script |
|  **generateEvent** | generate event with phone number and amount for pay |
|  **amountForPay** | amount for pay |
