## sleep

Given script makes the card enter the "sleep mode" on the channel for a particular period of time without leaving the network.

![sleep](sleep.assets/sleep.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event essential for script activation |
|  event after sleep | event when **sleep time** is over |
|  sleep time | sleep time of the card |
