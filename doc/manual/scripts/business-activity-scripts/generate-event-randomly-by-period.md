## generate event randomly by period

Given script is designed for any event performance randomly by the period.

![g_e_rand_by_per](generate-event-randomly-by-period.assets/g_e_rand_by_per.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event generation period** | regularity of **send event** performance |
|  **lock on failure** | block the card on unsuccessful performance |
|  **send event** | generated event |
|  **event chance** | possibility of **send event** performance |
|  **event limit** | quantity of  **send event** generated for **event generation period** |
