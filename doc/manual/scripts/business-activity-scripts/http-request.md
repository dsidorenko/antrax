## HTTP request

Given script is designed for send HTTP request.

![http_request](http-request.assets/http-request.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | initial event essential to activate the script |
|  event on failure | this event will be generated after failed HTTP request |
|  event on success | this event will be generated after succeed HTTP request |
|  apn | access point name, via this field configuring internet connection settings |
|  HTTP server pattern | path pattern for get server(url) from registry |
