## pay

Script **pay** is used for balance refill with the help of vouchers (scratch cards for balance refill). Vouchers are to be added to Registry section in the square vouchers._operator_name_._amount_of_replenishment_.new

![pay](pay.assets/pay.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event essential for script generating |
|  amount | amount to pay |
|  payment data | data necessary to perform payment |
|  lock on fail | block in case of failure |
|  skip intermediate states | skip intermediate states (if there is a "check mark", voucher will be transferred to bad. If there is no "check mark", voucher, in case of bad response, goes to failed1, then to failed2, following failed3, and only after that it goes to bad). The script sorts out vouchers from all directions and forwards them to registry (failed3->failed2->failed1->new) |
|  minimum amount | minimum amount of money available on balance sufficient to cancel the payment. To cancell the balance check-up set up 0 |
|  paid event | the event generated on successful script performance |
|  confirm payment | sends the same number of voucher to confirm the refill |
|  balance check data | data to perform a balance check |


### Configuration of payment data

There are two ways to refill your balance with the help of the voucher. The first – with the help of ussd request, the second – with the help of call.

#### payment data set-up for refill with the help of ussd

![pay_ussd](pay.assets/pay_ussd.png)

|  operator | register key which determines the vouchers of stated operator |
| -------- | -------- |
|  ussd | balance refill request. Set up $ where you have to insert the voucher |
|  response check regex |regular expression, necessary for operator's response processing  |

For this procedure you need to add refill codes to register in the square named vouchers.current_operator.10.new

#### payment data set-up for balance refill with the help of call

This type of balance refill implies, that after successful refill you will receive a massage confirming the refill.

![pay_call](pay.assets/pay_call.png)

|  operator | register key, which defines the vouchers of stated operator |
| -------- | -------- |
|  call | phone number you need to call in order to refill the balance |
|  time | active duration of the call (measured in seconds) |
|  check SMS regex | regular expression,necessary for operator's SMS processing |

###  bill check data set-up

![billcheckdata](pay.assets/billcheckdata.jpg)

|  ussd | balance check-up request |
| -------- | -------- |
|  response check regex | regular expression, necessary to determine the amount |

 
