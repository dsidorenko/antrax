## generate event after success send SMS

Given script is designed to perform any action after definite amount of success send SMS.

![generate-event-after-success_send-sms](generate-event-after-success-send-sms.assets/generate-event-after-success-send-sms.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | generate this event after the amount of fail send SMS specified by this script |
|  sms limit | limit of send fail sms. This filed allow set a range. At start of count, script generate random value from this range |

