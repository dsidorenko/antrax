## generate event on zero calls

Given script is designed for any event performance after call with zero duration.

![g_e_on_zero_calls](generate-event-on-zero-calls.assets/g_e_on_zero_calls.png)

### Parameters

| name| description|
| -------- | -------- |
|  **maximum zero calls** | quantity of zero calls |
|  **event** | event which requires generating after **maximum zero calls** is obtained |
