## generate event on call setup

Given script is designed for any action performance on call set-up.

![g_e_on_call_setup](generate-event-on-call-setup.assets/g_e_on_call_setup.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event  ** | event, which is generated on call set-up |

