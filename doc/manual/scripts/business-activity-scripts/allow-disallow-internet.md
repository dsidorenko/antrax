## Allow/Disallow internet
 
This script allows enable or disable internet possibility in SIM card.

![AllowDisallowInternet](allow-disallow-internet/allow-disallow-internet.png)

### Parameters

| name| definition|
| -------- | -------- |
|  allowEvent | After receiving this event, the Internet status on the SIM card is changed to allow |
|  disallowEvent | After receiving this event, the Internet status on the SIM card is changed to disallow |
