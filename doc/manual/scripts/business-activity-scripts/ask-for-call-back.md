## ask for call back

The script is a part of script chain for calls between cards. The script is only called from call action in **Action Provider Script** and must not be called directly.

![ask_for_call_back](ask-for-call-back.assets/ask_for_call_back.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | event which activates given script |
|  action | name of action script, which will be called |
|  lock on fail | whether to block the card in case of failure |
|  event on success | generate the event in case of successful call |
|  event on failure | generate the event in case of failure |
|  execute time | timeout script execution, after which the script will be canceled |
 

