## TF Transfer action

This script performs money transfer from source-card to card-receiver. The script is an intermediate part of a TF group scripts and can not be used separately from the group.

![tf_transfer_action](tf-transfer-action.assets/tf_transfer_action.png)

### Parameters

| name| description|
| -------- | -------- |
|  action name | the name of the initial action which performs this script (do not change this option if you are unsure of your actions) |
|  balance check data | parameters for balance check up |
|  amount | an amount of money to transfer |
|  USSD request | combination, which initiates the transfer of stated amount of money (inserted instead of '$')\\ to specified phone number (inserted instead of '@') |
|  response patterns list | regular expression intended to analyze the operator's reply and send conformation of money transfer |
|  transfer all | whether to transfer all money with exception of specified remaining amount   |
|  remains amount | amount of money which must remain on account after transfer |
|  action failed event | event generated in case of unsuccessful performance of this script |

### Balance check-up parameters

|{{:ru_new:user_interface:scripts:action_provider_script:tf_find_dst_card_action.png?650|}}|\\ The USSD field requires specifying the combination for balance check up .\\ The next field must be filled up with a regular expression to analyze the answer from operator  .

### Parameters for operator's reply analysis
 
![tf_transfer_action](tf-transfer-action.assets/tf_balance_check.png)

| name | description|
| -------- | -------- |
|  pattern | regular expression to analyze the operator's reply to money transfer request |
|  answer | whether confirmation is required for money transfer |
|  answer | a reply to the operator for money transfer confirmation. You can use an expression received from regular pattern expression. |

### Logic of the script

If current account balance on the source-card is less than the **remains amount** parameter index, money transfer will not be performed.
Also, event specified in the parameter **action failed event** will be generated.

If the parameter **transfer all** is set up or **amount** parameter index exceeds the amount of money balanced regardless remaining amount ( **remains amount** parameter), then all money will be transferred from source-card to receiver-card without balance specified in the **remains amount** parameter.

Note that script does not include the cost of the transaction. That's why, the balance on the source-card after transaction may be less than the **remains amount** parameter index, because some money will be debited from the card for the transfer performed.


 

