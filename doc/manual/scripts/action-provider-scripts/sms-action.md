## Sms action

The script enables one card to"ask" randomly chosen card to send it an SMS.
Given script sends SMS to the card of N group where the request for askForSMS or ExecuteAction was performed.

![sms_action](sms-action.assets/sms_action.png)

### Parameters

| name| description|
| -------- | -------- |
|  path in registry | register key, which defines the text of message |
|  send SMS inside one group | if selected, given activity will be performed with the card of the same SIM group as the card, which requested this activity |
|  ignore incoming SMS | if the option is selected, the script will be performed even in case of active inbound SMS|
|  skip errors | the option defines the need of errors being skipped or not |
|  event on success | generates the event in case of successful call performance |
|  event on failure | generates the event in case of failure |
|  send SMS count | quantity of SMS |
|  interval between send SMS | time interval between sending SMS |
|  interval between attempts | time interval between attempts of sending SMS |
|  number pattern | regular expression defining the number. Used in the collective with number replace pattern field |
|  number replace pattern | regular expression, which defines part of number |
|  action name | name of the initial event |

### Configuring number pattern and number replace pattern fields

These settings are used in the case of number conversion is needed. For example, remove the country code from the number, special characters.
number pattern usually is a regular expression which consists of two parts: the part which needs to be removed and a part which needs to be used.
For example : \ +5021(.*)

Dot-asterisk in braces defines the number with any number of digits or at least one digit. Special characters need to be screened by backslash “\”.

number replace pattern usually set to  $1 - in this case, the part in braces will be chosen.
Thus  any number which begins with +5021  will be converted. For example, +50219873452 will be changed to 9873452.
