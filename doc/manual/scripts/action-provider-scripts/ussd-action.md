## USSD action

This script enables to transfer money from one card to another with the help of USSD request.
This script sends ussd to N group card which requested for askForUSSD or ExecuteAction action.

![ussd_action](ussd-action.assets/ussd_action.png)

### Parameters

| name| description|
| -------- | -------- |
|  ussd request | number of requests |
|  amount | amount to pay |
|  skip errors | the option defining whether to skip the errors or not |
|  event on success | generates the event in case of successful call performance |
|  event on failure | generates the event in case of failure |
|  interval between attempts | time interval between ussd sending attempts |
|  event | generates the event after ussd sending |
|  attempts count | quantity of ussd |
|  number pattern | regular expression defining the number. Used in the collective with number replace pattern |
|  number replace pattern | regular expression defining the part of number |
|  send ussd inside one group |if selected, given activity will be performed with the card of the same SIM group as the card, which requested this activity   |
|  response patterns list | regular expression to analyze the request and generate the ussd reply |

### Configuring number pattern and number replace pattern fields


These settings are used in the case of number conversion is needed. For example, remove the country code from the number, special characters.
number pattern usually is a regular expression which consists of two parts: the part which needs to be removed and a part which needs to be used.
For example : \ +5021(.*)

Dot-asterisk in braces defines the number with any number of digits or at least one digit. Special characters need to be screened by backslash “\”.

number replace pattern usually set to  $1 - in this case, the part in braces will be chosen.
Thus any number which begins with +5021  will be converted. For example, +50219873452 will be changed to 9873452.

