## Description of drop code

All calls with such errors end with 42 code

LIMIT_FAS_ATTEMPTS - error of exceeding the FAS limit when trying to call;

INVALID_CALLER_FILTER - error that means that the call filter is not configured properly, that is, the correct setting of the A number;

INVALID_CALLED_FILTER - error that means that the call filter is not configured correctly, that is, the dialing is wrong;

ANSWER_ERROR - error in lifting the handset on the module. The error in the log itself:

    logger.warn("[{}] - can't answer for incoming call", callChannel, e)

START_AUDIO_PROBLEM - error of opening the audio channel on the module. The error itself in the logs For the incoming:

    logger.warn("[{}] - can't start incoming call", callChannel, e)

For outgoing:

    logger.warn("[{}] - can't process terminating call [{}]", this, terminatingCall, e)

ORIGINATING_ERROR - error of the beginning of the call in the VoIP network. The error in the log itself:

    logger.info("[{}] - can't start new call for incoming call", callChannel)

TERMINATING_ERROR - dialing error on the module or end of the outgoing call due to the busy module by another call. The error in the log itself:

    logger.error("[{}] - can't process terminating call [{}], because another call is active", this, terminatingCall);
    logger.warn("[{}] - can't process terminating call [{}]", this, terminatingCall, e);


