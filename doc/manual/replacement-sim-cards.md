## Replacement of sim cards in SB60, SB120
This section provides examples of correct and incorrect replacement of sim cards in sim holders.

### Correct replacement of the sim card in the sim holder

![](replacement-sim-cards.assets/correct_replacement_of_sim_card_in_sb.webm)

### Incompatible replacement of sim card in sim holder

![](replacement-sim-cards.assets/not_correct_replacement_of_sim_card_in_sb.webm)
