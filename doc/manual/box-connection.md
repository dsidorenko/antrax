## The connection of GSM and SIM box
### GSM box
The connection algorithm of **GB4** and **GB8**.
### GB4
To connect the antennas one needs to open the panel on the side of LEDs which indicate the operation of each GSM channel.

![gb4_connect_1](box-connection.assets/gb4_connect_1.jpg)

One needs carefully connect the antenna to the power plug for antennas.

![gb4_connect_2](box-connection.assets/gb4_connect_2.jpg)

Look of **GB4** with connected antennas.

![gb4_connect_3](box-connection.assets/gb4_connect_3.jpg)

On the current **GB4** side panel the following items are situated:
  * Power indicator;
  * Power plug for the power supply unit with nominal parameters 12V 5А;
  * USB port;
  * Ehternet port for Internet connection;
  * Reset Button, it is located between Power plug and USB port.

![gb4_connect_4](box-connection.assets/gb4_connect_4.jpg)

Look of switched on **GB4** and connected to Internet.

![gb4_connect_5](box-connection.assets/gb4_connect_5.jpg)

### GB8
To connect the antennas one needs to open the panel on the side of LEDs which indicate the operation of each GSM channel.

![gb8_connect_1](box-connection.assets/gb8_connect_1.jpg)

One needs carefully connect the antenna to the power plug for antennas.

![gb8_connect_2](box-connection.assets/gb8_connect_2.jpg)

Look of **GB8** with connected antennas.

![gb8_connect_3](box-connection.assets/gb8_connect_3.jpg)

On the current **GB8** side panel the following items are situated:
  * Power indicator;
  * Power plug for the power supply unit with nominal parameters 12V 5А;
  * USB port;
  * Ehternet port fro Internet connection;
  * Reset Button, it is located between Power plug and USB port.

![gb8_connect_4](box-connection.assets/gb8_connect_4.jpg)

Look of switched **GB8** and connected to Internet.

![gb8_connect_5](box-connection.assets/gb8_connect_5.jpg)

### SIM box
Connection algorithm **SB60** and **SB120**.

### SB60
The look of the lower part of the panel **SB60**.

![sb60_connect_1](box-connection.assets/sb60_connect_1.jpg)

The look of the upper part of the panel **SB60** which has the following plugs:
  * Power indicator;
  * Power plug for the power supply unit with nominal parameters 12V 5А;
  * USB port;
  * Ehternet port for Internet connection;
  * Reset Button, it is located between Power plug and USB port.

![sb60_connect_2](box-connection.assets/sb60_connect_2.jpg)

Look of switched on **SB60** and connected to Internet.

![sb60_connect_3](box-connection.assets/sb60_connect_3.jpg)

Look of **SB60** with connected SIM cards.

![sb60_connect_4](box-connection.assets/sb60_connect_4.jpg)

### SB120
Look of lower part of panel **SB120**.

![sb120_connect_1](box-connection.assets/sb120_connect_1.jpg)

Look of upper part of the panel **SB120** which has the following plugs:
  * Power indicator;
  * Power plug for the power supply unit with nominal parameters 12V 5А;
  * USB port;
  * Ehternet port for Internet connection;
  * Reset Button, it is located between Power plug and USB port.

![sb120_connect_2](box-connection.assets/sb120_connect_2.jpg)

Look of switched on **SB120** and connected to Internet.

![sb120_connect_3](box-connection.assets/sb120_connect_3.jpg)

Look of **SB120** with connected SIM cards.

![sb120_connect_4](box-connection.assets/sb120_connect_4.jpg)

### Connection set-up on GSM/SIM box
Connection set-up with GSM/SIM box is possible due to:
Client's DHCP switched into GSM/SIM box;
Using manual set-up of network interface in GSM/SIM box.

### GSM/SIM box set-up using DHCP
To set-up GSM/SIM box using DHCP one requires: GSM/SIM box, router with switched on DHCP server and access to its web interface, remote access (RDP or teamviewer session) to PC which is connected to router.
The sequence of connection performance:
  - Connect GSM/SIM box to router with the help of Ethernet connection;
  - Connect the PC to the specified router on condition of Internet connection available on PC;
  - Launch teamviewer session or RDP and provide the access to Antrax Tech Support Center in ticket.

### GSM/SIM box set-up in manual mode
In order to set-up GSM/SIM box manually one requires: GSM/SIM box and PC with Ethernet port.
Sequence of connection performance:
  - You need to assign the IP address 172.16.0.1 on the network board of PC.
  - Connect GSM/SIM box to PC with the help of  Ethernet connection.
  - Connect to GSM/SIM box either via ssh connection with the help of interpreter of Linux command line, or via Putty program in Windows. Box is available on address 172.16.0.2.
Access data for ssh connection: **Login: “root”; Password: “antrax”**(the data is to be inserted without special characters “”).
  - After successful root authentication on GSM/SIM box on ssh connection with the help of command line interpreter one needs to perform the following command:

```
nano /etc/network/interfaces
```

  - For __statistical IP set-up__ at Ethernet connection one needs to change eth0 interface set-ups according to the sample:

```
auto eth0 
iface eth0 inet static 
address xxx.xxx.xxx.xxx 
netmask 255.0.0.0 
gateway xxx.xxx.xxx.xxx
```

Network interface set-ups explanatory note:

```
address - statistical IP address which will be assigned to GSM/SIM box;
netmask - mask of the network which is to be specified to indicate the range of IP addresses, that will be locally available;
gateway – IP address of the main gateway, which is to be specified for Internet connection. (This information is to be rendered by Internet provider)
```

  - It is required to save the changes and leave nano editor.
After editing one needs to push the button: Ctrl + x (insert the data without + character), afterwords you will see the message:
“Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?”
Then one needs to push the “y” symbol on the keyboard (data is to be inserted without special characters “”), afterwords you will see a message:
“File Name to Write:

```
/etc/network/interfaces
```

Then one needs to push the button: “Enter” (insert the data without special characters “”) in order to confirm the saving of network interface set-ups.
  - In order to apply network interface set-ups one needs to perform the following command:

```
/etc/init.d/networking restart
```

### Restores the BOX to its original factory default settings
To reset configuration of the BOX, locate the reset button (hole) on the rear panel of the unit. With the BOX powered on, use a paperclip to hold the
button down for 5 seconds. Release the button and the BOX will restore the configuration to default. Wait about 30 seconds to access
the BOX. After that can connect to BOX as described at [Connection set-up on GSM/SIM box](#connection-set-up-on-gsmsim-box).

Settings that will be restored to default:
* iptables
* password for root user
* ssh configs
* /etc/network/interfaces
 
