## SIM SERVERS

Any activity with SIM cards is carried out in SIM SERVERS section

![ss](sim-server.assets/ss.png)

### Button description

| Button | Action|
| -------- | -------- |
| ![import](sim-server.assets/import.png) | Importing of setting for SIM cards |
| ![export_excel](sim-server.assets/export_excel.png) | export to excel |
| ![enable_call](sim-server.assets/enable_call.png) | utilize the SIM card |
| ![disable_call](sim-server.assets/disable_call.png) | switch off the SIM card together with channel|
| ![lock_sim](sim-server.assets/lock_sim.png) | block the SIM card |
| ![unlock_sim](sim-server.assets/unlock_sim.png) | unblock the SIM card |
| ![reset_scritps](sim-server.assets/reset_scritps.png) | reset the script statistics |
| ![reset_imei](sim-server.assets/reset_imei.png) | reassignment of IMEI attached to SIM card |
| ![note_ss](sim-server.assets/note_ss.png) | custom note |
| ![allow_internet](sim-server.assets/allow_internet.png) | invert possibility to allow internet |
| ![set_tariff](sim-server.assets/set_tar.png) | set the end date of the tariff plan |
| ![reset_statistic](sim-server.assets/reset_statistic.png) | reset SIM card statistic for the selected cards |

| Menu/Filter | Action|
| -------- | -------- |
| ![show_empty_holders](sim-server.assets/show_empty_holders.png) | If this check box is set, the only information about SIM holders unoccupied by SIM cards will be place to the table|
| ![show_unvailable_holders](sim-server.assets/show_unvailable_holders.png) | If this check box is set, the only information about unavailable SIM holders will be placed to the table |
| ![pin_code](sim-server.assets/pin_code.png) | Select the card in the table, insert the PIN code in this box, apply the changes|
| ![pin_code](sim-server.assets/phone_number.png) | Select the card in the table, insert the telephone number in this box and apply the changes|
| ![imei](sim-server.assets/imei.png) | Manual IMEI assignment for selected SIM card |
| ![sim_group](sim-server.assets/sim_group.png) | To set the SIM group it is required to select the range of SIM cards, select the required group from pop-up list, apply the changes|
| ![filter_search](sim-server.assets/filter_search.jpg) | information insertion filter in working area according to specified parameters |

### Filters

| Filter | Description|
| -------- | -------- |
| requires pin | display the SIM cards without stated PIN only |
| no phone number | display the SIM cards without telephone numbers only |
| no group | only the SIM cards without stated group |
| locked | only blocked SIM cards will remain available on the screen |
| unlocked | display all SIM cards except the blocked ones |
| empty | filters empty sim  holders |
| unavailable | display unavailable SIM holders only |

### Description of parameter values in columns

<table>
	<tr><th>Heading</th><th>Description</th></tr>
	<tr><td>Gateway</td><td>Voice server, SIM card is registered on</td></tr>
	<tr><td>Prev Gateway</td><td>Voice server, SIM card was registered on before</td></tr>
	<tr><td>SIM holder</td><td>information about SIM module and holder, where SIM card is located</td></tr>
	<tr><td>SIM holder</td><td>GSM channel, where SIM card is registered</td></tr>
	<tr><td rowspan="3">E</td></tr>
	<tr><td><img src="sim-server.assets/simstate_enable.png" alt="simstate_enable"> - SIM card is available on channel and active</td></tr>
	<tr><td><img src="sim-server.assets/simstate_disable.png" alt="simstate_disable"> - channel is blocked together with SIM card</td></tr>
	<tr><td rowspan="3">L</td></tr>
	<tr><td><img src="sim-server.assets/simlockstate_unlocked.gif" alt="simlockstate_unlocked"> - SIM card is not blocked</td></tr>
	<tr><td><img src="sim-server.assets/simlockstate_l.gif" alt="simlockstate_l"> - SIM card is blocked</td></tr>
	<tr><td>Lock timeout</td><td>time period while SIM card is blocked or active</td></tr>
	<tr><td>(Un)Lock reason</td><td>the reason of (un)blocking</td></tr>
	<tr><td>SIM group</td><td>SIM group where SIM card is located</td></tr>
	<tr><td>Phone number</td><td>phone number of SIM card</td></tr>
	<tr><td>Status</td><td>status of SIM card</td></tr>
	<tr><td>Status timeout</td><td>time period of SIM card being in status mentioned above</td></tr>
	<tr><td>ASR</td><td>the ratio of answered calls to the total number of attempts</td></tr>
	<tr><td>ACD</td><td>average call duration</td></tr>
	<tr><td>Successful</td><td>quantity of successful calls</td></tr>
	<tr><td>Total</td><td>overall quantity of calls</td></tr>
	<tr><td>Calls dur.</td><td>overall duration of calls</td></tr>
	<tr><td>Inc. Success</td><td>quantity of successful incoming calls</td></tr>
	<tr><td>Inc. Total</td><td>overall quantity if incoming calls</td></tr>
	<tr><td>Inc. Calls dur.</td><td>overall duration of incoming calls</td></tr>
	<tr><td>Last call dur.</td><td>duration of the last call</td></tr>
	<tr><td>Successful SMS</td><td>count of successful SMS</td></tr>
	<tr><td>Total SMS</td><td>overall quantity of SMS</td></tr>
	<tr><td>SMS ASR</td><td>the ratio of successful SMS to the total number of sending attempts</td></tr>
	<tr><td>Inc. Total SMS</td><td>overall quantity of incoming SMS messages</td></tr>
	<tr><td>Total SMS statuses</td><td>general statuses of SMS messages</td></tr>
	<tr><td>ICCID</td><td>unique serial number of the SIM-card</td></tr>
	<tr><td>IMEI</td><td>IMEI assigned for SIM-card with GSM channel</td></tr>
	<tr><td>Last reconf. time</td><td>time when the last changes occurred to card</td></tr>
	<tr><td>Note</td><td>custom note</td></tr>
	<tr><td>User message</td><td>last message from the operator</td></tr>
	<tr><td rowspan="3">Status of sim boards connection</td></tr>
	<tr><td><img src="sim-server.assets/live.png" alt="live"> - sim holder/board is connected</td></tr>
	<tr><td><img src="sim-server.assets/unlive.png" alt="unlive"> - sim holder/board is disconnected</td></tr>
	<tr><td rowspan="6">License status and network status</td></tr>
	<tr><td><img src="sim-server.assets/license_correct.png" alt="license_correct"> - license is relevant</td></tr>
	<tr><td><img src="sim-server.assets/license_invalid.png" alt="license_invalid"> - license is not valid</td></tr>
	<tr><td><img src="sim-server.assets/license_expire.png" alt="license_expire"> - license is overdue</td></tr>
	<tr><td><img src="sim-server.assets/license_expire.png" alt="license_expire"> - IP address establishment error</td></tr>
	<tr><td><img src="sim-server.assets/ip_config_parameters_invalid.png" alt="ip_config_parameters_invalid"> - wrong IP address settings</td></tr>	
</table>
