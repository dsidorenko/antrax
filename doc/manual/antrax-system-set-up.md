## Antrax system set-up

Installation and setting of Antrtax system software components on Centos 6.8 x64 and on Debian 8.X x64

### Perform a basic OS setup

#### Check OS Centos

    arch
    
The output of the command should be:

    x86_64

Otherwise, you need to reinstall the OS using the x64 distribution.

#### Check the version of Centos installed

    cat /etc/*release*

If the release version is different from version 6, then you need to tell the server owner about the need to reinstall the OS.

If the release version is 6.9 and higher, then it is required:

#### Open /etc/yum.repos.d/CentOS-Vault.repo and make changes to:

    [C6.8-base]
    name=CentOS-6.8 - Base
    baseurl=http://vault.centos.org/6.8/os/$basearch/
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
    enabled=1
    [C6.8-updates]
    name=CentOS-6.8 - Updates
    baseurl=http://vault.centos.org/6.8/updates/$basearch/
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
    enabled=1
    [C6.8-extras]
    name=CentOS-6.8 - Extras
    baseurl=http://vault.centos.org/6.8/extras/$basearch/
    gpgcheck=1
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6
    enabled=1

#### Need to make downgrade

    yum downgrade redhat-release

#### Installing the old version of the kernel

    yum install kernel-2.6.32-642\*

After changes, make reboot PC/VPS/VDS

#### Switch off Selinux

Need to make changes in the file of configuration:

    mcedit /etc/selinux/config

Need to make this changes:

    SELINUX=disabled

#### Installing additional software

    yum install -y setuptool system-config-securitylevel-tui authconfig system-config-network-tui ntsysv nc mc gcc make zlib-devel wget ntpdate unzip autoconf

### Synchronize time with the _ntpdate_ utility

    ntpdate ntp.time.in.ua

#### Change hostname

    hostname name_of_host

For example, hostname Yate-CS-NEW

It is highly advisable not to use underscore characters ("_") in the host name. Some operating systems do not understand it, which can lead to errors of operation when accessing the host.

 Make changes in the file /etc/hosts

    mcedit /etc/hosts

Need to make this changes:

    127.0.0.1 localhost.localdomain localhost name_of_host

For example, 127.0.0.1 localhost.localdomain Yate-CS-NEW

Make changes in the file /etc/sysconfig/network

    mcedit /etc/sysconfig/network

Need to make this changes:

    NETWORKING=yes
    HOSTNAME=name_of_host

Example file after editing:

    NETWORKING=yes
    HOSTNAME=Yate-CS-NEW

Conclusion, if the hostname is correctly configured, the following command output results will be displayed:

    hostname - displays your host name  
    hostname -i - display the ip address to which you assigned this hostname

If the hostname is not assigned correctly, then the command: hostname -i produces the following result:

    hostname -i
    hostname: Name or service not know

#### Disabling IPv6 and IPv4 Configuration

Make changes in the file /etc/sysctl.conf

    mcedit /etc/sysctl.conf

Need to make this changes:

    net.ipv4.ip_forvard = 1
    net.ipv6.conf.all.disable_ipv6 = 1
    net.ipv6.conf.default.disable_ipv6 = 1

Apply settings:

    sysctl -p

#### Need to add an interface for the boards (only for 2U racks or a PC with 3U racks)

Need to add an interface for the boards (only for 2U racks or a PC with 3U racks)
Need to edit the interface for the boards in the file with the name ifcfg-iface relative to the path / etc / sysconfig / network-scripts /, where iface is the name of the interface

    DEVICE=eth0
    HWADDR=08:00:27:6c:e1:fc
    TYPE=Ethernet
    ONBOOT=yes
    NM_CONTROLLED=no
    BOOTPROTO=none
    IPADDR=192.168.1.99
    NETMASK=255.255.255.0
    GATEWAY=192.168.1.1
    IPV6INIT=no

Configuring an alias connection for an ethernet interface that connects to the Internet if the second interface on the server does not work / there is no. For a 3U river or ethernet connection for a cross-board in a 2U river. We add the interface for the boards to a file called ifcfg-iface: 0 relative to the path / etc / sysconfig / network-scripts /, where iface is the name of the relative interface that will be produced by the settings alias.

    DEVICE=eth0:0
    TYPE=Ethernet
    ONBOOT=yes
    NM_CONTROLLED=no
    BOOTPROTO=static
    IPADDR=192.168.111.1
    NETMASK=255.255.255.0
    IPV6INIT=no

#### Increase the number of descriptors for Antrax user
Adding a library that sets limits on system resources that can be obtained in a user session

    echo "session required pam_limits.so" >> /etc/pam.d/login
    echo "antrax soft nofile 4096" >> /etc/security/limits.conf
    echo "antrax hard nofile 16384" >> /etc/security/limits.conf
    echo 'if [ $USER = antrax ]; then' >> /etc/profile
    echo "ulimit -n 16384" >> /etc/profile
    echo "ulimit -c unlimited" >> /etc/profile
    echo "fi" >> /etc/profile

#### Checking version of JAVA

    java -version

Correct version:

    java version "1.8.0_XX"
    Java(TM) SE Runtime Environment (build 1.8.0_XXX-bXX)
    Java HotSpot(TM) 64-Bit Server VM (build 25.XXX-bXX, mixed mode)

where XXX-bXX is an exposure and can have different values.

If Java is installed in a different format (OpenJDK), then you should remove Java (OpenJDK) and install the correct version (OracleJDK):

Installing JDK 8.XX, where XX is an exposure and can have different values

#### Installing JAVA

    mkdir -p /opt/install
    chmod 777 /opt/install
    cd /opt/install
    wget --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.rpm
    export JAVA_HOME="/usr/java/latest"

#### Installing jsvc

    cd /opt/install
    wget http://archive.apache.org/dist/commons/daemon/source/commons-daemon-1.0.15-src.tar.gz
    tar -zxf commons-daemon-1.0.15-src.tar.gz
    cd ./commons-daemon-1.0.15-src/src/native/unix
    ./configure && make
    cp ./jsvc /usr/bin/
    echo "export JAVA_HOME="/usr/java/latest"" >> /etc/profile

### Install the DB and perform the basic configuration

#### Adding a repository to install and installing PostgreSQL 9.5

    rpm -Uvh http://yum.postgresql.org/9.5/redhat/rhel-6-x86_64/pgdg-redhat95-9.5-2.noarch.rpm
    yum install rpm-devel -y
    yum install postgresql95-server postgresql95 -y

#### Configuration disposition of Database

    mkdir -p /opt/postgresql/data
    chown -R postgres:postgres /opt/postgresql
    echo "PGDATA=/opt/postgresql/data
    PGLOG=/opt/postgresql/pgstartup.log" > /etc/sysconfig/pgsql/postgresql-9.5

#### Initialization of Database 

    /etc/init.d/postgresql-9.5 initdb

#### Configuration of Database

    sed -i -E 's/host(.+)127.0.0.1\/32(.+)ident/host all all 127.0.0.1\/32 md5/g' /opt/postgresql/data/pg_hba.conf
    echo "host all all  0.0.0.0/0  md5" >> /opt/postgresql/data/pg_hba.conf
    echo "listen_addresses = '*'" >> /opt/postgresql/data/postgresql.conf
    sed -i -e "s/#bytea_output = 'hex'/bytea_output = 'escape'/" /opt/postgresql/data/postgresql.conf
    sed -i -e "s/^#wal_level =.*$/wal_level = logical/" /opt/postgresql/data/postgresql.conf

#### Configuring Autostart Database

    chkconfig postgresql-9.5 on
    /etc/init.d/postgresql-9.5 start

#### Creating user Antrax on the Database

    sudo -u postgres createuser -d -R -s -P antrax

It is necessary to specify the password of the antrax user, and also to duplicate it upon repeated request:

    antrax

#### Creating an empty Antrax Database

    sudo -u postgres createdb -T template1 -E UTF8 -O antrax antrax

### Setting YATE and perform the basic setting

#### Adding Repository of Yate 

##### Create / edit a repository to install Yate

    mcedit /etc/yum.repos.d/antrax.repo

##### Add the setting of repository

    [yate]
    name=Yate Yum Repo
    baseurl=https://antrax-repo:antraxSupport@rpm.antrax.mobi/6/yate/
    enabled=0
    # not secure
    gpgcheck=0
    sslverify=1

##### Yate installation

    yum install -y yate\* --enablerepo=yate

##### After installation, you should add Yate to the autostart

    chkconfig --add yate

### Installation

#### Configuring SIP. Make changes to the file /opt/yate/etc/yate/ysipchan.conf

    port=5060

If Yate and Voice Server are installed on the same server - in the file /opt/yate/etc/yate/yiaxchan.conf specify the port for IAX - 4579. If on different servers - do nothing.

    ; port: int: UDP port for incoming connections
    port=4579

#### Increase logging for Yate

    mcedit /etc/init.d/yate

Change from "vvv" to "vvvvvv"

    OPTS="-F -s -r -l /opt/yate/var/log/yate -vvvvvv -Df"

#### Run Yate

    /etc/init.d/yate start

### Configuring NAT for YATE

If YATE is behind the router, there may be problems with the passage of RTP and SIP traffic. YATE can be configured to bypass NAT. For this it is necessary.

#### Configuring the Router

For correct operation, the following ports must be flown:

    5060 - SIP Port
    4569 - port IAX2
    16400-17000 - RTP ports. For most systems, the allocation of 600 ports should be enough. If necessary, the quantity can be expanded.

#### Configuring SIP

For SIP to work, one port must be available. By default, this is port 5060. In addition, you must perform a substitution of all private addresses in SIP messages to a public address.

_ysipchan.conf:_


    ; port: integer: Port to bind to
    ; Defaults to 5060 for UDP and TCP, 5061 for TLS listener
    port=5060
    ; nat: bool: Enable automatic NAT support
    nat=enable
    ; nat_address: ipaddress: IP address to advertise in SDP, empty to use the
    local RTP
    ; This parameter is applied on reload
    ; Set this parameter when you know your RTP is behind a NAT
    nat_address=<PUBLIC_SERVER_IP>
    ; ignorevia: bool: Ignore Via headers and send answer back to the source
    ; This violates RFC 3261 but is required to support NAT over UDP transport.
    ignorevia=enable


_regexroute.conf:_

    [extra]
    call.answered=3
    [call.answered]
    ${address}^\(192\.168\)\|\(10\.\)\|\(172\.16\.\)=;osip_Contact=<sip:NAT@<PUBLIC_SERVER_IP>:5060>

#### Configuring RTP

_yrtp.conf:_

    ; minport: int: Minimum port range to allocate
    minport=16400
    ; maxport: int: Maximum port range to allocate
    maxport=17000


### Installing Antrax

##### Add the setting of repository for Antrax

Create / edit the repository:

    mcedit /etc/yum.repos.d/antrax.repo

Add the repository settings:

    [antrax]
    name=Antrax Yum Repo
    baseurl=https://antrax-repo:antraxSupport@rpm.antrax.mobi/6/antrax/release/1.17.3
    enabled=0
    # not secure
    gpgcheck=0
    sslverify=

### List of possible packages for installation

    antrax.commons.package - a package of key server configurations
    antrax.tools.package - toolset
    antrax.control-server.package - management server package
    antrax.gui-client-webstart.package - GUI package
    antrax.sim-server.package - SS package for working with sim cards
    antrax.voice-server.package - VS package for working with gsm boards

### Installing packages

    yum install -y --enablerepo=antrax antrax.package_name

### Configuring Antrax

#### Common configurations for all servers

After installation go to / opt / antrax / commons / etc / and in the file server.name need to register the name of server

### CS Configuration

In / opt / antrax / commons / etc / in the file control.server.url we will register the IP address of the server. When using OpenVpn, you need to specify the server address in the OpenVpn network (For example 10.2.0.1), if you do not use OpenVpn then you need to specify an external IP address that looks to the Internet.

    rmi://IP_address:1222

Open the file /opt/antrax/control-server/etc /control-server.properties and made changes

    rmi.server.hostname = IP_address(server)

This setting should specify the IP address of the server on which the Control server will be launched. When using OpenVpn, you need to specify the server address in the OpenVpn network (For example 10.2.0.1), if you do not use OpenVpn then you need to specify an external IP address that looks to the Internet.
Next, you need to configure a connection to the DB.

    antrax.db.config.url=jdbc:postgresql://IP_address:5432/antrax

Instead of IP_address, you need to specify an IP DB. If the DB is on the same server with CS, then you need to specify 127.0.0.1, if at different then the external IP DB.

Next, we'll connect to YATE

    external.module.yate.address=IP_address(Yate)

If YATE is installed on this server, then you need to specify a local IP address, for example 127.0.0.1, if on another server you need to specify the IP address of the remote server.

### VS / SS Configuration

In / opt / antrax / commons / etc / in the file control.server.url we assign the IP address of the CS server. When using OpenVpn, you need to specify the server address in the OpenVpn network (For example 10.2.0.1), if you do not use OpenVpn then you need to specify an external IP address that looks to the Internet.

    rmi://IP_address:1222

#### For 2U and 3U racks

The files /opt/antrax/voice-server/etc/network.properties and /opt/antrax/sim-server/etc/network.properties prescribe the IP of the configurable server, the IP master for the boards and the broadcast for the boards.

    antrax.device.manager.netmask=255.255.255.0
    antrax.device.manager.gateway=server_ip
    antrax.device.manager.masterIP=boards_master_ip
    antrax.device.manager.broadcast=boards_broadcast

In the files /opt/antrax/voice-server/etc/devices.properties (GSM boards) and /opt/antrax/sim-server/etc/devices.properties (SIM cards), we assign ip for the boards following the example:

    SIMB-1019=192.168.111.2
    GSMB3-111=192.168.111.3

IP addresses must belong to the same network with the server. The boards must be on a local network that is only connected to this server.

    GSMB3-111=192.168.111.3
    SIMB-1019=192.168.111.2

In the /opt/antrax/voice-server/etc/jiax2.properties file, you assign IP addresses to listen to iax (192.168.100.200:4569) and to enable the trunk (192.168.100.200)

    iax.peer.listener.address=0.0.0.0:4569
    trunk.addresses=192.168.100.200

If there is a need to enable GSM network monitoring, anti IMSI Catcher - change the value of the parameters network.survey.enabled, cell.equalizer.enabled, imsi.catcher to /opt/antrax/voice-server/etc/voice-server.properties. enabled in true

    # enabling GSM network monitoring
    network.survey.enabled = true
    network.survey.duration = 2h
    network.survey.period = 10m
     
    # enable cell equalizer to select the cells on which you want to register the map
    cell.equalizer.enabled = true
     
    # inclusion of the imsi catcher definition
    imsi.catcher.enabled = true
    # consider neighbor BSs
    imsi.catcher.check.neighbours.enabled = false

Add additional IPTABLES rules, if you have settings of IPtables.

    # Allow connection to boards via udp, where ethX must be interface where SIM/GSM boards are located
    # This rule must be applied only at SS and VS
    /sbin/iptables -I INPUT -d 255.255.255.255/32 -i ethX -p udp -j ACCEPT
    # range if IPs, needs for old equipment
    /sbin/iptables -A INPUT -s 192.168.0.0/16 -j ACCEPT

### Starting Servers

To run all the system components that are on this server, you can use this command:

    /etc/init.d/antrax.all start

To run individual components, you need to run one of the commands:

    /etc/init.d/antrax.gui-client-webstartd start
    /etc/init.d/antrax.control-serverd start start
    /etc/init.d/antrax.sim-serverd start
    /etc/init.d/antrax.voice-serverd start

Each component has such commands:

    start - start the component
    stop - stop the component
    restart - restarting the component
    status - displays the status of the component, the duration of the work

### Autostart components

    chkconfig --add antrax.all
    chkconfig antrax.all on