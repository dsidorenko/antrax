**Antrax** system is software-based hardware which allows making calls from IP network (internet) into GSM network (mobile operators) with a possibility of placement of GSM modules and SIM cards away from each other.

The basic components of the system are GSM and SIM modules. Each GSM module services two GSM channels whereas each SIM module – 20 SIM cards.
**GSM** is a logical unit which is equivalent to GSM terminal (mobile phone) and can exchange controlling commands (by AT protocol) voice data (in PCM16 format) and various queries/responses to/from the SIM card.

**SIM card** is a subscriber identification module used in mobile connection.

Modern GSM networks use SIM cards to identify their users, installed into the users’ GSM terminals (mobile phones). User’s terminal interacts with a SIM card and can also store and find various users’ data (like settings, contacts, SMS etc.) Mobile operator also can interact with the SIM card through the user’s terminal.

You will also find term **SIM channel** in this manual which stands for a logical unit equivalent to SIM card and can be used to exchange queries/responses to/from the SIM card.

To register in the GSM network and be able to make calls one needs to connect GSM channel to a SIM card (SIM channel) which,to put it in a simple way, stands for inserting of a SIM card into the mobile phone. Such unification is called a **voice channel**.

One of the key features of ANTRAX system is a possibility of remote interaction between the GSM terminal and a SIM card. This means that SIM card may be located 1000km away from the GSM terminal and still can successfully register within the GSM network. Thanks to such remote interaction capability a flexible control of each SIM card is possible and at the same time SIM card can move from one server to another.

Voice server accepts calls from the internet (VoIP) and addresses them into GSM network through GSM modules. SIM server manages SIM cards. To manage and monitor the system a remote GUI client is used which in turn is managed by UI server within ANTRAX system. The system can handle multiple GUI clients' connections simultaneously.

We will dwell more upon system components purposes in the chapter [System Architecture](architecture.md)