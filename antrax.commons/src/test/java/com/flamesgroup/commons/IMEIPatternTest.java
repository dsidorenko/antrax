/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import org.junit.Test;

public class IMEIPatternTest {

  @Test
  public void validAllRandomPattern() {
    IMEIPattern imeiPattern = new IMEIPattern("35XXXXXXXXXXXX");
  }

  @Test
  public void validRandomWithPhonePatternOnce() {
    IMEIPattern imeiPattern = new IMEIPattern("35XX(N4+5)XXXXXXXXX");
  }

  @Test
  public void positionOfPhoneNumberMoreNine() {
    IMEIPattern imeiPattern = new IMEIPattern("35XXXXXXXXXX(N10-5)X");
  }

  @Test
  public void validRandomWithPhonePatternRepeatedly() {
    IMEIPattern imeiPattern = new IMEIPattern("35XXXX(N4-5)XX(N1-3)X(N4-4)XX");
  }

  @Test(expected = IllegalArgumentException.class)
  public void badBracesAtPhonePattern() {
    IMEIPattern imeiPattern = new IMEIPattern("35XXXXXXXXX(N4-4XX");
  }

  @Test(expected = IllegalArgumentException.class)
  public void badPhonePattern() {
    IMEIPattern imeiPattern = new IMEIPattern("35XXXXXXXXX(N4-)XX");
  }

}

