/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

public class ArrayProxyTest {

  @Test
  public void testOne() throws Exception {
    List<String> invocationHolder = new LinkedList<>();
    FirstClass firstClass = new FirstClass(invocationHolder);
    DummyInterface proxy = ArrayProxy.proxy(DummyInterface.class, firstClass);
    proxy.method1();
    assertEquals(1, proxy.method2());
    assertEquals(FirstClass.class, proxy.method3().getClass());
    assertEquals("First class:method1", invocationHolder.remove(0));
    assertEquals("First class:method2", invocationHolder.remove(0));
    assertEquals("First class:method3", invocationHolder.remove(0));
  }

  @Test
  public void testSeveral() throws Exception {
    List<String> invocationHolder = new LinkedList<>();
    FirstClass firstClass = new FirstClass(invocationHolder);
    SecondClass secondClass = new SecondClass(invocationHolder);
    DummyInterface proxy = ArrayProxy.proxy(DummyInterface.class, firstClass, secondClass);
    proxy.method1();
    assertEquals(2, proxy.method2());
    assertEquals(SecondClass.class, proxy.method3().getClass());
    assertEquals("First class:method1", invocationHolder.remove(0));
    assertEquals("Second class:method1", invocationHolder.remove(0));
    assertEquals("First class:method2", invocationHolder.remove(0));
    assertEquals("Second class:method2", invocationHolder.remove(0));
    assertEquals("First class:method3", invocationHolder.remove(0));
    assertEquals("Second class:method3", invocationHolder.remove(0));
  }

  public interface DummyInterface {

    void method1();

    int method2();

    Object method3();

  }

  private class FirstClass implements DummyInterface {

    private final List<String> invocationHolder;

    public FirstClass(final List<String> invocationHolder) {
      this.invocationHolder = invocationHolder;
    }

    @Override
    public void method1() {
      invocationHolder.add("First class:method1");
    }

    @Override
    public int method2() {
      invocationHolder.add("First class:method2");
      return 1;
    }

    @Override
    public Object method3() {
      invocationHolder.add("First class:method3");
      return new FirstClass(invocationHolder);
    }

  }

  private class SecondClass implements DummyInterface {

    private final List<String> invocationHolder;

    public SecondClass(final List<String> invocationHolder) {
      this.invocationHolder = invocationHolder;
    }

    @Override
    public void method1() {
      invocationHolder.add("Second class:method1");
    }

    @Override
    public int method2() {
      invocationHolder.add("Second class:method2");
      return 2;
    }

    @Override
    public Object method3() {
      invocationHolder.add("Second class:method3");
      return new SecondClass(invocationHolder);
    }

  }

}
