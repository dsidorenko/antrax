/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TransientValueTest {

  @Test
  public void testValueRefreshes() throws Exception {
    TransientValue<Integer> value = new TransientValue<Integer>(0) {
      private int i = 0;

      @Override
      protected Integer readValue() {
        return i++;
      }
    };

    assertEquals(0, value.getValue().intValue());
    assertEquals(1, value.getValue().intValue());
    assertEquals(2, value.getValue().intValue());
    assertEquals(3, value.getValue().intValue());
    assertEquals(4, value.getValue().intValue());

  }

  @Test
  public void testValueFreezesForLifetime() {
    TransientValue<Integer> value = new TransientValue<Integer>(1000) {
      private int i = 0;

      @Override
      protected Integer readValue() {
        return i++;
      }
    };

    assertEquals(0, value.getValue().intValue());
    assertEquals(0, value.getValue().intValue());
    assertEquals(0, value.getValue().intValue());
    assertEquals(0, value.getValue().intValue());
    assertEquals(0, value.getValue().intValue());
  }

  @Test
  public void testValueBeforeAndAfterRefresh() throws InterruptedException {
    TransientValue<String> value = new TransientValue<String>(50) {
      String[] values = {"before_refresh", "after_refresh"};
      int index = 0;

      @Override
      protected String readValue() {
        return values[index++];
      }
    };

    assertEquals("before_refresh", value.getValue());
    assertEquals("before_refresh", value.getValue());
    Thread.sleep(60);
    assertEquals("after_refresh", value.getValue());
  }

}
