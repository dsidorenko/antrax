/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.timemachine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class TimeMachineTest {

  @Test
  public void testIsFinished() {
    TimeMachine tm = new TimeMachine(null, null);
    assertTrue(tm.isFinished());

    tm = new TimeMachine(null, new ConditionalStateImpl());
    assertFalse(tm.isFinished());
  }

  @Test
  public void testChangeState() {
    //Two things:
    // state will change
    // time will be updated for current time or for period of prevState
    ConditionalStateImpl finalState = new ConditionalStateImpl();
    ConditionalStateImpl middleState = new ConditionalStateImpl(finalState);
    ConditionalStateImpl initialState = new ConditionalStateImpl(middleState);
    TimeMachine tm = new TimeMachine(null, initialState);
    tm.changeState();
    assertSame(initialState, tm.currentState);
    tm.changeState();
    assertSame(middleState, tm.currentState);
    tm.changeState();
    assertSame(finalState, tm.currentState);
    tm.changeState();
    assertNull(tm.currentState);
  }

  @Test
  public void testChangeStateWillCallToEnterState(@Mocked final State mock) {
    new Expectations() {{
      mock.enterState();
    }};

    new TimeMachine(null, mock).changeState();
  }

  @Test
  public void testChangeStateWillNotUpdateTime() {
    TimeMachine tm = new TimeMachine(null, new ConditionalStateImpl());
    tm.lastTickTime = 0;
    tm.changeState();
    assertTrue(System.currentTimeMillis() - tm.lastTickTime < 5);
  }

  @Test
  public void testChangeStateOnTransientStateWillMoveTime() {
    TimeMachine tm = new TimeMachine(null, new TransientStateImpl(600));
    tm.changeState();
    tm.lastTickTime = 60;
    tm.changeState();
    assertEquals(660, tm.lastTickTime);
  }

  @Test
  public void testIsStateFinishedOnConditionalState() {
    TimeMachine tm = new TimeMachine(null, new ConditionalStateImpl() {
      @Override
      public boolean isFinished() {
        return false;
      }
    });
    tm.changeState();
    assertFalse(tm.isStateFinished());

    tm = new TimeMachine(null, new ConditionalStateImpl() {
      @Override
      public boolean isFinished() {
        return true;
      }
    });
    tm.changeState();
    assertTrue(tm.isStateFinished());
  }

  @Test
  public void testIsFinishedOnTransientState() {
    TimeMachine tm = new TimeMachine(null, new TransientStateImpl(100));
    tm.changeState();
    assertFalse(tm.isStateFinished());
    tm.lastTickTime -= 100;
    assertTrue(tm.isStateFinished());
  }

  @Test
  public void testTickOnUnfinishedConditionalState(@Mocked final ConditionalState mock) {
    new Expectations() {{
      mock.enterState();
      mock.isFinished();
      result = false;
      mock.tickRoutine();
    }};

    TimeMachine tm = new TimeMachine(null, mock);
    tm.tick();
    assertSame(mock, tm.currentState);
    assertTrue(System.currentTimeMillis() - tm.lastTickTime < 5);
  }

  @Test
  public void testTickOnFinishedConditionalState(@Mocked final ConditionalState mock) {
    new Expectations() {{
      mock.enterState();
      mock.isFinished();
      result = true;
      mock.getNextState();
      result = null;
    }};

    TimeMachine tm = new TimeMachine(null, mock);
    tm.tick();
    assertTrue(tm.isFinished());
  }

  @Test
  public void testTickOnSeveralFinishedConditionalState(@Mocked final ConditionalState initialState,
      @Mocked final ConditionalState secondState,
      @Mocked final ConditionalState finalState) {

    new Expectations() {{
      initialState.enterState();
      initialState.isFinished();
      result = true;
      initialState.getNextState();
      result = secondState;
      secondState.enterState();
      secondState.isFinished();
      result = true;
      secondState.getNextState();
      result = finalState;
      finalState.enterState();
      finalState.isFinished();
      result = true;
      finalState.getNextState();
      result = null;
    }};

    TimeMachine tm = new TimeMachine(null, initialState);
    tm.tick();
    assertTrue(tm.isFinished());
  }

  @Test
  public void testTickOnTransientState() {
    TransientState state = new TransientStateImpl(100);
    TimeMachine tm = new TimeMachine(null, state);
    tm.tick();
    assertSame(state, tm.currentState);
    tm.lastTickTime -= 20;
    tm.tick();
    assertSame(state, tm.currentState);
    tm.lastTickTime -= 80;
    tm.tick();
    assertTrue(tm.isFinished());
  }

}
