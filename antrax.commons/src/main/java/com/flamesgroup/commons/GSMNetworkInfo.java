/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.unit.RegistrationStatus.NetworkRegistrationStatus;

import java.io.Serializable;

public class GSMNetworkInfo implements Serializable {

  private static final long serialVersionUID = -4463553185471116664L;

  private final CellInfo[] cellInfo;
  private final String operator;
  private final OperatorSelectionMode operatorSelectionMode;
  private final NetworkRegistrationStatus regStatus;

  public GSMNetworkInfo(final CellInfo[] cellInfo, final String operator, final OperatorSelectionMode operatorSelectionMode,
      final NetworkRegistrationStatus regStatus) {
    this.cellInfo = cellInfo;
    this.operator = operator;
    this.operatorSelectionMode = operatorSelectionMode;
    this.regStatus = regStatus;
  }

  public CellInfo[] getCellInfo() {
    return cellInfo == null ? null : cellInfo.clone();
  }

  public String getOperator() {
    return operator;
  }

  public OperatorSelectionMode getOperatorSelectionMode() {
    return operatorSelectionMode;
  }

  public NetworkRegistrationStatus getRegStatus() {
    return regStatus;
  }

  @Override
  public String toString() {
    if (cellInfo != null && cellInfo.length > 0) {
      StringBuilder neighbourCellsStr = new StringBuilder();
      int i = 1;
      for (CellInfo cell : cellInfo) {
        neighbourCellsStr.append(System.getProperty("line.separator"));
        neighbourCellsStr.append(i++);
        neighbourCellsStr.append(" Neighbour cell: ");
        neighbourCellsStr.append(cell);
      }
      return String.format("Serving cell: %s%s", cellInfo[0], neighbourCellsStr.toString());
    }
    return String.format("Serving cell: %s%s", "", "");
  }

}
