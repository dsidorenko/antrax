package com.flamesgroup.commons.voipantispam;

public enum VoipAntiSpamNumberType {

  CALLER("Caller(A-number)"),
  CALLED("Called(B-number)");

  private final String value;

  VoipAntiSpamNumberType(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public static VoipAntiSpamNumberType getNumberTypeByValue(final String value) {
    for (VoipAntiSpamNumberType numberType : VoipAntiSpamNumberType.values()) {
      if (numberType.getValue().equals(value)) {
        return numberType;
      }
    }
    throw new IllegalArgumentException("Unsupported value: " + value);
  }

}
