/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons.voipantispam;

import java.io.Serializable;
import java.util.Objects;

public class CdrFilterRule implements Serializable {

  private static final long serialVersionUID = 3623282464584822995L;

  public enum CdrFilterRuleType {
    ASR, ACD, ASR_AND_ACD
  }

  private final CdrFilterRuleType ruleType;
  private final int minAsr;
  private final int minAcd;

  public CdrFilterRule(final CdrFilterRuleType ruleType, final int minAsr, final int minAcd) {
    this.ruleType = ruleType;
    this.minAsr = minAsr;
    this.minAcd = minAcd;
  }

  public CdrFilterRuleType getRuleType() {
    return ruleType;
  }

  public int getMinAsr() {
    return minAsr;
  }

  public int getMinAcd() {
    return minAcd;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CdrFilterRule)) {
      return false;
    }
    final CdrFilterRule that = (CdrFilterRule) object;

    return minAsr == that.minAsr && minAcd == that.minAcd && ruleType == that.ruleType;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(ruleType);
    result = prime * result + minAsr;
    result = prime * result + minAcd;
    return result;
  }

  @Override
  public String toString() {
    switch (ruleType) {
      case ASR:
        return "ASR: " + minAsr + "%";
      case ACD:
        return "ACD: " + minAcd + " sec";
      case ASR_AND_ACD:
        return "ASR: " + minAsr + "%" + " and ACD: " + minAcd + " sec";
    }
    return super.toString();
  }
}
