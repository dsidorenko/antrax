/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons.voipantispam;

import java.io.Serializable;
import java.util.Objects;

public class CdrConfig implements Serializable {

  private static final long serialVersionUID = -4343688908828606349L;

  public enum CdrListType {
    WHITE, BLACK
  }

  private final VoipAntiSpamNumberType numberType;
  private final int period;
  private final int maxCallCount;
  private final CdrListType listType;
  private final CdrFilterRule filterRule;

  public CdrConfig(final VoipAntiSpamNumberType numberType, final int period, final int maxCallCount, final CdrListType listType, final CdrFilterRule filterRule) {
    this.numberType = numberType;
    this.period = period;
    this.maxCallCount = maxCallCount;
    this.listType = listType;
    this.filterRule = filterRule;
  }

  public VoipAntiSpamNumberType getNumberType() {
    return numberType;
  }

  public int getPeriod() {
    return period;
  }

  public int getMaxCallCount() {
    return maxCallCount;
  }

  public CdrListType getListType() {
    return listType;
  }

  public CdrFilterRule getFilterRule() {
    return filterRule;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CdrConfig)) {
      return false;
    }
    final CdrConfig that = (CdrConfig) object;

    return period == that.period
        && maxCallCount == that.maxCallCount
        && numberType == that.numberType
        && Objects.equals(listType, that.listType)
        && Objects.equals(filterRule, that.filterRule);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(numberType);
    result = prime * result + period;
    result = prime * result + maxCallCount;
    result = prime * result + Objects.hashCode(listType);
    result = prime * result + Objects.hashCode(filterRule);
    return result;
  }

}
