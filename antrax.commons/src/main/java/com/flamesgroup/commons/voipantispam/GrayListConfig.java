/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons.voipantispam;

import java.io.Serializable;
import java.util.Objects;

public class GrayListConfig implements Serializable {

  private static final long serialVersionUID = 2485180619568867136L;

  private final VoipAntiSpamNumberType numberType;

  private final int period;
  private final int maxRoutingRequestPerPeriod;
  private final int blockPeriod;
  private final int maxBlockCountBeforeMoveToBlackList;

  private final AcdConfig acdConfig;
  private final AsrConfig asrConfig;
  private final boolean callToTheSameCalledNumbers;

  public GrayListConfig(final VoipAntiSpamNumberType numberType, final int period, final int maxRoutingRequestPerPeriod, final int blockPeriod, final int maxBlockCountBeforeMoveToBlackList,
      final boolean callToTheSameCalledNumbers) {
    this(numberType, period, maxRoutingRequestPerPeriod, blockPeriod, maxBlockCountBeforeMoveToBlackList, callToTheSameCalledNumbers, null, null);
  }

  public GrayListConfig(final VoipAntiSpamNumberType numberType, final int period, final int maxRoutingRequestPerPeriod, final int blockPeriod, final int maxBlockCountBeforeMoveToBlackList,
      final boolean callToTheSameCalledNumbers, final AcdConfig acdConfig, final AsrConfig asrConfig) {
    this.numberType = numberType;
    this.period = period;
    this.maxRoutingRequestPerPeriod = maxRoutingRequestPerPeriod;
    this.blockPeriod = blockPeriod;
    this.maxBlockCountBeforeMoveToBlackList = maxBlockCountBeforeMoveToBlackList;
    this.callToTheSameCalledNumbers = callToTheSameCalledNumbers;
    this.acdConfig = acdConfig;
    this.asrConfig = asrConfig;
  }

  public VoipAntiSpamNumberType getNumberType() {
    return numberType;
  }

  public int getPeriod() {
    return period;
  }

  public int getMaxRoutingRequestPerPeriod() {
    return maxRoutingRequestPerPeriod;
  }

  public int getBlockPeriod() {
    return blockPeriod;
  }

  public int getMaxBlockCountBeforeMoveToBlackList() {
    return maxBlockCountBeforeMoveToBlackList;
  }

  public boolean isCallToTheSameCalledNumbers() {
    return callToTheSameCalledNumbers;
  }

  public AcdConfig getAcdConfig() {
    return acdConfig;
  }

  public AsrConfig getAsrConfig() {
    return asrConfig;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof GrayListConfig)) {
      return false;
    }
    final GrayListConfig that = (GrayListConfig) object;

    return numberType == that.numberType
        && period == that.period
        && maxRoutingRequestPerPeriod == that.maxRoutingRequestPerPeriod
        && blockPeriod == that.blockPeriod
        && maxBlockCountBeforeMoveToBlackList == that.maxBlockCountBeforeMoveToBlackList
        && callToTheSameCalledNumbers == that.callToTheSameCalledNumbers
        && Objects.equals(acdConfig, that.acdConfig)
        && Objects.equals(asrConfig, that.asrConfig);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(numberType);
    result = prime * result + period;
    result = prime * result + maxRoutingRequestPerPeriod;
    result = prime * result + blockPeriod;
    result = prime * result + maxBlockCountBeforeMoveToBlackList;
    result = prime * result + Boolean.hashCode(callToTheSameCalledNumbers);
    result = prime * result + Objects.hashCode(acdConfig);
    result = prime * result + Objects.hashCode(asrConfig);
    return result;
  }

}
