/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

abstract class BasePattern implements Serializable {

  private static final long serialVersionUID = -2244444165172898073L;

  private final String pattern;

  public BasePattern(final String pattern) {
    validate(pattern);
    this.pattern = pattern;
  }

  public String getPattern() {
    return pattern;
  }

  @Override
  public String toString() {
    return getPattern();
  }

  private void validate(final String pattern) {
    Matcher matcher = getRegexPattern().matcher(pattern);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Wrong pattern [" + pattern + "]. Pattern must conform regex:" + matcher.pattern());
    }
  }

  protected abstract Pattern getRegexPattern();

}
