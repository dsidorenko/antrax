/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class IMEI implements Serializable {

  private static final long serialVersionUID = 2494950755117326567L;

  private static final String EXPRESSION_FROM_IMEI_PATTERN_TEMPLATE = "\\(N(?<phoneNumberPosition>\\d+)(?<operator>[+-])(?<number>\\d)\\)";
  private static final Pattern expressionFromIMEIPatternTemplate = Pattern.compile(EXPRESSION_FROM_IMEI_PATTERN_TEMPLATE);

  private final byte[] tac; // TAC (Type Approval Code)
  private final byte[] fac; // FAC (Final Assembly Code)
  private final byte[] snr; // SNR (Serial Number)

  public IMEI(final byte[] tac, final byte[] fac, final byte[] snr) {
    if (tac.length != 6) {
      throw new IllegalArgumentException("IMEI TAC length must be 6, but it's " + tac.length);
    } else {
      this.tac = tac;
    }
    if (fac.length != 2) {
      throw new IllegalArgumentException("IMEI FAC length must be 2, but it's " + fac.length);
    } else {
      this.fac = fac;
    }
    if (snr.length != 6) {
      throw new IllegalArgumentException("IMEI SNR length must be 6, but it's " + snr.length);
    } else {
      this.snr = snr;
    }
  }

  /**
   * Returns a copy of the bytes in IMEI TAC
   *
   * @return a copy of the bytes in IMEI TAC
   */
  public byte[] getTac() {
    byte[] retVal = new byte[tac.length];
    System.arraycopy(tac, 0, retVal, 0, tac.length);
    return retVal;
  }

  /**
   * Returns a copy of the bytes in IMEI FAC
   *
   * @return a copy of the bytes in IMEI FAC
   */
  public byte[] getFac() {
    byte[] retVal = new byte[fac.length];
    System.arraycopy(fac, 0, retVal, 0, fac.length);
    return retVal;
  }

  /**
   * Returns a copy of the bytes in IMEI SNR
   *
   * @return a copy of the bytes in IMEI SNR
   */
  public byte[] getSnr() {
    byte[] retVal = new byte[snr.length];
    System.arraycopy(snr, 0, retVal, 0, snr.length);
    return retVal;
  }

  public String getValue() {
    return new String(tac) + new String(fac) + new String(snr);
  }

  public static IMEI valueOf(final String imei) throws IMEIException {
    if (imei.length() != 14) {
      throw new IMEIException("IMEI length must be 14, but it's " + imei.length());
    }
    return new IMEI(imei.substring(0, 6).getBytes(), imei.substring(6, 8).getBytes(), imei.substring(8).getBytes());
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof IMEI)) {
      return false;
    }
    IMEI that = (IMEI) obj;
    return Arrays.equals(tac, that.tac) && Arrays.equals(fac, that.fac) && Arrays.equals(snr, that.snr);
  }

  @Override
  public int hashCode() {
    return Arrays.hashCode(tac) + Arrays.hashCode(fac) + Arrays.hashCode(snr) + 7;
  }

  @Override
  public String toString() {
    return "IMEI(" + getValue() + ")";
  }

  /**
   * Returns IMEI value as byte array
   *
   * @return IMEI value as byte array
   */
  public byte[] toByteArray() {
    byte[] retVal = new byte[tac.length + fac.length + snr.length];
    int offset = 0;
    System.arraycopy(tac, 0, retVal, offset, tac.length);
    offset += tac.length;
    System.arraycopy(fac, 0, retVal, offset, fac.length);
    offset += fac.length;
    System.arraycopy(snr, 0, retVal, offset, snr.length);
    return retVal;
  }

  /**
   * Generates IMEI using pattern, where pattern is sequence of digit, X, (NY±Z)<br>
   * <b>X</b> - it substitutes with random digit 0-9.<br>
   * <b>(NY±Z)</b> - expression where  "NY" - digit number in the Phone Number card located
   * in Y position Phone Number and count from right to left. "Z" - a number from 0 to 9. For example: (N2+4)
   *
   * @param imeiPattern - IMEI Pattern
   * @param phoneNumber - Phone number
   * @return IMEI
   */
  public static IMEI generate(final IMEIPattern imeiPattern, final PhoneNumber phoneNumber) throws IMEIException {
    String pattern = imeiPattern.getPattern();
    Matcher matcher = expressionFromIMEIPatternTemplate.matcher(pattern);
    while (matcher.find()) {
      int phoneNumberPositionFromEnd = Integer.parseInt(matcher.group("phoneNumberPosition"));
      String operator = matcher.group("operator");
      int randomNumber = Integer.parseInt(matcher.group("number"));
      if (phoneNumber == null || phoneNumber.isPrivate()) {
        throw new IMEIException("Bad phone number[" + phoneNumber + "]");
      } else if (phoneNumber.getValue().length() <= phoneNumberPositionFromEnd) {
        throw new IMEIException("Bad phone position in phone number, position: [" + phoneNumberPositionFromEnd + "], length of phone number: [" + phoneNumber.getValue().length() + "]");
      } else {
        int phoneNumberValue = Character.getNumericValue(phoneNumber.getValue().charAt(phoneNumber.getValue().length() - phoneNumberPositionFromEnd - 1));
        int expansionResult = Math.abs((operator.equals("-") ? phoneNumberValue - randomNumber : phoneNumberValue + randomNumber) % 10);

        pattern = pattern.replaceFirst(EXPRESSION_FROM_IMEI_PATTERN_TEMPLATE, String.valueOf(expansionResult));
      }
    }
    Random random = new Random();
    pattern = pattern.chars().mapToObj(i -> (char) i).map(character -> character == 'X' ? ((Character) Character.forDigit(random.nextInt(10), 10)).toString() : character.toString())
        .collect(Collectors.joining());
    return IMEI.valueOf(pattern);
  }

  public int getLength() {
    return tac.length + fac.length + snr.length;
  }

}
