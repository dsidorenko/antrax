/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

public class RemoteInvocationWrapper implements RemoteInvocationHandler {

  private final Object wrappedObject;

  public RemoteInvocationWrapper(final Object wrappedObject) {
    if (wrappedObject == null) {
      throw new IllegalArgumentException("Object to wrap is required");
    }
    this.wrappedObject = wrappedObject;
  }

  @Override
  public Object invoke(final RemoteInvocation invocation) throws RemoteException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
    return invocation.invoke(wrappedObject);
  }

}
