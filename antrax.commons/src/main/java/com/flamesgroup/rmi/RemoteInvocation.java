/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RemoteInvocation implements Serializable {

  private static final long serialVersionUID = -7028946312046664966L;

  private String methodName;
  private Class<?>[] parameterTypes;
  private Object[] arguments;

  public RemoteInvocation() {
  }

  public RemoteInvocation(final String methodName, final Class<?>[] parameterTypes, final Object[] arguments) {
    this.methodName = methodName;
    this.parameterTypes = parameterTypes;
    this.arguments = arguments;
  }

  public String getMethodName() {
    return methodName;
  }

  public void setMethodName(final String methodName) {
    this.methodName = methodName;
  }

  public Class<?>[] getParameterTypes() {
    return parameterTypes;
  }

  public void setParameterTypes(final Class<?>[] parameterTypes) {
    this.parameterTypes = parameterTypes;
  }

  public Object[] getArguments() {
    return arguments;
  }

  public void setArguments(final Object[] arguments) {
    this.arguments = arguments;
  }

  public Object invoke(final Object targetObject) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    Method method = targetObject.getClass().getMethod(this.methodName, this.parameterTypes);
    return method.invoke(targetObject, this.arguments);
  }

}
