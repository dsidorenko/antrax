/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.rmi;

import com.flamesgroup.rmi.exception.RemoteConnectFailureException;
import com.flamesgroup.rmi.exception.RemoteInvocationFailureException;
import com.flamesgroup.rmi.exception.RemoteLookupFailureException;
import com.flamesgroup.rmi.exception.RemoteProxyFailureException;
import com.flamesgroup.rmi.utils.ProxyFactory;
import com.flamesgroup.rmi.utils.RmiUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class RemoteProxyFactory {

  private RemoteProxyFactory() {
  }

  @SuppressWarnings("unchecked")
  public static <T> T create(final RemoteAccessor<T> remoteAccessor) {
    RemoteClientInvocationHandler<T> remoteClientInvocationHandler = new RemoteClientInvocationHandler<>(remoteAccessor);
    remoteClientInvocationHandler.prepare();
    return (T) ProxyFactory.createProxy(remoteAccessor.getServiceInterface(), remoteClientInvocationHandler);
  }

  private static class RemoteClientInvocationHandler<T> implements InvocationHandler {

    private static final Logger logger = LoggerFactory.getLogger(RemoteClientInvocationHandler.class);

    private final RemoteAccessor<T> remoteAccessor;

    private final Lock stubLock = new ReentrantLock();
    private Remote cachedStub;

    private RemoteClientInvocationHandler(final RemoteAccessor<T> remoteAccessor) {
      this.remoteAccessor = remoteAccessor;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      Remote stub = getStub();
      try {
        return doInvoke(method, args, stub);
      } catch (RemoteConnectFailureException ex) {
        return handleRemoteConnectFailure(method, args, ex);
      } catch (RemoteException ex) {
        if (RmiUtils.isConnectFailure(ex)) {
          return handleRemoteConnectFailure(method, args, ex);
        } else {
          throw ex;
        }
      }
    }

    private void prepare() throws RemoteLookupFailureException {
      if (remoteAccessor.isLookupStubOnStartup()) {
        Remote remoteObj = lookupStub();
        if (logger.isDebugEnabled()) {
          if (remoteObj instanceof RemoteInvocationHandler) {
            logger.debug("[{}] - RMI stub [{}] is an RMI invoker", this, remoteAccessor.getServiceUrl());
          } else if (remoteAccessor.getServiceInterface() != null) {
            boolean isImpl = remoteAccessor.getServiceInterface().isInstance(remoteObj);
            logger.debug("[{}] - using service interface [{}] for RMI stub [{}] - {} directly implemented", this, remoteAccessor.getServiceInterface().getName(),
                remoteAccessor.getServiceUrl(), !isImpl ? "not " : "");
          }
        }
        if (remoteAccessor.isCacheStub()) {
          cachedStub = remoteObj;
        }
      }
    }

    private Remote lookupStub() throws RemoteLookupFailureException {
      try {
        Remote stub = null;
        if (remoteAccessor.getRegistryClientSocketFactory() == null) {
          stub = Naming.lookup(remoteAccessor.getServiceUrl());
        } else {
          URL url = new URL(null, remoteAccessor.getServiceUrl(), new DummyURLStreamHandler());
          String protocol = url.getProtocol();
          if (protocol != null && !"rmi".equals(protocol)) {
            throw new MalformedURLException("Invalid URL scheme '" + protocol + "'");
          }
          String host = url.getHost();
          int port = url.getPort();
          String name = url.getPath();
          if (name != null && name.startsWith("/")) {
            name = name.substring(1);
          }
          Registry registry = LocateRegistry.getRegistry(host, port, remoteAccessor.getRegistryClientSocketFactory());
          stub = registry.lookup(name);
        }
        if (logger.isDebugEnabled()) {
          logger.debug("[{}] - located RMI stub with URL [{}]", this, remoteAccessor.getServiceUrl());
        }
        return stub;
      } catch (MalformedURLException ex) {
        throw new RemoteLookupFailureException("Service URL [" + remoteAccessor.getServiceUrl() + "] is invalid", ex);
      } catch (NotBoundException ex) {
        throw new RemoteLookupFailureException("Could not find RMI service [" + remoteAccessor.getServiceUrl() + "] in RMI registry", ex);
      } catch (RemoteException ex) {
        throw new RemoteLookupFailureException("Lookup of RMI stub failed", ex);
      }
    }

    private Remote getStub() throws RemoteLookupFailureException {
      if (!remoteAccessor.isCacheStub() || (remoteAccessor.isLookupStubOnStartup() && !remoteAccessor.isRefreshStubOnConnectFailure())) {
        return (this.cachedStub != null ? this.cachedStub : lookupStub());
      } else {
        stubLock.lock();
        try {
          if (this.cachedStub == null) {
            this.cachedStub = lookupStub();
          }
          return this.cachedStub;
        } finally {
          stubLock.unlock();
        }
      }
    }

    private Object doInvoke(final Method method, final Object[] args, final Remote stub) throws Throwable {
      if (stub instanceof RemoteInvocationHandler) {
        try {
          RemoteInvocationHandler handler = (RemoteInvocationHandler) stub;
          return handler.invoke(new RemoteInvocation(method.getName(), method.getParameterTypes(), args));
        } catch (RemoteException ex) {
          throw RmiUtils.convertRmiAccessException(method, ex, RmiUtils.isConnectFailure(ex), remoteAccessor.getServiceUrl());
        } catch (InvocationTargetException ex) {
          throw ex.getTargetException();
        } catch (Throwable ex) {
          throw new RemoteInvocationFailureException("Invocation of method [" + method + "] failed in RMI service [" + remoteAccessor.getServiceUrl() + "]", ex);
        }
      } else {
        try {
          try {
            if (method.getDeclaringClass().isInstance(stub)) {
              return method.invoke(stub, args);
            } else {
              Method stubMethod = stub.getClass().getMethod(method.getName(), method.getParameterTypes());
              return stubMethod.invoke(stub, args);
            }
          } catch (InvocationTargetException ex) {
            throw ex;
          } catch (NoSuchMethodException ex) {
            throw new RemoteProxyFailureException("No matching RMI stub method found for: " + method, ex);
          } catch (Throwable ex) {
            throw new RemoteProxyFailureException("Invocation of RMI stub method failed: " + method, ex);
          }
        } catch (InvocationTargetException ex) {
          Throwable targetEx = ex.getTargetException();
          if (targetEx instanceof RemoteException) {
            RemoteException rex = (RemoteException) targetEx;
            throw RmiUtils.convertRmiAccessException(method, rex, RmiUtils.isConnectFailure(rex), remoteAccessor.getServiceUrl());
          } else {
            throw targetEx;
          }
        }
      }
    }

    private Object refreshAndRetry(final Method method, final Object[] args) throws Throwable {
      Remote freshStub = null;
      stubLock.lock();
      try {
        freshStub = lookupStub();
        if (remoteAccessor.isCacheStub()) {
          this.cachedStub = freshStub;
        }
      } finally {
        stubLock.unlock();
      }
      return doInvoke(method, args, freshStub);
    }

    private Object handleRemoteConnectFailure(final Method method, final Object[] args, final Exception ex) throws Throwable {
      if (remoteAccessor.isRefreshStubOnConnectFailure()) {
        if (logger.isDebugEnabled()) {
          logger.debug("[{}] - could not connect to RMI service [{}] - retrying", this, remoteAccessor.getServiceUrl(), ex);
        } else if (logger.isWarnEnabled()) {
          logger.warn("[{}] - could not connect to RMI service [{}] - retrying", this, remoteAccessor.getServiceUrl());
        }
        return refreshAndRetry(method, args);
      } else {
        throw ex;
      }
    }

    private static class DummyURLStreamHandler extends URLStreamHandler {

      @Override
      protected URLConnection openConnection(final URL url) throws IOException {
        throw new UnsupportedOperationException();
      }
    }
  }

}
