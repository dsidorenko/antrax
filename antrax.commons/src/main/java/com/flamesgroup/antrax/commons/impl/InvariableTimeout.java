/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.commons.impl;

import com.flamesgroup.antrax.commons.Timeout;

import java.util.Random;

public class InvariableTimeout implements Timeout {

  private static final long serialVersionUID = 5997897247468120794L;
  public static final String DELIMITER = "±";
  private final Random random;
  private final long mediana;
  private final long delta;

  public InvariableTimeout(final long mediana, final long delta) {
    if (delta < 0 || delta > mediana) {
      throw new IllegalArgumentException("delta: " + delta + "mediana: " + mediana);
    }
    random = new Random();
    this.delta = delta;
    this.mediana = mediana;
  }

  @Override
  public long getTimeout() {

    if (mediana == 0L && delta == 0L) {
      return 0L;
    }
    if (delta <= 0) {
      return mediana;
    }

    double minValue = mediana - delta;
    double maxValue = mediana + delta;

    double timeout = minValue + (maxValue - minValue) * random.nextDouble();
    return Math.round(timeout);
  }

  @Override
  public int hashCode() {
    return (int) delta + 3 * (int) mediana;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof InvariableTimeout) {
      InvariableTimeout that = (InvariableTimeout) obj;
      return that.delta == delta && that.mediana == mediana;
    }
    return false;
  }

  @Override
  public long getDelta() {
    return delta;
  }

  @Override
  public long getMediana() {
    return mediana;
  }

  @Override
  public String toString() {
    if (getDelta() == 0) {
      return String.valueOf(getMediana());
    }
    return String.format("%d%s%d", getMediana() / 1000, DELIMITER, getDelta() / 1000);
  }

  public static Timeout parseTimeout(final String text) {
    if (text == null) {
      return new InvariableTimeout(0, 0);
    }
    String[] parts = text.split(DELIMITER);
    if (parts.length == 0) {
      return new InvariableTimeout(0, 0);
    }
    int first = Integer.parseInt(parts[0]);
    if (parts.length == 1) {
      return new InvariableTimeout(first, 0);
    }
    int second = Integer.parseInt(parts[1]);
    return new InvariableTimeout(first, second);
  }

}
