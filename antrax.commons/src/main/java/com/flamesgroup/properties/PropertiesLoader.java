/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.properties;

import java.util.Properties;

public class PropertiesLoader {

  private final Properties properties;

  public PropertiesLoader(final Properties properties) {
    this.properties = properties;
  }

  public boolean getBoolean(final String property) {
    String propertyValue = getProperty(property);

    return "true".equalsIgnoreCase(propertyValue) || "yes".equalsIgnoreCase(propertyValue) || "on".equalsIgnoreCase(propertyValue);
  }

  public int getInt(final String property) {
    String propertyValue = getProperty(property);

    return Integer.parseInt(propertyValue);
  }

  public long getLong(final String property) {
    String propertyValue = getProperty(property);

    return Long.parseLong(propertyValue);
  }

  public long getPeriod(final String property) {
    String propertyValue = getProperty(property);

    return parsePeriodValue(propertyValue, property);
  }

  private long parsePeriodValue(final String val, final String propName) {
    long period = 0;
    PeriodTokenizer scanner = new PeriodTokenizer(val);
    long number = 0;
    while (scanner.hasMoreTokens()) {
      switch (scanner.nextTok()) {
        case OTHER:
          throw new BadPropertyException(propName, val);
        case DAY:
          number *= 24;
        case HOUR:
          number *= 60;
        case MINUTE:
          number *= 60;
        case SECOND:
          number *= 1000;
          period += number;
          number = 0;
          break;
        case NUMBER:
          number = number * 10 + Long.parseLong(scanner.getTokValue());
        case SEP:
      }
    }

    return period;
  }

  public String getString(final String property) {
    return getProperty(property);
  }

  private String getProperty(final String property) {
    if (!properties.containsKey(property)) {
      throw new PropertyNotFoundException(String.format("Can't find property %s", property));
    }
    return properties.getProperty(property);
  }

  private static class PeriodTokenizer {

    public enum Tok {
      NUMBER,
      DAY,
      HOUR,
      MINUTE,
      SECOND,
      SEP,
      OTHER
    }

    private int index;
    private final String str;

    public PeriodTokenizer(final String val) {
      index = 0;
      str = val;
    }

    public String getTokValue() {
      return str.substring(index - 1, index);
    }

    public boolean hasMoreTokens() {
      return index < str.length();
    }

    public Tok nextTok() {
      char c = str.charAt(index++);
      if (c == 'm') {
        return Tok.MINUTE;
      }
      if (c == 's') {
        return Tok.SECOND;
      }
      if (c == 'h') {
        return Tok.HOUR;
      }
      if (c == 'd') {
        return Tok.DAY;
      }
      if (c == ' ') {
        return Tok.SEP;
      }
      if (Character.isDigit(c)) {
        return Tok.NUMBER;
      }
      return Tok.OTHER;
    }
  }

}
