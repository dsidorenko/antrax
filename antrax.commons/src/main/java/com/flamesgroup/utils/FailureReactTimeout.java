/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

/**
 * On case of failures this timeout will grow until riches maximum value. First
 * success value will make this timeout to the minimum value.
 */
public class FailureReactTimeout {

  private final long minimum;
  private final long maximum;

  private int tryCount = 0;

  public FailureReactTimeout(final long minimum, final long maximum) {
    this.minimum = minimum;
    this.maximum = maximum;
  }

  public void sleep(final Object semaphore) throws InterruptedException {
    semaphore.wait(countTimeout());
  }

  private long countTimeout() {
    if (tryCount < 2) {
      return minimum;
    }
    if (tryCount > 4) {
      return maximum;
    }
    return (maximum - minimum) / 2;
  }

  public void register(final boolean result) {
    if (result) {
      tryCount = 0;
    } else {
      tryCount++;
    }
  }

  public static void main(final String[] args) {
    FailureReactTimeout t = new FailureReactTimeout(200, 5 * 60 * 1000);
    for (int i = 0; i < 10; ++i) {
      System.out.println(t.countTimeout());
      t.register(false);
    }
  }

}
