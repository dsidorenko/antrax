/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

public final class BuildInfo {

  private final String projectVersion;
  private final String buildTime;
  private final String buildGitSHA;
  private final String buildGitBranch;
  private final String buildJdk;

  public BuildInfo(final String projectVersion, final String buildTime, final String buildGitSHA, final String buildGitBranch, final String buildJdk) {
    this.projectVersion = projectVersion;
    this.buildTime = buildTime;
    this.buildGitSHA = buildGitSHA;
    this.buildGitBranch = buildGitBranch;
    this.buildJdk = buildJdk;
  }

  public String getProjectVersion() {
    return projectVersion;
  }

  public String getBuildTime() {
    return buildTime;
  }

  public String getBuildGitSHA() {
    return buildGitSHA;
  }

  public String getBuildGitBranch() {
    return buildGitBranch;
  }

  public String getBuildJdk() {
    return buildJdk;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append(getClass().getSimpleName()).append('@').append(Integer.toHexString(hashCode()));
    sb.append("[projectVersion:'").append(projectVersion).append('\'');
    sb.append(" buildTime:'").append(buildTime).append('\'');
    sb.append(" buildGitSHA:'").append(buildGitSHA).append('\'');
    sb.append(" buildGitBranch:'").append(buildGitBranch).append('\'');
    sb.append(" buildJdk:'").append(buildJdk).append('\'');
    sb.append(']');
    return sb.toString();
  }

}
