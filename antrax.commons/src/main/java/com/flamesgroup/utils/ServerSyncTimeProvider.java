/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import java.io.IOException;
import java.io.Serializable;

/**
 * Time provider, which takes in mind differences of times between servers. It
 * counts mother servers time.
 */
public class ServerSyncTimeProvider implements TimeProvider, Serializable {

  private static final long serialVersionUID = -3422760987377610015L;

  private transient long delta = 0;

  @Override
  public long currentTimeMillis() {
    return System.currentTimeMillis() + delta;
  }

  private void writeObject(final java.io.ObjectOutputStream out) throws IOException {
    out.writeLong(currentTimeMillis()); // Writing servers time
  }

  private void readObject(final java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
    long serversTime = in.readLong();
    delta = serversTime - System.currentTimeMillis();
  }

}
