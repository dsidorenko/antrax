/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

public class AntraxProperties {

  public final static int CONTROL_SERVER_RMI_PORT = parseRmiPort(System.getProperty("control.server.url"));
  public final static String CONTROL_SERVER_HOST_NAME = parseHostName(System.getProperty("control.server.url"));
  public final static String SERVER_NAME = parseName(System.getProperty("server.name"));

  static String parseHostName(final String property) {
    checkUrl(property);
    return property.substring(property.lastIndexOf("/") + 1, property.lastIndexOf(':'));
  }

  static int parseRmiPort(final String property) {
    checkUrl(property);
    return Integer.parseInt(property.substring(property.lastIndexOf(':') + 1));
  }

  static String parseName(final String property) {
    if (property == null || property.isEmpty()) {
      throw new IllegalStateException("server.name is empty");
    }
    return property;
  }

  static void checkUrl(final String property) {
    if (property == null || property.isEmpty()) {
      throw new IllegalStateException("control.server.url is empty");
    }
    if (!property.startsWith("rmi://")) {
      throw new IllegalStateException("control.server.url must starts with rmi://");
    }
    if (!property.matches(".*:\\d+$")) {
      throw new IllegalStateException("control.server.url should contains port");
    }
  }

}
