/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ExceptionlessProxy {

  private static class ExceptionlessHandler implements InvocationHandler {

    private final Object delegate;

    public ExceptionlessHandler(final Object delegate) {
      this.delegate = delegate;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      try {
        return method.invoke(delegate, args);
      } catch (Throwable ignored) {
        return defaultValueFor(method.getReturnType());
      }
    }

    private Object defaultValueFor(final Class<?> returnType) {
      if (returnType == Void.class) {
        return null;
      }
      if (returnType.isArray()) {
        return Array.newInstance(returnType.getComponentType(), 0);
      }
      return null;
    }

  }

  @SuppressWarnings("unchecked")
  public static <T> T proxy(final T delegate, final Class<T> clazz) {
    return (T) Proxy.newProxyInstance(delegate.getClass().getClassLoader(), new Class[] {clazz}, new ExceptionlessHandler(delegate));
  }

}
