/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import com.flamesgroup.unit.IMSI;

import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public final class OperatorsStorage {

  private final List<Operator> operators;

  private static volatile OperatorsStorage instance;

  private OperatorsStorage() {
    InputStream resourceStream = getClass().getClassLoader().getResourceAsStream("operators.list");
    if (resourceStream == null) {
      throw new IllegalStateException("Couldn't find operators.list in classpath");
    }

    Properties properties = new Properties();
    try {
      properties.load(resourceStream);
    } catch (IOException e) {
      throw new IllegalStateException("Failed to load properties: " + e.getMessage(), e);
    }

    operators = properties.entrySet().stream().map(e -> new Operator(String.valueOf(e.getKey()), String.valueOf(e.getValue())))
        .sorted(Comparator.comparing(Operator::getCode, Comparator.reverseOrder())).collect(Collectors.toList());
  }

  public List<Operator> getOperators() {
    return operators.stream().sorted(Comparator.comparing(Operator::getCode)).collect(Collectors.toList());
  }

  public Operator findOperatorByImsi(final IMSI imsi) {
    return operators.stream().filter(e -> imsi.getValue().startsWith(e.getCode())).findFirst().orElse(null);
  }

  public Operator findOperatorByCode(final String code) {
    return operators.stream().filter(e -> e.getCode().equals(code)).findFirst().orElse(null);
  }

  public static OperatorsStorage getInstance() {
    if (instance == null) {
      synchronized (OperatorsStorage.class) {
        if (instance == null) {
          instance = new OperatorsStorage();
        }
      }
    }
    return instance;
  }

  public final static class Operator {

    private final String code;
    private final String name;

    private Operator(final String code, final String name) {
      this.code = code;
      this.name = name;
    }

    public String getCode() {
      return code;
    }

    public String getName() {
      return name;
    }

    public short getMccShort() {
      return Short.parseShort(code.substring(0, 3), 16);
    }

    public short getMncShort() {
      return Short.parseShort(code.substring(3), 16);
    }

    public String getMccHex() {
      return code.substring(0, 3);
    }

    public String getMncHex() {
      return code.substring(3);
    }

    @Override
    public String toString() {
      return name + " (" + code + ")";
    }

  }

}
