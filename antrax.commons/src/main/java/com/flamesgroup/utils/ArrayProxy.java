/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Wraps several implementations of one interface and invokes method on every
 * implementation. Method returns result of last invoked implementation
 */
public class ArrayProxy {

  public static class ArrayInvHandler<T> implements InvocationHandler {

    private final T[] delegates;

    public ArrayInvHandler(final T... delegates) {
      this.delegates = delegates;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
      Object retVal = null;
      for (T delegate : delegates) {
        try {
          retVal = method.invoke(delegate, args);
        } catch (InvocationTargetException e) {
          throw e.getCause();
        }
      }
      return retVal;
    }

  }

  @SuppressWarnings("unchecked")
  public static <T, K extends T> T proxy(final Class<T> clazz, final K... delegates) {
    return (T) Proxy.newProxyInstance(delegates[0].getClass().getClassLoader(), new Class[] {clazz}, new ArrayInvHandler(delegates));
  }

}
