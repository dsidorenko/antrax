/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.callfilter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PhoneNumberMatcherTest {

  @Test
  public void testNull() {
    new PhoneNumberMatcher(null, null, null);
  }

  @Test
  public void testDeny() {
    PhoneNumberMatcher matcher = new PhoneNumberMatcher("a;b;c", ".*", "");
    assertTrue(matcher.matches("d"));
    assertFalse(matcher.matches("a"));
    assertFalse(matcher.matches("b"));
    assertFalse(matcher.matches("c"));
  }

  @Test
  public void testAllow() {
    PhoneNumberMatcher matcher = new PhoneNumberMatcher(null, "a;b;c", "");
    assertFalse(matcher.matches("d"));
    assertTrue(matcher.matches("a"));
    assertTrue(matcher.matches("b"));
    assertTrue(matcher.matches("c"));
  }

  @Test
  public void testSubstitude() {
    PhoneNumberMatcher matcher = new PhoneNumberMatcher(null, "a(bcd)e;linu(x)", "$1");
    assertEquals("bcd", matcher.substituteNumber("abcde"));
    assertEquals("x", matcher.substituteNumber("linux"));
  }

  @Test
  public void testSubstitude2() {
    PhoneNumberMatcher matcher = new PhoneNumberMatcher(null, "a(bcd)e;linu(x)", "");
    assertEquals("abcde", matcher.substituteNumber("abcde"));
    assertEquals("linux", matcher.substituteNumber("linux"));
  }
}
