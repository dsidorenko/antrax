/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.junit.Test;

public class SimpleActivityOrCompositePredictEndTest {

  @Test
  public void testAllActivityAllowed() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2))));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFirstActivityAllowed() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2))));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testSecondActivityAllowed() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2))));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFalseFalseActivitiesAllowed() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2))));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testAllActivityAllowedOrCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2)).or(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFirstActivityAllowedOrCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2)).or(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testSecondActivityAllowedOrCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2)).or(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFalseFalseActivitiesAllowedOrCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2)).or(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testAllActivityAllowedAndCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2)).and(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFirstActivityAllowedAndCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2)).and(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testSecondActivityAllowedAndCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(true, new TimePeriodPrediction(TimePeriod.inMinutes(2)).and(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  @Test
  public void testFalseFalseActivitiesAllowedAndCompositePrediction() {
    SimpleActivityOrComposite orComposite = new SimpleActivityOrComposite(
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(5))),
        new FakeSimpleActivityScript(false, new TimePeriodPrediction(TimePeriod.inMinutes(2)).and(new AlwaysTruePrediction())));
    assertEquals("timeout passed 00:05:00 and timeout passed 00:02:00", orComposite.predictEnd().toLocalizedString());
  }

  private class FakeSimpleActivityScript extends BaseSimpleActivityScript {

    private static final long serialVersionUID = -8378529595415070463L;

    protected final boolean isActivityAllowed;
    protected final Prediction predictEnd;

    public FakeSimpleActivityScript(final boolean isActivityAllowed, final Prediction predictEnd) {
      this.isActivityAllowed = isActivityAllowed;
      this.predictEnd = predictEnd;
    }

    @Override
    public boolean isActivityAllowed() {
      return isActivityAllowed;
    }

    @Override
    public Prediction predictEnd() {
      return predictEnd;
    }

    @Override
    public Prediction predictStart() {
      throw new UnsupportedOperationException();
    }

    @Override
    public ScriptSaver getScriptSaver() {
      throw new UnsupportedOperationException();
    }

  }
}
