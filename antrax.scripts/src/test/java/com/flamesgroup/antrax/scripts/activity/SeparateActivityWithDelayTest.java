/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.activity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.activity.SeparateActivityWithDelay;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.junit.Test;

public class SeparateActivityWithDelayTest {

  @Test
  public void testAllowsFirstTime() {
    SeparateActivityWithDelay delay = new SeparateActivityWithDelay();
    delay.setDelay(new TimeInterval(new TimePeriod(1000), new TimePeriod(1000)));
    assertTrue(delay.isActivityAllowed());
    assertTrue(delay.isActivityAllowed());
    assertTrue(delay.isActivityAllowed());
  }

  @Test
  public void testForbidBeforeDelayExceeds() {
    SeparateActivityWithDelay delay = new SeparateActivityWithDelay();
    delay.setDelay(new TimeInterval(new TimePeriod(1000), new TimePeriod(1000)));
    delay.handlePeriodEnd();
    assertFalse(delay.isActivityAllowed());
    assertFalse(delay.isActivityAllowed());
    assertFalse(delay.isActivityAllowed());
  }

  @Test
  public void testAllowsAfterTimeout() throws InterruptedException {
    SeparateActivityWithDelay delay = new SeparateActivityWithDelay();
    delay.setDelay(new TimeInterval(new TimePeriod(20), new TimePeriod(20)));
    delay.handlePeriodEnd();
    assertFalse(delay.isActivityAllowed());
    Thread.sleep(50);
    assertTrue(delay.isActivityAllowed());
  }

}
