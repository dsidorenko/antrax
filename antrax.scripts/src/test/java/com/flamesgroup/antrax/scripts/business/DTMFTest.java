/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.business.dtmf.DTMFScriplet;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JMockit.class)
public class DTMFTest {

  @Mocked
  private RegisteredInGSMChannel channel;

  @Test
  public void testEvent() throws Exception {
    DTMF dtmfScript = new DTMF();
    dtmfScript.setEvent("DTMF_event");
    assertFalse(dtmfScript.shouldStartBusinessActivity());
    dtmfScript.handleGenericEvent("asd");
    assertFalse(dtmfScript.shouldStartBusinessActivity());
    dtmfScript.handleGenericEvent("DTMF_event");
    assertTrue(dtmfScript.shouldStartBusinessActivity());
  }

  @Test
  public void acceptanceTestSuccess() throws Exception {
    DTMF dtmfScript = new DTMF();
    final PhoneNumber number = new PhoneNumber("111");
    DTMFScriplet scriplet = new DTMFScriplet(number);
    scriplet.addDial('5');
    scriplet.addTimeout(new TimePeriod(100));
    scriplet.addDial('6');
    dtmfScript.setDTMFScriplet(scriplet);
    dtmfScript.setAttemptsCount(3);
    dtmfScript.handleGenericEvent("dtmf", GenericEvent.EventType.CHECKED, "success", "failure");

    new Expectations() {{
      channel.dial(withSameInstance(number), withInstanceOf(CallStateChangeHandler.class));
      channel.sendDTMF("5");
      channel.sendDTMF("6");
      channel.dropCall();
      channel.fireGenericEvent("success");
    }};

    dtmfScript.invokeBusinessActivity(channel);
  }

  @Test
  public void acceptanceTestFailure() throws Exception {
    DTMF dtmfScript = new DTMF();
    final PhoneNumber number = new PhoneNumber("111");
    DTMFScriplet scriplet = new DTMFScriplet(number);
    scriplet.addDial('+');
    dtmfScript.setDTMFScriplet(scriplet);
    dtmfScript.setAttemptsCount(2);
    dtmfScript.handleGenericEvent("dtmf", GenericEvent.EventType.CHECKED, "success", "failure");

    new Expectations() {{
      channel.dial(withSameInstance(number), withInstanceOf(CallStateChangeHandler.class));
      channel.sendDTMF("+");
      result = new Exception("Dial failure");

      channel.dropCall();
      channel.dial(withSameInstance(number), withInstanceOf(CallStateChangeHandler.class));

      channel.sendDTMF("+");
      result = new Exception("Dial failure");

      channel.dropCall();

      channel.fireGenericEvent("failure", "Dial failure");
    }};

    dtmfScript.invokeBusinessActivity(channel);
  }

}
