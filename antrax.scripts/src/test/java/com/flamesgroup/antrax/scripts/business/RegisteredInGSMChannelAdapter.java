/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.businesscripts.USSDSession;
import com.flamesgroup.antrax.automation.data.SimGroupReference;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.antrax.storage.enums.CdrDropReason;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;

public class RegisteredInGSMChannelAdapter implements RegisteredInGSMChannel {

  @Override
  public void dial(final PhoneNumber number, final CallStateChangeHandler handler) throws Exception {
  }

  @Override
  public void sendDTMF(final String dtmfString) throws Exception {
  }

  @Override
  public void sendDTMF(final char dtmf, final int duration) throws Exception {
  }

  @Override
  public void dropCall() {
  }

  @Override
  public void dropCall(final byte callDropCauseCode, final CdrDropReason cdrDropReason) {
  }

  @Override
  public void executeAction(final Action action, final long maxTime) throws Exception {
  }

  @Override
  public void fireGenericEvent(final String event, final Serializable... args) {
  }

  @Override
  public SimData getSimData() {
    return null;
  }

  @Override
  public void lock(final String reason) {
  }

  @Override
  public void sendSMS(final PhoneNumber phoneNumber, final String text) throws Exception {
  }

  @Override
  public String sendUSSD(final String ussd) throws Exception {
    return null;
  }

  @Override
  public boolean shouldStopActivity() {
    return false;
  }

  @Override
  public boolean shouldStopSession() {
    return false;
  }


  @Override
  public USSDSession startUSSDSession(final String ussd) throws Exception {
    return null;
  }

  @Override
  public void addUserMessage(final String message) {
  }

  @Override
  public void addToBlackList(final BlackListNumber phoneNumber) throws Exception {
  }

  @Override
  public void changeGroup(final SimGroupReference simGroup) {
  }

  @Override
  public void updatePhoneNumber(final PhoneNumber phoneNumber) throws Exception {
  }

  @Override
  public void resetIMEI() throws Exception {
  }

  @Override
  public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
    return null;
  }

  @Override
  public void changeAllowedInternet(final boolean allowed) {
  }

  @Override
  public void setTariffPlanEndDate(final long endDate) {
  }

  @Override
  public void setBalance(final double balance) {
  }

  @Override
  public void processAnswer() {
  }

}
