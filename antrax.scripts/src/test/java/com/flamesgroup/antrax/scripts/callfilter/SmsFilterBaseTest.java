/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.helper.business.ReplacePattern;
import org.junit.Test;
import org.slf4j.LoggerFactory;

public class SmsFilterBaseTest {

  @Test
  public void defaultIsAcceptsSmsText() {
    SmsFilterBase smsFilterBase = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    smsFilterBase.addAllowedTextPattern(smsFilterBase.getAllowedTextPattern());
    smsFilterBase.addDeniedTextPattern(smsFilterBase.getDeniedTextPattern());

    assertEquals(smsFilterBase.isAcceptsSmsText("test random sms"), true);
  }

  @Test
  public void passWithBreakLineIsAcceptsSimsText() {
    SmsFilterBase smsFilterBase = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    smsFilterBase.addAllowedTextPattern(smsFilterBase.getAllowedTextPattern());
    smsFilterBase.addDeniedTextPattern(smsFilterBase.getDeniedTextPattern());

    assertEquals(smsFilterBase.isAcceptsSmsText("�.а�.ол�.: 493859\n"
        + "Теле�.он (логин): 380987\n"
        + "http://m.rug.ru"), true);
  }

  @Test
  public void passOnlyAllowedTextIsAcceptsSmsText() {
    SmsFilterBase smsFilterBase = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    smsFilterBase.addAllowedTextPattern("test1|test2");
    smsFilterBase.addAllowedTextPattern("test3");
    smsFilterBase.addDeniedTextPattern(smsFilterBase.getDeniedTextPattern());

    //allow
    assertEquals(smsFilterBase.isAcceptsSmsText("test1"), true);
    assertEquals(smsFilterBase.isAcceptsSmsText("test2"), true);
    assertEquals(smsFilterBase.isAcceptsSmsText("test3"), true);
    //denied
    assertEquals(smsFilterBase.isAcceptsSmsText("test4"), false);
    assertEquals(smsFilterBase.isAcceptsSmsText("test random sms"), false);
  }

  @Test
  public void notPassDeniedTextIsAcceptsSmsText() {
    SmsFilterBase smsFilterBase = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    smsFilterBase.addAllowedTextPattern(smsFilterBase.getAllowedTextPattern());
    smsFilterBase.addDeniedTextPattern("notTest1|notTest2");
    smsFilterBase.addDeniedTextPattern("notTest3");

    //denied
    assertEquals(smsFilterBase.isAcceptsSmsText("notTest1"), false);
    assertEquals(smsFilterBase.isAcceptsSmsText("notTest2"), false);
    assertEquals(smsFilterBase.isAcceptsSmsText("notTest3"), false);
    //allow
    assertEquals(smsFilterBase.isAcceptsSmsText("notTest4"), true);
    assertEquals(smsFilterBase.isAcceptsSmsText("test random sms"), true);
  }

  @Test
  public void testSubstituteTextDefault() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.addTextReplacePattern(complexSmsFilterScript.getTextReplacePattern());
    String testText = "Test text";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(testText, res);
  }

  @Test
  public void testSubstituteTextCustom() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(Uber)(.*)", "$1U b e r$3", false));
    String testText = "Test Uber some";
    String resText = "Test U b e r some";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(res, resText);
  }

  @Test
  public void testSubstituteTextCustomTwo() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(Uber)(.*)", "$1U b e r$3", false));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(Viber)(.*)", "$1VIBER$3", false));
    String testText = "Test Viber some";
    String resText = "Test VIBER some";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(res, resText);
  }

  @Test
  public void testSubstituteTextCustomMultiple() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(code\\s)(.*)(?s)(.*)(code\\s)(.*)", "$1$3$4$6", false));
    String testText = "Your activation code is: \n"
        + "882172. \n"
        + "Close this message and enter the code to activate your account.";
    String resText = "Your activation is: \n"
        + "882172. \n"
        + "Close this message and enter the to activate your account.";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(res, resText);
  }

  @Test
  public void testSubstituteTextCustomMultipleReplace() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.setReplaceAllPatterns(true);
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(G-)(\\d{6})(.*)", "$2$3", false));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(code)(.*)", "$1КoДe$3", false));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(.*)(\\sgoogle)(.*)", "$1$3", false));
    String testText = "G-679425 is your google verification code";
    String resText = "679425 is your verification КoДe";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(res, resText);
  }

  @Test
  public void testSubstituteTextFindSubstring() {
    SmsFilterBase complexSmsFilterScript = new SmsFilterBase(LoggerFactory.getLogger(SmsFilterBaseTest.class));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("( code)", "", true));
    complexSmsFilterScript.addTextReplacePattern(new ReplacePattern("(Facebook)", "Twitter", true));
    String testText = "Use code 613649 as your login code for Mood. For your safety, do not share this code. (Account Kit by Facebook)";
    String resText = "Use 613649 as your login for Mood. For your safety, do not share this. (Account Kit by Twitter)";
    String res = complexSmsFilterScript.baseSubstituteText(testText);
    assertEquals(res, resText);
  }

}
