package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Script(name = "generate random event", doc = "waits until event occured and then sends random event from list")
public class GenerateRandomEvent implements BusinessActivityScript, GenericEventListener {

  private String event = "send_random_event";
  private final List<String> randomEvents = new LinkedList<>();

  private volatile GenericEvent caughtEvent;

  @ScriptParam(name = "event", doc = "event to send random event")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "random event", doc = "random event")
  public void addRandomEvent(final String randomEvent) {
    this.randomEvents.add(randomEvent);
  }

  public String getRandomEvent() {
    return "";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    int randomIndex = ThreadLocalRandom.current().nextInt(randomEvents.size());
    channel.fireGenericEvent(randomEvents.get(randomIndex));
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "sending random event";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

}
