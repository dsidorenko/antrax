/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.gateway;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.predictions.GatewaysPredictionFactory;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.util.LinkedList;

@Script(name = "any except previous", doc = "Accepts any gateway, except previous one, so guarantees sim card rotation")
public class AnyExceptPreviousGatewayScript implements GatewaySelectorScript, SessionListener, StatefullScript {

  private static final long serialVersionUID = -3222056455432513459L;

  @StateField
  private final LinkedList<IServerData> previousGateways = new LinkedList<>();

  private int depth = 1;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "previous gateways depth", doc = "depth of previous gateways to remember and use to filter them out. If depth <= 0 behaves like AnyGatwayScript. ")
  public void setDepth(final int depth) {
    this.depth = depth;
  }

  public int getDepth() {
    return depth;
  }

  @Override
  public void handleSessionStarted(final IServerData server) {
    if (depth > 0) {
      //      while (previousGateways.size() >= depth) {
      //        previousGateways.removeFirst();
      //      }
      previousGateways.add(server);
      saver.save();
    }
  }

  @Override
  public void handleSessionEnded() {
  }

  @Override
  public boolean isGatewayApplies(final IServerData gateway) {
    if (depth >= 0) {
      while (previousGateways.size() > depth) {
        previousGateways.removeFirst();
      }
      saver.save();
      int index = previousGateways.indexOf(gateway);
      return index < 0 || index + 1 > depth;
    } else {
      return true;
    }
  }

  @Override
  public Prediction predictNextGateway() {
    if (depth > 0) {
      return GatewaysPredictionFactory.anyExcept(previousGateways.subList(0, Math.min(previousGateways.size(), depth)).toArray(new IServerData[Math.min(previousGateways.size(), depth)]));
    } else {
      return GatewaysPredictionFactory.any();
    }
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
