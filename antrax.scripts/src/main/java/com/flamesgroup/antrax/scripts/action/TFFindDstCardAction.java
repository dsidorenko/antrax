/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.action;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.helper.business.PhoneNumberGenerator;
import com.flamesgroup.antrax.helper.business.checkbalance.CheckBalanceHelper;
import com.flamesgroup.antrax.helper.business.checkbalance.USSDCheckBalanceHelper;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "TF Find dst card action", doc = "finds dst card for transfer")
public class TFFindDstCardAction implements ActionProviderScript, RegistryAccessListener {

  private final Logger logger = LoggerFactory.getLogger(TFFindDstCardAction.class);

  public String providesAction = "TFFindDstCardAction";
  protected String eventName = "TFStartTransfer";

  private PhoneNumberGenerator phoneNumberGenerator = new PhoneNumberGenerator("(8050).......");
  private double minimumAmount = 5;
  private CheckBalanceHelper checkBalanceHelper;
  private transient RegistryAccess registry;

  @ScriptParam(name = "event", doc = "name of event to initiate transferEx")
  public void setEvent(final String event) {
    this.eventName = event;
  }

  public String getEvent() {
    return eventName;
  }

  @ScriptParam(name = "action name", doc = "action name")
  public void setAction(final String action) {
    this.providesAction = action;
  }

  public String getAction() {
    return providesAction;
  }

  @ScriptParam(name = "balance check data", doc = "balance checking data. Balance checking may take up to 4 minutes")
  public void setCheckBalanceHelper(final CheckBalanceHelper checkBalanceHelper) {
    this.checkBalanceHelper = checkBalanceHelper;
  }

  public CheckBalanceHelper getCheckBalanceHelper() {
    return new USSDCheckBalanceHelper("*111#", ".*rahunku: (\\d+\\.\\d{2}) ?grn.*");
  }

  @ScriptParam(name = "minimum amount", doc = "Transfer money to this card if it's amount is less than minimum. Put 0 here to discard balance check and transfer by default")
  public void setMinimumAmount(final double amount) {
    this.minimumAmount = amount;
  }

  public double getMinimumAmount() {
    return minimumAmount;
  }

  @ScriptParam(name = "number pattern", doc = "phone number pattern. Pattern consists of numbers, asterisk (any number) and set of number put in []. For example 8(068|063|099)******** will generate one of the 8068, 8063 or 8099 and random number")
  public void setPhoneNumberGenerator(final PhoneNumberGenerator phoneNumberGenerator) {
    this.phoneNumberGenerator = phoneNumberGenerator;
  }

  public PhoneNumberGenerator getPhoneNumberGenerator() {
    return phoneNumberGenerator;
  }

  PhoneNumber extractPhoneNumber(final Action action) {
    Serializable[] args = action.getArgs();
    if (args.length != 1) {
      throw new IllegalArgumentException("Not enough arguments of action");
    }
    String phoneNumber = args[0].toString();
    return new PhoneNumber(phoneNumber);
  }

  @Override
  public boolean canExecuteAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    if (!action.getAction().equals(providesAction)) {
      return false;
    }

    logger.debug("[{}] - search for dst card; Trying {}", this, channel.getSimData().getPhoneNumber());
    if (null == channel.getSimData().getPhoneNumber()) {
      return false;
    }

    //Warning! If check balance regular expression doesn't match with USSD response the request
    //will be repeated in 50-70 seconds and only if it doesn't match second time the Exception will be thrown
    //If check balance timeout is longer than timeout for execution this action and the regular expression is wrong
    //you'll get timeout exception every time but not the real reason of the error.
    logger.debug("[{}] - checking balance value...", this);
    if (minimumAmount < 1 || checkBalanceHelper.getBalanceValue(channel, registry) < minimumAmount) {
      logger.debug("[{}] - found destination card: {}. minimumAmount={}", this, channel.getSimData().getPhoneNumber(), minimumAmount);
      return true;
    }
    logger.debug("[{}] - checked", this);
    return false;
  }

  @Override
  public void executeAction(final Action action, final RegisteredInGSMChannel channel) throws Exception {
    PhoneNumber srcNumber = extractPhoneNumber(action);

    logger.debug("[{}] - initiate transfer money: {} -> {}", this, srcNumber.getValue(), channel.getSimData().getPhoneNumber().getValue());

    //We can't call another action from here because of channel.executeAction() is   synchronous call and that action should be
    //performed by src channel which is currently performing this action. So, in case of usage actions we get deadlock.

    //Using events we can implement asynchronous call. Events don't support parameters.
    //This is a some kind of hack: passing src and dst phone numbers as Event successResponce and failureResponce
    //like action parameters
    GenericEvent event = GenericEvent.checkedEvent(eventName, srcNumber.getValue(), channel.getSimData().getPhoneNumber().getValue());
    event.fireEvent(channel);
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = registry;
  }

  @Override
  public String getProvidedAction() {
    return providesAction;
  }

}
