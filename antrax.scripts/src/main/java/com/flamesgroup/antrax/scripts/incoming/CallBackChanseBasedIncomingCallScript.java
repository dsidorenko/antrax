/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.incoming;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Script(name = "call back chances based", doc = "Uses chances to determine whether to accept or drop incoming calls and generate event to call back to called number")
public class CallBackChanseBasedIncomingCallScript extends ChancesBasedIncomingCallScriptBase {

  private static final long serialVersionUID = 8428690580113136486L;

  private static final Logger logger = LoggerFactory.getLogger(CallBackChanseBasedIncomingCallScript.class);

  private boolean callBackOnlyToAcceptedCalls;
  private boolean callBackOnlyToAnsweredCalls;
  private GenericEvent event = GenericEvent.uncheckedEvent("call_back");

  @ScriptParam(name = "event", doc = "generate event to call back to called number")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  private boolean answered;
  private PhoneNumber incomingNumber;

  @ScriptParam(name = "call back to accepted calls", doc = "generate event to call back only for accepted calls")
  public void setCallBackOnlyToAcceptedCalls(final boolean callBackOnlyToAcceptedCalls) {
    this.callBackOnlyToAcceptedCalls = callBackOnlyToAcceptedCalls;
  }

  public boolean getCallBackOnlyToAcceptedCalls() {
    return callBackOnlyToAcceptedCalls;
  }

  @ScriptParam(name = "call back to answered calls", doc = "generate event to call back only for answered calls")
  public void setCallBackOnlyToAnsweredCalls(final boolean callBackOnlyToAnsweredCalls) {
    this.callBackOnlyToAnsweredCalls = callBackOnlyToAnsweredCalls;
  }

  public boolean getCallBackOnlyToAnsweredCalls() {
    return callBackOnlyToAnsweredCalls;
  }

  @Override
  public void dropAction(final RegisteredInGSMChannel channel) throws Exception {
    super.dropAction(channel);
    if (incomingNumber.isPrivate()) {
      logger.info("[{}] - can't generate event to call back to private number, so only drop incoming call", this);
    } else if (callBackOnlyToAcceptedCalls && !isApplyIncomingCall()) {
      logger.info("[{}] - generate event to call back only for accepted calls, so only drop incoming call", this);
    } else if (callBackOnlyToAnsweredCalls && !answered) {
      logger.info("[{}] - generate event to call back only for answered calls, so only drop incoming call", this);
    } else {
      channel.fireGenericEvent(event.getEvent(), incomingNumber.getValue());
    }
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    super.handleIncomingCall(number);
    this.incomingNumber = number;
  }

  @Override
  public void handleIncomingCallAnswered() {
    super.handleIncomingCallAnswered();
    answered = true;
  }

}
