/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Script(name = "HTTP request", doc = "waits until event occurred and then sends HTTP request to the specified server from registry")
public class HTTPRequest implements BusinessActivityScript, GenericEventListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(HTTPRequest.class);
  
  private final Random random = new Random();

  private String event = "send_http_request";
  private String eventOnFail = "http_request_fail";
  private String eventOnSuccess = "http_request_success";
  private String HTTPServerPattern = "(#HTTPRequest#)(.*)";
  private String apn = "";

  private volatile GenericEvent caughtEvent;
  private transient RegistryAccess registryAccess;

  @ScriptParam(name = "event", doc = "event to send HTTP request")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when HTTP sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on success", doc = "this event will be generated when HTTP sending success")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  @ScriptParam(name = "HTTP server pattern", doc = "path pattern for get server from registry")
  public void setHTTPServerPattern(final String HTTPServerPattern) {
    this.HTTPServerPattern = HTTPServerPattern;
  }

  public String getHTTPServerPattern() {
    return HTTPServerPattern;
  }

  @ScriptParam(name = "apn", doc = "Access Point Name for internet connection")
  public void setApn(final String apn) {
    this.apn = apn;
  }

  public String getApn() {
    return apn;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      if (!channel.getSimData().isAllowInternet()) {
        logger.warn("[{}] - HTTP doesn't allowed", this);
        caughtEvent.respondFailure(channel, "HTTP doesn't allowed");
        fireFailGenericEvent(channel);
        return;
      }
      String[] strings = registryAccess.listAllPaths();
      List<String> paths = Arrays.stream(strings).filter(e -> e.matches(HTTPServerPattern)).collect(Collectors.toList());
      if (paths.isEmpty()) {
        logger.warn("[{}] - Can't find path at registry for pattern: [{}]", this, HTTPServerPattern);
        caughtEvent.respondFailure(channel, "Can't find path at registry for pattern: " + HTTPServerPattern);
        fireFailGenericEvent(channel);
        return;
      }

      String path = paths.get(random.nextInt(paths.size()));

      RegistryEntry[] registryEntries = registryAccess.listEntries(path, 1);
      if (registryEntries.length == 0) {
        logger.warn("[{}] - Can't find registryEntries at registry for path: [{}]", this, path);
        caughtEvent.respondFailure(channel, "Can't find registryEntries at registry for path: " + path);
        fireFailGenericEvent(channel);
        return;
      }
      Matcher matcher = Pattern.compile(HTTPServerPattern).matcher(registryEntries[0].getPath());
      if (!matcher.matches()) {
        logger.warn("[{}] - Can't match pattern: [{}] for path: [{}]", this, HTTPServerPattern, path);
        caughtEvent.respondFailure(channel, "Can't match pattern: " + HTTPServerPattern + " for path: " + path);
        fireFailGenericEvent(channel);
        return;
      }
      if (matcher.groupCount() != 2) {
        logger.warn("[{}] - Pattern: [{}] must consists two groups", this, HTTPServerPattern);
        caughtEvent.respondFailure(channel, "Pattern: " + HTTPServerPattern + " must consists two groups");
        fireFailGenericEvent(channel);
        return;
      }
      String url = matcher.replaceFirst("$2");
      String caCertificate = registryEntries[0].getValue();

      try {
        HTTPResponse httpResponse = channel.sendHTTPRequest(apn, url, caCertificate);
        logger.trace("[{}] - httpStatusCode: [{}], ContentType: [{}], data: [{}]", this, httpResponse.getHttpStatusCode(), httpResponse.getContentType(), httpResponse.getData());
        caughtEvent.respondSuccess(channel);
        if (eventOnSuccess != null && !eventOnSuccess.isEmpty()) {
          channel.fireGenericEvent(eventOnSuccess);
        }
      } catch (Exception e) {
        logger.warn("[{}] - Can't send HTTP request", this, e);
        caughtEvent.respondFailure(channel, "Can't send HTTP request");
        fireFailGenericEvent(channel);
      }

    } finally {
      caughtEvent = null;
    }

  }

  private void fireFailGenericEvent(final RegisteredInGSMChannel channel) {
    if (eventOnFail != null && !eventOnFail.isEmpty()) {
      channel.fireGenericEvent(eventOnFail);
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "send HTTP request";
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registryAccess = registry;
  }

}
