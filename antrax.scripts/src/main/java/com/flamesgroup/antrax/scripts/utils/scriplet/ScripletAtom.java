/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils.scriplet;

import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.helper.editors.ActivityTimeoutByEventScripletEditor;
import com.flamesgroup.antrax.helper.editors.AllowActivityInRandomPeriodScripletEditor;
import com.flamesgroup.antrax.helper.editors.IntervalEditor;
import com.flamesgroup.antrax.helper.editors.LimitCallDurationPerMonthScripletEditor;
import com.flamesgroup.antrax.helper.editors.VariableLongEditor;
import com.flamesgroup.antrax.scripts.utils.ActivityTimeoutByEventScriplet;
import com.flamesgroup.antrax.scripts.utils.AllowActivityInRandomPeriodScriplet;
import com.flamesgroup.antrax.scripts.utils.LimitCallDurationPerMonthScriplet;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

import java.lang.reflect.Method;

public class ScripletAtom extends BaseScriplet {

  private static final long serialVersionUID = 2500105574748692360L;

  private final String script;
  private Class<? extends SimpleActivityScript> clazz;
  private String setter;
  private Object value;
  private transient PropertyEditor<?> editor;

  public ScripletAtom(final String script) {
    this.script = script;
  }

  public ScripletAtom(final String name, final Class<? extends SimpleActivityScript> clazz, final String setter, final Object defaultValue) {
    this.script = name;
    this.clazz = clazz;
    this.setter = setter;
    value = defaultValue;
  }

  private static Method findMethod(final Class<? extends SimpleActivityScript> clazz, final String name) {
    for (Method m : clazz.getDeclaredMethods()) {
      if (m.getName().equals(name)) {
        return m;
      }
    }
    throw new IllegalArgumentException("Can't find method " + name + " in " + clazz);
  }

  private PropertyEditor<?> getEditorForType(final Class<?> type, final Object defaultValue) {
    if (TimeInterval.class == type) {
      IntervalEditor retval = new IntervalEditor();
      retval.setValue((TimeInterval) defaultValue);
      return retval;
    }
    if (VariableLong.class == type) {
      VariableLongEditor retval = new VariableLongEditor();
      retval.setValue((VariableLong) defaultValue);
      return retval;
    }
    if (ActivityTimeoutByEventScriplet.class == type) {
      ActivityTimeoutByEventScripletEditor retval = new ActivityTimeoutByEventScripletEditor();
      retval.setValue((ActivityTimeoutByEventScriplet) defaultValue);
      return retval;
    }
    if (LimitCallDurationPerMonthScriplet.class == type) {
      LimitCallDurationPerMonthScripletEditor retval = new LimitCallDurationPerMonthScripletEditor();
      retval.setValue((LimitCallDurationPerMonthScriplet) defaultValue);
      return retval;
    }
    if (AllowActivityInRandomPeriodScriplet.class == type) {
      AllowActivityInRandomPeriodScripletEditor retval = new AllowActivityInRandomPeriodScripletEditor();
      retval.setValue((AllowActivityInRandomPeriodScriplet) defaultValue);
      return retval;
    }
    throw new UnsupportedOperationException("Unsupported editor type: " + type);
  }

  public String getScript() {
    return script;
  }

  @Override
  public String toString() {
    return script;
  }

  public Object getValue() {
    return value;
  }

  public PropertyEditor<?> getPropertyEditor() {
    if (setter == null) {
      return null;
    }
    if (editor == null) {
      this.editor = getEditorForType(findMethod(clazz, setter).getParameterTypes()[0], value);
    }
    return editor;
  }

  public void setValue(final Object value) {
    this.value = value;
  }

  @Override
  public SimpleActivityScript createActivityScript() throws Exception {
    SimpleActivityScript retval = clazz.newInstance();
    if (setter != null) {
      findMethod(clazz, setter).invoke(retval, value);
    }
    return retval;
  }

}
