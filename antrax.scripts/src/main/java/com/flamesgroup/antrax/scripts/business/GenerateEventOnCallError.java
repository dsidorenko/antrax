/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

@Script(name = "generate event on call error", doc = "generates event on call error cause")
public class GenerateEventOnCallError implements BusinessActivityScript, CallsListener, StatefullScript {
  private static final long serialVersionUID = -6867220672350527579L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventOnCallError.class);

  private int callErrorsLimit;

  private final List<Integer> errorCauses = new LinkedList<>();

  @StateField
  private volatile int callErrorsCount;

  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "event", doc = "event on call error cause")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return event.getEvent();
  }

  @ScriptParam(name = "call errors limit", doc = "limit after which event will be generated")
  public void setCallErrorsLimit(final int callErrorsLimit) {
    this.callErrorsLimit = callErrorsLimit;
  }

  public int getCallErrorsLimit() {
    return callErrorsLimit;
  }

  @ScriptParam(name = "call error cause", doc = "call control connection management cause")
  public void addErrorCause(final int callControlConnectionManagementCause) {
    this.errorCauses.add(callControlConnectionManagementCause);
  }

  public int getErrorCause() {
    return 0;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event on specified call error cause code";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    callErrorsCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return callErrorsLimit > 0 && callErrorsCount >= callErrorsLimit;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (duration > 0) {
      callErrorsCount = 0;
      saver.save();
    }
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
    logger.debug("[{}] - got call error cause {}", this, callControlConnectionManagementCause);
    if (isErrorReportMatch(callControlConnectionManagementCause)) {
      this.callErrorsCount++;
      logger.debug("[{}] - call error '{}' match and increase call error count: {}", this, callControlConnectionManagementCause, callErrorsCount);
      saver.save();
    }
  }

  private boolean isErrorReportMatch(final int callControlConnectionManagementCause) {
    if (errorCauses.isEmpty()) {
      return false;
    }

    for (Integer cause : errorCauses) {
      if (cause == 0 || cause == callControlConnectionManagementCause) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

}
