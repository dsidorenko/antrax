/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;

@Script(name = "count sms by day", doc = "generate event after amount of sent SMS by day")
public class CountSMSByDay implements BusinessActivityScript, SMSListener, StatefullScript {

  private static final long serialVersionUID = -1473791371132966628L;

  private static final Logger logger = LoggerFactory.getLogger(CountSMSByDay.class);

  @StateField
  private volatile long amountLimitValue;
  @StateField
  private volatile long dayAmount;
  @StateField
  private volatile long lastMidnightTimestamp;
  @StateField
  private volatile boolean eventGenerated;

  private VariableLong countLimit = new VariableLong(40, 50);
  private String event = "event";


  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "count limit", doc = "limit amount of sent SMS per day to generate event")
  public void setCountLimit(final VariableLong countLimit) {
    this.countLimit = countLimit;
  }

  public VariableLong getCountLimit() {
    return countLimit;
  }


  @ScriptParam(name = "event", doc = "this event will be generated when exceeded count limit value")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @Override
  public String describeBusinessActivity() {
    return "count sms by day";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    eventGenerated = true;
    saver.save();
    channel.fireGenericEvent(event);
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (getCurrentMidnight() > lastMidnightTimestamp) {
      recalcParams();
      saver.save();
      logger.debug("[{}] - new day", this);
    }

    return !eventGenerated && dayAmount >= amountLimitValue;
  }

  private void recalcParams() {
    lastMidnightTimestamp = getCurrentMidnight();
    amountLimitValue = countLimit.random();
    logger.debug("[{}] - durationLimitValue set to {}", this, amountLimitValue);
    dayAmount = 0;
    eventGenerated = false;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    dayAmount += parts;
    saver.save();
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  private long getCurrentMidnight() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    return cal.getTimeInMillis();
  }

}
