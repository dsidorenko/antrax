/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import java.io.Serializable;

public class TimePeriod implements Serializable {

  private static final long serialVersionUID = 1029124687806960437L;

  private final long period;

  public long getPeriod() {
    return period;
  }

  public TimePeriod(final long millis) {
    this.period = millis;
  }

  public static long inHours(final int hours) {
    return hours * inMinutes(60);
  }

  public static long inMinutes(final int minutes) {
    return minutes * inSeconds(60);
  }

  public static long inSeconds(final int seconds) {
    return seconds * inMillis(1000);
  }

  public static long inMillis(final long milliseconds) {
    return milliseconds;
  }

  @Override
  public String toString() {
    return TimeUtils.writeTime(period);
  }

  public static TimePeriod parseTimePeriod(final String period) {
    long[] k = {60 * 60 * 1000, 60 * 1000, 1000, 1};
    int index = 0;
    long res = 0;
    for (String tok : period.split("[:.]")) {
      res += Integer.parseInt(tok) * k[index++];
    }
    return new TimePeriod(res);
  }

}
