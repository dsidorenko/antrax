/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;

import java.io.Serializable;

@Script(name = "allow/disallow internet", doc = "allow and disallow internet possibility")
public class AllowDisallowInternet implements BusinessActivityScript, GenericEventListener {

  private String allowEvent = "allowEvent";
  private String disallowEvent = "disallowEvent";

  private volatile GenericEvent caughtAllowedEvent;
  private volatile GenericEvent caughtDisallowedEvent;

  public String getAllowEvent() {
    return allowEvent;
  }

  @ScriptParam(name = "allowEvent", doc = "event to change status to allow")
  public void setAllowEvent(final String allowEvent) {
    this.allowEvent = allowEvent;
  }

  public String getDisallowEvent() {
    return disallowEvent;
  }

  @ScriptParam(name = "disallowEvent", doc = "event to change status to disallow")
  public void setDisallowEvent(final String disallowEvent) {
    this.disallowEvent = disallowEvent;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.allowEvent.equals(event)) {
      caughtAllowedEvent = GenericEvent.wrapEvent(event, args);
    }
    if (this.disallowEvent.equals(event)) {
      caughtDisallowedEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      if (caughtAllowedEvent != null) {
        channel.changeAllowedInternet(true);
        caughtAllowedEvent.respondSuccess(channel);
      }
      if (caughtDisallowedEvent != null) {
        channel.changeAllowedInternet(false);
        caughtDisallowedEvent.respondSuccess(channel);
      }
    } finally {
      caughtAllowedEvent = null;
      caughtDisallowedEvent = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtAllowedEvent != null || caughtDisallowedEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return null;
  }

}
