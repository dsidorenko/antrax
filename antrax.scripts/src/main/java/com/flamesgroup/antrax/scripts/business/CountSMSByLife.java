/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

@Script(name = "count sms by life", doc = "generate event after amount of sent SMS by life")
public class CountSMSByLife implements BusinessActivityScript, SMSListener, StatefullScript {

  private static final long serialVersionUID = -1473791371132966628L;

  @StateField
  private volatile long amountLimitValue;
  @StateField
  private volatile long amount;

  @StateField
  private volatile boolean eventGenerated;

  private VariableLong countLimit = new VariableLong(40, 50);
  private String event = "event";


  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "count limit", doc = "limit amount of sent SMS to generate event")
  public void setCountLimit(final VariableLong countLimit) {
    this.countLimit = countLimit;
    amountLimitValue = countLimit.random();
  }

  public VariableLong getCountLimit() {
    return countLimit;
  }


  @ScriptParam(name = "event", doc = "this event will be generated when exceeded count limit value")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @Override
  public String describeBusinessActivity() {
    return "count sms by life";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    eventGenerated = true;
    saver.save();
    channel.fireGenericEvent(event);
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return !eventGenerated && amount >= amountLimitValue;
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    amount += parts;
    saver.save();
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

}
