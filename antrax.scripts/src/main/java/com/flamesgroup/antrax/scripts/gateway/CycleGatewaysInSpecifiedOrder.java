/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.gateway;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.predictions.IncludeGatewayPrediction;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.util.ArrayList;
import java.util.List;

@Script(name = "specified order", doc = "orders gateways in specified order")
public class CycleGatewaysInSpecifiedOrder implements GatewaySelectorScript, StatefullScript, SessionListener {

  private static final long serialVersionUID = -6386708641308692283L;

  private final List<String> gateways = new ArrayList<>();

  @StateField
  public volatile int index = 0;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "gateway", doc = "specified gateway")
  public void addGateway(final String gateway) {
    this.gateways.add(gateway);
  }

  public String getGateway() {
    return null;
  }

  @Override
  public boolean isGatewayApplies(final IServerData gateway) {
    return gateway.getName().equals(gateways.get(index % gateways.size()));
  }

  @Override
  public Prediction predictNextGateway() {
    return new IncludeGatewayPrediction(gateways.get(index % gateways.size()));
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
    index++;
    saver.save();
  }

  @Override
  public void handleSessionEnded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
