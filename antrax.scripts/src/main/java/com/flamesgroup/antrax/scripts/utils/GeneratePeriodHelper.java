/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

public class GeneratePeriodHelper {

  private GeneratePeriodHelper() {
  }

  public static long generatePeriod(final long begin, final long end, final long current, final long limit, final long generated) {
    if (generated > limit) {
      return 0;
    }

    long currentLocal = current;
    long generatedLocal = generated;
    long step = (end - begin) / limit;
    long shouldGenerated = ((current - begin) / step) + 1;

    if (generated < shouldGenerated) {
      generatedLocal = shouldGenerated;
    } else if (generated == shouldGenerated) {
      currentLocal = begin + generated * step;
      generatedLocal = generated + 1;
    }

    return random(currentLocal, begin + generatedLocal * step);
  }

  private static long random(final long min, final long max) {
    return min + Math.round(Math.random() * ((max - min) + 1));
  }

}
