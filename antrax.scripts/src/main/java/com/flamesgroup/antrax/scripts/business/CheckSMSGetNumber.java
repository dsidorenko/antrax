/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Script(name = "check SMS get number", doc = "analyzes incoming SMS to get phone number from text and them set it to sim card")
public class CheckSMSGetNumber implements BusinessActivityScript, SMSListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(CheckSMSGetNumber.class);

  private String event = "check_sms_get_number";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));
  private String pattern = ".*(\\d+).*";
  private String eventOnSuccess = "get_number_successful";
  private String eventOnFail = "sms_failed";
  private String response;

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private final AtomicLong smsTime = new AtomicLong();

  @ScriptParam(name = "event", doc = "check sms will be started after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  public String getPattern() {
    return pattern;
  }

  @ScriptParam(name = "SMS response pattern", doc = "pattern for checking SMS response")
  public void setPattern(final String pattern) {
    this.pattern = pattern;
  }

  @ScriptParam(name = "even on success", doc = "this event will be generated if get number from SMS was successful")
  public void setEventOnSuccess(final String eventOnSuccess) {
    this.eventOnSuccess = eventOnSuccess;
  }

  public String getEventOnSuccess() {
    return eventOnSuccess;
  }

  public String getEventOnFail() {
    return eventOnFail;
  }

  @ScriptParam(name = "event on fail", doc = "this event will be generated when SMS sending failed")
  public void setEventOnFail(final String eventOnFail) {
    this.eventOnFail = eventOnFail;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (response == null) {
      smsTime.set(0);
      channel.fireGenericEvent(eventOnFail);
      logger.debug("[{}] - waiting sms timeout", this);
      return;
    }

    logger.debug("[{}] - get number from sms text [{}]", this, response);
    try {
      PhoneNumber phoneNumber = new PhoneNumber(response.replaceFirst(pattern, "$1"));
      channel.updatePhoneNumber(phoneNumber);
      if (eventOnSuccess != null && !eventOnSuccess.isEmpty()) {
        channel.fireGenericEvent(eventOnSuccess);
      }
    } catch (Exception e) {
      logger.debug("[{}] - incorrect response [{}], cause: {}", this, response, e.getMessage());
      channel.fireGenericEvent(eventOnFail);
    } finally {
      smsTime.set(0);
      response = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (response != null) {
      return true;
    } else if (smsWait.get() && System.currentTimeMillis() - smsTime.get() > smsTimeout.getPeriod()) {
      smsWait.set(false);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && text != null && text.matches(pattern)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches with pattern [{}]", this, phoneNumber, text, pattern);
      smsWait.set(false);
      response = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public String describeBusinessActivity() {
    return "check SMS get number";
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - start waiting to receive sms with number", this);
      smsWait.set(true);
      smsTime.set(System.currentTimeMillis());
    }
  }

}
