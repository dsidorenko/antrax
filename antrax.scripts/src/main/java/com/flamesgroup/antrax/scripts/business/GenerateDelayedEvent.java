/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.GenerousSleepHelper;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "generate event after timeout", doc = "generates event after some timeout left")
public class GenerateDelayedEvent implements BusinessActivityScript, GenericEventListener, StatefullScript {

  private static final Logger logger = LoggerFactory.getLogger(GenerateDelayedEvent.class);

  private static final long serialVersionUID = -2443987037328286497L;

  private static final String FAILURE_RESPONCE_PREFIX = " failure ";
  private static final String SUCCESS_RESPONCE_PREFIX = " successed ";

  private volatile GenericEvent eventCaugth;

  @StateField
  private boolean lock = false;

  private TimePeriod delayTimeout = new TimePeriod(30000);

  private String receiveEvent = "inEvent";

  private GenericEvent sendEvent = GenericEvent.checkedEvent("outEvent", successedResponse("outEvent"), failureResponse("outEvent"));

  private boolean lockOnFailure;

  @StateField
  private volatile String lockReason;

  private final ScriptSaver saver = new ScriptSaver();

  private boolean greedy = true;

  private volatile GenerousSleepHelper generousSleep = null;

  @ScriptParam(name = "receive event", doc = "sendEvent generatates after this event")
  public void setReceiveEvent(final String receiveEvent) {
    if (receiveEvent == null) {
      throw new NullPointerException();
    }
    this.receiveEvent = receiveEvent;
  }

  public String getReceiveEvent() {
    return receiveEvent;
  }

  @ScriptParam(name = "send event", doc = "this event generates after timeout")
  public void setSendEvent(final String event) {
    this.sendEvent = GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event));
  }

  public String getSendEvent() {
    return sendEvent.getEvent();
  }

  @ScriptParam(name = "delayTimeout", doc = "timeout to delay event")
  public void setTimeout(final TimePeriod delayTimeout) {
    this.delayTimeout = delayTimeout;
  }

  public TimePeriod getTimeout() {
    return delayTimeout;
  }

  @ScriptParam(name = "greedy script", doc = "greedy script will exclusively take sim card")
  public void setGreedy(final boolean greedy) {
    this.greedy = greedy;
  }

  public boolean getGreedy() {
    return greedy;
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @Override
  public String describeBusinessActivity() {
    return "generates " + sendEvent.getEvent() + " after " + delayTimeout.toString();
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (lock) {
      try {
        if (lockReason != null) {
          channel.lock(lockReason);
        } else {
          channel.lock("failedToExecute " + sendEvent.getEvent());
        }
      } finally {
        lock = false;
        lockReason = null;
        saver.save();
      }
    } else if (generousSleep != null) {
      if (generousSleep.isPeriodFinished()) {
        generousSleep = null;
        saver.save();
        sendEvent.fireEvent(channel);
      }
    } else {
      try {
        if (greedy) {
          Thread.sleep(delayTimeout.getPeriod());
          sendEvent.fireEvent(channel);
        } else {
          generousSleep = new GenerousSleepHelper(delayTimeout);
          generousSleep.startSleepPeriod();
          saver.save();
        }
      } finally {
        eventCaugth = null;
        saver.save();
      }
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return lock || eventCaugth != null || (generousSleep != null && generousSleep.isPeriodFinished());
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.receiveEvent.equals(event)) {
      if (generousSleep != null) {
        logger.debug("[{}] - wait to finish previous event {}, so ignore it", this, event);
        return;
      }

      eventCaugth = GenericEvent.wrapEvent(event);
      saver.save();
      return;
    }

    if (this.sendEvent.isFailureResponce(event, args)) {
      if (lockOnFailure) {
        lock = true;
        lockReason = sendEvent.getFailReason(event, args);
        saver.save();
      }
    }
  }

  private String successedResponse(final String event) {
    return event + SUCCESS_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + FAILURE_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
