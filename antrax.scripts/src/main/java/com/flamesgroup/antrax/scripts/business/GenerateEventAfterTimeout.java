/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;

import java.io.Serializable;
import java.util.Calendar;

@Script(name = "generate event after timeout passing", doc = "generate event after timeout passing")
public class GenerateEventAfterTimeout implements BusinessActivityScript, GenericEventListener, StatefullScript {

  private static final long serialVersionUID = 6416947154325342754L;

  private static final String FAILURE_RESPONSE_PREFIX = " failure ";
  private static final String SUCCESS_RESPONSE_PREFIX = " success ";

  @StateField
  private boolean lock;
  @StateField
  private long periodBeginning;

  private GenericEvent sendEvent = GenericEvent.checkedEvent("randomEvent", successResponse("randomEvent"), failureResponse("randomEvent"));

  private boolean lockOnFailure;
  private TimePeriod timeout = new TimePeriod(TimePeriod.inHours(1));

  @StateField
  private volatile String lockReason;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "send event", doc = "this event generates randomly")
  public void setSendEvent(final String event) {
    this.sendEvent = GenericEvent.checkedEvent(event, successResponse(event), failureResponse(event));
  }

  public String getSendEvent() {
    return sendEvent.getEvent();
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @ScriptParam(name = "timeout", doc = "timeout after which an event will be generated")
  public void setTimeout(final TimePeriod timeout) {
    this.timeout = timeout;
  }

  public TimePeriod getTimeout() {
    return timeout;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event after timeout";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (lock) {
      try {
        if (lockReason != null) {
          channel.lock(lockReason);
        } else {
          channel.lock("failedToExecute " + sendEvent.getEvent());
        }
      } finally {
        lock = false;
        lockReason = null;
      }
    } else {
      periodBeginning = 0;
      sendEvent.fireEvent(channel);
    }

    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return lock || shouldGenerateEvent();
  }

  private boolean shouldGenerateEvent() {
    long currentTime = getTimeOfDay();
    if (periodBeginning <= 0) {
      periodBeginning = currentTime;
      saver.save();
    }

    return currentTime - periodBeginning > timeout.getPeriod();
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.sendEvent.isFailureResponce(event, args)) {
      if (lockOnFailure) {
        lock = true;
        lockReason = sendEvent.getFailReason(event, args);
        saver.save();
      }
    }
  }

  private long getTimeOfDay() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_YEAR, 1);
    cal.clear(Calendar.MONTH);
    cal.clear(Calendar.YEAR);
    cal.set(Calendar.ZONE_OFFSET, 0);
    return cal.getTimeInMillis();
  }

  private String successResponse(final String event) {
    return event + SUCCESS_RESPONSE_PREFIX + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + FAILURE_RESPONSE_PREFIX + Math.random() + System.nanoTime();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
