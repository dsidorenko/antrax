/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.CallsListener;
import com.flamesgroup.antrax.automation.listeners.IncomingCallsListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.unit.PhoneNumber;

@Script(name = "generate event on defined duration calls", doc = "generates event when count of calls with duration [leftBound; rightBound] is greater than maximumCallsCount")
public class GenerateEventOnDefinedDurationCalls implements BusinessActivityScript, CallsListener, StatefullScript, IncomingCallsListener {
  private static final long serialVersionUID = 682802887695914442L;

  private GenericEvent event = GenericEvent.uncheckedEvent("lockMe");
  @StateField
  private int callsCount;
  private int maximumCallsCount = 5;
  private boolean countConsecutiveCalls = true;
  private TimeInterval durationBounds = new TimeInterval(TimePeriod.inSeconds(0), TimePeriod.inSeconds(12));
  private boolean useIncomingCalls = false;
  private boolean useOutgoingCalls = true;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "duration bounds", doc = "set call duration bounds to generate event")
  public void setDurationBounds(final TimeInterval durationBounds) {
    this.durationBounds = durationBounds;
  }

  public TimeInterval getDurationBounds() {
    return durationBounds;
  }

  @ScriptParam(name = "maximum calls", doc = "count of calls to generate event")
  public void setMaximumCallsCount(final int maximumCallsCount) {
    this.maximumCallsCount = maximumCallsCount;
  }

  public int getMaximumCallsCount() {
    return maximumCallsCount;
  }

  @ScriptParam(name = "event", doc = "event on calls")
  public void setEvent(final String event) {
    this.event = GenericEvent.uncheckedEvent(event);
  }

  public String getEvent() {
    return this.event.getEvent();
  }

  @ScriptParam(name = "count consecutive calls", doc = "if option is active it will count consecutive calls")
  public void setCountConsecutiveCalls(final boolean countConsecutiveCalls) {
    this.countConsecutiveCalls = countConsecutiveCalls;
  }

  public boolean getCountConsecutiveCalls() {
    return countConsecutiveCalls;
  }

  @ScriptParam(name = "use incoming calls", doc = "use duration of incoming calls at counting")
  public void setUseIncomingCalls(final boolean useIncomingCalls) {
    this.useIncomingCalls = useIncomingCalls;
  }

  public boolean getUseIncomingCalls() {
    return useIncomingCalls;
  }

  @ScriptParam(name = "use outgoing calls", doc = "use duration of outgoing calls at counting")
  public void setUseOutgoingCalls(final boolean useOutgoingCalls) {
    this.useOutgoingCalls = useOutgoingCalls;
  }

  public boolean getUseOutgoingCalls() {
    return useOutgoingCalls;
  }

  @Override
  public String describeBusinessActivity() {
    return "event on duration calls";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    event.fireEvent(channel);
    callsCount = 0;
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return callsCount >= maximumCallsCount;
  }

  @Override
  public void handleCallEnd(final long duration, final int causeCode) {
    if (useOutgoingCalls) {
      calculateCallsCount(duration);
    }
  }

  @Override
  public void handleCallError(final int callControlConnectionManagementCause) {
  }

  @Override
  public void handleDialError(final String errorStatus) {
  }

  @Override
  public void handleCallStart(final PhoneNumber phoneNumber) {
  }

  @Override
  public void handleFAS() {
  }

  @Override
  public void handleCallForwarded() {
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleCallSetup(final PhoneNumber phoneNumber) {

  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
  }

  @Override
  public void handleIncomingCallAnswered() {
  }

  @Override
  public void handleIncomingCallDropped(final long callDuration) {
    if (useIncomingCalls) {
      calculateCallsCount(callDuration);
    }
  }

  private void calculateCallsCount(final long duration) {
    if (duration >= durationBounds.getMin().getPeriod() && duration <= durationBounds.getMax().getPeriod()) {
      callsCount++;
    } else if (countConsecutiveCalls) {
      callsCount = 0;
    }
    saver.save();
  }

}
