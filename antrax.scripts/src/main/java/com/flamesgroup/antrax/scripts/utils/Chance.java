/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.utils;

import java.io.Serializable;

public class Chance implements Serializable {

  private static final long serialVersionUID = 8078105861971218398L;

  int chance = 0;

  public Chance(final int chance) {
    if (chance < 0 || chance > 100) {
      throw new IllegalArgumentException("chance should be in range 0..100");
    }
    this.chance = chance;
  }

  public int getChance() {
    return chance;
  }

  public boolean countChance() {
    return Math.random() < (chance / 100.0d);
  }

  @Override
  public String toString() {
    return chance + "%";
  }

}
