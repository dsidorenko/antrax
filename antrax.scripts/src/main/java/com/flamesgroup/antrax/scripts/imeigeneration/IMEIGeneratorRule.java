/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.imeigeneration;

import com.flamesgroup.antrax.scripts.utils.CalendarPeriod;

import java.io.Serializable;

public class IMEIGeneratorRule implements Serializable {

  private static final long serialVersionUID = 971094592419342270L;

  private IMEIGeneratorRuleType type = IMEIGeneratorRuleType.NONE;
  private CalendarPeriod period = CalendarPeriod.HOUR;
  private int registrationCount = 0;

  public IMEIGeneratorRuleType getType() {
    return type;
  }

  public void setType(final IMEIGeneratorRuleType type) {
    this.type = type;
  }

  public CalendarPeriod getPeriod() {
    return period;
  }

  public void setPeriod(final CalendarPeriod period) {
    this.period = period;
  }

  public int getRegistrationCount() {
    return registrationCount;
  }

  public void setRegistrationCount(final int registrationCount) {
    this.registrationCount = registrationCount;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder(type.toString());
    switch (type) {
      case NONE:
        break;
      case PERIOD:
        builder.append(String.format(": %s", period));
        break;
      case REGISTRATION_COUNT:
        builder.append(String.format(": %d", registrationCount));
        break;
    }
    return builder.toString();
  }

}
