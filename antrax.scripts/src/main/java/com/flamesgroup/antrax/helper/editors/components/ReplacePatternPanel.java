/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.helper.business.ReplacePattern;

import java.awt.*;

import javax.swing.*;

public class ReplacePatternPanel extends JPanel {

  private static final long serialVersionUID = -2237568232087637203L;

  private final JTextField pattern;
  private final JTextField replaceBy;
  private final JCheckBox findSubstring;

  public ReplacePatternPanel() {
    pattern = new JTextField();
    replaceBy = new JTextField();
    findSubstring = new JCheckBox();
    JLabel patternLabel = new JLabel("Pattern");
    patternLabel.setLabelFor(pattern);
    JLabel replaceByLabel = new JLabel("ReplaceBy");
    replaceByLabel.setLabelFor(replaceBy);
    JLabel findSubstringLabel = new JLabel("FindSubstring");
    findSubstringLabel.setLabelFor(findSubstring);
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    pattern.setEditable(true);
    replaceBy.setEditable(true);

    patternLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    add(patternLabel);
    add(pattern);
    replaceByLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    add(replaceByLabel);
    add(replaceBy);
    findSubstringLabel.setAlignmentX(JLabel.LEFT_ALIGNMENT);
    add(findSubstringLabel);
    add(findSubstring);
    this.setPreferredSize(new Dimension(400, this.getPreferredSize().height));
  }

  private String getPattern() {
    String patternString = pattern.getText();
    if (patternString == null) {
      return "";
    }
    return patternString;
  }

  private String getReplaceBy() {
    String eventString = replaceBy.getText();
    if (eventString == null) {
      return "";
    }
    return eventString;
  }

  private boolean getFindSubstring() {
    return findSubstring.isSelected();
  }

  public ReplacePattern getReplacePattern() {
    return new ReplacePattern(getPattern(), getReplaceBy(), getFindSubstring());
  }

  public void setReplacePattern(final ReplacePattern replacePattern) {
    pattern.setText(replacePattern.getPattern());
    replaceBy.setText(replacePattern.getReplaceBy());
    findSubstring.setSelected(replacePattern.isFindSubstring());
  }

}
