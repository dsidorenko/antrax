/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.commons.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public abstract class VaucherHelper {

  private static final Logger logger = LoggerFactory.getLogger(VaucherHelper.class);

  private static final int MAX_QUERY_SIZE = 100;
  private static final long TAKE_EXPIRE_TIMEOUT = TimePeriod.inMinutes(5);

  private final RegistryAccess access;
  private final int amount;
  private final String operator;
  private final List<Pair<String, String>> vaucherTemplatePaths;

  protected abstract boolean executePayment(String vaucher) throws Exception;

  public VaucherHelper(final String operator, final RegistryAccess access, final int moneyAmount, final boolean skipIntermStates) {
    this.access = access;
    this.amount = moneyAmount;
    this.operator = operator;

    List<Pair<String, String>> vaucherTemplatePaths = new LinkedList<>();
    if (skipIntermStates) {
      vaucherTemplatePaths.add(new Pair<>("%s.%d.new", "%s.%d.bad"));
    } else {
      vaucherTemplatePaths.add(new Pair<>("%s.%d.failed.3", "%s.%d.bad"));
      vaucherTemplatePaths.add(new Pair<>("%s.%d.failed.2", "%s.%d.failed.3"));
      vaucherTemplatePaths.add(new Pair<>("%s.%d.failed.1", "%s.%d.failed.2"));
      vaucherTemplatePaths.add(new Pair<>("%s.%d.new", "%s.%d.failed.1"));
    }
    this.vaucherTemplatePaths = vaucherTemplatePaths;
  }

  public void pay() throws Exception {
    logger.debug("[{}] - going to pay", this);
    List<Pair<String, String>> takenPaths = new LinkedList<>();
    for (Pair<String, String> path : vaucherTemplatePaths) {
      takenPaths.add(path(path.first() + ".taken", path.first()));
    }
    releaseOldTaken(takenPaths);

    Exception exception = null;
    for (Pair<String, String> path : vaucherTemplatePaths) {
      try {
        if (executePayment(resolve(path.first()), resolve(path.second()), resolve("%s.%d.used"))) {
          return;
        }
      } catch (UnsyncRegistryEntryException e) {
        logger.warn("[{}] - failed to move registry entry", this, e);
      } catch (Exception e) {
        exception = e;
      }
    }
    if (exception != null) {
      throw exception;
    }
    logger.warn("[{}] - failed to found vauchers", this);
    throw new PaymentError("NoVauchersLeft");
  }

  private void releaseOldTaken(final List<Pair<String, String>> paths) {
    logger.debug("[{}] - releasing old taken vauchers", this);
    for (Pair<String, String> e : paths) {
      releaseOldTaken(e.first(), e.second(), access);
    }
  }

  private boolean executePayment(final String path, final String onFail, final String onSuccess) throws Exception {
    logger.debug("[{}] - executing payment, searching for voucher in {}", this, path);
    for (RegistryEntry e : access.listEntries(path, MAX_QUERY_SIZE)) {
      RegistryEntry moved;
      try {
        moved = access.move(e, e.getPath() + ".taken");
      } catch (UnsyncRegistryEntryException e1) {
        logger.warn("[{}] - while moving path [{}] to taken", this, path, e);
        continue;
      }

      boolean result;
      try {
        result = executePayment(moved.getValue());
      } catch (Exception e1) {
        logger.warn("[{}] - move payment [{}] from [{}] to [{}] by exception", this, moved.getValue(), moved.getPath(), onFail, e1);
        access.move(moved, onFail);
        throw e1;
      }

      if (result) {
        logger.warn("[{}] - move payment [{}] from [{}] to [{}] on success", this, moved.getValue(), moved.getPath(), onSuccess);
        access.move(moved, onSuccess);
      } else {
        logger.warn("[{}] - move payment [{}] from [{}] to [{}] on fail", this, moved.getValue(), moved.getPath(), onFail);
        access.move(moved, onFail);
      }
      return result;
    }
    logger.debug("[{}] - no vouchers at {}", this, path);
    return false;
  }

  private void releaseOldTaken(final String takenPath, final String clearPath, final RegistryAccess access) {
    for (RegistryEntry e : access.listEntries(takenPath, MAX_QUERY_SIZE)) {
      if (System.currentTimeMillis() - e.getUpdatingTime().getTime() > TAKE_EXPIRE_TIMEOUT) {
        try {
          access.move(e, clearPath);
        } catch (UnsyncRegistryEntryException | DataModificationException e1) {
          logger.warn("[{}] - failed to move path {}", this, e, e1);
        }
      }
    }
  }

  private Pair<String, String> path(final String first, final String second) {
    return new Pair<>(resolve(first), resolve(second));
  }

  private String resolve(final String path) {
    return String.format(path, operator, amount);
  }

}
