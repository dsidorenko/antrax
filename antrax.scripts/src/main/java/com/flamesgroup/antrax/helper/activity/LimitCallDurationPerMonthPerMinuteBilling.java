/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.helper.activity.api.BaseLimitCharacteristicPerMonth;
import com.flamesgroup.antrax.predictions.CallDurationPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import com.flamesgroup.antrax.storage.commons.impl.SimData;

public class LimitCallDurationPerMonthPerMinuteBilling extends BaseLimitCharacteristicPerMonth {

  private static final long serialVersionUID = 8521262400813739773L;

  @StateField
  private long roundCallDuration;
  private long callDuration;

  public void setCallDurationLimit(final TimeInterval callDurationLimit) {
    setLimitValue(new VariableLong(callDurationLimit.getMin().getPeriod(), callDurationLimit.getMax().getPeriod()));
  }

  @Override
  protected long getCharacteristicValue(final SimData simData) {
    if (roundCallDuration == 0) {
      roundCallDuration = simData.getCallDuration();
      getScriptSaver().save();
    }

    if (callDuration == 0) {
      callDuration = simData.getCallDuration();
    }

    if (callDuration != simData.getCallDuration()) { // was a call
      long duration = simData.getCallDuration() - callDuration; // last call duration
      long roundDuration = duration;
      long mod = duration % 60_000;
      if (mod != 0) {
        roundDuration = duration + 60_000 - mod;
      }

      roundCallDuration += roundDuration;
      callDuration = simData.getCallDuration();
      getScriptSaver().save();
    }

    return roundCallDuration;
  }

  @Override
  protected Prediction getCharacteristicPrediction(final long value) {
    return new CallDurationPrediction(value);
  }

}
