/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import com.flamesgroup.unit.PhoneNumber;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Generates phone number using generation pattern:
 * <p>
 * <pre>
 * . - any number,
 * \. - .,
 * [2579] - set of numbers,
 * \[ - [
 * 1 - 1
 * ( - start of block
 * ) - end of block
 * | - one of the values in current block
 * </pre>
 */
public class PhoneNumberGenerator implements Serializable {

  private static final long serialVersionUID = 8233868706380441444L;

  private final Generator generator;
  private final String pattern;

  public PhoneNumberGenerator(final String pattern) {
    this.pattern = pattern;
    this.generator = compile(pattern);
  }

  private Generator compile(final String pattern) {
    Lexer lex = new Lexer(pattern);
    Generator chain = parseBlock(lex);
    if (!lex.isEof()) {
      throw new IllegalArgumentException("Unmatched )");
    }
    return chain;
  }

  public Generator parseBlock(final Lexer lex) {
    List<Generator> retval = new LinkedList<>();
    loop:
    while (!lex.isEof()) {
      char c = lex.nextChar();
      switch (c) {
        case '[':
          parseSet(retval, lex);
          break;
        case '.':
          retval.add(new AnyNumberGenerator());
          break;
        case '\\':
          retval.add(parseEscape(lex));
          break;
        case '(':
          retval.add(parseBlock(lex));
          break;
        case ')':
          break loop;
        case '|':
          Generator[] right = collapseVariants(parseBlock(lex));
          Generator left = new GeneratorsChain(retval.toArray(new Generator[retval.size()]));
          return new GeneratorsVariant(left, right);
        default:
          retval.add(new ConcreteNumberGenerator(c));
          break;
      }
    }
    return new GeneratorsChain(retval.toArray(new Generator[retval.size()]));
  }

  private Generator[] collapseVariants(final Generator variants) {
    if (variants instanceof GeneratorsVariant) {
      return ((GeneratorsVariant) variants).generators;
    }
    return new Generator[] {variants};
  }

  private void parseSet(final List<Generator> retval, final Lexer lex) {
    StringBuilder buf = new StringBuilder();
    while (!lex.isEof()) {
      char c = lex.nextChar();
      if (c == ']') {
        break;
      }
      buf.append(c);
    }
    if (buf.length() > 0) {
      retval.add(new OneOfNumberGenerator(buf.toString()));
    }
  }

  private Generator parseEscape(final Lexer lex) {
    if (lex.isEof()) {
      return new ConcreteNumberGenerator('\\');
    }
    return new ConcreteNumberGenerator(lex.nextChar());
  }

  public PhoneNumber generate() {
    return new PhoneNumber(generator.generate(new Random()));
  }

  @Override
  public String toString() {
    return pattern;
  }

  public static PhoneNumberGenerator createGeneratorUsingCodes(final String[] codes) {
    int maxCodeLength = determineMaxLength(codes);
    StringBuilder buf = new StringBuilder();
    buf.append("(");
    for (String c : codes) {
      buf.append(c);
      for (int i = maxCodeLength - c.length(); i > 0; --i) {
        buf.append(".");
      }
      buf.append("|");
    }
    buf.replace(buf.length() - 1, buf.length(), ")");
    for (int i = 11; i > maxCodeLength; --i) {
      buf.append(".");
    }
    return new PhoneNumberGenerator(buf.toString());
  }

  private static int determineMaxLength(final String[] codes) {
    int retval = 0;
    for (String v : codes) {
      int size = v.length();
      if (size > retval) {
        retval = size;
      }
    }
    return retval;
  }

  private interface Generator extends Serializable {
    String generate(Random rand);
  }

  private static class GeneratorsChain implements Generator {

    private static final long serialVersionUID = 6127405986272343L;

    private final Generator[] generators;

    public GeneratorsChain(final Generator[] generators) {
      this.generators = generators;
    }

    @Override
    public String generate(final Random rand) {
      StringBuilder buf = new StringBuilder();
      for (Generator g : generators) {
        buf.append(g.generate(rand));
      }
      return buf.toString();
    }

  }

  private static class GeneratorsVariant implements Generator {
    private static final long serialVersionUID = 4622697181022843709L;
    private final Generator[] generators;

    private GeneratorsVariant(final Generator left, final Generator[] right) {
      this.generators = new Generator[right.length + 1];
      generators[0] = left;
      System.arraycopy(right, 0, generators, 1, right.length);
    }

    @Override
    public String generate(final Random rand) {
      return generators[rand.nextInt(generators.length)].generate(rand);
    }
  }

  private static class AnyNumberGenerator implements Generator {
    private static final long serialVersionUID = -7095002896045039015L;

    @Override
    public String generate(final Random rand) {
      return "" + Integer.toString(rand.nextInt(10)).charAt(0);
    }
  }

  private static class ConcreteNumberGenerator implements Generator {
    private static final long serialVersionUID = 7496132001858385442L;
    private final char number;

    public ConcreteNumberGenerator(final char number) {
      this.number = number;
    }

    @Override
    public String generate(final Random rand) {
      return "" + number;
    }
  }

  private static class OneOfNumberGenerator implements Generator {
    private static final long serialVersionUID = 8002350128845708322L;
    private final String set;

    public OneOfNumberGenerator(final String set) {
      this.set = set;
    }

    @Override
    public String generate(final Random rand) {
      return "" + set.charAt(rand.nextInt(set.length()));
    }
  }

  private static class Lexer {
    int index = 0;
    String str;

    public Lexer(final String str) {
      this.str = str;
    }

    public char nextChar() {
      return str.charAt(index++);
    }

    public boolean isEof() {
      return index >= str.length();
    }
  }

}
