/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.scriplet;

import com.flamesgroup.antrax.helper.editors.ActivityScripletEditor;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletOperation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class OperationComponent extends JPanel {

  private static final long serialVersionUID = 924751120734288449L;

  private final JLabel lblOperation = ActivityScripletEditor.createLabel("", true);
  private final JButton btnEdit = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.EDIT_ICON));
  private final JButton btnJoin;
  private final JButton btnSplit;
  private JToolBar toolbar;
  private final ActivityScriplet right;
  private final ActivityScriplet left;
  private final ScripletBlock parent;
  private final ActivityScripletEditor editor;
  private final JPopupMenu popupMenu;
  private JRadioButtonMenuItem radioBtnAnd;
  private JRadioButtonMenuItem radioBtnOr;

  public OperationComponent(final ScripletBlock parent, final ActivityScriplet left, final ActivityScriplet right, final ScripletOperation operation,
      final ActivityScripletEditor editor) {
    super(new BorderLayout());
    this.parent = parent;
    this.right = right;
    this.left = left;
    this.editor = editor;
    setOpaque(true);

    if (parent.canJoin()) {
      btnJoin = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.JOIN_ICON));
      btnSplit = null;
    } else if (parent.canSplit()) {
      btnJoin = null;
      btnSplit = new JButton(ActivityScripletEditor.createIcon(ActivityScripletEditor.SPLIT_ICON));
    } else {
      btnJoin = null;
      btnSplit = null;
    }

    add(createToolbar(), BorderLayout.PAGE_START);
    add(lblOperation, BorderLayout.CENTER);
    popupMenu = createPopup(operation);

    initializeListeners();

    lblOperation.setText(operation.toString());
  }

  private JPopupMenu createPopup(final ScripletOperation operation) {
    JPopupMenu retval = new JPopupMenu();
    ButtonGroup group = new ButtonGroup();
    radioBtnAnd = new JRadioButtonMenuItem("and");
    radioBtnOr = new JRadioButtonMenuItem("or");
    group.add(radioBtnAnd);
    group.add(radioBtnOr);
    retval.add(radioBtnAnd);
    retval.add(radioBtnOr);
    if (operation == ScripletOperation.AND) {
      radioBtnAnd.setSelected(true);
    } else {
      radioBtnOr.setSelected(true);
    }

    ActionListener listener = new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        if (radioBtnAnd.isSelected()) {
          parent.setOperation(right, ScripletOperation.AND);
        } else {
          parent.setOperation(right, ScripletOperation.OR);
        }
        editor.refresh();
      }
    };

    radioBtnAnd.addActionListener(listener);
    radioBtnOr.addActionListener(listener);

    return retval;
  }

  private JToolBar createToolbar() {
    toolbar = new JToolBar();
    toolbar.setFloatable(false);
    toolbar.setRollover(true);

    toolbar.add(btnEdit);
    if (btnSplit != null) {
      toolbar.add(btnSplit);
    }
    if (btnJoin != null) {
      toolbar.add(btnJoin);
    }
    return toolbar;
  }

  private void initializeListeners() {
    btnEdit.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        popupMenu.show(btnEdit, 0, 0);
      }
    });

    if (btnSplit != null) {
      btnSplit.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          parent.split();
          editor.refresh();
        }
      });
    }

    if (btnJoin != null) {
      btnJoin.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          parent.join(left, right);
          editor.refresh();
        }
      });
    }
  }

}
