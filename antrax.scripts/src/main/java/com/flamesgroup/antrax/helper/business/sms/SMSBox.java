/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.sms;

import java.util.Random;

public class SMSBox {
  private static final Random random = new Random();

  private static final String[] smsArray = new String[] {"Abrasion", "Accumulation", "Active", "Affect", "Air", "Arctic", "Assistance",
      "Astronomy", "Avalanche", "Barometer", "Beauty", "Blizzard", "Blowing", "Blustery", "Bobsled", "Boots", "Breath", "Calendar",
      "Centigrade", "Changes", "Charm", "Chilblain", "Clean", "Clear", "Climate", "Clouds", "Coats", "Cold", "Colder", "Coldest", "Colds",
      "Conditions", "Cool", "Crystals", "Curling", "Daylight", "Dire", "Dreary", "Droplet", "Duration", "Dusting", "Elevation", "Endure",
      "Expect", "Exposure", "Extreme", "Fahrenheit", "Flu", "Flurries", "Forecast", "Freezing", "Frigid", "Frost", "Frozen", "Fuel", "Glacial",
      "Gloves", "Gusty", "Hail", "Hamper", "Hat", "Intense", "Intensify", "Intensity", "Invigorate", "Low", "Luge", "Magic", "Medication",
      "Medicine", "Melting", "Mercury", "Meteorologist", "Moisture", "Moisturize", "Nature", "Outage", "Outdoors", "Overcast", "Period",
      "Polar", "Power", "Protection", "Quake", "Quantity", "Quiver", "Region", "Resistance", "Season", "Several", "Showers", "Skating",
      "Skiing", "Sleet", "Slide", "Slip", "Slush", "Snow", "Snowball", "Snowfall", "Snowflakes", "Snowman", "Snowmobile", "Squall", "Stamina",
      "Storm", "Temperatures", "Time", "Toboggan", "Travel", "Umbrella", "Unbelievable", "Unique", "Unusual", "Variable", "Varying",
      "Visibility", "Warmth", "Weather", "Whiteout", "Wince", "Wind", "Winter", "Wintry", "Wrapped", "Zone"};

  public static String randomSMS() {
    return smsArray[random.nextInt(smsArray.length)];
  }

}
