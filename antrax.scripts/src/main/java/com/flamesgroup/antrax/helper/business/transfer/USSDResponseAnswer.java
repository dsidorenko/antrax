/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.transfer;

import java.io.Serializable;

public class USSDResponseAnswer implements Serializable {

  private static final long serialVersionUID = -3697195529418809177L;

  private String responsePattern;
  private String responseAnswer;
  private boolean responseAnswerPresent;

  public USSDResponseAnswer(final String responsePattern, final String responseAnswer, final boolean responseAnswerPresent) {
    this.responsePattern = responsePattern;
    this.responseAnswer = responseAnswer;
    this.responseAnswerPresent = responseAnswerPresent;
  }

  public boolean hasResponseAnswer() {
    return responseAnswerPresent;
  }

  public void setResponseAnswerPresent(final boolean responseAnswerPresent) {
    this.responseAnswerPresent = responseAnswerPresent;
  }

  public String getResponsePattern() {
    return responsePattern;
  }

  public void setResponsePattern(final String responsePattern) {
    this.responsePattern = responsePattern;
  }

  public String getResponseAnswer() {
    return responseAnswer;
  }

  public void setResponseAnswer(final String responseAnswer) {
    this.responseAnswer = responseAnswer;
  }

  @Override
  public String toString() {
    return String.format("Pattern: [ %s ]; Answer: [ %s ]", responsePattern, responseAnswer);
  }

}
