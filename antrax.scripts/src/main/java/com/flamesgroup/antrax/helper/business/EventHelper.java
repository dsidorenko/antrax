/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;

import java.io.Serializable;

public class EventHelper implements Serializable {

  private static final long serialVersionUID = -3019270688323990815L;

  private final GenericEvent event;
  private final long timeout;

  private volatile EventStatus status;
  private volatile long generateEventTime;

  private String failReason;

  public EventHelper(final GenericEvent event, final long timeout) {
    if (event == null) {
      throw new NullPointerException("Event can't be null");
    }
    this.event = event;
    this.timeout = timeout;
    status = EventStatus.READY;
  }

  public synchronized void generateEvent(final RegisteredInGSMChannel channel) {
    event.fireEvent(channel);
    generateEventTime = System.currentTimeMillis();
    status = EventStatus.WAITING;
  }

  public synchronized boolean isFinished() {
    if (status == EventStatus.WAITING && System.currentTimeMillis() - generateEventTime > timeout) {
      status = EventStatus.TIMEOUT;
      failReason = String.format("timeout for event %s elapsed", event);
    }
    return status != EventStatus.WAITING && status != EventStatus.READY;
  }

  public synchronized EventStatus getStatus() {
    return status;
  }

  public synchronized void handleEvent(final String handledEvent, final Serializable... args) {
    if (event.isFailureResponce(handledEvent, args)) {
      status = EventStatus.FAILED;
      failReason = String.format("event %s failed cause %s", event, event.getFailReason(handledEvent, args));
    } else if (event.isSuccessResponce(handledEvent, args)) {
      status = EventStatus.SUCCEED;
    }
  }

  public String getFailReason() {
    return failReason;
  }

}
