/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.scripts.imeigeneration.IMEIGeneratorRule;
import com.flamesgroup.antrax.scripts.imeigeneration.IMEIGeneratorRuleType;
import com.flamesgroup.antrax.scripts.utils.CalendarPeriod;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class IMEIGeneratorRulePanel extends JPanel {

  private static final long serialVersionUID = 8288016961599820591L;

  private final JComboBox<IMEIGeneratorRuleType> imeiGeneratorRuleTypeComboBox;
  private final JComboBox<CalendarPeriod> periodComboBox;
  private final JSpinner registrationCountSpinner;

  public IMEIGeneratorRulePanel() {
    setLayout(null);

    imeiGeneratorRuleTypeComboBox = new JComboBox<>(IMEIGeneratorRuleType.values());
    imeiGeneratorRuleTypeComboBox.setBounds(10, 10, 265, 25);
    add(imeiGeneratorRuleTypeComboBox);

    periodComboBox = new JComboBox<>(CalendarPeriod.values());
    periodComboBox.setBounds(10, 50, 265, 25);
    periodComboBox.setVisible(false);
    add(periodComboBox);

    registrationCountSpinner = new JSpinner();
    registrationCountSpinner.setBounds(10, 50, 265, 25);
    registrationCountSpinner.setVisible(false);
    add(registrationCountSpinner);

    imeiGeneratorRuleTypeComboBox.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        IMEIGeneratorRuleType ruleType = (IMEIGeneratorRuleType) imeiGeneratorRuleTypeComboBox.getSelectedItem();
        switch (ruleType) {
          case NONE:
            periodComboBox.setVisible(false);
            registrationCountSpinner.setVisible(false);
            break;
          case PERIOD:
            periodComboBox.setVisible(true);
            registrationCountSpinner.setVisible(false);
            break;
          case REGISTRATION_COUNT:
            registrationCountSpinner.setVisible(true);
            periodComboBox.setVisible(false);
            break;
        }
      }
    });
    setPreferredSize(new Dimension(285, 80));
  }

  public IMEIGeneratorRule getIMEIGeneratorRule() {
    IMEIGeneratorRule rule = new IMEIGeneratorRule();
    rule.setType((IMEIGeneratorRuleType) imeiGeneratorRuleTypeComboBox.getSelectedItem());
    rule.setPeriod((CalendarPeriod) periodComboBox.getSelectedItem());
    rule.setRegistrationCount(Integer.parseInt(registrationCountSpinner.getValue().toString()));
    return rule;
  }

  public void setIMEIGeneratorRule(IMEIGeneratorRule rule) {
    if (rule == null) {
      rule = new IMEIGeneratorRule();
    }

    imeiGeneratorRuleTypeComboBox.setSelectedItem(rule.getType());
    periodComboBox.setSelectedItem(rule.getPeriod());
    registrationCountSpinner.setValue(rule.getRegistrationCount());
  }

}
