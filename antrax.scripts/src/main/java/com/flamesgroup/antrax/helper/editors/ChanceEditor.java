/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.scripts.utils.Chance;

import java.awt.*;

import javax.swing.*;

public class ChanceEditor extends BasePropertyEditor<Chance> {

  private final JSpinner editComponent = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));

  @Override
  public Component getEditorComponent() {
    return editComponent;
  }

  @Override
  public Class<? extends Chance> getType() {
    return Chance.class;
  }

  @Override
  public Chance getValue() {
    return new Chance((Integer) editComponent.getValue());
  }

  @Override
  public void setValue(final Chance value) {
    if (value != null) {
      editComponent.setValue(value.getChance());
    } else {
      editComponent.setValue(0);
    }
  }

}
