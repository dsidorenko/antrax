/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import com.flamesgroup.antrax.automation.businesscripts.CallStateChangeHandler;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Exchanger;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class CallPaymentHelper implements PaymentHelper {

  private static final long serialVersionUID = 6778353229702266431L;

  private static final Logger logger = LoggerFactory.getLogger(CallPaymentHelper.class);

  private final String operator;
  private final String call;
  private final boolean enableCheckResponse;
  private final int waitingTimeOut;
  private final String[] responseCheckerRegexes;

  private final BlockingQueue<String> smsQueue = new LinkedBlockingDeque<>();

  public CallPaymentHelper(final String operator, final String call, final boolean enableCheckResponse, final int waitingTimeOut, final String... responseCheckerRegexes) {
    this.operator = operator;
    this.call = call;
    this.enableCheckResponse = enableCheckResponse;
    this.waitingTimeOut = waitingTimeOut;
    this.responseCheckerRegexes = responseCheckerRegexes;
  }

  @Override
  public void pay(final RegisteredInGSMChannel channel, final RegistryAccess registry, final int moneyAmount, final boolean skipIntermStates, final boolean confirmPayment) throws Exception {
    logger.debug("[{}] - going to pay a {} money", this, moneyAmount);
    new VaucherHelper(operator, registry, moneyAmount, skipIntermStates) {
      @Override
      protected boolean executePayment(final String vaucher) throws Exception {
        logger.debug("[{}] - execute payment by call with voucher [{}]", this, vaucher);
        String numberWithVaucher = call.replaceFirst("[$]", vaucher);

        dialAndWaitDrop(channel, new PhoneNumber(numberWithVaucher));

        return !enableCheckResponse || waitingSmsResponse();
      }
    }.pay();
  }

  private void dialAndWaitDrop(final RegisteredInGSMChannel channel, final PhoneNumber phoneNumber) throws Exception {
    logger.debug("[{}] - trying dial to phoneNumber [{}]", this, phoneNumber);
    final Exchanger<CallStateChangeHandler.CallState> callStateExchanger = new Exchanger<>();

    channel.dial(phoneNumber, new CallStateChangeHandler() {

      @Override
      public void handleStateChange(final CallState newState, final Map<CallState, Long> prevStates) {
        try {
          callStateExchanger.exchange(newState);
        } catch (InterruptedException e) {
          logger.warn("[{}] - can't exchange", this, e);
        }
      }

    });

    while (true) {
      CallStateChangeHandler.CallState state = callStateExchanger.exchange(null);
      if (state.equals(CallStateChangeHandler.CallState.DROP)) {
        break;
      }
    }
    logger.debug("[{}] - dial to phoneNumber [{}] succeed", this, phoneNumber);
  }

  private boolean waitingSmsResponse() throws Exception {
    logger.debug("[{}] - waiting for sms response", this);
    long startTime = System.currentTimeMillis();
    long timeOutInMils = waitingTimeOut * 1000;
    String message = null;
    while (true) {
      String sms = smsQueue.poll(timeOutInMils, TimeUnit.MILLISECONDS);
      if (sms == null) {
        break;
      }

      if (checkSms(sms)) {
        logger.debug("[{}] - received sms [{}] with good response", this, sms);
        return true;
      } else {
        message = String.format("sms: [%s] didn't contains text: %s", sms, Arrays.toString(responseCheckerRegexes));
      }
      timeOutInMils -= (System.currentTimeMillis() - startTime);
      startTime = System.currentTimeMillis();
    }
    if (message == null) {
      message = String.format("can't received sms for %d seconds", waitingTimeOut);
    }
    logger.warn("[{}] - {}", this, message);
    throw new Exception(message);
  }

  private boolean checkSms(final String response) {
    logger.debug("[{}] - got such response: {}", this, response);
    for (String checkRegex : responseCheckerRegexes) {
      if (response.matches(checkRegex)) {
        logger.debug("[{}] - response is ok", this);
        return true;
      }
    }
    return false;
  }

  public void handleIncomingSMS(final String phoneNumber, final String text) {
    logger.debug("[{}] - handle incoming SMS phoneNumber [{}], text [{}]", this, phoneNumber, text);
    try {
      smsQueue.put(text);
    } catch (InterruptedException e) {
      logger.error("[{}] - can't put smsQueue text: [{}] to queue", this, text, e);
    }
  }

  public String getOperator() {
    return operator;
  }

  public String getCall() {
    return call;
  }

  public boolean isEnableCheckResponse() {
    return enableCheckResponse;
  }

  public int getWaitingTimeOut() {
    return waitingTimeOut;
  }

  public String[] getResponseCheckerRegexes() {
    return responseCheckerRegexes;
  }

  @Override
  public String toString() {
    return String.format("%s at %s checked with %s", call, operator, Arrays.toString(responseCheckerRegexes));
  }

}
