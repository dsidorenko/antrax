/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import java.io.Serializable;
import java.util.Objects;

public class UssdDialogOperator implements Serializable {

  private static final long serialVersionUID = 3883286391011854448L;

  private final String operator;

  public UssdDialogOperator(final String operator) {
    this.operator = operator;
  }

  public String getOperator() {
    return operator;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof UssdDialogOperator)) {
      return false;
    }
    UssdDialogOperator that = (UssdDialogOperator) object;

    return Objects.equals(operator, that.operator);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(operator);
  }

  @Override
  public String toString() {
    return operator;
  }

}
