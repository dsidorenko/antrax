/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.checkbalance.sms;

import com.flamesgroup.antrax.helper.business.checkbalance.RegexBalanceParser;

import java.io.Serializable;

public class SMSCheckBalanceHelper implements Serializable {

  private static final long serialVersionUID = 7134328949029559583L;

  private final String[] regexes;

  public SMSCheckBalanceHelper(final String... regexes) {
    this.regexes = regexes;
  }

  public double getBalanceValue(final String text) throws Exception {
    return RegexBalanceParser.parseBalance(text, regexes);
  }

  public String[] getRegexes() {
    return regexes;
  }

  @Override
  public String toString() {
    if (regexes != null && regexes.length > 0) {
      return regexes[0];
    } else {
      return "";
    }
  }

}
