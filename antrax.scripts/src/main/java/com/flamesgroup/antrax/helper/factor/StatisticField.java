/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.factor;


public enum StatisticField {

  CALLED_DURATION("called duration"),
  SUCCESSFUL_CALLS_COUNT("successful calls count"),
  TOTAL_CALLS_COUNT("total calls count"),
  SUCCESSFUL_SMS_COUNT("successful sms count"),
  TOTAL_SMS_COUNT("total sms count");

  private final String desc;

  StatisticField(final String desc) {
    this.desc = desc;
  }

  @Override
  public String toString() {
    return desc;
  }

}
