/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import com.flamesgroup.antrax.automation.predictions.BasePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;

import java.text.DateFormat;
import java.util.Calendar;

public class TimePrediction extends BasePrediction {

  private static final long serialVersionUID = 286224989865011321L;

  private final long time;

  public TimePrediction(final long time) {
    this.time = time;
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof TimePrediction || other instanceof TimePeriodPrediction;
  }

  @Override
  protected int compareTo(final Prediction other) {
    if (other instanceof TimePrediction) {
      return (int) (time - ((TimePrediction) other).time);
    }
    if (other instanceof TimePeriodPrediction) {
      TimePeriodPrediction that = (TimePeriodPrediction) other;
      return (int) (time - (System.currentTimeMillis() + that.getTimeout()));
    }
    throw new IllegalStateException();
  }

  @Override
  public String toLocalizedString() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(time);
    return DateFormat.getInstance().format(calendar.getTime());
  }

  public long getTime() {
    return time;
  }

}
