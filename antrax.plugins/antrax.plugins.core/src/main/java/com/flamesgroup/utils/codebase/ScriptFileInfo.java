/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils.codebase;

import java.io.Serializable;
import java.util.Date;

public final class ScriptFileInfo implements Serializable {

  private static final long serialVersionUID = -8609002232791566049L;

  private final String version;
  private final Long checkSum;
  private final Date compilationTime;
  private final Date uploadTime;
  private final Integer fileCount;
  private final Integer classCount;

  public ScriptFileInfo(final String version, final Long checkSum, final Date compilationTime, final Date uploadTime, final Integer fileCount, final Integer classCount) {
    this.version = version;
    this.checkSum = checkSum;
    this.compilationTime = compilationTime;
    this.uploadTime = uploadTime;
    this.fileCount = fileCount;
    this.classCount = classCount;
  }

  public String getVersion() {
    return version;
  }

  public Long getCheckSum() {
    return checkSum;
  }

  public Date getCompilationTime() {
    return compilationTime;
  }

  public Date getUploadTime() {
    return uploadTime;
  }

  public Integer getFileCount() {
    return fileCount;
  }

  public Integer getClassCount() {
    return classCount;
  }

}
