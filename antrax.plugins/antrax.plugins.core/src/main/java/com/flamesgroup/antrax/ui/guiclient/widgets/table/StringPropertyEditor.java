/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.ui.guiclient.widgets.table;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class StringPropertyEditor extends BasePropertyEditor<String> {

  private final JTextField editorComponent;

  public StringPropertyEditor() {
    this(new JTextField());
  }

  public StringPropertyEditor(final JTextField textField) {
    editorComponent = textField;
    editorComponent.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        fireEditingStopped();
      }
    });
  }

  @Override
  public Class<? extends String> getType() {
    return String.class;
  }

  @Override
  public int getClickCountToEdit() {
    return 1;
  }

  @Override
  public void setValue(final String value) {
    editorComponent.setText((value != null) ? value : "");
  }

  @Override
  public String getValue() {
    return editorComponent.getText();
  }

  @Override
  public Component getEditorComponent() {
    return editorComponent;
  }

}
