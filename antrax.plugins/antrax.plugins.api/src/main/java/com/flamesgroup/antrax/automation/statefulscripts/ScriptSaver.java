/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.statefulscripts;

import java.io.Serializable;

/**
 * Provides any {@link StatefullScript} with possibility to notify when it
 * should be saved.
 * <p>
 * Use {@link #save()} when your script changed it's state
 * </p>
 */
public class ScriptSaver implements Serializable {

  private static final long serialVersionUID = -5492356326993125665L;

  private transient boolean requiresSave = false;
  private final ScriptSaver[] delegates;

  public ScriptSaver(final ScriptSaver... delegates) {
    this.delegates = delegates;
    for (ScriptSaver s : delegates) {
      if (s == null) {
        throw new NullPointerException();
      }
    }
  }

  public void save() {
    if (delegates.length == 0) {
      requiresSave = true;
    } else {
      for (ScriptSaver s : delegates) {
        s.save();
      }
    }
  }

  public boolean isRequiresSave() {
    if (delegates.length == 0) {
      return requiresSave;
    } else {
      for (ScriptSaver s : delegates) {
        if (s.isRequiresSave()) {
          return true;
        }
      }
      return false;
    }
  }

  public void handleSaved() {
    if (delegates.length == 0) {
      requiresSave = false;
    } else {
      for (ScriptSaver s : delegates) {
        s.handleSaved();
      }
    }
  }

}
