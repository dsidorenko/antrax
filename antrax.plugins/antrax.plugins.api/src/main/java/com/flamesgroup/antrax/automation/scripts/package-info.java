/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Provides interfaces for all type of scripts.
 * <p>
 * Script is implementation of one of the packaged interfaces with default constructor without any arguments.
 * It also should be annotated with {@linkplain com.flamesgroup.antrax.automation.annotations.Script @Script}.
 * </p>
 * For example:
 * <pre>
 * {@literal @}Script(name="five factor", doc="factor is always 5")
 * public class MyScript implements FactorEvaluationScript {
 *
 *   // Required by FactorEvaluationScript interface
 *   public int countFactor(GSMGroup gsmGroup, Server gateway) {
 *     return 5;
 *   }
 * }
 * </pre>
 * This exmple shows the most simple script ever. Note that it has no any constructor,
 * so that java generates default one for us. You can create your own constructors but
 * do not forget to left one wihtout args for scripting engine.
 * <pre>
 * {@literal @}Script(name="five factor", doc="factor is always 5")
 * public class MyScript implements FactorEvaluationScript {
 *   public MyScript() { // This one is for scripting engine
 *   }
 *
 *   public MyScript(int value) { // This one is for testing purpose
 *     ...
 *   }
 *   ...
 * }
 * </pre>
 * As you see class is public, this is required for scripting engine.
 * Script is documented using annotation {@linkplain com.flamesgroup.antrax.automation.annotations.Script @Script}.
 * Refer to it's documentation for details.
 * <p>
 * <p>
 * Scripts is configured outside using getters and setters.
 * Any setter annotated with {@linkplain com.flamesgroup.antrax.automation.annotations.ScriptParam @ScriptParam}
 * shown user as a configuration point. Setter must have only one argument. The type of argument must
 * be one of the primitive, String or any other which have {@link com.flamesgroup.antrax.automation.editors.PropertyEditor}.
 * Refer to the documentation of {@linkplain com.flamesgroup.antrax.automation.editors.PropertyEditor property editor}.
 * </p>
 * <pre>
 * ... implements ...Script {
 *   ...
 *   {@literal @}ScriptParam(name="lock on fail", doc="locks sim card when this script failed to execute")
 *   public void setLockOnFail(boolean lock) {
 *     this.lockOnFail = lock;
 *   }
 *   ...
 * }
 * </pre>
 * <p>
 * It's up to you to create clear and simple configration of your script.
 * Any parameter should have it's getter with the same name and returning same type. It is required.
 * Getter of param returns a default value for this param. Please be carefull with null values, because
 * they should be properly supported by {@linkplain com.flamesgroup.antrax.automation.editors.PropertyEditor PropertyEditor}.
 * </p>
 * <pre>
 * ... implements ...Script {
 *   ...
 *   {@literal @}ScriptParam(name="lock on fail", doc="locks sim card when this script failed to execute")
 *   public void setLockOnFail(boolean lock) {
 *     this.lockOnFail = lock;
 *   }
 *
 *   public boolean getLockOnFail() {
 *     return false;
 *   }
 * </pre>
 * <p>
 * Note that getter's name starts with "get" but not with "is". This is the limitations for creating parameters.
 * </p>
 * <p>
 * Any script can handle events occured in the system. For example when sim card got a call from voip and successfully
 * ends with it.
 * To handle them you should implement a proper event listener interface.
 * They are packaged in {@link com.flamesgroup.antrax.automation.listeners}. Refer to it's documentation to findout more information.
 * </p>
 * <pre>
 * {@literal @}Script(name="example of lstener")
 * public class ExampleOfListener implements FactorEvaluationScript, // This script is a factor evaluation script
 *                                           CallsListener // This script will listen everything about calls
 *  ...
 *  // Required by FactorEvaluationScript interface
 *  public int countFactor(GSMGroup gsmGroup, Server gateway) {...}
 *  ...
 *  void handleFAS() {
 *    // Called when fas is occured
 *  }
 *
 *  // Required by CallsListener interface
 *  void handleCallSetup(PhoneNumber phoneNumber) {
 *    // Called when call is settuped
 *  }
 *
 *  // Required by CallsListener interface
 *  void handleCallStart(PhoneNumber phoneNumber) {
 *    // Called when call started (i.e. user on the other side answered to our call)
 *  }
 *
 *  // Required by CallsListener interface
 *  void handleCallEnd(long duration) {
 *    // Called when call ended
 *  }
 *
 *  // Required by CallsListener interface
 *  void handleCallError(ErrorReport lastErrorReport) {
 *    // Called when error occured
 *  }
 *
 *   // Required by CallsListener interface
 *  void handleCallForwarded(PhoneNumber phoneNumber) {
 *    // Called when call was forwarded
 *  }
 * </pre>
 * <p>
 * Note that handlers and business methods of script can be called from diferent thread,
 * so use volatiles on all fields modified by events.
 * </p>
 * <pre>
 * ... implements ..Script, CallsListener {
 *   private volatile long totalDuration = 0;
 *   ...
 *   // Required by CallsListener interface
 *   public void handleCallEnd(long duration) {
 *     totalDuration += duration;
 *   }
 *   ...
 * }
 * </pre>
 * <p>
 * Any script has it's lifecycle. For example business script:
 * <pre>
 * sim card loaded
 * sim card goes to VoiceServer-1
 * business script "example" instantiated
 * business script "example" configured using script params
 * use of business script "example"
 * notify event to business script "example"
 * sim card goes to VoiceServer-2
 * business script "example" is garbage collected
 * business script "example" instantiated
 * ...
 * </pre>
 * When card goes to <code>VoiceServer-2</code> it will be recreated and configured from the beginning.
 * This is because script is moved to another server. The same situation may happen when server is crashed
 * or power down. If script changed it's internal state this state is lost.
 * </p>
 * <p>
 * Script can be statefull and lives until sim card is living in database. It's internal states will be
 * recovered each time script is instantiated. If speaking trully script will be serialized and saved
 * to the database.
 * </p>
 * <p>
 * Making script statuefull is simply implement an interface {@link com.flamesgroup.antrax.automation.statefulscripts.StatefullScript}
 * and mark all state fields with annotation {@linkplain com.flamesgroup.antrax.automation.annotations.StateField @StateField}.
 * Refer to the documentation to the {@link com.flamesgroup.antrax.automation.statefulscripts.StatefullScript} and
 * {@link com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver}.
 * <pre>
 * {@literal @}Script(name="sample of statefull script")
 * public class ExampleScript implements FactorEvaluationScript,
 *                               StatefullScript // Here we marks engine to save us
 * {
 *   private ScriptSaver scriptSaver = new ScriptSaver();
 *
 *   {@literal @}StateField
 *   private String lastCountedGroup = null;
 *
 *   ...
 *   // Required by FactorEvaluationScript interface
 *   public int countFactor(GSMGroup gsmGroup, Server gateway) {
 *     ...
 *     lastCountedGroup = gsmGroup.getName(); // Here we modified state of script
 *     scriptSaver.save();                    // And asking to save us (save will not be performed immediately)
 *     ...
 *     return ...;
 *   }
 *   ...
 *   // Required by StatefullScript interface
 *   public ScriptSaver getScriptSaver() { // This is how most of implementation of this method looks
 *     return scriptSaver;
 *   }
 *   ...
 * }
 * </pre>
 * Here is lifecycle of this script:
 * <pre>
 *   sim card loaded
 *   sim card goes to VoiceServer-1
 *   script "sample of statefull script" instantiated
 *   script "sample of statefull script" configured using script params
 * + script "sample of statefull script" restated using deserialized instance from DB
 *   use of script "sample of statefull script"
 * + script "sample of statefull script" saved to DB
 *   sim card goes to VoiceServer-2
 *   business script "sample of statefull script" is garbage collected
 *   business script "sample of statefull script" instantiated
 *   ...
 * </pre>
 * I marked new opertions with +. Note restate and save of script. Refer to the documantation of
 * {@linkplain com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver ScriptSaver} and
 * {@linkplain com.flamesgroup.antrax.automation.statefulscripts.RestateHelper RestateHelper} for advanced info.
 * </p>
 * <p>
 * Any script can log it's activity to the log file. To use this feature
 * refer to {@link com.flamesgroup.antrax.automation.utils.ScriptLogger}.
 * We can't guarantee proper work of other logging systems.
 *
 * @see com.flamesgroup.antrax.automation.annotations.Script
 * @see com.flamesgroup.antrax.automation.statefulscripts.StatefullScript
 * @see com.flamesgroup.antrax.automation.listeners
 */
package com.flamesgroup.antrax.automation.scripts;

