/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.businesscripts;

import java.util.Map;

/**
 * Handles events on every call state change.
 * <p>
 * During call it's state changed over:
 * <ul>
 * </ul>
 * </p>
 */
public interface CallStateChangeHandler {
  /**
   * Enumeration of states occurred during call
   */
  enum CallState {
    /**
     * Initial state. Very short faze when GSM modem initiates dialing to
     * the gsm
     */
    DIALING,
    /**
     * Occurred when GSM got dial and starts alerting call destination about
     * call
     */
    ALERTING,
    /**
     * Occurred after destination answers the call
     */
    ACTIVE,

    /**
     * Occurred after source or destination drops call
     */
    DROP,
  }

  /**
   * Invokes each state change. It passes linked map with previous states per
   * times in which that states been
   *
   * @param newState
   * @param prevStates linked map with state per time
   */
  void handleStateChange(CallState newState, Map<CallState, Long> prevStates);

}
