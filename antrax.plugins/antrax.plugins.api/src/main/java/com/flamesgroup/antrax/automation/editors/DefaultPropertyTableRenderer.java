/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.editors;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Default implementation of cell renderer using {@link Object#toString()}
 *
 * @param <T>
 */
public class DefaultPropertyTableRenderer<T> extends JLabel implements PropertyTableRenderer<T> {

  private static final long serialVersionUID = 1506519378915279528L;

  //  // We need a place to store the color the JLabel should be returned
  //  // to after its foreground and background colors have been set
  //  // to the selection background color.
  //  // These ivars will be made protected when their names are finalized.
  //  private Color unselectedForeground;
  //  private Color unselectedBackground;

  /**
   * Overrides <code>JComponent.setForeground</code> to assign the
   * unselected-foreground color to the specified color.
   *
   * @param c set the foreground color to this value
   */
  @Override
  public void setForeground(final Color c) {
    super.setForeground(c);
    //    unselectedForeground = c;
  }

  /**
   * Overrides <code>JComponent.setBackground</code> to assign the
   * unselected-background color to the specified color.
   *
   * @param c set the background color to this value
   */
  @Override
  public void setBackground(final Color c) {
    super.setBackground(c);
    //    unselectedBackground = c;
  }

  /**
   * Notification from the <code>UIManager</code> that the look and feel [L&F]
   * has changed. Replaces the current UI object with the latest version from
   * the <code>UIManager</code>.
   *
   * @see JComponent#updateUI
   */
  @Override
  public void updateUI() {
    super.updateUI();
    setForeground(null);
    setBackground(null);
  }

  /*
   * The following methods are overridden as a performance measure to to prune
   * code-paths are often called in the case of renders but which we know are
   * unnecessary. Great care should be taken when writing your own renderer to
   * weigh the benefits and drawbacks of overriding methods like these.
   */

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public boolean isOpaque() {
    Color back = getBackground();
    Component p = getParent();
    if (p != null) {
      p = p.getParent();
    }
    // p should now be the JTable.
    boolean colorMatch = (back != null) && (p != null) && back.equals(p.getBackground()) && p.isOpaque();
    return !colorMatch && super.isOpaque();
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   *
   * @since 1.5
   */
  @Override
  public void invalidate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void validate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void revalidate() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void repaint(final long tm, final int x, final int y, final int width, final int height) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void repaint(final Rectangle r) {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   *
   * @since 1.5
   */
  @Override
  public void repaint() {
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  protected void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    // Strings get interned...
    if (propertyName == "text" || propertyName == "labelFor" || propertyName == "displayedMnemonic" || ((propertyName == "font" || propertyName == "foreground") && oldValue != newValue
        && getClientProperty(javax.swing.plaf.basic.BasicHTML.propertyKey) != null)) {

      super.firePropertyChange(propertyName, oldValue, newValue);
    }
  }

  /**
   * Overridden for performance reasons. See the <a
   * href="#override">Implementation Note</a> for more information.
   */
  @Override
  public void firePropertyChange(final String propertyName, final boolean oldValue, final boolean newValue) {
  }

  /**
   * A subclass of <code>DefaultTableCellRenderer</code> that implements
   * <code>UIResource</code>. <code>DefaultTableCellRenderer</code> doesn't
   * implement <code>UIResource</code> directly so that applications can
   * safely override the <code>cellRenderer</code> property with
   * <code>DefaultTableCellRenderer</code> subclasses.
   * <p>
   * <strong>Warning:</strong> Serialized objects of this class will not be
   * compatible with future Swing releases. The current serialization support
   * is appropriate for short term storage or RMI between applications running
   * the same version of Swing. As of 1.4, support for long term storage of
   * all JavaBeans<sup><font size="-2">TM</font></sup> has been added to the
   * <code>java.beans</code> package. Please see {@link java.beans.XMLEncoder}.
   */
  public static class UIResource extends DefaultTableCellRenderer implements javax.swing.plaf.UIResource {

    private static final long serialVersionUID = -6868829100661809528L;
  }

  protected void setValue(final T value) {
    String s = (value == null) ? "" : value.toString();
    setText(s);
    setToolTipText(s);
  }

  @Override
  public Component getRendererComponent(final T value) {
    setValue(value);
    return this;
  }

  public Component getRendererComponent(final String value, final String hint) {
    setText(value);
    setToolTipText(hint);
    return this;
  }

}
